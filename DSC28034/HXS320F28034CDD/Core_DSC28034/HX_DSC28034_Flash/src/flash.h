

#ifndef FLASH_H_
#define FLASH_H_

#include "DSP2803x_Device.h"
#include "DSP2803x_Examples.h"



/*---------------------------------------------------------------------------
 API Status Messages

 The following status values are returned from the API to the calling
 program.  These can be used to determine if the API function passed
 or failed.
---------------------------------------------------------------------------*/
 // Operation passed, no errors were flagged
#define SUCCESS                                    0
#define STATUS_SUCCESS                        0

// The CSM is preventing the function from performing its operation
#define STATUS_FAIL_CSM_LOCKED               10

// Device REVID does not match that required by the API
#define STATUS_FAIL_REVID_INVALID            11

// Invalid address passed to the API
#define STATUS_FAIL_ADDR_INVALID             12

// Incorrect PARTID
// For example the F2806 API was used on a F2808 device.
#define STATUS_FAIL_INCORRECT_PARTID         13

// API/Silicon missmatch.  An old version of the
// API is being used on silicon it is not valid for
// Please update to the latest API.
#define STATUS_FAIL_API_SILICON_MISMATCH     14

// ---- Erase Specific errors ----
#define STATUS_FAIL_NO_SECTOR_SPECIFIED      20
#define STATUS_FAIL_PRECONDITION             21
#define STATUS_FAIL_ERASE                    22
#define STATUS_FAIL_COMPACT                  23
#define STATUS_FAIL_PRECOMPACT               24

// ---- Program Specific errors ----
#define STATUS_FAIL_NO_PAGE_SPECIFIED        20
#define STATUS_FAIL_PROGRAM                  30
#define STATUS_FAIL_ZERO_BIT_ERROR           31

// ---- Verify Specific errors ----
#define STATUS_FAIL_VERIFY                   40

// Busy is set by each API function before it determines
// a pass or fail condition for that operation.
// The calling function will will not receive this
// status condition back from the API
#define STATUS_BUSY                999

/*---------------------------------------------------------------------------
 Flash sector mask definitions

 The following macros can be used to form a mask specifying which sectors
 will be erased by the erase API function.

---------------------------------------------------------------------------*/









/*---------------------------------------------------------------------------
 API Status Structure

 This structure is used to pass debug data back to the calling routine.
 Note that the Erase API function has 3 parts: precondition, erase and
 and compaction. Erase and compaction failures will not populate
 the expected and actual data fields.
---------------------------------------------------------------------------*/

typedef struct {
    Uint32  FirstFailAddr;
    Uint16  ExpectedData;
    Uint16  ActualData;
}FLASH_ST;


/*---------------------------------------------------------------------------
   Frequency Scale factor:
   The calling program must provide this global parameter used
   for frequency scaling the algo's.
----------------------------------------------------------------------------*/

//extern Uint32 Flash_CPUScaleFactor;

/*---------------------------------------------------------------------------
   Callback Function Pointer:
   A callback function can be specified.  This function will be called
   at safe times during erase, program and verify.  This function can
   then be used to service an external watchdog or send a communications
   packet.

   Note:
   THE FLASH AND OTP ARE NOT AVAILABLE DURING THIS FUNCTION CALL.
   THE FLASH/OTP CANNOT BE READ NOR CAN CODE EXECUTE FROM IT DURING THIS CALL
   DO NOT CALL ANY OF THE THE FLASH API FUNCTIONS DURING THIS CALL
----------------------------------------------------------------------------*/
//extern void (*Flash_CallbackPtr) (void);
/****************************************************************************
** 功能：配置flash的控制字
** 入参：Param是指芯片的运行频率，Param的输入范围4~120。
*****************************************************************************/
void  Flash28034CDD_Param_Init(uint32 Param);


Uint16  Flash28034CDD_OTP_Page_EraseVerify(Uint32 PageID);


Uint16  Flash28034CDD_OTP_Page_ProgramVerify(Uint32 PageID,Uint32 *Flash_Buffer);

/****************************************************************************
** 功能：将FLASH中的数据与缓存Buf中的数据进行一致性比对。
** 入参：PageID的取值范围为0~1023。一个Page大小为0x100。
** 入参：Flash_Buffer是缓存的数据的地址。
** 返回值：0代表相应Page的数据写入成功，其他值代表与缓存Buf中的数据不一致的个数。
*****************************************************************************/
Uint16  Flash28034CDD_Page_ProgramVerify(Uint32 PageID,Uint32 *Flash_Buffer);

/****************************************************************************
** 功能：将FLASH中的数据与0xFFFFFFFF进行一致性对比，判断是否擦除成功。
** 入参：PageID的取值范围为0~1023。一个Page大小为0x100。
** 返回值：0代表相应Page为全F。其他值代表不为0xFFFFFFFF的个数。
*****************************************************************************/
Uint16  Flash28034CDD_Page_EraseVerify(Uint32 PageID);
Uint16  Flash28034CDD_OTP_Page_Erase(Uint32 PageID);
Uint16  Flash28034CDD_OTP_Page_Program(Uint32 PageID,Uint32 *Flash_Buffer);

/****************************************************************************
** 功能：根据输入的PageID，对FLASH相应的Page进行擦除。
** 入参：PageID的取值范围为0~1023。一个Page大小为0x100。
** 返回值：0代表成功，10代表未解密，20代表PageID的取值超限，22代表擦除失败。
*****************************************************************************/
Uint16  Flash28034CDD_Page_Erase(Uint32 PageID);

/****************************************************************************
功能：根据输入的SectorID，对FLASH相应的Sector进行擦除。
入参：SectorID的取值范围为0~127。
返回值：0代表成功，10代表未解密，20代表SectorID的取值超限，22代表擦除失败。
*****************************************************************************/
Uint16  Flash28034CDD_Sector_Erase(Uint32 SectorID);

/****************************************************************************
** 功能：根据输入的BankID，对FLASH相应的Bank进行擦除。
** 入参：BankID的取值范围为0~3。
** 返回值：0代表成功，10代表未解密，20代表BankID的取值超限，22代表擦除失败。
*****************************************************************************/
Uint16  Flash28034CDD_Bank_Erase(Uint32 BankID);

/******************************************************************
** 功能：对FLASH相应的Bulk进行擦除。
** 返回值：0代表成功，10代表未解密，22代表擦除失败。
*******************************************************************/
Uint16  Flash28034CDD_Bulk_Erase();

/****************************************************************************
** 功能：根据输入的PageID，将缓存中的数据，烧写到FLASH的相应page中。
** 入参：PageID的取值范围为0~1023。一个Page大小为0x100。
** 入参：Flash_Buffer是缓存的数据的地址。
** 返回值：0代表成功，10代表未解密，20代表PageID的取值超限，30代表烧写失败。
*****************************************************************************/
uint16  Flash28034CDD_Page_Program(Uint32 PageID,Uint32 *Flash_Buffer);

/****************************************************************************
** 功能：从给定的地址处开始，根据长度，将缓存中的数据，烧写到Flash中。
** 入参：FlashAddr的取值范围为0x700000~0x740000。
** 入参：BufAddr是缓存的数据的地址。
** 入参：Length是BufAddr数组的长度。
** 返回值：0代表成功，10代表未解密，12代表FlashAddr的取值超限，30代表烧写失败。
*****************************************************************************/
Uint16  Flash28034CDD_Program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length);
Uint16  Flash28034CDD_OTP_Program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length);
void  Flash28034CDD_OTP_Once_again();
float  Flash28034CDD_APIVersion();
Uint16  Flash28034CDD_APIVersionHex();


#ifdef __cplusplus
}
#endif /* extern "C" */












#endif
