/******************************************************************
 文 档 名：      HX_DSC28034_Flash
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_DSC28034_V1.3
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 作     用：    调用Flash API实现对片内的flash的读写擦除等操作
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz

 连接方式：

 现象：       读写正确的时候，D400/LED1亮。
                   否则，D402闪

 版 本：      V1.0.0
 时 间：      2022年8月25日
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "flash.h"

//
// Flash Status Structure
//
typedef struct
{
		Uint32 *StartAddr;
		Uint32 *EndAddr;
} SECTOR_ST;

/* Total 128 Sectors */
SECTOR_ST Sector[128] = {{(Uint32*)0x700000, (Uint32*)0x7007FF}, {(Uint32*)0x700800, (Uint32*)0x700FFF}, {(Uint32*)0x701000, (Uint32*)0x7017FF}, {(Uint32*)0x701800, (Uint32*)0x701FFF}, {(Uint32*)0x702000, (Uint32*)0x7027FF}, {(Uint32*)0x702800, (Uint32*)0x702FFF}, {(Uint32*)0x703000,
		(Uint32*)0x7037FF}, {(Uint32*)0x703800, (Uint32*)0x703FFF}, {(Uint32*)0x704000, (Uint32*)0x7047FF}, {(Uint32*)0x704800, (Uint32*)0x704FFF}, {(Uint32*)0x705000, (Uint32*)0x7057FF}, {(Uint32*)0x705800, (Uint32*)0x705FFF}, {(Uint32*)0x706000, (Uint32*)0x7067FF}, {(Uint32*)0x706800,
		(Uint32*)0x706FFF}, {(Uint32*)0x707000, (Uint32*)0x7077FF}, {(Uint32*)0x707800, (Uint32*)0x707FFF}, {(Uint32*)0x708000, (Uint32*)0x7087FF}, {(Uint32*)0x708800, (Uint32*)0x708FFF}, {(Uint32*)0x709000, (Uint32*)0x7097FF}, {(Uint32*)0x709800, (Uint32*)0x709FFF}, {(Uint32*)0x70A000,
		(Uint32*)0x70A7FF}, {(Uint32*)0x70A800, (Uint32*)0x70AFFF}, {(Uint32*)0x70B000, (Uint32*)0x70B7FF}, {(Uint32*)0x70B800, (Uint32*)0x70BFFF}, {(Uint32*)0x70C000, (Uint32*)0x70C7FF}, {(Uint32*)0x70C800, (Uint32*)0x70CFFF}, {(Uint32*)0x70D000, (Uint32*)0x70D7FF}, {(Uint32*)0x70D800,
		(Uint32*)0x70DFFF}, {(Uint32*)0x70E000, (Uint32*)0x70E7FF}, {(Uint32*)0x70E800, (Uint32*)0x70EFFF}, {(Uint32*)0x70F000, (Uint32*)0x70F7FF}, {(Uint32*)0x70F800, (Uint32*)0x70FFFF}, {(Uint32*)0x710000, (Uint32*)0x7107FF}, {(Uint32*)0x710800, (Uint32*)0x710FFF}, {(Uint32*)0x711000,
		(Uint32*)0x7117FF}, {(Uint32*)0x711800, (Uint32*)0x711FFF}, {(Uint32*)0x712000, (Uint32*)0x7127FF}, {(Uint32*)0x712800, (Uint32*)0x712FFF}, {(Uint32*)0x713000, (Uint32*)0x7137FF}, {(Uint32*)0x713800, (Uint32*)0x713FFF}, {(Uint32*)0x714000, (Uint32*)0x7147FF}, {(Uint32*)0x714800,
		(Uint32*)0x714FFF}, {(Uint32*)0x715000, (Uint32*)0x7157FF}, {(Uint32*)0x715800, (Uint32*)0x715FFF}, {(Uint32*)0x716000, (Uint32*)0x7167FF}, {(Uint32*)0x716800, (Uint32*)0x716FFF}, {(Uint32*)0x717000, (Uint32*)0x7177FF}, {(Uint32*)0x717800, (Uint32*)0x717FFF}, {(Uint32*)0x718000,
		(Uint32*)0x7187FF}, {(Uint32*)0x718800, (Uint32*)0x718FFF}, {(Uint32*)0x719000, (Uint32*)0x7197FF}, {(Uint32*)0x719800, (Uint32*)0x719FFF}, {(Uint32*)0x71A000, (Uint32*)0x71A7FF}, {(Uint32*)0x71A800, (Uint32*)0x71AFFF}, {(Uint32*)0x71B000, (Uint32*)0x71B7FF}, {(Uint32*)0x71B800,
		(Uint32*)0x71BFFF}, {(Uint32*)0x71C000, (Uint32*)0x71C7FF}, {(Uint32*)0x71C800, (Uint32*)0x71CFFF}, {(Uint32*)0x71D000, (Uint32*)0x71D7FF}, {(Uint32*)0x71D800, (Uint32*)0x71DFFF}, {(Uint32*)0x71E000, (Uint32*)0x71E7FF}, {(Uint32*)0x71E800, (Uint32*)0x71EFFF}, {(Uint32*)0x71F000,
		(Uint32*)0x71F7FF}, {(Uint32*)0x71F800, (Uint32*)0x71FFFF}, {(Uint32*)0x720000, (Uint32*)0x7207FF}, {(Uint32*)0x720800, (Uint32*)0x720FFF}, {(Uint32*)0x721000, (Uint32*)0x7217FF}, {(Uint32*)0x721800, (Uint32*)0x721FFF}, {(Uint32*)0x722000, (Uint32*)0x7227FF}, {(Uint32*)0x722800,
		(Uint32*)0x722FFF}, {(Uint32*)0x723000, (Uint32*)0x7237FF}, {(Uint32*)0x723800, (Uint32*)0x723FFF}, {(Uint32*)0x724000, (Uint32*)0x7247FF}, {(Uint32*)0x724800, (Uint32*)0x724FFF}, {(Uint32*)0x725000, (Uint32*)0x7257FF}, {(Uint32*)0x725800, (Uint32*)0x725FFF}, {(Uint32*)0x726000,
		(Uint32*)0x7267FF}, {(Uint32*)0x726800, (Uint32*)0x726FFF}, {(Uint32*)0x727000, (Uint32*)0x7277FF}, {(Uint32*)0x727800, (Uint32*)0x727FFF}, {(Uint32*)0x728000, (Uint32*)0x7287FF}, {(Uint32*)0x728800, (Uint32*)0x728FFF}, {(Uint32*)0x729000, (Uint32*)0x7297FF}, {(Uint32*)0x729800,
		(Uint32*)0x729FFF}, {(Uint32*)0x72A000, (Uint32*)0x72A7FF}, {(Uint32*)0x72A800, (Uint32*)0x72AFFF}, {(Uint32*)0x72B000, (Uint32*)0x72B7FF}, {(Uint32*)0x72B800, (Uint32*)0x72BFFF}, {(Uint32*)0x72C000, (Uint32*)0x72C7FF}, {(Uint32*)0x72C800, (Uint32*)0x72CFFF}, {(Uint32*)0x72D000,
		(Uint32*)0x72D7FF}, {(Uint32*)0x72D800, (Uint32*)0x72DFFF}, {(Uint32*)0x72E000, (Uint32*)0x72E7FF}, {(Uint32*)0x72E800, (Uint32*)0x72EFFF}, {(Uint32*)0x72F000, (Uint32*)0x72F7FF}, {(Uint32*)0x72F800, (Uint32*)0x72FFFF}, {(Uint32*)0x730000, (Uint32*)0x7307FF}, {(Uint32*)0x730800,
		(Uint32*)0x730FFF}, {(Uint32*)0x731000, (Uint32*)0x7317FF}, {(Uint32*)0x731800, (Uint32*)0x731FFF}, {(Uint32*)0x732000, (Uint32*)0x7327FF}, {(Uint32*)0x732800, (Uint32*)0x732FFF}, {(Uint32*)0x733000, (Uint32*)0x7337FF}, {(Uint32*)0x733800, (Uint32*)0x733FFF}, {(Uint32*)0x734000,
		(Uint32*)0x7347FF}, {(Uint32*)0x734800, (Uint32*)0x734FFF}, {(Uint32*)0x735000, (Uint32*)0x7357FF}, {(Uint32*)0x735800, (Uint32*)0x735FFF}, {(Uint32*)0x736000, (Uint32*)0x7367FF}, {(Uint32*)0x736800, (Uint32*)0x736FFF}, {(Uint32*)0x737000, (Uint32*)0x7377FF}, {(Uint32*)0x737800,
		(Uint32*)0x737FFF}, {(Uint32*)0x738000, (Uint32*)0x7387FF}, {(Uint32*)0x738800, (Uint32*)0x738FFF}, {(Uint32*)0x739000, (Uint32*)0x7397FF}, {(Uint32*)0x739800, (Uint32*)0x739FFF}, {(Uint32*)0x73A000, (Uint32*)0x73A7FF}, {(Uint32*)0x73A800, (Uint32*)0x73AFFF}, {(Uint32*)0x73B000,
		(Uint32*)0x73B7FF}, {(Uint32*)0x73B800, (Uint32*)0x73BFFF}, {(Uint32*)0x73C000, (Uint32*)0x73C7FF}, {(Uint32*)0x73C800, (Uint32*)0x73CFFF}, {(Uint32*)0x73D000, (Uint32*)0x73D7FF}, {(Uint32*)0x73D800, (Uint32*)0x73DFFF}, {(Uint32*)0x73E000, (Uint32*)0x73E7FF}, {(Uint32*)0x73E800,
		(Uint32*)0x73EFFF}, {(Uint32*)0x73F000, (Uint32*)0x73F7FF}, {(Uint32*)0x73F800, (Uint32*)0x73FFFF}, };




//FLASH_ST FlashStatus,FlashProgstatus;
//#define Flash28034CDD_Param_Init              (Uint16 (*)(void))0x7fe100
//#define Flash28034CDD_Sector_Erase            (Uint16 (*)(Uint32 SectorIdx, FLASH_ST *FEraseStat))0x7fe83e
//#define Flash28034CDD_Page_Program            (Uint16 (*)(Uint32 PageID,Uint32 *Flash_Buffer, FLASH_ST *FProgramStat))0x7feca4
//#define Flash28034CDD_Program                 (Uint16 (*)(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FEraseStat))0x7fef54
//#define Flash28034CDD_Bank_Erase	           (Uint16 (*)(Uint32 BankID,FLASH_ST *FEraseStat))0x7fea7a
//#define Flash28034CDD_Bulk_Erase	           (Uint16 (*)(FLASH_ST *FEraseStat))0x7feb9c
//#define Flash28034CDD_OTP_Page_Erase          (Uint16 (*)(Uint32 *OtpAddr,Uint32 *BufAddr,Uint32 Length,FLASH_ST *FProgStatus))0x7fdf60

uint16 status;




Uint16  Flash28034CDD_ProgramVerify(Uint32 *Flash_Addr,Uint32 *Flash_Buffer,Uint32 length,FLASH_ST *FVerifyStat)
{
	Uint32 i,ECnt;
	ECnt = 0;
	for(i = 0;i < length;i++)
	{
		if(*(Flash_Addr + i) != *((Uint32 *)Flash_Buffer+i))
		{
          ECnt++;
		}

	}
	if(ECnt == 0)
		return 0;
	else
		return 31;
}

uint32 OTP_Data[512] ={};
uint32 temp[30];
uint32 i;
uint32 check;

void GPIO_INT()
{
    EALLOW;
    GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0;
    GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0;
    GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0;
    GpioCtrlRegs.GPBMUX1.bit.GPIO44= 0;

    GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;
    GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
    GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
    GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;

    GpioDataRegs.GPBSET.bit.GPIO34 = 1;
    GpioDataRegs.GPBSET.bit.GPIO41 = 1;
    GpioDataRegs.GPBSET.bit.GPIO43 = 1;
    GpioDataRegs.GPBSET.bit.GPIO44 = 1;

    EDIS;
}





int main(void)
{
    /*
     * If the user need Flash Pipeline mode to improve performance, The user can uncomment InitFlash();
     * If the system clock goes from low to high, the user needs to configure FBANKWAIT at
     * low clock to suit the control word at high clock.
     * The default FBANKWAIT value is 4.
     * For the configuration of other clocks,Please refer to the following link:
     * http://haawking.com/zyxz
     */
//    InitFlash();


    InitSysCtrl();  //Initializes the System Control registers to a known state.

    CsmUnlock();
    GPIO_INT();

    //初始化flash编程，
    //入口参数：Param是指芯片的运行频率，Param的输入范围4~120。
    Flash28034CDD_Param_Init(120);

    //初始化写入的数据
        for(i = 0;i < 20 ; i++ )
        {
        	OTP_Data[i] = 0xAA550000+i;
        }
     //擦除第120扇区
        	status =   Flash28034CDD_Sector_Erase(120);
        	if(status != 0)
        	{
        		GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1;
        		while(1);
        	}
      //写入数据
       	 status = (* Flash28034CDD_Program)((uint32 *)(0x700000+0x800*120), OTP_Data, 20);
   	       if(0 != status)
           {
        	    	GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1;
        	    	while(1);
   	       }
   	   //校验数据
   	    check = 0;
           for(i = 0;i < 20 ;i++ )
           {
        	   temp[i] =  (*(uint32 *)(Sector[120].StartAddr+i));
        	   if(OTP_Data[i] != temp[i])
        		   check++;
           }

           if(check == 0)
           {
        	   GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
        	   GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
           }
           else
           {
        	   GpioDataRegs.GPBDAT.bit.GPIO43 = 0;
        	   GpioDataRegs.GPBDAT.bit.GPIO44=0;
        	   for(i = 0; i < 120000; i++);
        	   GpioDataRegs.GPBDAT.bit.GPIO43 = 1;
        	   GpioDataRegs.GPBDAT.bit.GPIO44 = 1;
        	   for(i = 0; i < 120000; i++);

           }



    while(1){

        	if(check == 0)
        	{
        		GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
        		GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
        	}
        	else
        	{
        		GpioDataRegs.GPBDAT.bit.GPIO43 = 0;
        		GpioDataRegs.GPBDAT.bit.GPIO44=0;
        		for(i = 0; i < 120000; i++);
        		GpioDataRegs.GPBDAT.bit.GPIO43 = 1;
        		GpioDataRegs.GPBDAT.bit.GPIO44 = 1;
        		for(i = 0; i < 120000; i++);

        	}
    }

	return 0;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
