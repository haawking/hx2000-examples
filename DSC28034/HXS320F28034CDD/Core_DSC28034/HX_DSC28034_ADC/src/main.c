/*
 *
 *******************************************************************
 文 档 名：     HX_DSC28034_ADC
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：     Core_DSC28034_V1.5
                       Start_DSC28034_V1.2
 D S P：       DSC28034
 使 用 库：
 作     用：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过配置ADC，PWM1的寄存器，实现ADC采集，获取被测电压对应数字量

 连接方式：把被测电压和5A连接
			Core板上将测试的电压与F28034的与ADC通道采集输入引脚A5连接
 	 	 	 Start板上电位器RG端与ADC通道采集输入引脚A5连接

 现象：在Haawking IDE调试界面上可正确读出电压对应的数字量值adcValue
 	 	    Start板上当正常运行时，	D400	亮，		D401	灭；
 	 	             当adc采样异常时，	D400	灭，		D401	亮；

 版 本：V1.0.0
 时 间：2022年12月26日
 作 者：
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

extern void pwmInit();
extern void adcInit();
extern void gpioInit();
extern void adcIsr();

uint16_t adcCount = 0;
uint16_t adcValue[3] = {0,0,0};
uint16_t i = 0,j = 0;
uint16_t error = 0;

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    DINT;

    gpioInit();
    pwmInit();
    adcInit();

    EALLOW;
	PieVectTable.ADCINT1 = &adcIsr;
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;
	IER |= M_INT1;
	EINT;
	EDIS;

    while(1)
    {

    }

	return 0;
}

__interrupt void adcIsr()
{
	adcCount ++;

	for(i=0;i<3;i++)
	{
		adcValue[i] = 0;
	}

	adcValue[0] = AdcResult.ADCRESULT0;
	if(!((adcValue[0]>0)&&(adcValue[0]<4095)))
	{
		error++;
	}
	adcValue[1] = AdcResult.ADCRESULT1;
	if(!((adcValue[1]>0)&&(adcValue[1]<4095)))
	{
		error++;
	}

	adcValue[2] = AdcResult.ADCRESULT2;
	if(!((adcValue[2]>0)&&(adcValue[2]<4095)))
	{
		error++;
	}

	if(adcCount > 1000)
	{
		adcCount = 0;
		if(error != 0)
		{
			GpioDataRegs.GPBSET.bit.GPIO41 = 1;
			GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1;

		}
		else
		{
			GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
			GpioDataRegs.GPBSET.bit.GPIO34 = 1;

		}
	}


	AdcRegs.ADCINTFLGCLR.all = 1;
	AdcRegs.ADCINTOVFCLR.all = 1;
	PieCtrlRegs.PIEACK.bit.ACK1 = 1;
}

void pwmInit()
{
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;

	EPwm1Regs.TBPRD = 12000;               	//200k
	EPwm1Regs.TBPHS.half.TBPHS = 0;
	EPwm1Regs.TBCTR = 0;
	EPwm1Regs.TBCTL.bit.FREE_SOFT = 3;
	EPwm1Regs.TBCTL.bit.CLKDIV = 0;
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm1Regs.TBCTL.bit.CTRMODE = 0;    			// up count

	EPwm1Regs.CMPA.half.CMPA = 6000;
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0; // CMPA shadow
	EPwm1Regs.CMPCTL.bit.LOADAMODE = 1; // Load on CTR = Prd

	EPwm1Regs.AQCTLA.bit.ZRO = 2;       //set
	EPwm1Regs.AQCTLA.bit.CAU = 1;       //clear

	EPwm1Regs.ETSEL.bit.SOCAEN = 1;
	EPwm1Regs.ETSEL.bit.SOCASEL = 1;
	EPwm1Regs.ETPS.bit.SOCACNT = 1;
	EPwm1Regs.ETPS.bit.SOCAPRD = 1;
	EPwm1Regs.ETCLR.bit.SOCA = 1;

	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void adcInit()
{
	InitAdc();
	EALLOW;

	AdcRegs.ADCCTL1.bit.INTPULSEPOS=1;
	//AdcRegs.ADCCTL1.bit.TEMPCONV=0;

	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 0;    // System clock is 120M,divided by two

	AdcRegs.INTSEL1N2.bit.INT1CONT = 1;
	AdcRegs.INTSEL1N2.bit.INT1SEL = 2;		//EOC6 TRIG
	AdcRegs.INTSEL1N2.bit.INT1E = 1;

	AdcRegs.ADCINTOVFCLR.all = 1;
	AdcRegs.ADCINTFLGCLR.all = 1;

	AdcRegs.ADCSOC0CTL.bit.CHSEL = 5;		//A5
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 5;		//SOCx触发源选择:ADCTRIG5-ePWM1
	AdcRegs.ADCSOC0CTL.bit.ACQPS = 7;		//设置采样窗口20个周期（2n+6个时钟周期）

	AdcRegs.ADCSOC1CTL.bit.CHSEL = 5;		//A5
	AdcRegs.ADCSOC1CTL.bit.TRIGSEL = 5;
	AdcRegs.ADCSOC1CTL.bit.ACQPS = 7;

	AdcRegs.ADCSOC2CTL.bit.CHSEL = 5;		//A5
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 5;
	AdcRegs.ADCSOC2CTL.bit.ACQPS = 7;

	EDIS;
}

void gpioInit()
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0; 	//D400 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0; 	//D401 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;
	GpioDataRegs.GPBSET.bit.GPIO34 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; 	//D402 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	EDIS;
}
// ----------------------------------------------------------------------------
