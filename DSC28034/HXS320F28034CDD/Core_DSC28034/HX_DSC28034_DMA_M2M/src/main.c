/******************************************************************
 文 档 名：      HXcore_DSC28034_DMA_M2M
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_DSC28034_V1.5
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过DMA实现从内存到内存的数据传输

 连接方式：

 现象：
 Core: DMA传输数据成功并且正确，LED1(GPIO44)点亮；
 	 	  DMA传输数据失败或不一致， LED1(GPIO44)灭
 Start: DMA传输数据成功并且正确，D402(GPIO43)点亮；
 	 	  DMA传输数据失败或不一致，D402 LED1(GPIO43)灭

 版 本：      V1.0.0
 时 间：      2023年3月24日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"


//
// Defines
//
#define BUF_SIZE   1024         // Sample buffer size

//
// DMA Defines
//
#define CH1_TOTAL               DATA_POINTS_PER_CHANNEL
#define CH1_WORDS_PER_BURST     ADC_CHANNELS_TO_CONVERT


volatile Uint16 CODE_SECTION("ramfuncs1")  DMABuf1[1024];
volatile Uint16 CODE_SECTION("ramfuncs1")  DMABuf2[1024];

volatile Uint32 *DMADest;
volatile Uint32 *DMASource;

//
// Function Prototypes
//
__interrupt void local_DINTCH1_ISR(void);
uint32 i;



void GPIO_INIT(){
	    EALLOW;
	    GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0;
	    GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0;
	    GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0;
	    GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;

	    GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;
	    GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	    GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	    GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;

	    GpioDataRegs.GPBSET.bit.GPIO34 = 1;
	    GpioDataRegs.GPBSET.bit.GPIO41 = 1;
	    GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	    GpioDataRegs.GPBSET.bit.GPIO44 = 1;

	    EDIS;
}
//
// Main
//
int main(void)
{
    /*
     * If the user need Flash Pipeline mode to improve performance, The user can uncomment InitFlash();
     * If the system clock goes from low to high, the user needs to configure FBANKWAIT at
     * low clock to suit the control word at high clock.
     * The default FBANKWAIT value is 4.
     * For the configuration of other clocks,Please refer to the following link:
     * http://haawking.com/zyxz
     */
	//InitFlash();
    InitSysCtrl();  //Initializes the System Control registers to a known state.
    Uint16 i;
    DINT;


     InitPieCtrl();

     //
     // Disable CPU interrupts and clear all CPU interrupt flags
     //
     IER = 0x0000;
     IFR = 0x0000;

     //
     // Initialize the PIE vector table with pointers to the shell Interrupt
     // Service Routines (ISR).
     // This will populate the entire table, even if the interrupt
     // is not used in this example.  This is useful for debug purposes.
     //
     InitPieVectTable();

     //
     // Interrupts that are used in this example are re-mapped to
     // ISR functions found within this file.
     //
     EALLOW;	// Allow access to EALLOW protected registers
     PieVectTable.DINTCH1= &local_DINTCH1_ISR;
     EDIS;   // Disable access to EALLOW protected registers

      //
     // Step 5. User specific code, enable interrupts:
     //
     IER = M_INT7 ;                        //Enable INT7 (7.1 DMA Ch1)
     EnableInterrupts();
     CpuTimer0Regs.TCR.bit.TSS  = 1;       //Stop Timer0 for now

     //
     // Initialize DMA
     //
     DMAInitialize();
     GPIO_INIT();
     //
     // Initialize Tables
     //
     for (i=0; i<BUF_SIZE; i++)
     {
         DMABuf1[i] = 0;
         DMABuf2[i] = i;
     }

     //
     // Configure DMA Channel
     //
     DMADest   = (Uint32 *)&DMABuf1[0];
     DMASource = (Uint32 *)&DMABuf2[0];
     DMACH1AddrConfig(DMADest,DMASource);

     //
     // Will set up to use 32-bit datasize, pointers are based on 16-bit words
     //
     DMACH1BurstConfig(31,2,2);

     //
     // so need to increment by 2 to grab the correct location
     //
     DMACH1TransferConfig(31,2,2);
     DMACH1WrapConfig(0xFFFF,0,0xFFFF,0);

     //
     // Use timer0 to start the x-fer.
     // Since this is a static copy use one shot mode, so only one trigger
     // is needed. Also using 32-bit mode to decrease x-fer time
     //
     DMACH1ModeConfig(DMA_TINT0,PERINT_ENABLE,ONESHOT_ENABLE,CONT_DISABLE,
                      SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,THIRTYTWO_BIT,
                      CHINT_END,CHINT_ENABLE);

     StartDMACH1();

     //
     // Init the timer 0
     //

     //
     // load low value so we can start the DMA quickly
     //
     CpuTimer0Regs.TIM.half.LSW = 512;
 //    CpuTimer0Regs.TCR.bit.SOFT = 1;      //Allow to free run even if halted
 //    CpuTimer0Regs.TCR.bit.FREE = 1;
     CpuTimer0Regs.TCR.bit.TIE  = 1;      //Enable the timer0 interrupt signal
     CpuTimer0Regs.TCR.bit.TSS  = 0;      //restart the timer 0
     for(;;)
     {

     }
 }

 //
 // local_DINTCH1_ISR - INT7.1(DMA Channel 1)
 //
 __interrupt void
 local_DINTCH1_ISR(void)
 {
     //
     // To receive more interrupts from this PIE group, acknowledge this
     // interrupt
     //
     PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;

     for (i = 0;i<BUF_SIZE;i++)
     {
     	if(DMABuf1[i] != DMABuf2[i])
     	{
     		GpioDataRegs.GPBSET.bit.GPIO44 = 1;
     		GpioDataRegs.GPBSET.bit.GPIO43 = 1;
     		while(1);
     	}
     }
     GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
  	 GpioDataRegs.GPBCLEAR.bit.GPIO43 = 1;
  	  while(1);

 }

 //
 // End of File
 //
// ----------------------------------------------------------------------------
