#include "HRCAP.h"

//defines
#define PWM1_TIMER_MAX 4000
#define PWM2_TIMER_MAX 4000

Uint16 period;
Uint16 EPwm1TimerDirection;
Uint16 EPwm2TimerDirection;

#define EPWM_TIMER_UP   1
#define EPWM_TIMER_DOWN 0

void ePWM1_Config(Uint16 period)
{
	/*采用向下计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE=TB_COUNT_DOWN;
	/*配置EPWM的周期为period+1个TBCLK*/
	EPwm1Regs.TBPRD=period;
	/*配置EPWM输出无相位延迟*/
	EPwm1Regs.TBPHS.all=0x0;
	/*配置AQ动作模块，当CTR=PRD时，PWM波输出翻转*/
	EPwm1Regs.AQCTLA.bit.PRD=AQ_TOGGLE;
	/*配置PWM输出不采用高速与低速时钟分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	EPwm1Regs.TBCTL.bit.CLKDIV=TB_DIV1;
	/*配置PWM输出周期方向初始为向下，初始方向为递减*/
	EPwm1TimerDirection=EPWM_TIMER_DOWN;
}

void ePWM2_Config(Uint16 period)
{
	/*采用向上计数模式*/
	EPwm2Regs.TBCTL.bit.CTRMODE=TB_COUNT_UP;
	EPwm2Regs.TBPRD=period;
	EPwm2Regs.TBPHS.all=0x0;
	EPwm2Regs.AQCTLA.bit.PRD=AQ_TOGGLE;

	EPwm2Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	EPwm2Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	/*配置PWM输出周期方向初始为向下，初始方向为递增*/
	EPwm2TimerDirection=EPWM_TIMER_UP;
}
