#include "HRCAP.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO34=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO34=1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO41=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO41=1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43=1;

	GpioCtrlRegs.GPAMUX1.bit.GPIO6=0;
	GpioCtrlRegs.GPADIR.bit.GPIO6=1;
	EDIS;
}
