#include "dsc_config.h"
/******************************************************************
*函数名：void InitLED()
*参 数：无
*返回值：无
*作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的空间*/
	EALLOW;
	/*将GPIO44配置为数字IO*/
	GpioCtrlRegs.GPBMUX1.bit.GPIO44=0;
	/*将GPIO44配置为输出*/
	GpioCtrlRegs.GPBDIR.bit.GPIO44=1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO41=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO41=1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43=1;

	/*禁止访问受保护的空间*/
	EDIS;
}
