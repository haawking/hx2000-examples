#include "spi.h"

Uint16 sdata;
Uint16 rdata;
Uint16 rdata_point;

#define true 1
#define false 0

bool_t status=true;

INTERRUPT void spiTxFifoIsr(void)
{
	SpiaRegs.SPITXBUF=sdata;

	 /*SPI发送FIFO中断清零*/
	SpiaRegs.SPIFFTX.bit.TXFFINTCLR=1;
	 /*中断应答：锁定SPI中断*/
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP6;
}

INTERRUPT void spiRxFifoIsr(void)
{
	rdata=SpiaRegs.SPIRXBUF;

	if(rdata==sdata)
	{
		status=true;
	}
	else
	{
		status=false;
	}

	if(status==true)
	{
		GpioDataRegs.GPBCLEAR.bit.GPIO44=1;
		GpioDataRegs.GPBCLEAR.bit.GPIO43=1;
	}
	else
	{
		GpioDataRegs.GPBSET.bit.GPIO44=1;
		GpioDataRegs.GPBSET.bit.GPIO43=1;
	}

	/*SPI接收溢出FIFO中断清零*/
	SpiaRegs.SPIFFRX.bit.RXFFOVFCLR=1;
	/*SPI接收FIFO中断清零*/
	SpiaRegs.SPIFFRX.bit.RXFFINTCLR=1;
	 /*中断应答：锁定SPI中断*/
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP6;
}

