
#ifndef		__f_dspcan__
#define		__f_dspcan__

#include "dsc_config.h"

#define		ECANMBOXES			ECanaMboxes					// 邮箱宏定义

// CAN数据
typedef struct DSP_CAN_DATA                                  // 数据引用联合体
{
    Uint32 mdl;
	Uint32 mdh;	
}CanDataType;

// DSP  CAN帧完整 数据结构
typedef struct DSP_CAN_TYPE
{
	Uint32  msgid;                                          // ID 29位  11位使用低11位
    CanDataType data;                                       // CAN帧数据
	Uint16 len;                                             // 缓存数据长度
}DspCanDataStru;

//功能函数定义
void CAN_Gpio(void);
void CAN_Init(void);
uint16 CAN_GetWordData(void);
INTERRUPT void eCanRxIsr(void);//CAN接收
void CAN_trans(void);//CAN发送
#endif


