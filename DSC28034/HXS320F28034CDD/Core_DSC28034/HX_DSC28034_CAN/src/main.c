/******************************************************************
 文 档 名：      HX_DSC28034_ECAN
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_DSC28034_V1.3
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：

 连接方式：1.VCC-3.3V 共地 GPIO30-RX GPIO31-TX 连接到USB-CAN模块上
 	 	 	 	  2. USB-CAN模块连接湖人板CANH CANL，J3（ 2-1引脚）相连
 现象：      通过USB_CAN通信软件，可实时显示发送与接收数据。1000Kbps ,ID 01

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include "IQmathLib.h"
#include "f_dspcan.h"

int main(void)
{
    InitSysCtrl();//系统时钟初始化, 使用外部时钟12M
    CAN_Gpio();//CAN的Gpio引脚配置
	CAN_Init();//CAN的初始化参数配置
    EALLOW;
    PieCtrlRegs.PIEIER9.bit.INTx5 = 0;                      // 禁止中断
    ECanaRegs.CANGIM.all = 0;
    ECanaRegs.CANMIM.all = 0;                               // 禁用所有邮箱中断
    ECanaRegs.CANGIF0.all = 0xffffffff;
    ECanaRegs.CANGIF1.all = 0xffffffff;                     // 清除中断
    ECanaRegs.CANMIL.bit.MIL1 = 0;                               // 选择EcanA的发送MBOX1中断映射INT0
    ECanaRegs.CANGIM.bit.I0EN = 1;                               // 使能打开中断INT0
    PieVectTable.ECAN0INTA = &eCanRxIsr;                    // CANA 0接收中断入口
    EDIS;
    PieCtrlRegs.PIEIER9.bit.INTx5 = 1;                      // 使能ECAN1中断
    IER |= M_INT9; 														// Enable CPU INT9
    EINT;
    while(1){
    	CAN_trans();//CAN的发送
    }
	return 0;
}
// ----------------------------------------------------------------------------
