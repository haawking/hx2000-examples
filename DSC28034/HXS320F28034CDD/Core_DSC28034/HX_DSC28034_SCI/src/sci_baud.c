#include "dsc_config.h"

void Scia_Print(char *str);

void Scia_Autobaudlock(void)
{
	Uint16 byteData;
	uint16_t BRR = 0;
	uint32_t baudrate = 0;
	char tmp[10];

	SciaRegs.SCILBAUD = 1;
	/*使能波特率自动对齐*/
	SciaRegs.SCIFFCT.bit.CDC = 1;
	/* 清除第15位的ABD标志*/
	SciaRegs.SCIFFCT.bit.ABDCLR = 1;

	while(SciaRegs.SCIFFCT.bit.ABD != 1)
	{
	}
	/*清除第15位的ABD标志*/
	SciaRegs.SCIFFCT.bit.ABDCLR = 1;
	/* 禁用波特自动对齐*/
	SciaRegs.SCIFFCT.bit.CDC = 0;

	while(SciaRegs.SCIRXST.bit.RXRDY != 1)
	{
	}
	byteData = SciaRegs.SCIRXBUF.bit.RXDT;
	/*传输数据缓存*/
	SciaRegs.SCITXBUF = byteData;

	BRR = ((uint16_t)(SciaRegs.SCIHBAUD << 8) + SciaRegs.SCILBAUD);
	baudrate = 30000000 / ((BRR + 1) * 8);
	itoa(baudrate, tmp, 10);
	Scia_Print("\r\nBaudRate: ");
	Scia_Print(tmp);
	Scia_Print("\r\n");

	return;
}


/******************************************************************
 *函数名：Scia_Print(char *str)
 *参   数： *str，要发送的字符串
 *返回值：无
 *作   用： SCIA 发送一个字符串
 ******************************************************************/
void Scia_Print(char *str)
{
	while (*str != '\0')
	{
		while (SciaRegs.SCICTL2.bit.TXRDY == 0)
		{
		}

		SciaRegs.SCITXBUF = *str++;
	}
}

