//###########################################################################
//
// FILE:    DSP2803x_OTP.c
//
// TITLE:   DSP2803x OTP file.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-02-03 01:12:06 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "DSP2803x_Device.h"     //  Headerfile Include File


/***************************************************************
  To  uncomment ,you can use OTP
*****************************************************************/

/***************************************************************

volatile Uint32   CODE_SECTION(".User_Otp_Table") OTP_DATA[] = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};


*****************************************************************/
