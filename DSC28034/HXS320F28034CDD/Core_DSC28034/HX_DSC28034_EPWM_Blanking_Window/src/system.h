
#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"

//epwm
void InitEpwm2_Example(void);

//epwm_tz
void INTERRUPT epwm2_tz_isr(void);

void InitLED(void);

#endif /* SYSTEM_H_ */
