//###########################################################################
//
// FILE:    disable_interrupt.S
//
// TITLE:   disable_interrupt Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-02-03 01:12:06 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 disable_interrupt


disable_interrupt:
    li a0, 0x0
    csrw mstatus, a0
	ret


	

