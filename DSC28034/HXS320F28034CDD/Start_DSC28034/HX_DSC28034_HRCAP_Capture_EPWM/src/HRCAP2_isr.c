#include "HRCAP.h"

#define PWM2_TIMER_MAX 4000

#define EPWM_TIMER_UP   1
#define EPWM_TIMER_DOWN 0

void INTERRUPT hrcap2_isr(void)
{
	EALLOW;
	/*延迟一条指令周期，保证EPWM到达下降沿，便于捕获*/
	if(first==0)
	{
		first=1;
	}
	else
	{
		/*下降沿捕获使用禁用*/
		HRCap2Regs.HCCTL.bit.FALLINTE=0;
		/*高脉冲宽度为上升沿捕获计数器计数+1*/
				PULSEHIGH=HRCap2Regs.HCCAPCNTRISE1+1;
				/*高脉冲宽度HRCAP的可捕获周期之外，捕获失败，执行空语句*/
				if((PULSEHIGH>((Uint32)(EPwm2Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ+NUM_HCCAPCLKS_PER_HCSYSCLK+1))||(PULSEHIGH<((Uint32)(EPwm2Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ-NUM_HCCAPCLKS_PER_HCSYSCLK-1)))
				{
					__asm(" NOP");
				}

				/*低脉冲宽度为下降沿捕获计数器计数+1*/
				PULSELOW=HRCap1Regs.HCCAPCNTFALL0+1;
				/*低脉冲宽度在HRCAP的可捕获周期之外，捕获失败，执行空语句*/
				if((PULSELOW>((Uint32)(EPwm2Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ+NUM_HCCAPCLKS_PER_HCSYSCLK+1))||(PULSELOW<((Uint32)(EPwm2Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ-NUM_HCCAPCLKS_PER_HCSYSCLK-1)))
				{
					__asm(" NOP");
				}

				/*高低脉冲宽度在HRCAP捕获周期内时，EPWM2执行周期从1000-4000TBCLK间变化的捕获指令，输出的PWM波周期在1000-4000之间先逐渐增大，然后又逐渐减小*/
				if (EPwm2TimerDirection == EPWM_TIMER_UP)
		        {
					/*判断PWM周期是否小于最大值，若小于，则增大到最大值，此时D402(GPIO43)翻转闪灯*/
					if(EPwm2Regs.TBPRD < PWM2_TIMER_MAX)
		            {
		                EPwm2Regs.TBPRD+=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO43=1;
		                DELAY_US(1000000);
		            }
		            else
		            {
		            	/*PWM周期大于等于最大值时，减小至最小值，此时蜂鸣器(GPIO6)间断响*/
		            	EPwm2TimerDirection = EPWM_TIMER_DOWN;
		                EPwm2Regs.TBPRD-=500;
		                GpioDataRegs.GPATOGGLE.bit.GPIO6=1;
		                DELAY_US(1000000);
		            }
		        }
		        else
		        {
		            if(EPwm2Regs.TBPRD > period)
		            {
		                EPwm2Regs.TBPRD-=500;
		                GpioDataRegs.GPATOGGLE.bit.GPIO6=1;
		                DELAY_US(1000000);
		            }
		            else
		            {
		                EPwm2TimerDirection = EPWM_TIMER_UP;
		                EPwm2Regs.TBPRD+=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO43=1;
		                DELAY_US(1000000);
		            }
		        }
		    }
	/*清除HRCAP的所有HCIFR中断置位*/
	HRCap2Regs.HCICLR.all=0x001F;
	/*使能下降沿捕获中断*/
	HRCap2Regs.HCCTL.bit.FALLINTE=1;
	/*清除HRCAP的全局中断标志*/
	HRCap2Regs.HCICLR.bit.INT=1;
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP4;
	EDIS;
}
