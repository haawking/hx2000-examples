#ifndef HRCAP_H_
#define HRCAP_H_

#include "dsc_config.h"

void InitLED(void);

void InitHRCapGpio(void);
void HRCAP1_Config(void);
void HRCAP2_Config(void);


void INTERRUPT hrcap1_isr(void);
void INTERRUPT hrcap2_isr(void);

void ePWM1_Config(Uint16 period);
void ePWM2_Config(Uint16 period);
extern Uint16 period;

extern Uint16 EPwm1TimerDirection;
extern Uint16 EPwm2TimerDirection;

//defines
#define HCCAPCLK_FREQ 240
#define HRCAP_SYSCLK_FREQ 120

#define NUM_HCCAPCLKS_PER_HCSYSCLK HCCAPCLK_FREQ/HRCAP_SYSCLK_FREQ
#define SYSCLK_FREQ 120

extern Uint16 first;
extern Uint16 PULSELOW;
extern Uint16 PULSEHIGH;


#endif /* HRCAP_H_ */
