/*
 * system.h
 *
 *  Created on: 2022��4��26��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"

void Initlin_scia_gpio(void);

void INTERRUPT Lina_Level0_ISR(void);
void INTERRUPT Lina_Level1_ISR(void);

void SCI_RXD_isr(void);
void SCI_TXD_isr(void);
void PackTxBuffers(void);
void UnpackRxBuffers(void);
void CheckData(void);
void error(void);

void lin_scia_init(void);

void resetLinSci(void);
void setRxConfig(void);

void InitLED(void);

extern Uint32 LinL0IntVect;
extern Uint32 LinL1IntVect;

extern Uint32 delay;
#endif /* SYSTEM_H_ */
