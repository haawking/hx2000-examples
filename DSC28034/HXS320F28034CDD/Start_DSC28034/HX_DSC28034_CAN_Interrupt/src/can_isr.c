#include "can.h"

Uint32 can_cnt;

INTERRUPT void eCanRxIsr(void)
{
	can_cnt++;

	receive_data=CAN_GetWordData();
	CAN_Tx();//发送数据

    ECanaRegs.CANGIF0.all = 0xffffffff;                      // 清除所有中断标志

    PieCtrlRegs.PIEACK.bit.ACK9 = 1;                        // Issue PIE ACK
}
