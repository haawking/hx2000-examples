#include "f_dspcan.h"

Uint16 CAN_GetWordData()
{
    Uint16 wordData;
    Uint16 byteData;

     while(ECanaRegs.CANRMP.all == 0){ }//等待信箱RMPn的挂起，挂起后可读取发送的数据
    wordData = (Uint16)ECanaMboxes.MBOX1.MDL.byte.BYTE0;
    byteData = (Uint16)ECanaMboxes.MBOX1.MDL.byte.BYTE1;

    wordData |= (byteData << 8);//读取发送的数据

    ECanaRegs.CANRMP.all = 0xFFFFFFFF;//清除RMPn信箱的挂起状态

    return wordData;
}

void CAN_trans(void)
{
	uint16 receive_data;
	receive_data = CAN_GetWordData();//发送数据

	ECanaMboxes.MBOX0.MDL.byte.BYTE0 = (receive_data&0xff);//读取发送数据
	ECanaMboxes.MBOX0.MDL.byte.BYTE1 = ((receive_data>>8)&0xff);
	ECanaRegs.CANTRS.bit.TRS0 = 1;//配置MBOX0发送信箱
    while(ECanaRegs.CANTA.bit.TA0 != 1)//等待MBOX0信箱的发送成功状态
	{}
    ECanaRegs.CANTA.bit.TA0 = 1;//置位MBOX0发送标志，发送成功
}
