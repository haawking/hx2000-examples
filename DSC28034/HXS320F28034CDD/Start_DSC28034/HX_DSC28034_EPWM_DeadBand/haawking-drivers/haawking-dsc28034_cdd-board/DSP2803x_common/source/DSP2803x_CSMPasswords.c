//###########################################################################
//
// FILE:	DSP2803x_CSMPasswords.c
//
// TITLE:	DSP2803x Code Security Module Passwords.
// 
// DESCRIPTION:
//
//         This file is used to specify password values to
//         program into the CSM password locations in Flash
//         at 0x73FFF0 - 73FFFF.
//
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-02-03 01:12:06 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "DSP2803x_Device.h"     //  Headerfile Include File
/**********************************************
password file
*****************************************************************/

volatile struct CSM_PWL  CODE_SECTION(".CsmPwl") CsmPwl = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};


void init_call(void)
{
	CsmPwl.PSWD0;
	CsmPwl.PSWD1;
	CsmPwl.PSWD2;
	CsmPwl.PSWD3;
}




