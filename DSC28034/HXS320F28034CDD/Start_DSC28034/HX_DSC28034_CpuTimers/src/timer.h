#ifndef TIMER_H_
#define TIMER_H_

#include "DSP2803x_Device.h"
#include "DSP2803x_Examples.h"

extern Uint32 msCounter;

void  INTERRUPT timer0_ISR(void);

void Timer0_init(void);




#endif /* TIMER_H_ */
