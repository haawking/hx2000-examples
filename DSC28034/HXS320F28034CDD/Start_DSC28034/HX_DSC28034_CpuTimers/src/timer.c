#include "timer.h"

Uint32 msCounter;

/******************************************************************
函数名：void  Timer0_init(void)
参	数：无
返回值：无
作	用：120MHz的系统时钟，timer0初始化，定时器的中断时间等于120*1000/SYSCLK
******************************************************************/

void Timer0_init(void)
{
	/*中断配置步骤-----1,开启模块中断使能，位于 Timer->RegsAddr->TCR.bit.TIE = 1;*/
	ConfigCpuTimer(&CpuTimer0, 120, 500000);/*120MHz CPU Freq, uSeconds)*/
	/*CpuTimer0Regs.TCR.all = 0x4001;*/		   /* Use write-only instruction to set TSS bit = 0*/
	CpuTimer0Regs.TCR.bit.TSS = 0;        /* To start or restart the CPU-timer, set TSS to 0*/
	CpuTimer0Regs.TCR.bit.TIE = 1;       /*中断使能*/
	/*中断配置步骤-----2，重映射中断服务函数*/
	/* Interrupts that are used in this example are re-mapped to ISR functions found within this file*/
	EALLOW;
	PieVectTable.TINT0 = &timer0_ISR;
	EDIS;
	/*中断配置步骤-----3，连接CPU中断Y*/
	IER_ENABLE(M_INT1);      /*enable interrupt reg*/
	/*IER |= M_INT1;*/
	/*中断配置步骤-----4，连接Y中断里的第几位*/
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
}

/******************************************************************
函数名：void  INTERRUPT TINT0_ISR(void)
参	数：无
返回值：无
作	用：定时器中断，每进一次中断，msCounter加1
******************************************************************/

void  INTERRUPT timer0_ISR(void)
{
 /*  timer0Base.Mark_Para.Status_Bits.OnemsdFlag = 1;*/
 /*  timer0Base.msCounter++;*/

	msCounter++;
   /* Acknowledge this interrupt to receive more interrupts from group 1*/
	EALLOW;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
	EDIS;

}




