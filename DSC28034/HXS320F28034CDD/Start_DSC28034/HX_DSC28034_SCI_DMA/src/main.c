/******************************************************************
 文 档 名：       HX_DSC28034_SCI_DMA
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_DSC28034_V1.5
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 作     用：      通过配置SCI模块寄存器，使能串口DMA接收传送到指定位置
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块DMA模式接收功能

 连接方式：通过CH340串口线连接电脑端的USB口

 现象：电脑端使用串口调试助手，按照SCIA配置中的参数设置波特率、数据位、校验
 位、和停止位。电脑一次发送10个字符1234567890，DSP收到后会通过DMA传输到"DstData"，
 然后重新读取目的地的数据，通过SCI 输出

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "system.h"

uint32_t DstData[15] CODE_SECTION("L1");


volatile Uint32 *DMADest;
volatile Uint32 *DMASource;

bool_t sci_dma_rxFlag = false;

INTERRUPT void dma_isr(void);

int main(void)
{
	uint8_t i = 0;

	InitSysCtrl();

	InitSciaGpio();

	InitPieCtrl();

	InitPieVectTable();

	IER = 0x0000;
	IFR = 0x0000;

	EALLOW;
	PieVectTable.DINTCH1 = &dma_isr;
	EDIS;

	memset(DstData, 0, 10);

	 DMAInitialize();

	DMASource = (Uint32) (&(SciaRegs.SCIRXBUF));
	DMADest = (Uint32) (&DstData[0]);

    /*DMA通道CH1突发配置(内环数据量)*/
    //长度1个16位,
    //源地址增大2个字,目标地址增大2个字
    DMACH1BurstConfig(0,0,2);
    /*DMA通道CH1传输配置(外环数据量)*/
    //长度1个16位,
    //源地址增大2个字,目标地址增大2个字
    DMACH1TransferConfig(9,0,2);
    /*DMA通道CH1地址返回配置*/
    //源与目的地址传输65536次数完成，发生地址返回，归零时字个数不变
    DMACH1WrapConfig(0xFFFF,0,0xFFFF,0);
    /*DMA通道CH1模式与功能配置*/
    //DMA中断触发源采用SCI_RX接收,单次触发模式,传输计数为零时停止计数
    //屏蔽中断同步信号,屏蔽源/目的同步影响,溢出中断屏蔽
    //通道传输字的宽度为32位,传输结束时产生中断,通道1中断使能
    DMACH1ModeConfig(DMA_SCIA_RX,PERINT_ENABLE,ONESHOT_DISABLE,CONT_ENABLE,
                     SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,THIRTYTWO_BIT,
                     CHINT_END,CHINT_ENABLE);
	/*SCI初始化配置*/
	Scia_Config(9600);


	IER |= M_INT7;
	PieCtrlRegs.PIEIER7.bit.INTx1 = 1;
	EINT;
    /*DMA通道CH1启动*/
    StartDMACH1();


	for (;;)
	{
		if (sci_dma_rxFlag == true)
		{
			sci_dma_rxFlag = false;
			Scia_Print("Receive: ");
			for (i = 0; i < 10; i++)
			{
				while (SciaRegs.SCICTL2.bit.TXRDY == 0)
				{
				}
				SciaRegs.SCITXBUF = DstData[i];
			}
			Scia_Print("\r\n");
		}
	}

}

void CODE_SECTION("ramfuncs") INTERRUPT dma_isr()
{

		/*配置DMA数据迁移方向的源与目的*/
		 DMACH1AddrConfig(DMADest,DMASource);
	     /*DMA通道CH1启动*/
	     StartDMACH1();
		 sci_dma_rxFlag = true;


	PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
}

// ----------------------------------------------------------------------------
