#include "epwm.h"

void epwm1_pc(void)
{
	/*One shot脉冲宽度为2 x SYSCLKOUT / 8 wide*/
	EPwm1Regs.PCCTL.bit.OSHTWTH=0x1;
	/*斩波时钟占空比100%*/
	EPwm1Regs.PCCTL.bit.CHPDUTY=0x7;
	/*斩波时钟频率*/
	EPwm1Regs.PCCTL.bit.CHPFREQ=0x0;
	/*斩波使能*/
	EPwm1Regs.PCCTL.bit.CHPEN=1;
}

void epwm2_pc(void)
{
	/*One shot脉冲宽度为2 x SYSCLKOUT / 8 wide*/
	EPwm2Regs.PCCTL.bit.OSHTWTH=0x1;
	/*斩波时钟占空比62.5％*/
	EPwm2Regs.PCCTL.bit.CHPDUTY=0x4;
	/*斩波1/8系统时钟频率三分频 5M高频信号调制*/
	EPwm2Regs.PCCTL.bit.CHPFREQ=0x2;
	/*斩波使能*/
	EPwm2Regs.PCCTL.bit.CHPEN=1;
}
