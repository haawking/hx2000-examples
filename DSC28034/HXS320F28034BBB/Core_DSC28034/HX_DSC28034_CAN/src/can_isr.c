#include "f_dspcan.h"

INTERRUPT void eCanRxIsr(void)
{
    Uint16 mbox;
    Uint32 *pi;
    DspCanDataStru *data;

    mbox = ECanaRegs.CANGIF0.bit.MIV0;                      // 获取中断邮箱号

    pi = (Uint32 *)(&ECANMBOXES.MBOX0.MSGID);
    data->msgid = pi[mbox<<2];							    //  读ID，读数据
    data->data.mdl= pi[(mbox<<2) + 2];
    data->data.mdh = pi[(mbox<<2) + 3];
    data->len= pi[(mbox<<2) + 1] & 0xf;                     // 读取接收数据长度

    ECanaRegs.CANRMP.bit.RMP1=1;//清除MBOX1接收信箱的挂起标志

    ECanaRegs.CANGIF0.all = 0xffffffff;                      // 清除所有中断标志
    ECanaRegs.CANGIF1.all = 0xffffffff;
    //CAN_Tx();//CAN发送

    PieCtrlRegs.PIEACK.bit.ACK9 = 1;                        // Issue PIE ACK
}
