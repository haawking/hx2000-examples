#include "dsc_config.h"
/******************************************************************
 *函数名：InitComp1(void)
 *参 数：无
 *返回值：无
 *作 用：比较器初始化
 ******************************************************************/

void InitComp1(void)
{
	EALLOW;
	/*比较器2被时钟使能*/
	SysCtrlRegs.PCLKCR3.bit.COMP1ENCLK = 1;
	/*ADC模块被时钟使能*/
	SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;

	/*比较器/DAC逻辑已上电*/
	Comp1Regs.COMPCTL.bit.COMPDACEN = 1;
	/*	传递比较器的同步值*/
	Comp1Regs.COMPCTL.bit.QUALSEL = 0;
	/*传递了比较器输出的异步版本*/
	Comp1Regs.COMPCTL.bit.SYNCSEL = 0;
	/*比较器的反相输入连接到内部DAC*/
	Comp1Regs.COMPCTL.bit.COMPSOURCE = 0;
	/*DAC位值设置为256，电压阈值为256/1024*3.3V=0.825V*/
	Comp1Regs.DACVAL.bit.DACVAL = 256;
	/*DACVAL控制DAC*/
	Comp1Regs.DACCTL.bit.DACSOURCE = 0;

	Comp1Regs.COMPCTL.bit.CMPINV=1;
	EDIS;
}
