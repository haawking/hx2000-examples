#include "system.h"
extern Uint32 LinL0IntVect;
extern Uint32 LinL1IntVect;
extern Uint16 sdataA[];
extern Uint16 rdataA[];
extern Uint16 i;

void INTERRUPT Lina_Level0_ISR(void)
{
	LinL0IntVect=LinaRegs.SCIINTVECT0.all;//映射到INT0中断的偏移量
	/*接收中断*/
	if(LinL0IntVect==11)
	{
		SCI_RXD_isr();


//		UnpackRxBuffers();
//		CheckData();
//		PackTxBuffers();

	}
	else
	{
//		error();
	}

	PieCtrlRegs.PIEACK.bit.ACK9=1;  /*PIEACK中断应答：锁定第9组中断向量*/
}


void INTERRUPT Lina_Level1_ISR(void)
{
    LinL1IntVect=LinaRegs.SCIINTVECT1.all;//映射到INT1中断的偏移量
	/*发送中断*/
	if(LinL1IntVect==12)
	{
		SCI_TXD_isr();
//		PackTxBuffers();
	}

	PieCtrlRegs.PIEACK.bit.ACK9=1; 	/*PIEACK中断应答：锁定第9组中断向量*/
}


void SCI_RXD_isr(void)
{
	Uint16 tmp;

	tmp=LinaRegs.SCIRD;//从RS485串口线上接收数据

	LinaRegs.SCITD=LinaRegs.SCIRD;//接收数据传输到发送缓冲区，进行发送
}


void SCI_TXD_isr(void)
{
	GpioDataRegs.GPBSET.bit.GPIO44=1;
	DELAY_US(5000);

	LinaRegs.SCITD=LinaRegs.SCIRD;

	GpioDataRegs.GPBCLEAR.bit.GPIO44=1;
	DELAY_US(5000);
}



void PackTxBuffers(void)
{
	LinaRegs.LINTD0.bit.TD0 = sdataA[0] >> 8;
	LinaRegs.LINTD0.bit.TD1 = sdataA[0] & 0x00FF;
	LinaRegs.LINTD0.bit.TD2 = sdataA[1] >> 8;
	LinaRegs.LINTD0.bit.TD3 = sdataA[1] & 0x00FF;
	LinaRegs.LINTD1.bit.TD4 = sdataA[2] >> 8;
	LinaRegs.LINTD1.bit.TD5 = sdataA[2] & 0x00FF;
	LinaRegs.LINTD1.bit.TD6 = sdataA[3] >> 8;
	LinaRegs.LINTD1.bit.TD7 = sdataA[3] & 0x00FF;
}

void UnpackRxBuffers(void)
{
	Uint32 ReadData;
	ReadData = LinaRegs.LINRD0.all;
	rdataA[0] = (ReadData & 0xFFFF0000) >> 16;
	rdataA[1] = ReadData & 0x0000FFFF;
	ReadData = LinaRegs.LINRD1.all;
	rdataA[2] = (ReadData & 0xFFFF0000) >> 16;
	rdataA[3] = ReadData & 0x0000FFFF;

}

void CheckData(void)
{
	for(i=0 ; i<4 ; i++)
	{
		if(sdataA[i] != rdataA[i])
		{
//			error();
		}
		sdataA[i] += 4;

	}
}

//void error(void)
//{
//	_asm("   ESTOP0");
//	for(;;);
//}
