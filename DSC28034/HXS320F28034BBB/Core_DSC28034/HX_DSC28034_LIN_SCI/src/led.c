#include "system.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO41=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO41=1;
	GpioCtrlRegs.GPBMUX1.bit.GPIO43=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43=1;
	EDIS;
}
