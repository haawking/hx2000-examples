/*
 * LIN.h
 */

#ifndef LIN_H_
#define LIN_H_

#include "dsc_config.h"

void LIN_LIN_init(void);

void INTERRUPT LIN1_isr(void);

#define LIN_ID		0x23
#define LINIDNUM_Slave 0x23

extern Uint16 tx_flag;

void LIN_transmit(void);
Uint16 LIN_trans(void);

void LIN_interrupt_init(void);

void LIN_receive(void);

#endif /* LIN_H_ */
