/******************************************************************
 文 档 名：     flash.h
 D S P：       DSC28034
 使 用 库：
 作     用：
 说     明：      提供flash接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年3月3日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#ifndef FLASH_H_
#define FLASH_H_

#include "dsc_config.h"

/*---------------------------------------------------------------------------
 API Status Structure

 This structure is used to pass debug data back to the calling routine.
 Note that the Erase API function has 3 parts: precondition, erase and
 and compaction. Erase and compaction failures will not populate
 the expected and actual data fields.
 ---------------------------------------------------------------------------*/

typedef struct
{
		Uint32 FirstFailAddr;
		Uint16 ExpectedData;
		Uint16 ActualData;
} FLASH_ST;

typedef struct
{
		Uint32 *StartAddr;
		Uint32 *EndAddr;
} SECTOR_ST;

Uint16 Flash28034_Program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus);
Uint16 Flash28034_ProgVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28034_Verify(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28034_EraseVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28034_Erase(Uint32 SectorIdx, FLASH_ST *FEraseStat);
Uint16 Flash28034_MassErase(FLASH_ST *FEraseStat);

void boot_interface_write_flash(uint32_t *address, uint8_t *buf, uint32_t length);

#define FAIL          1
#define SUCCESS       0

//Flash Config Words
#define FPROWAIT_120M 2521
#define FNVSHWAIT_120M 631
#define FPGSWAIT_120M 1261
#define FMERWAIT_120M 2520001
#define FNVH1WAIT_120M 12001
#define FPERWAIT_120M 2520001
#define FBANKWAIT_120M 4

#define FPROWAIT_96M 2101
#define FNVSHWAIT_96M 526
#define FPGSWAIT_96M 1051
#define FMERWAIT_96M 2100001
#define FNVH1WAIT_96M 10501
#define FPERWAIT_96M 2100001
#define FBANKWAIT_96M 4

extern Uint16 chip_erase();
extern Uint16 erase(int sector_idx);
extern Uint16 flash_program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus);
extern Uint16 otp_program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus);
extern SECTOR_ST Sector[128];
extern SECTOR_ST OTP_Sector[4];
/*---------------------------------------------------------------------------
 API Status Messages

 The following status values are returned from the API to the calling
 program.  These can be used to determine if the API function passed
 or failed.
 ---------------------------------------------------------------------------*/

// The CSM is preventing the function from performing its operation
#define STATUS_FAIL_CSM_LOCKED               10

// Device REVID does not match that required by the API
#define STATUS_FAIL_REVID_INVALID            11

// Invalid address passed to the API
#define STATUS_FAIL_ADDR_INVALID             12

// Incorrect PARTID
// For example the F2806 API was used on a F2808 device.
#define STATUS_FAIL_INCORRECT_PARTID         13

// API/Silicon missmatch.  An old version of the
// API is being used on silicon it is not valid for
// Please update to the latest API.
#define STATUS_FAIL_API_SILICON_MISMATCH     14

// ---- Erase Specific errors ----
#define STATUS_FAIL_NO_SECTOR_SPECIFIED      20
#define STATUS_FAIL_PRECONDITION             21
#define STATUS_FAIL_ERASE                    22
#define STATUS_FAIL_COMPACT                  23
#define STATUS_FAIL_PRECOMPACT               24

// ---- Program Specific errors ----
#define STATUS_FAIL_PROGRAM                  30
#define STATUS_FAIL_ZERO_BIT_ERROR           31

// ---- Verify Specific errors ----
#define STATUS_FAIL_VERIFY                   40

// Busy is set by each API function before it determines
// a pass or fail condition for that operation.
// The calling function will will not receive this
// status condition back from the API
#define STATUS_BUSY                999

#endif /* FLASH_H_ */
