/******************************************************************
 文 档 名：      HX_DSC28034_Flash
 开 发 环 境：  Haawking IDE V2.0.0
 开 发 板：      Core_DSC28034_V1.3
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 作     用：    调用Flash API实现对片内的flash的读写擦除等操作
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz

 连接方式：

 现象：       读写正确的时候，D400/LED1亮。
                   否则，D402闪

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "flash.h"

void GPIO_init();

#define LEN 16
Uint32 Buffer[LEN] = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16};
Uint32 verify;

int main(void)
{
	int temp;
	volatile unsigned int i;
	uint16 status;
	FLASH_ST FlashProgStatus;

	InitSysCtrl();

	GPIO_init();

	/*擦除第126扇区 */
	status = erase(126);
	/* 写Buffer[LEN]到第126扇区, 从 0X73F000地址 */
	status = flash_program(Sector[126].StartAddr, Buffer, LEN, &FlashProgStatus);
	temp = 0;
	for(i = 0; i < LEN; i++)
	{
		verify = (*(Uint32*)(Sector[126].StartAddr + i));
		/* 读第126扇区,核对与写入数据是否一致 */
		if(verify != Buffer[i])
			temp++;
	}

	while(1)
	{
		if(temp == 0)
		{
			GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
			GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
		}
		else
		{
			GpioDataRegs.GPBDAT.bit.GPIO43 = 0;
			GpioDataRegs.GPBDAT.bit.GPIO44=0;
			for(i = 0; i < 120000; i++);
			GpioDataRegs.GPBDAT.bit.GPIO43 = 1;
			GpioDataRegs.GPBDAT.bit.GPIO44 = 1;
			for(i = 0; i < 120000; i++);

		}
	}

	return 0;
}

void GPIO_init()
{
	EALLOW;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;
	GpioDataRegs.GPBSET.bit.GPIO44 = 0;

	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioDataRegs.GPBDAT.bit.GPIO43 = 1;

	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;
	EDIS;
}

// ----------------------------------------------------------------------------
