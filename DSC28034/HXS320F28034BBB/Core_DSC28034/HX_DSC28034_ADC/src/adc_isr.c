#include "epwm.h"
#include "adc.h"

Uint32 adcVal[16];
/******************************************************************
 *函数名：void INTERRUPT adc_isr()
 *参 数：无
 *返回值：无
 *作 用：adc中断服务函数
 ******************************************************************/
void INTERRUPT adc_isr()
{
	while (AdcRegs.ADCCTL1.bit.ADCBSY == 1)
	{
	}

	adcVal[0] = AdcResult.ADCRESULT0;
	adcVal[1] = AdcResult.ADCRESULT1;
	adcVal[2] = AdcResult.ADCRESULT2;
	adcVal[3] = AdcResult.ADCRESULT3;
	adcVal[4] = AdcResult.ADCRESULT4;
	adcVal[5] = AdcResult.ADCRESULT5;
	adcVal[6] = AdcResult.ADCRESULT6;
	adcVal[7] = AdcResult.ADCRESULT7;
	adcVal[8] = AdcResult.ADCRESULT8;
	adcVal[9] = AdcResult.ADCRESULT9;
	adcVal[10] = AdcResult.ADCRESULT10;
	adcVal[11] = AdcResult.ADCRESULT11;
	adcVal[12] = AdcResult.ADCRESULT12;
	adcVal[13] = AdcResult.ADCRESULT13;
	adcVal[14] = AdcResult.ADCRESULT14;
	adcVal[15] = AdcResult.ADCRESULT15;

	EALLOW;
	/*清除ADCINTFLG寄存器中各自的标志位*/
	AdcRegs.ADCINTFLGCLR.bit.ADCINT3 = 0x1;
	/* 清除ADCINTOVF寄存器中相应的溢出位*/
	AdcRegs.ADCINTOVFCLR.bit.ADCINT3 = 0x1;
	/* 中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
	EDIS;

}

