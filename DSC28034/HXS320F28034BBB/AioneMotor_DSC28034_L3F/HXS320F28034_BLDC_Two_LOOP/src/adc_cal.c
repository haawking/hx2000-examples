#include "epwm.h"

Uint32 adcdata[4];

_iq Ua,Ub,Uc;
_iq ia,ib,ic;
_iq i_error;

_iq i_ref;

_iq current_kp=3,current_ki=2;

_iq i_p=0,i_ei=0,i_out=0;

_iq compare_i=0;

_iq i;

void adc_loop(void)
{
  	/*相电流检测*/
	current_phase();
  	/*电流偏差计算*/
	current_error();
  	/*电流闭环*/
	current_loop();
}

void current_phase(void)
{
	/*相电流零点校准，令驱动电上电，电机使能前一时刻的电流为0*/
	adcdata[3]=adcVal[4]+current_offset;
	/*根据运放电路求解BLDC的实际电流值，变比根据驱动一体板电路确定*/
	i=_IQ8mpy(_IQ8div(adcdata[3]-2048,2048),_IQ8(1.65)*_IQ8(16.5/1000));
}

void current_error(void)
{
	/*以转速环输出作为电流环输入的参考值*/
	i_ref=speed_out;
	/*电流环偏差计算*/
	i_error=i_ref-i;
}

void current_loop(void)
{
	/*电流环PI调节器运算，与转速环算法一致*/
	i_p=_IQ8mpy(current_kp,i_error);
	i_ei=i_error;
	i_out=_IQ8mpy(current_ki,i_ei)+i_p;
}

void current_output(void)
{
	/*根据输出的电流值所占额定电流值的百分比，求解调制脉宽信号*/
	compare_i=i_out/i_rated;
}
