#include "epwm.h"

void InitKEY(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO40=0;//RUN
	GpioCtrlRegs.GPBDIR.bit.GPIO40=0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO41=0;//STOP
	GpioCtrlRegs.GPBDIR.bit.GPIO41=0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO15=0;//UP
	GpioCtrlRegs.GPADIR.bit.GPIO15=0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO14=0;//DOWN
	GpioCtrlRegs.GPADIR.bit.GPIO14=0;
	EDIS;
}
