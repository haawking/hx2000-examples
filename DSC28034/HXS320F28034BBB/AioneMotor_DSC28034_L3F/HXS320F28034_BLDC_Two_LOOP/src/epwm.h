/*
 * epwm.h
 *
 *  Created on: 2021��12��6��
 *      Author: HX
 */

#ifndef EPWM_H_
#define EPWM_H_

#include "dsc_config.h"
#include "IQmathLib.h"

void InitEPwm1Example(void);
void InitEPwm2Example(void);
void InitEPwm3Example(void);

void INTERRUPT epmw1_isr(void);

void epwm_compare(void);

void thei(void);
void theta_config(void);

/*EQep*/
void speed_cal(void);

void speed_loop(void);
void speed_output(void);

extern long Speed_ref;
extern _iq speed_out;

/*ADC*/
void ADC_Init(void);
void INTERRUPT adc_isr();

#define ADC_OFFSET 1178
#define ADC_Gain 12

#define current_offset 320;

#define Rs 52

extern Uint32 adcVal[6];

void adc_loop(void);

void current_phase(void);
void current_error(void);
void current_loop(void);
void current_output(void);

#define i_rated 8

extern _iq compare_i;

extern int Hall;

extern int Hall_old;
extern int Hall_new;

extern int qep[3];

extern int Hall;

void InitKEY(void);

void InitHC(void);

#endif /* EPWM_H_ */
