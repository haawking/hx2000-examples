/*function1:EPWM1/2/3的输出占空比变化配置的中断服务程序
 *function1: 配置pwm波占空比实现转速追踪，采用六步换相法切换相序导通顺序
 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : epwm_int.c
 *****************************************************************/

#include "epwm.h"

int EPwmIntCount=0;

int qep[3];

int Hall;

int Hall_old,Hall_new;

void qep_cal(void)
{
 	/*霍尔信号UVW检测*/
	qep[0]=GpioDataRegs.GPBDAT.bit.GPIO44;//HALLA
	qep[1]=GpioDataRegs.GPBDAT.bit.GPIO33;//HALLB
	qep[2]=GpioDataRegs.GPBDAT.bit.GPIO32;//HALLC

	Hall=qep[2]*4+qep[1]*2+qep[0]*1;
 	/*读取更新值*/
	Hall_new=Hall;
}

void INTERRUPT epmw1_isr(void)
{
 	/*霍尔信号UVW检测*/
	qep_cal();

	if(GpioDataRegs.GPBDAT.bit.GPIO40==0)//RUN
	{
	 	/*电机启动*/
		EPwmIntCount++;
	}

	if(EPwmIntCount>0)
	{
	 	/*转速检测计算*/
		speed_cal();

    	/*电机调速*/
    	theta_config();

      	/*转速闭环*/
		speed_loop();

      	/*电流闭环*/
      	adc_loop();

		/*占空比输出，转速与电流追踪*/
		epwm_compare();
	}

 	/*检测到停止动作后,封波使电机停止*/
	if(GpioDataRegs.GPBDAT.bit.GPIO41==0)//STOP
	{
		EPwm1Regs.CMPA.half.CMPA=0;
		EPwm2Regs.CMPA.half.CMPA=0;
		EPwm3Regs.CMPA.half.CMPA=0;
		EPwmIntCount=0;
	}

	/*清除事件中断的INT全局中断*/
	EPwm1Regs.ETCLR.bit.INT=1;

	/*中断应答，锁定IER的第3组中断向量*/
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}
