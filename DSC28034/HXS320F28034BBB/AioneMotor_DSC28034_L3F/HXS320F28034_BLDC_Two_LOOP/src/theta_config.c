/*function1: Gpio40/41启动与停止
 *function2: Gpio15按下，电机加速，按下一次加速500rpm(Speed_ref+500)
 *function3: Gpio14按下，电机减速，按下一次减速500rpm(Speed_ref-500)
 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : main.c
 * FILE   : main.c
 *****************************************************************/
#include "epwm.h"

void theta_config(void)
{
	if(GpioDataRegs.GPADAT.bit.GPIO15==0)//UP
	{
		Speed_ref+=500;
	}
	else
	{

	}
	if(GpioDataRegs.GPADAT.bit.GPIO14==0)//DOWN
	{
		Speed_ref-=500;
	}
	else
	{

	}
}
