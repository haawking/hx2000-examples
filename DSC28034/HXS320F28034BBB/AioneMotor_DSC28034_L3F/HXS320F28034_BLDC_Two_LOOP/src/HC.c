#include "epwm.h"

void InitHC(void)
{
	GpioCtrlRegs.GPBMUX1.bit.GPIO44=0;//HALLA
	GpioCtrlRegs.GPBDIR.bit.GPIO44=0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO33=0;//HALLB
	GpioCtrlRegs.GPBDIR.bit.GPIO33=0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO32=0;//HALLC
	GpioCtrlRegs.GPBDIR.bit.GPIO32=0;
}
