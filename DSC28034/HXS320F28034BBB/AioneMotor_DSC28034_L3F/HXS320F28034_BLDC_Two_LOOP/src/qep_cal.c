#include "epwm.h"
#include "IQmathLib.h"

Uint32 Speed_cnt=0;

Uint32 HallTime=0;

_iq Speed=0;
/*Speed loop*/
long Speed_ref=800;
_iq speed_e=0,speed_ei=0;
_iq speed_kp=3,speed_ki=2;
_iq speed_p=0,speed_out=0;

void speed_cal(void)
{
	/*定义脉冲计数器Speed_cnt:求解两个换相点间的时间*/
	Speed_cnt++;
	/*判断该时刻是否换相*/
	if(Hall!=Hall_old)
	{
		/*换相时，记录两个换相点间计数*/
		HallTime=Speed_cnt;
		/*换相时，计数清零，使记录的计数值为两个换相点间的变化值*/
		Speed_cnt=0;
		/*换相时，以该时刻的霍尔位置信息作为当前时刻霍尔位置值*/
		Hall_old=Hall;
	}

	/*通过计算换相时间，求解转速speed=60/p/(Speed_cnt*6*PWM中断周期)*/
	Speed=50000/HallTime;
}

void speed_loop(void)
{
	/*将转速设定值与反馈值相比较*/
	speed_e=Speed_ref-Speed;
	/*PI调节器运算*/
	speed_p=_IQ8mpy(speed_kp,speed_e);
	speed_ei=speed_e;
	speed_out=_IQ8mpy(speed_ki,speed_ei)+speed_p;
}
