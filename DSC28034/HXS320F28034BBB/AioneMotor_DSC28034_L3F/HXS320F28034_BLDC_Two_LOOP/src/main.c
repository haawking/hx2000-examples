/******************************************************************
文 档 名 ：HX_DSC28034_BLDC_Two_LOOP
开 发 环 境：Haawking IDE V2.1.3
开 发 板 ：HX28034驱控一体板
D S P： DSC28034
使 用 库：无
功能1:PWM20KHz
 说明: 三路占空比转速追踪的pwm波20KHz，TBCTR采用向上向下计数
 功能2: GPIO40/41电机启停
  说明: Gpio40按下,电机RUN键启动,Gpio41按下,电机STOP键停止
 功能3: GPIO15/14电机调速
 说明: Gpio15按下,电机加速,Gpio14按下,电机减速
功能4:PWM触发ADC采样
说明: 在A3通道上，进行换相电流检测
功能5: 电机转速检测
说明: 采用霍尔信号，检测换相60度的时间差，以计算转速

----------------------例程使用说明-----------------------------*
此例程仅为客户提供无刷直流电机的六步换相，
方波双闭环调速示例程序，仅实现演示调速功能，无保护设置

1.连线：（1）+、-连接直流稳压电源：
A.上电调节24V电压，驱动电24V，分压后芯片供电5V输出，可保证足够的驱动能力；
B.1.0A电流限幅，以保证调试中的安全性，防止电流过流烧MOS管
（2）UVW三线连接电机的三相电流
（3）HA、HB、HC、+、-连接电机霍尔传感器三相接线与5V电源接线

2.启动：待开发板上电，下载完成程序后，
拨动开发板上的单刀双掷开关，给MOS管上强电，
按下RUN键，可启动，STOP键，可停止电机
3.调速：按下一次调速500rpm,UP键加速，DOWN键减速
可在theta_config.c中根据需求修改

4.适配电机：
（1）霍尔检测相序，可在epwm_int.c中qep_cal()根据实际接线与UVW真值表调整
（2）控制参数调整，
								A.转速环可在qep_cal.c中speed_loop()根据实际电机及驱动电路进行适配调整
								B.电流环可在adc_cal.c中current_loop()根据实际电机及驱动电路进行适配调整

5.原理说明，请参见"中科昊芯"官网应用板块
链接1：开环控制https://www.bilibili.com/read/cv14493034?spm_id_from=333.999.0.0
链接2：双闭环控制https://www.bilibili.com/read/cv14592962?spm_id_from=333.999.0.0

注：（1）本算法为无刷直流电机的六步换相算法，
同一时刻不存在上下桥臂EPWMxA/B共用输出，即不会导致双管直通，因此未考虑死区
（2）原始电机参数为时代超群无刷直流电机60BL100S15-230TF9,
详见image文件夹下driver_meter.jpg

 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/

#include <stdio.h>
#include "dsc_config.h"
#include "hx_rv32_dev.h"
#include "hx_rv32_type.h"
#include <syscalls.h>
#include "xcustom.h"
#include "IQmathLib.h"
#include "epwm.h"

int main(void)
{
	/*系统时钟初始化*/
	InitSysCtrl();

	/*EPWM外设引脚初始化:发波驱动电机配置*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();
	/*按键控制配置*/
	InitKEY();
	/*霍尔检测信号配置*/
	InitHC();

	/*ADC外设Aio模拟量引脚初始化配置*/
	InitAdcAio();

	/*ADC复位初始化*/
	InitAdc();

	/*清中断，关中断*/
	InitPieCtrl();

	/*压栈，入栈*/
	IER=0x0000;
	IFR=0x0000;

	/*中断向量表初始化配置*/
	InitPieVectTable();

	EALLOW;
	/*配置EPWM1_INT中断,执行电机双闭环调速*/
	PieVectTable.EPWM1_INT=&epmw1_isr;

	/*配置ADCINT3_INT中断,执行电流采样*/
	PieVectTable.ADCINT3=&adc_isr;
	EDIS;

	EALLOW;
	/*禁止EPWM的时基使能，允许EPWM初始化配置写入*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=0;
	EDIS;

	/*EPWM的初始化配置*/
	InitEPwm1Example();
	InitEPwm2Example();
	InitEPwm3Example();

	EALLOW;
	/*打开EPWM的时基使能，使EPWM的初始化配置起作用*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=1;
	EDIS;

	/*ADC模块初始化,用于电流采样通道、触发源、采样周期配置*/
	ADC_Init();

	/*使能打开IER的第3组与第10组中断向量*/
	IER|=(M_INT3|M_INT10);

	/*使能打开IER的第3组中断向量的第一个向量*/
	PieCtrlRegs.PIEIER3.bit.INTx1=1;

	/*使能打开IER的第10组中断向量的第三个向量*/
	PieCtrlRegs.PIEIER10.bit.INTx3=1;

	/*使能打开全局中断*/
	EINT;
	/*关闭全局中断*/
	ERTM;
 	/*霍尔信号UVW检测*/
    	qep[0]=GpioDataRegs.GPBDAT.bit.GPIO44;//HALLA
    	qep[1]=GpioDataRegs.GPBDAT.bit.GPIO33;//HALLB
    	qep[2]=GpioDataRegs.GPBDAT.bit.GPIO32;//HALLC

    	Hall=qep[2]*4+qep[1]*2+qep[0]*1;
     	/*存储霍尔信号UVW上一次值，与本次值，用于转速计算求差*/
    	Hall_old=Hall;
    	Hall_new=Hall;
    while(1){

    }


	return 0;
}

// ----------------------------------------------------------------------------

