//###########################################################################
//
// FILE:    DSP2803x_BootVars.h
//
// TITLE:   DSP2803x Boot Variable Definitions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.2.1 $
// $Release Date: 2023-02-03 01:09:38 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################
#ifndef DSP2803x_BOOT_VARS_H
#define DSP2803x_BOOT_VARS_H

#ifdef __cplusplus
extern "C" {
#endif



//---------------------------------------------------------------------------
// External Boot ROM variable definitions:
//


extern Uint32 Flash_CPUScaleFactor;
extern void (*Flash_CallbackPtr) (void);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  // end




