/******************************************************************
文 档 名：      HX_DSC28034_SCI_Interrupt_RS485
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Start_DSC28034_V1.0
 D S P：          DSC28034
 使 用 库：
作     用：      通过配置SCI模块寄存器，使能串口中断模式下的发送和接收
说     明：      FLASH工程
-------------------------- 例程使用说明 --------------------------
功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块的发送和接收功能


连接方式：电脑使用一个USB转RS485的收发器，RS485差分线连接到034开发板的RS485收发器排针。

现象：电脑端使用串口调试助手，按照SCIA配置中的参数设置波特率9600、数据位、校验
位、和停止位。
设备上电后，PC端发送一些数据，DSP通过中断接收到数据后，回应收到的数据

 版 本：      V1.0.0
 时 间：      2023年3月23日
 作 者：      liyuyao
 @ mail：   support@mail.haawking.com
******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

/*全局变量*/

/*函数原型*/
void Scia_Config(uint32 baud);
void Scia_Send(uint8 data);
void Scia_Print(char *str);
void RS485_Dir_Init();
void RS485_Dir_Set_Rx(void);
void RS485_Dir_Set_Tx(void);

INTERRUPT void sciaTxIsr(void);
INTERRUPT void sciaRxIsr(void);

/*主程序入口*/
int main(void)
{

	/*初始化系统控制*/
	InitSysCtrl();

	/*初始化内存控制寄存器，使能内存流水线模式*/
	InitFlash();

	/*初始化串口通信的GPIO口*/
	/*GPIO28: SCIRXDA*/
	/*GPIO29: SCITXDA*/
	InitSciGpio();

	/*RS485收发器的DE脚连接到GPIO44*/
	RS485_Dir_Init();

	/*关闭中断*/
	DINT;

	/*初始化PIE控制*/
	InitPieCtrl();

	/*关闭CPU中断，清除中断标志位*/
	IER = 0x0000;
	IFR = 0x0000;

	/*初始化PIE中断向量表*/
	InitPieVectTable();

	/*SCI寄存器配置*/
	Scia_Config(9600);

	/*使能外部中断和CPU中断*/
	EINT;

	/*通过SCI发送字符串*/
	RS485_Dir_Set_Tx();
	Scia_Print("--------- SCI works as RS485, Interrupt Mode ----------\r\n");
	Scia_Print("Baud Rate: 9600\r\n");
	Scia_Print("Data Bits: 8\r\n");
	Scia_Print("Parity:    none\r\n");
	Scia_Print("Stop Bits: 1\r\n");
	Scia_Print("\r\n");
	Scia_Print("Please enter a character, DSP will echo it back.\r\n");
	Scia_Print("\r\n");
	/*等待发送完成*/
	while(SciaRegs.SCICTL2.bit.TXEMPTY == 0)
	{
	}
	RS485_Dir_Set_Rx();

	for(;;)
	{
	}

	return 0;
}

/******************************************************************
 *函数名：Scia_Config(uint32 baud)
 *参   数： baud，串口波特率
 *返回值：无
 *作   用： SCIA 初始化配置
 ******************************************************************/
void Scia_Config(uint32 baud)
{
	uint32 div = 0;
	uint32 divsel = 0;
	uint32 lospcp = 0;
	uint32 lspclk = 0;
	uint16 brr = 0;

	//获取系统时钟的倍频、分频和低速外部时钟的值
	div = SysCtrlRegs.PLLCR.bit.DIV;
	divsel = SysCtrlRegs.PLLSTS.bit.DIVSEL;
	lospcp = SysCtrlRegs.LOSPCP.bit.LSPCLK;

	if(lospcp != 0)
	{
		lospcp = lospcp * 2;
	}
	else
	{
		lospcp = 1;
	}

	/*分频值设置
	 divsel为 0时，系统时钟4分频
	 divsel为 1时，系统时钟4分频
	 divsel为 2时，系统时钟2分频
	 divsel为 3时，系统时钟1分频*/
	switch(divsel)
	{
	case 0:
	case 1:
		lspclk = 12000000 * div / 4 / lospcp;
		break;
	case 2:
		lspclk = 12000000 * div / 2 / lospcp;
		break;
	case 3:
		lspclk = 12000000 * div / 1 / lospcp;
		break;
	}

	brr = lspclk / (baud * 8) - 1;

	/*SCI 停止位设置    0：一个停止位   1：两个停止位*/
	SciaRegs.SCICCR.bit.STOPBITS = 0;

	/*SCI 奇偶校验位    0：奇偶校验   1：偶偶校验*/
	SciaRegs.SCICCR.bit.PARITY = 0;

	/*SCI 奇偶校验使能   0：关闭   1：启用*/
	SciaRegs.SCICCR.bit.PARITYENA = 0;

	/*SCI 字符长度   0：1个字长  1：2个字长 ... 7：8个字长*/
	SciaRegs.SCICCR.bit.SCICHAR = 7;

	/*使能SCI的发送机和接收机*/
	SciaRegs.SCICTL1.bit.TXENA = 1;
	SciaRegs.SCICTL1.bit.RXENA = 1;

	/*SCI 16位波特率选择寄存器 高8位*/
	SciaRegs.SCIHBAUD = (uint8)((brr >> 8) & 0xff);
	/*SCI 16位波特率选择寄存器 低8位*/
	SciaRegs.SCILBAUD = (uint8)(brr & 0xff);

	/*SCI 发送中断使能*/
	SciaRegs.SCICTL2.bit.TXINTENA = 1;
	/*SCI 接收中断使能*/
	SciaRegs.SCICTL2.bit.RXBKINTENA = 1;

	/*SCI 指定发送和接收中断处理函数，该寄存器受EALLOW保护*/
	EALLOW;
	PieVectTable.SCIRXINTA = &sciaRxIsr;
	PieVectTable.SCITXINTA = &sciaTxIsr;
	EDIS;

	/*PIE Group 9, SCIRXINTA*/
	PieCtrlRegs.PIEIER9.bit.INTx1 = 1;
	/*PIE Group 9, SCITXINTA*/
	PieCtrlRegs.PIEIER9.bit.INTx2 = 1;
	IER_ENABLE(M_INT9);

	/*SCI 软件复位，重新启动SCI*/
	SciaRegs.SCICTL1.bit.SWRESET = 1;

}

/******************************************************************
 *函数名：Scia_Send(uint8 data)
 *参   数： data，准备发送的字节
 *返回值：无
 *作   用： SCIA 发送一个字节
 ******************************************************************/
void Scia_Send(uint8 data)
{
	while(SciaRegs.SCICTL2.bit.TXRDY == 0)
	{
	}

	SciaRegs.SCITXBUF = data;
}

/******************************************************************
 *函数名：Scia_Print(char *str)
 *参   数： *str，要发送的字符串
 *返回值：无
 *作   用： SCIA 发送一个字符串
 ******************************************************************/
void Scia_Print(char *str)
{
	while(*str != '\0')
	{
		while(SciaRegs.SCICTL2.bit.TXRDY == 0)
		{
		}

		SciaRegs.SCITXBUF = *str++;
	}
}

/******************************************************************
 *函数名：sciaTxIsr(void)
 *参   数： 无
 *返回值：无
 *作   用： SCIA中断发送处理
 ******************************************************************/
INTERRUPT void sciaTxIsr(void)
{
	/*清除中断标志位*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}

/******************************************************************
 *函数名：sciaRxIsr(void)
 *参   数： 无
 *返回值：无
 *作   用： SCIA中断接收处理
 ******************************************************************/
INTERRUPT void sciaRxIsr(void)
{
	if(SciaRegs.SCICTL2.bit.TXRDY == 1)
	{
		RS485_Dir_Set_Tx();
		Scia_Send(SciaRegs.SCIRXBUF.bit.RXDT);
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
	}
	/*等待发送完成*/
	while(SciaRegs.SCICTL2.bit.TXEMPTY == 0)
	{
	}
	RS485_Dir_Set_Rx();
}

void RS485_Dir_Init()
{
	 EALLOW;
	/*GPIO44作为DIR 信号连接到485收发器DE, 禁止上拉*/
	GpioCtrlRegs.GPBPUD.bit.GPIO44 = 1;
	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;  //输出
	EDIS;
	// 设置收发器工作在接收状态
	GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
}
void RS485_Dir_Set_Rx(void)
{
	// 设置收发器工作在接收状态
	GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
}
void RS485_Dir_Set_Tx(void)
{
	// 设置收发器工作在发送状态
	GpioDataRegs.GPBSET.bit.GPIO44 = 1;
}
// ----------------------------------------------------------------------------
