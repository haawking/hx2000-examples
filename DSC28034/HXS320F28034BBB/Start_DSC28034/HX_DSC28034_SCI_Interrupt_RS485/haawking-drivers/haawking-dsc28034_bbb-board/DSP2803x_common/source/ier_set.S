//###########################################################################
//
// FILE:    ier_set.S
//
// TITLE:   ier_set Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.2.1 $
// $Release Date: 2023-02-03 01:09:46 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 ier_set

ier_set:
 csrs 0x304,a0  //IER |= a0 
 ret


	

