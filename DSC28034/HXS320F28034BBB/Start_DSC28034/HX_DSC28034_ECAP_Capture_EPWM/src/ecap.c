#include "epwm.h"
/******************************************************************
 *函数名：void InitECapture(void)
 *参 数：无
 *返回值：无
 *作 用：初始化ECAP
 ******************************************************************/

void InitECapture(void)
{
	/*ECEINT寄存器使能*/
	ECap1Regs.ECEINT.all = 0x0000;
	/*ECCLR清零*/
	ECap1Regs.ECCLR.all = 0xFFFF;
	/*在捕获事件时禁用 CAP1-4 寄存器加载*/
	ECap1Regs.ECCTL1.bit.CAPLDEN = 0;
	/*TSCTR 停止*/
	ECap1Regs.ECCTL2.bit.TSCTRSTOP = 0;
	/*运行于单次模式*/
	ECap1Regs.ECCTL2.bit.CONT_ONESHT = 1;
	/*在单次模式下，捕获事件3之后停止*/
	ECap1Regs.ECCTL2.bit.STOP_WRAP = 3;
	/*下降沿触发的捕获事件1*/
	ECap1Regs.ECCTL1.bit.CAP1POL = 1;
	/*上升沿触发的捕获事件2*/
	ECap1Regs.ECCTL1.bit.CAP2POL = 0;
	/*下降沿触发的捕获事件3*/
	ECap1Regs.ECCTL1.bit.CAP3POL = 1;
	/*上升沿触发的捕获事件4*/
	ECap1Regs.ECCTL1.bit.CAP4POL = 0;
	/*在捕获事件1发生时复位计数器*/
	ECap1Regs.ECCTL1.bit.CTRRST1 = 1;
	/*在捕获事件2发生时复位计数器*/
	ECap1Regs.ECCTL1.bit.CTRRST2 = 1;
	/*在捕获事件3发生时复位计数器*/
	ECap1Regs.ECCTL1.bit.CTRRST3 = 1;
	/*在捕获事件4发生时复位计数器*/
	ECap1Regs.ECCTL1.bit.CTRRST4 = 1;
	/*通过 SYNCI 信号或 S/W 强制从 CTRPHS 寄存器加载使能计数器(TSCTR)*/
	ECap1Regs.ECCTL2.bit.SYNCI_EN = 1;
	/*选择同步输入事件作为同步输出信号（通 过）*/
	ECap1Regs.ECCTL2.bit.SYNCO_SEL = 0;
	/*捕获事件时使能 CAP1-4 寄存器加载*/
	ECap1Regs.ECCTL1.bit.CAPLDEN = 1;
	/*TSCTR 自由运行*/
	ECap1Regs.ECCTL2.bit.TSCTRSTOP = 1;
	/*启动单次发射序列*/
	ECap1Regs.ECCTL2.bit.REARM = 1;
	/*捕获事件时使能 CAP1-4 寄存器加载*/
	ECap1Regs.ECCTL1.bit.CAPLDEN = 1;
	/*捕获事件 4 中断使能*/
	ECap1Regs.ECEINT.bit.CEVT4 = 1;

}

