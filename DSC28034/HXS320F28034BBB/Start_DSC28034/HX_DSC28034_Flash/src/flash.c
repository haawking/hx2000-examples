/******************************************************************
 文 档 名：     flash.c
 D S P：       DSC28034
 使 用 库：
 作     用：
 说     明：      提供flash.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年3月3日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "flash.h"

SECTOR_ST OTP_Sector[4] = {{(Uint32*)0x7A0000, (Uint32*)0x7A07FF}, {(Uint32*)0x7A0800, (Uint32*)0x7A0FFF}, {(Uint32*)0x7A1000, (Uint32*)0x7A17FF}, {(Uint32*)0x7A1800, (Uint32*)0x7A1FFF}, };

/* Total 128 Sectors */
SECTOR_ST Sector[128] = {{(Uint32*)0x700000, (Uint32*)0x7007FF}, {(Uint32*)0x700800, (Uint32*)0x700FFF}, {(Uint32*)0x701000, (Uint32*)0x7017FF}, {(Uint32*)0x701800, (Uint32*)0x701FFF}, {(Uint32*)0x702000, (Uint32*)0x7027FF}, {(Uint32*)0x702800, (Uint32*)0x702FFF}, {(Uint32*)0x703000,
		(Uint32*)0x7037FF}, {(Uint32*)0x703800, (Uint32*)0x703FFF}, {(Uint32*)0x704000, (Uint32*)0x7047FF}, {(Uint32*)0x704800, (Uint32*)0x704FFF}, {(Uint32*)0x705000, (Uint32*)0x7057FF}, {(Uint32*)0x705800, (Uint32*)0x705FFF}, {(Uint32*)0x706000, (Uint32*)0x7067FF}, {(Uint32*)0x706800,
		(Uint32*)0x706FFF}, {(Uint32*)0x707000, (Uint32*)0x7077FF}, {(Uint32*)0x707800, (Uint32*)0x707FFF}, {(Uint32*)0x708000, (Uint32*)0x7087FF}, {(Uint32*)0x708800, (Uint32*)0x708FFF}, {(Uint32*)0x709000, (Uint32*)0x7097FF}, {(Uint32*)0x709800, (Uint32*)0x709FFF}, {(Uint32*)0x70A000,
		(Uint32*)0x70A7FF}, {(Uint32*)0x70A800, (Uint32*)0x70AFFF}, {(Uint32*)0x70B000, (Uint32*)0x70B7FF}, {(Uint32*)0x70B800, (Uint32*)0x70BFFF}, {(Uint32*)0x70C000, (Uint32*)0x70C7FF}, {(Uint32*)0x70C800, (Uint32*)0x70CFFF}, {(Uint32*)0x70D000, (Uint32*)0x70D7FF}, {(Uint32*)0x70D800,
		(Uint32*)0x70DFFF}, {(Uint32*)0x70E000, (Uint32*)0x70E7FF}, {(Uint32*)0x70E800, (Uint32*)0x70EFFF}, {(Uint32*)0x70F000, (Uint32*)0x70F7FF}, {(Uint32*)0x70F800, (Uint32*)0x70FFFF}, {(Uint32*)0x710000, (Uint32*)0x7107FF}, {(Uint32*)0x710800, (Uint32*)0x710FFF}, {(Uint32*)0x711000,
		(Uint32*)0x7117FF}, {(Uint32*)0x711800, (Uint32*)0x711FFF}, {(Uint32*)0x712000, (Uint32*)0x7127FF}, {(Uint32*)0x712800, (Uint32*)0x712FFF}, {(Uint32*)0x713000, (Uint32*)0x7137FF}, {(Uint32*)0x713800, (Uint32*)0x713FFF}, {(Uint32*)0x714000, (Uint32*)0x7147FF}, {(Uint32*)0x714800,
		(Uint32*)0x714FFF}, {(Uint32*)0x715000, (Uint32*)0x7157FF}, {(Uint32*)0x715800, (Uint32*)0x715FFF}, {(Uint32*)0x716000, (Uint32*)0x7167FF}, {(Uint32*)0x716800, (Uint32*)0x716FFF}, {(Uint32*)0x717000, (Uint32*)0x7177FF}, {(Uint32*)0x717800, (Uint32*)0x717FFF}, {(Uint32*)0x718000,
		(Uint32*)0x7187FF}, {(Uint32*)0x718800, (Uint32*)0x718FFF}, {(Uint32*)0x719000, (Uint32*)0x7197FF}, {(Uint32*)0x719800, (Uint32*)0x719FFF}, {(Uint32*)0x71A000, (Uint32*)0x71A7FF}, {(Uint32*)0x71A800, (Uint32*)0x71AFFF}, {(Uint32*)0x71B000, (Uint32*)0x71B7FF}, {(Uint32*)0x71B800,
		(Uint32*)0x71BFFF}, {(Uint32*)0x71C000, (Uint32*)0x71C7FF}, {(Uint32*)0x71C800, (Uint32*)0x71CFFF}, {(Uint32*)0x71D000, (Uint32*)0x71D7FF}, {(Uint32*)0x71D800, (Uint32*)0x71DFFF}, {(Uint32*)0x71E000, (Uint32*)0x71E7FF}, {(Uint32*)0x71E800, (Uint32*)0x71EFFF}, {(Uint32*)0x71F000,
		(Uint32*)0x71F7FF}, {(Uint32*)0x71F800, (Uint32*)0x71FFFF}, {(Uint32*)0x720000, (Uint32*)0x7207FF}, {(Uint32*)0x720800, (Uint32*)0x720FFF}, {(Uint32*)0x721000, (Uint32*)0x7217FF}, {(Uint32*)0x721800, (Uint32*)0x721FFF}, {(Uint32*)0x722000, (Uint32*)0x7227FF}, {(Uint32*)0x722800,
		(Uint32*)0x722FFF}, {(Uint32*)0x723000, (Uint32*)0x7237FF}, {(Uint32*)0x723800, (Uint32*)0x723FFF}, {(Uint32*)0x724000, (Uint32*)0x7247FF}, {(Uint32*)0x724800, (Uint32*)0x724FFF}, {(Uint32*)0x725000, (Uint32*)0x7257FF}, {(Uint32*)0x725800, (Uint32*)0x725FFF}, {(Uint32*)0x726000,
		(Uint32*)0x7267FF}, {(Uint32*)0x726800, (Uint32*)0x726FFF}, {(Uint32*)0x727000, (Uint32*)0x7277FF}, {(Uint32*)0x727800, (Uint32*)0x727FFF}, {(Uint32*)0x728000, (Uint32*)0x7287FF}, {(Uint32*)0x728800, (Uint32*)0x728FFF}, {(Uint32*)0x729000, (Uint32*)0x7297FF}, {(Uint32*)0x729800,
		(Uint32*)0x729FFF}, {(Uint32*)0x72A000, (Uint32*)0x72A7FF}, {(Uint32*)0x72A800, (Uint32*)0x72AFFF}, {(Uint32*)0x72B000, (Uint32*)0x72B7FF}, {(Uint32*)0x72B800, (Uint32*)0x72BFFF}, {(Uint32*)0x72C000, (Uint32*)0x72C7FF}, {(Uint32*)0x72C800, (Uint32*)0x72CFFF}, {(Uint32*)0x72D000,
		(Uint32*)0x72D7FF}, {(Uint32*)0x72D800, (Uint32*)0x72DFFF}, {(Uint32*)0x72E000, (Uint32*)0x72E7FF}, {(Uint32*)0x72E800, (Uint32*)0x72EFFF}, {(Uint32*)0x72F000, (Uint32*)0x72F7FF}, {(Uint32*)0x72F800, (Uint32*)0x72FFFF}, {(Uint32*)0x730000, (Uint32*)0x7307FF}, {(Uint32*)0x730800,
		(Uint32*)0x730FFF}, {(Uint32*)0x731000, (Uint32*)0x7317FF}, {(Uint32*)0x731800, (Uint32*)0x731FFF}, {(Uint32*)0x732000, (Uint32*)0x7327FF}, {(Uint32*)0x732800, (Uint32*)0x732FFF}, {(Uint32*)0x733000, (Uint32*)0x7337FF}, {(Uint32*)0x733800, (Uint32*)0x733FFF}, {(Uint32*)0x734000,
		(Uint32*)0x7347FF}, {(Uint32*)0x734800, (Uint32*)0x734FFF}, {(Uint32*)0x735000, (Uint32*)0x7357FF}, {(Uint32*)0x735800, (Uint32*)0x735FFF}, {(Uint32*)0x736000, (Uint32*)0x7367FF}, {(Uint32*)0x736800, (Uint32*)0x736FFF}, {(Uint32*)0x737000, (Uint32*)0x7377FF}, {(Uint32*)0x737800,
		(Uint32*)0x737FFF}, {(Uint32*)0x738000, (Uint32*)0x7387FF}, {(Uint32*)0x738800, (Uint32*)0x738FFF}, {(Uint32*)0x739000, (Uint32*)0x7397FF}, {(Uint32*)0x739800, (Uint32*)0x739FFF}, {(Uint32*)0x73A000, (Uint32*)0x73A7FF}, {(Uint32*)0x73A800, (Uint32*)0x73AFFF}, {(Uint32*)0x73B000,
		(Uint32*)0x73B7FF}, {(Uint32*)0x73B800, (Uint32*)0x73BFFF}, {(Uint32*)0x73C000, (Uint32*)0x73C7FF}, {(Uint32*)0x73C800, (Uint32*)0x73CFFF}, {(Uint32*)0x73D000, (Uint32*)0x73D7FF}, {(Uint32*)0x73D800, (Uint32*)0x73DFFF}, {(Uint32*)0x73E000, (Uint32*)0x73E7FF}, {(Uint32*)0x73E800,
		(Uint32*)0x73EFFF}, {(Uint32*)0x73F000, (Uint32*)0x73F7FF}, {(Uint32*)0x73F800, (Uint32*)0x73FFFF}, };

//
// Flash Status Structure
//
FLASH_ST FlashStatus;
FLASH_ST FlashProgStatus,FlashEraseStatus;

// Erase only 1 Sector once, sector_ide: 0-127,total 128 Sectors
/******************************************************************
 *函数名：erase
 *参   数： sector_idx, 擦写的扇区索引值
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") erase(int sector_idx)
{
	Uint16 Status;
	Status = Flash28034_Erase(sector_idx, &FlashStatus);
	if(Status != SUCCESS)
		return FAIL;
	else
		return SUCCESS;
}

/******************************************************************
 *函数名：flash_program
 *参   数： FlashAddr, Flash烧写地址
 BufAddr,   Flash烧写数据
 Length,    烧写长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") flash_program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus)
{
	Uint16 Status;
	Status = Flash28034_Program((Uint32*)FlashAddr, (Uint32*)BufAddr, Length, FProgStatus);
	if(Status != SUCCESS)
		return FAIL;
	else
		return SUCCESS;
}

/******************************************************************
 *函数名：Flash28034_Program
 *参   数： FlashAddr, Flash烧写地址
 BufAddr,   Flash烧写数据
 Length,    烧写长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_Program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus)
{
	Uint32 i;
	FLASH_ST VerifyStatus;
	FLASH_ST ProgVerifyStat;
	Uint16 Verify_Status;

	// CSM Unlock
	if(FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	// Flash has been Written
	if(Flash28034_ProgVerify(FlashAddr, Length, &ProgVerifyStat) != SUCCESS)
		return STATUS_FAIL_ZERO_BIT_ERROR;

	EALLOW;
	FlashRegs.FMEMWREN.bit.FMEMWREN = 1;
	FlashRegs.FPROWAIT = FPROWAIT_96M;
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FPGSWAIT = FPGSWAIT_96M;
	FlashRegs.FPERWAIT = FPERWAIT_96M;
	FlashRegs.FBANKWAIT = FBANKWAIT_96M;
	EDIS;

	for(i = 0; i < Length; i++)
	{
		while(FlashRegs.FSTAT.bit.BUSY || FlashRegs.FSTAT.bit.PROGRAMING || FlashRegs.FSTAT.bit.ERASEING || FlashRegs.FSTAT.bit.READING || FlashRegs.FSTAT.bit.MASS_ERASE);
		*(Uint32*)(FlashAddr + i) = *(BufAddr + i);
	}
	//	while (FlashRegs.FSTAT.bit.BUSY || FlashRegs.FSTAT.bit.PROGRAMING);

	Verify_Status = Flash28034_Verify(FlashAddr, BufAddr, Length, &VerifyStatus);
	if(Verify_Status == SUCCESS)
		return SUCCESS;
	else
		return STATUS_FAIL_PROGRAM;
}

/******************************************************************
 *函数名：Flash28034_Verify
 *参   数： FlashAddr, Flash校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_Verify(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i,ECnt;
	ECnt = 0;
	for(i = 0; i < Length; i++)
	{
		if(*(BufAddr + i) != *(FlashAddr + i))
			ECnt++;
	}

	if(ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_VERIFY;
}

/******************************************************************
 *函数名：Flash28034_ProgVerify
 *参   数： FlashAddr, Flash校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_ProgVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i,ECnt;
	ECnt = 0;
	for(i = 0; i < Length; i++)
	{
		if(0xFFFFFFFF != *(FlashAddr + i))
			ECnt++;
	}

	if(ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_ZERO_BIT_ERROR;
}

/******************************************************************
 *函数名：Flash28034_EraseVerify
 *参   数： FlashAddr, Flash擦写校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_EraseVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i,ECnt;
	ECnt = 0;
	for(i = 0; i < Length; i++)
	{
		if((Uint32)(FlashAddr + i) < 0x7DFFF0)
		{
			if(0xFFFFFFFF != *(FlashAddr + i))
				ECnt++;
		}
	}

	if(ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_ERASE;
}

/******************************************************************
 *函数名：Flash28034_Erase
 *参   数： SectorIdx, Flash擦除扇区索引值
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_Erase(Uint32 SectorIdx, FLASH_ST *FEraseStat)
{
//	Uint32 *FlashAddr;
	FLASH_ST EraseVerifyStatus;
	Uint16 EraseVerify_Status;

	// CSM Unlock
	if(FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	if(SectorIdx < 0x0 || SectorIdx > 0x7f)
		return STATUS_FAIL_NO_SECTOR_SPECIFIED;

	EALLOW;
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FPERWAIT = FPERWAIT_96M;
	FlashRegs.FBANKWAIT = FBANKWAIT_96M;
	EDIS;

	EALLOW;
	FlashRegs.FPERCTL.bit.erase = SectorIdx;
	EDIS;
	while(FlashRegs.FSTAT.bit.ERASEING);
	// Verify Each Sector, Length = 512 * 4B
	EraseVerify_Status = Flash28034_EraseVerify(Sector[SectorIdx].StartAddr, 512, &EraseVerifyStatus);
	if(EraseVerify_Status != SUCCESS)
		return STATUS_FAIL_ERASE;
	return SUCCESS;
}

/******************************************************************
 *函数名：Flash28034_MassErase
 *参   数：
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用： Flash区全部擦除操作
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28034_MassErase(FLASH_ST *FEraseStat)
{

	// CSM Unlock
	if(FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	EALLOW;
	// FlashRegs.FPERCTL.bit.enable_erase = 1;
	// FlashRegs.FPROWAIT = 60; // T = OSC_CLK / 4
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FNVH1WAIT = FNVH1WAIT_96M;
	FlashRegs.FMERWAIT = FMERWAIT_96M;
	EDIS;

	EALLOW;
	FlashRegs.FMERCTL.bit.MASS_ERASE = 1;
	EDIS;
	while(FlashRegs.FSTAT.bit.MASS_ERASE);
	return SUCCESS;
}
