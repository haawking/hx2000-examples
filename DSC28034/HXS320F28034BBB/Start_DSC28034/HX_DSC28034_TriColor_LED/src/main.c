﻿/******************************************************************
 文 档 名 ：     HX_DSC28034_TriColor_LED
 开 发 环 境： IDE V2.0.0
 开 发 板 ：    Start_DSC28034_V1.2湖人板
 D S P：         DSC28034
 使 用 库：无
 作 用：pwm波生成
 说 明：采用ePWM模块编程实现输出周期在2.2ms，
 * 采用事件触发中断脉冲(脉冲计数0-10循环）
 * 实现高电平在55us到2145us变化的PWM波，TBCTR采用向上向下计数；
 ----------------------例程使用说明-----------------------------
 *
 *            EPWM发波，占空比调节蜂鸣器与三色灯功能
 *
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

 
#include <stdio.h>
#include "dsc_config.h"
#include "hx_rv32_dev.h"
#include "hx_rv32_type.h"
#include <syscalls.h>
#include "xcustom.h"
#include "IQmathLib.h"
#include "epwm.h"



int main(void)
{
	/*系统初始化控制*/
	InitSysCtrl();
	/*EPWM外设的GPIO配置*/
	InitEPwm4Gpio();
	InitEPwm5Gpio();
	/*关中断*/
	InitPieCtrl();
	/*清中断*/
	IER_DISABLE(0xffff);
	IFR_DISABLE(0xffff);
	/*初始化中断向量表*/
	InitPieVectTable();
	/*EPWM_INT中断向量表地址指向执行相应的EPWM中断服务程序*/
	EALLOW;
	PieVectTable.EPWM4_INT=&epwm4_isr;
	PieVectTable.EPWM5_INT=&epwm5_isr;
	EDIS;
	/*禁止EPWM的时基使能,此时允许进行EPWM初始化配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=0;
	EDIS;
	/*EPWM模块初始化配置*/
	InitEPwm4Example();
	InitEPwm5Example();
	/*EPWM的时基使能,此时EPWM的配置功能将开始起作用*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=1;
	EDIS;
	/*PIEIER第3组中断向量使能*/
	IER_ENABLE(M_INT3);
	/*PIEIER第3组第1到3个中断向量使能*/
	PieCtrlRegs.PIEIER3.bit.INTx4=1;
	PieCtrlRegs.PIEIER3.bit.INTx5=1;
	/*打开全局中断*/
	EINT;

    while(1){

    }


	return 0;
}

// ----------------------------------------------------------------------------

