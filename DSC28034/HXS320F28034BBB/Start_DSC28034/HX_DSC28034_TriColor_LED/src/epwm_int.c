#include "epwm.h"

void INTERRUPT epwm4_isr(void)
{
	update_compare(&epwm4_info);

    epwm4_info.EPwmTimerIntCount++;

	EPwm4Regs.ETCLR.bit.INT=1;

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}

void INTERRUPT epwm5_isr(void)
{
	update_compare(&epwm5_info);

	epwm5_info.EPwmTimerIntCount++;

	EPwm5Regs.ETCLR.bit.INT=1;

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}
