#include "epwm.h"

#define EPWM_CMP_UP   1
#define EPWM_CMP_DOWN 0

void update_compare(EPWM_INFO *epwm_info)
{
	if(epwm_info->EPwmTimerIntCount>=10)
	{
		epwm_info->EPwmTimerIntCount=0;
		if(epwm_info->EPwm_CMPA_Direction==EPWM_CMP_UP)
		{
			if(epwm_info->EPwmRegHandle->CMPA.half.CMPA<epwm_info->EPwmMaxCMPA)
					{
						epwm_info->EPwmRegHandle->CMPA.half.CMPA+=10;
					}
					else
					{
						epwm_info->EPwm_CMPA_Direction=EPWM_CMP_DOWN;
						epwm_info->EPwmRegHandle->CMPA.half.CMPA-=10;
					}
				}
				else
				{
					if(epwm_info->EPwmRegHandle->CMPA.half.CMPA==epwm_info->EPwmMinCMPA)
					{
						epwm_info->EPwm_CMPA_Direction=EPWM_CMP_UP;
						epwm_info->EPwmRegHandle->CMPA.half.CMPA+=10;
					}
					else
					{
						epwm_info->EPwmRegHandle->CMPA.half.CMPA-=10;
					}
				}

				if(epwm_info->EPwm_CMPB_Direction==EPWM_CMP_UP)
				{
					if(epwm_info->EPwmRegHandle->CMPB<epwm_info->EPwmMaxCMPB)
					{
						epwm_info->EPwmRegHandle->CMPB+=10;
					}
					else
					{
						epwm_info->EPwm_CMPB_Direction=EPWM_CMP_DOWN;
						epwm_info->EPwmRegHandle->CMPB-=10;
					}
				}
				else
				{
					if(epwm_info->EPwmRegHandle->CMPB==epwm_info->EPwmMinCMPB)
					{
						epwm_info->EPwm_CMPB_Direction=EPWM_CMP_UP;
						epwm_info->EPwmRegHandle->CMPB+=10;
					}
					else
					{
						epwm_info->EPwmRegHandle->CMPB-=10;
					}
				}
		}
	else
	{
		epwm_info->EPwmTimerIntCount++;
	}
	return;
}





