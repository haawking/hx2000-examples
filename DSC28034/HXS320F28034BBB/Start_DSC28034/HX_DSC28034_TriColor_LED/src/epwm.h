#ifndef epwm_h_
#define epwm_h_

#include "dsc_config.h"

typedef struct
{
	volatile struct EPWM_REGS *EPwmRegHandle;
	uint16_t EPwm_CMPA_Direction;
	uint16_t EPwm_CMPB_Direction;
	uint16_t EPwmTimerIntCount;
	uint16_t EPwmMaxCMPA;
	uint16_t EPwmMinCMPA;
	uint16_t EPwmMaxCMPB;
	uint16_t EPwmMinCMPB;
} EPWM_INFO;


void InitEPwm4Example(void);
void InitEPwm5Example(void);

void INTERRUPT epwm4_isr(void);
void INTERRUPT epwm5_isr(void);

void update_compare(EPWM_INFO*);

extern EPWM_INFO epwm4_info;
extern EPWM_INFO epwm5_info;

#endif/*epwm_h_*/
