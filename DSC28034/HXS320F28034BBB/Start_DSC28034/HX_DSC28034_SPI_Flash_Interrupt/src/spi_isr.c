#include "spi.h"

void INTERRUPT spi_rx_int(void)
{
	GpioDataRegs.GPBTOGGLE.bit.GPIO34=1;

	Wait_Busy();
	/*读取数据*/
	ReadData(0, upper_128, 128);

	SpiaRegs.SPIFFRX.bit.RXFFINTCLR=1;
	SpiaRegs.SPIFFRX.bit.RXFFOVFCLR = 1; /*清除RXFFOVF位*/
	PieCtrlRegs.PIEACK.bit.ACK6=1;
}
