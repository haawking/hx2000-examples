#include "epwm.h"

#define EPWM_DB_UP   1
#define EPWM_DB_DOWN 0

void update_db(EPWM_INFO *epwm_info)
{
	if(epwm_info->EPWM_DB_Direction == EPWM_DB_UP)
	{
		if(epwm_info->EPwmRegHandle->DBFED < epwm_info->EPWMMaxDB)
		{
			epwm_info->EPwmRegHandle->DBFED += 10;
			epwm_info->EPwmRegHandle->DBRED += 10;
		}
		else
		{
			epwm_info->EPWM_DB_Direction = EPWM_DB_DOWN;
			epwm_info->EPwmRegHandle->DBFED -= 10;
			epwm_info->EPwmRegHandle->DBRED -= 10;
		}
	}
	else
	{
		if(epwm_info->EPwmRegHandle->DBFED == epwm_info->EPWMMinDB)
		{
			epwm_info->EPWM_DB_Direction = EPWM_DB_UP;
			epwm_info->EPwmRegHandle->DBFED += 10;
			epwm_info->EPwmRegHandle->DBRED += 10;
		}
		else
		{

			epwm_info->EPwmRegHandle->DBFED -= 10;
			epwm_info->EPwmRegHandle->DBRED -= 10;
		}
	}
}
