/******************************************************************
 文 档 名 ：     HX_DSC28034_ADC_SIMUL
 开 发 环 境：  Haawking IDE V2.0.0
 开 发 板：      Core_DSC28034_V1.3
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：
 作 用：单触发源多通道ADC同时采样
 说 明：1.在Haawking IDE调试界面上输入adcValA与adcValB数组变量；
2.将F28034全功能开发板（湖人板）的电位器负端RG
 与ADC通道采集输入引脚ADCINA0/A1/A2/A3/A4/A5/A6/A7连接，
 可从adcValA[0]/A[1]/A[2]/A[3]/A[4]/A[5]/A[6]/A[7]
 正确读出电位器电阻两端电压对应的数字量值X，
 转换结果为Vi=X/4096*3.3V。
3.将F28034全功能开发板（湖人板）的电位器负端RG
 与ADC通道采集输入引脚ADCINB0/B1/B2/B3/B4/B5/B6/B7连接，
 可从adcValB[0]/B[1]/B[2]/B[3]/B[4]/B[5]/B[6]/B[7]
 正确读出电位器电阻两端电压对应的数字量值X，
 转换结果为Vi=X/4096*3.3V。
 ----------------------例程使用说明-----------------------------
 *
 *            测试ADC同时采样功能
 *
 *
 * 现象：可正确读出电位器电阻两端电压对应的数字量值X
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "epwm.h"
#include "adc.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();

	/*初始化Flash*/
	InitFlash();

	DINT;

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();

	/*初始化模拟IO*/
	InitAdcAio();
	/*ADC初始化*/
	InitAdc();

	EALLOW;
	/*清除中断*/
	PieCtrlRegs.PIEACK.all = 0XFFFF;
	/*将adc_isr入口地址赋给ADCINT3*/
	PieVectTable.ADCINT3 = &adc_isr;
	/*开启对应中断*/
	PieCtrlRegs.PIEIER10.bit.INTx3 = 1;
	EDIS;

	/*使能CPU中断*/
	IER |= M_INT10;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*配置epwm1*/
	epwm1_config();

	EALLOW;
	/*所有使能的ePWM模块时钟都是在TBCLK的第一个上升沿对齐的情况下开始的*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*初始化ADC*/
	ADC_Config();

	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
