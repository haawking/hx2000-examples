#include "HRCAP.h"

void HRCAP1_Config(void)
{
	EALLOW;
	/*清零计数器、捕获寄存器、IFR寄存器位*/
	HRCap1Regs.HCCTL.bit.SOFTRESET=1;
	/*捕获时钟选择：旁路时钟分频器*/
	HRCap1Regs.HCCTL.bit.HCCAPCLKSEL=1;
	/*使能上升沿捕获中断*/
	HRCap1Regs.HCCTL.bit.RISEINTE=1;
	/*禁用下降沿捕获中断*/
	HRCap1Regs.HCCTL.bit.FALLINTE=0;
	/*禁用计数器溢出中断*/
	HRCap1Regs.HCCTL.bit.OVFINTE=0;

	EDIS;
}

void HRCAP2_Config(void)
{
	EALLOW;
	/*清零计数器、捕获寄存器、IFR寄存器位*/
	HRCap2Regs.HCCTL.bit.SOFTRESET=1;
	/*捕获时钟选择：旁路时钟分频器*/
	HRCap2Regs.HCCTL.bit.HCCAPCLKSEL=1;
	/*禁用上升沿捕获中断*/
	HRCap2Regs.HCCTL.bit.RISEINTE=0;
	/*使能下降沿捕获中断*/
	HRCap2Regs.HCCTL.bit.FALLINTE=1;
	/*禁用计数器溢出中断*/
	HRCap2Regs.HCCTL.bit.OVFINTE=0;

	EDIS;
}

