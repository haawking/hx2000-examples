/******************************************************************
 文 档 名：     epwm_int.c
 D S P：       DSC28034
 使 用 库：     
 作     用：      EPWM1/2/3的输出占空比变化配置的中断服务程序
 说     明：      配置pwm波占空比按正弦规律变化，将一圈分割成30等份
 ******************************************************************/

#include "epwm.h"

void INTERRUPT epmw1_isr(void)
{
	/*占空比按正弦规律变化配置*/
	epwm1_compare();

	/*清除事件中断的INT全局中断*/
	EPwm1Regs.ETCLR.bit.INT = 1;

	/*中断应答，锁定IER的第3组中断向量*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

