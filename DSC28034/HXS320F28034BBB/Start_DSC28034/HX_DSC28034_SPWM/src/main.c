/******************************************************************
 文 档 名：     HX_DSC28034_SPWM
 开 发 环 境： Haawking IDE V2.0.0
 开 发 板：     Core_DSC28034_V1.3
                      Start_DSC28034_V1.2

 D S P：       DSC28034
 使 用 库：
 作     用：      SPWM10KHz、死区5us、偏差120度输出
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：  三路占空比按正弦规律变化的pwm波10KHz、死区5us、
                     相位偏差120度输出，TBCTR采用向上向下计数

 现象：

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "epwm.h"

int32 cmp1,cmp2,cmp3;

int main(void)
{
	/*系统时钟初始化*/
		InitSysCtrl();

		/*EPWM外设引脚初始化配置*/
		InitEPwm1Gpio();
		InitEPwm2Gpio();
		InitEPwm3Gpio();

		InitPieCtrl();

		/*清中断，关中断*/
		IER = 0x0000;
		IFR = 0x0000;

		/*中断向量表初始化配置*/
		InitPieVectTable();

		EALLOW;
		/*配置EPWM1_INT中断向量表指向执行相应的中断服务程序*/
		PieVectTable.EPWM1_INT = &epmw1_isr;
		EDIS;

		EALLOW;
		/*禁止EPWM的时基使能，允许EPWM初始化配置写入*/
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;

		/*EPWM的初始化配置*/
		InitEPwm1Example();
		InitEPwm2Example();
		InitEPwm3Example();

		EALLOW;
		/*打开EPWM的时基使能，使EPWM的初始化配置起作用*/
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;

		/*使能打开IER的第3组中断向量*/
		IER = M_INT3;

		/*使能打开IER的第3组中断向量的第一个向量*/
		PieCtrlRegs.PIEIER3.bit.INTx1 = 1;

		/*使能打开全局中断*/
		EINT;

		while(1)
		{
			cmp1=EPwm1Regs.CMPA.half.CMPA;
			cmp2=EPwm2Regs.CMPA.half.CMPA;
			cmp3=EPwm3Regs.CMPA.half.CMPA;
		}


	return 0;
}

// ----------------------------------------------------------------------------
