/******************************************************************
 文 档 名 ：     HX_DSC28034_EPWM_Blanking_Window
 开 发 环 境：  Haawking IDE V2.0.0
 开 发 板：      Core_DSC28034_V1.3
                       Start_DSC28034_V1.2
 D S P：          DSC28034
 使 用 库：

 ----------------------例程使用说明-----------------------------
 *
 *            测试DC的BLANK_WINDOW空窗功能
 *
 作 用：比较器输出触发DC事件动作+空窗
 说 明：按程序设置将F28034全功能开发板（湖人板）
 将3.3V(或>0.825V)的输出与比较器输入引脚ADCINA2/AIO2相连，
 将比较器1输出GPIO1与PWM TZ2-GPIO17相连，GPIO12-3.3V相连

 *
 * 现象：可观察到D402常亮  不滤波的DC事件可触发 产生动作，EPWM2A置高
 * 滤波的DC事件被滤除，不触发动作
 * EPWM2A动作期间，可以看到一段空窗，被滤除的DC事件，表现出正常的EPWM波输出
 *
 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "system.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm2Gpio();
	/*初始化TZ故障保护模块*/
	InitTzGpio();
	/*初始化Adc*/
	InitAdc();
	/*初始化GPIO，复用为comp功能*/
	InitComp1Gpio();
	/*初始化LED配置*/
	/*将PIE控制寄存器初始化为默认状态，该状态禁止所有PIE中断并清除所有标志*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();
	/*允许访问受保护的空间*/
	EALLOW;
	/*执行带有空窗的DC事件触发EPWM2A,DC事件触发EPWM2B*/
	PieVectTable.EPWM2_TZINT = &epwm2_tz_isr;
	/*将adc_isr入口地址赋给ADCINT3，执行ADC采样*/
	PieVectTable.ADCINT3 = &adc_isr;
	EDIS;

	EALLOW;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止。*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*	初始化EPWM2*/
	InitEpwm2_Example();

	EALLOW;
	/*所有使能的ePWM模块同步使用TBCLK*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*初始化ADC*/
	ADC_Init();
	/*初始化comp1*/
	InitComp1();
	/*使能相对应的中断*/
	IER |= M_INT2  | M_INT10;

	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;

	PieCtrlRegs.PIEIER10.bit.INTx3 = 1;

	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
