#ifndef CAN_H_
#define CAN_H_

#include "dsc_config.h"

void CAN_Init(void);
uint16 CAN_GetWordData(void);
INTERRUPT void eCanRxIsr(void);
void CAN_Tx(void);
extern uint16 receive_data;

#endif /* CAN_H_ */
