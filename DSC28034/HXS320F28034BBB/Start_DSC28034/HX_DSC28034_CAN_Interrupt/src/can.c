#include  "dsc_config.h"

Uint16 receive_data;

void CAN_Init()
{
   volatile struct ECAN_REGS ECanaShadow;

   EALLOW;

/* Configure eCAN RX and TX pins for CAN operation using eCAN regs*/

    ECanaShadow.CANTIOC.all = P_ECanaRegs->CANTIOC.all;
    ECanaShadow.CANTIOC.bit.TXFUNC = 1;
    P_ECanaRegs->CANTIOC.all = ECanaShadow.CANTIOC.all;

    ECanaShadow.CANRIOC.all = P_ECanaRegs->CANRIOC.all;
    ECanaShadow.CANRIOC.bit.RXFUNC = 1;
    P_ECanaRegs->CANRIOC.all = ECanaShadow.CANRIOC.all;

/* Initialize all bits of 'Message Control Register' to zero */
// Some bits of MSGCTRL register come up in an unknown state. For proper operation,
// all bits (including reserved bits) of MSGCTRL must be initialized to zero

    P_ECanaMboxes->MBOX1.MSGCTRL.all = 0x00000000;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   //P_ECanaMboxes->MBOX1.MSGCTRL.all = 0x00000002;

/* Clear all RMPn, GIFn bits */
// RMPn, GIFn bits are zero upon reset and are cleared again as a precaution.

   P_ECanaRegs->CANRMP.all = 0xFFFFFFFF;

/* Clear all interrupt flag bits */

   P_ECanaRegs->CANGIF0.all = 0xFFFFFFFF;
   P_ECanaRegs->CANGIF1.all = 0xFFFFFFFF;


   // Initialize all bits of 'Message Control Register' to zero
      // Some bits of MSGCTRL register come up in an unknown state.
      // For proper operation, all bits (including reserved bits) of MSGCTRL must
      // be initialized to zero
      //
      ECanaMboxes.MBOX0.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX1.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX2.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX3.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX4.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX5.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX6.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX7.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX8.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX9.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX10.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX11.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX12.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX13.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX14.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX15.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX16.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX17.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX18.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX19.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX20.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX21.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX22.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX23.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX24.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX25.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX26.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX27.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX28.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX29.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX30.MSGCTRL.all = 0x00000000;
      ECanaMboxes.MBOX31.MSGCTRL.all = 0x00000000;

/* Configure bit timing parameters for eCANA*/

	ECanaShadow.CANMC.all = P_ECanaRegs->CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
    P_ECanaRegs->CANMC.all = ECanaShadow.CANMC.all;

    ECanaShadow.CANES.all = P_ECanaRegs->CANES.all;

    do
	{
	    ECanaShadow.CANES.all = P_ECanaRegs->CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 1 );  		// Wait for CCE bit to be set..

    ECanaShadow.CANBTC.all = 0;

	ECanaShadow.CANBTC.bit.BRPREG = 2;
	ECanaShadow.CANBTC.bit.TSEG2REG = 4;
	ECanaShadow.CANBTC.bit.TSEG1REG = 13;

    P_ECanaRegs->CANBTC.all = ECanaShadow.CANBTC.all;

    ECanaShadow.CANMC.all = P_ECanaRegs->CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 0 ;            // Set CCR = 0
    P_ECanaRegs->CANMC.all = ECanaShadow.CANMC.all;

    ECanaShadow.CANES.all = P_ECanaRegs->CANES.all;

    do
    {
       ECanaShadow.CANES.all = P_ECanaRegs->CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 0 ); 		// Wait for CCE bit to be  cleared..

/* Disable all Mailboxes  */

   P_ECanaRegs->CANME.all = 0;     // Required before writing the MSGIDs

/* Assign MSGID to MBOX1 */

   P_ECanaMboxes->MBOX1.MSGID.all = 0x00040000;	// Standard ID of 1, Acceptance mask disabled
   P_ECanaMboxes->MBOX0.MSGID.all = 0x00040000;	// Standard ID of 1, Acceptance mask disabled

   ECanaMboxes.MBOX1.MSGCTRL.bit.DLC = 0x2;
/* Configure MBOX1 to be a receive MBOX */

   ECanaMboxes.MBOX0.MSGCTRL.bit.DLC = 0x2;
/* Configure MBOX0 to be a receive MBOX */
   ECanaRegs.CANMD.bit.MD0=1;
   ECanaRegs.CANMD.bit.MD1=0;

/* Enable MBOX0 and MBOX1 */
   ECanaRegs.CANME.bit.ME0=1;
   ECanaRegs.CANME.bit.ME1=1;

   //挂起接收邮箱，以触发接收中断
   if(ECanaRegs.CANRMP.bit.RMP0==0)
   {
	   ECanaRegs.CANRMP.bit.RMP0=1;
   }
   EDIS;

    return;
}


Uint16 CAN_GetWordData()
{
    Uint16 wordData;
    Uint16 byteData;

    wordData = 0x0000;
    byteData = 0x0000;
    //等待MBOX0邮箱接收成功后，读取消息
     while(ECanaRegs.CANRMP.all == 0){ }
    wordData = (Uint16)ECanaMboxes.MBOX0.MDL.byte.BYTE0;
    byteData = (Uint16)ECanaMboxes.MBOX0.MDL.byte.BYTE1;
    wordData |= (byteData << 8);
    ECanaRegs.CANRMP.bit.RMP0=1;//清除接收邮箱MBOX1的挂起状态
    return wordData;
}

void CAN_Tx(void)
{
	//等待MBOX0接收成功，读取消息
	 while(ECanaRegs.CANRMP.bit.RMP0!=1){}
	 ECanaMboxes.MBOX1.MDL.byte.BYTE0 = (receive_data&0xff);
	 ECanaMboxes.MBOX1.MDL.byte.BYTE1 = ((receive_data>>8)&0xff);

	 ECanaRegs.CANTRS.bit.TRS1 = 1;//发送MBOX1数据到MBOX0
}


