################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Adc.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CSMPasswords.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Comp.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CpuTimers.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_DefaultIsr.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECan.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECap.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EPwm.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EQep.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Gpio.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_HRCap.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_I2C.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Lin.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_OTP.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieCtrl.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieVect.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Sci.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Spi.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_SysCtrl.c \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_TempSensorConv.c 

S_UPPER_SRCS += \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/_DSP2803x_usDelay.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/disable_interrupt.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/enable_interrupt.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_set.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_unset.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_set.S \
../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_unset.S 

OBJS += \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Adc.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CSMPasswords.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Comp.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CpuTimers.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_DefaultIsr.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECan.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECap.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EPwm.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EQep.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Gpio.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_HRCap.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_I2C.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Lin.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_OTP.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieCtrl.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieVect.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Sci.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Spi.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_SysCtrl.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_TempSensorConv.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/_DSP2803x_usDelay.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/disable_interrupt.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/enable_interrupt.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_set.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_unset.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_set.o \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_unset.o 

S_UPPER_DEPS += \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/_DSP2803x_usDelay.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/disable_interrupt.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/enable_interrupt.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_set.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ier_unset.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_set.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/ifr_unset.d 

C_DEPS += \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Adc.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CSMPasswords.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Comp.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_CpuTimers.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_DefaultIsr.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECan.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_ECap.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EPwm.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_EQep.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Gpio.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_HRCap.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_I2C.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Lin.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_OTP.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieCtrl.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_PieVect.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Sci.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_Spi.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_SysCtrl.d \
./haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/DSP2803x_TempSensorConv.d 


# Each subdirectory must supply rules for building sources it contributes
haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/%.o: ../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T ../haawking-drivers/ldscripts/DSC28034_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_2_1_0=0 -DDEBUG -DDSC28034_BBB -DHAAWKING_DSC28034_BOARD -I"../haawking-drivers/haawking-dsc28034_bbb-board/common" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_headers/include" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/%.o: ../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/source/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross Assembler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T ../haawking-drivers/ldscripts/DSC28034_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_2_1_0=0 -x assembler-with-cpp -DDEBUG -DDSC28034_BBB -DHAAWKING_DSC28034_BOARD -I"../haawking-drivers/haawking-dsc28034_bbb-board/common" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_headers/include" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


