################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/can.c \
../src/can_isr.c \
../src/main.c 

OBJS += \
./src/can.o \
./src/can_isr.o \
./src/main.o 

C_DEPS += \
./src/can.d \
./src/can_isr.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T ../haawking-drivers/ldscripts/DSC28034_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/Haawking-IDE-win64-V2.1.0-beta9/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_2_1_0=0 -DDEBUG -DDSC28034_BBB -DHAAWKING_DSC28034_BOARD -I"../haawking-drivers/haawking-dsc28034_bbb-board/common" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_headers/include" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


