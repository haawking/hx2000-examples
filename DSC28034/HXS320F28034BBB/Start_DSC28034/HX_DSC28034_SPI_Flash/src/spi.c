
#include "spi.h"

uint8_t upper_128[128] = { 0 };
W25Q64Flash SPIFlash = { 0, 0, 0, 0, 0 };

/******************************************************************
 函数名：void SPI_IOinit(void)
 参	数：无
 返回值：无
 作	用：GPIO16、GPIO17、GPIO18为SPI功能，GPIO39为片选引脚
 ******************************************************************/

void spi_ioinit(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO39 = 0; /*作为普通IO口使用*/
	GpioCtrlRegs.GPBDIR.bit.GPIO39 = 1; /*管脚配置成输出*/

	GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0; /*Enable pull-up on GPIO16 (SPIAMOSI)*/
	GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0; /*Enable pull_up on GPIO17 (SPIAMISO)*/
	GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0; /* Enable pull-up on GPIO18 (SPICLKA)*/
	/*GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;    Enable pull-up on GPIO19 (SPISTEA)*/

	GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 3; /* Asynch input GPIO16 (SPISIMOA)*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3; /* Asynch input GPIO17 (SPISOMIA)*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO18 = 3; /* Asynch input GPIO18 (SPICLKA)*/
	/*GpioCtrlRegs.GPAQSEL2.bit.GPIO19 = 3;  Asynch input GPIO19 (SPISTEA)*/

	GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 1; /* Configure GPIO16 as SPISIMOA*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 1; /* Configure GPIO17 as SPISOMIA*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 1; /* Configure GPIO18 as SPICLKA*/
	/*GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 1;  Configure GPIO19 as SPISTEA*/
	EDIS;

}

/******************************************************************
 函数名：void spi_fifo_init(void)
 参	数：无
 返回值：无
 作	用：初始化SPI的FIFO寄存器，
 ******************************************************************/

void spi_fifo_init(void)
{
	SpiaRegs.SPIFFTX.bit.SPIRST = 1; /*写0复位SPI发送和接收通道，SPI FIFO寄存器配置位将被保留,SPI发送寄存器*/
	/*写1，SPI FIFO能重新开始发送或接收，这不影响SPI的寄存器位*/
	SpiaRegs.SPIFFTX.bit.SPIFFENA = 1; /*SPI FIFO增强使能*/
	SpiaRegs.SPIFFTX.bit.TXFFIL = 1;		//发送 FIFO 小于等于1时产生TX FIFO中断
	SpiaRegs.SPIFFTX.bit.TXFFINTCLR = 1; /*Write 1 to clear TXFFINT flag in bit 7.*/
	SpiaRegs.SPIFFTX.bit.TXFFIENA = 0; /*不用中断*/

	SpiaRegs.SPIFFRX.bit.RXFFOVFCLR = 1; /*清除RXFFOVF位*/
	SpiaRegs.SPIFFRX.bit.RXFFINTCLR = 1; /*写1清除RXFFINT标志位*/
	SpiaRegs.SPIFFRX.bit.RXFFST = 1; /*接收FIFO有1个字*/
	SpiaRegs.SPIFFRX.bit.RXFFIENA = 0; /*不用FIFO接收中断*/
	SpiaRegs.SPIFFRX.bit.RXFFIL = 4;

	SpiaRegs.SPIFFCT.all = 0x0; /*无延迟*/

}

/******************************************************************
 函数名：void spi_init(void)
 参	数：无
 返回值：无
 作	用：初始化SPI的控制寄存器，
 ******************************************************************/

void spi_init(void)
{
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;

	SpiaRegs.SPICCR.bit.SPISWRESET = 0; /*初始化SPI操作标志位到复位条件*/
	SpiaRegs.SPICCR.bit.SPICHAR = 7; /*8位数据传输*/
	SpiaRegs.SPICCR.bit.CLKPOLARITY = 0; /*when no SPI data is sent,SPICLK is at low*/
	SpiaRegs.SPICTL.bit.CLK_PHASE = 1; /* 当CPHA，数据线在SCK的偶数边沿采样*/
	SpiaRegs.SPICTL.bit.MASTER_SLAVE = 1;
	SpiaRegs.SPIBRR = 2; /* For SPIBRR = 3 to 127;  SPI Baud Rate = LSPCLK/(SPIBRR+1)*/

	SpiaRegs.SPICCR.bit.SPISWRESET = 1; /* SW Reset off*/

	SpiaRegs.SPIPRI.bit.FREE = 1; /*  Set so breakpoints don't disturb xmission*/

}

void WrSReg(uint16 setReg)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(WRITE_SREG_IST);
	Send_Byte((setReg & 0x00ff));
	Send_Byte((setReg & 0x0300) >> 8);
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;
}

/******************************************************************
 函数名：unsigned long Jedec_ID_Read(void)
 参	数：无
 返回值：W25Q16 ID参数
 作	用：
 P17  W25Q16 ID参数
 P20  ID 指令表格与读取指令的通信格式
 ******************************************************************/
unsigned long Jedec_ID_Read(void)
{
	unsigned long temp1, temp2, temp3;
	temp1 = 0;
	temp2 = 0;
	temp3 = 0;

	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;

	Send_Byte(0x9F); /* send JEDEC ID command (9Fh) */

	temp1 = Get_Byte();
	temp2 = Get_Byte();
	temp3 = Get_Byte();

	GpioDataRegs.GPBSET.bit.GPIO39 = 1;

	temp1 = ((temp1 << 16) | (temp2 << 8) | (temp3)) & 0x00FFFFFF;

	return temp1;
}

uint8 Send_Byte(uint16 a)
{
	uint16 rdata;

	SpiaRegs.SPICTL.bit.TALK = 1; /*P23 Enable Transmit path*/
	SpiaRegs.SPITXBUF = (a << 8) & 0xff00; /*发送的数据必须左对齐*/
	while (SpiaRegs.SPIFFRX.bit.RXFFST == 0)
	{

	}
	/*for(i = 0;i<200;i++){};*/
	rdata = SpiaRegs.SPIRXBUF;
	return rdata;
}

uint16 Get_Byte(void)
{
	uint8_t rdata;
	SpiaRegs.SPICTL.bit.TALK = 0; /* Disable Transmit pat*/
	SpiaRegs.SPITXBUF = DUMMYDATA; /* Send dummy to start*/
	while (SpiaRegs.SPIFFRX.bit.RXFFST == 0)
	{
	} /* Wait until data is received*/

	rdata = SpiaRegs.SPIRXBUF; /* Master reads data*/
	return rdata;
}

/************************************************************************
 函数名：uint16 Read_Status_2Reg(void)
 参	数：无
 返回值：状态寄存器1和状态寄存器2中的值
 作	用：读状态寄存器1和状态寄存器2的值
 *P23
 *指令 0x03
 *地址 A23--A0
 ************************************************************************/

uint16 Read_Status_2Reg(void)
{
	uint16 wbyte1, wbyte2;

	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(READ_STATUS_REG_IST); /*读状态寄存器1指令，返回Status Register1*/
	wbyte1 = Get_Byte();
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;

	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(0x35); /*READ_STATUS_REG_IST  读状态寄存器2指令，返回Status Register2*/
	wbyte2 = Get_Byte();
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;

	wbyte2 <<= 6;
	wbyte2 &= 0xff00;
	wbyte2 += wbyte1 & 0xff;
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;

	return wbyte2;

}

void ReadData(unsigned long Dst, unsigned char *Rxbuf, unsigned long len)
{
	unsigned long i = 0;
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /*使能设备*/
	Send_Byte(READ_DATA_IST); /*读命令*/
	Send_Byte(((Dst & 0xFFFFFF) >> 16)); /* send 3 address bytes */
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	for (i = 0; i < len; i++)
	{
		*(Rxbuf++) = Get_Byte(); /*读取地址*/
	}
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /*不使能设备*/
}

void PageProgram(unsigned long Dst, unsigned char *byte, unsigned char len)
{
	unsigned char i;
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(PAGE_PRG_IST); /* send Byte Program command */
	Send_Byte(((Dst & 0xFFFFFF) >> 16));/* send 3 address bytes */
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	for (i = 0; i < len; i++)
	{
		/*判断忙标志，再进行写操作*/
		/*printf(" %x",(*byte)&0xff);*/
		Send_Byte(*(byte++));
	}
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

void chip_Erase(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(CHIP_ERASE_IST); /* send Chip Erase command (60h or C7h) */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;
}

void Sector4K_Erase(unsigned long Dst)
{
	Dst = Dst / 0x1000;
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
	Send_Byte(SECTOR_ERASE_IST);
	Send_Byte(((Dst & 0xFFFFFF) >> 16));
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	GpioDataRegs.GPBSET.bit.GPIO39 = 1;
}

/************************************************************************
 * PROCEDURE: Read_Status_Register
 *
 * This procedure read the status register and returns the byte.
 *
 * Input:
 *		None
 *
 * Returns:
 *		byte
 ************************************************************************/
unsigned char Read_Status_Register(void)
{
	unsigned char byte = 0;
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(READ_STATUS_REG_IST); /* send RDSR command */
	byte = Get_Byte(); /* receive byte */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
	return byte;
}

/************************************************************************/
/* PROCEDURE: EWSR							*/
/*									*/
/* This procedure Enables Write Status Register.  			*/
/*									*/
/* Input:								*/
/*		None							*/
/*									*/
/* Returns:								*/
/*		Nothing							*/
/************************************************************************/
void EWSR(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(WRITE_SREG_IST); /* enable writing to the status register */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************/
/* PROCEDURE: WRSR							*/
/*									*/
/* This procedure writes a byte to the Status Register.			*/
/*									*/
/* Input:								*/
/*		byte							*/
/*									*/
/* Returns:								*/
/*		Nothing							*/
/************************************************************************/
void WRSR(char unsigned byte)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(0x01); /* select write to status register */
	Send_Byte(byte); /* data that will change the status of BPx
	 or BPL (only bits 2,3,4,5,7 can be written) */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable the device */
}

/******************************************************************
 函数名：void WREN(void)
 参	数：无
 返回值：无
 作	用： 这个函数使能写使能锁存，它也能被用使能写状态寄存器
 *********************************************************************/
void WREN(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(WREN_IST); /* send WREN command */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************/
/* PROCEDURE: WRDI							*/
/*									*/
/* This procedure disables the Write Enable Latch.			*/
/*									*/
/* Input:								*/
/*		None							*/
/*									*/
/* Returns:								*/
/*		Nothing							*/
/************************************************************************/
void WRDI(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(0x04); /* send WRDI command */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************
 * PROCEDURE: Chip_Erase
 *
 * This procedure erases the entire Chip.
 *
 * Input:
 *		None
 *
 * Returns:
 *		Nothing
 ************************************************************************/
void Chip_Erase(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(CHIP_ERASE_IST); /* send Chip Erase command (60h or C7h) */
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************
 * PROCEDURE: Sector_Erase
 *
 * This procedure Sector Erases the Chip.
 *
 * Input:
 *		Dst:		Destination Address 000000H - 3FFFFFH
 *
 * Returns:
 *		Nothing
 ************************************************************************/
void Sector_Erase(unsigned long Dst)
{

	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(0x20); /* send Sector Erase command */
	Send_Byte(((Dst & 0xFFFFFF) >> 16)); /* send 3 address bytes */
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************
 * PROCEDURE: Block_Erase_32K
 *
 * This procedure Block Erases 32 KByte of the Chip.
 *
 * Input:
 *		Dst:	Destination Address 000000H - 3FFFFFH
 *
 * Returns:
 *		Nothing
 ************************************************************************/
void Block_Erase_32K(unsigned long Dst)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(0x52); /* send 32 KByte Block Erase command */
	Send_Byte(((Dst & 0xFFFFFF) >> 16)); /* send 3 address bytes */
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************
 * PROCEDURE: Block_Erase_64K
 *
 * This procedure Block Erases 64 KByte of the Chip.
 *
 * Input:   Dst:  Destination Address 000000H - 3FFFFFH
 *
 * Returns:	Nothing
 ************************************************************************/
void Block_Erase_64K(unsigned long Dst)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1; /* enable device */
	Send_Byte(0xD8); /* send 64KByte Block Erase command */
	Send_Byte(((Dst & 0xFFFFFF) >> 16)); /* send 3 address bytes */
	Send_Byte(((Dst & 0xFFFF) >> 8));
	Send_Byte(Dst & 0xFF);
	GpioDataRegs.GPBSET.bit.GPIO39 = 1; /* disable device */
}

/************************************************************************
 * PROCEDURE: Wait_Busy
 *
 * This procedure waits until device is no longer busy (can be used by
 * Byte-Program, Sector-Erase, Block-Erase, Chip-Erase).
 *
 * Input: None
 * Returns: Nothing
 *
 ************************************************************************/
void Wait_Busy(void)
{
	uint32_t waitCnt = 0;
	unsigned char Wbusy;
	waitCnt = 600000;
	Wbusy = Read_Status_Register();
	while ((Wbusy & 0x01) == 0x01) /* waste time until not busy */
	{
		Wbusy = Read_Status_Register();

		if (waitCnt == 0)
		{
			WREN();
			WrSReg(0x0000);
			SPIFlash.SReg = Read_Status_2Reg();
			break;
		}

		else
			waitCnt--;
	}
}

