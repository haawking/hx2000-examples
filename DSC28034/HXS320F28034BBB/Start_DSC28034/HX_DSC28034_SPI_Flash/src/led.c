
#include "dsc_config.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;
	GpioDataRegs.GPBSET.bit.GPIO34 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;
	GpioDataRegs.GPBSET.bit.GPIO44 = 1;
	EDIS;
}
