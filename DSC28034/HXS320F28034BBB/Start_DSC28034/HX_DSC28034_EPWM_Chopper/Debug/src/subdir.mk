################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/epwm.c \
../src/epwm_int.c \
../src/main.c \
../src/pwm_pc.c 

OBJS += \
./src/epwm.o \
./src/epwm_int.o \
./src/main.o \
./src/pwm_pc.o 

C_DEPS += \
./src/epwm.d \
./src/epwm_int.d \
./src/main.d \
./src/pwm_pc.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T DSC28034_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/hawwkingIDEV2.0/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/hawwkingIDEV2.0/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_1_9_2=0 -DDEBUG -DDSC28034_BBB -DHAAWKING_DSC28034_BOARD -I"../haawking-drivers/haawking-dsc28034_bbb-board/common" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_headers/include" -I"../haawking-drivers/haawking-dsc28034_bbb-board/DSP2803x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


