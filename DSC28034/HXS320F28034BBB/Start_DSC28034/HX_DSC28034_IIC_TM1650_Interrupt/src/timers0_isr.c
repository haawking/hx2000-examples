#include "TM1650_IIC.h"

/******************************************************************
 *函数名：void InitLED(void)
 *参 数：无
 *返回值：无
 *作 用：初始化LED，用于判断数码管刷新显示与IIC接收应答
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的寄存器*/
	EALLOW;


	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0;

	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;

	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0;

	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;

	/*禁止访问受保护的寄存器*/
	EDIS;
}


/******************************************************************
 *函数名：INTERRUPT void cpu_timer0_isr(void)
 *参 数：无
 *返回值：无
 *作 用：定时器0中断服务函数
 ******************************************************************/
INTERRUPT void cpu_timer0_isr(void)
{
	/*定时器0中断次数累计*/
	CpuTimer0.InterruptCount++;

	/*检测IIC模块状态为空闲还是写入*/
	char i;

	for(i = 0; i < IIC_NODE_NUM; i++)
	{
		PtrMsg[i]->IIC_TimerOUT = (PtrMsg[i]->MasterStatus == IIC_IDLE) ? 0 : (PtrMsg[i]->IIC_TimerOUT + 1);
	}

	/*通知可以接收第一组中断的所有中断*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
