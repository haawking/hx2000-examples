/*
 * TM1650_IIC.h
 *
 *  Created on: 2021年11月9日
 *      Author: liuq
 */

#ifndef TM1650_IIC_H_
#define TM1650_IIC_H_

#include "dsc_config.h"

/*IIC节点数为2个，分别是TM1650和AT24C02*/
#define IIC_NODE_NUM       2
/*ms为单位*/
#define AT24CO2_TIMER_OUT  3
/*IIC 复位*/
#define IIC_RST_BIT        0X0020

/*S101 S102 S103在TM1650扫描下的返回键码值*/
#define S101_CODE 0x44
#define S102_CODE 0x4C
#define S103_CODE 0x54

extern char keyVal, keyReg, LigntVal;

// Status Flags
#define I2C_AL_BIT          0x0001
#define I2C_NACK_BIT        0x0002
#define I2C_ARDY_BIT        0x0004
#define I2C_RRDY_BIT        0x0008
#define I2C_SCD_BIT         0x0020

// I2C State defines
#define IIC_IDLE               0
#define IIC_WRITE              1
#define IIC_READ_STEP1         2
#define IIC_READ_STEP2         3
#define IIC_READ_STEP3         4
#define IIC_BUS_BUSY           5
#define IIC_BUS_ERROR          6

/*与TM1650有关的物理地址（需要右移动1位进行标准的IIC操作；标准IIC的LSB为读写位，高7位为物理地址位*/
/*显示模式 ADDR*/
#define CMD_SEG  (0X48)>>1
/*按键模式 ADDR 0x24*/
#define CMD_KEY  (0X49)>>1

/*数码管从左到右依次为DIG0,DIG1,DIG2,DIG3*/
#define DIG0  (0X68)>>1
#define DIG1  (0X6A)>>1
#define DIG2  (0X6C)>>1
#define DIG3  (0X6E)>>1

/*SDA采用GPIO32*/
#define SDAGPIO GPIO32
#define SDAPUD  GpioCtrlRegs.GPBPUD.bit.SDAGPIO
#define SDAMUX  GpioCtrlRegs.GPBMUX1.bit.SDAGPIO
#define SDADIR  GpioCtrlRegs.GPBDIR.bit.SDAGPIO
#define SDAQSEL1 GpioCtrlRegs.GPBQSEL1.bit.SDAGPIO
#define SDA_H()  GpioDataRegs.GPBSET.bit.SDAGPIO=1
#define SDA_L()  GpioDataRegs.GPBCLEAR.bit.SDAGPIO =1

/*CLK采用GPIO33*/
#define CLKGPIO GPIO33
#define CLKPUD  GpioCtrlRegs.GPBPUD.bit.CLKGPIO
#define CLKMUX  GpioCtrlRegs.GPBMUX1.bit.CLKGPIO
#define CLKDIR  GpioCtrlRegs.GPBDIR.bit.CLKGPIO
#define CLKQSEL1 GpioCtrlRegs.GPBQSEL1.bit.CLKGPIO
#define CLK_H()  GpioDataRegs.GPBSET.bit.CLKGPIO=1
#define CLK_L()  GpioDataRegs.GPBCLEAR.bit.CLKGPIO =1

// I2C Message Structure
typedef struct I2CSlaveMSG
{
	/*中断源，测试用*/
	Uint16 IntSRC[10];

	//中断源记录，测试用*/
	Uint16 IntSRCCnt;

	/*自行软件定义的IIC状态*/
	Uint16 MasterStatus;

	/*IIC物理地址（硬件地址）*/
	Uint16 SlavePHYAddress;

	/*类似于EEPROM，需要提供逻辑地址*/
	Uint16 LogicAddr;

	/*操作数据的长度（不含物理地址）*/
	Uint16 Len;

	/*发送数组，最大4个深度*/
	Uint16 MsgInBuffer[4];

	/*接收数组，最大4个深度*/
	Uint16 MsgOutBuffer[4];

	/*软件定义的超时变量，在1ms定时器中计数*/
	Uint16 IIC_TimerOUT;
} I2CSlaveMSG;

extern I2CSlaveMSG *PtrMsg[];

extern I2CSlaveMSG TM1650;
extern char *IIC_ISR_String[];
extern uint8_t SEG7Table[];

void InitI2C_Gpio(void);
void I2CA_Init(void);
void softResetIIC_BUS(void);
Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg);
Uint16 I2CA_Rxdata_STOP(struct I2CSlaveMSG *msg);
void TM1650_Send(char addr, char data);
void TM1650_Read(char addr, char *data);
//接收应答中断
INTERRUPT void i2c_int1a_isr(void);
void InitLED(void);
//定时器中断，用于控制数码管刷新显示周期
INTERRUPT void cpu_timer0_isr(void);

//数码管刷新显示与控制
void TM1650_display(void);
void TM1650_control(void);

#endif /* TM1650_IIC_H_ */
