# HX2000_Examples

#### 介绍
HX2000系列芯片示例程序，独立更新及发布，适用Haawking IDE版本信息请查阅示例程序的说明文档。

芯片选型：http://haawking.com/hx2000

开发（板）套件：http://haawking.com/kfb

#### 软件架构
目录结构：芯片系列-->芯片型号-->开发(板)套件
```
--HX2000_Examples
    --DSC28027
        --HXS320F28027BBB（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027BBB）
            --Core_DSC28027（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027BBB/Core_DSC28027）
            --Start_DSC28027（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027BBB/Start_DSC28027）
            --AioneMotor_DSC28027_L3F（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027BBB/AioneMotor_DSC28027_L3F）
        --HXS320F28027ECD（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027ECD）
            --Core_DSC28027（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027ECD/Core_DSC28027）
            --Start_DSC28027（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027ECD/Start_DSC28027）
            --AioneMotor_DSC28027_L3F（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28027/HXS320F28027ECD/AioneMotor_DSC28027_L3F）
    --DSC28034
        --HXS320F28034BBB（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034BBB）
            --Core_DSC28034（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034BBB/Core_DSC28034）
            --Start_DSC28034（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034BBB/Start_DSC28034）
            --AioneMotor_DSC28034_L3F（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034BBB/AioneMotor_DSC28034_L3F）
        --HXS320F28034CDD(https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034CDD)
            --Core_DSC28034(https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034CDD/Core_DSC28034)
            --Start_DSC28034(https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034CDD/Start_DSC28034)
            --AioneMotor_DSC28034_L3F(https://gitee.com/haawking/hx2000-examples/tree/master/DSC28034/HXS320F28034CDD/AioneMotor_DSC28034_L3F)
    --DSC28035
        --HXS320F28035ECB（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28035/HXS320F28035ECB）
            --Core_DSC28035（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28035/HXS320F28035ECB/Core_DSC28035）
		--HXS320F28035ECC（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28035/HXS320F28035ECC）
            --Core_DSC28035（https://gitee.com/haawking/hx2000-examples/tree/master/DSC28035/HXS320F28035ECC/Core_DSC28035）
    --DSC280103P
        --HXS320F280103PECD(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280103P/HXS320F280103PECD)
            --Core_DSC280103P(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280103P/HXS320F280103PECD/Core_DSC280103P)
    --DSC280025C
        --HXS320F280025CEDB(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280025C/HXS320F280025CEDB)
            --Core_DSC280025C(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280025C/HXS320F280025CEDB/Core_DSC280025C)
            --LaunchBoard_DSC280025C(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280025C/HXS320F280025CEDB/LaunchBoard_DSC280025C)
    --DSC280049C
        --HXS320F280049CEDB(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280049C/HXS320F280049CEDB)
            --Core_DSC280049C(https://gitee.com/haawking/hx2000-examples/tree/master/DSC280049C/HXS320F280049CEDB/Core_DSC280049C)
```
#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
