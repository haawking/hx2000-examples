/******************************************************************
 文 档 名：       HX_DSC280049_CMPSS_digital_filter
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     CMPSS数字滤波器配置
 -------------------------- 例程使用说明 --------------------------
 功能描述：   CMPSS数字滤波器配置
                     本例使能CMPSS1 COMEP比较器，
                     并通过数字滤波器将输出馈送到GPIO14/OUTPUTXBAR3引脚。

CMPIN1P用于提供正输入，内部DAC配置为提供负输入。内部 DAC 配置为 VDD/2

 当向CMPIN1P提供低输入（VSS）时，：
    - GPIO14 输出为低
为CMPIN1P提供高输入（高于VDD/2）时：
    - GPIO14 输出为高


 外部连接：
      -在CMPIN1P上提供输入（引脚与ADCINB6共享）
      -可以使用示波器在 GPIO14 上观察输出

调试 ：无

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚并使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 板级初始化
    // - 配置 GPIO14 输出CTRIPOUT1H（通过 XBAROUTPUT3 路由）
    // - 将输出 X-BAR 设置为在 OUTPUTXBAR3 上输出 CTRIPOUTH
    // - 配置 CMPSS1 的高比较器
    // - 使能CMPSS并将负输入信号配置为来自DAC
    // - 使用VDDA作为DAC的基准，并将DAC值设置为中点作为任意值参考。
    // - 配置数字滤波器。在此示例中，时钟预分频、采样窗口大小和阈值将使用最大值。
    // - 初始化过滤逻辑并开始过滤
    // - 配置输出信号。CTRIPH 和 CTRIPOUTH 都将由滤波器输出馈送
    //

    Board_init();

    //
    // 启用全局中断 （INTM） 和实时中断 （DBGM）
    //
    EINT;
    ERTM;

    //
    // 循环
    //
    while(1)
    {

    }
}

