/******************************************************************
 文 档 名：       HX_DSC280049_SCI_interrupts.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：
 D S P：          DSC280049
 使 用 库：
 作     用：      SCI中断回显数据
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：SCI-A通过中断方式发送回显接收到的数据，
 	 	 	 	 芯片主频160MHz。
				波特率：9600
				数据位：8
				校验位：无
				停止位：1
				硬件流控：无
外部接线：连接开发板USB，找到串口号。
				GPIO28-SCIA_RX/eCAP1与SCI串口通讯发送TX，
				GPIO29-SCI_TX与SCI串口通讯接收RX相连

现象：通过串口调试助手，任意发送一组数据，并通过串口返回数据
			通过counter，可以查看发送的字符数量
 版 本：      V1.0.0
 时 间：      2023年6月20日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"


//
// 全局变量
//
uint16_t counter = 0;
unsigned char *msg;


//
// 函数声明
//
__interrupt void sciaTxISR(void);
__interrupt void sciaRxISR(void);


int main(void)
{
		//
	    // 配置 PLL， 禁用看门狗，使能外设时钟。
	    //


	Device_init();
    	//
        // 解锁引脚锁定.
        //
        Device_initGPIO();

        //
        // GPIO28 SCI 接收
        //
        GPIO_setMasterCore(DEVICE_GPIO_PIN_SCIRXDA, GPIO_CORE_CPU1);
        GPIO_setPinConfig(DEVICE_GPIO_CFG_SCIRXDA);
        GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_DIR_MODE_IN);
        GPIO_setPadConfig(DEVICE_GPIO_PIN_SCIRXDA, GPIO_PIN_TYPE_STD);
        GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_QUAL_ASYNC);

        //
        // GPIO29  SCI 发送
        //
        GPIO_setMasterCore(DEVICE_GPIO_PIN_SCITXDA, GPIO_CORE_CPU1);
        GPIO_setPinConfig(DEVICE_GPIO_CFG_SCITXDA);
        GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_DIR_MODE_OUT);
        GPIO_setPadConfig(DEVICE_GPIO_PIN_SCITXDA, GPIO_PIN_TYPE_STD);
        GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_QUAL_ASYNC);

        //
        // 禁用全局中断
        //
        DINT;

        //
        // 初始化中断控制和向量表.
        //
        Interrupt_initModule();
        Interrupt_initVectorTable();
        IER = 0x0000;
        IFR = 0x0000;

        //
        // 中断指向中断函数
        //
        Interrupt_register(INT_SCIA_TX, sciaTxISR);
        Interrupt_register(INT_SCIA_RX, sciaRxISR);

        //
        // 初始化 SCIA 和 FIFO.
        //
        SCI_performSoftwareReset(SCIA_BASE);

        //
        // 配置 SCIA
        //
        SCI_setConfig(SCIA_BASE, 40000000, 9600, (SCI_CONFIG_WLEN_8 |
                                                 SCI_CONFIG_STOP_ONE |
                                                 SCI_CONFIG_PAR_NONE));
        SCI_resetChannels(SCIA_BASE);
        SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXRDY | SCI_INT_RXRDY_BRKDT);
        SCI_enableModule(SCIA_BASE);
        SCI_performSoftwareReset(SCIA_BASE);

        //
        // 使能 TXRDY 和 RXRDY 中断.
        //
        SCI_enableInterrupt(SCIA_BASE, SCI_INT_TXRDY | SCI_INT_RXRDY_BRKDT);

    #ifdef AUTOBAUD
        //
        // 执行自动波特锁定。
        // SCI 接收 'a' 或者 'A' 锁定波特率.
        //
        SCI_lockAutobaud(SCIA_BASE);
    #endif

        //
        // 开始发送信息
        //
        msg = "\r\n\n\nHello World!\0";
        SCI_writeCharArray(SCIA_BASE, (uint8_t*)msg, 16);
        msg = "\r\nYou will enter a character, and the DSP will echo it back!\n\0";
        SCI_writeCharArray(SCIA_BASE, (uint8_t*)msg, 61);

        //
        // 清除 SCI 中断
        //
        SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXRDY | SCI_INT_RXRDY_BRKDT);

        //
        // 使能发送接收中断，PIE: Group 9 interrupts 1 & 2.
        // 清除中断ACK。
        //
        Interrupt_enable(INT_SCIA_RX);
        Interrupt_enable(INT_SCIA_TX);
        Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

        //
        // 使能全局中断
        //
        EINT;

        while(1)
        	;
        return 0;
}



//发送中断函数
// sciaTxISR -
//禁用  TXRDY 中断
// 打印信息.
//
__interrupt void
sciaTxISR(void)
{
    //
    // Disable the TXRDY interrupt.
    //
    SCI_disableInterrupt(SCIA_BASE, SCI_INT_TXRDY);

    msg = "\r\nEnter a character: ";
    SCI_writeCharArray(SCIA_BASE, (uint8_t*)msg, 21);

    //
    // Ackowledge the PIE interrupt.
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}

//接收中断函数
// sciaRxISR
//从RXBUF读取字符并回显
//
__interrupt void
sciaRxISR(void)
{
    uint16_t receivedChar;

    //
    // 使能 TXRDY 中断.
    //
    SCI_enableInterrupt(SCIA_BASE, SCI_INT_TXRDY);

    //
    // 从RXBUF读取字符
    //
    receivedChar = SCI_readCharBlockingNonFIFO(SCIA_BASE);

    //
    // 回显字符
    //
    msg = "  You sent: ";
    SCI_writeCharArray(SCIA_BASE, (uint8_t*)msg, 12);
    SCI_writeCharBlockingNonFIFO(SCIA_BASE, receivedChar);

    //
    // 清除中断ACK.
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

    counter++;
}

//
// End of File
//

