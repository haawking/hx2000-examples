/******************************************************************
 文 档 名：      HX_DSC280049_ADC_adc_soc_continuous
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：        ADC连续自触发
 -------------------------- 例程使用说明 --------------------------
功能描述：ADC连续自触发。
          本例将ADC设置为连续转换，从而实现最大采样率。

外部连接：A0 连接到 ADC转换通道。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  adcAResults - 从引脚A0开始连续AD转换，采样间隔时间是根据ADC速度实现的最小值。


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Defines
//
#define RESULTS_BUFFER_SIZE     32 //数据存储buffer，大小必须是16的倍数


//
// Globals
//
uint16_t adcAResults[RESULTS_BUFFER_SIZE];
uint16_t resultsIndex;

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 板级初始化
    // - 配置ADC并开启
    // - 设置ADC为0通道连续转换
    //
    Board_init();

    //
    // 初始化数据
    //
    for(resultsIndex = 0; resultsIndex < RESULTS_BUFFER_SIZE; resultsIndex++)
    {
        adcAResults[resultsIndex] = 0;
    }
    resultsIndex = 0;

    //
    //启用全局中断和更高优先级的实时调试事件。
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 在循环中转换
    //
    do
       {
           //
           // 使能ADC中断
           //
           ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER1);
           ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER2);
           ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER3);
           ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER4);

           //
           // 清空所有中断标志位
           //
           HWREG(myADC0_BASE + ADC_O_INTFLGCLR) = 0x000F;

           //
           // 初始化数据通道
           //
           resultsIndex = 0;

           //
           // SOC0-SOC7 软件强制启动
           //
           HWREG(myADC0_BASE + ADC_O_SOCFRC1) = 0x00FF;

           //
           // 继续取样，直到结果缓冲区已满
           //
           while(resultsIndex < RESULTS_BUFFER_SIZE)
           {
               //
               // 等待第一组 8 次转换完成
               //
               while( false == ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER3));

               //
               // 清空中断标志位
               //
               ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER3);

               //
               // 存储第一组8组数据结果
               //
               // 请注意，在此期间，第二组8个数据已经被EOC6->ADCIN1触发，
               // 并将在保存前 8 个结果时主动转换
               //
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER0);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER1);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER2);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER3);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER4);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER5);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER6);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER7);

               //
               //等待第二组8个数据完成转换
               //
               while( false ==  ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER4) );

               //
               // 清空中断标志位
               //
               ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER4);

               //
               // 存储第二组8个数据
               //
               // 请注意，在此期间，第一组8个数据已经被EOC14->ADCIN2触发，
               // 将在保存前 8 个结果时主动转换
               //
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER8);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER9);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER10);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER11);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER12);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER13);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER14);
               adcAResults[resultsIndex++] = ADC_readResult(ADCARESULT_BASE,
                                                            ADC_SOC_NUMBER15);
           }

           //
           // 禁用所有 ADCINT 标志以停止采样
           //
           ADC_disableInterrupt(myADC0_BASE, ADC_INT_NUMBER1);
           ADC_disableInterrupt(myADC0_BASE, ADC_INT_NUMBER2);
           ADC_disableInterrupt(myADC0_BASE, ADC_INT_NUMBER3);
           ADC_disableInterrupt(myADC0_BASE, ADC_INT_NUMBER4);

           //
           // 在这，adcAResults[] 包含来自所选通道的一系列转换数据
           //

           //
           // 软件断点，再次点击运行以获得更新的转换数据
           //
           ESTOP0;
       }
    while(1); // Loop forever
}

//
// End of file
//
