/*#############################################################################*/
/*                                                                             */
/* $Copyright:                                                                 */
/* Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd                 */
/* http://www.haawking.com/ All rights reserved.                               */
/*                                                                             */
/* Redistribution and use in source and binary forms, with or without          */
/* modification, are permitted provided that the following conditions          */
/* are met:                                                                    */
/*                                                                             */
/*   Redistributions of source code must retain the above copyright            */
/*   notice, this list of conditions and the following disclaimer.             */
/*                                                                             */
/*   Redistributions in binary form must reproduce the above copyright         */
/*   notice, this list of conditions and the following disclaimer in the       */
/*   documentation and/or other materials provided with the                    */
/*   distribution.                                                             */
/*                                                                             */
/*   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of  */
/*   its contributors may be used to endorse or promote products derived       */
/*   from this software without specific prior written permission.             */
/*                                                                             */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS         */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT           */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR       */
/* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT            */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY       */
/* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT         */
/* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE       */
/* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.        */
/*                                                                             */
/*#############################################################################*/
/*                                                                             */
/* Release for HXS320F280049CEDB, Flash Linker, 1.0.0                     */
/*                                                                             */
/* Release time: 2023-06-19 13:16:55.263222                                    */
/*                                                                             */
/*#############################################################################*/

OUTPUT_ARCH( "riscv" )
ENTRY(_start)
INCLUDE "Peripheral.ld" 
INCLUDE "BootROM.ld" 

MEMORY
{
    RAMM0M1(rwx) :          ORIGIN = 0x000, LENGTH = 0x1000             /* RAM M0M1*/  
    RAMLS0To7(rwx) :        ORIGIN = 0x10000, LENGTH = 0x10000          /* RAM LS0~7 */
                            
    RAMGS0(rwx) :           ORIGIN = 0x200000, LENGTH = 0x2000          /* RAM GS0 */
    RAMGS1(rwx) :           ORIGIN = 0x208000, LENGTH = 0x2000          /* RAM GS1 */
    RAMGS2(rwx) :           ORIGIN = 0x210000, LENGTH = 0x2000          /* RAM GS2 */
    RAMGS3(rwx) :           ORIGIN = 0x218000, LENGTH = 0x2000          /* RAM GS3 */ 
    
    BEGIN (rwx)      :      ORIGIN = 0x600000, LENGTH = 0x10
    FLASH_Bank0(rwx) :      ORIGIN = 0x600010, LENGTH = 0x3FFF0         /* FLASH_Bank0 */ 
    FLASH_Bank1(rwx) :      ORIGIN = 0x680000, LENGTH = 0x40000         /* FLASH_Bank1 */
    
    IER_REGISTER_FILE(rw) : ORIGIN  = 0x100C10, LENGTH = 0x4
    IFR_REGISTER_FILE(rw) : ORIGIN  = 0x100D10, LENGTH = 0x4
}

/*----------------------------------------------------------------------*/
/* Sections                                                             */
/*----------------------------------------------------------------------*/

SECTIONS
{
    . = 0x0;
    M0M1 : 
    {
        __global_sp$ = . + 0xFF8;
    } > RAMM0M1
   
    /* data segment */
    . = 0x00010000;
    .sdata : AT( __flash_start )  
    {
        __data_start = .;
        __global_pointer$ = . + 0x800;
        *(.data)  *(.data.*)
        *(.srodata.cst16) *(.srodata.cst8) *(.srodata.cst4) *(.srodata.cst2) *(.srodata*)
        *(.sdata .sdata.* .gnu.linkonce.s.*)
        *(ramfuncs)
        *(ramconsts)
        *(.scratchpad)  FILL(0x400)
        __data_end = ALIGN(0x10) ;
    }  > RAMLS0To7
    
    /* bss segment */
    .sbss  ALIGN(0x10) : {
        __bss_start = .;
        *(.sbss .sbss.* .gnu.linkonce.sb.*)
        *(.scommon)
    }  > RAMLS0To7
    .bss  ALIGN(0x10)  : { *(.bss)   *(.bss.*)  *(COMMON)   __bss_end = ALIGN(0x10);} > RAMLS0To7

    . = 0x00200000;
    ramgs0 : AT(__gs0_flash_start)
    {
        __gs0_data_start = .;
        *(ramgs0)
        __gs0_data_end = ALIGN(0x10);
    } > RAMGS0

    . = 0x00208000;
    ramgs1 :AT(__gs1_flash_start)
    {
        __gs1_data_start = .;
        *(ramgs1)
        __gs1_data_end = ALIGN(0x10);
    } > RAMGS1

    . = 0x100C10;
    .IER_REGISTER(NOLOAD) : {*(.ier_register)} > IER_REGISTER_FILE
    . = 0x100D10;
    .IFR_REGISTER(NOLOAD) : {*(.ifr_register)} > IFR_REGISTER_FILE
    
    . = 0x00600000;
    .codestart : 
    { 
        *(codestart) 
    } > BEGIN
    
    /* text: test code section */
    . = 0x600010;
    .text.init  : { *(.text.init)  }  >  FLASH_Bank0
    .text  ALIGN(0x10) : { *(.text)   *(.IQmath.*)  *(.text*)  *(.rodata .rodata.*) } > FLASH_Bank0
    .FLASH_DATA_START   ALIGN(0x10) :{ __flash_start   =  . ; } > FLASH_Bank0 
    .FLASH_DATA_SECTION ALIGN(0x10) : { __gs0_flash_start =  __flash_start + ( __data_end - __data_start ) ; } > FLASH_Bank0 
    .FLASH_GS0_SECTION  ALIGN(0x10) : { __gs1_flash_start = __gs0_flash_start + (__gs0_data_end - __gs0_data_start ) ; } > FLASH_Bank0 
    .FLASH_GS1_SECTION  ALIGN(0x10) : { __gs1_flash_end = __gs1_flash_start + (__gs1_data_end - __gs1_data_start ) ; } > FLASH_Bank0 
  
    /* End of uninitalized data segement */
    _end = .;
}

