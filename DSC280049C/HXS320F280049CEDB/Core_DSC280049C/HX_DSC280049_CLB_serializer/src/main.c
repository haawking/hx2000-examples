/******************************************************************
 文 档 名：      HX_DSC280049_CLB_serializer
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB_serializer
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB_serializer
           在本例中，CLB COUNTER在串行器模式下用作移位寄存器。此模块仅适用于 CLB 类型 2
           及更高版本


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);

__interrupt void clb1ISR(void);

#define GPREG_0_EVENT_DATA_SHIFT  0U
#define GPREG_1_MODE0_EN_SHIFT    1U

int main(void)
{
    uint32_t counterValue = 0;

    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);
    Interrupt_enable(INT_CLB1);

    PinMux_init();
    CLB_init();

    //
    // 串行器模式
    // Match1 EQUAL 将在 0b110101 处生成中断
    // Match2 TAP Select 位启用
    // Match 2 在 BIT6 的上升沿生成中断
    // 通过 GPREG 控制的 MODE0 和 EVENT
    //
    initTILE1(CLB1_BASE);

    CLB_enableCLB(CLB1_BASE);

    EINT;
    ERTM;

    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // 现在我们将转换新的值
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    //
    // The value is now 0b1
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    //
    // The value is now 0b11
    //

    //
    // Write 0 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    //
    // The value is now 0b110
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    //
    // The value is now 0b1101
    //

    //
    // Write 0 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    //
    // The value is now 0b11010
    //

    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0b11010)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // 移位成功!
    //
//    ESTOP0;
    GPIO_togglePin(0);

    //
    // match 1 设置为 0b110101，应产生中断
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    DEVICE_DELAY_US(1000);
//    ESTOP0;

    //
    // Match 2 TAP SELECT for bit 6, 0b"1"101011 should get an interrupt
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    DEVICE_DELAY_US(1000);
//    ESTOP0;

    for(;;)
    {

    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);
	GPIO_setPinConfig(GPIO_1_GPIO1);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(1, GPIO_DIR_MODE_OUT);
	GPIO_setPinConfig(GPIO_2_GPIO2);
	GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(2, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(2, GPIO_DIR_MODE_OUT);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_enableSynchronization(CLB1_BASE, CLB_IN0);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_enableSynchronization(CLB1_BASE, CLB_IN1);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_RISING_EDGE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_disableCLB(CLB1_BASE);
}

__interrupt void clb1ISR(void)
{
    uint32_t intTag = CLB_getInterruptTag(CLB1_BASE);

    if (intTag == 1)
    {
        //
        // Match 1 值等于
        //
//        ESTOP0;
        GPIO_togglePin(1);
    }
    else if (intTag == 2)
    {
        //
        // Match 2 TAP Select 位设置
        //
//        ESTOP0;
        GPIO_togglePin(2);
    }

    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
//
// End of File
//

