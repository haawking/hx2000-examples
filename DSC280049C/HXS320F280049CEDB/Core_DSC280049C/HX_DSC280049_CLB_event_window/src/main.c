/******************************************************************
 文 档 名：       HX_280049_CLB_event_window
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB 事件窗口
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB 事件窗口
                      本示例使用负载均衡的 counter、FSM 和 HLC 子模块实现事件定时功能，用于检测
                      中断服务程序响应中断时间是否过长。该示例将四个 PWM 模块配置为在向上计数模
                      式下运行，并在定时器零匹配事件上生成从低到高的边沿。零匹配事件还会触发 PWM
           ISR，在本例中，该 ISR 包含可变长度的虚拟有效负载。在 ISR 结束时，将对 CLB
           GP 寄存器执行写入操作，以指示 ISR 已结束。


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);
void EPWM_init(void);

__interrupt void epwm1ISR(void);
__interrupt void epwm2ISR(void);
__interrupt void epwm3ISR(void);
__interrupt void epwm4ISR(void);
__interrupt void clb1ISR(void);
__interrupt void clb2ISR(void);
__interrupt void clb3ISR(void);
__interrupt void clb4ISR(void);

#define EPWM1_TIMER_TBPRD  20000U
#define EPWM1_CMPA          1933U
#define EPWM1_CMPB          1933U

#define EPWM2_TIMER_TBPRD  25000U
#define EPWM2_CMPA          1933U
#define EPWM2_CMPB          1933U

#define EPWM3_TIMER_TBPRD  30000U
#define EPWM3_CMPA          1933U
#define EPWM3_CMPB          1933U

#define EPWM4_TIMER_TBPRD  35000U
#define EPWM4_CMPA          1933U
#define EPWM4_CMPB          1933U

// default interrupt payloads
#define ISR1_PAYLOAD         36U
#define ISR2_PAYLOAD         36U
#define ISR3_PAYLOAD         36U
#define ISR4_PAYLOAD         36U

uint32_t gpreg1 = 4;
uint32_t gpreg2 = 4;
uint32_t gpreg3 = 4;
uint32_t gpreg4 = 4;

uint32_t payload_1 = ISR1_PAYLOAD;
uint32_t payload_2 = ISR2_PAYLOAD;
uint32_t payload_3 = ISR3_PAYLOAD;
uint32_t payload_4 = ISR4_PAYLOAD;

volatile uint16_t i = 0;
bool first_trip_1 = false;
bool first_trip_2 = false;
bool first_trip_3 = false;
bool first_trip_4 = false;


int main(void)
{
    Device_init();
    Device_initGPIO();
    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_EPWM1, &epwm1ISR);
    Interrupt_register(INT_EPWM2, &epwm2ISR);
    Interrupt_register(INT_EPWM3, &epwm3ISR);
    Interrupt_register(INT_EPWM4, &epwm4ISR);
    Interrupt_register(INT_CLB1, &clb1ISR);
    Interrupt_register(INT_CLB2, &clb2ISR);
    Interrupt_register(INT_CLB3, &clb3ISR);
    Interrupt_register(INT_CLB4, &clb4ISR);

    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EPWM_init();

    SysCtl_enablePeripheral((SysCtl_PeripheralPCLOCKCR)0x0011);
    SysCtl_enablePeripheral((SysCtl_PeripheralPCLOCKCR)0x0111);
    SysCtl_enablePeripheral((SysCtl_PeripheralPCLOCKCR)0x0211);
    SysCtl_enablePeripheral((SysCtl_PeripheralPCLOCKCR)0x0311);

    PinMux_init();
    CLB_init();

    initTILE1(CLB1_BASE);
    initTILE2(CLB2_BASE);
    initTILE3(CLB3_BASE);
    initTILE4(CLB4_BASE);

    Interrupt_enable(INT_EPWM1);
    Interrupt_enable(INT_EPWM2);
    Interrupt_enable(INT_EPWM3);
    Interrupt_enable(INT_EPWM4);
    Interrupt_enable(INT_CLB1);
    Interrupt_enable(INT_CLB2);
    Interrupt_enable(INT_CLB3);
    Interrupt_enable(INT_CLB4);
    EINT;
    ERTM;

    // enable CLB counters
    HWREG(CLB1_BASE + CLB_LOGICCTL) = 0x6;
    HWREG(CLB2_BASE + CLB_LOGICCTL) = 0x6;
    HWREG(CLB3_BASE + CLB_LOGICCTL) = 0x6;
    HWREG(CLB4_BASE + CLB_LOGICCTL) = 0x6;
    CLB_setGPREG(CLB1_BASE, gpreg1);
    CLB_setGPREG(CLB2_BASE, gpreg2);
    CLB_setGPREG(CLB3_BASE, gpreg3);
    CLB_setGPREG(CLB4_BASE, gpreg4);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    for(;;)
    {

    }
}

//--- interrupt service routines ---

__interrupt void epwm1ISR(void)
{
    EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
    // pulse GPREG bit 2 to indicate ISR finished
    CLB_setGPREG(CLB1_BASE, gpreg1 | 0x00000002);
   // ISR payload
    for (i=0; i<payload_1; i++)
    {
        asm(" NOP");
    }


    asm(" NOP");
    CLB_setGPREG(CLB1_BASE, gpreg1);
}


__interrupt void epwm2ISR(void)
{
    EPWM_clearEventTriggerInterruptFlag(EPWM2_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
    // pulse GPREG bit 2 to indicate ISR finished
    CLB_setGPREG(CLB2_BASE, gpreg2 | 0x00000002);
    // ISR payload
    for (i=0; i<payload_2; i++)
    {
        asm(" NOP");
    }


    asm(" NOP");
    CLB_setGPREG(CLB2_BASE, gpreg2);
}


__interrupt void epwm3ISR(void)
{
    EPWM_clearEventTriggerInterruptFlag(EPWM3_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
    // pulse GPREG bit 2 to indicate ISR finished
    CLB_setGPREG(CLB3_BASE, gpreg3 | 0x00000002);
    // ISR payload
    for (i=0; i<payload_3; i++)
    {
        asm(" NOP");
    }


    asm(" NOP");
    CLB_setGPREG(CLB3_BASE, gpreg3);
}


__interrupt void epwm4ISR(void)
{
    EPWM_clearEventTriggerInterruptFlag(EPWM4_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
    // pulse GPREG bit 2 to indicate ISR finished
    CLB_setGPREG(CLB4_BASE, gpreg4 | 0x00000002);
    // ISR payload
    for (i=0; i<payload_4; i++)
    {
        asm(" NOP");
    }


    asm(" NOP");
    CLB_setGPREG(CLB4_BASE, gpreg4);
}


__interrupt void clb1ISR(void)
{
    if (!first_trip_1)
    {
        first_trip_1 = true;
    }
    else
    {
        ESTOP0;
    }

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}


__interrupt void clb2ISR(void)
{
    if (!first_trip_2)
    {
        first_trip_2 = true;
    }
    else
    {
    	ESTOP0;
    }

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}


__interrupt void clb3ISR(void)
{
    if (!first_trip_3)
    {
        first_trip_3 = true;
    }
    else
    {
    	ESTOP0;
    }

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}


__interrupt void clb4ISR(void)
{
    if (!first_trip_4)
    {
        first_trip_4 = true;
    }
    else
    {
    	ESTOP0;
    }

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_1_EPWM1_B);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_2_EPWM2_A);
	GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(2, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_3_EPWM2_B);
	GPIO_setPadConfig(3, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(3, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_4_EPWM3_A);
	GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(4, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_5_EPWM3_B);
	GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(5, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_6_EPWM4_A);
	GPIO_setPadConfig(6, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(6, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_7_EPWM4_B);
	GPIO_setPadConfig(7, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(7, GPIO_QUAL_SYNC);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);

	CLB_setOutputMask(CLB2_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB2_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB2_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM2A);
	CLB_configGPInputMux(CLB2_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB2_BASE, CLB_IN0, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB2_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB2_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB2_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB2_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB2_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB2_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB2_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB2_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB2_BASE,0);
	CLB_enableCLB(CLB2_BASE);

	CLB_setOutputMask(CLB3_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB3_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB3_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM3A);
	CLB_configGPInputMux(CLB3_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB3_BASE, CLB_IN0, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB3_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB3_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB3_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB3_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB3_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB3_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB3_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB3_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB3_BASE,0);
	CLB_enableCLB(CLB3_BASE);

	CLB_setOutputMask(CLB4_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB4_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB4_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM4A);
	CLB_configGPInputMux(CLB4_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB4_BASE, CLB_IN0, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB4_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB4_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB4_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB4_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB4_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB4_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB4_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB4_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB4_BASE,0);
	CLB_enableCLB(CLB4_BASE);
}

void EPWM_init(void)
{
    EPWM_setTimeBasePeriod(EPWM1_BASE, EPWM1_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM1_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0U);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setSyncOutPulseMode(EPWM1_BASE, EPWM_SYNC_OUT_PULSE_ON_COUNTER_ZERO);
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM1_CMPA);
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_B, EPWM1_CMPB);
    EPWM_disablePhaseShiftLoad(EPWM1_BASE);
    EPWM_setClockPrescaler(EPWM1_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);

    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);

    EPWM_setInterruptSource(EPWM1_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM1_BASE);
    EPWM_setInterruptEventCount(EPWM1_BASE, 1U);

    EPWM_setTimeBasePeriod(EPWM2_BASE, EPWM2_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM2_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM2_BASE, 0U);
    EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_disablePhaseShiftLoad(EPWM2_BASE);
    EPWM_setPhaseShift(EPWM2_BASE, 0U);
    EPWM_setCounterCompareValue(EPWM2_BASE, EPWM_COUNTER_COMPARE_A, EPWM2_CMPA);
    EPWM_setCounterCompareValue(EPWM2_BASE, EPWM_COUNTER_COMPARE_B, EPWM2_CMPB);
    EPWM_setClockPrescaler(EPWM2_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);

    EPWM_setCounterCompareShadowLoadMode(EPWM2_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM2_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setActionQualifierAction(EPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);

    EPWM_setInterruptSource(EPWM2_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM2_BASE);
    EPWM_setInterruptEventCount(EPWM2_BASE, 1U);

    EPWM_setTimeBasePeriod(EPWM3_BASE, EPWM3_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM3_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM3_BASE, 0U);
    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_disablePhaseShiftLoad(EPWM3_BASE);
    EPWM_setPhaseShift(EPWM3_BASE, 0U);
    EPWM_setCounterCompareValue(EPWM3_BASE, EPWM_COUNTER_COMPARE_A, EPWM3_CMPA);
    EPWM_setCounterCompareValue(EPWM3_BASE, EPWM_COUNTER_COMPARE_B, EPWM3_CMPB);
    EPWM_setClockPrescaler(EPWM3_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);

    EPWM_setCounterCompareShadowLoadMode(EPWM3_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM3_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setActionQualifierAction(EPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);

    EPWM_setInterruptSource(EPWM3_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM3_BASE);
    EPWM_setInterruptEventCount(EPWM3_BASE, 1U);

    EPWM_setTimeBasePeriod(EPWM4_BASE, EPWM4_TIMER_TBPRD);
    EPWM_setPhaseShift(EPWM4_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM4_BASE, 0U);
    EPWM_setTimeBaseCounterMode(EPWM4_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_disablePhaseShiftLoad(EPWM4_BASE);
    EPWM_setPhaseShift(EPWM4_BASE, 0U);
    EPWM_setCounterCompareValue(EPWM4_BASE, EPWM_COUNTER_COMPARE_A, EPWM4_CMPA);
    EPWM_setCounterCompareValue(EPWM4_BASE, EPWM_COUNTER_COMPARE_B, EPWM4_CMPB);
    EPWM_setClockPrescaler(EPWM4_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);

    EPWM_setCounterCompareShadowLoadMode(EPWM4_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(EPWM4_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setActionQualifierAction(EPWM4_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM4_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM4_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM4_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);

    EPWM_setInterruptSource(EPWM4_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM4_BASE);
    EPWM_setInterruptEventCount(EPWM4_BASE, 1U);
}
//
// End of File
//

