/******************************************************************
 文 档 名：      HX_DSC280049_CLB_lfsr
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB_lfsr
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB_lfsr
           在本例中，CLB COUNTER模块在线性反馈移位寄存器（LFSR）模式下使用。此模块仅适用
           于 CLB 类型 2 及更高版本。


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);

//
// Shift register GP REG bits
//
#define GPREG_0_EVENT_DATA_SHIFT  0U
#define GPREG_1_MODE0_EN_SHIFT    1U

int main(void)
{
    uint32_t counterValue = 0;

    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    PinMux_init();
    CLB_init();

    //
    // LFSR Mode
    // Poly = MATCH2 = 0x0000FFFF
    // MODE1 = 0
    //
    initTILE1(CLB1_BASE);

    CLB_enableCLB(CLB1_BASE);

    EINT;
    ERTM;

    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // Match2 (多项式将切换前 16 位。值应为0x0000FFFF)
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));


    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0x0000FFFF)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // 如果 DATA 为 0，则下一个值应为0x10001
    //

    //
    // Write 0 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (0U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0x00010001)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // 如果 DATA 为 1，则下一个值应为0x20002
    //

    //
    // Write 1 to DATA
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (0U << GPREG_1_MODE0_EN_SHIFT));
    //
    // Write 1 to EN
    //
    CLB_setGPREG(CLB1_BASE, (1U << GPREG_0_EVENT_DATA_SHIFT) | (1U << GPREG_1_MODE0_EN_SHIFT));

    counterValue = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    if (counterValue != 0x00020002)
    {
        //
        // Error
        //
        ESTOP0;
    }

    //
    // 移位成功!
    //
//    ESTOP0;
    GPIO_togglePin(0);

    for(;;)
    {

    }

}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_enableSynchronization(CLB1_BASE, CLB_IN0);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_enableSynchronization(CLB1_BASE, CLB_IN1);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_RISING_EDGE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_disableCLB(CLB1_BASE);
}
//
// End of File
//

