/******************************************************************
 文 档 名：       HX_DSC280049_BUFFDAC_enable.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 	 	 在buffer DAC 输出产生一个电压，
         DACOUTA/ADCINA0 使用默认的VDAC参考设置。

 外部接线：
  	  	  当DAC参考设置为VDAC时，外部参考电压必须应用于VDAC引脚。
  	  	  这可以通过连接3.3伏至ADCINB3的跨接导线。

 现象：


 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

uint16_t dacVal = 2048;


int main(void)
{
    Device_init();


    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Board initialization
    // Configure DAC - Setup the reference voltage and output value for the DAC
    //
    Board_init();

    //
    // Continuously set the DAC output value
    //
    while(1)
    {
        DAC_setShadowValue(myDAC0_BASE, dacVal);
        DEVICE_DELAY_US(1600);
    }
    return 0;
}

//
// End of File
//

