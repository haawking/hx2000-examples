/******************************************************************
 文 档 名：       HX_DSC280049_DAC_bufferdac_random
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     DAC buffer 随机输出示例
 -------------------------- 例程使用说明 --------------------------
 功能描述：  DAC buffer 随机输出示例
                     此示例在 DACOUTA/ADCINA0的 DAC buffer上生成随机电压，
                     并使用VDAC的默认DAC基准设置

 外部连接：当DAC基准电压设置为VDAC时，必须向VDAC引脚施加一个外部基准电压。
                    这可以通过将 3.3V 的跳线连接到 ADCINB3 来实现。

调试 ：无

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"



//
// Globals
//
uint16_t adcvalue;
uint16_t lowLimit = 410;
uint16_t highLimit = 3686;
uint16_t value[256];
//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 初始化随机时间种子
    //

  //  srand(time(NULL));

    // 配置DAC模块
    // 配置DAC-设置 DAC 的基准电压和输出值
    //
    Board_init();


    //
    // 根据随机值连续设置DAC输出值
    //
    while(1)
    {
    	   adcvalue++;

    	       if(adcvalue>3686)
    	       {

    	    	   adcvalue=410;
    	       }

        DAC_setShadowValue(myDAC0_BASE,value[adcvalue] );
        DEVICE_DELAY_US(1);
    }
}

//
// End of File
//

// End of File
//

