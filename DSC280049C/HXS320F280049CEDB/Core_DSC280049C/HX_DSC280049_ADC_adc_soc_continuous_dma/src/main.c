/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_soc_continuous_dma
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：        ADC通过DMA连续转换
 -------------------------- 例程使用说明 --------------------------
功能描述： ADC通过DMA连续转换。
                   此示例设置两个ADC通道同时进行转换。并将结果将由 DMA 传输到 RAM 中的缓冲区中。


外部连接：A3 & C3连接到 ADC转换通道。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  myADC0DataBuffer - 引脚A3的数字量
      myADC1DataBuffer - 引脚C3的数字量
 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Function Prototypes
//
__interrupt void dmach1ISR(void);

void configureEPWM(uint32_t epwmBase);
void initializeDMA(void);
void configureDMAChannels(void);

//
// Defines
//
#define RESULTS_BUFFER_SIZE     16 //存储转换数据的buffer，必须是16的倍数


//
// Globals
//
//#pragma DATA_SECTION(myADC0DataBuffer, "ramgs0");
//#pragma DATA_SECTION(myADC1DataBuffer, "ramgs0");
CODE_SECTION("ramgs0") uint16_t myADC0DataBuffer[RESULTS_BUFFER_SIZE];
CODE_SECTION("ramgs0") uint16_t myADC1DataBuffer[RESULTS_BUFFER_SIZE];
volatile uint16_t done;

int main(void)
{
    uint16_t resultsIndex;

    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 板级初始化
    // - 配置ADCA和ADCC并开启
    // - 设置ADC通道A3,C3为连续转换
    // - 设置ADCA INT1的中断服务函数 - 在第一次转换后发生
    // - 使能指定的 PIE&CPU中断
    //
    Board_init();

    //
    // 设置这个例程的中断服务函数
    // DMA 通道1的中断服务函数 - DMA传输完成后产生
    //
    Interrupt_register(INT_DMA_CH1, &dmach1ISR);

    //
    // 使能指定的 PIE&CPU中断
    //
    Interrupt_enable(INT_DMA_CH1);

    //
    // 停止 ePWM时钟
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 调用 ePWM2的函数
    //
    configureEPWM(EPWM2_BASE);

    //
    // 打开 ePWM 时钟
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 初始化DMA ,配置DMA 通道1&2
    //
    initializeDMA();
    configureDMAChannels();

    //
    // 初始化结果buffer
    //
    for(resultsIndex = 0; resultsIndex < RESULTS_BUFFER_SIZE; resultsIndex++)
    {
        myADC0DataBuffer[resultsIndex] = 0;
        myADC1DataBuffer[resultsIndex] = 0;
    }

    //
    // 清空所有挂起的中断标志
    //
    DMA_clearTriggerFlag(DMA_CH1_BASE);   // DMA channel 1
    DMA_clearTriggerFlag(DMA_CH2_BASE);   // DMA channel 2
    HWREG(myADC0_BASE + ADC_O_INTFLGCLR) = 0x3U; // ADCA
    HWREG(myADC1_BASE + ADC_O_INTFLGCLR) = 0x3U; // ADCC
    EPWM_forceADCTriggerEventCountInit(EPWM2_BASE, EPWM_SOC_A); // EPWM2 SOCA
    EPWM_clearADCTriggerFlag(EPWM2_BASE, EPWM_SOC_A);    // EPWM2 SOCA

    //
    // 通过将最后一个 SOC 设置为重新触发来启用连续操作
    // the first
    //
    ADC_setInterruptSOCTrigger(myADC0_BASE, ADC_SOC_NUMBER0,    // ADCA
                               ADC_INT_SOC_TRIGGER_ADCINT2);
    ADC_setInterruptSOCTrigger(myADC1_BASE, ADC_SOC_NUMBER0,    // ADCC
                               ADC_INT_SOC_TRIGGER_ADCINT2);

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 开始DMA Start DMA
    //
    done = 0;
    DMA_startChannel(DMA_CH1_BASE);
    DMA_startChannel(DMA_CH2_BASE);

    //
    // 最后，使能SOCA 的 ePWM触发，这将在下一次 ePWM 活动中启动转换。
    //
    EPWM_enableADCTrigger(EPWM2_BASE, EPWM_SOC_A);

    //
    // 循环直到 ISR 发出传输完成的信号
    //
    while(done == 0)
    {
        __asm(" NOP");
    }
    ESTOP0;
}

//
// adcA1ISR - 这是在第一次转换后调用的，并将禁用 ePWM SOC 以避免重新触发问题。
//

__interrupt void adcA1ISR(void)
{
    //
    // 移除 ePWM 触发
    //
    EPWM_disableADCTrigger(EPWM2_BASE, EPWM_SOC_A);

    //
    // 禁止此中断再次发生
    //
    Interrupt_disable(INT_ADCA1);

    //
    // 接收中断
    //
    Interrupt_clearACKGroup(INT_myADC0_1_INTERRUPT_ACK_GROUP);
}

//
// dmach1ISR - 在 DMA 传输结束时调用，通过从最后一个 SOC 中删除第一个 SOC 的触发器来停止转换。
//

__interrupt void dmach1ISR(void)
{
    //
    // 通过移除SOC0的触发器来停止ADC
    //
    ADC_setInterruptSOCTrigger(myADC0_BASE, ADC_SOC_NUMBER0,
                               ADC_INT_SOC_TRIGGER_NONE);
    ADC_setInterruptSOCTrigger(myADC1_BASE, ADC_SOC_NUMBER0,
                               ADC_INT_SOC_TRIGGER_NONE);
    done = 1;

    //
    // 接收中断
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
}

//
// configureEPWM - 设置 ePWM2 模块，使 A 输出的周期为 40us，占空比为 50%。
// SOCA信号与该信号的上升沿重合
//
void configureEPWM(uint32_t epwmBase)
{
    //
    // 设置定时器向上计数，周期为40us
    //
    HWREG(epwmBase + EPWM_O_TBCTL) = 0x0000U;
    EPWM_setTimeBasePeriod(epwmBase, 4000U);

    //
    // 设置A输出在零时为高在CMPA时为低
    //
    EPWM_setActionQualifierAction(epwmBase, EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(epwmBase, EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);

    //
    // 设置CMPA为20us以获得50%的占空比
    //
    EPWM_setCounterCompareValue(epwmBase, EPWM_COUNTER_COMPARE_A, 2000U);

    //
    // 开启ADC当定时器等于0时。
    //
    EPWM_setADCTriggerSource(epwmBase, EPWM_SOC_A, EPWM_SOC_TBCTR_ZERO);
    EPWM_setADCTriggerEventPrescale(epwmBase, EPWM_SOC_A, 1U);

    //
    // 启用 SOCA 事件计数器的初始化。由于我们禁用了 ETSEL.SOCAEN 位，
    // 我们需要一种方法来重置 SOCACNT。因此，启用计数器初始化控件。
    //
    EPWM_enableADCTriggerEventCountInit(epwmBase, EPWM_SOC_A);
}

//
// 初始化DMA - 初始化DMA 通过硬件复位
//
void initializeDMA(void)
{
    //
    // 在DMA 上硬件复位
    //
    DMA_initController();

    //
    // 允许 DMA 在仿真挂起时自由运行
    //
    DMA_setEmulationMode(DMA_EMULATION_FREE_RUN);
}

//
// 配置DMA通道 - 初始化 DMA 通道 1 以传输 ADCA结果,
//             初始化 DMA 通道 2 以传输 ADCB结果
//
//
void configureDMAChannels(void)
{
    //
    // 为 ADCA 设置 DMA 通道 1
    //
    DMA_configAddresses(DMA_CH1_BASE, (uint16_t *)&myADC0DataBuffer,
                        (uint16_t *)ADCARESULT_BASE);

    //
    // 执行完整的 16 字突发以填充结果缓冲区。数据将一次传输 32 位，因此地址步骤如下
    //
//    DMA_configBurst(DMA_CH1_BASE, 16, 2, 2);
    DMA_configBurst(DMA_CH1_BASE, 16, 2, 1);
    DMA_configTransfer(DMA_CH1_BASE, (RESULTS_BUFFER_SIZE >> 4), -14, 2);//srcStep为-14是因为16个结果寄存器，以32bit的大小进行传输，最后一次传输地址指针指向ADCRESULT14，所以在开始新的burst传输时需要将地址指针减去14用以使指针指向ADCRESULT0，

    //destStep为2以32bit的大小进行传输，最后一次传输时地址指针指向addr1.14，每次传输16个16bit的结果，所以需要使addr1.14+2 = addr2.0 从而开始新的16个16bit结果的存储
    DMA_configMode(DMA_CH1_BASE, DMA_TRIGGER_ADCA2,
                   (DMA_CFG_ONESHOT_DISABLE | DMA_CFG_CONTINUOUS_DISABLE |
                		   DMA_CFG_SIZE_16BIT));
//                    DMA_CFG_SIZE_32BIT));

    DMA_enableTrigger(DMA_CH1_BASE);
    DMA_disableOverrunInterrupt(DMA_CH1_BASE);
    DMA_setInterruptMode(DMA_CH1_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH1_BASE);

    //
    // 为ADCC设置为DMA通道2
    //
    DMA_configAddresses(DMA_CH2_BASE, (uint16_t *)&myADC1DataBuffer,
                        (uint16_t *)ADCCRESULT_BASE);

    //
    // 执行完整的 16 字突发以填充结果缓冲区。数据将一次传输 32 位，因此地址步骤如下
    //
//    DMA_configBurst(DMA_CH2_BASE, 16, 2, 2);
    DMA_configBurst(DMA_CH2_BASE, 16, 2, 1);
    DMA_configTransfer(DMA_CH2_BASE, (RESULTS_BUFFER_SIZE >> 4), -14, 2);
    DMA_configMode(DMA_CH2_BASE, DMA_TRIGGER_ADCA2,
                   (DMA_CFG_ONESHOT_DISABLE | DMA_CFG_CONTINUOUS_DISABLE |
                		   DMA_CFG_SIZE_16BIT));
//                    DMA_CFG_SIZE_32BIT));

    DMA_enableTrigger(DMA_CH2_BASE);
    DMA_disableOverrunInterrupt(DMA_CH2_BASE);
    DMA_setInterruptMode(DMA_CH2_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH2_BASE);
}

//
// End of file
//
