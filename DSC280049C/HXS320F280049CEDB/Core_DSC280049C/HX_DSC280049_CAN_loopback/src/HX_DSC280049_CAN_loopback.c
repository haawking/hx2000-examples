/******************************************************************
 文 档 名：       HX_DSC280049_CAN_loopback.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 	 	 此示例显示了CAN的基本设置，以便发送和接收CAN总线上的消息。
 	 	 外设CAN配置为传输带有特定CAN ID的消息。然后，使用一个简单的延迟环路进行定时。
 	 	 发送的消息是包含递增模式的2字节消息。
		本例将CAN控制器设置为外部环回测试模式。
		传输的数据在CANTXA引脚上可见，并在内部接收回到CAN核心。

 外部接线：


 现象：在Expressions或Variables或者LiveView窗口观察变量
		msgCount 成功接收到消息的计数器
		txMsgData	发送的数据
		rxMsgData	接收到的数据

 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

#define MSG_DATA_LENGTH    2

volatile unsigned long msgCount = 0;

uint16_t txMsgData[2], rxMsgData[2];

int main(void)
{
	uint16_t i;

    Device_init();

    //
        // Initialize GPIO and configure GPIO pins for CANTX/CANRX
        //
        Device_initGPIO();

        //
        // Board initialization
        //
        Board_init();

        //
        // Initialize PIE and clear PIE registers. Disables CPU interrupts.
        //
        Interrupt_initModule();

        //
        // Initialize the PIE vector table with pointers to the shell Interrupt
        // Service Routines (ISR).
        //
        Interrupt_initVectorTable();

        //
        // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
        //
        EINT;


        //
        // Setup send and receive buffers
        //
        txMsgData[0] = 0x01;
        txMsgData[1] = 0x02;
        *(uint16_t *)rxMsgData = 0;

    while(1)
    {
          //
          // Send CAN message data from message object 1
          //
          CAN_sendMessage(myCAN0_BASE, 1, MSG_DATA_LENGTH, txMsgData);

          //
          // Delay before receiving the data
          //
          for(i=0;i<500;i++)
        	  DEVICE_DELAY_US(1000);

          //
          // Read CAN message object 2 and check for new data
          //
          if (CAN_readMessage(myCAN0_BASE, 2, rxMsgData))
          {
              //
              // Check that received data matches sent data.
              // Device will halt here during debug if data doesn't match.
              //
              if((txMsgData[0] != rxMsgData[0]) ||
                 (txMsgData[1] != rxMsgData[1]))
              {
                  ESTOP0;
              }
              else
              {
                  //
                  // Increment message received counter
                  //
                  msgCount++;
              }
          }
          else
          {
              //
              // Device will halt here during debug if no new data was received.
              //
              ESTOP0;
          }

          //
          // Increment the value in the transmitted message data.
          //
          txMsgData[0] += 0x01;
          txMsgData[1] += 0x01;

          //
          // Reset data if exceeds a byte
          //
          if(txMsgData[0] > 0xFF)
          {
              txMsgData[0] = 0;
          }
          if(txMsgData[1] > 0xFF)
          {
              txMsgData[1] = 0;
          }
      }
    return 0;
}

//
// End of File
//

