 /******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_ppb_limits
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       使用 f280049 的后处理模块进行 ADC 限值检查
 -------------------------- 例程使用说明 --------------------------
功能描述：    使用 f280049 的后处理模块进行 ADC限值检查。
                      此示例将ePWM设置为定期触发ADC。如果结果超出定义的范围，则后处理模块
                      将生成中断。当 VREFHI 设置为 3.3V 时，默认限制为 1000LSB 和 3000LSB。
                      如果输入电压高于约2.4V或低于约0.8V，PPB将产生中断。

外部连接：A0连接到 ADC转换通道。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
            无
 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint32_t intStatus;
//
// Functional Prototypes
//
void configureEPWM(uint32_t epwmBase);
void adcAEvtISR(void);

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉.
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    //  板级初始化
    // - 配置ADCs并上电
    // - 配置ADC通道0为ePWM触发转换。
    // - 配置ADC PPB限值。C
    //      如果转换值高于或低于限制，SOC0 将产生中断。
    //
    Board_init();

    //
    // 配置ePWM
    //
    configureEPWM(EPWM1_BASE);

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 开启ePWM
    // 使能PWM 的同步和时钟。
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 使能SOCA触发
    //
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    //
    // 开启 epwm 计数器以进行计数。
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // 再循环中转换。
    //
    do
    {
        //
        // 等待ePWM导致ADC转换。如果ADC转换超过PPB限值，则会产生中断。
        // 用户可以根据应用要求在空闲环路或ADCINT ISR中读取ADC结果。
        //
    }
    while(1);
}

//
// configureEPWM - 设置ePWM的SOC和比较值。
void configureEPWM(uint32_t epwmBase)
{
    //
    // 禁止SOCA触发
    //
    EPWM_disableADCTrigger(epwmBase, EPWM_SOC_A);

    //
    // 在CMPA向上计数时触发SOCA。
    //
    EPWM_setADCTriggerSource(epwmBase, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);

    //
    // 在第一个事件上产生脉冲。
    //
    EPWM_setADCTriggerEventPrescale(epwmBase, EPWM_SOC_A, 1U);

    //
    // 设置A 比较值为2048。
    //
    EPWM_setCounterCompareValue(epwmBase, EPWM_COUNTER_COMPARE_A, 2048U);

    //
    // 设置周期为4096。
    //
    EPWM_setTimeBasePeriod(epwmBase, 4096U);

    //
    // 冻结计数器。
    //
    EPWM_setTimeBaseCounterMode(epwmBase, EPWM_COUNTER_MODE_STOP_FREEZE);
}

//
// adcAEvtISR - ADCA PPB 中断服务函数。ISR for ADCA PPB
//
 void adcAEvtISR(void)
{
    //
    // 注意，你超出了PPB 的限值。
    //
    intStatus = ADC_getPPBEventStatus(myADC0_BASE, ADC_PPB_NUMBER1);
    if((intStatus & ADC_EVT_TRIPHI) != 0U)
    {
        //
        // 电压超过上限。
        //
      //  asm("   ESTOP0");

        //
        // 清除跳闸标志位并继续。
        //
        ADC_clearPPBEventStatus(myADC0_BASE, ADC_PPB_NUMBER1, ADC_EVT_TRIPHI);
    }
    if((intStatus & ADC_EVT_TRIPLO) != 0U)
    {
        //
        //电压超过下限。
        //
       // asm("   ESTOP0");

        //
        //清除跳闸标志位并继续。 Clear the trip flag and continue
        //
        ADC_clearPPBEventStatus(myADC0_BASE, ADC_PPB_NUMBER1, ADC_EVT_TRIPLO);
    }
    Interrupt_clearACKGroup(INT_myADC0_EVT_INTERRUPT_ACK_GROUP);
}

//
// End of file
//
