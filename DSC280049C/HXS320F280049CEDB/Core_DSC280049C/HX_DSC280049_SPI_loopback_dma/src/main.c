/******************************************************************
 文 档 名：      HX_DSC280049_loopback_dma
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     带DMA的SPI数字环回
 -------------------------- 例程使用说明 --------------------------
 功能描述：  带DMA的SPI数字环回
                     该程序使用SPI模块的内部环回测试模式。DMA中断和SPI FIFO均使用。
                     当SPI发送FIFO有足够的空间（如其FIFO电平中断信号所示）时，
           DMA会将数据从全局变量sData传输到FIFO。这将通过内部环回传输到接收FIFO。

将所有数据放入 rData 后，将在其中一个 DMA 通道的 ISR 中检查数据的有效性。

 外部连接：
   无

调试 ：在Variables或  Live View中观察变量
   sData - 发送数据
   rData - 接收数据

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

#define SPI0_TX_DMA_ADDRESS ((uint16_t *)(SPIA_BASE + SPI_O_TXBUF))
#define SPI0_RX_DMA_ADDRESS ((uint16_t *)(SPIA_BASE + SPI_O_RXBUF))
//
// Place buffers in GSRAM
//
CODE_SECTION("ramgs0") uint16_t sData[128];                // Send data buffer
CODE_SECTION("ramgs0") uint16_t rData[128];                // Receive data buffer

const void *destAddr = (const void *)rData;
const void *srcAddr = (const void *)sData;

volatile uint16_t done = 0;         // Flag to set when all data transferred

void Pinmux_init(void);
void SPI_init(void);
void DMA_init(void);
//
// Function Prototypes
//
__interrupt void INT_SPI0_RX_DMA_ISR(void);
__interrupt void INT_SPI0_TX_DMA_ISR(void);

int main(void)
{
    uint16_t i;

    Device_init();
    Device_initGPIO();
    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_DMA_CH5, &INT_SPI0_TX_DMA_ISR);
    Interrupt_register(INT_DMA_CH6, &INT_SPI0_RX_DMA_ISR);

    Pinmux_init();
    DMA_init();
    SPI_init();

    for(i = 0; i < 105; i++)
    {
        sData[i] = i;
        rData[i]= 0;
    }

    Interrupt_enable(INT_DMA_CH5);
    Interrupt_enable(INT_DMA_CH6);

    EINT;
    ERTM;

    DMA_startChannel(DMA_CH5_BASE);
    DMA_startChannel(DMA_CH6_BASE);

    while(!done);
    ESTOP0;
    return 0;
}

//
// DMA Channel 6 ISR
//
__interrupt void INT_SPI0_RX_DMA_ISR(void)
{
    DMA_stopChannel(DMA_CH6_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
    return;
}

//
// DMA Channel 5 ISR
//
 __interrupt void INT_SPI0_TX_DMA_ISR(void)
{
    uint16_t i;

    DMA_stopChannel(DMA_CH5_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);

    //
    // Check for data integrity
    //
    for(i = 0; i < 105; i++)
    {
        if (rData[i] != i)
        {
            // Something went wrong. rData doesn't contain expected data.
        	asm("NOP");
        }
        else
        {
            GPIO_togglePin(0);
        }
    }

    done = 1;
    return;
}

void Pinmux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

 	GPIO_setPinConfig(GPIO_16_SPIA_SIMO);
 	GPIO_setPadConfig(16, GPIO_PIN_TYPE_STD);
 	GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC);

 	GPIO_setPinConfig(GPIO_10_SPIA_SOMI);
 	GPIO_setPadConfig(10, GPIO_PIN_TYPE_STD);
 	GPIO_setQualificationMode(10, GPIO_QUAL_ASYNC);

 	GPIO_setPinConfig(GPIO_18_SPIA_CLK);
 	GPIO_setPadConfig(18, GPIO_PIN_TYPE_STD);
 	GPIO_setQualificationMode(18, GPIO_QUAL_ASYNC);

 	GPIO_setPinConfig(GPIO_11_SPIA_STE);
 	GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
 	GPIO_setQualificationMode(11, GPIO_QUAL_ASYNC);
}

void SPI_init(void)
{
	SPI_disableModule(SPIA_BASE);
	SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0, SPI_MODE_MASTER, 800000, 16);
	SPI_setSTESignalPolarity(SPIA_BASE, SPI_STE_ACTIVE_LOW);
	SPI_enableFIFO(SPIA_BASE);
	SPI_setFIFOInterruptLevel(SPIA_BASE, SPI_FIFO_TX8, SPI_FIFO_RX8);
	SPI_enableLoopback(SPIA_BASE);
	SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_STOP_MIDWAY);
	HWREG(SPIA_BASE + SPI_O_CTL) |= 0x20;
	SPI_enableModule(SPIA_BASE);
}

void DMA_init(void)
{
    DMA_setEmulationMode(DMA_EMULATION_STOP);
    DMA_configAddresses(DMA_CH5_BASE, SPI0_TX_DMA_ADDRESS, srcAddr);
    DMA_configBurst(DMA_CH5_BASE, 8U, 1, 0);
    DMA_configTransfer(DMA_CH5_BASE, 16U, 1, 0);
    DMA_configWrap(DMA_CH5_BASE, 65535U, 0, 65535U, 0);
    DMA_configMode(DMA_CH5_BASE, DMA_TRIGGER_SPIATX, DMA_CFG_ONESHOT_DISABLE | DMA_CFG_CONTINUOUS_DISABLE | DMA_CFG_SIZE_16BIT);
    DMA_setInterruptMode(DMA_CH5_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH5_BASE);
    DMA_disableOverrunInterrupt(DMA_CH5_BASE);
    DMA_enableTrigger(DMA_CH5_BASE);
    DMA_stopChannel(DMA_CH5_BASE);

    DMA_setEmulationMode(DMA_EMULATION_STOP);
    DMA_configAddresses(DMA_CH6_BASE, destAddr, SPI0_RX_DMA_ADDRESS);
    DMA_configBurst(DMA_CH6_BASE, 8U, 0, 1);
    DMA_configTransfer(DMA_CH6_BASE, 16U, 0, 1);
    DMA_configWrap(DMA_CH6_BASE, 65535U, 0, 65535U, 0);
    DMA_configMode(DMA_CH6_BASE, DMA_TRIGGER_SPIARX, DMA_CFG_ONESHOT_DISABLE | DMA_CFG_CONTINUOUS_DISABLE | DMA_CFG_SIZE_16BIT);
    DMA_setInterruptMode(DMA_CH6_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH6_BASE);
    DMA_disableOverrunInterrupt(DMA_CH6_BASE);
    DMA_enableTrigger(DMA_CH6_BASE);
    DMA_stopChannel(DMA_CH6_BASE);
}
//
// End of File
//

