//###########################################################################
//
// FILE:   hw_types.h
//
// TITLE:  Type definitions used in driverlib functions.
//
//###########################################################################
// Release for HXS320F280049CEDB, Hal DriverLib, 1.0.0
//
// Release time: 2023-06-15 09:57:14.891718
//
//#############################################################################

#ifndef HW_TYPES_H
#define HW_TYPES_H

#include "hx_rv32_type.h"

//*****************************************************************************
//
// Macros for hardware access
//
//*****************************************************************************

#define CODE_SECTION(v) __attribute__((section(v)))

#define __byte(x,y)  (((uint8_t *)(x))[y])

#define HWREG(x)                                                              \
        (*((volatile uint32_t *)((uintptr_t)(x))))
#define HWREG_BP(x)                                                           \
        (*((volatile uint32_t *)((uintptr_t)(x))))



//*****************************************************************************
//
// SUCCESS and FAILURE for API return value
//
//*****************************************************************************
#define STATUS_S_SUCCESS    (0)
#define STATUS_E_FAILURE    (-1)

//*****************************************************************************
//
// Definition of 8 bit types for USB Driver code to maintain portability
// between byte and word addressable cores of C2000 Devices.
//
//*****************************************************************************
//typedef uint16_t uint8_t;
//typedef int16_t int8_t;

//****************************************************************************
//
// For checking NULL pointers
//
//****************************************************************************
#ifndef NULL
#define NULL ((void *)0x0)
#endif

//*****************************************************************************
//
// 32-bit and 64-bit float type
//
//*****************************************************************************
typedef float         float32_t;
typedef double        float64_t;

//*****************************************************************************
//
// Extern compiler intrinsic prototypes. See compiler User's Guide for details.
// These are provided to satisfy static analysis tools. The #ifndef is required
// because the '&' is for a C++-style reference, and although it is the correct
// prototype, it will not build in C code.
//
//*****************************************************************************
#if defined(DSC280049_EDB)
#else
extern int16_t &__byte(int16_t *array, uint16_t byte_index);
extern uint32_t &__byte_peripheral_32(uint32_t *x);
#endif

#endif // HW_TYPES_H
