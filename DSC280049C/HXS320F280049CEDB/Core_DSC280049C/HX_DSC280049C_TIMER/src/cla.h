//###########################################################################
//
// FILE:  Cla.h
//
// TITLE: DSP28004x Control Law Accelerator Initialization & Support Functions statements.
//
//###########################################################################
// $HAAWKING Release: DSP28004x Support Library V1.0.0 $
// $Release Date: 2022-10-18 05:22:31 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef _CLA_H_
#define _CLA_H_


//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "CLAmath.h"
//
// Defines
//

//
// Common Functions
//
//
__always_inline void _CLA_MTVEC_INIT(void)
{
    asm volatile(".align 2");
    asm volatile("la t0, _CLA_TRAP_ENTRY": : : "t0");
    asm volatile("csrw mtvec, t0": : : "t0");
}


__always_inline void  _CLA_usDELAY(unsigned int data)
{
       asm volatile( \
                       ".align 2;"\
                       "rpt %[loop], 4;" \
                       "and x0, x0, x0;" \
                       ".align 1;" \
                       : \
                       : [loop]"r"(data)\
                       : );
}

#define _CLA_DELAY_US(A)  _CLA_usDELAY((((long double)A * 1000.0L) / (long double)CPU_RATE) - 15.0L) // range A: 1 ~ 34000
//
//Task 1 (C) Variables
//


//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//


//
// CLA C Tasks
//
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task1();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task2();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task3();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task4();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task5();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task6();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task7();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task8();

#endif /* _CLA_H_ */

//
// End of file
//
