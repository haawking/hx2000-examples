//###########################################################################
//
// FILE:   cpu.h
//
// TITLE:  Useful H28x CPU defines.
//
//###########################################################################

#ifndef CPU_H
#define CPU_H

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

#include "stdint.h"
#include "hx_rv32_type.h"
#include "hx_rv32_dev.h"

//
// External reference to the interrupt flag register (IFR) register
//
extern volatile uint32_t  CODE_SECTION(".ifr_register")  IFR;



//
// External reference to the interrupt enable register (IER) register
//
extern volatile uint32_t  CODE_SECTION(".ier_register")  IER;


//
// Define for emulation stop
//
#ifndef ESTOP0
#define ESTOP0 __asm("ebreak")
#endif

//
// Define for emulation stop
//
#ifndef ESTOP1
#define ESTOP1 __asm("ebreak")
#endif



//
// Define for putting processor into a low-power mode
//
#ifndef _DUAL_HEADERS
#ifndef IDLE
#define IDLE   __asm(" wfi")
#endif
#else
#define IDLE_ASM __asm(" wfi");
#endif

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif // CPU_H
