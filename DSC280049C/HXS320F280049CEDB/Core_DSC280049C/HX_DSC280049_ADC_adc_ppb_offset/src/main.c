/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_ppb_offset
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       使用后处理模块进行ADC失调调整
 -------------------------- 例程使用说明 --------------------------
功能描述：    使用后处理模块进行ADC失调调整。
                      本例软件触发ADC,某些 SOC 具有由后处理模块实现的自动偏移校正。
                      程序运行后，存储器将包含ADC和后处理模块（PPB）结果。


外部连接：A2 C2连接到 ADC转换通道。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  myADC0Result - 引脚A2的数字量
      myADC0PPBResult - 引脚A2减去100 LSB的自动偏移后的数字量
      myADC1Result - 引脚C2的数字量
      myADC1PPBResult - 引脚C2加上100 LSB的自动偏移后的数字量

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t myADC0Result;
uint16_t myADC0PPBResult;
uint16_t myADC1Result;
uint16_t myADC1PPBResult;

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉.
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。.
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 板级初始化
    // - 配置ADCs并上电
    // - 设置ADC以进行软件转换
    // - 设置 PPB 偏移校正
    //      通道 A 上的转换将减去 100
    //      通道 C 上的转换将加上 100
    //
    Board_init();

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 在再循环中转换
    //
    do
    {
        //
        // 转换，等待转换完成并且存储数据
        // ADCA通过软件立即开始转换
        //
        ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER0);
        ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER1);

        //
        // 等待ADCA转换完成，然后接收中断标志
        //
        while(ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1) == false);
        ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);

        //
        // ADCC通过软件立即开始转换
        //
        ADC_forceSOC(myADC1_BASE, ADC_SOC_NUMBER0);
        ADC_forceSOC(myADC1_BASE, ADC_SOC_NUMBER1);

        //
        // 等待ADCC转换完成，然后接收中断标志
        //
        while(ADC_getInterruptStatus(myADC1_BASE, ADC_INT_NUMBER1) == false);
        ADC_clearInterruptStatus(myADC1_BASE, ADC_INT_NUMBER1);

        //
        // 存储数据
        //
        myADC0Result = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        myADC0PPBResult = ADC_readPPBResult(ADCARESULT_BASE, ADC_PPB_NUMBER1);
        myADC1Result = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
        myADC1PPBResult = ADC_readPPBResult(ADCCRESULT_BASE, ADC_PPB_NUMBER1);

        //
        // Software breakpoint, hit run again to get updated conversions
        //
   //     asm("   ESTOP0");
    }
    while(1);
}

//
// End of file
//
