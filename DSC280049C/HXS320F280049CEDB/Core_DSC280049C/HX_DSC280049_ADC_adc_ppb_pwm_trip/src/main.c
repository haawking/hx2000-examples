/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_ppb_pwm_trip
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      ADC限值检查和PWM跳闸（如果输入越界）
 -------------------------- 例程使用说明 --------------------------
功能描述：    ADC限值检查和PWM跳闸（如果输入越界）
                      此示例演示了通过ADC PPB限值检测的EPWM跳闸，
           ADCAINT1配置为定期强制触发ADCA通道2，
                      如果ADC结果超出定义的范围，PPB限值检测将生成ADCxEVTy事件，
                      通过配置 EPWM XBAR 和相应的 EPWM 的跳闸区和数字比较子模块，
                      将此事件配置为 EPWM 跳闸源。
示例展示
    -单次触发
    -周期循环
  -PWM直接跳闸
通过数字比较子模块ADCAEVT1源

默认限制为 0LSB 和 3600LSB。当VREFHI设置为3.3V时，
如果输入电压高于约2.9V，PPB将产生跳闸事件。

外部连接：A3连接到ADC模块。

观察示波器上的以下信号
    - ePWM1(GPIO0 - GPIO1)
    - ePWM2(GPIO2 - GPIO3)
    - ePWM3(GPIO4 - GPIO5)

调试 ：在Expressions或Variables或者LiveView窗口观察变量
      adcA3Results - 引脚A3上的数字量

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "device.h"

//
// Defines
//

// Uncomment to enable profiling
//#define ENABLE_PROFILING
#define RESULTS_BUFFER_SIZE     512U


//
// Globals
//
volatile uint16_t indexA = 0;                // Index into result buffer
volatile uint16_t bufferFull;                // Flag to indicate buffer is full
uint16_t adcA3Results[RESULTS_BUFFER_SIZE];  // ADC result buffer

//
// Characteristics of PWMs to be tripped
// EPWM1 - frequency -> 10kHz, dutyA -> 50%, dutyB -> 50%, ePWM1B - inverted
// EPWM2 - frequency -> 2.5kHz, dutyA -> 20%, dutyB -> 50%, ePWM2B - inverted
// EPWM3 - frequency -> 5kHz, dutyA -> 70%, dutyB -> 50%, ePWM3B - inverted
//
EPWM_SignalParams pwmSignal1 =
            {10000, 0.5f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

EPWM_SignalParams pwmSignal2 =
            {25000, 0.2f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

EPWM_SignalParams pwmSignal3 =
            {5000, 0.7f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

//
// Functional Prototypes
//

//
// ADC配置相关API
//
void configureADC(uint32_t adcBase);
void configureADCSOC(uint32_t adcBase, uint16_t channel);
void configureLimitDetectionPPB(uint16_t soc, uint16_t limitHigh,
                                uint16_t limitLow);

//
// EPWM 配置相关 API
//
void initEPWMGPIO(void);
void configureOSHTripSignal(uint32_t epwmBase);
void configureCBCTripSignal(uint32_t epwmBase);
void configureDirectTripSignal(uint32_t epwmBase);

#ifdef ENABLE_PROFILING
void setupProfileGpio(void);
#endif

//
// ISRs
//
__interrupt void adcA1ISR(void);

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉。
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    //此示例中使用的中断将重新映射到此文件中的 ISR 函数。
    //
    Interrupt_register(INT_ADCA1, &adcA1ISR);

#ifdef ENABLE_PROFILING
    //
    // 设置分析 GPIO
    //
    setupProfileGpio();
#endif

    //
    // 在这种情况下，只需初始化 ePWM1、ePWM2、ePWM3 的 GPIO 引脚
    //
    initEPWMGPIO();

    //
    // 使能ADC上的内部基准电压源
    //
    ADC_setVREF(ADCA_BASE, ADC_REFERENCE_INTERNAL, ADC_REFERENCE_3_3V);
    ADC_setVREF(ADCB_BASE, ADC_REFERENCE_INTERNAL, ADC_REFERENCE_3_3V);
    ADC_setVREF(ADCC_BASE, ADC_REFERENCE_INTERNAL, ADC_REFERENCE_3_3V);

    //
    // 配置ADC并上电
    //
    configureADC(ADCA_BASE);

    //
    // 禁用同步（也将时钟冻结到PWM）
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 配置ePWM模块以获得所需的频率和占空比
    //
    EPWM_configureSignal(EPWM1_BASE, &pwmSignal1);
    EPWM_configureSignal(EPWM2_BASE, &pwmSignal2);
    EPWM_configureSignal(EPWM3_BASE, &pwmSignal3);

    //
    // 将ADCEVTx配置为ePWM1的单次触发跳闸信号
    //
    configureOSHTripSignal(EPWM1_BASE);

    //
    // 将ADCEVTx配置为ePWM2的逐周期跳闸信号
    //
    configureCBCTripSignal(EPWM2_BASE);

    //
    // 将ADCEVTx配置为ePWM3的直接跳闸信号
    //
    configureDirectTripSignal(EPWM3_BASE);

    //
    // 设置ADC通道3为ADCAINT1触发转换
    //
    configureADCSOC(ADCA_BASE, 3U);

    //
    //配置 ADC 后处理块限值 ，如果转换高于或低于限值，SOC0 将产生中断
    //
    configureLimitDetectionPPB(0U, 3600U, 0U);

    //
    // 使能ADC中断
    //
    Interrupt_enable(INT_ADCA1);

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;  // 使能全局中断 INTM
    ERTM;  // 使能全局实时中断 DBGM

    //
    // 开启ePWM:
    // 使能PWM 同步和时钟
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 通过软件触发ADC一次
    //
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);

    //
    // 空循环
    //
    do
    {
    }
    while(1);
}

//
// configureOSHTripSignal - 将ADCAEVT1配置为所需PWM通道实例的单次跳闸信号
//
void configureOSHTripSignal(uint32_t epwmBase)
{
    //
    // 配置跳闸 7 输入由 ADCAEVT1 触发
    //
    XBAR_setEPWMMuxConfig(XBAR_TRIP7, XBAR_EPWM_MUX00_ADCAEVT1);

    //
    // 使能 mux 0
    //
    XBAR_enableEPWMMux(XBAR_TRIP7, XBAR_MUX00);

    //
    // 选择跳闸 7 输入作为直流模块的输入
    //
    EPWM_selectDigitalCompareTripInput(epwmBase, EPWM_DC_TRIP_TRIPIN7,
                                       EPWM_DC_TYPE_DCAH);

    //
    // 当DCAH为高电平时产生DCAEVT1.
    //
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase,EPWM_TZ_DC_OUTPUT_A1,
                                                 EPWM_TZ_EVENT_DCXH_HIGH);

    //
    // DCAEVT1使用未过滤的 DCAEVT1
    //
    EPWM_setDigitalCompareEventSource(epwmBase, EPWM_DC_MODULE_A,
                            EPWM_DC_EVENT_1, EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    //
    // DCAEVT1 是异步的
    //
    EPWM_setDigitalCompareEventSyncMode( epwmBase, EPWM_DC_MODULE_A,
                              EPWM_DC_EVENT_1,EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    //
    // 启用跳闸信号
    //
    EPWM_enableTripZoneSignals(epwmBase, EPWM_TZ_SIGNAL_DCAEVT1);

    //
    // DCAEVT1 的行动
    // 强制 EPWMxA 为低电平，将 EPWMxB 设置为高电平
    //
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_TZA,
                           EPWM_TZ_ACTION_LOW);

    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_TZB,
                           EPWM_TZ_ACTION_HIGH);

    //
    // 在启用中断之前清除任何虚假跳闸区标志
    //
    EPWM_clearTripZoneFlag(epwmBase, EPWM_TZ_FLAG_DCAEVT1);

    EPWM_clearOneShotTripZoneFlag(epwmBase, EPWM_TZ_OST_FLAG_DCAEVT1);

    //
    // 启用 TZ中断
    //
    EPWM_enableTripZoneInterrupt(epwmBase, EPWM_TZ_INTERRUPT_OST);
}

//
// configureCBCTripSignal - 将ADCAEVT1配置为所需PWM实例的逐周期跳闸信号
//
void configureCBCTripSignal(uint32_t epwmBase)
{
    //
    // 配置跳闸 8 输入由 ADCAEVT1 触发
    //
    XBAR_setEPWMMuxConfig(XBAR_TRIP8, XBAR_EPWM_MUX00_ADCAEVT1);

    //
    // 使能 mux 0
    //
    XBAR_enableEPWMMux(XBAR_TRIP8, XBAR_MUX00);

    //
    // 选择跳闸 8 输入作为直流模块的输入
    //
    EPWM_selectDigitalCompareTripInput(epwmBase, EPWM_DC_TRIP_TRIPIN8,
                                       EPWM_DC_TYPE_DCAH);

    //
    // 当DCAH为高电平时产生DCAEVT2.
    //
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase, EPWM_TZ_DC_OUTPUT_A2,
                                                 EPWM_TZ_EVENT_DCXH_HIGH);

    //
    //  DCAEVT2使用未过滤的 DCAEVT2
    //
    EPWM_setDigitalCompareEventSource(epwmBase, EPWM_DC_MODULE_A,
                            EPWM_DC_EVENT_2, EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    //
    // DCAEVT2 是异步的
    //
    EPWM_setDigitalCompareEventSyncMode(epwmBase, EPWM_DC_MODULE_A,
                              EPWM_DC_EVENT_2, EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    //
    // 启用跳闸信号
    //
    EPWM_enableTripZoneSignals(epwmBase, EPWM_TZ_SIGNAL_DCAEVT2);

    //
    // 配置 CBC 清除事件
    //
    EPWM_selectCycleByCycleTripZoneClearEvent(epwmBase,
                                       EPWM_TZ_CBC_PULSE_CLR_CNTR_ZERO_PERIOD);

    //
    // DCAEVT2 的动作
    // 强制 EPWMxA/EPWMxB为低
    //
    EPWM_setTripZoneAction(epwmBase, EPWM_TZ_ACTION_EVENT_TZA,
                           EPWM_TZ_ACTION_LOW);

    EPWM_setTripZoneAction(epwmBase, EPWM_TZ_ACTION_EVENT_TZB,
                           EPWM_TZ_ACTION_HIGH);

    //
    // 在启用中断之前清除任何虚假跳闸区标志
    //
    EPWM_clearTripZoneFlag(epwmBase, EPWM_TZ_FLAG_DCAEVT2);

    EPWM_clearCycleByCycleTripZoneFlag(epwmBase, EPWM_TZ_CBC_FLAG_DCAEVT2);

    //
    // 启用 TZ 中断
    //
    EPWM_enableTripZoneInterrupt(epwmBase, EPWM_TZ_INTERRUPT_CBC);
}

//
// configureDirectTripSignal - 将ADCAEVT1配置为所需PWM实例的直接跳闸信号
//
void configureDirectTripSignal(uint32_t epwmBase)
{
    //
    // 配置跳闸 9 输入由 ADCAEVT1 触发
    //
    XBAR_setEPWMMuxConfig(XBAR_TRIP9, XBAR_EPWM_MUX00_ADCAEVT1);

    //
    // 使能 mux 0
    //
    XBAR_enableEPWMMux(XBAR_TRIP9, XBAR_MUX00);

    //
    // 选择跳闸 9 输入作为直流模块的输入
    //
    EPWM_selectDigitalCompareTripInput(epwmBase, EPWM_DC_TRIP_TRIPIN9,
                                       EPWM_DC_TYPE_DCAH);
    EPWM_selectDigitalCompareTripInput(epwmBase, EPWM_DC_TRIP_TRIPIN9,
                                       EPWM_DC_TYPE_DCBH);

    //
    // 当 DCAH 为高电平时产生DCAEVT1。当DCBH为高电平时产生DCBEVT2
    //
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase, EPWM_TZ_DC_OUTPUT_A1,
                                                 EPWM_TZ_EVENT_DCXH_HIGH);
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase, EPWM_TZ_DC_OUTPUT_B2,
                                                 EPWM_TZ_EVENT_DCXH_HIGH);

    //
    // DCAEVT1/DCBEVT2 使用未过滤的 DCAEVT1/DCBEVT2
    //
    EPWM_setDigitalCompareEventSource(epwmBase, EPWM_DC_MODULE_A,
                            EPWM_DC_EVENT_1, EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);
    EPWM_setDigitalCompareEventSource(epwmBase, EPWM_DC_MODULE_B,
                            EPWM_DC_EVENT_2, EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    //
    // DCAEVT1/ DCBEVT2 是异步的
    //
    EPWM_setDigitalCompareEventSyncMode(epwmBase, EPWM_DC_MODULE_A,
                              EPWM_DC_EVENT_1, EPWM_DC_EVENT_INPUT_NOT_SYNCED);
    EPWM_setDigitalCompareEventSyncMode(epwmBase, EPWM_DC_MODULE_B,
                              EPWM_DC_EVENT_2, EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    //
    // TZA/TZB 的动作
    // 强制 EPWMxA 为低并且强制 EPWMxB 为高
    //
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_DCAEVT1,
                           EPWM_TZ_ACTION_LOW);

    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_DCBEVT2,
                           EPWM_TZ_ACTION_HIGH);

    //
    // 在启用中断之前清除任何虚假跳闸区标志
    //
    EPWM_clearTripZoneFlag(epwmBase, (EPWM_TZ_INTERRUPT_DCAEVT1 |
                                      EPWM_TZ_INTERRUPT_DCBEVT2));

    //
    // 使能 TZ 中断
    EPWM_enableTripZoneInterrupt(epwmBase, (EPWM_TZ_INTERRUPT_DCAEVT1 |
                                            EPWM_TZ_INTERRUPT_DCBEVT2));
}

//
// configureADC - 编写ADC配置，并为ADC A和ADC B的ADC上电
//
void configureADC(uint32_t adcBase)
{

	EALLOW;
		//HWREG(ADCA_BASE + ADC_O_CTL1) &= ~((uint16_t)ADC_CTL1_PGA_DIS) ;		//CLR 0 ENLPGA
		HWREG(ADCA_BASE + ADC_O_CTL1) |=  ((uint16_t)ADC_CTL1_PGA_DIS) ;		//SET 1 DISpga
		EDIS;
    //
    // 设置 ADCCLK 为2分频
    //
    ADC_setPrescaler(adcBase, ADC_CLK_DIV_3_0);


    //
    // 设置脉冲位置
    //
    ADC_setInterruptPulseMode(adcBase, ADC_PULSE_END_OF_CONV);

    //
    // ADC上电然后延时1ms
    //
    ADC_enableConverter(adcBase);

    //
    // 延迟 1ms，使 ADC 有时间上电
    //
    DEVICE_DELAY_US(1000);
}

//
// configureLimitDetectionPPB - 配置 ADCPPB 的上限和下限
//
void configureLimitDetectionPPB(uint16_t soc, uint16_t limitHigh,
                                uint16_t limitLow)
{
    //
    // 将 PPB1 与 SOC 关联
    //
    ADC_setupPPB(ADCA_BASE, ADC_PPB_NUMBER1, (ADC_SOCNumber)soc);

    //
    // 设置上限和下限
    //
    ADC_setPPBTripLimits(ADCA_BASE, ADC_PPB_NUMBER1, limitHigh, limitLow);

    //
    // 启用上限和下限 PPB 事件
    //
    ADC_enablePPBEvent(ADCA_BASE, ADC_PPB_NUMBER1, (ADC_EVT_TRIPHI |
                                                    ADC_EVT_TRIPLO));

    //
    // 配置 CBCEN
    //
    EALLOW;
    HWREG(ADCA_BASE + ADC_O_PPB1CONFIG) |= ADC_PPB2CONFIG_CBCEN;
    EDIS;
}

//
// configureADCSOC - 设置 ADC EPWM 通道和触发设置
//
void configureADCSOC(uint32_t adcBase, uint16_t channel)
{
    uint16_t acqps;

    //
    // 确定最小采样窗口时间 (in SYSCLKS)
    //
    acqps = 8; // 80ns
    //
    // - NOTE: A longer sampling window will be required if the ADC driving
    //   source is less than ideal (an ideal source would be a high bandwidth
    //   op-amp with a small series resistance). See TI application report
    //   SPRACT6 for guidance on ADC driver design.
    //

    //
    // 选择要转换的通道和ADCA转换结束的标志
    //
    ADC_setupSOC(adcBase, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 (ADC_Channel)channel, acqps);

    //
    // 配置 ADCINT1 作为 SOC0 触发源
    //
    ADC_setInterruptSOCTrigger(adcBase, ADC_SOC_NUMBER0,
                               ADC_INT_SOC_TRIGGER_ADCINT1);

    //
    // 启用连续模式
    //
    ADC_enableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // 将源配置为 EOC1，清除并启用中断
    //
    ADC_setInterruptSource(adcBase, ADC_INT_NUMBER1, ADC_SOC_NUMBER0);
    ADC_clearInterruptStatus(adcBase, ADC_INT_NUMBER1);
    ADC_enableInterrupt(adcBase, ADC_INT_NUMBER1);
}

//
// ADC A Interrupt 1 ISR
//
__interrupt void adcA1ISR(void)
{
#ifdef ENABLE_PROFILING
    //
    // 设置性能分析 GPIO12：需要 3 个周期
    //
    HWREG(GPIODATA_BASE  + GPIO_O_GPASET) = 0x1000;
#endif

    //
    // 将最新结果添加到缓冲区
    //
    adcA3Results[indexA++] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);

    //
    // 如果缓冲区已满，则设置 bufferFull 标志
    //
    if(RESULTS_BUFFER_SIZE <= indexA)
    {
        indexA = 0;
    }

    //
    // 确认中断
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);

#ifdef ENABLE_PROFILING
    //
    // 重置性能分析 GPIO12
    //
    HWREG(GPIODATA_BASE  + GPIO_O_GPACLEAR) = 0x1000;
#endif
}

//
// initEPWMGPIO - 配置 ePWM1-ePWM3 GPIO
//
void initEPWMGPIO(void)
{
    //
    // 禁用 GPIO0-5 上的上拉并将其配置为 PWM
    //
    GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_0_EPWM1A);

    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_1_EPWM1B);

    GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_2_EPWM2A);

    GPIO_setPadConfig(3, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_3_EPWM2B);

    GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_4_EPWM3A);

    GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_5_EPWM3B);
}

#ifdef ENABLE_PROFILING
void setupProfileGpio(void)
{
    GPIO_setDirectionMode(12,GPIO_DIR_MODE_OUT);
    GPIO_setQualificationMode(12,GPIO_QUAL_SYNC);
    GPIO_setPinConfig(GPIO_12_GPIO12);
    GPIO_writePin(12,0);
}

#endif
//
// End of file
//

