/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_multiple_soc_epwm
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       ADC ePWM触发多个SOC
 -------------------------- 例程使用说明 --------------------------
功能描述：    ADC ePWM触发多个SOC
                      此示例将 ePWM1 设置为定期触发 ADCA 和 ADCC 上的一组转换。
                      此示例演示了多个ADC协同工作，使用跨多个ADC的可用并行处理一批转换

           ADCA中断ISR用于读取ADCA和ADCC的结果。

外部连接：A0, A1, A2 和and C2, C3, C4 连接到 ADC转换通道。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  adcAResult0 - 引脚A0的数字量
 	  adcAResult1 - 引脚A1的数字量
 	  adcAResult2 - 引脚A2的数字量
 	  adcCResult0 - 引脚C2的数字量
 	  adcCResult1 - 引脚C3的数字量
 	  adcCResult2 - 引脚C4的数字量

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
//
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t adcAResult0;
uint16_t adcAResult1;
uint16_t adcAResult2;
uint16_t adcCResult0;
uint16_t adcCResult1;
uint16_t adcCResult2;

//
// Function Prototypes
//
void initEPWM();
__interrupt void adcA1ISR(void);

//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉.
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 设置ADCA和ADCC并初始化ADC SOC。
    // ADC分辨率 - 12-bit，信号模式 - 单端。
    // ADCA SOC0,SOC1,SOC2配置为转换引脚A0的值。
    // EPWM1SOCA作为A1和A2的SOC触发。
    // ADCC SOC0,SOC1,SOC2配置为转换引脚C2的值。
    // EPWM1SOCA作为C3和 C4的触发。
    //
    Board_init();

    //
    // 配置EPWM1 ADC SOCA 触发。
    //
    initEPWM();

    //
    // 使能ADC中断。
    //
    Interrupt_enable(INT_ADCA1);

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;
    ERTM;

    //
    // 启动 ePWM1，使能 SOCA 并将计数器置于向上计数模式
    //
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // 在循环中转换
    //
    do
    {
        //
        // 等待ePWM触发ADC转换.
        // ADCA1 ISR 处理每组新的转换
        //
    }
    while(1);
}

//
// 配置 ePWM1触发SOC的函数
//
void initEPWM(void)
{
    //
    // 禁止SOCA。
    //
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    //
    // 将 SOC 配置为在第一个向上计数发生触发时间。
    //
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);

    //
    // 设置比较器A的值为1000并且周期为1999。
    // 假设ePWM时钟为100MHz，则采样率为50kHz。
    // 50MHz ePWM 时钟将提供 25kHz 采样率。
    // 也可以通过直接改变ePWM周期来调制采样率（确保比较A值小于周期）。
    //
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 1000);
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);

    //
    // 设置本地ePWM模块时钟分频为1。
    //
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // 冻结定时器。
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}

//
// ADC A Interrupt 1 ISR
//
__interrupt void adcA1ISR(void)
{
    //
    //存储数据 Store results
    //
    adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
    adcAResult2 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2);
    adcCResult0 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
    adcCResult1 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER1);
    adcCResult2 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER2);

    //
    // 清空中断标志位。
    //
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // 检查是否溢出。
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    //
    // 确认中断。
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// End of file
//
