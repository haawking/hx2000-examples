/******************************************************************
 文 档 名：      HX_DSC280049_CLB_cyclic_redundancy_check
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB循环冗余校验
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB循环冗余校验
           在此示例中，CLB 模块用于执行循环冗余校验 （C.R.C.），其中 12 条消息以位为单位，
           并使用 10 个不同的 CRC 多项式进行检查。
           传入的第一个元素是消息长度，第二个元素是存储在 input_data 中的消息。

           此示例仅适用于 CLB 类型 2 及更高版本

           将output_data中的已知值与基于 CLB 的 CRC 计算的预期值进行比较。共验证了 120 条消息，
           匹配消息的数量显示在 passCount 中


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量
      passCount - 生成的 CRC 值和已知 CRC 值之间匹配的消息数
      failCoun  - 未通过 CRC 值验证的消息数

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "clb_config.h"
#include "clb.h"

void CLB_init(void);

#define NUM_MESSAGES       12  // 输入消息数
#define NUM_CRC_POLY       10  // 测试中的 CRC 多项式数
#define BIT_LIMIT (5ul)
//
// 位数限制为 32 位，表示为 << BIT_LIMIT
//
#define CLB_GPREG_RESET (1ul<<1)
#define CLB_GPREG_ASSERT (1ul<<2)
//
typedef struct CRC_DATA
{
    uint16_t crcSize;
    uint32_t crcMask;
    uint32_t crcPoly;
} CRC_DATA;
//
    uint16_t passCount = 0 ;
    uint16_t failCount = 0 ;
//
// 该CRC_data包括十个不同的CRC多项式进行测试
//
struct CRC_DATA myCRC[NUM_CRC_POLY] =
{
    // 0 Parity   x^1 + x^0
    {1u, 0x01ul, 0b11ul},
    // 1 CRC3_GSM x^3 + x^1 + x^0
    {3u, 0x07ul, 0b1011ul},
    // 2 CRC4_ITU x^4 + X^1 + X^0 = BiSS-C 0x13
    {4u, 0x0Ful, 0b10011ul},
    // 3 CRC5_ITU x^5 + x^4 + x^2 + x^0
    {5u, 0x1Ful, 0b110101ul},
    // 4 CRC5_USB x^5 + x^2 + 1 = BiSS-C 0x25
    {5u, 0x1Ful, 0b100101ul},
    // 5 CRC6_GSM x^6 + x^5 + x^3 + x^2 + x^1 + x^0
    {6u, 0x3Ful, 0b1101111ul},
    // 6 CRC6_ITU x^6 + x^1 + x^0 = BiSS-C 0x43
    {6u, 0x3Ful, 0b1000011ul},
    // 7 CRC7  x^7 + x^3 + x^0
    {7u, 0x7Ful, 0b10001001ul},
    // 8 CRC8_AUTOSAR x^8 + x^5 + x^3 + x^2 + x + 1
    {8u, 0xFFul, 0b100101111ul},
    // 9 CRC30 CDMA x^30 + x^29 + x^21 + x^20 + x^15 +
    //    x^13 + x^12 + x^11 + x^8 + x^7 + x^6 + x^2 + x^1 + x0
    {30u, 0x3FFFFFFFul, 0b1100000001100001011100111000111ul}
};

//
// 每个input_data中的第一个元素是消息长度（以位为单位）
// 第二个元素是消息。
//
// 注意：此实现不支持 31 >位计数
//
uint32_t input_data[NUM_MESSAGES][2] =
{
      {1, 0x00000000},  // 0
      {1, 0x00000001},  // 1
      {2, 0x00000002},  // 2
      {8, 0x00000025},  // 3
      {16, 0x00002516},  // 4
      {24, 0x00106612},  // 5
      {18, 0x0002ADCB},  // 6
      {31, 0x251478AD},  // 7
      {23, 0x0003F737},  // 8
      {23, 0x0003FB43},  // 9
      {23, 0x0003FE13},  // 10
      {23, 0x0003FFA3}   // 11
};
//
// 每个多项式的预期 CRC 结果
// 此处的输出将按行表示，Row_1表示所有 12 条消息的第一个多项式
//
uint32_t output_data[NUM_CRC_POLY][NUM_MESSAGES] = {
    {0x0ul, 0x1ul, 0x1ul, 0x1ul, 0x0ul, 0x1ul, 0x1ul, 0x0ul, 0x0ul, 0x0ul, 0x0ul, 0x0ul},               // 0
    {0x0ul, 0x3ul, 0x6ul, 0x6ul, 0x7ul, 0x6ul, 0x2ul, 0x0ul, 0x4ul, 0x0ul, 0x6ul, 0x0ul},               // 1
    {0x0ul, 0x3ul, 0x6ul, 0x5ul, 0xDul, 0x1ul, 0xBul, 0xAul, 0xAul, 0x6ul, 0x2ul, 0xCul},               // 2
    {0x00ul, 0x15ul, 0x1Ful, 0x19ul, 0x09ul, 0x02ul, 0x16ul, 0x14ul, 0x14ul, 0x0Cul, 0x17ul, 0x18ul},   // 3
    {0x00ul, 0x05ul, 0x0aul, 0x00ul, 0x04ul, 0x0Eul, 0x09ul, 0x18ul, 0x19ul, 0x05ul, 0x1Bul, 0x02ul},   // 4
    {0x00ul, 0x2Ful, 0x31ul, 0x25ul, 0x2Dul, 0x1Cul, 0x37ul, 0x30ul, 0x17ul, 0x0aul, 0x36ul, 0x21ul},   // 5
    {0x00ul, 0x03ul, 0x06ul, 0x2Cul, 0x23ul, 0x28ul, 0x3ful, 0x12ul, 0x3Ful, 0x10ul, 0x22ul, 0x2Ful},   // 6
    {0x00ul, 0x09ul, 0x12ul, 0x1Ful, 0x7Aul, 0x73ul, 0x76ul, 0x6Ful, 0x21ul, 0x3Eul, 0x64ul, 0x05ul},   // 7
    {0x00ul, 0x2Ful, 0x5Eul, 0xe0ul, 0x3Aul, 0xE6ul, 0xCBul, 0x21ul, 0x3Ful, 0xE8ul, 0x9Cul, 0x4Bul},   // 8
    {0x00ul, 0x2030b9c7ul, 0x2051ca49ul, 0x24a9d36ful, 0x393897e8ul, 0x3f90cada, 0x269a9339, 0x356b051c,
     0x03f87CEDul, 0x08548079ul, 0x01a89110ul, 0x37066ef0ul  }                                          // 9
};

uint32_t calculate_crc(uint32_t *data, uint16_t bitCount, uint16_t crcSize, uint32_t crcPoly);

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

    CLB_init();

    initTILE1(CLB1_BASE);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);
    CLB_enableCLB(CLB1_BASE);

    uint16_t myPoly;


     for(myPoly = 0; myPoly < NUM_CRC_POLY; myPoly++)
     {
         uint16_t myMessage;

         for(myMessage = 0; myMessage < NUM_MESSAGES; myMessage++ )
         {
             uint16_t bitCount;
             uint32_t crc;

             //
             // CRC verification is done based on bit specified in bitCount
             //
             bitCount = input_data[myMessage][0];

             crc = 0x00;
             crc = calculate_crc(&input_data[myMessage][1], bitCount,
                 myCRC[myPoly].crcSize, myCRC[myPoly].crcPoly);

             crc &= myCRC[myPoly].crcMask;
             if (crc == output_data[myPoly][myMessage])
             {
                 passCount++;
             }
             else
             {
                 failCount++;
             }
         }

     }
 //
 // Passcount displays -  CRC 校验成功的数字
 // Failcount displays — CRC 校验失败的数字
 //
     if( passCount == (NUM_MESSAGES * NUM_CRC_POLY)  && failCount == 0)
     {
         // all passed
    	 GPIO_togglePin(0);
//         asm(" ESTOP0");
     }
     else
     {
         // failures
         //ESTOP0;
     }

    while(1);
    return 0;
}

uint32_t calculate_crc(uint32_t * data, uint16_t bitCount,
            uint16_t crcSize, uint32_t crcPoly)
{
    uint32_t crc;
    uint32_t msg;
    //
    // RESET and STOP (reset = 1, mode0 = 0)
    //
    CLB_setGPREG(CLB1_BASE, (0 << 2 ) | (1 << 0));
    msg = *data;
    //
    // 使用COUNTER_0作为线性反馈移位寄存器（LFSR）来计算输入的CRC
    //
    // 初始化CRC多项式和多项式幂
    //    match2 -> polynomial
    //    match1 -> crc size - 1
    //    mode_1 -> 1
    //

    // CRC发生器
    //
    CLB_writeInterface(CLB1_BASE, CLB_ADDR_COUNTER_0_MATCH1, crcSize - 1);
    CLB_writeInterface(CLB1_BASE, CLB_ADDR_COUNTER_0_MATCH2, crcPoly);

    //
    // 序列化数据 - 将数据馈送到 CRC 生成器输入
    //
    CLB_configCounterTapSelects(CLB1_BASE, (bitCount) << BIT_LIMIT);
    CLB_writeInterface(CLB1_BASE, CLB_ADDR_COUNTER_1_LOAD, msg);

    //
    // 位计数限制为 32 位，表示为 << BIT_LIMIT 计算将数据序列化到 CRC 生成器所需
    // 的移位数。当计数匹配时，CRC 和串行器将停止
    //
    CLB_writeInterface(CLB1_BASE, CLB_ADDR_COUNTER_2_MATCH1, (bitCount + crcSize + 1) );

    //
    //  释放 RESET 并断言 RUN
    //  等待 CRC 完成，然后读取 CRC 结果的通过计数和失败计数
    //
    CLB_setGPREG(CLB1_BASE, CLB_GPREG_ASSERT);
    while(CLB_getInterruptTag(CLB1_BASE) != 1)
    {
    }
    crc = CLB_getRegister(CLB1_BASE, CLB_REG_CTR_C0);
    CLB_clearInterruptTag(CLB1_BASE);
    return(crc);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_disableCLB(CLB1_BASE);
}

//
// End of File
//

