/******************************************************************
 文 档 名：       HX_DSC280049_DMA_gsram_transfer.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
		使用DMA传输数据，从RAMGS0到RAMGS1，
		设置DMA通道PERINTFRC位重复传输，
		直到16突发包（每个包 8个 16位的字节）传输完成。
		整个传输完成，会产生DMA中断。

 外部接线：


 现象：在Expressions或Variables或者LiveView窗口观察变量
		sData	发送数据
		rData	接收数据

 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


//
// Globals
//
uint16_t CODE_SECTION("ramgs0") sData[128];   // Send data buffer
uint16_t CODE_SECTION("ramgs1") rData[128];   // Receive data buffer

volatile uint16_t done;

const void *destAddr = (const void *)rData;
const void *srcAddr = (const void *)sData;

//
// Function Prototypes
//
void error();
__interrupt void INT_myDMA0_ISR(void);


int main(void)
{
    uint16_t i;

    Device_init();

    Board_init();


    //
    // User specific code, enable interrupts:
    // Initialize the data buffers
    //
    for(i = 0; i < 128; i++)
    {
        sData[i] = i;
        rData[i] = 0;
    }

    //
    // Enable interrupts required for this example
    //
    Interrupt_enable(INT_myDMA0);
    EINT;                                // Enable Global Interrupts
    // Start DMA channel
    DMA_startChannel(myDMA0_BASE);

    done = 0;           // Test is not done yet

    while(!done)        // wait until the DMA transfer is complete
    {
       DMA_forceTrigger(myDMA0_BASE);

       asm volatile (".align 2;RPTI 255,4;NOP");
    }

    //
    // When the DMA transfer is complete the program will stop here
    //
    ESTOP0;

    return 0;
}


//
// error - Error Function which will halt the debugger
//
void error(void)
{
    ESTOP0;  //Test failed!! Stop!
    for (;;);
}

//
// local_D_INTCH6_ISR - DMA Channel6 ISR
//
__interrupt void INT_myDMA0_ISR(void)
{
    uint16_t i;

    DMA_stopChannel(myDMA0_BASE);
    // ACK to receive more interrupts from this PIE group
    EALLOW;
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
    EDIS;

    for( i = 0; i < 128; i++ )
    {
        //
        // check for data integrity
        //
        if (rData[i] != i)
        {
            error();
        }
    }

    done = 1; // Test done.
    return;
}

//
// End of File
//

