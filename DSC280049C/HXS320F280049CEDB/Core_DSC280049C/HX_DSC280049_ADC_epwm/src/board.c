

#include "board.h"

//*****************************************************************************
//
// Board Configurations
// Initializes the rest of the modules.
// Call this function in your application if you wish to do all module
// initialization.
// If you wish to not use some of the initializations, instead of the
// Board_init use the individual Module_inits
//
//*****************************************************************************
void Board_init()
{
	EALLOW;

	PinMux_init();
	ASYSCTL_init();
	InitAdcAio();
	ADC_init();
	INTERRUPT_init();

	EDIS;
}

//*****************************************************************************
//
// PINMUX Configurations
//
//*****************************************************************************
void PinMux_init()
{
	//
	// PinMux for modules assigned to CPU1
	//



}

//*****************************************************************************
//
// ADC Configurations
//
//*****************************************************************************
void InitAdcAio(void)
{
	/*配置GPIO231为模拟量输入A0/C15*/
	GPIO_setPinConfig(GPIO_247_GPIO247);
	/*配置GPIO231模拟量输入*/
	GPIO_setAnalogMode(247, GPIO_ANALOG_ENABLED);

	/*配置GPIO232为模拟量输入A1*/
	GPIO_setPinConfig(GPIO_246_GPIO246);
	GPIO_setAnalogMode(246, GPIO_ANALOG_ENABLED);
	/*配置GPIO230为模拟量输入A10/C10*/
	GPIO_setPinConfig(GPIO_230_GPIO230);
	GPIO_setAnalogMode(230, GPIO_ANALOG_ENABLED);
	/*配置GPIO237为模拟量输入A11/C0*/
	GPIO_setPinConfig(GPIO_241_GPIO241);
	GPIO_setAnalogMode(241, GPIO_ANALOG_ENABLED);
	/*配置GPIO238为模拟量输入A12/C1*/
	GPIO_setPinConfig(GPIO_242_GPIO242);
	GPIO_setAnalogMode(242, GPIO_ANALOG_ENABLED);
	/*配置GPIO239为模拟量输入A14/C4*/
	GPIO_setPinConfig(GPIO_237_GPIO237);
	GPIO_setAnalogMode(237, GPIO_ANALOG_ENABLED);
	/*配置GPIO233为模拟量输入A15/C7*/
	GPIO_setPinConfig(GPIO_224_GPIO224);
	GPIO_setAnalogMode(224, GPIO_ANALOG_ENABLED);
	/*配置GPIO224为模拟量输入A2/C9*/
	GPIO_setPinConfig(GPIO_244_GPIO244);
	GPIO_setAnalogMode(244, GPIO_ANALOG_ENABLED);
	/*配置GPIO242为模拟量输入A3/C5/VDAC*/
	GPIO_setPinConfig(GPIO_226_GPIO226);
	GPIO_setAnalogMode(226, GPIO_ANALOG_ENABLED);
	/*配置GPIO225为模拟量输入A4/C14*/
	GPIO_setPinConfig(GPIO_233_GPIO233);
	GPIO_setAnalogMode(233, GPIO_ANALOG_ENABLED);
	/*配置GPIO244为模拟量输入A5/C2*/
	GPIO_setPinConfig(GPIO_239_GPIO239);
	GPIO_setAnalogMode(239, GPIO_ANALOG_ENABLED);
	/*配置GPIO228为模拟量输入A6*/
	GPIO_setPinConfig(GPIO_228_GPIO228);
	GPIO_setAnalogMode(228, GPIO_ANALOG_ENABLED);
	/*配置GPIO245为模拟量输入A7/C3*/
	GPIO_setPinConfig(GPIO_232_GPIO232);
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);
	/*配置GPIO241为模拟量输入A8/C11*/
	GPIO_setPinConfig(GPIO_231_GPIO231);
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);
	/*配置GPIO227为模拟量输入A9/C8*/
	GPIO_setPinConfig(GPIO_227_GPIO227);
	GPIO_setAnalogMode(227, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_245_GPIO245);
	GPIO_setAnalogMode(245, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_235_GPIO235);
	GPIO_setAnalogMode(235, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_236_GPIO236);
	GPIO_setAnalogMode(236, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_234_GPIO234);
	GPIO_setAnalogMode(234, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_243_GPIO243);
	GPIO_setAnalogMode(243, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_225_GPIO225);
	GPIO_setAnalogMode(225, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_238_GPIO238);
	GPIO_setAnalogMode(238, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_229_GPIO229);
	GPIO_setAnalogMode(229, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_240_GPIO240 );
	GPIO_setAnalogMode(240, GPIO_ANALOG_ENABLED);
}




void ADC_init(){
	myADC0_init();

	EALLOW;
		//HWREG(ADCA_BASE + ADC_O_CTL1) &= ~((uint16_t)ADC_CTL1_PGA_DIS) ;		//CLR 0 ENLPGA
		HWREG(ADCA_BASE + ADC_O_CTL1) |= ((uint16_t) ADC_CTL1_PGA_DIS);//SET 1 DISpga
		EDIS;

}

void myADC0_init(){
	//
	// ADC Initialization: Write ADC configurations and power up the ADC
	//
	// Configures the ADC module's offset trim
	//
//	ADC_setOffsetTrimAll(ADC_REFERENCE_INTERNAL,ADC_REFERENCE_3_3V);
	//
	// Configures the analog-to-digital converter module prescaler.
	//
	ADC_setPrescaler(myADC0_BASE, ADC_CLK_DIV_8_0);  //   /8=20M
	//
	// Sets the timing of the end-of-conversion pulse
	//
	ADC_setInterruptPulseMode(myADC0_BASE, ADC_PULSE_END_OF_CONV);
	//
	// Powers up the analog-to-digital converter core.
	//
	ADC_enableConverter(myADC0_BASE);
	//
	// Delay for 1ms to allow ADC time to power up
	//
	DEVICE_DELAY_US(5000);
	//
	// SOC Configuration: Setup ADC EPWM channel and trigger settings
	//
	// Disables SOC burst mode.
	//
	ADC_disableBurstMode(myADC0_BASE);
	//
	// Sets the priority mode of the SOCs.
	//
	ADC_setSOCPriority(myADC0_BASE, ADC_PRI_ALL_ROUND_ROBIN);
	//
	// Start of Conversion 0 Configuration
	//
	//
	// Configures a start-of-conversion (SOC) in the ADC and its interrupt SOC trigger.
	// 	  	SOC number		: 0
	//	  	Trigger			: ADC_TRIGGER_EPWM1_SOCA
	//	  	Channel			: ADC_CH_ADCIN0
	//	 	Sample Window	: 8 SYSCLK cycles
	//		Interrupt Trigger: ADC_INT_SOC_TRIGGER_NONE
	//
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER3, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);

	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER4, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER5, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER6, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER7, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);

	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER8, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER9, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER10, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER11, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);

	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER12, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN3, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER13, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN3, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER14, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN3, 8U);
	ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER15, ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN3, 8U);

	ADC_setInterruptSOCTrigger(myADC0_BASE, ADC_SOC_NUMBER0, ADC_INT_SOC_TRIGGER_NONE);
	//
	//      ADC Interrupt 1 Configuration
	// 		SOC/EOC number	: 0
	// 		Interrupt Source: enabled
	// 		Continuous Mode	: disabled
	//

	ADC_setInterruptSource(myADC0_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER0);

	ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER1);
	ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);
	ADC_disableContinuousMode(myADC0_BASE, ADC_INT_NUMBER1);
}

//*****************************************************************************
//
// ASYSCTL Configurations
//
//*****************************************************************************
void ASYSCTL_init(){
	//
	// asysctl initialization
	//
	// Disables the temperature sensor output to the ADC.
	//
	ASysCtl_disableTemperatureSensor();
	//
	// Set the analog voltage reference selection to internal.
	//
	ASysCtl_setAnalogReferenceInternal( ASYSCTL_VREFHIA | ASYSCTL_VREFHIB | ASYSCTL_VREFHIC );
	//
	// Set the internal analog voltage reference selection to 1.65V.
	//
	ASysCtl_setAnalogReference1P65( ASYSCTL_VREFHIA | ASYSCTL_VREFHIB | ASYSCTL_VREFHIC );
}
//*****************************************************************************
//
// INTERRUPT Configurations
//
//*****************************************************************************
void INTERRUPT_init(){

	// Interrupt Setings for INT_myADC0_1
	Interrupt_register(INT_myADC0_1, &adcA1ISR);
	Interrupt_enable(INT_myADC0_1);
}
