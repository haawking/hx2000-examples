/******************************************************************
 文 档 名：       HX_DSC280049_ADC_epwm.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：配置epwm1触发ADCA的转换。

 外部接线：A0 接到被测的信号

 现象：在Expressions或Variables或者LiveView窗口观察变量
 	 myADC0Results


 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


#define RESULTS_BUFFER_SIZE     16 //256


uint16_t myADC0Results[RESULTS_BUFFER_SIZE];   // Buffer for results
uint16_t hxindex;                              // Index into result buffer
volatile uint16_t bufferFull;                // Flag to indicate buffer is full

void initEPWM(void);


int main(void)
{
         Device_init();
    //
        // Disable pin locks and enable internal pullups.
        //
        Device_initGPIO();

        //
        // Initialize PIE and clear PIE registers. Disables CPU interrupts.
        //
        Interrupt_initModule();

        //
        // Initialize the PIE vector table with pointers to the shell Interrupt
        // Service Routines (ISR).
        //
        Interrupt_initVectorTable();

        //
        // Board Initialization
        // - Set up the ADC and initialize the SOC
        // - Enable ADC interrupt
        //
        Board_init();

        // Set up the ePWM


        initEPWM();



        //
        // Initialize results buffer
        //
        for(hxindex = 0; hxindex < RESULTS_BUFFER_SIZE; hxindex++)
        {
            myADC0Results[hxindex] = 0;
        }

        hxindex = 0;
        bufferFull = 0;

        //
        // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
        //
        EINT;


        //
        // Loop indefinitely
        //
    while(1)
    {
//           //
//           // Wait while ePWM1 causes ADC conversions which then cause interrupts.
//           // When the results buffer is filled, the bufferFull flag will be set.
//           //
//           while(bufferFull == 0)
//           {
//           }
//           bufferFull = 0;     // Clear the buffer full flag
//
//           //
//           // Software breakpoint. At this point, conversion results are stored in
//           // myADC0Results.
//           //
//           // Hit run again to get updated conversions.
//           //
//           //ESTOP0;
       }
    return 0;
}

//
// Function to configure ePWM1 to generate the SOC.
//
void initEPWM(void)
{
    //
    // Disable SOCA
    //
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    //
    // Freeze the counter
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    //
    // Configure the SOC to occur on the first up-count event
    //
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);

    //
    // Set the compare A value to 1000 and the period to 1999
    // Assuming ePWM clock is 160MHz, this would give 80kHz sampling
    // 50MHz ePWM clock would give 25kHz sampling, etc.
    // The sample rate can also be modulated by changing the ePWM period
    // directly (ensure that the compare A value is less than the period).
    //
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 1000); //duty 50%
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);  //160M/2k=80K

    //
    // Set the local ePWM module clock divider to /1
    //
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // Start ePWM1, enabling SOCA and putting the counter in up-count mode
    //
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);
}

//
// adcA1ISR - ADC A Interrupt 1 ISR
//
__interrupt void adcA1ISR(void)
{
    //
    // Add the latest result to the buffer
    //
    myADC0Results[0] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    myADC0Results[1] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
    myADC0Results[2] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2);
    myADC0Results[3] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER3);

    myADC0Results[4] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER4);
    myADC0Results[5] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER5);
    myADC0Results[6] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER6);
    myADC0Results[7] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER7);

    myADC0Results[8] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER8);
    myADC0Results[9] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER9);
    myADC0Results[10] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER10);
    myADC0Results[11] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER11);

    myADC0Results[12] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER12);
    myADC0Results[13] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER13);
    myADC0Results[14] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14);
    myADC0Results[15] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER15);
    //
    // Set the bufferFull flag if the buffer is full
    //


    if(RESULTS_BUFFER_SIZE <= hxindex)
    {
    	hxindex = 0;
        bufferFull = 1;
    }

    //
    // Clear the interrupt flag
    //
    ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);

    //
    // Check if overflow has occurred
    //
    if(true == ADC_getInterruptOverflowStatus(myADC0_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(myADC0_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);
    }

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INT_myADC0_1_INTERRUPT_ACK_GROUP);
}

//
// End of File
//


