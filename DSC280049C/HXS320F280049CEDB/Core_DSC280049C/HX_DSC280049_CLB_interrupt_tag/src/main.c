/******************************************************************
 文 档 名：      HX_DSC280049_CLB_interrupt_tag
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB_中断标记
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB_中断标记
           在此示例中，使用两个不同的匹配值设置计时器。HLC 子模块使用这两个事件来生成中断。
           中断 TAG 用于区分 CLB 计数器的 match1 事件和 CLB 计数器的 match2 事件产生的中断


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);
void XBAR_init(void);

__interrupt void clb1ISR(void);

#define COUNTER0_TIMER_ENABLE     0x1

#define COUNTER0_INT_TAG    11
#define COUNTER1_INT_TAG    12

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);

    PinMux_init();
    XBAR_init();
    CLB_init();

    initTILE1(CLB1_BASE);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    CLB_setGPREG(CLB1_BASE, COUNTER0_TIMER_ENABLE);

    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    for(;;)
    {

    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_1_GPIO1);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(1, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_24_OUTPUTXBAR1);
	GPIO_setPinConfig(GPIO_16_OUTPUTXBAR7);
}

void XBAR_init(void)
{
	XBAR_setOutputLatchMode(XBAR_OUTPUT1, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT1, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX01);

	XBAR_setOutputLatchMode(XBAR_OUTPUT7, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT7, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT7, XBAR_OUT_MUX03_CLB1_OUT5);
	XBAR_enableOutputMux(XBAR_OUTPUT7, XBAR_MUX03);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

__interrupt void clb1ISR(void)
{
    uint16_t tag = CLB_getInterruptTag(CLB1_BASE);
    if (tag == COUNTER0_INT_TAG)
    {
        GPIO_togglePin(0);
    }
    if (tag == COUNTER1_INT_TAG)
    {
        GPIO_togglePin(1);
    }

    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
//
// End of File
//

