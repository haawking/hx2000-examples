/******************************************************************
 文 档 名：       HX_DSC280049_PGA_dac_pga_adc_ext_loopback
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     PGA DAC-ADC 外部回环测试
 -------------------------- 例程使用说明 --------------------------
 功能描述：  PGA DAC-ADC 外部回环测试
                     本例使用DAC输出产生400 mV电压（使用内部基准电压源）。DAC的输出从外部连接到PGA2，以实现3倍增益放大。使用两个ADC通道对
           DAC输出和PGA2的放大电压采样。ADC在内部连接到这些信号。


 外部连接：
   -DACA_OUT（模拟引脚 A0）连接到PGA2_IN
   -PGA246NEG连接到地

调试 ：在Variables或  Live View中观察变量
   dacResult - DAC输出电压
   pgaResult - 放大的DAC电压
   pgaGain   - 放大后的DAC电压与原始DAC输出的比值。始终读取值 ~3.0。
 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
volatile uint16_t dacResult;
volatile uint16_t pgaResult;
float pgaGain;


//
// Main
//
int main(void)
{
       //
        // 初始化系统时钟和外设
        //
        Device_init();

        Board_init();

        //
        // 使能全局中断 (INTM) 和实时中断 (DBGM)
        //
        EINT;
        ERTM;

        //
        // 强制ADC转换
        //
        ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER0);
        ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER1);

        //
        // 循环
        //
        while(1)
        {
            //
            // 检查转换是否完成
            //
            if(ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1))
            {
                //
                // 确认中断标志
                //
                ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);

                dacResult = ADC_readResult(myADC0_RESULT_BASE, ADC_SOC_NUMBER0);
                pgaResult = ADC_readResult(myADC0_RESULT_BASE, ADC_SOC_NUMBER1);
                pgaGain   = (float)pgaResult / (float)dacResult;

                //
                // 强制ADC转换
                //
                ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER0);
                ADC_forceSOC(myADC0_BASE, ADC_SOC_NUMBER1);
            }
        }



  //  return 0;
}
