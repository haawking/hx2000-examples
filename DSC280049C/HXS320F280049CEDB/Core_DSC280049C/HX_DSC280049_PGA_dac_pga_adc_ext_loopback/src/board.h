/*
 * board.h
 *
 *  Created on: 2023��7��13��
 *      Author: admin
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_
//
// Included Files
//

#include "driverlib.h"
#include "device.h"


#define myADC0_BASE ADCA_BASE
#define myADC0_RESULT_BASE ADCARESULT_BASE
#define myADC0_SOC0 ADC_SOC_NUMBER0
#define myADC0_FORCE_SOC0 ADC_FORCE_SOC0
#define myADC0_SOC1 ADC_SOC_NUMBER1
#define myADC0_FORCE_SOC1 ADC_FORCE_SOC1

#define myDAC0_BASE DACA_BASE

#define myPGA0_BASE PGA2_BASE
#define myPGA0_GAIN PGA_GAIN_3
#define myPGA0_FILTER PGA_LOW_PASS_FILTER_DISABLED


void    Board_init();
void    ADC_init();
void    ASYSCTL_init();
void    DAC_init();
void    PGA_init();
void    PinMux_init();
#endif /* SRC_BOARD_H_ */
