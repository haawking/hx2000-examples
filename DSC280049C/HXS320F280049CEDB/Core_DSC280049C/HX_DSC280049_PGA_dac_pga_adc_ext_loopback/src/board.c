/*
 * board.c
 *
 *  Created on: 2023��7��13��
 *      Author: admin
 */

#include "board.h"

void Board_init()
{
    EALLOW;

    PinMux_init();
    ASYSCTL_init();
    ADC_init();
    DAC_init();
    PGA_init();

    EDIS;
}

void PinMux_init()
{

}

void ADC_init(){
    //myADC0 initialization

    // ADC Initialization: Write ADC configurations and power up the ADC
    // Configures the ADC module's offset trim
  ADC_setOffsetTrimAll(ADC_REFERENCE_INTERNAL,ADC_REFERENCE_3_3V);
  //  ADC_setVREF(myADC0_BASE, ADC_REFERENCE_EXTERNAL,ADC_REFERENCE_3_3V);
    // Configures the analog-to-digital converter module prescaler.
    ADC_setPrescaler(myADC0_BASE, ADC_CLK_DIV_3_0);
    // Sets the timing of the end-of-conversion pulse
    ADC_setInterruptPulseMode(myADC0_BASE, ADC_PULSE_END_OF_CONV);
    // Powers up the analog-to-digital converter core.
    ADC_enableConverter(myADC0_BASE);
    // Delay for 1ms to allow ADC time to power up
    DEVICE_DELAY_US(5000);

    // SOC Configuration: Setup ADC EPWM channel and trigger settings
    // Disables SOC burst mode.
    ADC_disableBurstMode(myADC0_BASE);
    // Sets the priority mode of the SOCs.
    ADC_setSOCPriority(myADC0_BASE, ADC_PRI_ALL_ROUND_ROBIN);
    // Start of Conversion 0 Configuration
    // Configures a start-of-conversion (SOC) in the ADC and its interrupt SOC trigger.
    //      SOC number      : 0
    //      Trigger         : ADC_TRIGGER_SW_ONLY
    //      Channel         : ADC_CH_ADCIN0
    //      Sample Window   : 10 SYSCLK cycles
    //      Interrupt Trigger: ADC_INT_SOC_TRIGGER_NONE
    ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN0, 512U);
    ADC_setInterruptSOCTrigger(myADC0_BASE, ADC_SOC_NUMBER0, ADC_INT_SOC_TRIGGER_NONE);
    // Start of Conversion 1 Configuration
    // Configures a start-of-conversion (SOC) in the ADC and its interrupt SOC trigger.
    //      SOC number      : 1
    //      Trigger         : ADC_TRIGGER_SW_ONLY
    //      Channel         : ADC_CH_ADCIN12
    //      Sample Window   : 10 SYSCLK cycles
    //      Interrupt Trigger: ADC_INT_SOC_TRIGGER_NONE
    ADC_setupSOC(myADC0_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN12, 512U);
    ADC_setInterruptSOCTrigger(myADC0_BASE, ADC_SOC_NUMBER1, ADC_INT_SOC_TRIGGER_NONE);
    // ADC Interrupt 1 Configuration
    //      SOC/EOC number  : 1
    //      Interrupt Source: enabled
    //      Continuous Mode : disabled
    ADC_setInterruptSource(myADC0_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER1);
    ADC_enableInterrupt(myADC0_BASE, ADC_INT_NUMBER1);
    ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);
    ADC_disableContinuousMode(myADC0_BASE, ADC_INT_NUMBER1);

}
void ASYSCTL_init(){
    // asysctl initialization
    // Disables the temperature sensor output to the ADC.
    ASysCtl_disableTemperatureSensor();
    // Set the analog voltage reference selection to external.
    ASysCtl_setAnalogReferenceExternal( ASYSCTL_VREFHIA | ASYSCTL_VREFHIB | ASYSCTL_VREFHIC );
    // Set the analog voltage reference selection to internal.
   // ASysCtl_setAnalogReferenceInternal( ASYSCTL_VREFHIA | ASYSCTL_VREFHIB | ASYSCTL_VREFHIC );
    // Set the internal analog voltage reference selection to 1.65V.
   // ASysCtl_setAnalogReference1P65( ASYSCTL_VREFHIA | ASYSCTL_VREFHIB | ASYSCTL_VREFHIC );
}
void DAC_init(){


    // myDAC0 initialization
    // Set DAC reference voltage.
    DAC_setReferenceVoltage(myDAC0_BASE, DAC_REF_ADC_VREFHI);
    // Set DAC gain mode.
    DAC_setGainMode(myDAC0_BASE, DAC_GAIN_ONE);
    // Set DAC load mode.
    DAC_setLoadMode(myDAC0_BASE, DAC_LOAD_SYSCLK);
    // Enable the DAC output
    DAC_enableOutput(myDAC0_BASE);
    // Set the DAC shadow output
    DAC_setShadowValue(myDAC0_BASE, 500U);

    // Delay for buffered DAC to power up.
    DEVICE_DELAY_US(5000);

}

void PGA_init(){
    //myPGA0 initialization
    PGA_setGain(myPGA0_BASE, PGA_GAIN_3);
 //  PGA_setFilterResistor(myPGA0_BASE, PGA_LOW_PASS_FILTER_DISABLED);
    PGA_enable(myPGA0_BASE);
    // Delay for PGA to power up.
    DEVICE_DELAY_US(500);
}
