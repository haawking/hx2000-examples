/******************************************************************
 文 档 名：      HX_DSC280049_SPI_loopback_fifo_interrupt
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     SPI数字环回，带FIFO中断
 -------------------------- 例程使用说明 --------------------------
 功能描述：   SPI数字环回，带FIFO中断
                     该程序使用SPI模块的内部环回测试模式。同时使用SPI FIFO及其中断。

发送数据流，然后与接收到的数据流进行比较。发送的数据如下所示:
  0000 0001 \n
  0001 0002 \n
  0002 0003 \n
  .... \n
  FFFE FFFF \n
  FFFF 0000 \
这种模式永远重复

 外部连接：
    无

调试 ：在Variables或  Live View中观察变量
   sData - 发送数据
   rData - 接收数据
   rDataPoint - 用于跟踪接收流中的最后一个位置以进行错误检查

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

//
// Globals
//
uint16_t sData[2];                  // 发送数据缓冲区
uint16_t rData[2];                  // 接收数据缓冲区
uint16_t rDataPoint = 0;            // 跟踪我们在数据流中的位置以检查接收到的数据
void Pinmux_init(void);
void SPI_init(void);

//
// Function Prototypes
//
__interrupt void spiTxFIFOISR(void);
__interrupt void spiRxFIFOISR(void);

int main(void)
{
    uint16_t i;

    Device_init();
    Device_initGPIO();
    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_SPIA_TX, &spiTxFIFOISR);
    Interrupt_register(INT_SPIA_RX, &spiRxFIFOISR);

    Pinmux_init();
    SPI_init();

    //
    // 初始化数据buffer
    //
    for(i = 0; i < 2; i++)
    {
        sData[i] = i;
        rData[i]= 0;
    }

    Interrupt_enable(INT_SPIA_TX);
    Interrupt_enable(INT_SPIA_RX);

    EINT;
    ERTM;

    while(1);
    return 0;

}

//
// SPI A Transmit FIFO ISR
//
__interrupt void spiTxFIFOISR(void)
{
    uint16_t i;
    //
    // 发送数据
    //
    for(i = 0; i < 2; i++)
    {
       SPI_writeDataNonBlocking(SPIA_BASE, sData[i]);
    }
    //
    // 为下一个周期增加数据
    //
    for(i = 0; i < 2; i++)
    {
       sData[i] = sData[i] + 1;
    }
    //
    //清除中断标志并发出 ACK
    //
    SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_TXFF);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
}

//
// SPI A Receive FIFO ISR
//
 __interrupt void spiRxFIFOISR(void)
{
    uint16_t i;

    //
    // 接收数据
    //
    for(i = 0; i < 2; i++)
    {
        rData[i] = SPI_readDataNonBlocking(SPIA_BASE);
    }
    //
    // 检查接受数据
    //
    for(i = 0; i < 2; i++)
    {
        if(rData[i] != (rDataPoint + i))
        {
            // 出错，rData 不包含预期数据。
        	asm("NOP");
        }
        else
        {
        	GPIO_togglePin(0);
        }
    }
    rDataPoint++;
    //
    // 清除中断标志并发出 ACK
    //
    SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_RXFF);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
}

void Pinmux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_16_SPIA_SIMO);
	GPIO_setPadConfig(16, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_10_SPIA_SOMI);
	GPIO_setPadConfig(10, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(10, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_18_SPIA_CLK);
	GPIO_setPadConfig(18, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(18, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_11_SPIA_STE);
	GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(11, GPIO_QUAL_ASYNC);
}

void SPI_init(void)
{
	SPI_disableModule(SPIA_BASE);
	SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0, SPI_MODE_MASTER, 800000, 16);
	SPI_setSTESignalPolarity(SPIA_BASE, SPI_STE_ACTIVE_LOW);
	SPI_enableFIFO(SPIA_BASE);
	SPI_setFIFOInterruptLevel(SPIA_BASE, SPI_FIFO_TX2, SPI_FIFO_RX2);
	SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_RXFF | SPI_INT_TXFF);
	SPI_enableInterrupt(SPIA_BASE, SPI_INT_RXFF | SPI_INT_TXFF);
	SPI_enableLoopback(SPIA_BASE);
	SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_STOP_AFTER_TRANSMIT);
	SPI_enableModule(SPIA_BASE);
}
//
// End of File
//

