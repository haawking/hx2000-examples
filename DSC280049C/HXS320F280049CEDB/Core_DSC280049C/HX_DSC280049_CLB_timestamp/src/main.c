/******************************************************************
 文 档 名：      HX_DSC280049_CLB_timestamp
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB_时间戳
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB_时间戳
           当 GPIO12 连接到 GND 时，TZ1 跳闸。 当 GPIO13 连接到 GND 时，TZ2 跳闸。发生中断
           时，中断处理程序确定初始跳闸源，并将此值存储在变量“initialTripZone”中


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量
     initialTripZone: 存储第一个被跳闸的 TZ
     tz1Counter64bit：在 TZ1 跳闸时存储计数器值
     tz2Counter64bit：存储 TZ2 跳闸时的计数器值。


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "clb_config.h"
#include "clb.h"


void PinMux_init(void);
void CLB_init(void);
void XBAR_init(void);
void initEPWM1(void);
__interrupt void epwm1TZISR(void);
__interrupt void clb1ISR(void);

uint32_t epwm1TZIntCount;
#define EPWM_TZ1_INT_TAG    12U
#define EPWM_TZ2_INT_TAG    13U

uint32_t initialTripZone;
uint64_t tz1Counter64bit;
uint64_t tz2Counter64bit;

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_EPWM1_TZ, &epwm1TZISR);
    Interrupt_register(INT_CLB1, &clb1ISR);

    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    initEPWM1();

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    PinMux_init();
    XBAR_init();
    CLB_init();

    initTILE1(CLB1_BASE);

    Interrupt_enable(INT_EPWM1_TZ);
    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    HWREG(CLB1_BASE + CLB_LOGICCTL + CLB_O_GP_REG) = 1 << 7;

    for(;;)
    {
    }
}

__interrupt void clb1ISR(void)
{
    // 获取 TZ1 的计数器值
    uint32_t baseTZ1 = CLB_getRegister(CLB1_BASE, CLB_REG_HLC_R0);
    uint32_t overflowTZ1 = CLB_getRegister(CLB1_BASE, CLB_REG_HLC_R2);
    tz1Counter64bit = (uint64_t) overflowTZ1 << 32 | baseTZ1;

    // 获取 TZ2 的计数器值
    uint32_t baseTZ2 = CLB_getRegister(CLB1_BASE, CLB_REG_HLC_R1);
    uint32_t overflowTZ2 = CLB_getRegister(CLB1_BASE, CLB_REG_HLC_R3);
    tz2Counter64bit = (uint64_t) overflowTZ2 << 32 | baseTZ2;

    // 确定中断的初始源，TZ1或TZ2
    if ( (tz1Counter64bit != 0) && ((tz1Counter64bit < tz2Counter64bit) || (tz2Counter64bit == 0)) )
        initialTripZone = 1;
    else if ( (tz2Counter64bit != 0) && ((tz2Counter64bit < tz1Counter64bit) || (tz1Counter64bit == 0)) )
        initialTripZone = 2;
    else
        initialTripZone = 0;

    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}

__interrupt void epwm1TZISR(void)
{
    epwm1TZIntCount++;

    //
    // 要重新启用OST中断，请取消注释以下代码:
    //
    // EPWM_clearTripZoneFlag(myEPWM1_BASE,
    //                        (EPWM_TZ_INTERRUPT | EPWM_TZ_FLAG_CBC));

    //
    // 确认此中断以接收来自组 2 的更多中断
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP2);
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_11_GPIO11);
	GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(11, GPIO_QUAL_ASYNC);
	GPIO_setDirectionMode(11, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_12_GPIO12);
	GPIO_setPadConfig(12, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(12, GPIO_QUAL_ASYNC);
	GPIO_setDirectionMode(12, GPIO_DIR_MODE_IN);

	GPIO_setPinConfig(GPIO_13_GPIO13);
	GPIO_setPadConfig(13, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(13, GPIO_QUAL_ASYNC);
	GPIO_setDirectionMode(13, GPIO_DIR_MODE_IN);
}

void XBAR_init(void)
{
	XBAR_setCLBMuxConfig(XBAR_AUXSIG0, XBAR_CLB_MUX01_INPUTXBAR1);
	XBAR_enableCLBMux(XBAR_AUXSIG0, XBAR_MUX01);

	XBAR_setCLBMuxConfig(XBAR_AUXSIG1, XBAR_CLB_MUX03_INPUTXBAR2);
	XBAR_enableCLBMux(XBAR_AUXSIG1, XBAR_MUX03);

	XBAR_setInputPin(XBAR_INPUT1, 12);
	XBAR_lockInput(XBAR_INPUT1);

	XBAR_setInputPin(XBAR_INPUT2, 13);
	XBAR_lockInput(XBAR_INPUT2);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_CLB_AUXSIG0);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_CLB_AUXSIG1);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_RISING_EDGE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN7, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN7, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN7, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN7, CLB_FILTER_RISING_EDGE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

void initEPWM1(void)
{
    //
    // 启用 TZ1 和 TZ2 作为单次跳闸源
    //
    EPWM_enableTripZoneSignals(EPWM1_BASE, EPWM_TZ_SIGNAL_OSHT1);
    EPWM_enableTripZoneSignals(EPWM1_BASE, EPWM_TZ_SIGNAL_OSHT2);

    //
    //  TZ1动作
    //
    EPWM_setTripZoneAction(EPWM1_BASE, EPWM_TZ_ACTION_EVENT_TZA, EPWM_TZ_ACTION_HIGH);

    //
    // 使能TZ中断
    //
    EPWM_enableTripZoneInterrupt(EPWM1_BASE, EPWM_TZ_INTERRUPT_OST);

    //
    // 设置 TBCLK
    //
    EPWM_setTimeBasePeriod(EPWM1_BASE, 12000U);
    EPWM_setPhaseShift(EPWM1_BASE, 0U);
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0U);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_disablePhaseShiftLoad(EPWM1_BASE);

    //
    // 设置 ePWM 时钟分频
    //
    EPWM_setClockPrescaler(EPWM1_BASE, EPWM_CLOCK_DIVIDER_4, EPWM_HSCLOCK_DIVIDER_4);

    //
    // 设置影子寄存器
    //
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);

    //
    // 设置比较值
    //
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 6000U);

    //
    // 设置动作
    //
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
}
//
// End of File
//

