//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F280049CEDB, Hal DriverLib, 1.0.1
//
// Release time: 2023-09-15 12:35:18.257216
//
//#############################################################################


#ifndef HW_PGA_H
#define HW_PGA_H

//*************************************************************************************************
//
// The following are defines for the PGA register offsets
//
//*************************************************************************************************
#define PGA_O_CTL      0x0U    // PGA Control Register
#define PGA_O_LOCK     0x4U    // PGA Lock Register
#define PGA_O_PM       0x8U    // PGA Power Manage Control Signal Register
#define PGA_O_FILTER   0xCU    // PGA filter resistance signal
#define PGA_O_TYPE     0x10U   // PGA Type Register


//*************************************************************************************************
//
// The following are defines for the bit fields in the PGACTL register
//
//*************************************************************************************************
#define PGA_CTL_PGAEN    0x1U    // PGA Enable
#define PGA_CTL_GAIN_S   5U
#define PGA_CTL_GAIN_M   0xE0U   // PGA gain setting

//*************************************************************************************************
//
// The following are defines for the bit fields in the PGALOCK register
//
//*************************************************************************************************
#define PGA_LOCK_PGACTL      0x1U   // Lock bit for PGACTL.
#define PGA_LOCK_PGAFILTER   0x4U   //  
#define PGA_LOCK_PGAPM       0x8U   //  

//*************************************************************************************************
//
// The following are defines for the bit fields in the PGAPM register
//
//*************************************************************************************************
#define PGA_PM_PM_S   0U
#define PGA_PM_PM_M   0x7U   // Power Manage Control Signal

//*************************************************************************************************
//
// The following are defines for the bit fields in the PGAFILTER register
//
//*************************************************************************************************
#define PGA_FILTER_FILTEREN      0x1U    // Filter Enable
#define PGA_FILTER_FILTERSEL_S   1U
#define PGA_FILTER_FILTERSEL_M   0x1EU   // Filter Select

//*************************************************************************************************
//
// The following are defines for the bit fields in the PGATYPE register
//
//*************************************************************************************************
#define PGA_TYPE_REV_S    0U
#define PGA_TYPE_REV_M    0xFFU     // PGA Revision Field
#define PGA_TYPE_TYPE_S   8U
#define PGA_TYPE_TYPE_M   0xFF00U   // PGA Type Field



#endif
