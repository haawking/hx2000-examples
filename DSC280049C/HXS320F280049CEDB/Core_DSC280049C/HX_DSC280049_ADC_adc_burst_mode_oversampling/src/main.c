/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_burst_mode_oversampling
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       ADC突发模式过采样示例
 -------------------------- 例程使用说明 --------------------------
功能描述：    此示例将 ePWM1 设置为定期触发ADCA SOC0 和 SOC1
                   （用于采样 A3 和 A4）。此外，ADC突发模式也通过ePWM1触发。
                     突发 SOC 用于在多个 ePWM 周期内累积多个转换值以对 A5 进行过采样

外部连接：A3, A4, A5

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  adcAResult0 - 引脚A3的数字量
 	  adcAResult1 - 引脚A4的数字量
 	  adcAResult2 - 引脚A5的数字量

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t adcAResult0;
uint16_t adcAResult1;
uint16_t adcAResult2;
volatile uint16_t isrCount;

//
// Function Prototypes
//
void initEPWM();
__interrupt void adcA1ISR(void);

//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉。
    //
    Device_initGPIO();

    //
    //初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 设置ADCs：
    // 信号模式：单端
    // 转换分辨率：12 - bit
    //
    // 突发模式配置：
    // 每次转换4次触发，每次触发需要 4 个 SOC，
    // 因此 SOC0 到 SOC11 设置为高优先级，
    // 剩下4个SOC用作循环突发。
    //
    // 将 SOC0-12 配置为高优先级模式，
    // 将 SOC0和 SOC1 配置为EPWM1SOCA触发分别对 A3 和 A4 通道进行采样。
    //
    // 为 SOC12-15 启用突发模式，突发大小为 1，
    // 突发模式触发源为EPWM1 SOCA。
    // 选择要转换的通道并配置 S+H 持续时间
    // Burst1: SOC12  (A5), Burst2: SOC13  (A5),
    // Burst3: SOC14  (A5), Burst4: SOC14  (A5)。
    //
    // 注册并启用中断：在每次PWM触发后读取高优先级SOC0和SOC1结果，
    // 而 4 个突发结果将在 4 次 ePWM 触发后取平均值。
    //
    Board_init();

    //
    // 初始化PWM
    //
    initEPWM();

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;
    ERTM;

    //
    // 初始化ISR计数器
    //
    isrCount = 0;

    //
    // 开启ePWM1,使能SOCA并设置计数器为向上计数模式。
    //
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // 在循环中转换
    //
    do
    {
        //
        // 等待ePWM触发ADC转换。ADCA1 ISR 处理每组新的转换。.
        //
    }
    while(1);
}

//
// ePWM1触发SOC的功能函数
//
void initEPWM(void)
{
    //
    // 关闭SOCA
    //
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    //
    // 将 SOC 配置为在第一个向上计数事件时产生触发
    //
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);

    //
    // 设置比较器A的值为1000并且周期为1999。
    // 假设ePWM时钟为100MHz，则采样率为50kHz。
    // 50MHz ePWM 时钟将提供 25kHz 采样率。
    // 也可以通过直接改变ePWM周期来调制采样率（确保比较A值小于周期）。
    //
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 1000);
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);

    //
    // 设置本地ePWM模块时钟分频为1。
    //
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // 冻结定时器。
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}

//
// adcA1ISR - ADCA中断突发模式 ISR 将采样收集 A3 和 A4 的每个 ISR 新结果
//            A5 结果在 4 个 ePWM 触发后取平均值。
//
__interrupt void adcA1ISR(void)
{
    //
    // 记录A0和A1的结果
    //
    adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);

    //
    // 每4个ISR后，突发模式buffer将填充要平均的新结果
    //
    if(++isrCount == 4)
    {
        //
        // 求4个结果的平均值并复位计数器
        //
        adcAResult2 =
            (ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER12) +
            ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER13) +
            ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14) +
            ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER15)) >> 2;
        isrCount = 0;
    }

    //
    // 清空中断标志
    //
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // 检查是否发生溢出
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    //
    // 确认中断
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// End of file
//
