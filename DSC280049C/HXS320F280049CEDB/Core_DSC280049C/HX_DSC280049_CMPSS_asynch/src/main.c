/******************************************************************
 文 档 名：       HX_DSC280049_CMPSS_asynch
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：    CMPSS异步跳闸
 -------------------------- 例程使用说明 --------------------------
 功能描述：   CMPSS异步跳闸
                     此示例使能 CMPSS1 COMPH 比较器，并将异步 CTRIPOUTH 信号馈送到 GPIO14/OUTPUTXBAR3 引脚，
                     将 CTRIPH 馈送到 GPIO15/EPWM8B

 CMPSS 配置为生成跳闸信号以使 EPWM 信号跳闸。CMPIN1P用于提供正输入，内部DAC配置为提供负输入。内部DAC配置为
 VDD/2 。EPWM信号在GPIO15上生成并配置为被CTRIPOUTH跳闸。

 当向CMPIN1P提供低输入 （VSS） 时：
      -跳闸信号（GPIO14）输出低电平
   -PWM8B（GPIO15）给出PWM信号
为CMPIN1P提供高输入（高于VDD/2）时：
      - 跳闸信号（GPIO14）输出变为高电平
   -PWM8B（GPIO15） 跳闸并输出为高电平


 外部连接：
      -在CMPIN1P上提供输入（引脚与ADCINB6共享）
      -可以使用示波器在 GPIO14 和 GPIO15 上观察输出

调试 ：无

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Function Prototypes
//
void initEPWM(void);

//
// Main
//
int main(void)
{
    //
    //初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚并使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 将 ePWM8 设置为将 CTRIPH 作为其直流跳闸输入的 TRIP4
    //
    initEPWM();

    //
    // 板级初始化
    // - 配置 GPIO14 输出CTRIPOUT1H（通过 XBAROUTPUT3 路由）
    // - 将输出 X-BAR 设置为在 OUTPUTXBAR3 上输出
    // - 配置 CMPSS1 的高比较器
    // - 使能CMPSS并将负输入信号配置为来自DAC
    // - 使用VDDA作为DAC的基准，并将DAC值设置为中点作为任意值参考。
    // - 配置输出信号。CTRIPH 和 CTRIPOUTH 都将由异步比较器输出馈送。
    //
    Board_init();


    //
    // 配置 GPIO15 以输出 CLIPH（通过 ePWM、TRIP4 和 ePWM8 路由）
    //
    GPIO_setPinConfig(GPIO_15_EPWM8B);

    //
    // 启用全局中断 （INTM） 和实时中断 （DBGM）
    //
    EINT;
    ERTM;

    //
    // 循环
    //
    while(1)
    {
        //
        // 当 CTRIP 信号被置位时设置跳闸标志
        //
        if((EPWM_getTripZoneFlagStatus(EPWM8_BASE) &
            EPWM_TZ_FLAG_OST) != 0U)
        {
            //
            // 等待比较器 CTRIP 取消置位
            //
            while((CMPSS_getStatus(CMPSS1_BASE) & CMPSS_STS_HI_FILTOUT) != 0U)
            {
                ;
            }

            //
            //清空跳闸标志位
            //
            EPWM_clearTripZoneFlag(EPWM8_BASE, EPWM_TZ_INTERRUPT |
                                   EPWM_TZ_FLAG_OST);
        }
    }
}

//
// initEPWM - 配置 ePWM8 和 ePWM X-BAR 以将 CTRIPH 作为直流脱扣输入
//
void initEPWM(void)
{
    //
    // 在配置模块之前禁用 ePWM 时基时钟
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 将时基时钟预分频器设置为 /1
    //
    EPWM_setClockPrescaler(EPWM8_BASE, EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // 初始化 ePWM 计数器和周期的虚拟值
    //
    EPWM_setTimeBaseCounter(EPWM8_BASE, 0);
    EPWM_setTimeBasePeriod(EPWM8_BASE, 0xFFFF);

    //
    // 设置比较值
    //
    EPWM_setCounterCompareValue(EPWM8_BASE, EPWM_COUNTER_COMPARE_B, 0x8000);

    //
    // 设置动作
    //
    EPWM_setActionQualifierAction(EPWM8_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);

    EPWM_setActionQualifierAction(EPWM8_BASE,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);

    //
    // 将 ePWM8B 配置为在 TZB TRIP 上输出高电平

    //
    EPWM_setTripZoneAction(EPWM8_BASE, EPWM_TZ_ACTION_EVENT_TZB,
                           EPWM_TZ_ACTION_HIGH);

    //
    // DCBH 为高电平时触发事件
    //
    EPWM_setTripZoneDigitalCompareEventCondition(EPWM8_BASE,
                                                 EPWM_TZ_DC_OUTPUT_B1,
                                                 EPWM_TZ_EVENT_DCXH_HIGH);

    //
    // 将 DCBH 配置为使用 TRIP4 作为输入
    //
    EPWM_enableDigitalCompareTripCombinationInput(EPWM8_BASE,
                                                  EPWM_DC_COMBINATIONAL_TRIPIN4,
                                                  EPWM_DC_TYPE_DCBH);

    //
    // 启用 DCB 作为 OST
    //
    EPWM_enableTripZoneSignals(EPWM8_BASE, EPWM_TZ_SIGNAL_DCBEVT1);

    //
    // 将 DCB 路径配置为未滤波和异步
    //
    EPWM_setDigitalCompareEventSource(EPWM8_BASE,
                                      EPWM_DC_MODULE_B,
                                      EPWM_DC_EVENT_1,
                                      EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    //
    // 同步 ePWM 时基时钟
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 使用 ePWM X-BAR 将 TRIP4 配置为CTRIP1H
    //
    XBAR_setEPWMMuxConfig(XBAR_TRIP4, XBAR_EPWM_MUX00_CMPSS1_CTRIPH);
    XBAR_enableEPWMMux(XBAR_TRIP4, XBAR_MUX00);

    //
    // 清除跳闸标志

    //
    EPWM_clearTripZoneFlag(EPWM8_BASE, EPWM_TZ_INTERRUPT |
                           EPWM_TZ_FLAG_OST);

    //
    // 将时基计数器置于向上计数模式
    //
    EPWM_setTimeBaseCounterMode(EPWM8_BASE, EPWM_COUNTER_MODE_UP);
}



