/******************************************************************
 文 档 名：       HX_DSC280049_GPIO_toggle.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：配置GPIO23，IO在主循环翻转。

 外部接线：

 现象：LED2 一秒闪一次

 版 本：      V1.0.0
 时 间：      2023年7月12日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


int main(void)
{
	uint16 i;

    Device_init();

       // Initializes PIE and clear PIE registers. Disables CPU interrupts.
       // and clear all CPU interrupt flags.
       //
       Interrupt_initModule();

       //
       // Initialize the PIE vector table with pointers to the shell interrupt
       // Service Routines (ISR).
       //
       Interrupt_initVectorTable();
       //
       // Board Initialization
       //
       Board_init();

       //
       // Enables CPU interrupts
       //
       Interrupt_enableMaster();



    while(1)
    {
         GPIO_togglePin(myGPIOOutput0);
         for(i=0; i<1000; i++)
         {
           	DEVICE_DELAY_US(1000);
         }
    }
    return 0;
}

//
// End of File
//

