/******************************************************************
 文 档 名：      HX_DSC280049_CLB_siggen
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB siggen
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB siggen
                      此示例使用 CLB1 生成矩形波，并使用 CLB2 检查CLB1 产生的矩形波不超过定义的
                      占空比和周期限制


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void CLB_init(void);
void XBAR_init(void);
void Pinmux_init(void);

__interrupt void clb2ISR(void);


//gpreg1 = 0 : not used
uint32_t gpreg1 = 1;
uint32_t limit = 397;


int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB2, &clb2ISR);

    Pinmux_init();
    XBAR_init();
    CLB_init();

    initTILE1(CLB1_BASE);
    initTILE2(CLB2_BASE);

    Interrupt_enable(INT_CLB2);

    EINT;
    ERTM;

    CLB_setGPREG(CLB1_BASE, gpreg1);
    CLB_writeInterface(CLB2_BASE, CLB_ADDR_COUNTER_0_MATCH1, limit);

    for(;;)
    {

    }
}

void Pinmux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_2_OUTPUTXBAR1);
}

void XBAR_init(void)
{
	XBAR_setCLBMuxConfig(XBAR_AUXSIG0, XBAR_CLB_MUX01_CLB1_OUT4);
	XBAR_enableCLBMux(XBAR_AUXSIG0, XBAR_MUX01);

	XBAR_setCLBMuxConfig(XBAR_AUXSIG1, XBAR_CLB_MUX03_CLB1_OUT5);
	XBAR_enableCLBMux(XBAR_AUXSIG1, XBAR_MUX03);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX01);

}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);

	CLB_setOutputMask(CLB2_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB2_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB2_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_CLB_AUXSIG0);
	CLB_configGPInputMux(CLB2_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_enableSynchronization(CLB2_BASE, CLB_IN0);
	CLB_selectInputFilter(CLB2_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB2_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB2_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_CLB_AUXSIG1);
	CLB_configGPInputMux(CLB2_BASE, CLB_IN1, CLB_GP_IN_MUX_EXTERNAL);
	CLB_enableSynchronization(CLB2_BASE, CLB_IN1);
	CLB_selectInputFilter(CLB2_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_setGPREG(CLB2_BASE,0);
	CLB_enableCLB(CLB2_BASE);
}

void clb2ISR(void)
{
//	ESTOP0;
	GPIO_togglePin(0);

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
//
// End of File
//

