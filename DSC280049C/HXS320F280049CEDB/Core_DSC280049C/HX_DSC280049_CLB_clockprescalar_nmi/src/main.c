/******************************************************************
 文 档 名：      HX_DSC280049_CLB_clockprescalar_nmi
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB_clockprescalar_nmi
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB_clockprescalar_nmi
           CLB 模块的预标量用于对 CLB 时钟进行分频，并将其用作 TILE 逻辑
                      的输入。此外，HLC 模块还用于生成 NMI 中断。此模块仅适用于 CLB 类型 2 及更高版本


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);

__interrupt void clb1ISR(void);
__interrupt void clb1NMIISR(void);

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();
    Interrupt_register(INT_CLB1, &clb1ISR);
    Interrupt_register(INT_NMI, &clb1NMIISR);
    Interrupt_enable(INT_CLB1);
    Interrupt_enable(INT_NMI);

    SysCtl_enableNMIGlobalInterrupt();

    PinMux_init();
    CLB_init();

    initTILE1(CLB1_BASE);

    CLB_enableCLB(CLB1_BASE);

    EINT;
    ERTM;

    for(;;)
    {

    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);
}

void CLB_init(void)
{
	CLB_enableNMI(CLB1_BASE);
	CLB_configureClockPrescalar(CLB1_BASE, 10000);
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_CLB_PSCLK);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_enableSynchronization(CLB1_BASE, CLB_IN0);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_disableCLB(CLB1_BASE);
}

__interrupt void clb1ISR(void)
{
    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}

__interrupt void clb1NMIISR(void)
{

    if (SYSCTL_NMI_CLBNMI & SysCtl_getNMIFlagStatus())
    {
        //
        // CLB NMI
        //
        GPIO_togglePin(0);

    }
    SysCtl_clearAllNMIFlags();
}

//
// End of File
//

