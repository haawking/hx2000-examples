/******************************************************************
 文 档 名：       HX_DSC280049_SPI_loopback.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
		使用SPI内部回环测试模式，不使用FIFO或中断。
		一个数据流被发送，然后比较接收到的数据。
		发送数据
		0000 0001 0002 0003 0004 0005 0006 0007 .... FFFE FFFF 0000


 外部接线：


 现象：在Expressions或Variables或者LiveView窗口观察变量
		sData 发送数据
		rData 接收数据


 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"
uint16_t sData = 0;                  // Send data
uint16_t rData = 0;                  // Receive data

int main(void)
{

    Device_init();


    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();

    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Board initialization
    //
    Board_init();

    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;


    //
    // Loop forever. Suspend or place breakpoints to observe the buffers.
    //
    while(1)
    {
        // Transmit data
        SPI_writeDataNonBlocking(mySPI0_BASE, sData);

        // Block until data is received and then return it
        rData = SPI_readDataBlockingNonFIFO(mySPI0_BASE);

        // Check received data against sent data
        if(rData != sData)
        {
            // Something went wrong. rData doesn't contain expected data.
            ESTOP0;
        }

        sData++;

    }

    return 0;
}

//
// End of File
//

