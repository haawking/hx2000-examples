/******************************************************************
 文 档 名：       HX_DSC280049_SPI_loopback
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     SPI数字环回测试
 -------------------------- 例程使用说明 --------------------------
 功能描述：   SPI数字环回测试
                     该程序使用SPI模块的内部环回测试模式。这是一个非常基本的环回，不使用 FIFO 或中断。发送数据流，然后与接收到的数据流进行比较。
           pinmux 和 SPI 模块通过 sysconfig 文件进行配置。

发送的数据如下所示：
  0000 0001 0002 0003 0004 0005 0006 0007 .... FFFE FFFF 0000

这种模式永远重复。

 外部连接：
    无

调试 ：在Variables或  Live View中观察变量
   sData - 发送数据
   rData - 接收数据

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

void Pinmux_init(void);
void SPI_init(void);


int main(void)
{
    uint16_t sData = 0;                  // Send data
    uint16_t rData = 0;                  // Receive data

    Device_init();
    Device_initGPIO();
    Interrupt_initModule();
    Interrupt_initVectorTable();

    Pinmux_init();
    SPI_init();

    EINT;
    ERTM;

    while(1)
    {
        // 发送数据
        SPI_writeDataNonBlocking(SPIA_BASE, sData);

        // 等待，直到收到数据，然后返回数据
        rData = SPI_readDataBlockingNonFIFO(SPIA_BASE);

        // 将接收的数据与发送的数据进行对比
        if(rData != sData)
        {
            // 出错，rData 不包含预期数据。
        	asm("NOP");
        }
        else
        {
            GPIO_togglePin(0);
        }

        sData++;

    }
}

void Pinmux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_16_SPIA_SIMO);
	GPIO_setPadConfig(16, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_10_SPIA_SOMI);
	GPIO_setPadConfig(10, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(10, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_18_SPIA_CLK);
	GPIO_setPadConfig(18, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(18, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_11_SPIA_STE);
	GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(11, GPIO_QUAL_ASYNC);
}

void SPI_init(void)
{
	SPI_disableModule(SPIA_BASE);
	SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0, SPI_MODE_MASTER, 1600000, 16);
	SPI_setSTESignalPolarity(SPIA_BASE, SPI_STE_ACTIVE_LOW);
	SPI_disableFIFO(SPIA_BASE);
	SPI_enableLoopback(SPIA_BASE);
	SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_STOP_AFTER_TRANSMIT);
	SPI_enableModule(SPIA_BASE);
}

//
// End of File
//

