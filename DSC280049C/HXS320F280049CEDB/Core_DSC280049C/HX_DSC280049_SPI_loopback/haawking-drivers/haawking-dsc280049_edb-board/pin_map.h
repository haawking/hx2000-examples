//###########################################################################
//
// FILE:   pin_map.h
//
// TITLE:  Definitions of pin mux info for gpio.c.
//
//###########################################################################

#ifndef __PIN_MAP_H__
#define __PIN_MAP_H__

//*****************************************************************************
// 0x00000003 = MUX register value
// 0x0000000C = GMUX register value
// 0x0000FF00 = Shift amount within mux registers
// 0xFFFF0000 = Offset of MUX register
//*****************************************************************************


#define GPIO_0_GPIO0                    0x000C0000U
#define GPIO_0_EPWM1_A                  0x000C0001U
#define GPIO_0_I2CA_SDA                 0x000C0006U

#define GPIO_1_GPIO1                    0x000C0200U
#define GPIO_1_EPWM1_B                  0x000C0201U
#define GPIO_1_I2CA_SCL                 0x000C0206U

#define GPIO_2_GPIO2                    0x000C0400U
#define GPIO_2_EPWM2_A                  0x000C0401U
#define GPIO_2_OUTPUTXBAR1              0x000C0405U
#define GPIO_2_PMBUSA_SDA               0x000C0406U
#define GPIO_2_SCIA_TX                  0x000C0409U
#define GPIO_2_FSIRXA_D1                0x000C040AU
                                             
#define GPIO_3_GPIO3                    0x000C0600U
#define GPIO_3_EPWM2_B                  0x000C0601U
#define GPIO_3_OUTPUTXBAR2              0x000C0602U
#define GPIO_3_PMBUSA_SCL               0x000C0606U
#define GPIO_3_SPIA_CLK                 0x000C0607U
#define GPIO_3_SCIA_RX                  0x000C0609U
#define GPIO_3_FSIRXA_D0                0x000C060AU
                                             
#define GPIO_4_GPIO4                    0x000C0800U
#define GPIO_4_EPWM3_A                  0x000C0801U
#define GPIO_4_OUTPUTXBAR3              0x000C0805U
#define GPIO_4_CANA_TX                  0x000C0806U
#define GPIO_4_FSIRXA_CLK               0x000C080AU
                                             
#define GPIO_5_GPIO5                    0x000C0A00U
#define GPIO_5_EPWM3_B                  0x000C0A01U
#define GPIO_5_OUTPUTXBAR3              0x000C0A03U
#define GPIO_5_CANA_RX                  0x000C0A06U
#define GPIO_5_SPIA_STE                 0x000C0A07U
#define GPIO_5_FSITXA_D1                0x000C0A09U
                                             
#define GPIO_6_GPIO6                    0x000C0C00U
#define GPIO_6_EPWM4_A                  0x000C0C01U
#define GPIO_6_OUTPUTXBAR4              0x000C0C02U
#define GPIO_6_SYNCOUT                  0x000C0C03U
#define GPIO_6_EQEP1_A                  0x000C0C05U
#define GPIO_6_CANB_TX                  0x000C0C06U
#define GPIO_6_SPIB_SOMI                0x000C0C07U
#define GPIO_6_FSITXA_D0                0x000C0C09U
                                             
#define GPIO_7_GPIO7                    0x000C0E00U
#define GPIO_7_EPWM4_B                  0x000C0E01U
#define GPIO_7_OUTPUTXBAR5              0x000C0E03U
#define GPIO_7_EQEP1_B                  0x000C0E05U
#define GPIO_7_CANB_RX                  0x000C0E06U
#define GPIO_7_SPIB_SIMO                0x000C0E07U
#define GPIO_7_FSITXA_CLK               0x000C0E09U
                                             
#define GPIO_8_GPIO8                    0x000C1000U
#define GPIO_8_EPWM5_A                  0x000C1001U
#define GPIO_8_CANB_TX                  0x000C1002U
#define GPIO_8_ADCSOCAO                 0x000C1003U
#define GPIO_8_EQEP1_STROBE             0x000C1005U
#define GPIO_8_SCIA_TX                  0x000C1006U
#define GPIO_8_SPIA_SIMO                0x000C1007U
#define GPIO_8_I2CA_SCL                 0x000C1009U
#define GPIO_8_FSITXA_D1                0x000C100AU
                                             
#define GPIO_9_GPIO9                    0x000C1200U
#define GPIO_9_EPWM5_B                  0x000C1201U
#define GPIO_9_SCIB_TX                  0x000C1202U
#define GPIO_9_OUTPUTXBAR6              0x000C1203U
#define GPIO_9_EQEP1_INDEX              0x000C1205U
#define GPIO_9_SCIA_RX                  0x000C1206U
#define GPIO_9_SPIA_CLK                 0x000C1207U
#define GPIO_9_FSITXA_D0                0x000C120AU
                                             
#define GPIO_10_GPIO10                  0x000C1400U
#define GPIO_10_EPWM6_A                 0x000C1401U
#define GPIO_10_CANB_RX                 0x000C1402U
#define GPIO_10_ADCSOCBO                0x000C1403U
#define GPIO_10_EQEP1_A                 0x000C1405U
#define GPIO_10_SCIB_TX                 0x000C1406U
#define GPIO_10_SPIA_SOMI               0x000C1407U
#define GPIO_10_I2CA_SDA                0x000C1409U
#define GPIO_10_FSITXA_CLK              0x000C140AU
                                             
#define GPIO_11_GPIO11                  0x000C1600U
#define GPIO_11_EPWM6_B                 0x000C1601U
#define GPIO_11_SCIB_RX                 0x000C1602U
#define GPIO_11_OUTPUTXBAR7             0x000C1603U
#define GPIO_11_EQEP1_B                 0x000C1605U
#define GPIO_11_SPIA_STE                0x000C1607U
#define GPIO_11_FSIRXA_D1               0x000C1609U
                                             
#define GPIO_12_GPIO12                  0x000C1800U
#define GPIO_12_EPWM7_A                 0x000C1801U
#define GPIO_12_CANB_TX                 0x000C1802U
#define GPIO_12_EQEP1_STROBE            0x000C1805U
#define GPIO_12_SCIB_TX                 0x000C1806U
#define GPIO_12_PMBUSA_CTL              0x000C1807U
#define GPIO_12_FSIRXA_D0               0x000C1809U
                                             
#define GPIO_13_GPIO13                  0x000C1A00U
#define GPIO_13_EPWM7_B                 0x000C1A01U
#define GPIO_13_CANB_RX                 0x000C1A02U
#define GPIO_13_EQEP1_INDEX             0x000C1A05U
#define GPIO_13_SCIB_RX                 0x000C1A06U
#define GPIO_13_PMBUSA_ALERT            0x000C1A07U
#define GPIO_13_FSIRXA_CLK              0x000C1A09U
                                             
#define GPIO_14_GPIO14                  0x000C1C00U
#define GPIO_14_EPWM8_A                 0x000C1C01U
#define GPIO_14_SCIB_TX                 0x000C1C02U
#define GPIO_14_OUTPUTXBAR3             0x000C1C06U
#define GPIO_14_PMBUSA_SDA              0x000C1C07U
#define GPIO_14_SPIB_CLK                0x000C1C09U
#define GPIO_14_EQEP2_A                 0x000C1C0AU
                                             
#define GPIO_15_GPIO15                  0x000C1E00U
#define GPIO_15_EPWM8_B                 0x000C1E01U
#define GPIO_15_SCIB_RX                 0x000C1E02U
#define GPIO_15_OUTPUTXBAR4             0x000C1E06U
#define GPIO_15_PMBUSA_SCL              0x000C1E07U
#define GPIO_15_SPIB_STE                0x000C1E09U
#define GPIO_15_EQEP2_B                 0x000C1E0AU

#define GPIO_16_GPIO16                  0x00100000U
#define GPIO_16_SPIA_SIMO               0x00100001U
#define GPIO_16_CANB_TX                 0x00100002U
#define GPIO_16_OUTPUTXBAR7             0x00100003U
#define GPIO_16_EPWM5_A                 0x00100005U
#define GPIO_16_SCIA_TX                 0x00100006U
#define GPIO_16_SD1_D1                  0x00100007U
#define GPIO_16_EQEP1_STROBE            0x00100009U
#define GPIO_16_PMBUSA_SCL              0x0010000AU
#define GPIO_16_XCLKOUT                 0x0010000BU
                                            
#define GPIO_17_GPIO17                  0x00100200U
#define GPIO_17_SPIA_SOMI               0x00100201U
#define GPIO_17_CANB_RX                 0x00100202U
#define GPIO_17_OUTPUTXBAR8             0x00100203U
#define GPIO_17_EPWM5_B                 0x00100205U
#define GPIO_17_SCIA_RX                 0x00100206U
#define GPIO_17_SD1_C1                  0x00100207U
#define GPIO_17_EQEP1_INDEX             0x00100209U
#define GPIO_17_PMBUSA_SDA              0x0010020AU
                                      
#define GPIO_18_GPIO18_X2               0x00100400U
#define GPIO_18_SPIA_CLK                0x00100401U
#define GPIO_18_SCIB_TX                 0x00100402U
#define GPIO_18_CANA_RX                 0x00100403U
#define GPIO_18_EPWM6_A                 0x00100405U
#define GPIO_18_I2CA_SCL                0x00100406U
#define GPIO_18_SD1_D2                  0x00100407U
#define GPIO_18_EQEP2_A                 0x00100409U
#define GPIO_18_PMBUSA_CTL              0x0010040AU
#define GPIO_18_XCLKOUT                 0x0010040BU
                                        
#define GPIO_20_GPIO20                  0x00100800U
                                            
#define GPIO_21_GPIO21                  0x00100A00U
                                            
#define GPIO_22_GPIO22_VFBSW            0x00100C00U
#define GPIO_22_EQEP1_STROBE            0x00100C01U
#define GPIO_22_SCIB_TX                 0x00100C03U
#define GPIO_22_SPIB_CLK                0x00100C06U
#define GPIO_22_SD1_D4                  0x00100C07U
#define GPIO_22_LINA_TX                 0x00100C09U
                                            
#define GPIO_23_GPIO23_VSW              0x00100E00U
#define GPIO_23_GPIO23                  0x00100E04U
                                            
#define GPIO_24_GPIO24                  0x00101000U
#define GPIO_24_OUTPUTXBAR1             0x00101001U
#define GPIO_24_EQEP2_A                 0x00101002U
#define GPIO_24_EPWM8_A                 0x00101005U
#define GPIO_24_SPIB_SIMO               0x00101006U
#define GPIO_24_SD1_D1                  0x00101007U
#define GPIO_24_PMBUSA_SCL              0x0010100AU
#define GPIO_24_SCIA_TX                 0x0010100BU
#define GPIO_24_ERRORSTS                0x0010100DU
                                            
#define GPIO_25_GPIO25                  0x00101200U
#define GPIO_25_OUTPUTXBAR2             0x00101201U
#define GPIO_25_EQEP2_B                 0x00101202U
#define GPIO_25_SPIB_SOMI               0x00101206U
#define GPIO_25_SD1_C1                  0x00101207U
#define GPIO_25_FSITXA_D1               0x00101209U
#define GPIO_25_PMBUSA_SDA              0x0010120AU
#define GPIO_25_SCIA_RX                 0x0010120BU
                                            
#define GPIO_26_GPIO26                  0x00101400U
#define GPIO_26_OUTPUTXBAR3             0x00101401U
#define GPIO_26_EQEP2_INDEX             0x00101402U
#define GPIO_26_SPIB_CLK                0x00101406U
#define GPIO_26_SD1_D2                  0x00101407U
#define GPIO_26_FSITXA_D0               0x00101409U
#define GPIO_26_PMBUSA_CTL              0x0010140AU
#define GPIO_26_I2CA_SDA                0x0010140BU
                                            
#define GPIO_27_GPIO27                  0x00101600U
#define GPIO_27_OUTPUTXBAR4             0x00101601U
#define GPIO_27_EQEP2_STROBE            0x00101602U
#define GPIO_27_SPIB_STE                0x00101606U
#define GPIO_27_SD1_C2                  0x00101607U
#define GPIO_27_FSITXA_CLK              0x00101609U
#define GPIO_27_PMBUSA_ALERT            0x0010160AU
#define GPIO_27_I2CA_SCL                0x0010160BU
                                            
#define GPIO_28_GPIO28                  0x00101800U
#define GPIO_28_SCIA_RX                 0x00101801U
#define GPIO_28_EPWM7_A                 0x00101803U
#define GPIO_28_OUTPUTXBAR5             0x00101805U
#define GPIO_28_EQEP1_A                 0x00101806U
#define GPIO_28_SD1_D3                  0x00101807U
#define GPIO_28_EQEP2_STROBE            0x00101809U
#define GPIO_28_LINA_TX                 0x0010180AU
#define GPIO_28_SPIB_CLK                0x0010180BU
#define GPIO_28_ERRORSTS                0x0010180DU
                                            
#define GPIO_29_GPIO29                  0x00101A00U
#define GPIO_29_SCIA_TX                 0x00101A01U
#define GPIO_29_EPWM7_B                 0x00101A03U
#define GPIO_29_OUTPUTXBAR6             0x00101A05U
#define GPIO_29_EQEP1_B                 0x00101A06U
#define GPIO_29_SD1_C3                  0x00101A07U
#define GPIO_29_EQEP2_INDEX             0x00101A09U
#define GPIO_29_LINA_RX                 0x00101A0AU
#define GPIO_29_SPIB_STE                0x00101A0BU
#define GPIO_29_ERRORSTS                0x00101A0DU
                                            
#define GPIO_30_GPIO30                  0x00101C00U
#define GPIO_30_CANA_RX                 0x00101C01U
#define GPIO_30_SPIB_SIMO               0x00101C03U
#define GPIO_30_OUTPUTXBAR7             0x00101C05U
#define GPIO_30_EQEP1_STROBE            0x00101C06U
#define GPIO_30_SD1_D4                  0x00101C07U
                                            
#define GPIO_31_GPIO31                  0x00101E00U
#define GPIO_31_CANA_TX                 0x00101E01U
#define GPIO_31_SPIB_SOMI               0x00101E03U
#define GPIO_31_OUTPUTXBAR8             0x00101E05U
#define GPIO_31_EQEP1_INDEX             0x00101E06U
#define GPIO_31_SD1_C4                  0x00101E07U
#define GPIO_31_FSIRXA_D1               0x00101E09U

#define GPIO_32_GPIO32                  0x008C0000U
#define GPIO_32_I2CA_SDA                0x008C0001U
#define GPIO_32_SPIB_CLK                0x008C0003U
#define GPIO_32_EPWM8_B                 0x008C0005U
#define GPIO_32_LINA_TX                 0x008C0006U
#define GPIO_32_SD1_D3                  0x008C0007U
#define GPIO_32_FSIRXA_D0               0x008C0009U
#define GPIO_32_CANA_TX                 0x008C000AU
                                          
#define GPIO_33_GPIO33                  0x008C0200U
#define GPIO_33_I2CA_SCL                0x008C0201U
#define GPIO_33_SPIB_STE                0x008C0203U
#define GPIO_33_OUTPUTXBAR4             0x008C0205U
#define GPIO_33_LINA_RX                 0x008C0206U
#define GPIO_33_SD1_C3                  0x008C0207U
#define GPIO_33_FSIRXA_CLK              0x008C0209U
#define GPIO_33_CANA_RX                 0x008C020AU
                                          
#define GPIO_34_GPIO34                  0x008C0400U
#define GPIO_34_OUTPUTXBAR1             0x008C0401U
#define GPIO_34_PMBUSA_SDA              0x008C0406U
                                     
#define GPIO_35_GPIO35                  0x008C0600U
#define GPIO_35_SCIA_RX                 0x008C0601U
#define GPIO_35_I2CA_SDA                0x008C0603U
#define GPIO_35_CANA_RX                 0x008C0605U
#define GPIO_35_PMBUSA_SCL              0x008C0606U
#define GPIO_35_LINA_RX                 0x008C0607U
#define GPIO_35_EQEP1_A                 0x008C0609U
#define GPIO_35_PMBUSA_CTL              0x008C060AU
#define GPIO_35_TDI                     0x008C060FU
                                      
#define GPIO_37_GPIO37                  0x008C0A00U
#define GPIO_37_OUTPUTXBAR2             0x008C0A01U
#define GPIO_37_I2CA_SCL                0x008C0A03U
#define GPIO_37_SCIA_TX                 0x008C0A05U
#define GPIO_37_CANA_TX                 0x008C0A06U
#define GPIO_37_LINA_TX                 0x008C0A07U
#define GPIO_37_EQEP1_B                 0x008C0A09U
#define GPIO_37_PMBUSA_ALERT            0x008C0A0AU
#define GPIO_37_TDO                     0x008C0A0FU
                                       
#define GPIO_39_GPIO39                  0x008C0E00U
#define GPIO_39_CANB_RX                 0x008C0E06U
#define GPIO_39_FSIRXA_CLK              0x008C0E07U
                                        
#define GPIO_40_GPIO40                  0x008C1000U
#define GPIO_40_PMBUSA_SDA              0x008C1006U
#define GPIO_40_FSIRXA_D0               0x008C1007U
#define GPIO_40_SCIB_TX                 0x008C1009U
#define GPIO_40_EQEP1_A                 0x008C100AU
                                       
#define GPIO_41_GPIO41                  0x008C1200U
                                    
#define GPIO_42_GPIO42                  0x008C1400U
                                  
#define GPIO_43_GPIO43                  0x008C1600U
                                 
#define GPIO_44_GPIO44                  0x008C1800U
                                 
#define GPIO_45_GPIO45                  0x008C1A00U
                                  
#define GPIO_46_GPIO46                  0x008C1C00U
                                 
#define GPIO_47_GPIO47                  0x008C1E00U

#define GPIO_48_GPIO48                  0x00900000U
                                      
#define GPIO_49_GPIO49                  0x00900200U
                                   
#define GPIO_50_GPIO50                  0x00900400U
                                 
#define GPIO_51_GPIO51                  0x00900600U
                               
#define GPIO_52_GPIO52                  0x00900800U
                               
#define GPIO_53_GPIO53                  0x00900A00U
                                     
#define GPIO_54_GPIO54                  0x00900C00U
                                   
#define GPIO_55_GPIO55                  0x00900E00U
                               
#define GPIO_56_GPIO56                  0x00901000U
#define GPIO_56_SPIA_CLK                0x00901001U
#define GPIO_56_EQEP2_STROBE            0x00901005U
#define GPIO_56_SCIB_TX                 0x00901006U
#define GPIO_56_SD1_D3                  0x00901007U
#define GPIO_56_SPIB_SIMO               0x00901009U
#define GPIO_56_EQEP1_A                 0x0090100BU
                      
#define GPIO_57_GPIO57                  0x00901200U
#define GPIO_57_SPIA_STE                0x00901201U
#define GPIO_57_EQEP2_INDEX             0x00901205U
#define GPIO_57_SCIB_RX                 0x00901206U
#define GPIO_57_SD1_C3                  0x00901207U
#define GPIO_57_SPIB_SOMI               0x00901209U
#define GPIO_57_EQEP1_B                 0x0090120BU
                       
#define GPIO_58_GPIO58                  0x00901400U
#define GPIO_58_OUTPUTXBAR1             0x00901405U
#define GPIO_58_SPIB_CLK                0x00901406U
#define GPIO_58_SD1_D4                  0x00901407U
#define GPIO_58_LINA_TX                 0x00901409U
#define GPIO_58_CANB_TX                 0x0090140AU
#define GPIO_58_EQEP1_STROBE            0x0090140BU
              
#define GPIO_59_GPIO59                  0x00901600U
#define GPIO_59_OUTPUTXBAR2             0x00901605U
#define GPIO_59_SPIB_STE                0x00901606U
#define GPIO_59_SD1_C4                  0x00901607U
#define GPIO_59_LINA_RX                 0x00901609U
#define GPIO_59_CANB_RX                 0x0090160AU
#define GPIO_59_EQEP1_INDEX             0x0090160BU

#define GPIO_247_GPIO247                0x03900E00U
                                           
#define GPIO_246_GPIO246                0x03900C00U

#define GPIO_230_GPIO230                0x038C0C00U

#define GPIO_241_GPIO241                0x03900200U

#define GPIO_242_GPIO242                0x03900400U

#define GPIO_237_GPIO237                0x038C1A00U

#define GPIO_224_GPIO224                0x038C0000U

#define GPIO_244_GPIO244                0x03900800U

#define GPIO_226_GPIO226                0x038C0400U

#define GPIO_233_GPIO233                0x038C1200U
                
#define GPIO_239_GPIO239                0x038C1E00U
                                   
#define GPIO_228_GPIO228                0x038C0800U
                                
#define GPIO_232_GPIO232                0x038C1000U
                                          
#define GPIO_231_GPIO231                0x038C0E00U
                                       
#define GPIO_227_GPIO227                0x038C0600U

#define GPIO_245_GPIO245                0x03900A00U

#define GPIO_235_GPIO235                0x038C1600U

#define GPIO_236_GPIO236                0x038C1800U

#define GPIO_234_GPIO234                0x038C1400U

#define GPIO_243_GPIO243                0x03900600U

#define GPIO_225_GPIO225                0x038C0200U

#define GPIO_238_GPIO238                0x038C1C00U

#define GPIO_229_GPIO229                0x038C0A00U

#define GPIO_240_GPIO240                0x03900000U

#endif // PIN_MAP_H
