/******************************************************************
 文 档 名：      HX_DSC280049_ADC_adc_soc_socftware_sync
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：        ADC同步软件触发例程
 -------------------------- 例程使用说明 --------------------------
 功能描述：ADC SOC 软件同步。
          此示例使用输入 X-BAR 的输入 5 通道强制转换 ADCA 和 ADCC 上的一些电压。
          输入 5 通过切换 GPIO0电平 触发，也可以使用任何其他 GPIO。这种方法将确保所有ADC
          同时开始转换。

 外部连接：A2,A3,C2,C3 连接转换信号

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	 myADC0Result0    -   引脚A2电压的数字量
     myADC0Result1  - 引脚A3电压的数字量
     myADC1Result0  - 引脚C2电压的数字量
     myADC1Result1  - 引脚C3电压的数字量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t myADC0Result0;
uint16_t myADC0Result1;
uint16_t myADC1Result0;
uint16_t myADC1Result1;

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 板级初始化
    // - 配置ADC并为ADCA和ADCC上电
    // - 设置GPIO0 作为输入XBAR 通道5 的触发源，在例程中GPIO0被使用，其他的的GPIO
    //   也可以被使用，GPIO默认配置是浮空输入，一旦代码配置执行后
    //   它被设置成输出并且能改变输出值。
    // - 设置ADC 采样时间和比较值
    //
    Board_init();

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 在循环中一直转换
    //
    do
    {
        //
        // 转换，等待转换完成并且存储数据
        //

        //
        // 切换GPIO0的输出电平，这将产生一个触发并通过输入XBAR 通道5
        // 触发所有ADCsboth ADCs
        //
        GPIO_writePin(0U, 1U); // Set pin
        GPIO_writePin(0U, 0U); // Clear pin

        //
        // 等待 ADCA 完成，然后接受该标志。 由于两个ADC同步运行，
        // 因此无需等待两个ADC的转换完成标志
        //
        while(ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1) == 0U);
        ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);

        //
        // 存储数据
        //
        myADC0Result0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        myADC0Result1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        myADC1Result0 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
        myADC1Result1 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER1);

        //
        // 在这里 转换数据被存储到
        // myADC0Result0, myADC0Result1, myADC1Result0, 和 myADC1Result1
        //

        //
        // Software breakpoint, hit run again to get updated conversions
        //
      //  asm("   ESTOP0");

    }
    while(1);
}

//
// End of file
//
