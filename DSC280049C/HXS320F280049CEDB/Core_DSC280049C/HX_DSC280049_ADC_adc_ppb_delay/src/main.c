/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_ppb_delay
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       使用后处理模块进行ADC延迟捕获。
 -------------------------- 例程使用说明 --------------------------
功能描述：    使用后处理模块进行ADC延迟捕获。
                      两个异步ADC触发器设置为：
           -ePWM1,周期2048，SOC0触发转换，引脚A0
           -ePWM2,周期9999，SOC1触发转换，引脚A2
  每次转换都会在转换结束时生成一个 ISR。 在SOC0 的 ISR函数里，转换计数器递增，
  并检查 PPB 以确定样本是否延迟。


程序运行后，内存将包含：
    -转换 ：使用 SOC0 延迟的转换序列。
    -延迟 ：每个转换的相应延迟。

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Defines
//
#define DELAY_BUFFER_SIZE 30

//
// Globals
//
uint32_t conversion_count;
uint32_t conversion[DELAY_BUFFER_SIZE];
uint16_t delay[DELAY_BUFFER_SIZE];
volatile uint16_t delay_index;

//
// Functional Prototypes
//
void configureEPWM(uint32_t epwmBase, uint16_t period);
__interrupt void adcA1ISR(void);
__interrupt void adcA2ISR(void);

int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉.
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR)。
    //
    Interrupt_initVectorTable();

    //
    // 初始化程序变量。
    //
    conversion_count = 0;
    for(delay_index = 0; delay_index < DELAY_BUFFER_SIZE; delay_index++)
    {
        delay[delay_index] = 0;
        conversion[delay_index] = 0;
    }

    //
    // 设置ADC:
    // 信号模式：单端
    // 转换分辨率：12-bit
    // 配置ADC SOCs和PPB 以进行延时捕获。
    // 注册并启用中断。
    //
    Board_init();

    //
    // 配置ePWM的异步周期。
    //
    configureEPWM(EPWM1_BASE, 2048);
    configureEPWM(EPWM2_BASE, 9999);

    //
    // 启用全局中断和更高优先级的实时调试事件。
    //
    EINT;  // 使能全局中断
    ERTM;  // 使能全局实时中断

    //
    // 开启ePWM
    // 使能PWM的异步时钟
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // 在循环中转换
    //
    do
    {
        delay_index = 0;

        //
        // 使能SOCA触发
        //
        EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
        EPWM_enableADCTrigger(EPWM2_BASE, EPWM_SOC_A);

        //
        // 解冻ePWM计数器并向上计数。
        //
        EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);
        EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_UP);

        //
        //等待延迟转化列表填充中断。
        //
        while(DELAY_BUFFER_SIZE > delay_index){}

        //
        // 停止ePWMs，失能SOCAs并且冻结计数器。
        //
        EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
        EPWM_disableADCTrigger(EPWM2_BASE, EPWM_SOC_A);
        EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
        EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);

        //
        //software breakpoint
        //
    //    ESTOP0;

        //
        //conversion[] 将显示哪些转换被延迟了
        //delay[] 将显示每次转化延迟了多长时间
        //conversion_count将显示转换转化总数
        //conversion_count -DELAY_BUFFER_SIZE是样本数
        // 立即开始，没有任何延迟。
        //
    }
    while(1);
}

//
// configureEPWM - Setup SOC and compare values for EPWM and set specified
// period.
//
void configureEPWM(uint32_t epwmBase, uint16_t period)
{
    //
    // 禁止SOCA触发
    //
    EPWM_disableADCTrigger(epwmBase, EPWM_SOC_A);

    //
    // 在CMPA向上计数时触发SOCA。
    //
    EPWM_setADCTriggerSource(epwmBase, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);

    //
    // 在第一个事件上产生脉冲。
    //
    EPWM_setADCTriggerEventPrescale(epwmBase, EPWM_SOC_A, 1U);

    //
    // 将比较器 A 值设置为周期的大约一半。
    //
    EPWM_setCounterCompareValue(epwmBase, EPWM_COUNTER_COMPARE_A, period/2);

    //
    // 设置指定周期。
    //
    EPWM_setTimeBasePeriod(epwmBase, period);

    //
    // 设置本地ePWM模块时钟分频器为1。
    //
    EPWM_setClockPrescaler(epwmBase,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // 冻结定时器。
    //
    EPWM_setTimeBaseCounterMode(epwmBase, EPWM_COUNTER_MODE_STOP_FREEZE);
}

//
// ADC A Interrupt 1 ISR - 检查转换延迟
//
__interrupt void adcA1ISR(void)
{
    //
    //如果样本没有延迟，DLYSTAMP 将读取 2。
    //
    if(2 < ADC_getPPBDelayTimeStamp(ADCA_BASE, ADC_PPB_NUMBER1))
    {
        //
        //如果 DLYSTAMP > 2，则样本延迟了 （DLYSTAMP - 2） 个时钟周期 （SYSCLK）。
        //
        conversion[delay_index] = conversion_count;
        delay[delay_index] = ADC_getPPBDelayTimeStamp(ADCA_BASE, ADC_PPB_NUMBER1) - 2;
        delay_index++;

        //
        //此处可对采样延迟采取校正措施。
        //...
        //
    }

    //
    //在这读取 ADC 采样值。
    //...
    //

    conversion_count++;

    //
    // 清空中断标志位。
    //
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // 检查检车是否发生溢出
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    //
    // 确认中断。
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// ADC A Interrupt 2 ISR - 空中断函数，产生异步信号到ADC A ISR 1。
//
__interrupt void adcA2ISR(void)
{
    //
    //在这读取 ADC采样值。
    //

    //
    //清空中断标志位
    //
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);

    //
    // 检查是否发生溢出
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER2))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER2);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);
    }

    //
    // 确认中断。
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP10);
}

//
// End of file
//
