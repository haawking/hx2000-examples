/*
 * board.h
 *
 *  Created on: 2023��7��7��
 *      Author: admin
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_
//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//
// Included Files
//

#include "driverlib.h"
#include "device.h"

//*****************************************************************************
//
// PinMux Configurations
//
//*****************************************************************************

//*****************************************************************************
//
// ADC Configurations
//
//*****************************************************************************
#define myADC0_BASE ADCA_BASE
#define myADC0_RESULT_BASE ADCARESULT_BASE
#define myADC0_SOC0 ADC_SOC_NUMBER0
#define myADC0_FORCE_SOC0 ADC_FORCE_SOC0
#define myADC0_SAMPLE_WINDOW_SOC0 80
#define myADC0_TRIGGER_SOURCE_SOC0 ADC_TRIGGER_EPWM1_SOCA
#define myADC0_CHANNEL_SOC0 ADC_CH_ADCIN0
#define myADC0_SOC1 ADC_SOC_NUMBER1
#define myADC0_FORCE_SOC1 ADC_FORCE_SOC1
#define myADC0_SAMPLE_WINDOW_SOC1 80
#define myADC0_TRIGGER_SOURCE_SOC1 ADC_TRIGGER_EPWM2_SOCA
#define myADC0_CHANNEL_SOC1 ADC_CH_ADCIN2
#define myADC0_PPB1 ADC_PPB_NUMBER1
#define myADC0_SOC_PPB1 ADC_SOC_NUMBER0
void myADC0_init();


//*****************************************************************************
//
// ASYSCTL Configurations
//
//*****************************************************************************

//*****************************************************************************
//
// INTERRUPT Configurations
//
//*****************************************************************************

// Interrupt Settings for INT_myADC0_1
#define INT_myADC0_1 INT_ADCA1
#define INT_myADC0_1_INTERRUPT_ACK_GROUP INTERRUPT_ACK_GROUP1
extern __interrupt void adcA1ISR(void);

// Interrupt Settings for INT_myADC0_2
#define INT_myADC0_2 INT_ADCA2
#define INT_myADC0_2_INTERRUPT_ACK_GROUP INTERRUPT_ACK_GROUP10
extern __interrupt void adcA2ISR(void);

//*****************************************************************************
//
// Board Configurations
//
//*****************************************************************************
void    Board_init();
void    ADC_init();
void    ASYSCTL_init();
void    INTERRUPT_init();
void    PinMux_init();

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif /* SRC_BOARD_H_ */
