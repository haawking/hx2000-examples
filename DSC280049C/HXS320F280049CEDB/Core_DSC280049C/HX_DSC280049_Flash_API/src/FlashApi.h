/*
 * FlashApi.h
 *
 *  Created on: 2023年11月27日
 *      Author: liyuyao
 */

#ifndef SRC_FLASHAPI_H_
#define SRC_FLASHAPI_H_

typedef struct {
    Uint32  FirstFailAddr;
    Uint16  ExpectedData;
    Uint16  ActualData;
}FLASH_ST;

Uint32 * FlashAPITable = (uint32 *)0x7fff54;

/*
 * void  Fapi_initializeApi(uint32 u32HclkFrequency)
 * 功能：对整个Flash（Bank0 & Bank1）进行初始化，在对Flash（包含UserOtp）进行擦除和编写操作之前需要首先调用此函数。
 * 入参：u32HclkFrequency: 以兆为单位的频率，范围应该在[10, 160]
 * */
#define Fapi_initializeAPI             (void (*)(uint32 u32HclkFrequency))*FlashAPITable
/*
* Uint32 Flash280049_UserOtp32BitProgram
* 			(Uint32 bank, Uint32 OTPAddr, Uint32 Data)
* 功能：将32位数据写入相应OTPAddr地址。
*入参：bank：bank0 还是 bank1，
*入参：OTPAddr：在UserOtp区域要进行写入的地址 [0x680000, 0x681000)
*入参：Data：要写入的内容（Uint32类型）
*返回值： 0代表成功，12代表OTPAddrr的取值超限，40代表编程失败。
* */
#define Flash280049_UserOTP_32BIT_Program  (Uint16 (*)(Uint8 bank,Uint32 *OTPAddr,Uint32 Data,Uint8 zone_select,FLASH_ST *FProgStatus))(*(FlashAPITable+13))
/*
* Uint32 Flash280049_UserOtpBitProgram
* 			(Uint32 bank, Uint32 OTPAddr,
* 			Uint32 BitOffset)
* 功能：本函数可以对UserOtp区域内的任意一个比特写入数据
*入参：bank：bank0 还是 bank1，
*入参：OTPAddr：在UserOtp区域要进行写入的地址 [0x680000, 0x681000)
*入参：BitOffset：要写入的bit偏移量
*返回值： 0代表成功，12代表OTPAddrr的取值超限，40代表编程失败。
* */
#define Flash280049_UserOTP_BIT_Program     (Uint16 (*)(Uint8 bank,Uint32 *OTPAddr,Uint32 Field,Uint8 zone_select, FLASH_ST *FProgStatus))*(FlashAPITable + 15)

/*
 * Uint32 Flash280049_SectorErase(Uint32 bank, Uint32 SectorIdx)
 * 功能：对Flash的指定的Sector区域进行擦除操作，一个Flash Bank包含128个Sector，
 *       每次函数执行完后会检测该区域的第一个值是否为0xFFFFFFFF。如果是则返回擦除成功。
 * 入参：bank：bank0 还是 bank1，
 * 入参：SectorIdx：代表第几个sector，[1, 128]
 * 返回值：0代表成功，20代表SectorIdx的取值超限，22代表擦除失败
 * */
#define Flash280049_SectorErase  (Uint16 (*)(Uint8 Bank, Uint8 SectorIdx, FLASH_ST *FEraseStat))*(FlashAPITable+3)
/*
 * Uint32 Flash280049_MassErase(Uint32 bank)
 * 功能：对整个Flash的Bank区域进行擦除（0x600000, 0x640000）
 * 入参：bank：0代表bank0， 1代表bank1，
 * 返回值：0代表成功，22代表擦除失败。
 * */
#define Flash280049_MassErase    (Uint16 (*)(Uint8 Bank,FLASH_ST *FEraseStat))*(FlashAPITable+6)
/*
 *
 * Flash280049_Program (Uint32 bank, Uint32 FlashAddr, Uint32 *data, Uint32 Length)
*
*功能：本函数可以对Flash的任意区域进行任意长度的编程，
	   限制条件：
		1.每个256bits只能编写一次，再编写需先对该区域进行Erase操作；
		2.每次调用该函数只能在一个bank内进行编程，不能同时处理两个bank；
*入参：bank：bank0 还是 bank1，
*入参：*FlashAddr：在Flash区域内要进行编程的地址 [0x600000, 0x680000)
*入参：*data：要写入的数据内容
*入参：Length：要写入的数据长度（以Uint32为单位）
*	  注意：要确保每次输入的起始地址到结束地址要在一个bank内
*返回值：0代表成功，12代表地址不正确，30代表烧写失败。
* */
#define Flash280049_Program     (Uint16 (*)(Uint8 Bank,Uint32 *FlashAddr,Uint32 *BufAddr,Uint32 Length,FLASH_ST *FProgStatus))*(FlashAPITable+10)

/*
 * Uint32 Flash280049_UserOtpProgram
 *        (Uint32 bank, Uint32 OTPAddr, Uint32 *BufAddr,
 *        Uint32 Length,Uint32 *OTPTempBuffer)
 * 功能：本函数可以对UserOtp的任意区域进行任意长度的编程。限制：每次调用该函数只能在一个bank内进行编程，不能同时处理两个bank
 *入参：bank：bank0 还是 bank1，
 *入参：*OTPAddr：在UserOtp区域要进行编程的地址 [0x680000, 0x681000)
 *入参：*BufAddr：要写入内容的地址。
 *入参：Length：要写入的数据长度（以Uint32为单位）
 *入参：OTPTempBuffer是长度为Length的32位数组的首地址，用于验证。
 *返回值： 0代表成功，12代表OTPAddrr的取值超限，40代表编程失败。
 * */
#define Flash280049_UserOTP_Program    (Uint16 (*)(Uint8 Bank,Uint32 *OTPAddr,Uint32 *BufAddr,Uint32 Length,Uint32 *OTPTempBuffer,Uint8 zone_select,FLASH_ST *FProgStatus))*(FlashAPITable+12)

#endif /* SRC_FLASHAPI_H_ */
