/******************************************************************
 文 档 名：       HX_DSC280049_ADC_software
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：        ADC软件触发例程
 -------------------------- 例程使用说明 --------------------------
 功能描述：本例程基于软件触发ADCA和ADCC的电压转换。
                   在ADCA转换完成前，ADCC不会转换，因此，ADCs将不会异步运行，然而，这比允许
          ADCs异步转换（例如，使用ePWM触发）的效率低得多。

 外部连接：A0,A1,C1,C2 连接转换信号

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	 myADC0Result0    -   引脚A0电压的数字量
     myADC0Result1  - 引脚A1电压的数字量
     myADC1Result0  - 引脚C1电压的数字量
     myADC1Result0  - 引脚C2电压的数字量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// 全局变量 Globals
//
uint16_t myADC0Result0;
uint16_t myADC0Result1;
uint16_t myADC1Result0;
uint16_t myADC1Result1;

//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 配置ADC,初始化SOCs的软件触发。
    //
    Board_init();

    //
    //打开全局中断和实时中断。
    //
    EINT;
    ERTM;

    //
    // 循环
    //
    while(1)
    {
        //
        // 转换，等待转换完成并且存储数据
        //
        ADC_forceMultipleSOC(myADC0_BASE, (ADC_FORCE_SOC0 | ADC_FORCE_SOC1));

        //
        //等待ADCA转换完成，然后接收标志位
        //
        while(ADC_getInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1) == false)
        {
        }
        ADC_clearInterruptStatus(myADC0_BASE, ADC_INT_NUMBER1);

        ADC_forceMultipleSOC(myADC1_BASE, (ADC_FORCE_SOC0 | ADC_FORCE_SOC1));
        //
        // 等待ADCC转换完成，然后接收标志位
        //
        while(ADC_getInterruptStatus(myADC1_BASE, ADC_INT_NUMBER1) == false)
        {
        }
        ADC_clearInterruptStatus(myADC1_BASE, ADC_INT_NUMBER1);

        //
        // 存储数据
        //
        myADC0Result0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        myADC0Result1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        myADC1Result0 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
        myADC1Result1 = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER1);

        //
        // Software breakpoint. At this point, conversion results are stored in
        // myADC0Result0, myADC0Result1, myADC1Result0, and myADC1Result1.
        //
        // Hit run again to get updated conversions.
        //
      //  ESTOP0;
    }
}

