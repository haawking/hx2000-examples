/******************************************************************
 文 档 名：      HX_DSC280049_SPI_external_loopback
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：     SPI数字环回，无需使用FIFO和中断
 -------------------------- 例程使用说明 --------------------------
 功能描述：   SPI数字环回，无需使用FIFO和中断
                     该程序使用两个SPI模块之间的外部环回。本例中未同时使用SPI FIFO和中断。
           SPIA配置为从机，SPI B配置为主机。此示例演示了主机和从机同时发送和
                     接收数据的全双工通信。


 外部连接：
   -GPIO24 and GPIO16 - SPIPICO
   -GPIO31 and GPIO17 - SPIPOCI
   -GPIO22 and GPIO56 - SPICLK
   -GPIO27 and GPIO57 - SPISTE

调试 ：在Variables或  Live View中观察变量
   -  TxData_SPIA - 从 SPIA发送的数据 (从机)
   -  TxData_SPIB - 从 SPIB发送的数据 (主机)
   -  RxData_SPIA - SPIA接收的数据 (从机)
   -  RxData_SPIB - SPIB接收的数据 (主机)

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

uint16_t TxData_SPIA[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
uint16_t RxData_SPIA[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uint16_t TxData_SPIB[] = {0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
uint16_t RxData_SPIB[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

void Pinmux_init(void);
void SPI_init(void);

int main(void)
{
    uint16_t i;

    Device_init();
    Device_initGPIO();
    Interrupt_initModule();
    Interrupt_initVectorTable();

    Pinmux_init();
    SPI_init();

    for(i = 0; i < 16; i++)
    {
        //
        // 设置SPI从机的发送buffer。
        //
        SPI_writeDataNonBlocking(SPIA_BASE, TxData_SPIA[i]);

        //
        // 设置主机发送buffer，这将触发数据发送
        //
        SPI_writeDataNonBlocking(SPIB_BASE, TxData_SPIB[i]);

        //
        // 读取接受的数据
        //
        RxData_SPIA[i] = SPI_readDataBlockingNonFIFO(SPIA_BASE);
        RxData_SPIB[i] = SPI_readDataBlockingNonFIFO(SPIB_BASE);

        //
        // 检查接受的数据
        //
        if(RxData_SPIA[i] != TxData_SPIB[i])
        {
        	asm("NOP");
        }
        else
        {
            GPIO_togglePin(0);
        }
        if(RxData_SPIB[i] != TxData_SPIA[i])
        {
        	asm("NOP");
        }
        else
        {
            GPIO_togglePin(0);
        }
    }

    while(1);
    return 0;
}

void Pinmux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_16_SPIA_SIMO);
	GPIO_setPadConfig(16, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_17_SPIA_SOMI);
	GPIO_setPadConfig(17, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(17, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_56_SPIA_CLK);
	GPIO_setPadConfig(56, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(56, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_57_SPIA_STE);
	GPIO_setPadConfig(57, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(57, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_24_SPIB_SIMO);
	GPIO_setPadConfig(24, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(24, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_31_SPIB_SOMI);
	GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(31, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_22_SPIB_CLK);
	GPIO_setPadConfig(22, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(22, GPIO_QUAL_ASYNC);

	GPIO_setPinConfig(GPIO_27_SPIB_STE);
	GPIO_setPadConfig(27, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(27, GPIO_QUAL_ASYNC);
}

void SPI_init(void)
{
	SPI_disableModule(SPIA_BASE);
	SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0, SPI_MODE_SLAVE, 800000, 16);
	SPI_setSTESignalPolarity(SPIA_BASE, SPI_STE_ACTIVE_LOW);
	SPI_disableFIFO(SPIA_BASE);
	SPI_disableLoopback(SPIA_BASE);
	SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_FREE_RUN);
	SPI_enableModule(SPIA_BASE);

	SPI_disableModule(SPIB_BASE);
	SPI_setConfig(SPIB_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0, SPI_MODE_MASTER, 800000, 16);
	SPI_setSTESignalPolarity(SPIB_BASE, SPI_STE_ACTIVE_LOW);
	SPI_disableFIFO(SPIB_BASE);
	SPI_disableLoopback(SPIB_BASE);
	SPI_setEmulationMode(SPIB_BASE, SPI_EMULATION_FREE_RUN);
	SPI_enableModule(SPIB_BASE);
}

//
// End of File
//

