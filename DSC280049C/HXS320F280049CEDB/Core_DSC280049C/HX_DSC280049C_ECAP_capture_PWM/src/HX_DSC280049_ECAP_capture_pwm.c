/******************************************************************
 文 档 名：       HX_DSC280049_ECAP_capture_pwm.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 	 	 配置eEPWM3A
 	 	向上计数
 	 	周期从500到8000
 	 	PRD翻转输出
 	 	配置eCAP1捕获eEPWM3A输出上升沿到下降沿的时间

 外部接线：
 	 	GPIO16  eCAP1
		GPIO4	ePWM3A
		连接 GPIO16 和 GPIO4
 现象：在Expressions或Variables或者LiveView窗口观察变量
	ecap1PassCount	成功捕获计数
	ecap1IntCount	中断计数

 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


//
// Defines
//
#define PWM3_TIMER_MIN     500U
#define PWM3_TIMER_MAX     8000U
#define EPWM_TIMER_UP      1U
#define EPWM_TIMER_DOWN    0U

//
// Globals
//
uint32_t ecap1IntCount;
uint32_t ecap1PassCount;
uint32_t epwm3TimerDirection;
volatile uint16_t cap2Count;
volatile uint16_t cap3Count;
volatile uint16_t cap4Count;
volatile uint16_t epwm3PeriodCount;

//
// Function Prototypes
//
void error(void);
void initECAP(void);
void initEPWM(void);



int main(void)
{
    Device_init();

    //
       // Disable pin locks and enable internal pullups.
       //
       Device_initGPIO();

       //
       // Initialize PIE and clear PIE registers. Disables CPU interrupts.
       //
       Interrupt_initModule();

       //
       // Initialize the PIE vector table with pointers to the shell Interrupt
       // Service Routines (ISR).
       //
       Interrupt_initVectorTable();

       //
       // Configure GPIO4/5 as ePWM3A/3B
       //
       GPIO_setPadConfig(4,GPIO_PIN_TYPE_STD);
       GPIO_setPinConfig(GPIO_4_EPWM3_A);
       GPIO_setPadConfig(5,GPIO_PIN_TYPE_STD);
       GPIO_setPinConfig(GPIO_5_EPWM3_B);

       //
       // Board initialization
       // Configure GPIO 16 as eCAP input
       // Enable interrupts required for this example
       //
       Board_init();

       //
       // Configure ePWM
       //
       initEPWM();

       //
       // Initialize counters:
       //
       cap2Count = 0U;
       cap3Count = 0U;
       cap4Count = 0U;
       ecap1IntCount = 0U;
       ecap1PassCount = 0U;
       epwm3PeriodCount = 0U;

       //
       // Enable Global Interrupt (INTM) and Real time interrupt (DBGM)
       //
       EINT;

    while(1)
    	;
    return 0;
}


//
// initEPWM - Configure ePWM
//
void initEPWM()
{
    //
    // Disable sync(Freeze clock to PWM as well)
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // Configure ePWM
    //       Counter runs in up-count mode.
    //       Action qualifier will toggle output on period match
    //
    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBasePeriod(EPWM3_BASE, PWM3_TIMER_MIN);
    EPWM_setPhaseShift(EPWM3_BASE, 0U);
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_TOGGLE,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
    EPWM_setClockPrescaler(EPWM3_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_2);

    epwm3TimerDirection = EPWM_TIMER_UP;

    //
    // Enable sync and clock to PWM
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
}

//
// myECAP0 ISR
//
__interrupt void INT_myECAP0_ISR(void)
{
    //
    // Get the capture counts. Each capture should be 2x the ePWM count
    // because of the ePWM clock divider.
    //
    cap2Count = ECAP_getEventTimeStamp(myECAP0_BASE, ECAP_EVENT_2);
    cap3Count = ECAP_getEventTimeStamp(myECAP0_BASE, ECAP_EVENT_3);
    cap4Count = ECAP_getEventTimeStamp(myECAP0_BASE, ECAP_EVENT_4);

    //
    // Compare the period value with the captured count
    //
    epwm3PeriodCount = EPWM_getTimeBasePeriod(EPWM3_BASE);

    if(cap2Count > ((epwm3PeriodCount *2) + 2U) ||
       cap2Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    if(cap3Count > ((epwm3PeriodCount *2) + 2U) ||
       cap3Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    if(cap4Count > ((epwm3PeriodCount *2) + 2U) ||
       cap4Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    ecap1IntCount++;

    //
    // Keep track of the ePWM direction and adjust period accordingly to
    // generate a variable frequency PWM.
    //
    if(epwm3TimerDirection == EPWM_TIMER_UP)
    {
        if(epwm3PeriodCount < PWM3_TIMER_MAX)
        {
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
        else
        {
           epwm3TimerDirection = EPWM_TIMER_DOWN;
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
    }
    else
    {
        if(epwm3PeriodCount > PWM3_TIMER_MIN)
        {
            EPWM_setTimeBasePeriod(EPWM3_BASE, --epwm3PeriodCount);
        }
        else
        {
           epwm3TimerDirection = EPWM_TIMER_UP;
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
    }

    //
    // Count correct captures
    //
    ecap1PassCount++;

    //
    // Clear interrupt flags for more interrupts.
    //
    ECAP_clearInterrupt(myECAP0_BASE,ECAP_ISR_SOURCE_CAPTURE_EVENT_4);
    ECAP_clearGlobalInterrupt(myECAP0_BASE);

    //
    // Start eCAP
    //
    ECAP_reArm(myECAP0_BASE);

    //
    // Acknowledge the group interrupt for more interrupts.
    //
    Interrupt_clearACKGroup(INT_myECAP0_INTERRUPT_ACK_GROUP);
}

//
// error - Error function
//
void error()
{
    ESTOP0;
}

//
// End of File
//

