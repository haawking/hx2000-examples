#ifndef DRIVER_INCLUSIVE_TERMINOLOGY_MAPPING_H_
#define DRIVER_INCLUSIVE_TERMINOLOGY_MAPPING_H_


//*****************************************************************************
// CLB
//*****************************************************************************
#define  CLB_LOCAL_IN_MUX_SPIPICO_PERIPHERAL       			CLB_LOCAL_IN_MUX_SPISIMO_SLAVE          
#define  CLB_LOCAL_IN_MUX_SPIPICO_CONTROLLER       			CLB_LOCAL_IN_MUX_SPISIMO_MASTER         
#define  CLB_GLOBAL_IN_MUX_SPI1_SPIPOCI_CONTROLLER       	CLB_GLOBAL_IN_MUX_SPI1_SPISOMI_MASTER   
#define  CLB_GLOBAL_IN_MUX_SPI1_SPIPTE       				CLB_GLOBAL_IN_MUX_SPI1_SPISTE           
#define  CLB_GLOBAL_IN_MUX_SPI2_SPIPOCI_CONTROLLER       	CLB_GLOBAL_IN_MUX_SPI2_SPISOMI_MASTER   
#define  CLB_GLOBAL_IN_MUX_SPI2_SPIPTE       				CLB_GLOBAL_IN_MUX_SPI2_SPISTE           
#define  CLB_GLOBAL_IN_MUX_SPI3_SPIPOCI_CONTROLLER       	CLB_GLOBAL_IN_MUX_SPI3_SPISOMI_MASTER   
#define  CLB_GLOBAL_IN_MUX_SPI3_SPIPTE       				CLB_GLOBAL_IN_MUX_SPI3_SPISTE           
#define  CLB_GLOBAL_IN_MUX_SPI4_SPIPOCI_CONTROLLER       	CLB_GLOBAL_IN_MUX_SPI4_SPISOMI_MASTER   
#define  CLB_GLOBAL_IN_MUX_SPI4_SPIPTE       				CLB_GLOBAL_IN_MUX_SPI4_SPISTE           

//*****************************************************************************
// FSI
//*****************************************************************************
#define  FSI_TX_MAIN_CORE_RESET                             FSI_TX_MASTER_CORE_RESET    
#define  FSI_RX_MAIN_CORE_RESET                             FSI_RX_MASTER_CORE_RESET    

//*****************************************************************************
// LIN
//*****************************************************************************
#define  LIN_MODE_LIN_RESPONDER                             LIN_MODE_LIN_SLAVE    
#define  LIN_MODE_LIN_COMMANDER                             LIN_MODE_LIN_MASTER   
		                     
#define  LIN_MSG_FILTER_IDRESPONDER                         LIN_MSG_FILTER_IDSLAVE
		                     
#define  LIN_setIDResponderTask                             LIN_setIDSlaveTask    

//*****************************************************************************
// SPI
//*****************************************************************************
#define  SPI_MODE_PERIPHERAL                                SPI_MODE_SLAVE          
#define  SPI_MODE_CONTROLLER                                SPI_MODE_MASTER         
#define  SPI_MODE_PERIPHERAL_OD                             SPI_MODE_SLAVE_OD       
#define  SPI_MODE_CONTROLLER_OD                             SPI_MODE_MASTER_OD      
		                          
#define  SPI_PTE_ACTIVE_LOW                                 SPI_STE_ACTIVE_LOW      
#define  SPI_PTE_ACTIVE_HIGH                                SPI_STE_ACTIVE_HIGH     
		                          
#define  SPI_setPTESignalPolarity                           SPI_setSTESignalPolarity


//*****************************************************************************
// Interrupt
//*****************************************************************************
#define  Interrupt_enableGlobal                             Interrupt_enableMaster 
#define  Interrupt_disableGlobal                            Interrupt_disableMaster


//*****************************************************************************
// SysCtrl
//*****************************************************************************
#define  SysCtl_AccessController                            SysCtl_AccessMaster


//*****************************************************************************
// GPIO
//*****************************************************************************
#define  GPIO_setControllerCore                             GPIO_setMasterCore



//*****************************************************************************
// Memcfg
//*****************************************************************************
#define  MemCfg_LSRAMMControllerSel                         MemCfg_LSRAMMMasterSel     
#define  MEMCFG_LSRAMCONTROLLER_CPU_ONLY                    MEMCFG_LSRAMMASTER_CPU_ONLY
#define  MEMCFG_LSRAMCONTROLLER_CPU_CLA1                    MEMCFG_LSRAMMASTER_CPU_CLA1
#define  MemCfg_setLSRAMControllerSel                       MemCfg_setLSRAMMasterSel   


//*****************************************************************************
// I2C
//*****************************************************************************
#define  I2C_CONTROLLER_SEND_MODE                           I2C_MASTER_SEND_MODE   
#define  I2C_CONTROLLER_RECEIVE_MODE                        I2C_MASTER_RECEIVE_MODE
#define  I2C_TARGET_SEND_MODE                               I2C_SLAVE_SEND_MODE    
#define  I2C_TARGET_RECEIVE_MODE                            I2C_SLAVE_RECEIVE_MODE 
#define  I2C_INT_ADDR_TARGET                                I2C_INT_ADDR_SLAVE     
#define  I2C_STS_ADDR_TARGET                                I2C_STS_ADDR_SLAVE     
#define  I2C_STS_TARGET_DIR                                 I2C_STS_SLAVE_DIR      
#define  I2C_INTSRC_ADDR_TARGET                             I2C_INTSRC_ADDR_SLAVE  
		                                                    
#define  I2C_initController                                 I2C_initMaster         
#define  I2C_setTargetAddress                               I2C_setSlaveAddress    
#define  I2C_setOwnAddress                                  I2C_setOwnSlaveAddress 

//*****************************************************************************
// PMBUS
//*****************************************************************************
#define  PMBUS_CONTROLLER_ENABLE_PRC_CALL                   PMBUS_MASTER_ENABLE_PRC_CALL      
#define  PMBUS_CONTROLLER_ENABLE_GRP_CMD                    PMBUS_MASTER_ENABLE_GRP_CMD       
#define  PMBUS_CONTROLLER_ENABLE_PEC                        PMBUS_MASTER_ENABLE_PEC           
#define  PMBUS_CONTROLLER_ENABLE_EXT_CMD                    PMBUS_MASTER_ENABLE_EXT_CMD       
#define  PMBUS_CONTROLLER_ENABLE_CMD                        PMBUS_MASTER_ENABLE_CMD           
#define  PMBUS_CONTROLLER_ENABLE_READ                       PMBUS_MASTER_ENABLE_READ          
#define  PMBUS_INT_TARGET_ADDR_READY                        PMBUS_INT_SLAVE_ADDR_READY        
#define  PMBUS_TARGET_ENABLE_MANUAL_ACK                     PMBUS_SLAVE_ENABLE_MANUAL_ACK     
#define  PMBUS_TARGET_ENABLE_PEC_PROCESSING                 PMBUS_SLAVE_ENABLE_PEC_PROCESSING 
#define  PMBUS_TARGET_TRANSMIT_PEC                          PMBUS_SLAVE_TRANSMIT_PEC          
#define  PMBUS_TARGET_ENABLE_MANUAL_CMD_ACK                 PMBUS_SLAVE_ENABLE_MANUAL_CMD_ACK 
#define  PMBUS_TARGET_DISABLE_ADDRESS_MASK                  PMBUS_SLAVE_DISABLE_ADDRESS_MASK  
#define  PMBUS_TARGET_AUTO_ACK_1_BYTES                      PMBUS_SLAVE_AUTO_ACK_1_BYTES      
#define  PMBUS_TARGET_AUTO_ACK_2_BYTES                      PMBUS_SLAVE_AUTO_ACK_2_BYTES      
#define  PMBUS_TARGET_AUTO_ACK_3_BYTES                      PMBUS_SLAVE_AUTO_ACK_3_BYTES      
#define  PMBUS_TARGET_AUTO_ACK_4_BYTES                      PMBUS_SLAVE_AUTO_ACK_4_BYTES      
#define  PMBUS_ENABLE_TARGET_ALERT                          PMBUS_ENABLE_SLAVE_ALERT          
#define  PMBUS_ENABLE_TARGET_MODE                           PMBUS_ENABLE_SLAVE_MODE           
#define  PMBUS_ENABLE_CONTROLLER_MODE                       PMBUS_ENABLE_MASTER_MODE          
#define  PMBUS_INTSRC_TARGET_ADDR_READY                     PMBUS_INTSRC_SLAVE_ADDR_READY     
#define  PMBus_getControllerData                            PMBus_getMasterData               
#define  PMBus_getTargetData                                PMBus_getSlaveData                
		                                                     
#define  PMBus_configControlleer                            PMBus_configMaster                
#define  PMBus_putControllerData                            PMBus_putMasterData               
#define  PMBus_putTargetData                                PMBus_putSlaveData                
#define  PMBus_initTargetMode                               PMBus_initSlaveMode               
#define  PMBus_initControllerMode                           PMBus_initMasterMode              

//*****************************************************************************
// SDFM
//*****************************************************************************
#define  SDFM_enableMainInterrupt                           SDFM_enableMasterInterrupt 
#define  SDFM_disableMainInterrupt                          SDFM_disableMasterInterrupt
#define  SDFM_enableMainFilter                              SDFM_enableMasterFilter    
#define  SDFM_disableMainFilter                             SDFM_disableMasterFilter   
		                                          
#define  SDFM_MAIN_INTERRUPT_FLAG                           SDFM_MASTER_INTERRUPT_FLAG 

#endif /* DRIVER_INCLUSIVE_TERMINOLOGY_MAPPING_H_ */
