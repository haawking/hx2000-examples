/******************************************************************
 文 档 名：      HX_DSC280049_CLB_timer
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB 定时器
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB 定时器
           在此示例中，COUNTER 模块用于创建定时事件。显示了 GP Register 的使用。通过设置/清除
      GP寄存器中的位，定时器被启动、停止或改变方向。定时器事件（1 个时钟周期）的输出导
           出到 GPIO。使用 HLC 模块从计时器事件生成中断。GPIO 也在 CLB ISR 内部切换。间接CLB
            寄存器访问用于更新定时器事件匹配值，活动计数器寄存器用于修改定时器的频率 。

外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);
void XBAR_init(void);

void Delay(void);

__interrupt void clb1ISR(void);

#define RESET_TIMER     1
#define ENABLE_TIMER    2
#define COUNTUP_MODE    4

uint16_t i ;

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);

    PinMux_init();
    XBAR_init();
    CLB_init();

    initTILE1(CLB1_BASE);

    CLB_setGPREG(CLB1_BASE, ENABLE_TIMER | COUNTUP_MODE);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    for(;;)
    {
    	GPIO_togglePin(1);
    	Delay();
        CLB_configCounterLoadMatch(CLB1_BASE, CLB_CTR0, 0, 2000, 0);
        GPIO_togglePin(1);
        Delay();
        CLB_configCounterLoadMatch(CLB1_BASE, CLB_CTR0, 0, 1000, 0);
    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);
    GPIO_setDirectionMode(1, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_24_OUTPUTXBAR1);
}

void XBAR_init(void)
{
	XBAR_setOutputLatchMode(XBAR_OUTPUT1, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT1, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX01);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

__interrupt void clb1ISR(void)
{
    GPIO_togglePin(0);
    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}

void Delay(void)
{
	for(i=0; i<100; i++)
	{
		SysCtl_delay(1000000);
	}
}
//
// End of File
//

