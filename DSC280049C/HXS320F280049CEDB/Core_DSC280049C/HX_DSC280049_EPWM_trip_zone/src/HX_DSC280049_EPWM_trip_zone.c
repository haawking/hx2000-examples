/******************************************************************
 文 档 名：       HX_DSC280049_EPWM_trip_zone.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 	 	 配置eEPWM1 eEPWM2
 	 	ePWM1A TZ1 单次跳闸源
 	 	ePWM2A TZ2 为周期跳闸源

		TZ1接高，开始测试，示波器查看ePWM1A ePWM2A的输出波形，
 	 	拉TZ1到低，查看效果。

 	 	例程也可以使用X-BAR输入，外部跳闸源GPIO12可以接到X-BAR，然后接到TZ1.
		TZ-Event定义为ePWM1A经历单次跳闸，ePWM2A经历周期性跳闸。

 外部接线：
		ePWM1A GPIO0
		ePWM2A GPIO2
		TZ1    GPIO12

 现象：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


//
// Globals
//
uint32_t  epwm1TZIntCount;
uint32_t  epwm2TZIntCount;

//
// Function Prototypes
//
__interrupt void epwm1TZISR(void);
__interrupt void epwm2TZISR(void);

int main(void)
{
	 //
	    // Initialize global variables
	    //
	    epwm1TZIntCount = 0U;
	    epwm2TZIntCount = 0U;

    Device_init();
    //
       // Disable pin locks and enable internal pull-ups.
       //
       Device_initGPIO();

       //
       // Initialize PIE and clear PIE registers. Disables CPU interrupts.
       //
       Interrupt_initModule();

       //
       // Initialize the PIE vector table with pointers to the shell Interrupt
       // Service Routines (ISR).
       //
       Interrupt_initVectorTable();

       //
       // Disable sync(Freeze clock to PWM as well)
       //
       SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

       //
       // Configure ePWM1, ePWM2, and TZ GPIOs/Modules
       //
       Board_init();

       //
       // Enable sync and clock to PWM
       //
       SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

       //
       // Enable Global Interrupt (INTM) and real time interrupt (DBGM)
       //
       EINT;


       //
       // IDLE loop. Just sit and loop forever (optional):
       //

    while(1)
    	;
    return 0;
}


//
// epwm1TZISR - ePWM1 TZ ISR
//
__interrupt void epwm1TZISR(void)
{
    epwm1TZIntCount++;

    //
    // To re-enable the OST Interrupt, uncomment the below code:
    //
    // EPWM_clearTripZoneFlag(EPWM1_BASE,
    //                        (EPWM_TZ_INTERRUPT | EPWM_TZ_FLAG_OST));

    //
    // Acknowledge this interrupt to receive more interrupts from group 2
    //
    Interrupt_clearACKGroup(INT_myEPWM1_TZ_INTERRUPT_ACK_GROUP);
}

//
// epwm2TZISR - ePWM2 TZ ISR
//
__interrupt void epwm2TZISR(void)
{
    epwm2TZIntCount++;

    //
    // Toggle GPIO to notify when TZ is entered
    //
    GPIO_togglePin(myGPIO11);

    //
    // Clear the flags - we will continue to take this interrupt until the TZ
    // pin goes high.
    //
    EPWM_clearTripZoneFlag(myEPWM2_BASE, (EPWM_TZ_INTERRUPT | EPWM_TZ_FLAG_CBC));

    //
    // Acknowledge this interrupt to receive more interrupts from group 2
    //
    Interrupt_clearACKGroup(INT_myEPWM2_TZ_INTERRUPT_ACK_GROUP);
}



//
// End of File
//

