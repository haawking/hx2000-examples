/******************************************************************
 文 档 名：       HX_DSC280049_ADC_adc_burst_mode_epwm
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：       ADC突发模式
 -------------------------- 例程使用说明 --------------------------
功能描述：    ADC突发模式示例
                      此示例将 ePWM1 设置为使用突发模式定期触发 ADCA。
                      这允许在每次突发时对不同的通道进行采样。

                      每次突发触发 3 次转换。 A0 和 A1 是每次突发转换的一部分，
                      而第三次转换在 A2、A3 和 A4 之间轮换。 这允许以高速采样高重要性信号，
                      而以较低的速率采样低优先级信号。

           ADCA 中断 ISR 用于读取 ADCA 的结果。

外部连接：A0, A1, A2, A3, A4。

调试 ：在Expressions或Variables或者LiveView窗口观察变量
 	  adcAResult0 - 引脚A0的数字量
 	  adcAResult1 - 引脚A1的数字量
 	  adcAResult2 - 引脚A2的数字量
 	  adcAResult3 - 引脚A3的数字量
 	  adcAResult4 - 引脚A4的数字量

 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/

//
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t adcAResult0;
uint16_t adcAResult1;
uint16_t adcAResult2;
uint16_t adcAResult3;
uint16_t adcAResult4;


//
// Function Prototypes
//
void initEPWM();
__interrupt void adcABurstISR(void);

//
// Main
//
int main(void)
{
    //
    // 初始化设备时钟和外设
    //
    Device_init();

    //
    // 解锁PIN脚且使能内部上拉。
    //
    Device_initGPIO();

    //
    // 初始化PIE并且清空PIE寄存器，关闭CPU中断。
    //
    Interrupt_initModule();

    //
    // 使用指向 shell 中断的指针初始化 PIE中断向量表
    // 中断服务程序 (ISR).
    //
    Interrupt_initVectorTable();

    //
    // 设置ADCs：
    // 信号模式：单端
    // 转换分辨率：12 - bit
    //
    // 为 SOC7-15 启用突发模式，突发大小为 3，突发模式触发为 EPWM1 SOCA。
    // 选择要转换的通道并配置 S+H 持续时间。
    // 触发1：SOC7  (A0) + SOC8  (A1) + SOC9  (A2)
    // 触发2：SOC10 (A0) + SOC11 (A1) + SOC12 (A3)
    // 触发3：SOC13 (A0) + SOC14 (A1) + SOC15 (A4)
    //
    // 注册并启用中断：此示例中使用的中断将重新映射到此文件中的 ISR 函数。
    // 3 个中断中的每一个都将发生在 3 个不同突发结束时，
    // 但所有 3 个中断都将映射到处理相同 ADC 结果的ISR。
    //
    Board_init();

    //
    // 初始化PWM
    //
    initEPWM();

    //
    // 启用全局中断和更高优先级的实时调试事件
    //
    EINT;
    ERTM;

    //
    // 启动 ePWM1，使能 SOCA 并将计数器置于向上计数模式。
    //
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // 在循环中转换。
    //
    do
    {
        //
        // 等待ePWM触发导致ADC转换。
        // ADCA 突发 ISR 处理每组新的转换。
        //
    }
    while(1);
}

//
// 配置 ePWM1触发SOC的函数Function to configure ePWM1 to generate the SOC.
//
void initEPWM(void)
{
    //
    // 禁止SOCA。
    //
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    //
    // 将 SOC 配置为在第一个向上计数事件时触发
    //
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);

    //
    // 设置比较器A的值为1000并且周期为1999。
    // 假设ePWM时钟为100MHz，则采样率为50kHz。
    // 50MHz ePWM 时钟将提供 25kHz 采样率。
    // 也可以通过直接改变ePWM周期来调制采样率（确保比较A值小于周期）。
    //
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 1000);
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);

    //
    // 设置本地ePWM模块时钟分频为1。
    //
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    //
    // 冻结定时器。
    //
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}

//
// ADC A Interrupt Burst Mode ISR
//
__interrupt void adcABurstISR(void)
{
    uint16_t rrPointer;
    ADC_IntNumber burstIntSource;

    //
    //读取循环指针以确定刚刚完成的突发。
    //
    rrPointer = (HWREG(ADCA_BASE + ADC_O_SOCPRICTL) & 0x03E0) >> 5;
    switch(rrPointer){
        //
        // Burst 1
        //
        case 9:
            adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER7);
            adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER8);
            adcAResult2 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER9);
            burstIntSource = ADC_INT_NUMBER1;
        break;

        //
        // Burst 2
        //
        case 12:
            adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER10);
            adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER11);
            adcAResult3 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER12);
            burstIntSource = ADC_INT_NUMBER2;
        break;

        //
        // Burst 3
        //
        case 15:
            adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER13);
            adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14);
            adcAResult4 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER15);
            burstIntSource = ADC_INT_NUMBER3;
        break;

        default:
            ESTOP0; //处理意外 RR 指针值的错误。
    }

    //
    // 清空中断标志位
    //
    ADC_clearInterruptStatus(ADCA_BASE, burstIntSource);

    //
    // 检查是否溢出
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, burstIntSource))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, burstIntSource);
        ADC_clearInterruptStatus(ADCA_BASE, burstIntSource);
    }

    //
    // 确认中断。
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP10);
}

//
// End of file
//
