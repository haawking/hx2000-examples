/******************************************************************
 文 档 名：      HX_DSC280049_CLB_timer_two_states
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB  状态转换计时
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB  状态转换计时
                      在此示例中，计时器的设置与上一个示例相同。区别在于使用 FSM 子模块来切换 CLB
                       的输出，然后将其导出到 GPIO。FSM 模块充当单位存储器块。中断的设置格式与上
                       一个示例相同。通过比较 CLB 的输出和 ISR 中切换的 GPIO，可以看出 CLB 的中断延迟


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);
void XBAR_init(void);

__interrupt void clb1ISR(void);

#define RESET_TIMER     1
#define ENABLE_TIMER    2
#define COUNTUP_MODE    4

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);

    PinMux_init();
    XBAR_init();
    CLB_init();

    initTILE1(CLB1_BASE);

    CLB_setGPREG(CLB1_BASE, ENABLE_TIMER | COUNTUP_MODE);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    for(;;)
    {
        SysCtl_delay(32000000);
        CLB_configCounterLoadMatch(CLB1_BASE, CLB_CTR0, 0, 2000, 0);
        SysCtl_delay(32000000);
        CLB_configCounterLoadMatch(CLB1_BASE, CLB_CTR0, 0, 1000, 0);
        asm(" NOP");
    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

	GPIO_setPinConfig(GPIO_24_OUTPUTXBAR1);
}

void XBAR_init(void)
{
	XBAR_setOutputLatchMode(XBAR_OUTPUT1, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT1, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX01);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

__interrupt void clb1ISR(void)
{
    GPIO_togglePin(0);
    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
//
// End of File
//

