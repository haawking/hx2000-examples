/******************************************************************
 文 档 名：      HX_280049_auxiliary_pwm
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB辅助PWM
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB辅助PWM
                      此示例将 CLB tile 配置为辅助 PWM 生成器。该示例使用组合逻辑 （LUT）、
                      状态机 （FSM）、计数器和高级控制器 （HLC） 来演示使用 CLB 的 PWM 输出生成功能


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void PinMux_init(void);
void CLB_init(void);
void OUTPUTXBAR_init(void);

__interrupt void clb1ISR(void);

#define ENABLE_AUX_PWM      1
volatile uint16_t dutyValue=2000;

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);

    PinMux_init();
    CLB_init();
    OUTPUTXBAR_init();

    initTILE1(CLB1_BASE);

    CLB_setGPREG(CLB1_BASE, ENABLE_AUX_PWM);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    for(;;)
    {

    }
}

void PinMux_init(void)
{
	GPIO_setPinConfig(GPIO_0_GPIO0);
	GPIO_setPinConfig(GPIO_2_OUTPUTXBAR1);

	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);
	GPIO_setMasterCore(0, GPIO_CORE_CPU1);
}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

void OUTPUTXBAR_init(void)
{
	XBAR_setOutputLatchMode(XBAR_OUTPUT1, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT1, false);
	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX01);
}

void clb1ISR(void)
{
	GPIO_togglePin(0);
    if (dutyValue == 1500)
    {
        dutyValue = 1000U;
    }
    else
    {
        dutyValue = 1500U;
    }
    CLB_setHLCRegisters(CLB1_BASE, dutyValue, 0, 0, 0);
    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
//
// End of File
//

