/******************************************************************
 文 档 名：      HX_DSC280049_CLB_state_machine
 开 发 环 境：  Haawking IDE V2.1.9
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：         DSC280049
 使 用 库：
 作     用：
 说     明：      CLB 状态机
 -------------------------- 例程使用说明 --------------------------
功能描述：    CLB 状态机
                      本应用报告描述了创建此 CLB 示例的过程，并可用作使用 CLB 设计自定义逻辑的指
                      南。此示例使用 CLB TILE 中的所有子模块来实现一个完整的系统。


外部连接：


调试 ：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年11月28日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "clb_config.h"
#include "clb.h"

void GPIO_init(void);
void CLB_init(void);
void XBAR_init(void);
void EPWM_init(void);


#define UPDATE_PWM_COMPLETED_TAG    1
#define TRIGGER_PWM_UPDATE_SHIFT    2
volatile uint16_t canUpdate = 1;

volatile uint32_t clbPwmPeriod = 3200;
volatile uint32_t clbPwmDuty = 1600;
volatile uint16_t clbPwmUpdateNow = 1;

__interrupt void clb1ISR(void);
void updateClbPwm(uint32_t period, uint32_t duty);

int main(void)
{
    Device_init();
    Device_initGPIO();

    Interrupt_initModule();
    Interrupt_initVectorTable();

    Interrupt_register(INT_CLB1, &clb1ISR);

    GPIO_init();
    XBAR_init();
    CLB_init();

//    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CLB1);

    EPWM_init();

//    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    initTILE1(CLB1_BASE);

    Interrupt_enable(INT_CLB1);

    EINT;
    ERTM;

    CLB_clearInterruptTag(CLB1_BASE);

    while(1)
    {
        if (clbPwmUpdateNow)
        {
        	SysCtl_delay(500000);
            updateClbPwm(clbPwmPeriod, clbPwmDuty);
            clbPwmUpdateNow = 0;
        }
        asm(" NOP");
    }
}

void GPIO_init(void)
{
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_1_EPWM1_B);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);

	GPIO_setPinConfig(GPIO_4_GPIO4);
	GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(4, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(4, GPIO_DIR_MODE_IN);

	GPIO_setPinConfig(GPIO_5_GPIO5);
	GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(5, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(5, GPIO_DIR_MODE_IN);

	GPIO_setPinConfig(GPIO_31_OUTPUTXBAR8);
	GPIO_setPinConfig(GPIO_34_OUTPUTXBAR1);
}

void XBAR_init(void)
{
	XBAR_setInputPin(XBAR_INPUT1, 4);
	XBAR_setInputPin(XBAR_INPUT2, 5);

	XBAR_setOutputLatchMode(XBAR_OUTPUT8, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT8, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT8, XBAR_OUT_MUX01_CLB1_OUT4);
	XBAR_enableOutputMux(XBAR_OUTPUT8, XBAR_MUX01);

	XBAR_setOutputLatchMode(XBAR_OUTPUT1, false);
	XBAR_invertOutputSignal(XBAR_OUTPUT1, false);

	XBAR_setOutputMuxConfig(XBAR_OUTPUT1, XBAR_OUT_MUX03_CLB1_OUT5);
	XBAR_enableOutputMux(XBAR_OUTPUT1, XBAR_MUX03);

	XBAR_setCLBMuxConfig(XBAR_AUXSIG0, XBAR_CLB_MUX01_INPUTXBAR1);
	XBAR_enableCLBMux(XBAR_AUXSIG0, XBAR_MUX01);

	XBAR_setCLBMuxConfig(XBAR_AUXSIG1, XBAR_CLB_MUX03_INPUTXBAR2);
	XBAR_enableCLBMux(XBAR_AUXSIG1, XBAR_MUX03);

}

void CLB_init(void)
{
	CLB_setOutputMask(CLB1_BASE, (0UL << 0UL) | (1UL << 0UL) | (1UL << 2UL), true);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN0, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN0, CLB_GLOBAL_IN_MUX_CLB_AUXSIG0);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN0, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN0, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN1, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN1, CLB_GLOBAL_IN_MUX_CLB_AUXSIG1);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN1, CLB_GP_IN_MUX_EXTERNAL);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN1, CLB_FILTER_NONE);

	CLB_configLocalInputMux(CLB1_BASE, CLB_IN2, CLB_LOCAL_IN_MUX_GLOBAL_IN);
	CLB_configGlobalInputMux(CLB1_BASE, CLB_IN2, CLB_GLOBAL_IN_MUX_EPWM1A);
	CLB_configGPInputMux(CLB1_BASE, CLB_IN2, CLB_GP_IN_MUX_GP_REG);
	CLB_selectInputFilter(CLB1_BASE, CLB_IN2, CLB_FILTER_NONE);

	CLB_setGPREG(CLB1_BASE,0);
	CLB_enableCLB(CLB1_BASE);
}

void EPWM_init(void)
{
    EPWM_setClockPrescaler(EPWM1_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_2);
    EPWM_setTimeBasePeriod(EPWM1_BASE, 0);
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    EPWM_disablePhaseShiftLoad(EPWM1_BASE);
    EPWM_setPhaseShift(EPWM1_BASE, 0);
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 0);
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_B, 0);
    EPWM_setCounterCompareShadowLoadMode(EPWM1_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);
}

__interrupt void clb1ISR(void)
{
    uint16_t tag = CLB_getInterruptTag(CLB1_BASE);
    if (tag == UPDATE_PWM_COMPLETED_TAG)
    {
        canUpdate = 1;
        CLB_setGPREG(CLB1_BASE, 0);
    }

    CLB_clearInterruptTag(CLB1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}

void updateClbPwm(uint32_t period, uint32_t duty)
{
    if (canUpdate)
    {
        canUpdate = 0;
        CLB_writeInterface(CLB1_BASE, CLB_ADDR_COUNTER_1_LOAD, period);
        HWREG(CLB1_BASE + CLB_LOGICCTL + CLB_O_BUF_PTR) = 0U;
        HWREG(CLB1_BASE + CLB_DATAEXCH + CLB_O_PULL(0)) = period;
        HWREG(CLB1_BASE + CLB_DATAEXCH + CLB_O_PULL(1)) = duty;
        CLB_setGPREG(CLB1_BASE, 1 << TRIGGER_PWM_UPDATE_SHIFT);
    }
}
//
// End of File
//

