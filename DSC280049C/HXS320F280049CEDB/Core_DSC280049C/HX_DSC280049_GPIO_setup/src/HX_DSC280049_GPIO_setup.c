/******************************************************************
 文 档 名：       HX_DSC280049_GPIO_setup.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：此代码描述了如何配置芯片的GPIO，实际应用中可以把代码组合起来。
 	 	 这个例子只是配置了GPIO，没有实际操作GPIO。
 外部接线：

 现象：所有上拉电阻器均已启用。对于ePWMs，这可能不是我们想要的。
 	 通信端口（CAN、SPI、SCI、I2C）的输入是异步的
 	 跳闸引脚（TZ）的输入是异步的
	 eCAP和eQEP信号的输入与SYSCLKOUT同步
	 某些I/O和中断的输入可能具有采样窗口

 版 本：      V1.0.0
 时 间：      2023年7月12日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
//
// 包含文件
//

#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

//
// 定义
// 选择编译的例子.  只有一个例子可以设置为 1
// 其余的应该设置为 0.
//
#define EXAMPLE1 1  // 基本引脚配置例子
#define EXAMPLE2 0  // 通讯引脚配置例子

//
// 函数声明
//
void setup1GPIO(void);
void setup2GPIO(void);


int main(void)
{


	Device_init();

       // Initializes PIE and clear PIE registers. Disables CPU interrupts.
       // and clear all CPU interrupt flags.
       //
       Interrupt_initModule();

       //
       // Initialize the PIE vector table with pointers to the shell interrupt
       // Service Routines (ISR).
       //
       Interrupt_initVectorTable();

   #if EXAMPLE1

       //
       // 这个例子是基本的引脚配置
       //
       setup1GPIO();

   #endif  // - EXAMPLE1

   #if EXAMPLE2

       //
       // 这个例子是通讯的引脚配置
       //
       setup2GPIO();

   #endif

    while(1);
    return 0;

}

//
// setup1GPIO - 演示基本引脚配置
//
void
setup1GPIO(void)
{
    //
    // Example 1: 基本引脚配置.
    // 基本引脚包括:
    // PWM1-3, ECAP1, ECAP2, TZ1-TZ4, SPI-A, EQEP1, SCI-A, I2C
    // 和一些 I/O 引脚
    // 这些可以合并为一条代码以提高效率
    //

    //
    // Enable PWM1-3 on GPIO0-GPIO5
    //
    GPIO_setPadConfig(0, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO0
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO1
    GPIO_setPadConfig(2, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO2
    GPIO_setPadConfig(3, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO3
    GPIO_setPadConfig(4, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO4
    GPIO_setPadConfig(5, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO5
    GPIO_setPinConfig(GPIO_0_EPWM1_A);               // GPIO0 = PWM1A
    GPIO_setPinConfig(GPIO_1_EPWM1_B);               // GPIO1 = PWM1B
    GPIO_setPinConfig(GPIO_2_EPWM2_A);               // GPIO2 = PWM2A
    GPIO_setPinConfig(GPIO_3_EPWM2_B);               // GPIO3 = PWM2B
    GPIO_setPinConfig(GPIO_4_EPWM3_A);               // GPIO4 = PWM3A
    GPIO_setPinConfig(GPIO_5_EPWM3_B);               // GPIO5 = PWM3B

    //
    // Enable a GPIO output on GPIO6, set it high
    //
    GPIO_setPadConfig(6, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO6
    GPIO_writePin(6, 1);                            // Load output latch
    GPIO_setPinConfig(GPIO_6_GPIO6);                // GPIO6 = GPIO6
    GPIO_setDirectionMode(6, GPIO_DIR_MODE_OUT);    // GPIO6 = output

    //
    // Enable eCAP1 on GPIO7
    //
    GPIO_setPadConfig(7, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO7
    GPIO_setQualificationMode(7, GPIO_QUAL_SYNC);   // Synch to SYSCLKOUT
    XBAR_setInputPin(XBAR_INPUT7, 7);               // GPIO7 = ECAP1

    //
    // Enable GPIO outputs on GPIO8 - GPIO11, set it high
    //
    GPIO_setPadConfig(8, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO8
    GPIO_writePin(8, 1);                            // Load output latch
    GPIO_setPinConfig(GPIO_8_GPIO8);                // GPIO8 = GPIO8
    GPIO_setDirectionMode(8, GPIO_DIR_MODE_OUT);    // GPIO8 = output

    GPIO_setPadConfig(9, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO9
    GPIO_writePin(9, 1);                            // Load output latch
    GPIO_setPinConfig(GPIO_9_GPIO9);                // GPIO9 = GPIO9
    GPIO_setDirectionMode(9, GPIO_DIR_MODE_OUT);    // GPIO9 = output

    GPIO_setPadConfig(10, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO10
    GPIO_writePin(10, 1);                           // Load output latch
    GPIO_setPinConfig(GPIO_10_GPIO10);              // GPIO10 = GPIO10
    GPIO_setDirectionMode(10, GPIO_DIR_MODE_OUT);   // GPIO10 = output

    GPIO_setPadConfig(11, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO11
    GPIO_writePin(11, 1);                           // Load output latch
    GPIO_setPinConfig(GPIO_11_GPIO11);              // GPIO11 = GPIO11
    GPIO_setDirectionMode(11, GPIO_DIR_MODE_OUT);   // GPIO11 = output

    //
    // Enable Trip Zone inputs on GPIO12 - GPIO14
    //
    GPIO_setPadConfig(12, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO12
    GPIO_setPadConfig(13, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO13
    GPIO_setPadConfig(14, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO14
    GPIO_setQualificationMode(12, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(13, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(14, GPIO_QUAL_ASYNC); // asynch input
    XBAR_setInputPin(XBAR_INPUT1, 12);              // GPIO12 = TZ1
    XBAR_setInputPin(XBAR_INPUT2, 13);              // GPIO13 = TZ2
    XBAR_setInputPin(XBAR_INPUT3, 14);              // GPIO14 = TZ3

    //
    // Enable SPI-A on GPIO16 - GPIO18 & GPIO57
    //
    GPIO_setPadConfig(16, GPIO_PIN_TYPE_PULLUP);    // Pullup GPIO16 (SPISIMOA)
    GPIO_setPadConfig(17, GPIO_PIN_TYPE_PULLUP);    // Pullup GPIO17 (SPIS0MIA)
    GPIO_setPadConfig(18, GPIO_PIN_TYPE_PULLUP);    // Pullup GPIO18 (SPICLKA)
    GPIO_setPadConfig(57, GPIO_PIN_TYPE_PULLUP);    // Pullup GPIO57 (SPISTEA)
    GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(17, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(18, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(57, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setPinConfig(GPIO_16_SPIA_SIMO);            // GPIO16 = SPISIMOA
    GPIO_setPinConfig(GPIO_17_SPIA_SOMI);            // GPIO17 = SPIS0MIA
    GPIO_setPinConfig(GPIO_18_SPIA_CLK);             // GPIO18 = SPICLKA
    GPIO_setPinConfig(GPIO_57_SPIA_STE);             // GPIO57 = SPISTEA

    //
    // Enable EQEP1 on GPIO's 22,59,40,37
    //
    GPIO_setPadConfig(22, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO22
    GPIO_setPadConfig(59, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO59
    GPIO_setPadConfig(40, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO40
    GPIO_setPadConfig(37, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO37
    GPIO_setQualificationMode(22, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    GPIO_setQualificationMode(59, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    GPIO_setQualificationMode(40, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    GPIO_setQualificationMode(37, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    GPIO_setPinConfig(GPIO_22_EQEP1_STROBE);              // GPIO22 = EQEP1S
    GPIO_setPinConfig(GPIO_59_EQEP1_INDEX);              // GPIO59 = EQEP1I
    GPIO_setPinConfig(GPIO_40_EQEP1_A);              // GPIO40 = EQEP1A
    GPIO_setPinConfig(GPIO_37_EQEP1_B);              // GPIO37 = EQEP1B

    //
    // Enable eCAP2 on GPIO24
    //
    GPIO_setPadConfig(24, GPIO_PIN_TYPE_PULLUP);    // Pullup on GPIO24 (ECAP1)
    GPIO_setQualificationMode(24, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    XBAR_setInputPin(XBAR_INPUT8, 24);              // GPIO24 = ECAP2

    //
    // Set input qualification period for GPIO25 & GPIO26
    //
    GPIO_setQualificationPeriod(24, 2);              // Qual period=SYSCLKOUT/2
    GPIO_setQualificationMode(25, GPIO_QUAL_6SAMPLE);// 6 samples
    GPIO_setQualificationMode(26, GPIO_QUAL_6SAMPLE);// 6 samples

    //
    // Make GPIO25 the input source for XINT1
    //
    GPIO_setPinConfig(GPIO_25_GPIO25);            // GPIO25 = GPIO25
    GPIO_setDirectionMode(25, GPIO_DIR_MODE_IN);  // GPIO25 = input
    GPIO_setInterruptPin(25,GPIO_INT_XINT1);      // XINT1 connected to GPIO25

    //
    // Make GPIO26 the input source for XINT2
    //
    GPIO_setPinConfig(GPIO_26_GPIO26);             // GPIO26 = GPIO26
    GPIO_setDirectionMode(26, GPIO_DIR_MODE_IN);   // GPIO26 = input
    GPIO_setInterruptPin(26, GPIO_INT_XINT2);      // XINT2 connected to GPIO26

    //
    // Make GPIO27 wakeup from HALT Low Power Modes
    //
    GPIO_setPinConfig(GPIO_27_GPIO27);           // GPIO27 = GPIO27
    GPIO_setDirectionMode(27, GPIO_DIR_MODE_IN); // GPIO27 = input
    SysCtl_enableLPMWakeupPin(27);               // GPIO27 will wake the device

    //
    // Enable SCI-A on GPIO28 - GPIO29
    //
    GPIO_setPadConfig(28, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO28
    GPIO_setQualificationMode(28, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPinConfig(GPIO_28_SCIA_RX);              // GPIO28 = SCIRXDA
    GPIO_setPadConfig(29, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO29
    GPIO_setPinConfig(GPIO_29_SCIA_TX);              // GPIO29 = SCITXDA

    //
    // Enable CAN-A on GPIO30 - GPIO31
    //
    GPIO_setPadConfig(30, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO30
    GPIO_setPinConfig(GPIO_30_CANA_RX);               // GPIO30 = CANRXA
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO31
    GPIO_setQualificationMode(31, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPinConfig(GPIO_31_CANA_TX);               // GPIO31 = CANTXA

    //
    // Enable I2C-A on GPIO32 - GPIO33
    //
    GPIO_setPadConfig(32, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO32
    GPIO_setPinConfig(GPIO_32_I2CA_SDA);                 // GPIO32 = SDAA
    GPIO_setQualificationMode(32, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPadConfig(33, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO33
    GPIO_setQualificationMode(33, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPinConfig(GPIO_33_I2CA_SCL);                 // GPIO33 = SCLA

    //
    // Make GPIO34 an input on GPIO34
    //
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO34
    GPIO_setPinConfig(GPIO_34_GPIO34);               // GPIO34 = GPIO34
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_IN);     // GPIO34 = input
}

//
// setup2GPIO - 演示通讯引脚配置
//
void
setup2GPIO(void)
{
    //
    // Example 2:
    // 通讯引脚
    // 基本通讯引脚包括:
    // PWM1-3, CAP1, CAP2, SPI-A, SPI-B, CAN-A, SCI-A and I2C
    // 和一些I/O 引脚
    //

    //
    // Enable PWM1-3 on GPIO0-GPIO5
    //
    GPIO_setPadConfig(0, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO0
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO1
    GPIO_setPadConfig(2, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO2
    GPIO_setPadConfig(3, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO3
    GPIO_setPadConfig(4, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO4
    GPIO_setPadConfig(5, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO5
    GPIO_setPinConfig(GPIO_0_EPWM1_A);              // GPIO0 = PWM1A
    GPIO_setPinConfig(GPIO_1_EPWM1_B);              // GPIO1 = PWM1B
    GPIO_setPinConfig(GPIO_2_EPWM2_A);              // GPIO2 = PWM2A
    GPIO_setPinConfig(GPIO_3_EPWM2_B);              // GPIO3 = PWM2B
    GPIO_setPinConfig(GPIO_4_EPWM3_A);              // GPIO4 = PWM3A
    GPIO_setPinConfig(GPIO_5_EPWM3_B);              // GPIO5 = PWM3B

    //
    // Enable an GPIO output on GPIO6
    //
    GPIO_setPadConfig(6, GPIO_PIN_TYPE_PULLUP);   // Enable pullup on GPIO6
    GPIO_writePin(6, 1);                          // Load output latch
    GPIO_setPinConfig(GPIO_6_GPIO6);              // GPIO6 = GPIO6
    GPIO_setDirectionMode(6, GPIO_DIR_MODE_OUT);  // GPIO6 = output

    //
    // Enable eCAP1 on GPIO7
    //
    GPIO_setPadConfig(7, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO7
    GPIO_setQualificationMode(7, GPIO_QUAL_SYNC);   // Synch to SYSCLKOUT
    XBAR_setInputPin(XBAR_INPUT7, 7);               // GPIO7 = ECAP1

    //
    // Enable GPIO outputs on GPIO8 - GPIO11
    //
    GPIO_setPadConfig(8, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO8
    GPIO_writePin(8, 1);                           // Load output latch
    GPIO_setPinConfig(GPIO_8_GPIO8);               // GPIO8 = GPIO8
    GPIO_setDirectionMode(8, GPIO_DIR_MODE_OUT);   // GPIO8 = output

    GPIO_setPadConfig(9, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO9
    GPIO_writePin(9, 1);                           // Load output latch
    GPIO_setPinConfig(GPIO_9_GPIO9);               // GPIO9 = GPIO9
    GPIO_setDirectionMode(9, GPIO_DIR_MODE_OUT);   // GPIO9 = output

    GPIO_setPadConfig(10, GPIO_PIN_TYPE_PULLUP);   // Enable pullup on GPIO10
    GPIO_writePin(10, 1);                          // Load output latch
    GPIO_setPinConfig(GPIO_10_GPIO10);             // GPIO10 = GPIO10
    GPIO_setDirectionMode(10, GPIO_DIR_MODE_OUT);  // GPIO10 = output

    GPIO_setPadConfig(11, GPIO_PIN_TYPE_PULLUP);   // Enable pullup on GPIO11
    GPIO_writePin(11, 1);                          // Load output latch
    GPIO_setPinConfig(GPIO_11_GPIO11);             // GPIO11 = GPIO11
    GPIO_setDirectionMode(11, GPIO_DIR_MODE_OUT);  // GPIO11 = output

    //
    // Enable SPI-B on GPIO22, GPIO24, GPIO25 & GPIO15
    //
    GPIO_setPadConfig(22, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO22 (SPICLKB)
    GPIO_setPadConfig(15, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO15 (SPISTEB)
    GPIO_setPadConfig(24, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO24 (SPISIMOB)
    GPIO_setPadConfig(25, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO25 (SPISOMIB)
    GPIO_setQualificationMode(22, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(15, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(24, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(25, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setPinConfig(GPIO_22_GPIO22_VFBSW);        // Set Group Mux
    GPIO_setPinConfig(GPIO_15_GPIO15);              // Set Group Mux
    GPIO_setPinConfig(GPIO_24_GPIO24);              // Set Group Mux
    GPIO_setPinConfig(GPIO_25_GPIO25);              // Set Group Mux
    GPIO_setPinConfig(GPIO_22_SPIB_CLK);             // GPIO22 = SPICLK
    GPIO_setPinConfig(GPIO_15_SPIB_STE);             // GPIO15 = SPISTEB
    GPIO_setPinConfig(GPIO_24_SPIB_SIMO);            // GPIO24 = SPISIMOB
    GPIO_setPinConfig(GPIO_25_SPIB_SOMI);            // GPIO25 = SPISOMIB

    //
    // Enable SPI-A on GPIO16 - GPIO18 & GPIO57
    //
    GPIO_setPadConfig(16, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO16 (SPISIMOA)
    GPIO_setPadConfig(17, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO17 (SPIS0MIA)
    GPIO_setPadConfig(18, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO18 (SPICLKA)
    GPIO_setPadConfig(57, GPIO_PIN_TYPE_PULLUP); // Pullup on GPIO57 (SPISTEA)
    GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(17, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(18, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setQualificationMode(57, GPIO_QUAL_ASYNC); // asynch input
    GPIO_setPinConfig(GPIO_16_SPIA_SIMO);            // GPIO16 = SPISIMOA
    GPIO_setPinConfig(GPIO_17_SPIA_SOMI);            // GPIO17 = SPIS0MIA
    GPIO_setPinConfig(GPIO_18_SPIA_CLK);             // GPIO18 = SPICLKA
    GPIO_setPinConfig(GPIO_57_SPIA_STE);             // GPIO57 = SPISTEA

    //
    // Enable eCAP1 on GPIO24
    //
    GPIO_setPadConfig(24, GPIO_PIN_TYPE_PULLUP);    // Pullup on GPIO24 (ECAP1)
    GPIO_setQualificationMode(24, GPIO_QUAL_SYNC);  // Synch to SYSCLKOUT
    XBAR_setInputPin(XBAR_INPUT7, 24);              // GPIO24 = ECAP1

    //
    // Set input qualifcation period for GPIO25 & GPIO26 inputs
    //
    GPIO_setQualificationPeriod(24, 2);            // Qual period = SYSCLKOUT/2
    GPIO_setQualificationMode(25, GPIO_QUAL_6SAMPLE);   // 6 samples
    GPIO_setQualificationMode(26, GPIO_QUAL_3SAMPLE);   // 3 samples

    //
    // Make GPIO25 the input source for XINT1
    //
    GPIO_setPinConfig(GPIO_25_GPIO25);            // GPIO25 = GPIO25
    GPIO_setDirectionMode(25, GPIO_DIR_MODE_IN);  // GPIO25 = input
    GPIO_setInterruptPin(25,GPIO_INT_XINT1);      // XINT1 connected to GPIO25

    //
    // Make GPIO26 the input source for XINT2
    //
    GPIO_setPinConfig(GPIO_26_GPIO26);             // GPIO26 = GPIO26
    GPIO_setDirectionMode(26, GPIO_DIR_MODE_IN);   // GPIO26 = input
    GPIO_setInterruptPin(26,GPIO_INT_XINT2);       // XINT2 connected to GPIO26

    //
    // Make GPIO27 wakeup from HALT Low Power Modes
    //
    GPIO_setPinConfig(GPIO_27_GPIO27);           // GPIO27 = GPIO27
    GPIO_setDirectionMode(27, GPIO_DIR_MODE_IN); // GPIO27 = input
    SysCtl_enableLPMWakeupPin(27);               // GPIO27 will wake the device

    //
    // Enable SCI-A on GPIO28 - GPIO29
    //
    GPIO_setPadConfig(28, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO28
    GPIO_setQualificationMode(28, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPinConfig(GPIO_28_SCIA_RX);              // GPIO28 = SCIRXDA
    GPIO_setPadConfig(29, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO29
    GPIO_setPinConfig(GPIO_29_SCIA_TX);              // GPIO29 = SCITXDA

    //
    // Enable CAN-A on GPIO30 - GPIO31
    //
    GPIO_setPadConfig(30, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO30
    GPIO_setPinConfig(GPIO_30_CANA_RX);               // GPIO30 = CANRXA
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO31
    GPIO_setQualificationMode(31, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setPinConfig(GPIO_31_CANA_TX);               // GPIO31 = CANTXA

    //
    // Enable I2C-A on GPIO32 - GPIO33
    //
    GPIO_setPadConfig(32, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO32
    GPIO_setPadConfig(33, GPIO_PIN_TYPE_PULLUP);     // Enable pullup on GPIO33
    GPIO_setQualificationMode(32, GPIO_QUAL_ASYNC);  // asynch input
    GPIO_setQualificationMode(33,GPIO_QUAL_ASYNC);   // asynch input
    GPIO_setPinConfig(GPIO_32_I2CA_SDA);                 // GPIO32 = SDAA
    GPIO_setPinConfig(GPIO_33_I2CA_SCL);                 // GPIO33 = SCLA

    //
    // Make GPIO34 an input
    //
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_PULLUP);    // Enable pullup on GPIO34
    GPIO_setPinConfig(GPIO_34_GPIO34);              // GPIO34 = GPIO34
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_IN);    // GPIO34 = input
}

//
// End of File
//

