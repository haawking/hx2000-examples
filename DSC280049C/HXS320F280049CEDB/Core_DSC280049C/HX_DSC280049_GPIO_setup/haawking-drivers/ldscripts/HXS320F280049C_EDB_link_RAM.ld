/*#############################################################################*/
/*                                                                             */
/* $Copyright:                                                                 */
/* Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd                 */
/* http://www.haawking.com/ All rights reserved.                               */
/*                                                                             */
/* Redistribution and use in source and binary forms, with or without          */
/* modification, are permitted provided that the following conditions          */
/* are met:                                                                    */
/*                                                                             */
/*   Redistributions of source code must retain the above copyright            */
/*   notice, this list of conditions and the following disclaimer.             */
/*                                                                             */
/*   Redistributions in binary form must reproduce the above copyright         */
/*   notice, this list of conditions and the following disclaimer in the       */
/*   documentation and/or other materials provided with the                    */
/*   distribution.                                                             */
/*                                                                             */
/*   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of  */
/*   its contributors may be used to endorse or promote products derived       */
/*   from this software without specific prior written permission.             */
/*                                                                             */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS         */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT           */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR       */
/* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT            */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY       */
/* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT         */
/* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE       */
/* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.        */
/*                                                                             */
/*#############################################################################*/
/*                                                                             */
/* Release for HXS320F280049CEDB, RAM Linker, 1.0.0                     */
/*                                                                             */
/* Release time: 2023-06-19 13:16:55.263222                                    */
/*                                                                             */
/*#############################################################################*/

OUTPUT_ARCH( "riscv" )
ENTRY(_start)
INCLUDE "Peripheral.ld"
INCLUDE "BootROM.ld"


MEMORY
{
  BEGIN(rwx) :            ORIGIN = 0x000, LENGTH = 0x010
  RAMM0M1(rwx) :          ORIGIN = 0x010, LENGTH = 0xFF0              /* RAM M0M1 */
  
  RAMLS0To7(rwx) :        ORIGIN = 0x10000, LENGTH = 0x10000          /* RAM LS0~7 */
						  
  RAMGS0(rwx) :           ORIGIN = 0x200000, LENGTH = 0x2000          /* RAM GS0 */
  RAMGS1(rwx) :           ORIGIN = 0x208000, LENGTH = 0x2000          /* RAM GS1 */
  RAMGS2(rwx) :           ORIGIN = 0x210000, LENGTH = 0x2000          /* RAM GS2 */
  RAMGS3(rwx) :           ORIGIN = 0x218000, LENGTH = 0x2000          /* RAM GS3 */ 
  
  FLASH_Bank0(rwx) :      ORIGIN = 0x600000, LENGTH = 0x40000          /* FLASH_Bank0 */ 
  FLASH_Bank1(rwx) :      ORIGIN = 0x680000, LENGTH = 0x40000          /* FLASH_Bank1 */

  IER_REGISTER_FILE(rw) : ORIGIN  = 0x100C10, LENGTH = 0x04
  IFR_REGISTER_FILE(rw) : ORIGIN  = 0x100D10, LENGTH = 0x04
}

/*----------------------------------------------------------------------*/
/* Sections                                                             */
/*----------------------------------------------------------------------*/

SECTIONS
{
    . = 0x0;
    .codestart : { *(codestart) } > BEGIN
    
    . = 0x10;
    M0M1  : 
    {
    __global_sp$ = . + 0xFE8;
    } > RAMM0M1
    
    /* text: test code section */
    . = 0x00010000;
    /* data segment */
    .sdata ALIGN(0x10) :{
      __data_start = .;
      __global_pointer$ = . + 0x800;
		*(.data)  *(.data.*)
		*(.srodata.cst16) *(.srodata.cst8) *(.srodata.cst4) *(.srodata.cst2) *(.srodata*)
		*(.sdata .sdata.* .gnu.linkonce.s.*)
		__data_end = .;
    } > RAMLS0To7
    
    /* bss segment */
    .sbss ALIGN(0x10) : {
     __bss_start = .;
      *(.sbss .sbss.* .gnu.linkonce.sb.*)
      *(.scommon)
    }  > RAMLS0To7
    .bss  ALIGN(0x10) : { *(.bss)  *(.bss.*)  *(COMMON)  __bss_end = ALIGN(0x10); } > RAMLS0To7
    
    .text.init ALIGN(0x10) : { *(.text.init)  }  > RAMLS0To7
    .text ALIGN(0x10) : { *(.text) *(.IQmath.*) *(.text*)  *(.rodata .rodata.*) *(ramconsts) *(ramfuncs)} > RAMLS0To7
	
    . = 0x200000;
    ramgs0                          :{*(ramgs0)}  > RAMGS0
    . = 0x208000;
    ramgs1                          :{*(ramgs1)}  > RAMGS1
     
    . = 0x100C10;
    .IER_REGISTER(NOLOAD) : {*(.ier_register)} > IER_REGISTER_FILE
    .IFR_REGISTER(NOLOAD) : {*(.ifr_register)} > IFR_REGISTER_FILE
    
    /* End of uninitalized data segement */
    _end = .;
}

