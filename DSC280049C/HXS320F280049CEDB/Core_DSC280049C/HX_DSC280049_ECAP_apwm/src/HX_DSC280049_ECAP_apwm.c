/******************************************************************
 文 档 名：       HX_DSC280049_ECAP_apwm.c
 开 发 环 境：  Haawking IDE V2.1.7
 开 发 板：	Core_DSC280049CPZS_V1.0
 D S P：          DSC280049
 使 用 库：
 作     用：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 	 	 将eCAP模块设置为APWM模式。
 	 	 PWM波形将在GPIO5上输出。
 	 	 PWM的频率配置为在5Hz到10Hz之间变化
 	 	 使用阴影寄存器加载下一周期/比较值

 外部接线：


 现象：在Expressions或Variables或者LiveView窗口观察变量


 版 本：      V1.0.0
 时 间：      2023年7月13日
 mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"


//
// Defines
//
#define DECREASE_FREQUENCY  0
#define INCREASE_FREQUENCY  1

//
// Globals
//
uint16_t direction;
volatile uint32_t cap1Count;

int main(void)
{
    Device_init();

    //
       // Disable pin locks and enable internal pull ups.
       //
       Device_initGPIO();

       //
       // Initialize PIE and clear PIE registers. Disables CPU interrupts.
       //
       Interrupt_initModule();

       //
       // Initialize the PIE vector table with pointers to the shell Interrupt
       // Service Routines (ISR).
       //
       Interrupt_initVectorTable();

       //
       // Board initialization
       // Select eCAP1OUT on MUX 0. Make GPIO5 eCAP1OUT for PWM output
       // Configure eCAP in APWM mode
       //
       Board_init();

    while(1)
    {
          //
          // Set the duty cycle to 50%
          //
          ECAP_setAPWMShadowCompare(myECAP0_BASE,
                        (ECAP_getEventTimeStamp(myECAP0_BASE, ECAP_EVENT_1) >> 1U));

          cap1Count = ECAP_getEventTimeStamp(myECAP0_BASE, ECAP_EVENT_1);

          //
          // Vary frequency
          //
          if(cap1Count >= 20000000U)
          {
             direction = INCREASE_FREQUENCY;
          }
          else if (cap1Count <= 10000000U)
          {
             direction = DECREASE_FREQUENCY;
          }

          if(direction == INCREASE_FREQUENCY)
          {
             ECAP_setAPWMShadowPeriod(myECAP0_BASE, (cap1Count - 500000U));
          }
          else
          {
             ECAP_setAPWMShadowPeriod(myECAP0_BASE, (cap1Count + 500000U));
          }
      }

    return 0;
}

//
// End of File
//

