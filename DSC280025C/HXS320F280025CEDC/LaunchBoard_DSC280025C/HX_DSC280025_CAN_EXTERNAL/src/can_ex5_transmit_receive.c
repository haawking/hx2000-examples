/******************************************************************
 文 档 名：       HX_DSC280025_CAN_EXTERNAL
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      CAN中断方式收发数据
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，通过CAN总线中断方式收发数据

连线：GPIO33/CANRX、GPIO32/CANTX与CAN收发器相连
 现象:
//!  - MSGCOUNT   - 信息计数
//!  - txMsgCount - 发送数据中断计数++
//!  - txMsgData  - 发送数据
//!  - errorFlag  - 错误标志
//!  - rxMsgCount - 接收数据中断计数--
 * rxMsgData 接收数据
 * 无错误标志errorFlag=0
 * 发送时可发出数据，计数txMsgCount++
 * 接收时可接收到数据，计数rxMsgCount--

 版 本：      V1.0.0
 时 间：      2023年2月23日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "system.h"

//
// Comment to Make the CAN Controller work as a Receiver.
//
#define TRANSMIT

//
// Defines
//
#ifdef TRANSMIT
#define TX_MSG_OBJ_ID    1
#else
#define RX_MSG_OBJ_ID    1
#endif
#define MSG_DATA_LENGTH  4
#define MSGCOUNT        10

//
// Globals
//
#ifdef TRANSMIT
volatile uint32_t txMsgCount = 0;
uint32_t txMsgSuccessful  = 1;
uint16_t txMsgData[4];
#else
volatile uint32_t rxMsgCount = MSGCOUNT;
uint16_t rxMsgData[4];
#endif
volatile unsigned long i;
volatile uint32_t errorFlag = 0;

//
// Function Prototypes
//
__interrupt void canaISR(void);

//
// Main
//
void main(void)
{
    /*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();

    /*GPIO31,GPIO34配置为IO输出，以指示CAN传输状态*/
    setup1GPIO();
    /*GPIO33,GPIO32配置为CANRX,CANTX*/
    GPIO_setPinConfig(GPIO_33_CANA_RX);
    GPIO_setPinConfig(GPIO_32_CANA_TX);

    /*CAN模块初始化*/
    CAN_initModule(CANA_BASE);

    /*CAN波特率配置:时钟频率,波特率,比特时间*/
    CAN_setBitRate(CANA_BASE, DEVICE_SYSCLK_FREQ, 500000, 20);

    /*CAN中断使能:中断线0_can中断|错误中断|状态中断*/
    CAN_enableInterrupt(CANA_BASE, CAN_INT_IE0 | CAN_INT_ERROR |
                        CAN_INT_STATUS);

    /*清中断，关中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*打开全局中断*/
    EINT;

    /*中断入口地址INT_CANA0指向执行相应中断服务程序CANISR*/
    Interrupt_register(INT_CANA0,&canaISR);

    /*中断使能INT_CANA0*/
    Interrupt_enable(INT_CANA0);
    /*CAN全局中断使能*/
    CAN_enableGlobalInterrupt(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

#ifdef TRANSMIT

    /*CAN发送配置：对象ID配置：1，msgID配置：1，信息ID:0x15555555,帧类型：扩展帧，
          * msg类型：发送对象，msg ID掩码：0，对象标志：发送中断CAN_MSG_OBJ_TX_INT_ENABLE，信息数据长度：4字节*/
    CAN_setupMessageObject(CANA_BASE, TX_MSG_OBJ_ID, 0x15555555,
                           CAN_MSG_FRAME_EXT, CAN_MSG_OBJ_TYPE_TX, 0,
                           CAN_MSG_OBJ_TX_INT_ENABLE, MSG_DATA_LENGTH);
    //
    // Initialize the transmit message object data buffer to be sent
    //
    txMsgData[0] = 0x12;
    txMsgData[1] = 0x34;
    txMsgData[2] = 0x56;
    txMsgData[3] = 0x78;
#else
    /*CAN接收配置：对象ID配置：2，msgID配置：1，信息ID:0x15555555,帧类型：扩展帧，
           * msg类型：接收对象，msg ID掩码：0，对象标志：接收中断CAN_MSG_OBJ_RX_INT_ENABLE，信息数据长度：4字节*/

    CAN_setupMessageObject(CANA_BASE, RX_MSG_OBJ_ID, 0x15555555,
                           CAN_MSG_FRAME_EXT, CAN_MSG_OBJ_TYPE_RX, 0,
                           CAN_MSG_OBJ_RX_INT_ENABLE, MSG_DATA_LENGTH);
#endif

    /*启动CAN模块*/
    CAN_startModule(CANA_BASE);

#ifdef TRANSMIT
    /*CAN发送数据*/
    for(i = 0; i < MSGCOUNT; i++)
    {
        /*检查是否遇到错误，则GPIO34/LED2点亮，否则GPIO31/LED1点亮*/
        if(errorFlag)
        {
        	GPIO_writePin(34,0);
        }
        else
        {
        	GPIO_writePin(31,0);
        }

        /*发送txMsgData数据：发送对象TX_MSG_OBJ_ID,数据长度：4字节，发送数据txMsgData*/
        CAN_sendMessage(CANA_BASE, TX_MSG_OBJ_ID, MSG_DATA_LENGTH,
                        txMsgData);

        /*延时0.25s以完成发送*/
        DEVICE_DELAY_US(250000);
        /*等待发送完成*/
        while(txMsgSuccessful);

        /*持续赋值，以测试连续发送多组数据*/
        txMsgData[0] += 0x01;
        txMsgData[1] += 0x01;
        txMsgData[2] += 0x01;
        txMsgData[3] += 0x01;

        //
        // Reset data if exceeds a byte
        //
        if(txMsgData[0] > 0xFF)
        {
            txMsgData[0] = 0;
        }
        if(txMsgData[1] > 0xFF)
        {
            txMsgData[1] = 0;
        }
        if(txMsgData[2] > 0xFF)
        {
            txMsgData[2] = 0;
        }
        if(txMsgData[3] > 0xFF)
        {
            txMsgData[3] = 0;
        }

        //
        // Update the flag for next message.
        //
        txMsgSuccessful  = 1;
    }
#else
    //
    // Loop to keep receiving data from another CAN Controller.
    //
    while(rxMsgCount)
    {
    }
#endif

}

//
// CAN A ISR - The interrupt service routine called when a CAN interrupt is
//             triggered on CAN module A.
//
__interrupt void canaISR(void)
{
    uint32_t status;

    /*读取CAN中断状态*/
    status = CAN_getInterruptCause(CANA_BASE);

    /*若中断状态为中断线0ID，读取CAN状态*/
    if(status == CAN_INT_INT0ID_STATUS)
    {
        /*读取CAN状态*/
        status = CAN_getStatus(CANA_BASE);

        /*若发送与接收数据产生错误，errorFlag错误标志置位*/
#ifdef TRANSMIT
        if(((status  & ~(CAN_STATUS_TXOK)) != CAN_STATUS_LEC_MSK) &&
           ((status  & ~(CAN_STATUS_TXOK)) != CAN_STATUS_LEC_NONE))
#else
        if(((status  & ~(CAN_STATUS_RXOK)) != CAN_STATUS_LEC_MSK) &&
           ((status  & ~(CAN_STATUS_RXOK)) != CAN_STATUS_LEC_NONE))
#endif
        {
            //
            // Set a flag to indicate some errors may have occurred.
            //
            errorFlag = 1;
        }
    }
#ifdef TRANSMIT
    /*若中断状态为发送对象ID*/
    else if(status == TX_MSG_OBJ_ID)
    {
        /*清除CAN中断状态，发送对象TX_ID中断*/
        CAN_clearInterruptStatus(CANA_BASE, TX_MSG_OBJ_ID);
        /*发送数据计数+1*/
        txMsgCount++;
        /*无错误，错误标志errorFlag清零*/
        errorFlag = 0;

        /*发送完成，成功标志清零*/
        txMsgSuccessful  = 0;
    }
#else
    /*若中断状态为接收对象ID*/
    else if(status == RX_MSG_OBJ_ID)
    {
        /*can读取接收数据：接收对象RX_MSG_OBJ_ID,接收数据rxMsgData*/
        CAN_readMessage(CANA_BASE, RX_MSG_OBJ_ID, rxMsgData);

        /*清除CAN中断状态，接收对象RX_ID中断*/
        CAN_clearInterruptStatus(CANA_BASE, RX_MSG_OBJ_ID);

        /*接收数据计数-1*/
        rxMsgCount--;

        /*无错误，错误状态标志清零*/
        errorFlag = 0;
    }
#endif

    else
    {}

    /*清除CAN全局中断状态*/
    CAN_clearGlobalInterruptStatus(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

    /*清除PIEACK中断应答：第9组*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}

//
// End of File
//
