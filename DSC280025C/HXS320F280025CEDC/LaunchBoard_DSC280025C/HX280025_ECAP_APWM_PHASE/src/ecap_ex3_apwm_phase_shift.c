/******************************************************************
 文 档 名：       HX_DSC280025_ECAP_APWM_PHASESHIFT
 开 发 环 境：  Haawking IDE V2.2.10Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ECAP_APWM产生两路非对称，相位相差30%脉宽，即108度的16k PWM方波
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用ECAP_APWM模式产生
 两路非对称，相位相差30%脉宽，即108度的16k PWM方波

 现象:在GPIO5、GPIO6上输出 两路非对称，相位相差30%脉宽，即108度的16k PWM方波

 版 本：      V1.0.0
 时 间：      2022年11月17日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "system.h"

uint32_t cap1Count;
uint32_t cap2Count;

//
// Main
//
int main(void)
{
    /*系统时钟初始化*/
    Device_init();

    /*GPIO端口配置锁定关闭*/
    Device_initGPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*初始化PIE与清PIE寄存器，关CPU中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();


    EALLOW;
    /*GPIO5的交叉开关输出到ECAP功能输入复用配置*/
    GPIO_setPinConfig(GPIO_5_OUTPUTXBAR3);
    /*GPIO6的交叉开关输出到ECAP功能输入复用配置*/
    GPIO_setPinConfig(GPIO_6_OUTPUTXBAR4);
    /*ECAP的APWM功能配置*/
    ECAP_apwm_config();
    /*交叉开关输出功能配置*/
    OUTPUTXBAR_config();

    EDIS;

    while(1)
    {
        /*CAP1脉冲读取：CAP1*/
        cap1Count = ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_1);
        /*CAP1脉冲读取：CAP1*/
        cap2Count = ECAP_getEventTimeStamp(ECAP2_BASE, ECAP_EVENT_1);
        
        if((cap1Count==10000U)&&(cap1Count==cap2Count))
        		{
            /*GPIO31写入0,点亮LED1*/
            GPIO_writePin(31, 0);
        		}
        else
        	{
            /*GPIO34写入0,点亮LED2*/
            GPIO_writePin(34, 0);
        	}


    }
    return 0;
}

//
// End of File
//
void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED2灭*/
    GPIO_writePin(31, 1);


    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

    /*GPIO34写入1,初始化LED1灭*/
    GPIO_writePin(34, 1);
    EDIS;
}
