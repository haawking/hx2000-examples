/******************************************************************
 文 档 名：       HX0025_Sci_Loopback_V1.0.0
 开 发 环 境：  Haawking IDE V2.2.10Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      Spi内回环测试
 说     明：      RAM工程
 -------------------------- 例程使用说明 --------------------------

//!本程序使用SPI模块的内部环回测试模式。
//这是一个非常基本的环回，不使用fifo或中断。发送数据流，然后与接收的数据流进行比较。
//pinmux和SPI模块通过sysconfig文件进行配置。

//!  0000 0001 0002 0003 0004 0005 0006 0007 .... FFFE FFFF 0000
//!
//! 此模式一直持续
//!
//! 外部连接
//! 无
//!
//! 观看参数
//!  sData - 发送的数据
//!  rData - 接收的数据
//!   LED1 GPIO31亮表示正常
//!   LED2 GPIO34亮表示失败
****************************************************************************************/

//
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "board.h"

#include"system.h"

uint16_t errorCount=0;

uint16_t sData = 0;                  // Send data
uint16_t rData = 0;                  // Receive data

#define MB_ADDR 0x21FFC

void Success(void)
{

 GPIO_writePin(31,0);
 GPIO_writePin(34,1);
 HWREG(MB_ADDR) = 0x5555AAAA;

}

void Fail(void)
{
 GPIO_writePin(31,1);
 GPIO_writePin(34,0);
 HWREG(MB_ADDR) = 0xAAAA5555;
}

//
// Main
//
int main(void)
{
//    uint16_t sData = 0;                  // Send data
//    uint16_t rData = 0;                  // Receive data

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();
    setup1GPIO();
    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Board initialization
    //
    Board_init();

    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;
    ERTM;

    //
    // Loop forever. Suspend or place breakpoints to observe the buffers.
    //
    while(1)
    {
        // Transmit data
        SPI_writeDataNonBlocking(mySPI0_BASE, sData);

        // Block until data is received and then return it
        rData = SPI_readDataBlockingNonFIFO(mySPI0_BASE);

        // Check received data against sent data
        if(rData != sData)
        {
            // Something went wrong. rData doesn't contain expected data.
            //ESTOP0;
        	errorCount++;
            //GPIO_writePin(34, 0); //LED2 GPIO34亮表示失败
        	Fail();
        }

        else
        {
        	//GPIO_writePin(31, 0); //LED1 GPIO31亮表示成功
        	Success();
        }

        sData++;

    }

    return 0;
}

//
// End File
//
