/*
 * hrpwm.h
 *
 *  Created on: 2023��1��28��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"
#define EPWM_TIMER_TBPRD            100UL
#define MIN_HRPWM_DUTY_PERCENT      4.0/((float32_t)EPWM_TIMER_TBPRD)*100.0

__interrupt void epwm1ISR(void);
__interrupt void epwm2ISR(void);
__interrupt void epwm3ISR(void);
__interrupt void epwm4ISR(void);

void epwm_gpio(void);
void sync_config(void);

void hrpwm_config(uint32_t epwm_base,uint32_t epwm_tbprd,uint32_t epwm_cmpa,
		uint32_t epwm_cmpb,uint32_t epwm_tbphs,
		uint32_t ET_interruptSource,uint32_t ET_eventCount,
		HRPWM_Channel channel,HRPWM_MEPEdgeMode mepEdgeMode_A);

#endif /* SYSTEM_H_ */
