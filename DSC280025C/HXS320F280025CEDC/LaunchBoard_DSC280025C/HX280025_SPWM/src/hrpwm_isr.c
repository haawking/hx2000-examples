#include "system.h"
uint32_t epwm1_ctr;

__interrupt void epwm1ISR(void)
{
	epwm1_ctr++;

	EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

