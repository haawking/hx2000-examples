#include "system.h"

void configureOSHTripSignal(uint32_t epwmBase)
{
	/*配置XBAR_EPWM_MUX00_ADCAEVT1-EPWM_ADC复用事件
	 * 通过XBAR_TRIP7-事件7交叉开关输入*/
    XBAR_setEPWMMuxConfig(XBAR_TRIP7, XBAR_EPWM_MUX00_ADCAEVT1);

	/*使能XBAR_TRIP7-事件7交叉开关输入
	 * 通过XBAR_MUX00-交叉开关0输入*/
    XBAR_enableEPWMMux(XBAR_TRIP7, XBAR_MUX00);

	/*选择数据比较事件DC输入：
	 * 通过EPWM_DC_TRIP_TRIPIN7-事件7
	 * 触发EPWM_DC_TYPE_DCAH-DCAH事件*/
    EPWM_selectDigitalCompareTripInput(epwmBase,
    		EPWM_DC_TRIP_TRIPIN7,EPWM_DC_TYPE_DCAH);

    /*产生数字比较事件DCAEVT1的条件配置：
     * EPWM_TZ_DC_OUTPUT_A1-EPWMxA_DCAEVT1
     * EPWM_TZ_EVENT_DCXH_HIGH-DCAH置高*/
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase,
    		EPWM_TZ_DC_OUTPUT_A1,EPWM_TZ_EVENT_DCXH_HIGH);

    /*EPWM数字比较事件源：EPWM_DC_MODULE_A-DC事件A模块
     *EPWM_DC_EVENT_1-DCAEVT1事件
     *EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL-不经过滤波的原信号*/
    EPWM_setDigitalCompareEventSource(epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_1,
			EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    /*EPWM数字比较事件源同步：EPWM_DC_MODULE_A-DC事件A模块
        *EPWM_DC_EVENT_1-DCAEVT1事件
        *EPWM_DC_EVENT_INPUT_NOT_SYNCED-采用异步信号输入*/
    EPWM_setDigitalCompareEventSyncMode( epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_1,
			EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    /*TZ错误联防信号EPWM_TZ_SIGNAL_DCAEVT1使能*/
    EPWM_enableTripZoneSignals(epwmBase, EPWM_TZ_SIGNAL_DCAEVT1);

    /*TZ错误联防事件动作配置：
     * EPWM_TZ_ACTION_EVENT_TZA-对于EPWMxA
     * EPWM_TZ_ACTION_LOW-产生置低动作*/
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_TZA,
                           EPWM_TZ_ACTION_LOW);
    /*TZ错误联防事件动作配置：
     * EPWM_TZ_ACTION_EVENT_TZB-对于EPWMxB
     * EPWM_TZ_ACTION_HIGH-产生置高动作*/
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_TZB,
                           EPWM_TZ_ACTION_HIGH);

    /*清除TZ错误联防事件EPWM_TZ_FLAG_DCAEVT1标志*/
    EPWM_clearTripZoneFlag(epwmBase, EPWM_TZ_FLAG_DCAEVT1);
    /*清除TZ错误联防事件EPWM_TZ_OST_FLAG_DCAEVT1-单次触发标志*/
    EPWM_clearOneShotTripZoneFlag(epwmBase, EPWM_TZ_OST_FLAG_DCAEVT1);

    /*TZ错误联防事件中断类型：EPWM_TZ_INTERRUPT_OST-单次错误联防中断*/
    EPWM_enableTripZoneInterrupt(epwmBase, EPWM_TZ_INTERRUPT_OST);
}


void configureCBCTripSignal(uint32_t epwmBase)
{
	/*配置XBAR_EPWM_MUX00_ADCAEVT1-EPWM_ADC复用事件
	 * 通过XBAR_TRIP8-事件8交叉开关输入*/
    XBAR_setEPWMMuxConfig(XBAR_TRIP8, XBAR_EPWM_MUX00_ADCAEVT1);

	/*使能XBAR_TRIP8-事件8交叉开关输入
	 * 通过XBAR_MUX00-交叉开关0输入*/
    XBAR_enableEPWMMux(XBAR_TRIP8, XBAR_MUX00);

	/*选择数据比较事件DC输入：
	 * 通过EPWM_DC_TRIP_TRIPIN8-事件8
	 * 触发EPWM_DC_TYPE_DCAH-DCAH事件*/
    EPWM_selectDigitalCompareTripInput(epwmBase,
    		EPWM_DC_TRIP_TRIPIN8,EPWM_DC_TYPE_DCAH);

    /*产生数字比较事件DCAEVT2的条件配置：
     * EPWM_TZ_DC_OUTPUT_A2-EPWMxA_DCAEVT2
     * EPWM_TZ_EVENT_DCXH_HIGH-DCAH置高*/
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase,
    		EPWM_TZ_DC_OUTPUT_A2,EPWM_TZ_EVENT_DCXH_HIGH);

    /*EPWM数字比较事件源：EPWM_DC_MODULE_A-DC事件A模块
         *EPWM_DC_EVENT_1-DCAEVT1事件
         *EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL-不经过滤波的原信号*/
    EPWM_setDigitalCompareEventSource(epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_2,
			EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    /*EPWM数字比较事件源同步：EPWM_DC_MODULE_A-DC事件A模块
        *EPWM_DC_EVENT_2-DCAEVT2事件
        *EPWM_DC_EVENT_INPUT_NOT_SYNCED-采用异步信号输入*/
    EPWM_setDigitalCompareEventSyncMode(epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_2,
			EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    /*TZ错误联防信号EPWM_TZ_SIGNAL_DCAEVT2使能*/
    EPWM_enableTripZoneSignals(epwmBase, EPWM_TZ_SIGNAL_DCAEVT2);

    /*清除TZ错误联防事件
     * EPWM_TZ_CBC_PULSE_CLR_CNTR_ZERO_PERIOD-在CTR=0或周期值事件时清除CBC事件*/
    EPWM_selectCycleByCycleTripZoneClearEvent(epwmBase,
                                       EPWM_TZ_CBC_PULSE_CLR_CNTR_ZERO_PERIOD);

    /*TZ错误联防事件动作配置：
      * EPWM_TZ_ACTION_EVENT_TZA-对于EPWMxA
      * EPWM_TZ_ACTION_LOW-产生置低动作*/
    EPWM_setTripZoneAction(epwmBase,
    		EPWM_TZ_ACTION_EVENT_TZA,
			EPWM_TZ_ACTION_LOW);
    /*TZ错误联防事件动作配置：
      * EPWM_TZ_ACTION_EVENT_TZB-对于EPWMxB
      * EPWM_TZ_ACTION_HIGH-产生置高动作*/
    EPWM_setTripZoneAction(epwmBase,
    		EPWM_TZ_ACTION_EVENT_TZB,
			EPWM_TZ_ACTION_HIGH);

    /*清除TZ错误联防事件EPWM_TZ_FLAG_DCAEVT2标志*/
    EPWM_clearTripZoneFlag(epwmBase, EPWM_TZ_FLAG_DCAEVT2);
    /*清除TZ错误联防事件EPWM_TZ_CBC_FLAG_DCAEVT2-错误联防DCAEVT2触发标志*/
    EPWM_clearCycleByCycleTripZoneFlag(epwmBase, EPWM_TZ_CBC_FLAG_DCAEVT2);

    /*TZ错误联防事件中断类型：EPWM_TZ_INTERRUPT_CBC-周期错误联防中断*/
    EPWM_enableTripZoneInterrupt(epwmBase, EPWM_TZ_INTERRUPT_CBC);
}

void configureDirectTripSignal(uint32_t epwmBase)
{
	/*配置XBAR_EPWM_MUX00_ADCAEVT1-EPWM_ADC复用事件
	 * 通过XBAR_TRIP9-事件9交叉开关输入*/
    XBAR_setEPWMMuxConfig(XBAR_TRIP9, XBAR_EPWM_MUX00_ADCAEVT1);

	/*使能XBAR_TRIP9-事件9交叉开关输入
	 * 通过XBAR_MUX00-交叉开关0输入*/
    XBAR_enableEPWMMux(XBAR_TRIP9, XBAR_MUX00);

    /*数字比较事件输入选择：EPWM_DC_TRIP_TRIPIN9-事件9输入
     *EPWM_DC_TYPE_DCAH-DCAH事件 */
    EPWM_selectDigitalCompareTripInput(epwmBase,
    		EPWM_DC_TRIP_TRIPIN9,EPWM_DC_TYPE_DCAH);
    /*数字比较事件输入选择：EPWM_DC_TRIP_TRIPIN9-事件9输入
     *EPWM_DC_TYPE_DCBH-DCBH事件 */
    EPWM_selectDigitalCompareTripInput(epwmBase,
    		EPWM_DC_TRIP_TRIPIN9,EPWM_DC_TYPE_DCBH);

    /*产生数字比较事件DCAEVT1的条件配置：
     * EPWM_TZ_DC_OUTPUT_A1-EPWMxA_DCAEVT1
     * EPWM_TZ_EVENT_DCXH_HIGH-DCAH置高*/
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase,
    		EPWM_TZ_DC_OUTPUT_A1,EPWM_TZ_EVENT_DCXH_HIGH);
    /*产生数字比较事件DCBEVT2的条件配置：
     * EPWM_TZ_DC_OUTPUT_B2-EPWMxA_DCBEVT2
     * EPWM_TZ_EVENT_DCXH_HIGH-DCBH置高*/
    EPWM_setTripZoneDigitalCompareEventCondition(epwmBase,
    		EPWM_TZ_DC_OUTPUT_B2,EPWM_TZ_EVENT_DCXH_HIGH);

    /*EPWM数字比较事件源：EPWM_DC_MODULE_A-DC事件A模块
          *EPWM_DC_EVENT_1-DCAEVT1事件
          *EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL-不经过滤波的原信号*/
    EPWM_setDigitalCompareEventSource(epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_1,
			EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);
    /*EPWM数字比较事件源：EPWM_DC_MODULE_B-DC事件B模块
          *EPWM_DC_EVENT_2-DCBEVT2事件
          *EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL-不经过滤波的原信号*/
    EPWM_setDigitalCompareEventSource(epwmBase,
    		EPWM_DC_MODULE_B,EPWM_DC_EVENT_2,
			EPWM_DC_EVENT_SOURCE_ORIG_SIGNAL);

    /*EPWM数字比较事件源同步：EPWM_DC_MODULE_A-DC事件A模块
         *EPWM_DC_EVENT_1-DCAEVT1事件
         *EPWM_DC_EVENT_INPUT_NOT_SYNCED-采用异步信号输入*/
    EPWM_setDigitalCompareEventSyncMode(epwmBase,
    		EPWM_DC_MODULE_A,EPWM_DC_EVENT_1,
			EPWM_DC_EVENT_INPUT_NOT_SYNCED);
    /*EPWM数字比较事件源同步：EPWM_DC_MODULE_B-DC事件B模块
         *EPWM_DC_EVENT_2-DCBEVT2事件
         *EPWM_DC_EVENT_INPUT_NOT_SYNCED-采用异步信号输入*/
    EPWM_setDigitalCompareEventSyncMode(epwmBase,
    		EPWM_DC_MODULE_B,EPWM_DC_EVENT_2,
			EPWM_DC_EVENT_INPUT_NOT_SYNCED);

    /*TZ错误联防事件动作配置：
      * EPWM_TZ_ACTION_EVENT_DCAEVT1-对于DCAEVT1事件
      * EPWM_TZ_ACTION_LOW-使EPWMxA产生置低动作*/
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_DCAEVT1,
                           EPWM_TZ_ACTION_LOW);
    /*TZ错误联防事件动作配置：
      * EPWM_TZ_ACTION_EVENT_DCBEVT2-对于DCBEVT2事件
      * EPWM_TZ_ACTION_HIGH-使EPWMxB产生置高动作*/
    EPWM_setTripZoneAction(epwmBase,
                           EPWM_TZ_ACTION_EVENT_DCBEVT2,
                           EPWM_TZ_ACTION_HIGH);

    /*清除数字比较事件中断标志：EPWM_TZ_INTERRUPT_DCAEVT1-DCAEVT1事件
     *EPWM_TZ_INTERRUPT_DCBEVT2-DCBEVT2事件 */
    EPWM_clearTripZoneFlag(epwmBase, (EPWM_TZ_INTERRUPT_DCAEVT1 |
                                      EPWM_TZ_INTERRUPT_DCBEVT2));

    /*使能数字比较事件中断：EPWM_TZ_INTERRUPT_DCAEVT1-DCAEVT1事件
     *EPWM_TZ_INTERRUPT_DCBEVT2-DCBEVT2事件 */
    EPWM_enableTripZoneInterrupt(epwmBase, (EPWM_TZ_INTERRUPT_DCAEVT1 |
                                            EPWM_TZ_INTERRUPT_DCBEVT2));
}

