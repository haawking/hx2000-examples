/*
 * system.h
 *
 *  Created on: 2023年2月22日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Functional Prototypes
//
void configureEPWM(uint32_t epwmBase);

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);

__interrupt void adcAEvtISR(void);

void GPIO_config(void);

#endif /* SYSTEM_H_ */
