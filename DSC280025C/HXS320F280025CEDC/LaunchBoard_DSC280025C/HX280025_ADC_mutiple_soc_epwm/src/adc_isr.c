#include "system.h"

//
// Globals
//
uint16_t adcAResult0[3];
uint16_t adcAResult1[3];
uint16_t adcAResult2[3];
uint16_t adcCResult0[3];
uint16_t adcCResult1[3];
uint16_t adcCResult2[3];


uint16_t adc_A0result;
uint16_t adc_A1result;
uint16_t adc_A2result;

uint16_t adc_C0result;
uint16_t adc_C1result;
uint16_t adc_C2result;

uint16_t i;


__interrupt void adcA1ISR(void)
{
	/*读取ADC采样结果ADCA_SOC0-ADCA_SOC2*/
    for(i=0;i<3;i++)
    {
    	adcAResult0[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    	if(i>=2)
    	{
    		adc_A0result=adcAResult0[i];
    	}
    }

    for(i=0;i<3;i++)
    {
    	adcAResult1[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
    	if(i>=2)
    	{
    		adc_A1result=adcAResult1[i];
    	}
    }

    for(i=0;i<3;i++)
    {
    	adcAResult2[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2);
    	if(i>=2)
    	{
    		adc_A2result=adcAResult2[i];
    	}
    }

    /*C0-C2通道采样结果*/
    for(i=0;i<3;i++)
    {
       	adcCResult0[i]= ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
       	if(i>=2)
       	{
       		adc_C0result=adcCResult0[i];
       	}
    }

    for(i=0;i<3;i++)
    {
       	adcCResult1[i]= ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER1);
       	if(i>=2)
       	{
       		adc_C1result=adcCResult1[i];
       	}
    }

    for(i=0;i<3;i++)
    {
       	adcCResult2[i]= ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER2);
       	if(i>=2)
       	{
       		adc_C2result=adcCResult2[i];
       	}
    }

    /*清中断标志：ADC_INT_NUMBER1-ADCINT1*/
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    /*检查ADC中断是否溢出，若溢出清除溢出与中断状态：ADC_INT_NUMBER1-ADCINT1*/
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        /*清除溢出中断状态：ADC_INT_NUMBER1-ADCINT1*/
    	ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        /*清除中断状态：ADC_INT_NUMBER1-ADCINT1*/
    	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    /*清除中断应答PIEACK*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}
