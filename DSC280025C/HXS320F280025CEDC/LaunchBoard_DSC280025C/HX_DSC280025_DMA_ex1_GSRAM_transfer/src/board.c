#include "board.h"

void Board_init()
{
	EALLOW;

	PinMux_init();
	DMA_init();
	INTERRUPT_init();

	EDIS;
}

void PinMux_init()
{

}

void DMA_init(){
    DMA_initController();
     
    //myDMA0 ��ʼ��
    DMA_setEmulationMode(DMA_EMULATION_STOP);
    DMA_configAddresses(myDMA0_BASE, destAddr, srcAddr);
    DMA_configBurst(myDMA0_BASE, 8U, 1, 1);
    DMA_configTransfer(myDMA0_BASE, 16U, 1, 1);
    DMA_configWrap(myDMA0_BASE, 65535U, 0, 65535U, 0);
    DMA_configMode(myDMA0_BASE, DMA_TRIGGER_SOFTWARE, DMA_CFG_ONESHOT_DISABLE | DMA_CFG_CONTINUOUS_DISABLE | DMA_CFG_SIZE_16BIT);
    DMA_setInterruptMode(myDMA0_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(myDMA0_BASE);
    DMA_disableOverrunInterrupt(myDMA0_BASE);
    DMA_enableTrigger(myDMA0_BASE);
    DMA_stopChannel(myDMA0_BASE);
}
void INTERRUPT_init(){
	
	// INT_myDMA0�ж�����
	Interrupt_register(INT_myDMA0, &INT_myDMA0_ISR);
	Interrupt_disable(INT_myDMA0);
}

