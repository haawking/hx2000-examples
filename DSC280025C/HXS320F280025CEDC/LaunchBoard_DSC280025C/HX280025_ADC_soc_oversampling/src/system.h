/*
 * system.h
 *
 *  Created on: 2023年2月17日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Function Prototypes
//
void initEPWM();
__interrupt void adcA1ISR(void);

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);



#endif /* SYSTEM_H_ */
