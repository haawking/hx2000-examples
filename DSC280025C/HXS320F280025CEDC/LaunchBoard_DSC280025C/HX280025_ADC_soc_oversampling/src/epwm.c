#include "system.h"

void initEPWM(void)
{
	/*关闭EPWM_SOCA触发ADC采样*/
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

	/*配置EPWM_SOC_A-EPWM_SOCA触发ADC采样：
	 * 在EPWM_SOC_TBCTR_U_CMPA-向上计数TBCTR=CMPA时采样*/
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    /*配置ADC触发事件预分频为EPWM_TBCLK/1,最大可配置15分频*/
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);


    /*配置EPWM比较点：CMPA=1000-脉宽50%*/
    EPWM_setCounterCompareValue(EPWM1_BASE,
    		EPWM_COUNTER_COMPARE_A, 1000);
    /*配置EPWM时基频率=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)=160M/1/1/2k=80kHz*/
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);

    /*配置时钟周期分频：EPWM_CLOCK_DIVIDER_1-CLKDIV低速时钟1分频
      * EPWM_HSCLOCK_DIVIDER_1-HSPCLKDIV高速时钟1分频*/
     EPWM_setClockPrescaler(EPWM1_BASE,
                            EPWM_CLOCK_DIVIDER_1,
                            EPWM_HSCLOCK_DIVIDER_1);

     /*配置EPWM计数模式：EPWM_COUNTER_MODE_STOP_FREEZE-冻结计数器*/
     EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}
