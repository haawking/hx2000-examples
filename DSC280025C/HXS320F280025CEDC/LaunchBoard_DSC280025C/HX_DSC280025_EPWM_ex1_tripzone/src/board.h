#ifndef BOARD_H
#define BOARD_H

//
// Included Files
//

#include "driverlib.h"
#include "device.h"

#define GPIO_PIN_EPWM1_A 0
#define GPIO_PIN_EPWM2_A 2

#define myEPWM1_BASE EPWM1_BASE
#define myEPWM2_BASE EPWM2_BASE

#define myGPIO12 12
#define myGPIO11 11


// Interrupt Settings for INT_myEPWM1_TZ
#define INT_myEPWM1_TZ INT_EPWM1_TZ
#define INT_myEPWM1_TZ_INTERRUPT_ACK_GROUP INTERRUPT_ACK_GROUP2
extern __interrupt void epwm1TZISR(void);

// INT_myEPWM2_TZ�ж�����
#define INT_myEPWM2_TZ INT_EPWM2_TZ
#define INT_myEPWM2_TZ_INTERRUPT_ACK_GROUP INTERRUPT_ACK_GROUP2
extern __interrupt void epwm2TZISR(void);


void	Board_init();
void	EPWM_init();
void	GPIO_init();
void	INPUTXBAR_init();
void	INTERRUPT_init();
void	SYNC_init();
void	PinMux_init();

#endif
