#include "board.h"

void Board_init()
{
	EALLOW;

	PinMux_init();
	SYNC_init();
	EPWM_init();

	EDIS;
}

void PinMux_init()
{
	//
	// EPWM1 -> myEPWM1 引脚复用
	//
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	GPIO_setPinConfig(GPIO_1_EPWM1_B);
	//
	// EPWM2 -> myEPWM2 引脚复用
	//
	GPIO_setPinConfig(GPIO_2_EPWM2_A);
	GPIO_setPinConfig(GPIO_3_EPWM2_B);
	//
	// EPWM3 -> myEPWM3 引脚复用
	//
	GPIO_setPinConfig(GPIO_4_EPWM3_A);
	GPIO_setPinConfig(GPIO_5_EPWM3_B);

}

void EPWM_init(){
	//ePWM1
	//ePWM1设置时钟分频，时钟分频/1，高速时钟分频/1，160M/1/1=160M
    EPWM_setClockPrescaler(myEPWM1_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);	
    //ePWM1时基周期，2000，160M/(2000*2)=40K
    EPWM_setTimeBasePeriod(myEPWM1_BASE, 2000);	
    //ePWM1时基计数器，0
    EPWM_setTimeBaseCounter(myEPWM1_BASE, 0);	
    //ePWM1时基计数器模式，上下计数
    EPWM_setTimeBaseCounterMode(myEPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);	
    //ePWM1关闭相位偏移装载
    EPWM_disablePhaseShiftLoad(myEPWM1_BASE);	
    //ePWM1设置相位偏移，0
    EPWM_setPhaseShift(myEPWM1_BASE, 0);	
    //ePWM1设置计数器比较值，计数器比较A，50
    EPWM_setCounterCompareValue(myEPWM1_BASE, EPWM_COUNTER_COMPARE_A, 50);	
    //ePWM1设置计数器比较影子装载模式，计数器比较值A，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM1设置计数器比较值，计数器比较值B，1950
    EPWM_setCounterCompareValue(myEPWM1_BASE, EPWM_COUNTER_COMPARE_B, 1950);	
    //ePWM1设置计数器比较影子装载模式，计数器比较值B，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM1_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM1设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM1设置动作限定，ePWMxA，输出引脚没有变化，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM1设置动作限定，ePWMxA，输出引脚置高，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM1设置动作限定，ePWMxA，输出引脚置低，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM1设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM1设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM1设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM1设置动作限定，ePWMxB，输出引脚没有变化，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM1设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM1设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM1设置动作限定，ePWMxB，输出引脚置高，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM1设置动作限定，ePWMxB，输出引脚置低，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM1使能中断
    EPWM_enableInterrupt(myEPWM1_BASE);	
    //ePWM1设置中断源，时基计数器等于零
    EPWM_setInterruptSource(myEPWM1_BASE, EPWM_INT_TBCTR_ZERO);	
    //ePWM1设置中断事件计数，3
    EPWM_setInterruptEventCount(myEPWM1_BASE, 3);	

    //ePWM2设置时钟分频，时钟分频/1，高速时钟分频/1，160M/1/1=160M
    EPWM_setClockPrescaler(myEPWM2_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);	
    //ePWM2时基周期，2000，160M/(2000*2)=40K
    EPWM_setTimeBasePeriod(myEPWM2_BASE, 2000);	
    //ePWM2时基计数器，0
    EPWM_setTimeBaseCounter(myEPWM2_BASE, 0);
    //ePWM2时基计数器模式，上下计数
    EPWM_setTimeBaseCounterMode(myEPWM2_BASE, EPWM_COUNTER_MODE_UP_DOWN);	
    //ePWM2关闭相位偏移装载
    EPWM_disablePhaseShiftLoad(myEPWM2_BASE);	
    //ePWM2设置相位偏移，0
    EPWM_setPhaseShift(myEPWM2_BASE, 0);	
    //ePWM2设置计数器比较值，计数器比较A，50
    EPWM_setCounterCompareValue(myEPWM2_BASE, EPWM_COUNTER_COMPARE_A, 50);	
    //ePWM2设置计数器比较影子装载模式，计数器比较值A，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM2_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM2设置计数器比较值，计数器比较值B，50
    EPWM_setCounterCompareValue(myEPWM2_BASE, EPWM_COUNTER_COMPARE_B, 50);	
    //ePWM2设置计数器比较影子装载模式，计数器比较值B，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM2_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM2设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM2设置动作限定，ePWMxA，输出引脚没有变化，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM2设置动作限定，ePWMxA，输出引脚置高，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM2设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM2设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM2设置动作限定，ePWMxA，输出引脚置低，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM2设置动作限定，ePWMxB，输出引脚置低，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM2设置动作限定，ePWMxB，输出引脚置高，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM2设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM2设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM2设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM2设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM2启用中断
    EPWM_enableInterrupt(myEPWM2_BASE);	
    //ePWM2设置中断源，TBCTR=0中断
    EPWM_setInterruptSource(myEPWM2_BASE, EPWM_INT_TBCTR_ZERO);	
    //ePWM2中断事件计数，3
    EPWM_setInterruptEventCount(myEPWM2_BASE, 3);

    //ePWM3设置时钟分频，时钟分频/1，高速时钟分频/1，160M/1/1=160M
    EPWM_setClockPrescaler(myEPWM3_BASE, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);	
    //ePWM3时基周期，2000，160M/(2000*2)=40K
    EPWM_setTimeBasePeriod(myEPWM3_BASE, 2000);	
    //ePWM3时基计数器，0
    EPWM_setTimeBaseCounter(myEPWM3_BASE, 0);	
    //ePWM3时基计数器模式，上下计数
    EPWM_setTimeBaseCounterMode(myEPWM3_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    //ePWM3关闭相位偏移装载
    EPWM_disablePhaseShiftLoad(myEPWM3_BASE);
    //ePWM3设置相位偏移，0
    EPWM_setPhaseShift(myEPWM3_BASE, 0);	
    //ePWM3设置计数器比较值，计数器比较A，50
    EPWM_setCounterCompareValue(myEPWM3_BASE, EPWM_COUNTER_COMPARE_A, 50);	
    //ePWM3设置计数器比较影子装载模式，计数器比较值A，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM3_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM3设置计数器比较值，计数器比较值B，1950
    EPWM_setCounterCompareValue(myEPWM3_BASE, EPWM_COUNTER_COMPARE_B, 1950);
    //ePWM3设置计数器比较影子装载模式，计数器比较值B，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM3_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM3设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM3设置动作限定，ePWMxA，输出引脚置高，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM3设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM3设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM3设置动作限定，ePWMxA，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM3设置动作限定，ePWMxA，输出引脚置低，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM3设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM3设置动作限定，ePWMxB，输出引脚置低，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM3设置动作限定，ePWMxB，输出引脚置高，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM3设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM3设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM3设置动作限定，ePWMxB，输出引脚没有变化，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM3_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM3启用中断
    EPWM_enableInterrupt(myEPWM3_BASE);	
    //ePWM3设置中断源，TBCTR=0中断
    EPWM_setInterruptSource(myEPWM3_BASE, EPWM_INT_TBCTR_ZERO);	
    //ePWM3中断事件计数，3
    EPWM_setInterruptEventCount(myEPWM3_BASE, 3);	
}

void SYNC_init(){
	//EPWM1_SYNCOUNT
	SysCtl_setSyncOutputConfig(SYSCTL_SYNC_OUT_SRC_EPWM1SYNCOUT);
	// SOCA
	SysCtl_enableExtADCSOCSource(0);
	// SOCB
	SysCtl_enableExtADCSOCSource(0);
}
