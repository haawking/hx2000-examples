#ifndef BOARD_H
#define BOARD_H

//
// Included Files
//

#include "driverlib.h"
#include "device.h"

#define GPIO_PIN_EPWM1_A 0
#define GPIO_PIN_EPWM1_B 1
#define GPIO_PIN_EPWM2_A 2
#define GPIO_PIN_EPWM2_B 3
#define GPIO_PIN_EPWM3_A 4
#define GPIO_PIN_EPWM3_B 5

#define myEPWM1_BASE EPWM1_BASE
#define myEPWM2_BASE EPWM2_BASE
#define myEPWM3_BASE EPWM3_BASE

void	Board_init();
void	EPWM_init();
void	SYNC_init();
void	PinMux_init();

#endif
