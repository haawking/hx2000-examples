/*
 * FlashApi.h
 *
 *  Created on: 2023年5月9日
 *      Author: liyuyao
 */

#ifndef SRC_FLASHAPI_H_
#define SRC_FLASHAPI_H_

/*
 * void Fapi_initializeAPI (uint32 u32HclkFrequency)
 * 功能：配置flash的控制字
 * 入参：u32HclkFrequency是指芯片的运行频率，输入范围2~160。
 * */
#define Fapi_initializeAPI               \
	(void (*)(uint32 u32HClkFrequency))0x7fa900

/*
 * Uint16 Flash280025_Verify
*		(Uint32 *FlashAddr, Uint32 *BufAddr,
*		Uint32 Length)
 * 功能：将FLASH中的数据与缓存Buf中的数据进行一致性比对。
* 入参：FlashAddr的取值范围为0x600000~0x63FFFF。
* 入参：BufAddr是缓存的数据的首地址。
* 入参：Length是BufAddr数组的长度，32bit数据的个数。
* 返回值：0代表Flash中的数据写入成功，其他值代表Flash中的数据与缓存Buf中的数据不一致的个数。
 * */
#define Flash280025_Verify 		\
		(Uint16 (*)(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length))0x7fa99e

/*
 * Uint16  Flash280025_SectorErase (Uint8 SectorIdx, Uint8 zone_select)
 * 功能：根据输入的SectorIdx，对FLASH相应的SectorIdx进行擦除。
 * 入参：SectorIdx的取值范围为0~127。
 * 入参：zone_select的取值范围为1，2。1代表zone1；2代表zone2。
 * 返回值：0代表成功，10代表未解密，20代表SectorIdx的取值超限，22代表擦除失败
 * */
#define Flash280025_SectorErase      \
    (Uint16 (*)(Uint8 SectorIdx,uint8 zone_select, void *param))0x7fa9ec

/*
 * uint16  Flash280025_MassErase(Uint8 zone_select)
 * 功能：将FLASH空间全部擦除。
 * 入参：zone_select的取值范围为1，2。1代表zone1；2代表zone2。
 * 返回值：0代表成功，10代表未解密，22代表擦除失败。
 * */
#define Flash280025_MassErase    \
	(Uint16 (*)(void *param))0x7fab90

/*
 *
 * Uint16  Flash280025_Program
 *		(Uint32 *FlashAddr, Uint32 *BufAddr,
 *		Uint32 Length,void *param,
 *		Uint8 zone_select)
*
*功能：从给定的地址处开始，根据长度，将缓存中的数据，烧写到Flash中。
*入参：FlashAddr的取值范围为0x600000~0x63FFFF。
*入参：BufAddr是缓存的数据的首地址。
*入参：Length是BufAddr数组的长度，32bit数据的个数。
*入参：zone_select的取值范围为1，2。1代表zone1；2代表zone2。
*返回值：0代表成功，10代表未解密，12代表FlashAddr的取值超限，30代表烧写失败。
* */
#define Flash280025_Program   \
	(Uint16 (*)(Uint32 *FlashAddr,Uint32 *BufAddr,Uint32 Length,void *param))0x7fad7a

/*
* uint16 Flash280025_UserOTP_32BIT_Program
*		(Uint32 *OTPAddr,Uint32 Data,
*		Uint8 zone_select)
* 功能：将32位数据写入相应OTPAddr地址。
*入参：OTPAddr的取值范围为0x7A0000~0x7A07FF。
*入参：Data是缓存中将要烧写的数据。
*入参：zone_select的取值范围为1，2。1代表zone1；2代表zone2。
*返回值： 0代表成功，10代表未解密，12代表OTPAddrr的取值超限，30代表烧写失败。
* */
#define UserOtp32BitProgram                 \
	(Uint16 (*)(Uint32 *FlashAddr, Uint32 data, Uint8 zone, void *param))0x7fb2a2

/*
 * uint16  Flash280025_UserOTP_Program
*		(Uint32 *OTPAddr,Uint32 *BufAddr,
*		Uint32 Length,Uint32 *OTPTempBuffer,
*		Uint8 zone_select)
 * 功能：从给定的地址处开始，根据长度，将缓存中的数据，烧写到user otp中。
 *入参：OTPAddr的取值范围为0x7A0000~0x7A07FF。
 *入参：BufAddr时缓存的数据的首地址。
 *入参：Length是BufAddr数组的长度，32bit数据的个数。
 *入参：OTPTempBuffer是长度为Length的32位数组的首地址。
 *入参：zone_select的取值范围为1，2。1代表zone1；2代表zone2。
 *返回值： 0代表成功，10代表未解密，12代表OTPAddrr的取值超限，30代表烧写失败。
 * */
#define UserOTP_Program 		\
	(Uint16 (*)(Uint32 *FlashAddr,Uint32 *BufAddr,Uint32 Lenghth, Uint32 *OTPTempBuffer, Uint8 zone, void *param))0x7fb0bc


#endif /* SRC_FLASHAPI_H_ */
