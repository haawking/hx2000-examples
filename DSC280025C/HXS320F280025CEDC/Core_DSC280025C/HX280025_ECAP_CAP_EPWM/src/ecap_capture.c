/******************************************************************
 文 档 名：     ecap_capture.c
 D S P：       DSC280025
 使 用 库：
 作     用：		20k-320k PWM波捕获
 说     明：      提供ECAP捕获功能配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月17日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/


#include "system.h"

void ecap_cap_config()
{
    /*关闭ECAP事件捕获相关中断：ECAP_ISR_SOURCE_CAPTURE_EVENT_x-捕获事件x
     *ECAP_ISR_SOURCE_COUNTER_OVERFLOW-捕获源计数溢出
     *ECAP_ISR_SOURCE_COUNTER_PERIOD-捕获源计数等于周期值事件中断
     *ECAP_ISR_SOURCE_COUNTER_COMPARE-捕获源计数等于比较值事件中断 */
    ECAP_disableInterrupt(ECAP1_BASE,
                          (ECAP_ISR_SOURCE_CAPTURE_EVENT_1  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_2  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_3  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_4  |
                           ECAP_ISR_SOURCE_COUNTER_OVERFLOW |
                           ECAP_ISR_SOURCE_COUNTER_PERIOD   |
                           ECAP_ISR_SOURCE_COUNTER_COMPARE));
    /*清除ECAP事件捕获相关中断ECAP_ISR_SOURCE_CAPTURE_EVENT_x-捕获事件x
     *ECAP_ISR_SOURCE_COUNTER_OVERFLOW-捕获源计数溢出
     *ECAP_ISR_SOURCE_COUNTER_PERIOD-捕获源计数等于周期值事件中断
     *ECAP_ISR_SOURCE_COUNTER_COMPARE-捕获源计数等于比较值事件中断 */
    ECAP_clearInterrupt(ECAP1_BASE,
                        (ECAP_ISR_SOURCE_CAPTURE_EVENT_1  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_2  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_3  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_4  |
                         ECAP_ISR_SOURCE_COUNTER_OVERFLOW |
                         ECAP_ISR_SOURCE_COUNTER_PERIOD   |
                         ECAP_ISR_SOURCE_COUNTER_COMPARE));

    /*关闭ECAP时间戳捕获*/
    ECAP_disableTimeStampCapture(ECAP1_BASE);
    /*停止ECAP时间戳计数*/
    ECAP_stopCounter(ECAP1_BASE);
    /*ECAP捕获模式配置*/
    ECAP_enableCaptureMode(ECAP1_BASE);
    // Sets the capture mode.
    /*ECAP捕获参数配置：ECAP_ONE_SHOT_CAPTURE_MODE单次触发，ECAP_CONTINUOUS_CAPTURE_MODE连续触发
     * ECAP_EVENT_x事件x捕获*/
    ECAP_setCaptureMode(ECAP1_BASE,ECAP_ONE_SHOT_CAPTURE_MODE,ECAP_EVENT_4);
    /*ECAP0捕获事件预分频设置：0-无分频*/
    ECAP_setEventPrescaler(ECAP1_BASE, 0U);

    /*ECAP0捕获事件1捕获方式：ECAP_EVNT_FALLING_EDGE下降沿捕获*/
    ECAP_setEventPolarity(ECAP1_BASE,ECAP_EVENT_1,ECAP_EVNT_FALLING_EDGE);
    /*ECAP0捕获事件2捕获方式：ECAP_EVNT_RISING_EDGE上升沿捕获*/
    ECAP_setEventPolarity(ECAP1_BASE,ECAP_EVENT_2,ECAP_EVNT_RISING_EDGE);
    /*ECAP0捕获事件3捕获方式：ECAP_EVNT_FALLING_EDGE下降沿捕获*/
    ECAP_setEventPolarity(ECAP1_BASE,ECAP_EVENT_3,ECAP_EVNT_FALLING_EDGE);
    /*ECAP0捕获事件4捕获方式：ECAP_EVNT_RISING_EDGE上升沿捕获*/
    ECAP_setEventPolarity(ECAP1_BASE,ECAP_EVENT_4,ECAP_EVNT_RISING_EDGE);

    /*ECAP0事件x计数使能复位*/
    ECAP_enableCounterResetOnEvent(ECAP1_BASE,ECAP_EVENT_1);
    ECAP_enableCounterResetOnEvent(ECAP1_BASE,ECAP_EVENT_2);
    ECAP_enableCounterResetOnEvent(ECAP1_BASE,ECAP_EVENT_3);
    ECAP_enableCounterResetOnEvent(ECAP1_BASE,ECAP_EVENT_4);
    /*选择ECAP0输入ECAP_INPUT_INPUTXBAR7交叉开关7*/
    ECAP_selectECAPInput(ECAP1_BASE,ECAP_INPUT_INPUTXBAR7);

    /*选择ECAP0相移：0-无相移*/
    ECAP_setPhaseShiftCount(ECAP1_BASE,0U);
    /*选择ECAP0装载计数使能*/
    ECAP_enableLoadCounter(ECAP1_BASE);
    /*ECAP的同步配置:ECAP_SYNC_OUT_SYNCI计数=0同步输出,
     * ECAP_SYNC_OUT_COUNTER_PRD计数=周期值同步输出,ECAP_SYNC_OUT_DISABLED关闭同步输出*/
    ECAP_setSyncOutMode(ECAP1_BASE,ECAP_SYNC_OUT_SYNCI);
    /*配置ECAP0仿真模式*/
    ECAP_setEmulationMode(ECAP1_BASE,ECAP_EMULATION_STOP);
    /*ECAP的APWM的同步脉冲源配置：ECAP_SYNC_IN_PULSE_SRC_DISABLE关闭*/
    ECAP_setSyncInPulseSource(ECAP1_BASE,ECAP_SYNC_IN_PULSE_SRC_DISABLE);

    /*ECAP启动时间戳计数*/
    ECAP_startCounter(ECAP1_BASE);
    /*ECAP时间戳捕获使能*/
    ECAP_enableTimeStampCapture(ECAP1_BASE);
    /*ECAP重载计数*/
    ECAP_reArm(ECAP1_BASE);


    /*ECAP中断源使能：ECAP_ISR_SOURCE_CAPTURE_EVENT_4-捕获事件4*/
    ECAP_enableInterrupt(ECAP1_BASE,(ECAP_ISR_SOURCE_CAPTURE_EVENT_4));
}

void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED1灭*/
    GPIO_writePin(31, 1);

    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);
    /*GPIO34写入1,初始化LED2灭*/
    GPIO_writePin(34, 1);
    EDIS;
}
