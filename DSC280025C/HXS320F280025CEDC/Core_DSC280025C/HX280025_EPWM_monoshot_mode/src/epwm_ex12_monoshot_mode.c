/******************************************************************
 文 档 名：       HX_DSC280025_EPWM_Monoshot_mode.c
 开 发 环 境：  Haawking IDE V2.2.5Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM单相输出接收与触发
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 （1）使用外部同步与TI动作限定符事件特性来实现所需输出，
 A.生成基于外部的单相PWM输出触发，即产生一个单一的脉冲输出接收外部触发器。
 B.下一个脉冲仅有在下一次触发时才会产生来了。
（2）ePWM1用于产生单相脉冲输出：
A.配置为接收时产生0.5us的单脉冲外部触发器
B.可通过启用相位同步来实现配置EPWMxSYNCI为EXTSYNCIN1。
C.该同步事件还配置为动作限定符的TI事件，以设置输出高，而CTR=PRD时动作输出低
（3）ePWM2用于外部输出触发：
A.产生100kHz信号，脉宽1%的上升沿触发器；
B.采用交叉开关X-BAR输入馈电，连接到EXTSYCIN1，不需要外部连接；
 *
 版 本：      V1.0.0
 时 间：      2023年2月6日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "system.h"

int main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定解除*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*屏蔽外设时钟TBCLK同步使能，便于PWM初始化配置*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EALLOW;
	/*配置GPIO0-EPWM1A，GPIO2-EPWM2A*/
    epwm_gpio();
	/*配置GPIO2为XBAR_INPUT5交叉开关输入5*/
    inputxbar_init();
    sync_init();
	/*PWM配置*/
    epwm1_config();
    epwm2_config();
    EDIS;

	/*外设时钟TBCLK同步使能，使PWM完成配置且同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

	/*使能打开全局中断*/
    EINT;
    ERTM;

    for(;;)
    {
        NOP;

    }
}

