/******************************************************************
 文 档 名：       HX_DSC280025_EPWM_CMPC_INTERRUPT
 开 发 环 境：  Haawking IDE V2.2.10Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM CMPC脉宽比较点扩充与触发事件中断
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，
 （1）通过EPWM1SYNCO在向上计数CTR=CMPC时，
 产生EPWM1SYNCI输入同步信号，产生TI事件，触发EPWM1A动作
 （2）可通过事件中断扩展在TI事件触发时产生事件中断，以用于更新占空比
现象：EPWM1A在CTR=CMPC-12.5%比较点事件置高
向上计数CTR=CMPC事件时，可通过TI事件触发中断，闪灯GPIO31/LED1

示波器：GPIO0/EPWM1在向上计数CTR=CMPC事件置高
此时通过TI事件触发中断，使GPIO31/LED1翻转闪灯
观察GPIO0与GPIO31的置高点与翻转点位置相同

应用：可采用此方法对PWM计数比较事件进行扩充划分，
由Type2 PWM的7个点，到Type4 PWM的最大可划分11个点

 *
 版 本：      V1.0.0
 时 间：      2023年2月8日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/



#include "system.h"

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定*/
    Device_initGPIO();

    /*GPIO配置，用于显示中断状态*/
    GPIO_config();

	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*中断入口地址INT_EPWM1,指向执行epwm1ISR中断服务程序*/
    Interrupt_register(INT_EPWM1, &epwm1ISR);

	/*屏蔽TBCLK时基同步，便于PWM初始化配置写入*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
    
    EALLOW;
	/*配置GPIO0为EPWM1A,GPIO1为EPWM1B*/
    epwm_gpio();
	/*同步策略*/
    sync_config();
	/*EPWM配置*/
    epwm_config();
    EDIS;

	/*使能TBCLK时基同步，以使PWM配置写入，并实现多PWM同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

	/*中断使能INT_EPWM1*/
    Interrupt_enable(INT_EPWM1);

	/*打开全局中断*/
    EINT;
    ERTM;

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}

void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED1灭*/
    GPIO_writePin(31, 1);
    EDIS;
}



