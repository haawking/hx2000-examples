/******************************************************************
 文 档 名：       HX_DSC280025_INTEERUPT_MUTIPLY_HANDLE_I2C_SCI_SPI_LOOPBACK
 开 发 环 境：  Haawking IDE V2.2.10Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      I2C,SCI,SPI内环多中断收发数据处理
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用内环中断方式,
 完成I2C,SPI,SCI FIFO的数据收发,多中断处理

连线：无

收发数据配置：
(1)I2C数据流:
//!  0000 0001 \n
//!  0001 0002 \n
//!  0002 0003 \n
//!  .... \n
//!  00FE 00FF \n
//!  00FF 0000 \n
(2)SPI数据流：
//!  0000 0001 \n
//!  0001 0002 \n
//!  0002 0003 \n
//!  .... \n
//!  FFFE FFFF \n
//!  FFFF 0000 \n
现象：
查看如下变量是否一致，验证收发数据成功：
(1)I2C
//!  - \b sDatai2cA - 发送数据
//!  - \b rDatai2cA - 接收数据
//!  - \b rDataPoint - 接收到的数据计数
 * 接收数据与发送数据一致，则点亮GPIO31/LED1
 * 不一致，则LED1灭
 *
(2)SPI
//!  - \b sDataspiA - 发送数据
//!  - \b rDataspiA - 接收数据
//!  - \b rDataPointspiA - 接收到的数据计数
 * 接收数据与发送数据一致，则点亮GPIO31/LED1 GPIO34/LED2
 * 不一致，则LED1灭 LED2灭
 *
 *
(3)SCI
//!  - \b sDatasciA - 发送数据
//!  - \b rDatasciA - 接收数据
//!  - \b rDataPointA - 接收到的数据计数
 *  接收数据与发送数据一致，则点亮GPIO34/LED2
 * 不一致，则LED2灭
 *
 *
 版 本：      V1.0.0
 时 间：      2022年11月23日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "system.h"

//
// I2C data Globals
//
uint16_t sDatai2cA[2];                  // Send I2CA data buffer
uint16_t rDatai2cA[2];                  // Receive I2CA data buffer
uint16_t rDataPoint = 0;            // To keep track of where we are in the
                                    // data stream to check received I2C data
//
// SCI data Globals
//
uint16_t sDatasciA[2];              // Send SCI-A data buffer
uint16_t rDatasciA[2];              // Receive SCI-A data buffer
uint16_t rDataPointA = 0;           // Used for checking the received SCI data

//
// SPI data Globals
//
uint16_t sDataspiA[2];                  // Send SPI data buffer
uint16_t rDataspiA[2];                  // Receive SPI data buffer
uint16_t rDataPointspiA = 0;            // To keep track of where we are in the
                                    // data stream to check received SPI data
void GPIO_config(void);

//
// Main
//
int main(void)
{
    uint16_t i;
    /*系统时钟初始化*/
    Device_init();

    /*GPIO锁定关闭*/
    Device_initGPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*关中断,清中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口地址INT_I2CA_FIFO,指向执行i2cFIFOISR服务程序*/
    Interrupt_register(INT_I2CA_FIFO, &i2cFIFOISR);
    /*中断入口地址INT_SCIA_RX,指向执行sciaRXFIFOISR服务程序*/
    Interrupt_register(INT_SCIA_RX, &sciaRXFIFOISR);
    /*中断入口地址INT_SCIA_TX,指向执行sciaTXFIFOISR服务程序*/
    Interrupt_register(INT_SCIA_TX, &sciaTXFIFOISR);
    /*中断入口地址INT_SPIA_TX,指向执行spiTxFIFOISR服务程序*/
    Interrupt_register(INT_SPIA_TX, &spiTxFIFOISR);
    /*中断入口地址INT_SPIA_RX,指向执行spiRxFIFOISR服务程序*/
    Interrupt_register(INT_SPIA_RX, &spiRxFIFOISR);

    /*I2C FIFO功能配置*/
    initI2CFIFO();

    /*SCI FIFO功能配置*/
    initSCIAFIFO();

    /*SPI FIFO功能配置*/
    initSPIFIFO();

    /*初始化定义I2C数据缓冲区数据*/
    for(i = 0; i < 2; i++)
    {
        sDatai2cA[i] = i;
        rDatai2cA[i]= 0;
    }

    /*初始化定义SCI发送数据*/
    for(i = 0; i < 2; i++)
    {
        sDatasciA[i] = i;
    }

    /*初始化定义SPI数据缓冲区*/
    for(i = 0; i < 2; i++)
    {
        sDataspiA[i] = i;
        rDataspiA[i]= 0;
    }

    //
    // Enable interrupts required for this example
    //
    /*使能打开INT_I2CA_FIFO中断*/
    Interrupt_enable(INT_I2CA_FIFO);
    /*使能打开INT_SCIA_RX中断*/
    Interrupt_enable(INT_SCIA_RX);
    /*使能打开INT_SCIA_TX中断*/
    Interrupt_enable(INT_SCIA_TX);
    /*使能打开INT_SPIA_TX中断*/
    Interrupt_enable(INT_SPIA_TX);
    /*使能打开INT_SPIA_RX中断*/
    Interrupt_enable(INT_SPIA_RX);

    /*使能打开全局与实时中断*/
    EINT;
    ERTM;

    while(1)
    {}

    return 0;
}
//
// End of File
//
void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED1灭*/
    GPIO_writePin(31, 1);


    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

    /*GPIO34写入1,初始化LED2灭*/
    GPIO_writePin(34, 1);
    EDIS;
}

