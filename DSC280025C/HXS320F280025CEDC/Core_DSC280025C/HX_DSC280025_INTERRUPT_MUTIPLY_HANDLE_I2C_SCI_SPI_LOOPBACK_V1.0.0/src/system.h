/*
 * system.h
 *
 *  Created on: 2022��11��23��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Function Prototypes
//
void initI2CFIFO(void);
__interrupt void i2cFIFOISR(void);
__interrupt void sciaTXFIFOISR(void);
__interrupt void sciaRXFIFOISR(void);
void initSCIAFIFO(void);
void initSPIFIFO(void);
__interrupt void spiTxFIFOISR(void);
__interrupt void spiRxFIFOISR(void);

//
// Defines
//
#define SLAVE_ADDRESS   0x3C

//
// I2C data Globals
//
extern uint16_t sDatai2cA[2];                  // Send I2CA data buffer
extern uint16_t rDatai2cA[2];                  // Receive I2CA data buffer
extern uint16_t rDataPoint;            // To keep track of where we are in the
                                    // data stream to check received I2C data
//
// SCI data Globals
//
extern uint16_t sDatasciA[2];              // Send SCI-A data buffer
extern uint16_t rDatasciA[2];              // Receive SCI-A data buffer
extern uint16_t rDataPointA;           // Used for checking the received SCI data

//
// SPI data Globals
//
extern uint16_t sDataspiA[2];                  // Send SPI data buffer
extern uint16_t rDataspiA[2];                  // Receive SPI data buffer
extern uint16_t rDataPointspiA;            // To keep track of where we are in the
                                    // data stream to check received SPI data


#endif /* SYSTEM_H_ */
