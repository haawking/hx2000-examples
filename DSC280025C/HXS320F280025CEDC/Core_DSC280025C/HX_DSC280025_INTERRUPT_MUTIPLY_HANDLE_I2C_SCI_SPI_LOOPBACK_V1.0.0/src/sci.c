/******************************************************************
 文 档 名：     sci.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供SCI的FIFO功能配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月23日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "system.h"

void initSCIAFIFO()
 {
     /*配置SCI模块：系统时钟，波特率9600bps
      * SCI_CONFIG_WLEN_8数据长度8位，SCI_CONFIG_STOP_ONE停止位1位
      * SCI_CONFIG_PAR_NONE奇偶校验无校验*/
     SCI_setConfig(SCIA_BASE, DEVICE_LSPCLK_FREQ, 9600, (SCI_CONFIG_WLEN_8 |
                                                         SCI_CONFIG_STOP_ONE |
                                                         SCI_CONFIG_PAR_NONE));
     /*使能SCI模块：TXENA，RXENA，软复位*/
     SCI_enableModule(SCIA_BASE);
     /*使能SCI的内环模式*/
     SCI_enableLoopback(SCIA_BASE);
     /*采用SCIRST重启SCI通道*/
     SCI_resetChannels(SCIA_BASE);
     /*使能SCI FIFO模块*/
     SCI_enableFIFO(SCIA_BASE);

     /*使能SCI接收与发送FIFO中断*/
     SCI_enableInterrupt(SCIA_BASE, (SCI_INT_RXFF | SCI_INT_TXFF));
     /*关闭RXERR中断*/
     SCI_disableInterrupt(SCIA_BASE, SCI_INT_RXERR);

     /*使能SCI FIFO中断线SCI_FIFO_TX2发送FIFO中断线2，SCI_FIFO_RX2接收FIFO中断线2*/
     SCI_setFIFOInterruptLevel(SCIA_BASE, SCI_FIFO_TX2, SCI_FIFO_RX2);
     /*SCI软复位，释放SCI模块*/
     SCI_performSoftwareReset(SCIA_BASE);
     /*复位SCI发送TXFIFO*/
     SCI_resetTxFIFO(SCIA_BASE);
     /*复位SCI接收RXFIFO*/
     SCI_resetRxFIFO(SCIA_BASE);
 }
