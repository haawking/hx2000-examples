/******************************************************************
 文 档 名：     spi_isr.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供SPI的FIFO中断收发程序
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月23日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

__interrupt void spiTxFIFOISR(void)
 {
     uint16_t i;

     /*向SPITXBUF缓冲区发送数据*/
     for(i = 0; i < 2; i++)
     {
        SPI_writeDataNonBlocking(SPIA_BASE, sDataspiA[i]);
     }

     /*发送数据循环增加*/
     for(i = 0; i < 2; i++)
     {
        sDataspiA[i] = sDataspiA[i] + 1;
     }

     /*清除发送TXFIFO的中断标志*/
     SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_TXFF);
     /*清除中断应答：第6组*/
     Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
 }

  __interrupt void spiRxFIFOISR(void)
 {
     uint16_t i;

     /*从SPIRXBUF中读取接收到的数据*/
     for(i = 0; i < 2; i++)
     {
         rDataspiA[i] = SPI_readDataNonBlocking(SPIA_BASE);
     }

     /*核对接收到的数据数量是否与发送数据一一对应*/
     for(i = 0; i < 2; i++)
     {
         if(rDataspiA[i] != (rDataPointspiA + i))
         {
              /*GPIO31写入1,LED1灭*/
             GPIO_writePin(31, 1);
             /*GPIO34写入1,LED2灭*/
             GPIO_writePin(34, 1);
         }
         else
         {
             /*GPIO31写入0,点亮LED1*/
            GPIO_writePin(31, 0);
            /*GPIO34写入0,点亮LED2*/
            GPIO_writePin(34, 0);
         }
     }

     rDataPointspiA++;

     /*清除接收RXFIFO的中断标志*/
     SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_RXFF);
     /*清除中断应答：第6组*/
     Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
 }
