/******************************************************************
 文 档 名：     apwm_phase_shift.c
 D S P：       DSC280025
 使 用 库：
 作     用：		产生两路非对称，相位相差30%脉宽，即108度的16k PWM方波
 说     明：      提供ECAP的APWM配置与交叉开关输入配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月17日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

void ECAP_apwm_config()
{
    /*ECAP1暂停时间戳计数*/
    ECAP_stopCounter(ECAP1_BASE);
    /*ECAP1的APWM模式使能*/
    ECAP_enableAPWMMode(ECAP1_BASE);
    /*ECAP1的APWM模式周期配置:160MHz/10k=16k*/
    ECAP_setAPWMPeriod(ECAP1_BASE,10000U);
    /*ECAP1的APWM模式比较值配置:50%*/
    ECAP_setAPWMCompare(ECAP1_BASE,5000U);
    /*ECAP1的APWM输出极性配置:ECAP_APWM_ACTIVE_LOW低,ECAP_APWM_ACTIVE_HIGH高*/
    ECAP_setAPWMPolarity(ECAP1_BASE,ECAP_APWM_ACTIVE_LOW);
    /*ECAP1的APWM相位偏移配置:0*/
    ECAP_setPhaseShiftCount(ECAP1_BASE,0U);
    /*ECAP1的APWM的相位偏移装载关闭*/
    ECAP_disableLoadCounter(ECAP1_BASE);
    /*ECAP1的APWM的同步配置:ECAP_SYNC_OUT_SYNCI计数=0同步输出,
     * ECAP_SYNC_OUT_COUNTER_PRD计数=周期值同步输出,ECAP_SYNC_OUT_DISABLED关闭同步输出*/
    ECAP_setSyncOutMode(ECAP1_BASE,ECAP_SYNC_OUT_COUNTER_PRD);
    /*ECAP1的APWM的仿真模式停止*/
    ECAP_setEmulationMode(ECAP1_BASE,ECAP_EMULATION_STOP);
    /*ECAP1的APWM的同步脉冲源配置：ECAP_SYNC_IN_PULSE_SRC_DISABLE关闭*/
    ECAP_setSyncInPulseSource(ECAP1_BASE,ECAP_SYNC_IN_PULSE_SRC_DISABLE);

    /*ECAP2暂停时间戳计数*/
    ECAP_stopCounter(ECAP2_BASE);
    /*ECAP2的APWM模式使能*/
    ECAP_enableAPWMMode(ECAP2_BASE);
    /*ECAP2的APWM模式周期配置:160MHz/10k=16k*/
    ECAP_setAPWMPeriod(ECAP2_BASE,10000U);
    /*ECAP2的APWM模式比较值配置:50%*/
    ECAP_setAPWMCompare(ECAP2_BASE,5000U);
    /*ECAP2的APWM输出极性配置:ECAP_APWM_ACTIVE_LOW低,ECAP_APWM_ACTIVE_HIGH高*/
    ECAP_setAPWMPolarity(ECAP2_BASE,ECAP_APWM_ACTIVE_LOW);
    /*ECAP2的APWM相位偏移配置:3k/10k=30%*/
    ECAP_setPhaseShiftCount(ECAP2_BASE,3000U);
    /*ECAP2的APWM的相位偏移装载使能*/
    ECAP_enableLoadCounter(ECAP2_BASE);
    /*ECAP2的APWM的同步配置:ECAP_SYNC_OUT_SYNCI计数=0同步输出,
     * ECAP_SYNC_OUT_COUNTER_PRD计数=周期值同步输出,ECAP_SYNC_OUT_DISABLED关闭同步输出*/
    ECAP_setSyncOutMode(ECAP2_BASE,ECAP_SYNC_OUT_SYNCI);
    /*ECAP2的APWM的仿真模式停止*/
    ECAP_setEmulationMode(ECAP2_BASE,ECAP_EMULATION_STOP);
    /*ECAP2的APWM的同步脉冲源配置：ECAP_SYNC_IN_PULSE_SRC_DISABLE关闭*/
    ECAP_setSyncInPulseSource(ECAP2_BASE,ECAP_SYNC_IN_PULSE_SRC_SYNCOUT_ECAP1);

    /*ECAP1启动时间戳计数*/
    ECAP_startCounter(ECAP1_BASE);
    /*ECAP2启动时间戳计数*/
    ECAP_startCounter(ECAP2_BASE);
}

void OUTPUTXBAR_config(){

    /*交叉开关输出OUTPUTXBAR3启动模式关闭*/
    XBAR_setOutputLatchMode(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);
    /*交叉开关输出OUTPUTXBAR3输出信号反转关闭*/
    XBAR_invertOutputSignal(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);

    /*交叉开关输出OUTPUTXBAR3的位置配置：XBAR_OUT_MUX00_ECAP1_OUT输出到ECAP*/
    XBAR_setOutputMuxConfig(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_OUT_MUX00_ECAP1_OUT);
    /*交叉开关输出OUTPUTXBAR3使能XBAR_MUX00使能*/
    XBAR_enableOutputMux(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_MUX00);

    /*交叉开关输出OUTPUTXBAR4启动模式关闭*/
    XBAR_setOutputLatchMode(OUTPUTXBAR_BASE, XBAR_OUTPUT4, false);
    /*交叉开关输出OUTPUTXBAR4输出信号反转关闭*/
    XBAR_invertOutputSignal(OUTPUTXBAR_BASE, XBAR_OUTPUT4, false);

    /*交叉开关输出OUTPUTXBAR4的位置配置：XBAR_OUT_MUX00_ECAP1_OUT输出到ECAP*/
    XBAR_setOutputMuxConfig(OUTPUTXBAR_BASE, XBAR_OUTPUT4, XBAR_OUT_MUX02_ECAP2_OUT);
    /*交叉开关输出OUTPUTXBAR4使能XBAR_MUX00使能*/
    XBAR_enableOutputMux(OUTPUTXBAR_BASE, XBAR_OUTPUT4, XBAR_MUX02);
}
