/*
 * system.h
 *
 *  Created on: 2023年2月2日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

	/*配置GPIO0为EPWM1A,GPIO1为EPWM1B*/
void epwm_gpio(void);
	/*同步策略*/
void sync_config(void);
	/*EPWM配置*/
void epwm_config(void);

__interrupt void epwm1ISR(void);

#endif /* SYSTEM_H_ */
