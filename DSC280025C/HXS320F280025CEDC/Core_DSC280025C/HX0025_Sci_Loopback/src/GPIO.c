/*
 * GPIO.c
 *
 *  Created on: 2023��1��16��
 *      Author: Administrator
 */
#include "device.h"

void setup1GPIO(void){

	GPIO_setPinConfig(GPIO_31_GPIO31);
	GPIO_setPadConfig(31, GPIO_PIN_TYPE_PULLUP);
	GPIO_setDirectionMode(31,GPIO_DIR_MODE_OUT );

	GPIO_setPinConfig(GPIO_34_GPIO34);
	GPIO_setPadConfig(34, GPIO_PIN_TYPE_PULLUP);
	GPIO_setDirectionMode(34,GPIO_DIR_MODE_OUT );

	GPIO_writePin(31, 1);
	GPIO_writePin(34,1);

}
