/******************************************************************
 文 档 名：    HX_DSC280025C_EDB_ADC_Workaround
 开 发 环 境：Haawking IDE V2.1.6
 开 发 板 ：   Haawking IDE V2.2.5Pre
 D S P：        DSC280025C
 使 用 库：    无
 作     用：    0025C的EDB版本，ADC首次采样不准，需要舍弃
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：ADC Timer 0触发需要采样的SOC转换
 	 	   ADC Timer 2触发SOC15转换，作为Background SOC，保证ADC不会停下来。

 连接方式：A0和A1作为2个ADC输入通道，接被测信号（范围0~3.3V）
 	 	 	 	 VREFHI接外部基准源，如果使用0025C核心板，可以将VREFHI接到3V3电源


 版 本：      V1.0.0
 时 间：      2024年4月1日
 作 者：      liyuyao
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"


#define myADCCH0 ADC_CH_ADCIN0
#define myADCCH1 ADC_CH_ADCIN1

uint32_t ISRCounter = 0;
uint16_t adcadata[16];

__interrupt void adca1ISR(void);

void configCPUTimer(uint32_t, float, float);
void Adc_Dummy_SOC_Config()
{
	//An extra Timer is needed to trigger extra ADC SOCs

	// timer2
	CPUTimer_setPeriod(CPUTIMER2_BASE, 0xFFFFFFFF);
	CPUTimer_setPreScaler(CPUTIMER2_BASE, 0);
	CPUTimer_stopTimer(CPUTIMER2_BASE);
	CPUTimer_reloadTimerCounter(CPUTIMER2_BASE);
	configCPUTimer(CPUTIMER2_BASE, DEVICE_SYSCLK_FREQ, 32);  // 0.2us
	CPUTimer_enableInterrupt(CPUTIMER2_BASE);

	//An extra ADC SOC is used and the result can be ignored
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER15, ADC_TRIGGER_CPU1_TINT0, ADC_CH_ADCIN13, 10U);
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER5, ADC_INT_SOC_TRIGGER_NONE);


	ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER15, ADC_TRIGGER_CPU1_TINT0, ADC_CH_ADCIN13, 10U);
	ADC_setInterruptSOCTrigger(ADCC_BASE, ADC_SOC_NUMBER5, ADC_INT_SOC_TRIGGER_NONE);


}
int main(void)
{
    Device_init();
    Device_initGPIO();

	GPIO_setPinConfig(GPIO_231_GPIO231);  // Analog PinMux for A0/C15
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);  // AIO -> Analog mode selected

	GPIO_setPinConfig(GPIO_232_GPIO232);  // Analog PinMux for A1
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);  // AIO -> Analog mode selected

	Interrupt_initModule();
	Interrupt_initVectorTable();
	Interrupt_register(INT_ADCA1, &adca1ISR);

	// timer0
	CPUTimer_setPeriod(CPUTIMER0_BASE, 0xFFFFFFFF);
	CPUTimer_setPreScaler(CPUTIMER0_BASE, 0);
	CPUTimer_stopTimer(CPUTIMER0_BASE);
	CPUTimer_reloadTimerCounter(CPUTIMER0_BASE);
	configCPUTimer(CPUTIMER0_BASE, DEVICE_SYSCLK_FREQ, 100);  // 100us
	CPUTimer_enableInterrupt(CPUTIMER0_BASE);


	// Analog Subsystem
	ASysCtl_disableTemperatureSensor();

	HWREG(ADCA_BASE + ADC_O_CTL1) |= 0x8000U;	//Disable PGA in ADC Module
//	ASysCtl_setAnalogReferenceInternal( ASYSCTL_VREFHIA |  ASYSCTL_VREFHIC);
	 EALLOW;
	 HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) = 0x1;		//External Reference, VREFHI should be connected to external source
	 EDIS;

	DEVICE_DELAY_US(10000);

    // Set main clock scaling factor (53MHz max clock for the ADC module)
    ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_4_0);

    // set the ADC interrupt pulse generation to end of conversion
    ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);

    // set priority of SOCs
    ADC_setSOCPriority(ADCA_BASE, ADC_PRI_ALL_ROUND_ROBIN);


    // enable the ADCs
    ADC_enableConverter(ADCA_BASE);
    ADC_enableConverter(ADCA_BASE);

    // delay to allow ADCs to power up
    SysCtl_delay(10000U);

    //-------------------------------------------------------------------------

	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_CPU1_TINT0, myADCCH0, 10U);
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER0, ADC_INT_SOC_TRIGGER_NONE);

	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1, ADC_TRIGGER_CPU1_TINT0, myADCCH1, 10U);
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER1, ADC_INT_SOC_TRIGGER_NONE);


	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER5);
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);

	// ADC Workaround configuration
	Adc_Dummy_SOC_Config();

	Interrupt_enable(INT_ADCA1);

    EINT;
    ERTM;

	// Fire up CPU timer0
	CPUTimer_startTimer(CPUTIMER0_BASE);

    while(1);
    return 0;
}
__interrupt void adca1ISR(void)
{
	ISRCounter++;

	adcadata[0] = HWREG(ADCARESULT_BASE + ADC_O_RESULT0);
	adcadata[1] = HWREG(ADCARESULT_BASE + ADC_O_RESULT1);


	ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // Acknowledge this interrupt to receive more interrupts from group 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);

}

void configCPUTimer(uint32_t cpuTimer, float freq, float period)
{
    uint32_t temp;

    //
    // Initialize timer period:
    //
    temp = (uint32_t)((freq / 1000000) * period);
    CPUTimer_setPeriod(cpuTimer, temp);

    //
    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    //
    CPUTimer_setPreScaler(cpuTimer, 0);

    //
    // Initializes timer control register. The timer is stopped, reloaded,
    // free run disabled, and interrupt enabled.
    // Additionally, the free and soft bits are set
    //
    CPUTimer_stopTimer(cpuTimer);
    CPUTimer_reloadTimerCounter(cpuTimer);
    CPUTimer_setEmulationMode(cpuTimer,
                              CPUTIMER_EMULATIONMODE_STOPAFTERNEXTDECREMENT);
    CPUTimer_enableInterrupt(cpuTimer);

}
//
// End of File
//

