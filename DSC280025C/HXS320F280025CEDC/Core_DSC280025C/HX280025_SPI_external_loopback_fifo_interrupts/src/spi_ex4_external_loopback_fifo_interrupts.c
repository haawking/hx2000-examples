/******************************************************************
 文 档 名：       HX_DSC280025_SPI_external_loopback_fifo_interrupts
 开 发 环 境：  Haawking IDE V2.2.10Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：     SPI外环FIFO通讯
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
（1）SPIA作为从机，SPIB为主机
（2）从SPIB主机向SPIA从机发送数据，SPIA从机接收数据

 连接方式：
 SPIB   SPIA
GPIO40-GPIO8  - SPISIMO
GPIO41-GPIO10 - SPISOMI
GPIO22-GPIO9  - SPICLK
GPIO23-GPIO11 - SPISTE

现象：
（1）SPIB主机接收到的数据与SPIA从机发送的数据一致，则GPIO34/LED2灯亮
（2）不一致，则GPIO31/LED1灯亮
 *
 版 本：      V1.0.0
 时 间：      2023年2月24日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//

#include "system.h"

//
// Globals
//
volatile uint16_t sData[2];                  // Send data buffer
volatile uint16_t rData[2];                  // Receive data buffer
volatile uint16_t rDataPoint = 0;            // To keep track of where we are in the
                                    // data stream to check received data

//
// Main
//
void main(void)
{
    uint16_t i;

    /*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();
    /*配置GPIO31/34为IO输出，以指示数据传输状态*/
    setup1GPIO();
    /*关中断,清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口地址INT_SPIB_TX,指向执行spibTxFIFOISR中断服务程序*/
    Interrupt_register(INT_SPIB_TX, &spibTxFIFOISR);
    /*中断入口地址INT_SPIA_RX,指向执行spiaRxFIFOISR中断服务程序*/
    Interrupt_register(INT_SPIA_RX, &spiaRxFIFOISR);

    EALLOW;
    /*SPIA/B的GPIO引脚配置*/
    spia_gpio();
    spib_gpio();
    /*SPIA/B的从机/主机配置*/
    SPIA_slave_init();
    SPIB_master_init();
    EDIS;

    /*初始化配置数据缓冲区*/
    for(i = 0; i < 2; i++)
    {
        sData[i] = i;
        rData[i]= 0;
    }

    /*中断使能INT_SPIA_RX*/
    Interrupt_enable(INT_SPIA_RX);
    /*中断使能INT_SPIB_TX*/
    Interrupt_enable(INT_SPIB_TX);

    /*使能打开全局中断*/
    EINT;
    ERTM;

    while(1)
    {
        ;
    }
}

