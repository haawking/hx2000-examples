/******************************************************************
 文 档 名：       HX_DSC280025_EPWM_DC
 开 发 环 境：  Haawking IDE V2.2.5Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM DC数字比较错误联防
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，
 （1）通过EPWM1A/B输出10kHz，脉宽50%互补的PWM波形
 （2）采用EPWM_DC数字比较错误错误联防触发
        通过错误联防引脚GPIO25-TZ1，通过交叉开关XBAR输入低电平信号，
 使EPWM1A/B置低，从而实现EPWM封波

 外部连接：EPWM1A/B连接示波器 GPIO25初始时不接或接3.3V
 输出PWM正常后，GPIO25接地，察看EPWM1A/B置低封波
 GPIO25拔掉后 EPWM1A/B恢复正常输出

 版 本：      V1.0.0
 时 间：      2023年2月9日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "system.h"


void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定*/
    Device_initGPIO();

	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*中断入口地址INT_EPWM1_TZ,指向执行epwm1TZISR中断服务程序*/
    Interrupt_register(INT_EPWM1_TZ, &epwm1TZISR);

	/*屏蔽TBCLK时基同步，便于PWM初始化配置写入*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EALLOW;
    /*EPWM的GPIO引脚配置*/
    epwm_gpio();
    /*EPWM的错误联防TZ引脚配置*/
    epwm_tz_gpio();
    /*同步策略*/
    sync_init();
    /*EPWM配置*/
    epwm_config();
    EDIS;

	/*使能TBCLK时基同步，以使PWM配置写入，并实现多PWM同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

	/*中断使能INT_EPWM1_TZ*/
    Interrupt_enable(INT_EPWM1_TZ);

	/*打开全局中断*/
    EINT;
    ERTM;

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}




