/*
 * system.h
 *
 *  Created on: 2023��2��24��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
// Qualification period at 6 samples in microseconds
#define DELAY   (6.0 * 510.0 * 1000000.0 * (1.0 / DEVICE_SYSCLK_FREQ))

//
// Globals
//
extern volatile uint32_t xint1Count;
extern volatile uint32_t xint2Count;
extern uint32_t loopCount;

//
// Function Prototypes
//
__interrupt void xint1ISR(void);
__interrupt void xint2ISR(void);



#endif /* SYSTEM_H_ */
