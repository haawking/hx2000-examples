/******************************************************************************************
 文 档 名：       HX_DSC280025_BGCRC_ex2_cpuBGCRC_basic
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：           DSC280025
 使 用 库：
 作     用：       BGCRC CPU中断
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------
 BGCRC 中断 使用看门狗

    这个例子演示了如何从CPU配置和触发BGCRC。
 它还展示了如何配置CRC看门狗和在配置模块后锁定寄存器。
 看门狗用于诊断，以检查内存测试是否在预期预定的时间窗口内完成。
 如果测试没有在预定的时间窗口内完成，则会生成一个错误信号。

该模块配置为1kB的GS0 RAM，该RAM用随机数据编程。
 用软件方法计算了Golden_CRC比较值。
 一旦计算完成，就会生成中断，并检查是否引发错误标志。
 NMI被启用，检测到错误时被触发。

外部连接：
无

观测变量：
  - pass
  - bgcrcDone

--------------------------------------------------------------------------------------------
版 本：V1.0.0
 时 间：2023年1月1日
 作 者：
 @ mail：support@mail.haawking.com
******************************************************************************************/
//
// Included Files
//

#include <syscalls.h>
#include "IQmathLib.h"

#include "driverlib.h"
#include "device.h"

//
// Defines
//
//#define DATA_SIZE 256 // Total size  = 256 * 4 = 1kB
#define DATA_SIZE 64 // Total size  = 64 * 4 = 256B
#define CRC_FAIL  0   // Set this as 1 to make the CRC check fail
//
// Globals
//
uint32_t POLYNOMIAL = (uint32_t)0x04C11DB7;
uint32_t crc32;
uint32_t seed;
uint32_t byteSwappedData;

volatile bool bgcrcDone = false;
volatile bool bgcrcError = false;
volatile uint32_t BgcrcNmiStatus = 0;
volatile uint32_t intStatus =0;
volatile bool Bgcrc_fail = false;
volatile bool Bgcrc_uncorr_err = false;
volatile bool Bgcrc_corr_err = false;
volatile bool Bgcrc_Wd_underflow = false;
volatile bool Bgcrc_Wd_overflow = false;
volatile bool error_status_pin_diagnostic_failed = false;

volatile bool pass = false;

//#pragma DATA_SECTION(data,"ramgs0")
//#pragma DATA_ALIGN(data, 0x80)
//volatile uint32_t CODE_SECTION("ramgs0") data[DATA_SIZE] = {0};
volatile CODE_SECTION(".text") __attribute__ ((aligned (0x80))) uint32_t data[DATA_SIZE] = {0};

//
// 函数自定义
//
void dataInit(void);
void bgcrcInit(void);
void delay(uint32_t i);
__interrupt void CPUbgcrcIsr(void);
__interrupt void NMIbgcrcIsr(void);

//
// 主函数
//
void main(void)
{
	/*初始化系统时钟和外设*/
    Device_init();

    /*GPIO端口配置锁定关闭*/
    Device_initGPIO();

    /*关闭全局中断*/
    DINT;

    /*初始化PIE并清除PIE中断，关闭CPU中断*/
    Interrupt_initModule();

    /*用指向中断服务例程（ISR）初始化PIE向量表*/
    Interrupt_initVectorTable();
    IER = 0x0000;
    IFR = 0x0000;


    /*BGCRC 中断和 NMI的ISR映射*/
    Interrupt_register(INT_BGCRC, CPUbgcrcIsr);
    Interrupt_register(INT_NMI, NMIbgcrcIsr);

     /*使能BGCRC中断*/
    Interrupt_enable(INT_BGCRC);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

    /*使能NMI*/
    Interrupt_enable(INT_NMI);
    SysCtl_enableNMIGlobalInterrupt();
    SysCtl_setNMIWatchdogPeriod(0xFFFFU);

    /*使能全局中断和实时中断*/
    EINT;
    ERTM;

    //
    // Initialize the data with random numbers
    //
    //write_reg32(MB_ADDR, 0x0000);
    dataInit();
    //write_reg32(MB_ADDR, 0x1111);

    /*初始化BGCRC模式*/
    bgcrcInit();
    //write_reg32(MB_ADDR, 0x2222);
    delay(2000); // Expecting the BGCRC completion in this time
    //write_reg32(MB_ADDR, 0x3333);

    if(error_status_pin_diagnostic_failed == true )
    {
        //
        // Something is wrong with the error status pin
        // Need to reset the system
        //
    }

    if((bgcrcDone == true) && (bgcrcError == false))
    {
        if((intStatus == BGCRC_TEST_DONE))
        {
            pass = true;
            //write_reg32(MB_ADDR, 0xAAAAAAAA);
        }
    }

    //write_reg32(MB_ADDR, 0xdeadbeef);
    while(1);
}

//
// Fill the buffer with random data and compute CRC using SW method
//
void dataInit()
{
    uint32_t tseed;
    uint32_t i,j;

    //
    // Initialize the random number generator
    //
    tseed = 34;  //time(NULL);
    srand(tseed);
    srand(rand());

    seed  =  rand();
    seed = (uint32_t)(seed <<16) | rand();       // Initialize with seed
    crc32 = seed;

    //
    // Initialize the Data_Buffer with 32 bit random data
    //
    for(i = 0; i < DATA_SIZE; i++)
    {
        data[i] = rand();
        data[i] = (data[i] << 16) | rand();
        write_reg32(0x20000+i*4,data[i]);
    }

    //
    // Compute the Software CRC for validation
    //
    for(i = 0; i < DATA_SIZE; i++)
    {
        byteSwappedData = (((data[i] & (uint32_t)0x000000FFU) << 24) |
                           ((data[i] & (uint32_t)0x0000FF00U) <<  8) |
                           ((data[i] & (uint32_t)0x00FF0000U) >>  8) |
                           ((data[i] & (uint32_t)0xFF000000U) >> 24)
                          );

        crc32 = byteSwappedData ^ crc32;

        for(j = 0; j < 32; j++)
        {
            if(crc32 & (uint32_t)0x80000000U)
            {
                crc32 = ( (crc32 << 1) ^ POLYNOMIAL);
            }
            else
            {
                crc32 = (crc32 << 1);
            }
            crc32 = crc32 & (uint32_t)0xFFFFFFFFU;
        }
        //write_reg32(MB_ADDR,data[i]);
        //write_reg32(MB_ADDR,crc32);
    }

#if CRC_FAIL == 1 // To make the BGCRC to fail
    crc32 = 0xdeadbeef; //This is to test the error NMIs and Pins
#endif
}

//
// Init routine for BGCRC module
//
void bgcrcInit()
{
    //
    // Unlock the BGCRC configuration in case it is locked CFG-1
    //
    BGCRC_unlockRegister(BGCRC_CPU_BASE, BGCRC_REG_CTRL1  |
                                         BGCRC_REG_WD_CFG |
                                         BGCRC_REG_INTEN  |
                                         BGCRC_REG_SEED);

    //
    // Unlock the BGCRC configuration in case it is locked CFG-2
    //
    BGCRC_unlockRegister(BGCRC_CPU_BASE, BGCRC_REG_EN         |
                                         BGCRC_REG_CTRL2      |
                                         BGCRC_REG_START_ADDR |
                                         BGCRC_REG_GOLDEN     |
                                         BGCRC_REG_WD_MIN     |
                                         BGCRC_REG_WD_MAX);
    //
    // Enable NMI and set the emulation mode as soft.
    // The CRC module and the watchdog stops immediately on debug suspend
    //
    BGCRC_setConfig(BGCRC_CPU_BASE, BGCRC_NMI_ENABLE, BGCRC_EMUCTRL_SOFT);

    //
    // Configure the CRC Watchdog
    // Watchdog window - Min : DATA_SIZE, Max : DATA_SIZE + 1
    //
    BGCRC_enableWatchdog(BGCRC_CPU_BASE);
    BGCRC_setWatchdogWindow(BGCRC_CPU_BASE, DATA_SIZE, DATA_SIZE + 1);

    //
    // Configure the region
    //256字节
 //   BGCRC_setRegion(BGCRC_CPU_BASE, (uint32_t)data,
 //                   BGCRC_SIZE_KBYTES(1), BGCRC_CRC_MODE);

    BGCRC_setRegion(BGCRC_CPU_BASE, (uint32_t)0x20000,
    		          BGCRC_SIZE_BYTES_256, BGCRC_CRC_MODE);

    //
    // Set the seed value same as used in SW computation
    //
    BGCRC_setSeedValue(BGCRC_CPU_BASE, seed);

    //
    // Initialize the golden value with previously computed CRC using software
    //
    BGCRC_setGoldenCRCValue(BGCRC_CPU_BASE, crc32);

    //
    // Enable interrupts
    // No need to enable the NMIs as all the NMIs are enabled by default
    //
    BGCRC_enableInterrupt(BGCRC_CPU_BASE, BGCRC_TEST_DONE    |
                                          BGCRC_CRC_FAIL     |
                                          BGCRC_UNCORR_ERR   |
                                          BGCRC_CORR_ERR     |
                                          BGCRC_WD_UNDERFLOW |
                                          BGCRC_WD_OVERFLOW);

    //
    // Lock Register configuration for CFG1
    //
    BGCRC_lockRegister(BGCRC_CPU_BASE, BGCRC_REG_CTRL1  |
                                       BGCRC_REG_WD_CFG |
                                       BGCRC_REG_INTEN  |
                                       BGCRC_REG_SEED);


    //
    // Commit Register Lock for CFG1
    //
    BGCRC_commitRegisterLock(BGCRC_CPU_BASE, BGCRC_REG_CTRL1  |
                                             BGCRC_REG_WD_CFG |
                                             BGCRC_REG_INTEN |
                                             BGCRC_REG_SEED);

    BGCRC_start(BGCRC_CPU_BASE);

    //
    // Lock Register configuration for CFG2
    //
    BGCRC_lockRegister(BGCRC_CPU_BASE, BGCRC_REG_EN         |
                                       BGCRC_REG_CTRL2      |
                                       BGCRC_REG_START_ADDR |
                                       BGCRC_REG_GOLDEN     |
                                       BGCRC_REG_WD_MIN     |
                                       BGCRC_REG_WD_MAX);
}

//
// CPUbgcrcIsr - DONE interrupt from BGCRC.
//
__interrupt void CPUbgcrcIsr(void)
{
    bgcrcDone = true;
    //write_reg32(MB_ADDR,0x4444);
    //
    // Read the interrupt status
    //
    intStatus= BGCRC_getInterruptStatus(BGCRC_CPU_BASE);
    intStatus &= ~BGCRC_GLOBAL_INT;

    //
    // Clear interrupt status
    //
    BGCRC_clearInterruptStatus(BGCRC_CPU_BASE, BGCRC_GLOBAL_INT |
                                               BGCRC_TEST_DONE  |
                                               BGCRC_ALL_ERROR_FLAGS);

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}

//
// NMIbgcrcIsr - NMI ISR
//
__interrupt void NMIbgcrcIsr(void)
{
    uint32_t timeout = 20;
    BgcrcNmiStatus = BGCRC_getNMIStatus(BGCRC_CPU_BASE);

    //
    // Safety diagnostic check
    //
    if(!SysCtl_isErrorTriggered())
    {
        //
        // If due to some fault the ERROR STS couldn't be triggered, force
        // the error status pin so that external host get to know the error
        //
        while((SysCtl_getErrorPinStatus() == 0) && (timeout > 0))
        {
            SysCtl_forceError();
            timeout--;
        }

        if (timeout == 0)
        {
            //
            // Something is wrong with the error status pin
            //
            error_status_pin_diagnostic_failed = true;
        }
    }

    if((BgcrcNmiStatus & BGCRC_CRC_FAIL) == BGCRC_CRC_FAIL)
    {
        //
        // CRC Fail NMI
        //
        Bgcrc_fail = true;
        BGCRC_clearNMIStatus(BGCRC_CPU_BASE, BGCRC_CRC_FAIL);
    }

    if( (BgcrcNmiStatus&BGCRC_UNCORR_ERR) == BGCRC_UNCORR_ERR )
    {
         //
         // 无法改正NMI错误
         Bgcrc_uncorr_err = true;
         BGCRC_clearNMIStatus(BGCRC_CPU_BASE, BGCRC_UNCORR_ERR);
    }

    if(BgcrcNmiStatus&BGCRC_CORR_ERR == BGCRC_CORR_ERR)
    {
          //
          // 可改正NMI错误
          //
          Bgcrc_corr_err = true;
          BGCRC_clearNMIStatus(BGCRC_CPU_BASE, BGCRC_CORR_ERR);
    }

    if(BgcrcNmiStatus&BGCRC_WD_UNDERFLOW == BGCRC_WD_UNDERFLOW)
    {
          //
          // Watchdog Underflow Error NMI
          //
          Bgcrc_Wd_underflow = true;
          BGCRC_clearNMIStatus(BGCRC_CPU_BASE, BGCRC_WD_UNDERFLOW);
    }

    if(BgcrcNmiStatus&BGCRC_WD_OVERFLOW == BGCRC_WD_OVERFLOW)
    {
          //
          // Watchdog Overflow Error NMI
          //
          Bgcrc_Wd_overflow = true;
          BGCRC_clearNMIStatus(BGCRC_CPU_BASE, BGCRC_WD_OVERFLOW);
    }

    SysCtl_clearAllNMIFlags();
    bgcrcError = true;
}

/*延时功能*/
void delay(uint32_t i)
{
    uint32_t j =i;
    while(j > 0)
    {
        j--;
    }
}
