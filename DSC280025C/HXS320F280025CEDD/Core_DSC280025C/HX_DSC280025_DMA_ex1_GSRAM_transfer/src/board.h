#ifndef BOARD_H
#define BOARD_H

//
// Included Files
//

#include "driverlib.h"
#include "device.h"


extern const void *srcAddr;
extern const void *destAddr;
#define myDMA0_BASE DMA_CH6_BASE 
#define myDMA0_BURSTSIZE 8U
#define myDMA0_TRANSFERSIZE 16U


// INT_myDMA0�ж�����
#define INT_myDMA0 INT_DMA_CH6
#define INT_myDMA0_INTERRUPT_ACK_GROUP INTERRUPT_ACK_GROUP7
extern __interrupt void INT_myDMA0_ISR(void);


void	Board_init();
void	DMA_init();
void	INTERRUPT_init();
void	PinMux_init();

#endif
