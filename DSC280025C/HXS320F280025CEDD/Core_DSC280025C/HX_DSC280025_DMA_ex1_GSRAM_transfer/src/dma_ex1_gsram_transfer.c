/******************************************************************************************
 文 档 名：       HX_DSC280025_DMA_GSRAM_transfer
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：           DSC280025
 使 用 库：
 作     用：       DMA GSRAM Transfer
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------

本例使用一个DMA通道将数据从RAMGS0中的缓冲区传输到RAMGS0中的缓冲区。
 该示例重复设置DMA通道PERINTFRC位，直到完成16个burst突发传输(其中每个突发是8个16位字)。
 当整个传输完成时，它将触发DMA中断。

观测变量：
  - \b sData - 数据发送
  - \b rData - 数据接收

版 本：V1.0.0
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
******************************************************************************************/
#include <syscalls.h>
#include "IQmathLib.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"

//
// DMA data section
//
//#pragma DATA_SECTION(sData, "ramgs0");  // map the TX data to memory
//#pragma DATA_SECTION(rData, "ramgs0");  // map the RX data to memory

//
// define
//
#define BURST       8       // write 8 to the register for a burst size of 8
#define TRANSFER    16      // [(MEM_BUFFER_SIZE/(BURST)]

//
// Globals
//
uint16_t CODE_SECTION("ramgs0") sData[128];   // Send data buffer
uint16_t CODE_SECTION("ramgs0") rData[128];   // Receive data buffer
volatile uint16_t done;
int32_t x;
//
// 函数自定义
//
__interrupt void dmaCh6ISR(void);
void initDMA(void);
void error();

void error();
__interrupt void INT_myDMA0_ISR(void);

const void *destAddr = (const void *)rData;
const void *srcAddr = (const void *)sData;


int main(void)
{
    uint16_t i;

    //
    // 初始化时钟和外设
    //
    Device_init();

    Board_init();


    //
    // 用户特殊代码，使能中断
    // 初始化数据缓冲器
    //
    for(i = 0; i < 128; i++)
    {
        sData[i] = i;
        rData[i] = 0;
    }

    //
    // 使能中断请求
    //
    Interrupt_enable(INT_myDMA0);
    EINT;
    // 打开 DMA 通道
    DMA_startChannel(myDMA0_BASE);

    done = 0;           // Test is not done yet

    while(!done)        // 等待直到 DMA 传输完成
    {
       DMA_forceTrigger(myDMA0_BASE);
       for(i=0;i<255;i++)
       {
       asm("  NOP");
       }
    }

    //
    // When the DMA transfer is complete the program will stop here
    //
    ESTOP0;

    return 0;
}

//
// error - Error Function which will 停止调试器
//
void error(void)
{
    ESTOP0;  //Test failed!! Stop!
    for (;;);
}

//
// local_D_INTCH6_ISR - DMA Channel6 ISR
//
__interrupt void INT_myDMA0_ISR(void)
{
    uint16_t i;

    DMA_stopChannel(myDMA0_BASE);

    EALLOW;
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
    EDIS;

    for( i = 0; i < 128; i++ )
    {
        //
        // 检验数据完整性
        //
        if (rData[i] != i)
        {
            error();
        }

    }

    done = 1; // Test done.
    return;
}
