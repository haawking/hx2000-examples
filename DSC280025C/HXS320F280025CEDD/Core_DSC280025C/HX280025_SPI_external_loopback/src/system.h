/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

    /*SPIA/B的GPIO引脚配置*/
void spia_gpio(void);
void spib_gpio(void);
    /*SPIA/B的从机/主机配置*/
void SPIA_slave_init(void);
void SPIB_master_init(void);

void setup1GPIO(void);

#endif /* SYSTEM_H_ */
