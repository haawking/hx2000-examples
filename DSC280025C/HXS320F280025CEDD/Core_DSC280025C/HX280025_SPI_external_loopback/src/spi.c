#include "system.h"

void spia_gpio(void)
{
	/*配置GPIO8为SPIA_SIMO-从机输入主机输出*/
	GPIO_setPinConfig(GPIO_8_SPIA_SIMO);
	GPIO_setPadConfig(8, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(8, GPIO_QUAL_ASYNC);
	/*配置GPIO10为SPIA_SOMI-从机输出主机输入*/
	GPIO_setPinConfig(GPIO_10_SPIA_SOMI);
	GPIO_setPadConfig(10, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(10, GPIO_QUAL_ASYNC);
	/*配置GPIO9为SPIA_CLK-时钟输入*/
	GPIO_setPinConfig(GPIO_9_SPIA_CLK);
	GPIO_setPadConfig(9, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(9, GPIO_QUAL_ASYNC);
	/*配置GPIO11为SPIA_STE-片选输入*/
	GPIO_setPinConfig(GPIO_11_SPIA_STE);
	GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(11, GPIO_QUAL_ASYNC);
}

void spib_gpio(void)
{
	/*配置GPIO40为SPIB_SIMO-从机输入主机输出*/
	GPIO_setPinConfig(GPIO_40_SPIB_SIMO);
	GPIO_setPadConfig(40, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(40, GPIO_QUAL_ASYNC);
	/*配置GPIO41为SPIB_SOMI-从机输出主机输入*/
	GPIO_setPinConfig(GPIO_41_SPIB_SOMI);
	GPIO_setPadConfig(41, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(41, GPIO_QUAL_ASYNC);
	/*配置GPIO22为SPIB_CLK-时钟输入*/
	GPIO_setPinConfig(GPIO_22_SPIB_CLK);
	GPIO_setPadConfig(22, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(22, GPIO_QUAL_ASYNC);
	/*配置GPIO23为SPIB_STE-片选输入*/
	GPIO_setPinConfig(GPIO_23_SPIB_STE);
	GPIO_setPadConfig(23, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(23, GPIO_QUAL_ASYNC);
}

void SPIA_slave_init(){
	/*复位SPI模块，以配置SPI*/
	SPI_disableModule(SPIA_BASE);
	/*SPI模块配置：时钟分频=DEVICE_LSPCLK_FREQ-SYSCLK/4=40M
	 * SPI_PROT_POL0PHA0-极性上升沿无延迟
	 * SPI_MODE_SLAVE-从机模式
	 * 波特率=500kbps,数据长度=16位*/
	SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ,
			SPI_PROT_POL0PHA0,SPI_MODE_SLAVE, 500000, 16);
	/*屏蔽SPI_FIFO功能*/
	SPI_disableFIFO(SPIA_BASE);
	/*屏蔽SPI内环功能*/
	SPI_disableLoopback(SPIA_BASE);
	/*SPI仿真模式配置为：
	 * SPI_EMULATION_FREE_RUN-SPI持续运行*/
	SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_FREE_RUN);
	/*释放SPI模块复位，以完成SPI配置*/
	SPI_enableModule(SPIA_BASE);
}

void SPIB_master_init(){
	/*复位SPI模块，以配置SPI*/
	SPI_disableModule(SPIB_BASE);
	/*SPI模块配置：时钟分频=DEVICE_LSPCLK_FREQ-SYSCLK/4=40M
	 * SPI_PROT_POL0PHA0-极性上升沿无延迟
	 * SPI_MODE_MASTER-主机模式
	 * 波特率=500kbps,数据长度=16位*/
	SPI_setConfig(SPIB_BASE, DEVICE_LSPCLK_FREQ,
			SPI_PROT_POL0PHA0,SPI_MODE_MASTER, 500000, 16);
	/*屏蔽SPI_FIFO功能*/
	SPI_disableFIFO(SPIB_BASE);
	/*屏蔽SPI内环功能*/
	SPI_disableLoopback(SPIB_BASE);
	/*SPI仿真模式配置为：
	 * SPI_EMULATION_FREE_RUN-SPI持续运行*/
	SPI_setEmulationMode(SPIB_BASE, SPI_EMULATION_FREE_RUN);
	/*释放SPI模块复位，以完成SPI配置*/
	SPI_enableModule(SPIB_BASE);
}
