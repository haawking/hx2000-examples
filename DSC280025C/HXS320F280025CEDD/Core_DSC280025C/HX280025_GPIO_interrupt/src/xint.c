#include "system.h"

void XINT_IOinit(void)
{
	/*配置GPIO30为IO输入，用于外部中断触发*/
	GPIO_setPinConfig(GPIO_30_GPIO30);
	GPIO_setPadConfig(30, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(30, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(30, GPIO_DIR_MODE_IN);

	/*配置GPIO31为IO输出*/
	GPIO_setPinConfig(GPIO_31_GPIO31);
	GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
}
