/******************************************************************
 文 档 名：       HX_DSC280025_GPIO_interrupt
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      GPIO按键输入，触发外部中断
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：GPIO9按键输入，下降沿触发外部中断GPIO9/XINT1，
 执行GPIO31/LED1翻转闪灯一次

现象：GPIO9/KEY按键按下一次，触发外部中断，执行GPIO31/LED1翻转闪灯一次
 *
 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"



//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();
    /*配置GPIO30为IO输入，用于触发外部中断
     * GPIO31为IO输出，用于指示外部中断触发*/
    XINT_IOinit();

    EALLOW;
	/*配置GPIO9为IO输入*/
    GPIO_setPinConfig(GPIO_9_GPIO9);
    GPIO_setDirectionMode(9, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(9, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(9, GPIO_QUAL_SYNC);
    EDIS;

    /*配置XINT1为下降沿触发外部中断*/
    GPIO_setInterruptType(GPIO_INT_XINT1, GPIO_INT_TYPE_FALLING_EDGE);
    /*配置GPIO9为XINT1为外部中断触发引脚*/
    GPIO_setInterruptPin(9, GPIO_INT_XINT1);
    /*使能外部中断XINT1CR*/
    GPIO_enableInterrupt(GPIO_INT_XINT1);

    /*中断入口地址INT_XIN1,指向执行gpioInterruptHandler中断服务程序*/
    Interrupt_register(INT_XINT1, &gpioInterruptHandler);

    /*中断XINT1对应PIE中断使能*/
    Interrupt_enable(INT_XINT1);

    /*中断XINT1对应CPU中断使能*/
    Interrupt_enableMaster();

    for(;;)
    {

    }
}

__interrupt void gpioInterruptHandler(void)
{
	GPIO_togglePin(31);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}


//
// End of File
//

