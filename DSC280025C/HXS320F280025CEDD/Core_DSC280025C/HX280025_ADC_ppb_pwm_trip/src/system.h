/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//

// Uncomment to enable profiling
//#define ENABLE_PROFILING
#define RESULTS_BUFFER_SIZE     512U

//
// Globals
//
extern volatile uint16_t indexA;                // Index into result buffer
extern volatile uint16_t bufferFull;                // Flag to indicate buffer is full
extern uint16_t adcA2Results[RESULTS_BUFFER_SIZE];  // ADC result buffer

//
// Functional Prototypes
//

//
// ADC configuration related APIs
//
void configureADC(uint32_t adcBase);
/*配置ADC_SOC*/
void configureADCSOC(uint32_t adcBase, uint16_t channel, uint32_t acqps);
/*配置ADC_SOC_PPB后处理模块*/
void configureLimitDetectionPPB(uint16_t soc, uint16_t limitHigh,
                                uint16_t limitLow);

void initEPWMGPIO(void);
/*单次触发OSHT事件配置*/
void configureOSHTripSignal(uint32_t epwmBase);
/*周期触发CBC事件配置*/
void configureCBCTripSignal(uint32_t epwmBase);
/*直接触发事件配置*/
void configureDirectTripSignal(uint32_t epwmBase);


#ifdef ENABLE_PROFILING
void setupProfileGpio(void);
#endif

//
// ISRs
//
__interrupt void adcA1ISR(void);

#endif /* SYSTEM_H_ */
