#include "system.h"

//
// FUNCTION:    getAverageBaud
// PURPOSE:     get the average baud rate of the array
// INPUTS:      array of pulse widths (in number eCAP counts),
//              size of array, and target baud rate
// RETURNS:     average baud rate
//
uint32_t getAverageBaud(volatile float arr[], int size, float targetBaudRate)
{
    //
    // clean up variable width array to single-bit-width array
    //
    float calcTargetWidth = (float)DEVICE_SYSCLK_FREQ/targetBaudRate;
    uint16_t pass = arrTo1PulseWidth(arr, size, calcTargetWidth);

    //
    // pass only if enough good samples provided
    //
    if(pass == 0)
    {
        return(0);
    }

    //
    // convert 2-bit width, 3-bit width, and so on to 1-bit width values by
    // dividing, and average these values. skip unrelated values
    //
    float averageBitWidth = computeAvgWidth(arr, size);

    //
    // get the rounded baud rate from the average number of clocks and the
    // sysclk frequency
    //
    return((uint32_t)(((float)DEVICE_SYSCLK_FREQ/(float)averageBitWidth)+0.5));
}

//
// FUNCTION:    arrTo1PulseWidth
// PURPOSE:     convert 2-bit and higher widths to 1-bit equivalent,
//              set "bad" values to zero
// INPUTS:      array of pulse widths in number eCAP counts, size of array,
//              and target pulse-width (in number of eCAP counts)
// RETURNS:     pass if enough "good" data received,
//              fail if not enough "good" data
//
uint16_t arrTo1PulseWidth(volatile float arr[], int size, float targetWidth)
{
    int iterator = 0, numBitWidths=0;
    uint16_t goodDataCount = 0, pass = 0;
    for(iterator=0;iterator<size;iterator++)
    {

        //
        // if the item is less than 10 times the bit width,
        //
        if(arr[iterator] < targetWidth*10)
        {

            //
            // if the item is not within +/-MARGINPERCENT% of the targetWidth,
            // then it is a multiple of 1-bit
            //
            bool belowBound = arr[iterator] < targetWidth*(1.0-MARGINPERCENT);
            bool aboveBound = arr[iterator] > targetWidth*(1.0+MARGINPERCENT);
            if(belowBound || aboveBound)
            {

                //
                // estimate how many bit-widths this is
                //
                numBitWidths = (int)((arr[iterator]/targetWidth)+0.5);

                //
                // multiply the multi-bit baudrate value by the estimated
                // number of bits to make this a 1-bit baud estimate
                //
                arr[iterator] = arr[iterator]/numBitWidths;

                //
                // find if this new value is within the bounds
                //
                belowBound = arr[iterator] < targetWidth*(1.0-MARGINPERCENT);
                aboveBound = arr[iterator] > targetWidth*(1.0+MARGINPERCENT);
                if(belowBound || aboveBound)
                {
                    arr[iterator] = 0; // discard if not within margins
                }
                else
                {
                    goodDataCount++; // iterate good data counter
                }
            }
            else
            {

                //
                // this is a 1-bit value so increment the counter for it
                //
                goodDataCount++;
            }
        }
        else
        {
            arr[iterator] = 0;
        }
    }

    //
    // if at least MINSAMPLEPERCENT% of the sampled values
    // are "good" samples, then return a pass
    //
    if((float)goodDataCount/(float)size > MINSAMPLEPERCENT)
    {
        pass=1;
    }
    return(pass); //return if the array had enough good samples or not
}

//
// FUNCTION:    computeAvgWidth
// PURPOSE:     average all non-zero items in the array
// INPUTS:      array of 1-bit pulse widths in number eCAP counts,
//              and size of array
// RETURNS:     average width
//
float computeAvgWidth(volatile float arr[], int size)
{
    int iterator = 0, totSamples = 0;
    float total = 0;
    for(iterator=0;iterator<size;iterator++)
    {
        //
        // if the item has not been removed
        //
        if(arr[iterator] != 0)
        {
            //
            // iterate the number of samples that are included in the total
            //
            totSamples++;
            //
            //add it to the moving average
            //
            total+=arr[iterator];
        }
    }
    return(total/totSamples); //return the average
}
