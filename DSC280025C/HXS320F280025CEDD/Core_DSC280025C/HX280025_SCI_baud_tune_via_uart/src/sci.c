#include "system.h"

void sci_gpio(void)
{
	/*配置GPIO9为SCIA_RX*/
	GPIO_setPinConfig(GPIO_9_SCIA_RX);
	GPIO_setPadConfig(9, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(9, GPIO_QUAL_ASYNC);
	/*配置GPIO8为SCIA_TX*/
	GPIO_setPinConfig(GPIO_8_SCIA_TX);
	GPIO_setPadConfig(8, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(8, GPIO_QUAL_ASYNC);
}

uint32_t scia_config(uint32_t baud)
{
	/*清除SCI中断状态：SCI_INT_RXFF-接收FIFO|SCI_INT_TXFF-发送FIFO
	 *SCI_INT_FE-帧错误| SCI_INT_OE-溢出错误|SCI_INT_PE-奇偶校验错误
	 *SCI_INT_RXERR-接收数据错误|SCI_INT_RXRDY_BRKDT-接收准备好或停止中断
	 *SCI_INT_TXRDY-发送准备好中断*/
	SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_RXFF | SCI_INT_TXFF |
			SCI_INT_FE | SCI_INT_OE | SCI_INT_PE |
			SCI_INT_RXERR | SCI_INT_RXRDY_BRKDT | SCI_INT_TXRDY);
	/*清除SCI中断溢出状态*/
	SCI_clearOverflowStatus(SCIA_BASE);
	/*复位SCI发送FIFO*/
	SCI_resetTxFIFO(SCIA_BASE);
	/*复位SCI接收FIFO*/
	SCI_resetRxFIFO(SCIA_BASE);
	/*复位SCI通道SCIRST*/
	SCI_resetChannels(SCIA_BASE);
	/*配置SCI：DEVICE_LSPCLK_FREQ-SCI时钟=SYSCLK/4=40MHz，波特率-9600bps
	 * SCI_CONFIG_WLEN_8-数据长度8位，SCI_CONFIG_STOP_ONE-停止位1位
	 * SCI_CONFIG_PAR_NONE-无奇偶校验*/
	SCI_setConfig(SCIA_BASE, DEVICE_LSPCLK_FREQ, baud,
			(SCI_CONFIG_WLEN_8|SCI_CONFIG_STOP_ONE|SCI_CONFIG_PAR_NONE));
	/*关闭自回环*/
	SCI_disableLoopback(SCIA_BASE);
	/*SCI软复位SWRESET*/
	SCI_performSoftwareReset(SCIA_BASE);
	/*SCI配置FIFO中断队列：SCI_FIFO_TX0-发送空，SCI_FIFO_RX0-接收空*/
	SCI_setFIFOInterruptLevel(SCIA_BASE, SCI_FIFO_TX0, SCI_FIFO_RX0);
	/*SCI使能FIFO：SCIRST模块复位释放&SCI_FFTX_SCIFFENA-SCI增强FIFO
	 *SCI_FFTX_TXFIFORESET-TX_FIFO复位释放
	 *SCI_FFRX_RXFIFORESET-RX_FIFO复位释放 */
	SCI_enableFIFO(SCIA_BASE);
	/*SCI模块使能：SCI_CTL1_TXENA-发送线使能&SCI_CTL1_RXENA-接收线使能
	 *SCI_CTL1_SWRESET-SCI软复位 */
	SCI_enableModule(SCIA_BASE);
}

void sci_transmit(void)
{
	/*判断是否接收到数据,若接收到数据,则将数据返回到发送端*/
	while(SCI_getRxFIFOStatus(SCIA_BASE) != SCI_FIFO_RX0)
	 {
		 SCI_writeCharNonBlocking(SCIA_BASE,
				 SCI_readCharBlockingFIFO(SCIA_BASE));
	 }
}
