#include "system.h"

void initECAP()
{
	/*ECAP关中断：ECAP_ISR_SOURCE_CAPTURE_EVENT_1-捕获事件1
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_2-捕获事件2
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_3-捕获事件3
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_4-捕获事件4
	 * ECAP_ISR_SOURCE_COUNTER_OVERFLOW -计数器溢出
	 * ECAP_ISR_SOURCE_COUNTER_PERIOD-计数器达到周期值
	 * ECAP_ISR_SOURCE_COUNTER_COMPARE-计数器达到比较值*/
    ECAP_disableInterrupt(ECAP1_BASE,
                          (ECAP_ISR_SOURCE_CAPTURE_EVENT_1  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_2  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_3  |
                           ECAP_ISR_SOURCE_CAPTURE_EVENT_4  |
                           ECAP_ISR_SOURCE_COUNTER_OVERFLOW |
                           ECAP_ISR_SOURCE_COUNTER_PERIOD   |
                           ECAP_ISR_SOURCE_COUNTER_COMPARE));
	/*ECAP清中断：ECAP_ISR_SOURCE_CAPTURE_EVENT_1-捕获事件1
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_2-捕获事件2
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_3-捕获事件3
	 * ECAP_ISR_SOURCE_CAPTURE_EVENT_4-捕获事件4
	 * ECAP_ISR_SOURCE_COUNTER_OVERFLOW -计数器溢出
	 * ECAP_ISR_SOURCE_COUNTER_PERIOD-计数器达到周期值
	 * ECAP_ISR_SOURCE_COUNTER_COMPARE-计数器达到比较值*/
    ECAP_clearInterrupt(ECAP1_BASE,
                        (ECAP_ISR_SOURCE_CAPTURE_EVENT_1  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_2  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_3  |
                         ECAP_ISR_SOURCE_CAPTURE_EVENT_4  |
                         ECAP_ISR_SOURCE_COUNTER_OVERFLOW |
                         ECAP_ISR_SOURCE_COUNTER_PERIOD   |
                         ECAP_ISR_SOURCE_COUNTER_COMPARE));

    /*关闭ECAP时间戳捕获*/
    ECAP_disableTimeStampCapture(ECAP1_BASE);
    /*ECAP停止计数器*/
    ECAP_stopCounter(ECAP1_BASE);

    /*ECAP使能捕获模式*/
    ECAP_enableCaptureMode(ECAP1_BASE);
    /*ECAP捕获模式配置：ECAP_ONE_SHOT_CAPTURE_MODE-单次触发捕获
     * ECAP_EVENT_4-捕获事件4*/
    ECAP_setCaptureMode(ECAP1_BASE, ECAP_ONE_SHOT_CAPTURE_MODE, ECAP_EVENT_4);

    /*ECAP捕获极性配置：ECAP_EVENT_1-捕获事件1，ECAP_EVNT_FALLING_EDGE-下降沿触发*/
    ECAP_setEventPolarity(ECAP1_BASE, ECAP_EVENT_1, ECAP_EVNT_FALLING_EDGE);
    /*ECAP捕获极性配置：ECAP_EVENT_2-捕获事件2，ECAP_EVNT_FALLING_EDGE-上升沿触发*/
    ECAP_setEventPolarity(ECAP1_BASE, ECAP_EVENT_2, ECAP_EVNT_RISING_EDGE);
    /*ECAP捕获极性配置：ECAP_EVENT_3-捕获事件3，ECAP_EVNT_FALLING_EDGE-下降沿触发*/
    ECAP_setEventPolarity(ECAP1_BASE, ECAP_EVENT_3, ECAP_EVNT_FALLING_EDGE);
    /*ECAP捕获极性配置：ECAP_EVENT_4-捕获事件4，ECAP_EVNT_RISING_EDGE-上升沿触发*/
    ECAP_setEventPolarity(ECAP1_BASE, ECAP_EVENT_4, ECAP_EVNT_RISING_EDGE);

    /*ECAP使能计数器复位：ECAP_EVENT_1-捕获事件1*/
    ECAP_enableCounterResetOnEvent(ECAP1_BASE, ECAP_EVENT_1);
    /*ECAP使能计数器复位：ECAP_EVENT_2-捕获事件2*/
    ECAP_enableCounterResetOnEvent(ECAP1_BASE, ECAP_EVENT_2);
    /*ECAP使能计数器复位：ECAP_EVENT_3-捕获事件3*/
    ECAP_enableCounterResetOnEvent(ECAP1_BASE, ECAP_EVENT_3);
    /*ECAP使能计数器复位：ECAP_EVENT_4-捕获事件4*/
    ECAP_enableCounterResetOnEvent(ECAP1_BASE, ECAP_EVENT_4);

    /*ECAP事件输入：通过ECAP_INPUT_INPUTXBAR7-交叉开关XBAR7输入*/
    ECAP_selectECAPInput(ECAP1_BASE, ECAP_INPUT_INPUTXBAR7);

    /*ECAP使能计数器装载：ECAP_ECCTL2_SYNCI_EN-同步输入*/
    ECAP_enableLoadCounter(ECAP1_BASE);
    /*ECAP同步输出模式：ECAP_SYNC_OUT_DISABLED-关闭同步输出*/
    ECAP_setSyncOutMode(ECAP1_BASE, ECAP_SYNC_OUT_DISABLED);

    /*ECAP启动计数器*/
    ECAP_startCounter(ECAP1_BASE);
    /*ECAP时间戳捕获使能*/
    ECAP_enableTimeStampCapture(ECAP1_BASE);
    /*ECAP重载计数*/
    ECAP_reArm(ECAP1_BASE);
    /*ECAP使能中断：ECAP_ISR_SOURCE_CAPTURE_EVENT_4-捕获事件中断4*/
    ECAP_enableInterrupt(ECAP1_BASE, ECAP_ISR_SOURCE_CAPTURE_EVENT_4);
}


__interrupt void ecap1ISR(void)
{
    if(stopCaptures==0)
    {
        /*读取ECAP捕获时间戳ECAP_EVENT_1-捕获事件1-Cap1*/
        capCountArr[0] = 1+ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_1);
        /*读取ECAP捕获时间戳ECAP_EVENT_2-捕获事件2-Cap2*/
        capCountArr[1] = 1+ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_2);
        /*读取ECAP捕获时间戳ECAP_EVENT_3-捕获事件3-Cap3*/
        capCountArr[2] = 1+ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_3);
        /*读取ECAP捕获时间戳ECAP_EVENT_4-捕获事件4-Cap4*/
        capCountArr[3] = 1+ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_4);

        /*判断实际波特率是否与设定波特率相一致*/
        capCountIter = 0;
        for(capCountIter=0;capCountIter<4;capCountIter++)
        {
            /*若实际波特率与设定波特率一致，则GPIO31/LED1亮*/
            if(sampleArrIter<NUMSAMPLES)
            {
                sampleArr[sampleArrIter] = capCountArr[capCountIter];
                sampleArrIter++;
                GPIO_writePin(31,0);
                /*数据传输*/
                sci_transmit();
            }
            /*若实际波特率与设定波特率不一致，停止接收，GPIO34/LED2亮*/
            else
            {
                stopCaptures=1;
                GPIO_writePin(34,0);
                break;
            }
        }
    }

    /*清除捕获事件中断：ECAP_ISR_SOURCE_CAPTURE_EVENT_4-捕获事件4*/
    ECAP_clearInterrupt(ECAP1_BASE,ECAP_ISR_SOURCE_CAPTURE_EVENT_4);
    /*清除全局中断*/
    ECAP_clearGlobalInterrupt(ECAP1_BASE);

    /*ECAP重载计数*/
    ECAP_reArm(ECAP1_BASE);

    /*清除PIE应答*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP4);
}
