/*
 * system.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
// must replace with the GPIO number of the SCIRX pin chosen in .syscfg
#define GPIO_SCIRX_NUMBER   9

//
// choose baud rate being received from target baud rate device
// closest baud rate to 9600 that can be set in register is 9601
//
#define TARGETBAUD          9601

//
// number of samples in the array (higher = better averaging, lower = faster)
//
#define NUMSAMPLES          32

//
// margin for what is considered a "good" pulse width:
// set this higher to allow more samples to be considered "good" data,
// if losing too much data set this lower to prevent "bad" samples to
// be discarded more strictly
//
#define MARGINPERCENT       0.05

//
// at least this percentage of the samples array must be "good"
// to not flag an error
//
#define MINSAMPLEPERCENT    0.50


//
// Globals
//
extern volatile uint32_t capCountArr[4];
extern volatile int capCountIter;
extern volatile float sampleArr[NUMSAMPLES];
extern volatile uint16_t sampleArrIter;
extern volatile uint16_t stopCaptures;

//
// Function Prototypes
//
void initECAP(void);
__interrupt void ecap1ISR(void);
uint16_t arrTo1PulseWidth(volatile float arr[], int size, float targetWidth);
float computeAvgWidth(volatile float arr[], int size);
uint32_t getAverageBaud(volatile float arr[], int size, float targetBaudRate);

void sci_gpio(void);
uint32_t scia_config(uint32_t baud);

void sci_transmit(void);

#endif /* SYSTEM_H_ */
