/******************************************************************
 文 档 名：       HX_DSC280025_SCI_baud_tune_via_uart.c
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      SCI波特率修正与数据传输
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用SCI与外部连接串口，
 实现数据传输，并修正实际波特率。

外部接线：GPIO9-SCIA_RX/eCAP1与SCI串口通讯发送TX，
GPIO8-SCI_TX与SCI串口通讯接收RX相连

现象：通过串口调试助手，任意发送一组数据，并通过串口返回数据

 版 本：      V1.0.1
 时 间：      2024年10月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//

#include "system.h"


//
// Globals
//
volatile uint32_t capCountArr[4];
volatile int capCountIter = 0;
volatile float sampleArr[NUMSAMPLES];
volatile uint16_t sampleArrIter = 0;
volatile uint16_t stopCaptures = 0;


uint32_t avgBaud;
//
// Main
//
int main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定关闭*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置SCIA_GPIO:SCIA_RX/GPIO9 SCIA_TX/GPIO8*/
    sci_gpio();
	/*配置SCIA模块：波特率9600bps*/
    scia_config(9600);
    EDIS;

	/*配置SCIRX的GPIO9，通过XBAR_INPUT7-交叉开关XBAR7输入接收*/
    XBAR_setInputPin(INPUTXBAR_BASE, XBAR_INPUT7, GPIO_SCIRX_NUMBER);

	/*中断入口地址INT_ECAP1,指向执行ecap1ISR中断服务程序*/
    Interrupt_register(INT_ECAP1, &ecap1ISR);

    /*配置ECAP捕获SCI_RX接收波特率*/
    initECAP();

    /*中断使能INT_ECAP1*/
    Interrupt_enable(INT_ECAP1);

    /*使能打开全局中断*/
    EINT;
    ERTM;

    for(;;)
    {
        /*若实际波特率与设定波特率不一致，则按实际波特率接收*/
        if(stopCaptures==1)
        {
            /*读取平均波特率*/
            avgBaud = getAverageBaud(sampleArr,NUMSAMPLES,TARGETBAUD);
            /*更新波特率配置*/
            while(avgBaud!=0)
            {
                SCI_setBaud(SCIA_BASE, DEVICE_LSPCLK_FREQ, avgBaud);
                /*数据传输*/
                sci_transmit();
            }
            sampleArrIter=0;
            stopCaptures=0;
        }
    }
    return 0;
}
