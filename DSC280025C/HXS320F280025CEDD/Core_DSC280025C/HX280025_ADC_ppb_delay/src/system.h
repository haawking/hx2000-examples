/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_
//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define DELAY_BUFFER_SIZE 30


//
// Globals
//
extern uint32_t conversion_count;
extern uint32_t conversion[DELAY_BUFFER_SIZE];
extern uint16_t delay[DELAY_BUFFER_SIZE];
extern volatile uint16_t delay_index;

//
// Functional Prototypes
//
void configureEPWM(uint32_t epwmBase, uint16_t period);
__interrupt void adcA1ISR(void);
__interrupt void adcA2ISR(void);

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);

#endif /* SYSTEM_H_ */
