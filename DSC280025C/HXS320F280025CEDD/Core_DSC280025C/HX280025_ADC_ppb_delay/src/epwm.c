#include "system.h"

void configureEPWM(uint32_t epwmBase, uint16_t period)
{
	/*关闭ADC触发：EPWM_SOC_A-EPWM_SOCA*/
    EPWM_disableADCTrigger(epwmBase, EPWM_SOC_A);

	/*配置ADC触发源时间：EPWM_SOC_A-EPWM_SOCA
	 * 在EPWM_SOC_TBCTR_U_CMPA-向上计数CTR=CMPA事件时触发产生ADC采样*/
    EPWM_setADCTriggerSource(epwmBase, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    /*配置ADC触发事件预分频=EPWM_TBCLK/1*/
    EPWM_setADCTriggerEventPrescale(epwmBase, EPWM_SOC_A, 1U);

    /*配置EPWM比较点：CMPA=TBPRD/2-对应脉宽50%*/
    EPWM_setCounterCompareValue(epwmBase, EPWM_COUNTER_COMPARE_A, period/2);
    /*配置PWM周期=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)*/
    EPWM_setTimeBasePeriod(epwmBase, period);
    /*配置EPWM时基时钟分频：低速时钟EPWM_CLOCK_DIVIDER_1-1分频
     * 高速时钟 EPWM_HSCLOCK_DIVIDER_1-1分频*/
    EPWM_setClockPrescaler(epwmBase,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    /*配置EPWM计数模式：EPWM_COUNTER_MODE_STOP_FREEZE-冻结计数器*/
    EPWM_setTimeBaseCounterMode(epwmBase, EPWM_COUNTER_MODE_STOP_FREEZE);
}
