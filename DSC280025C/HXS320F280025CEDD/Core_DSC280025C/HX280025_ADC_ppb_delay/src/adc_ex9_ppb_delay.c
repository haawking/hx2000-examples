/******************************************************************
 文 档 名：       HX_DSC280025_ADC_ppb_delay
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC顺序采样与时间测量
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 (1)采用EWM1_SOCA（向上计数）在向上计数CTR=CMPA时，触发A0通道ADC采样；
 采用EWM2_SOCA（向上计数）在向上计数CTR=CMPA时，触发A2通道ADC采样；
 (2)当采样宽度不超过缓冲区宽度时，读取采样结果

连接：A0/A2连接一可调的电位器,
现象：读出采样结果=(myADC0/2Result)/4096*3.3V，与实际电压一致

 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/



#include "system.h"


void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定解除*/
    Device_initGPIO();

    /*初始化程序变量清零*/
    conversion_count = 0;
    for(delay_index = 0; delay_index < DELAY_BUFFER_SIZE; delay_index++)
    {
        delay[delay_index] = 0;
        conversion[delay_index] = 0;
    }

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
    /*中断入口地址INT_ADCA1，指向执行adcA1ISR中断服务程序*/
	Interrupt_register(INT_ADCA1, &adcA1ISR);
    /*中断入口地址INT_ADCA2，指向执行adcA2ISR中断服务程序*/
	Interrupt_register(INT_ADCA2, &adcA2ISR);
    EDIS;

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();
    EDIS;

    /*配置EPWM1模块及SOC触发：频率=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)=78kHz*/
    configureEPWM(EPWM1_BASE, 2048);
    /*配置EPWM2模块及SOC触发：频率=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)=16kHz*/
    configureEPWM(EPWM2_BASE, 9999);

	/*中断使能INT_ADCA1*/
	Interrupt_enable(INT_ADCA1);
	/*中断使能INT_ADCA2*/
	Interrupt_enable(INT_ADCA2);

	/*打开全局中断*/
    EINT;
    ERTM;

    /*使能外设TBCLK同步，以完成EPWM配置与同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    do
    {
        delay_index = 0;

        /*使能ADC触发：EPWM1_SOCA*/
        EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
        /*使能ADC触发：EPWM2_SOCA*/
        EPWM_enableADCTrigger(EPWM2_BASE, EPWM_SOC_A);

        /*配置EPWM1计数模式：EPWM_COUNTER_MODE_UP-向上计数*/
        EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);
        /*配置EPWM2计数模式：EPWM_COUNTER_MODE_UP-向上计数*/
        EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_UP);

        /*等待采样宽度不超过缓冲区大小*/
        while(DELAY_BUFFER_SIZE > delay_index){}

    	/*关闭ADC触发：EPWM_SOC_A-EPWM_SOCA*/
        EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
        EPWM_disableADCTrigger(EPWM2_BASE, EPWM_SOC_A);
        /*配置EPWM计数模式：EPWM_COUNTER_MODE_STOP_FREEZE-冻结计数器*/
        EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
        EPWM_setTimeBaseCounterMode(EPWM2_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    }
    while(1);
}
//
// End of file
//
