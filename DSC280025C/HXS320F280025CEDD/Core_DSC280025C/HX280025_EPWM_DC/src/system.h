/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"


//
// Globals
//
extern uint32_t  epwm1TZIntCount;
//
// Function Prototypes
//
__interrupt void epwm1TZISR(void);

/*EPWM的GPIO引脚配置*/
void epwm_gpio(void);
/*EPWM的错误联防TZ引脚配置*/
void epwm_tz_gpio(void);
/*同步策略*/
void sync_init(void);
/*EPWM配置*/
void epwm_config(void);

#endif /* SYSTEM_H_ */
