/******************************************************************
 文 档 名：       HX_DSC280025_ADC_soc_software_sync
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC多路SOC顺序采样
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用GPIO9按键控制，强制软件触发,
 通过A2/A3/C2/C3通道，采样SOC输入电压

 现象:
通过A2/A3/C2/C3通道连接相应电位器元件,
可读出正确的电压值Vi=(myADC0/1Result0/1)/4096*3.3V

 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Globals
//
uint16_t myADC0Result0[3];
uint16_t myADC0Result1[3];
uint16_t myADC1Result0[3];
uint16_t myADC1Result1[3];

uint16_t i;


uint16_t adc_A2Result;
uint16_t adc_A3Result;
uint16_t adc_C2Result;
uint16_t adc_C3Result;

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置GPIO10为交叉开关XBAR_INPUT5输入*/
	XBAR_setInputPin(INPUTXBAR_BASE, XBAR_INPUT5, 10);
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    AdcA_config();
    AdcC_config();
	/*按键输入，强制ADC软件触发*/
    InitKEY();
    EDIS;

    /*打开全局中断*/
    EINT;
    ERTM;

    do
    {
    	/*GPIO10写入1置高，触发ADC软件采样*/
        GPIO_writePin(9U, 1U);
       	/*GPIO10写入0清除按键，防止连续触发*/
        GPIO_writePin(9U, 0U);

        /*等待ADC采样完成*/
        while(ADC_getInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1) == 0U);
        /*清除ADC中断：ADC_INT_NUMBER1-ADCINT1*/
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

        /*读取ADC采样结果ADC_SOC_NUMBER0-SOC0*/
        for(i=0;i<3;i++)
        {
        	myADC0Result0[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        	if(i>=2)
        	{
        		adc_A2Result=myADC0Result0[i];
        	}
        }

        /*读取ADC采样结果ADC_SOC_NUMBER1-SOC1*/
        for(i=0;i<3;i++)
        {
        	myADC0Result1[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        	if(i>=2)
        	{
        		adc_A3Result=myADC0Result1[i];
        	}
        }

        /*读取ADC采样结果ADC_SOC_NUMBER0-SOC0*/
        for(i=0;i<3;i++)
        {
        	myADC1Result0[i]= ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);
        	if(i>=2)
        	{
        		adc_C2Result=myADC1Result0[i];
        	}
        }
        /*读取ADC采样结果ADC_SOC_NUMBER1-SOC1*/
        for(i=0;i<3;i++)
        {
        	myADC1Result1[i]= ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        	if(i>=2)
        	{
        		adc_C3Result=myADC1Result1[i];
        	}
        }
    }
    while(1);
}

//
// End of file
//
