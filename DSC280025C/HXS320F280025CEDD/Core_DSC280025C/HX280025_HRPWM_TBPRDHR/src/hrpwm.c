#include <system.h>

void epwm_gpio(void)
{
	/*配置GPIO0为EPWM1A*/
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
	/*配置GPIO1为EPWM1B*/
	GPIO_setPinConfig(GPIO_1_EPWM1_B);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);

	/*配置GPIO2为EPWM2A*/
	GPIO_setPinConfig(GPIO_2_EPWM2_A);
	GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(2, GPIO_QUAL_SYNC);
	/*配置GPIO3为EPWM2B*/
	GPIO_setPinConfig(GPIO_3_EPWM2_B);
	GPIO_setPadConfig(3, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(3, GPIO_QUAL_SYNC);

	/*配置GPIO4为EPWM3A*/
	GPIO_setPinConfig(GPIO_4_EPWM3_A);
	GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(4, GPIO_QUAL_SYNC);
	/*配置GPIO5为EPWM3B*/
	GPIO_setPinConfig(GPIO_5_EPWM3_B);
	GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(5, GPIO_QUAL_SYNC);

	/*配置GPIO6为EPWM4A*/
	GPIO_setPinConfig(GPIO_6_EPWM4_A);
	GPIO_setPadConfig(4, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(4, GPIO_QUAL_SYNC);

	/*配置GPIO7为EPWM4B*/
	GPIO_setPinConfig(GPIO_7_EPWM4_B);
	GPIO_setPadConfig(7, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(7, GPIO_QUAL_SYNC);
}

void hrpwm_config(uint32_t epwm_base,uint32_t epwm_tbprd,uint32_t epwm_cmpa,
		uint32_t epwm_cmpb,uint32_t epwm_tbphs,
		uint32_t ET_interruptSource,uint32_t ET_eventCount,
		HRPWM_Channel channel,HRPWM_MEPEdgeMode mepEdgeMode_A)
{
    /*HRPWM仿真模式：自由运行*/
	HRPWM_setEmulationMode(epwm_base, EPWM_EMULATION_FREE_RUN);
    /*EPWM1时基分频配置：低速时钟EPWM_CLOCK_DIVIDER_1-1分频，高速时钟EPWM_HSCLOCK_DIVIDER_1-1分频*/
	HRPWM_setClockPrescaler(epwm_base, EPWM_CLOCK_DIVIDER_1, EPWM_HSCLOCK_DIVIDER_1);
    /*EPWM周期配置：SYSCLK/CLKDIV/HSPCLKDIV/TBPRD/2=160M/1/1/99/2=800kHz*/
	EPWM_setTimeBasePeriod(epwm_base, epwm_tbprd);
    /*HRPWM时基初值：TBCTR=0*/
	HRPWM_setTimeBaseCounter(epwm_base, 0);
    /*HRPWM时基计数模式：向上向下计数*/
	HRPWM_setTimeBaseCounterMode(epwm_base, EPWM_COUNTER_MODE_UP_DOWN);
    /*HRPWM相位禁止装载:TBPHSHRLOADE=0*/
	HRPWM_disablePhaseShiftLoad(epwm_base);
    /*高精度HRPWM相移：TBPHSHR=0*/
	HRPWM_setPhaseShift(epwm_base, epwm_tbphs);

	/*EPWM比较值CMPA配置：CMPA=50-占空比50%*/
	EPWM_setCounterCompareValue(epwm_base, EPWM_COUNTER_COMPARE_A, epwm_cmpa);
    /*EPWM比较值CMPA装载点配置：在EPWM_COMP_LOAD_ON_CNTR_ZERO_PERIOD-TBCTR=0或TBPRD-零或周期值时装载*/
	HRPWM_setCounterCompareShadowLoadMode(epwm_base, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO_PERIOD);
    /*EPWM比较值CMPB配置：CMPB=50-占空比50%*/
	EPWM_setCounterCompareValue(epwm_base, EPWM_COUNTER_COMPARE_B, epwm_cmpb);
	 /*EPWM比较值CMPB装载点配置：在EPWM_COMP_LOAD_ON_CNTR_ZERO_PERIOD-TBCTR=0或TBPRD-零或周期值时装载*/
	HRPWM_setCounterCompareShadowLoadMode(epwm_base, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO_PERIOD);

	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO-TBCTR=零时
	 * EPWM_AQ_OUTPUT_NO_CHANGE-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD-TBCTR=周期值时
	 * EPWM_AQ_OUTPUT_NO_CHANGE-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA-向上计数时TBCTR=CMPA事件时
	 * EPWM_AQ_OUTPUT_HIGH,-输出置高动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA-向下计数时TBCTR=CMPA事件时
	 * EPWM_AQ_OUTPUT_LOW,-输出置低动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB-向上计数时TBCTR=CMPB事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB-向下计数时TBCTR=CMPB事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_A-EPWM1A
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO时计数TBCTR=ZERO-零事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);

	/*HRPWM动作配置：EPWM_AQ_OUTPUT_B-EPWM1B
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD时计数TBCTR=PERIOD-周期事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_B-EPWM1B
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA-向上计数TBCTR=CMPA事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_B-EPWM1B
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA-向下计数TBCTR=CMPA事件时
	 * EPWM_AQ_OUTPUT_NO_CHANGE,-不产生动作*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_B-EPWM1B
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB-向上计数TBCTR=CMPB事件时
	 * EPWM_AQ_OUTPUT_LOW,-置低*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);
	/*HRPWM动作配置：EPWM_AQ_OUTPUT_B-EPWM1B
	 * 在EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB-向下计数TBCTR=CMPB事件时
	 * EPWM_AQ_OUTPUT_HIGH,-置高*/
	HRPWM_setActionQualifierAction(epwm_base, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);

	/*EPWM事件中断使能*/
	EPWM_enableInterrupt(epwm_base);
	/*EPWM事件中断选择:在CTR=0时触发中断*/
	EPWM_setInterruptSource(epwm_base,ET_interruptSource);
	/*EPWM事件中断次数选择：每次进中断触发一次，最多可支持15次*/
	EPWM_setInterruptEventCount(epwm_base,ET_eventCount);

	/*HRPWM使能自动转换：AUTOCONV=1*/
	HRPWM_enableAutoConversion(epwm_base);
    /*HRPWM MEP微边沿定位选择:HRPWM_CHANNEL_A-通道A
     * HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE-双边沿定位*/
	HRPWM_setMEPEdgeSelect(epwm_base, channel, mepEdgeMode_A);
    /*HRPWM 计数比较装载：HRPWM_CHANNEL_A-通道A
     * HRPWM_LOAD_ON_CNTR_ZERO_PERIOD-在CTR=零或周期值时装载影子模式*/
	HRPWM_setCounterCompareShadowLoadEvent(epwm_base, channel, HRPWM_LOAD_ON_CNTR_ZERO_PERIOD);
    /*HRPWM 使能周期控制：HRPE=1*/
	HRPWM_enablePeriodControl(epwm_base);
}
