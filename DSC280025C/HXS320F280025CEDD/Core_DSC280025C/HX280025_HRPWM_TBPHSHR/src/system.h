/*
 * hrpwm.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"
#define EPWM1_TIMER_TBPRD            100UL
#define EPWM1_TIMER_TBPHS              0UL
#define EPWM2_TIMER_TBPRD            100UL
#define EPWM2_TIMER_TBPHS              30UL
#define EPWM3_TIMER_TBPRD            100UL
#define EPWM3_TIMER_TBPHS              30UL

#define MIN_HRPWM_DUTY_PERCENT      4.0/((float32_t)EPWM_TIMER_TBPRD)*100.0

__interrupt void epwm1ISR(void);
__interrupt void epwm2ISR(void);
__interrupt void epwm3ISR(void);
__interrupt void epwm4ISR(void);

void epwm_gpio(void);
void sync_config(void);

void hrpwm1_config(void);
void hrpwm2_config(void);
void hrpwm3_config(void);

#endif /* SYSTEM_H_ */
