/*
 * system.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define PWM_FREQUENCY        10000U // 10 Khz
#define PWM_DUTY             0.5f   // 50% duty
#define PWM_PHASE_SHIFT      0.3f   // 30% Phase shift
#define PWM_PRD_VAL          DEVICE_SYSCLK_FREQ / PWM_FREQUENCY
#define PWM_CMP_VAL          (uint32_t)(PWM_DUTY * PWM_PRD_VAL)
#define PWM_PHASE_VAL        (uint32_t)(PWM_PHASE_SHIFT * PWM_PRD_VAL)

void ECAP_apwm_config();
void OUTPUTXBAR_config();

void GPIO_config(void);

#endif /* SYSTEM_H_ */
