/******************************************************************
 文 档 名：       HX_DSC280025_CAN_LOOPBACK
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      CAN发送内环测试
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用内部回环连接，通过CAN总线查询方式收发数据

 现象:
 //!  - msgCount -接收到数据数量循环
//!  - txMsgData - 发送数据
//!  - rxMsgData - 接收数据
 *rxMsgData=txMsgData

 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/


//
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"

#include "system.h"

//
// Defines
//
#define MSG_DATA_LENGTH    8

//
// Globals
//
volatile unsigned long msgCount = 0;
uint16_t errorCount=0;

uint16_t txMsgData[8], rxMsgData[8];



void can_gpio_init(void);
void can_config(void);

#define MB_ADDR 0x21FFC

void Success(void)
{

 GPIO_writePin(31,0);
 GPIO_writePin(34,1);
 HWREG(MB_ADDR) = 0x5555AAAA;

}

void Fail(void)
{
 GPIO_writePin(31,1);
 GPIO_writePin(34,0);
 HWREG(MB_ADDR) = 0xAAAA5555;
}

//
// Main
//
int main(void)
{
    /*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();
    setup1GPIO();


    EALLOW;
    /*CAN的GPIO配置*/
    can_gpio_init();
    /*CAN的初始化功能配置*/
    can_config();
    EDIS;

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*打开全局与实时中断*/
    EINT;
//    ERTM;

    /*配置缓冲区发送与接收初值*/
    txMsgData[0] = 0x00;
    txMsgData[1] = 0x01;
    txMsgData[2] = 0x02;
    txMsgData[3] = 0x03;
    txMsgData[4] = 0x04;
    txMsgData[5] = 0x05;
    txMsgData[6] = 0x06;
    txMsgData[7] = 0x07;
    *(uint16_t *)rxMsgData = 0;

    //
    // Loop Forever - Send and Receive data continuously
    //
    for(;;)
    {
        /*can发送数据:对象ID:1,数据长度:2字节,发送数据*/
        CAN_sendMessage(CANA_BASE, 1, MSG_DATA_LENGTH, txMsgData);

        /*延时500us,以完成数据接收*/
        DEVICE_DELAY_US(500);

        //
        // Read CAN message object 2 and check for new data
        //
        if (CAN_readMessage(CANA_BASE, 2, rxMsgData))
        {
            //
            // Check that received data matches sent data.
            // Device will halt here during debug if data doesn't match.
            //
            if((txMsgData[0] != rxMsgData[0]) ||
               (txMsgData[1] != rxMsgData[1])||(txMsgData[2] != rxMsgData[2])
			   ||(txMsgData[3] != rxMsgData[3])||(txMsgData[4] != rxMsgData[4])
			   ||(txMsgData[5] != rxMsgData[5])||(txMsgData[6] != rxMsgData[6])
			   ||(txMsgData[7] != rxMsgData[7]))
            {
//                asm(" ESTOP0");
            	errorCount++;
                //GPIO_writePin(34, 0); //LED2 GPIO34亮表示失败
                Fail();
            }
            else
            {
                //
                // Increment message received counter
                //
                msgCount++;
                //GPIO_writePin(31, 0); //LED1 GPIO31亮表示成功
                Success();
            }
        }
        else
        {
            //
            // Device will halt here during debug if no new data was received.
            //
//            asm(" ESTOP0");
        }

        //
        // Increment the value in the transmitted message data.
        //
        txMsgData[0] += 0x01;
        txMsgData[1] += 0x01;
        txMsgData[2] += 0x01;
        txMsgData[3] += 0x01;
        txMsgData[4] += 0x01;
        txMsgData[5] += 0x01;
        txMsgData[6] += 0x01;
        txMsgData[7] += 0x01;

        //
        // Reset data if exceeds a byte
        //
        if(txMsgData[0] > 0xFF)
        {
            txMsgData[0] = 0;
        }
        if(txMsgData[1] > 0xFF)
        {
            txMsgData[1] = 0;
        }
        if(txMsgData[2] > 0xFF)
        {
            txMsgData[2] = 0;
        }
        if(txMsgData[3] > 0xFF)
        {
            txMsgData[3] = 0;
        }
        if(txMsgData[4] > 0xFF)
        {
            txMsgData[4] = 0;
        }
        if(txMsgData[5] > 0xFF)
        {
            txMsgData[5] = 0;
        }
        if(txMsgData[6] > 0xFF)
        {
            txMsgData[6] = 0;
        }
        if(txMsgData[7] > 0xFF)
        {
            txMsgData[7] = 0;
        }
    }

    return 0;
}

//
// End of File
//

void can_gpio_init(void)
{
    /*配置GPIO3为CANRX*/
    GPIO_setPinConfig(GPIO_3_CANA_RX);
    /*配置CANRX引脚翻转输入或翻转输出*/
    GPIO_setPadConfig(3, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
    /*配置CANRX引脚采样异步*/
    GPIO_setQualificationMode(3, GPIO_QUAL_ASYNC);

    /*配置GPIO2为CANTX*/
    GPIO_setPinConfig(GPIO_2_CANA_TX);
    /*配置CANTX引脚翻转输入或翻转输出*/
    GPIO_setPadConfig(2, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
    /*配置CANTX引脚采样异步*/
    GPIO_setQualificationMode(2, GPIO_QUAL_ASYNC);
}

void can_config(void)
{
    /*CAN模块初始化*/
    CAN_initModule(CANA_BASE);
    /*CAN波特率配置：预分频7，扩展分频0，TSEG1=15，TSEG2=7，同步跳转宽度sjw=3*/
    CAN_setBitTiming(CANA_BASE, 24, 0, 1, 1, 3);
    /*CAN测试模式：CAN_TEST_SILENT静默模式，CAN_TEST_LBACK内环模式，CAN_TEST_EXL内环连接*/
    CAN_enableTestMode(CANA_BASE, CAN_TEST_LBACK);

    /*CAN发送配置：对象ID配置：1，msgID配置：4，帧类型：标准帧，
     * msg类型：发送对象，msg ID掩码：0，对象标志：0，信息数据长度：2字节*/
    CAN_setupMessageObject(CANA_BASE, 1, 4, CAN_MSG_FRAME_STD,CAN_MSG_OBJ_TYPE_TX, 0, 0,8);

    /*CAN接收配置：对象ID配置：2，msgID配置：4，帧类型：标准帧，
     * msg类型：接收对象，msg ID掩码：0，对象标志：0，信息数据长度：2字节*/
    CAN_setupMessageObject(CANA_BASE, 2, 4, CAN_MSG_FRAME_STD,CAN_MSG_OBJ_TYPE_RX, 0, 0,8);

    /*启动CAN模块操作*/
    CAN_startModule(CANA_BASE);
}
