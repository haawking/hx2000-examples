/******************************************************************
 文 档 名：     spi.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供SPI的FIFO功能配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

 void initSPIFIFO()
 {
     /*采用SPISWRESET软复位，关闭SPI模块，便于初始化配置*/
     SPI_disableModule(SPIA_BASE);

     /*SPI配置:系统时钟,SPI时序配置:极性SPI_PROT_POL0PHA0上升沿无延迟
      * SPI_MODE_MASTER主机模式,波特率500kbps,数据长度16位*/
     SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPI_PROT_POL0PHA0,
                   SPI_MODE_MASTER, 500000, 16);
     /*SPI内环使能*/
     SPI_enableLoopback(SPIA_BASE);
     /*SPI仿真模式配置:SPI_EMULATION_STOP_AFTER_TRANSMIT传输将停止在开始传输完成后*/
     SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_STOP_AFTER_TRANSMIT);

     /*SPI使能FIFO:SPIFFENA,TXFIFO,RXFIFORESET*/
     SPI_enableFIFO(SPIA_BASE);
     /*SPI清除中断状态：SPI接收FIFO：SPI_INT_RXFF,SPI发送FIFO：SPI_INT_TXFF*/
     SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_RXFF | SPI_INT_TXFF);
     /*SPI FIFO中断线配置：SPI_FIFO_TX2 SPI发送FIFO中断线2，SPI_FIFO_RX2 SPI接收FIFO中断线2*/
     SPI_setFIFOInterruptLevel(SPIA_BASE, SPI_FIFO_TX2, SPI_FIFO_RX2);
     /*SPI使能中断：SPI接收FIFO：SPI_INT_RXFF,SPI发送FIFO：SPI_INT_TXFF*/
     SPI_enableInterrupt(SPIA_BASE, SPI_INT_RXFF | SPI_INT_TXFF);

     /*SPISWRESET软复位释放，初始化配置生效*/
     SPI_enableModule(SPIA_BASE);
 }


