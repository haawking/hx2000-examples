/******************************************************************
 文 档 名：     i2c.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供I2C的FIFO功能配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月24日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "system.h"

void initI2CFIFO()
{
    /*采用I2CMDR[IRS]关闭I2C模块，便于初始化配置*/
    I2C_disableModule(I2CA_BASE);

    /*配置I2C主机：系统时钟，I2C时钟400KHz，占空比50%*/
    I2C_initMaster(I2CA_BASE, DEVICE_SYSCLK_FREQ, 400000, I2C_DUTYCYCLE_50);
    /*配置I2C模式为I2C_MASTER_SEND_MODE主机发送*/
    I2C_setConfig(I2CA_BASE, I2C_MASTER_SEND_MODE);
    /*配置I2C发送数据I2CCNT为2个*/
    I2C_setDataCount(I2CA_BASE, 2);
    /*配置I2C发送数据长度为8位*/
    I2C_setBitCount(I2CA_BASE, I2C_BITCOUNT_8);

    /*配置I2C的从机地址0x3C*/
    I2C_setSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);
    /*配置I2C的自身地址0x3C*/
    I2C_setOwnSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);
    /*采用I2CMDR[DLB]使能I2C的内环模式*/
    I2C_enableLoopback(I2CA_BASE);
    /*配置I2C的仿真模式：I2C_EMULATION_STOP_SCL_LOW拉低继续，拉高停止*/
    I2C_setEmulationMode(I2CA_BASE, I2C_EMULATION_STOP_SCL_LOW);

    /*配置I2C FIFO使能*/
    I2C_enableFIFO(I2CA_BASE);
    /*清除I2C接收FIFO与发送FIFO中断状态*/
    I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_RXFF | I2C_INT_TXFF);

    /*配置I2C FIFO中断线为I2C_FIFO_TX2与I2C_FIFO_RX2*/
    I2C_setFIFOInterruptLevel(I2CA_BASE, I2C_FIFO_TX2, I2C_FIFO_RX2);
    /*使能I2C接收FIFO与发送FIFO中断*/
    I2C_enableInterrupt(I2CA_BASE, I2C_INT_RXFF | I2C_INT_TXFF);

    /*采用I2CMDR[IRS]启动I2C模块，初始化配置生效*/
    I2C_enableModule(I2CA_BASE);
}
