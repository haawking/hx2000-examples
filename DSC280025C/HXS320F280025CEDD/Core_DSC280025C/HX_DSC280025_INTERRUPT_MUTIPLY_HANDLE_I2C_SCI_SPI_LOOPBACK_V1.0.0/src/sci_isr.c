/******************************************************************
 文 档 名：     sci_isr.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供SCI的FIFO中断收发程序
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

 __interrupt void sciaTXFIFOISR(void)
 {
     uint16_t i;
     /*发送2字节数组sDatasciA[i]*/
     SCI_writeCharArray(SCIA_BASE, sDatasciA, 2);

     /*发送数据循环增加*/
     for(i = 0; i < 2; i++)
     {
         sDatasciA[i] = (sDatasciA[i] + 1) & 0x00FF;
     }
     /*清除发送FIFO状态中断标志*/
     SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXFF);

     /*清除中断应答：第9组*/
     Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
 }

 __interrupt void sciaRXFIFOISR(void)
 {
     uint16_t i;
     /*读取接收到的2字节数组rDatasciA[i]*/
     SCI_readCharArray(SCIA_BASE, rDatasciA, 2);

     /*核对接收到的数据数量是否与发送数据一一对应*/
     for(i = 0; i < 2; i++)
     {
         if(rDatasciA[i] != ((rDataPointA + i) & 0x00FF))
         {
             /*GPIO34写入1,LED2灭*/
             GPIO_writePin(34, 1);
         }
         else
         {
             /*GPIO34写入0,点亮LED2*/
             GPIO_writePin(34, 0);
         }
     }

     rDataPointA = (rDataPointA + 1) & 0x00FF;
     /*清除SCI溢出状态*/
     SCI_clearOverflowStatus(SCIA_BASE);
     /*清除SCI中断状态*/
     SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_RXFF);

     /*清除中断应答：第9组*/
     Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
 }
