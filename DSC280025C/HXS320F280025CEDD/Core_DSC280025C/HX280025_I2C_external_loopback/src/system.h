/*
 * system.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define CHAR_LENGTH     8
#define FRAME_LENGTH    1

//
// Globals
//
extern volatile uint32_t rxCount;
extern volatile uint32_t vectorOffset_rx;
extern volatile uint16_t error;
extern uint16_t transmitChar;
extern uint16_t receivedChar;
extern volatile uint32_t txCount;

extern volatile uint32_t vectorOffset_tx;
//
// Function Prototypes
//
void GPIO_config(void);
__interrupt void dataRxISR(void);
__interrupt void dataTxISR(void);
void configureSCIAMode(void);
void configureSCIBMode(void);

void LIN_GPIO(void);

#endif /* SYSTEM_H_ */
