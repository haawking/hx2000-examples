/******************************************************************
 文 档 名：       HX_DSC280025_EPWM_CMPC
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM CMPC脉宽比较点扩充
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，通过EPWM1SYNCO在向上计数CTR=CMPC时，
 产生EPWM1SYNCI输入同步信号，产生TI事件，触发EPWM1A动作
现象：EPWM1A在CTR=CMPC-12.5%比较点事件置高
应用：可采用此方法对PWM计数比较事件进行扩充划分，
由Type2 PWM的7个点，到Type4 PWM的最大可划分11个点
 *
 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/



#include "system.h"

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定*/
    Device_initGPIO();

	/*屏蔽TBCLK时基同步，便于PWM初始化配置写入*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
    
    EALLOW;
	/*配置GPIO0为EPWM1A,GPIO1为EPWM1B*/
    epwm_gpio();
	/*同步策略*/
    sync_config();
	/*EPWM配置*/
    epwm_config();
    EDIS;

	/*使能TBCLK时基同步，以使PWM配置写入，并实现多PWM同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}


