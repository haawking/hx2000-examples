/******************************************************************************************
 文 档 名：       HX_DSC280025_EPWM_ex9_dma
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：           DSC280025
 使 用 库：
 作     用：       ePWM 使用 DMA
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------

// FILE:   epwm_ex9_dma.c

本例配置ePWM1和DMA如下:
PWM1用于生成PWM波形
DMA5设置为每周期更新CMPAHR, CMPA, CMPBHR和CMPB使用配置数组中的下一个值。
这允许用户创建一个DMA支持的fifo,为所有CMPx和CMPxHR寄存器生成非传统的PWM波形。
DMA6设置为每个周期更新TBPHSHR、TBPHS、TBPRDHR和TBPRD，并使用配置数组中的下一个值。
其他寄存器如AQCTL也可以按照相同的过程通过DMA进行控制。

外围连接：
GPIO0 EPWM1A
GPIO1 EPWM1B
观测变量：
无

版 本：V1.0.0
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
************************************************************************************/
//
// Included Files
//

#include "IQmathLib.h"
#include "syscalls.h"
#include "stdlib.h"
#include "time.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"

#define EPWM_TIMER_TBPRD    20000UL

//
// Defines
//
#define BURST       4              // 4 words per transfer
#define TRANSFER    4              // 4 transfers (different configs)

// Place buffers in GSRAM
//#pragma DATA_SECTION(compareConfigs,    "ramgs0");
//#pragma DATA_SECTION(phasePeriodConfigs, "ramgs0");
//uint16_t CODE_SECTION("ramgs0") compareConfigs[TRANSFER*BURST];
//uint16_t CODE_SECTION("ramgs0") phasePeriodConfigs[TRANSFER*BURST];


uint16_t CODE_SECTION("ramgs0") phasePeriodConfigs[TRANSFER*BURST] = {
//  TBPHSHR ,   TBPHS   ,  TBPRDHR ,   TBPRD,
    9  << 8 ,    17U    ,  13 << 8 ,   2000U,
    10 << 8 ,    18U    ,  14 << 8 ,   4000U,
    11 << 8 ,    19U    ,  15 << 8 ,   6000U,
    12 << 8 ,    20U    ,  16 << 8 ,   8000U,
};

uint16_t CODE_SECTION("ramgs0") compareConfigs[TRANSFER*BURST] = {
//  CMPAHR  ,   CMPA   ,   CMPBHR  ,   CMPB ,
    1 << 8  ,  1001U   ,   5 << 8  ,   1000U,
    2 << 8  ,  2001U   ,   6 << 8  ,   2000U,
    3 << 8  ,  3001U   ,   7 << 8  ,   3000U,
    4 << 8  ,  4001U   ,   8 << 8  ,   4000U,
};
//
// Function Prototypes
//
void initDMA(void);
void initEPWM(uint32_t base);

__interrupt void dmaCh5ISR(void);
__interrupt void dmaCh6ISR(void);
__interrupt void epwm1ISR(void);

//
// Main
//
int main(void)
{

    //初始化设备时钟和外围设备
    Device_init();

    //禁用引脚锁并启用内部上拉。
    Device_initGPIO();

    //初始化PIE并清空PIE寄存器。禁止CPU中断
    Interrupt_initModule();

    //用指向shell Interrupt的指针初始化PIE向量表服务例程(ISR)。
    Interrupt_initVectorTable();

    //将中断服务例程分配给ePWM中断
    Interrupt_register(INT_EPWM1, &epwm1ISR);
    Interrupt_register(INT_DMA_CH5, &dmaCh5ISR);
    Interrupt_register(INT_DMA_CH6, &dmaCh6ISR);

    //配置EPWM和输入X-BAR引脚
    Board_init();

    //禁用同步(冻结时钟到PWM)。GTBCLKSYNC只适用于多核心设备。如果取消注释适用。
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    initDMA();
    initEPWM(myEPWM1_BASE);

    //启用同步和时钟PWM
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //启用ePWM中断
    Interrupt_enable(INT_EPWM1);
    Interrupt_enable(INT_DMA_CH5);
    Interrupt_enable(INT_DMA_CH6);

    //Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    EINT;
    ERTM;

    EALLOW;
    DMA_startChannel(DMA_CH5_BASE);
    DMA_startChannel(DMA_CH6_BASE);

    //空闲循环
    for(;;)
    {

    }

    return 0;
}


//
// DMA setup channels.
//
void initDMA()
{
    // Initialize DMA
    DMA_initController();

    // DMA CH5
    DMA_configAddresses(DMA_CH5_BASE, (uint16_t *)(myEPWM1_BASE + EPWM_O_CMPA),
                        compareConfigs[0]);
    DMA_configBurst(DMA_CH5_BASE, BURST, 1, 1);
    DMA_configTransfer(DMA_CH5_BASE, TRANSFER, 1, 1-BURST);
    DMA_configMode(DMA_CH5_BASE, DMA_TRIGGER_EPWM1SOCA, DMA_CFG_ONESHOT_DISABLE |
                   DMA_CFG_CONTINUOUS_ENABLE | DMA_CFG_SIZE_16BIT);


    // 配置DMA Ch5中断
    DMA_setInterruptMode(DMA_CH5_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH5_BASE);
    DMA_enableTrigger(DMA_CH5_BASE);

    // DMA CH6
    DMA_configAddresses(DMA_CH6_BASE, (uint16_t *)(myEPWM1_BASE + EPWM_O_TBPHS),
                        phasePeriodConfigs[0]);
    DMA_configBurst(DMA_CH6_BASE, BURST, 1, 1);
    DMA_configTransfer(DMA_CH6_BASE, TRANSFER, 1, 1-BURST);
    DMA_configMode(DMA_CH6_BASE, DMA_TRIGGER_EPWM1SOCA, DMA_CFG_ONESHOT_DISABLE |
                   DMA_CFG_CONTINUOUS_ENABLE | DMA_CFG_SIZE_16BIT);

    //配置DMA Ch6中断
    DMA_setInterruptMode(DMA_CH6_BASE, DMA_INT_AT_END);
    DMA_enableInterrupt(DMA_CH6_BASE);
    DMA_enableTrigger(DMA_CH6_BASE);

}


// DMA Ch5 ISR
__interrupt void dmaCh5ISR(void)
{
    DMA_startChannel(DMA_CH5_BASE);

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
    return;
}

// DMA Ch6 ISR
__interrupt void dmaCh6ISR(void)
{
    DMA_startChannel(DMA_CH6_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
    return;
}



// ePWM 1 ISR
__interrupt void epwm1ISR(void)
{
    //
    // Un-comment below to check the status of each register after CTR=0
    //
    // ESTOP0;

    //
    // Clear INT flag for this timer
    //
    EPWM_clearEventTriggerInterruptFlag(myEPWM1_BASE);

    //
    // Acknowledge interrupt group
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}


void initEPWM(uint32_t base)
{
    EPWM_setEmulationMode(base, EPWM_EMULATION_STOP_AFTER_FULL_CYCLE);

    EPWM_setTimeBasePeriod(base, EPWM_TIMER_TBPRD);
    EPWM_setPhaseShift(base, 0U);
    EPWM_setTimeBaseCounter(base, 0U);

    EPWM_setCounterCompareValue(base,
                                EPWM_COUNTER_COMPARE_A,
                                EPWM_TIMER_TBPRD/2);
    EPWM_setCounterCompareValue(base,
                                EPWM_COUNTER_COMPARE_B,
                                EPWM_TIMER_TBPRD/2);

    EPWM_setTimeBaseCounterMode(base, EPWM_COUNTER_MODE_UP);
    EPWM_disablePhaseShiftLoad(base);
    EPWM_setClockPrescaler(base,
                           EPWM_CLOCK_DIVIDER_64,
                           EPWM_HSCLOCK_DIVIDER_1);

    EPWM_setCounterCompareShadowLoadMode(base,
                                         EPWM_COUNTER_COMPARE_A,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);
    EPWM_setCounterCompareShadowLoadMode(base,
                                         EPWM_COUNTER_COMPARE_B,
                                         EPWM_COMP_LOAD_ON_CNTR_ZERO);

    EPWM_setActionQualifierAction(base,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);


    EPWM_setActionQualifierAction(base,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);

    EPWM_setActionQualifierAction(base,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(base,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);


    //
    // Interrupt where we will change the Compare Values
    // Select INT on Time base counter zero event,
    // Enable INT, generate INT on 1st event
    //
	EPWM_setInterruptSource(base, EPWM_INT_TBCTR_ZERO);
	EPWM_enableInterrupt(base);
	EPWM_setInterruptEventCount(base, 1U);

	EPWM_enableADCTrigger(base, EPWM_SOC_A);
	EPWM_setADCTriggerSource(base, EPWM_SOC_A, EPWM_SOC_TBCTR_ZERO);
	EPWM_setADCTriggerEventPrescale(base, EPWM_SOC_A, 1);
	EPWM_clearADCTriggerFlag(base, EPWM_SOC_A);
}

