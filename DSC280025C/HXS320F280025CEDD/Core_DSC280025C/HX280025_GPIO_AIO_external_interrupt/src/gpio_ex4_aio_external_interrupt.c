/******************************************************************
 文 档 名：       HX_DSC280025_GPIO_AIO_external_interrupt
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      GPIO11/GPIO30触发外部中断XINT1/2（AIO引脚触发）
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 (1)采用GPIO30，下降沿输入触发引脚AIO224，触发外部中断XINT1
 (2)采用GPIO11，上升沿输入触发引脚AIO225，触发外部中断XINT2

 连接：GPIO30连接AIO224/A2
 GPIO11连接AIO225/A4

现象：
（1）外部中断XIN1触发一次，GPIO31/LED1闪灯一次
（2）外部中断XINT2触发一次，GPIO34/LED2闪灯一次
 *
 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

uint32_t xint1CountTemp;
uint32_t xint2CountTemp;

//
// Globals
//
volatile uint32_t xint1Count;
volatile uint32_t xint2Count;
uint32_t loopCount;

//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定配置解除*/
    Device_initGPIO();

	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*中断入口地址INT_XINT1,指向执行xint1ISR中断服务程序*/
    Interrupt_register(INT_XINT1, &xint1ISR);
	/*中断入口地址INT_XINT2,指向执行xint2ISR中断服务程序*/
    Interrupt_register(INT_XINT2, &xint2ISR);

    /*中断使能INT_XINT1*/
    Interrupt_enable(INT_XINT1);
    /*中断使能INT_XINT2*/
    Interrupt_enable(INT_XINT2);

    /*打开全局中断*/
    EINT;
    ERTM;

    /*配置GPIO30为IO输出，用于触发外部中断XINT1*/
    GPIO_writePin(30, 1);
    GPIO_setPinConfig(GPIO_30_GPIO30);
    GPIO_setDirectionMode(30, GPIO_DIR_MODE_OUT);
    /*配置GPIO11为IO输出，用于触发外部中断XINT2*/
    GPIO_writePin(11, 0);
    GPIO_setPinConfig(GPIO_11_GPIO11);
    GPIO_setDirectionMode(11, GPIO_DIR_MODE_OUT);

    /*配置GPIO224为数字量AIO输入*/
    GPIO_setPinConfig(GPIO_224_GPIO224);
    GPIO_setAnalogMode(224, GPIO_ANALOG_DISABLED);
    GPIO_setQualificationMode(224, GPIO_QUAL_SYNC);
    /*配置GPIO225为数字量AIO输入*/
    GPIO_setPinConfig(GPIO_225_GPIO225);
    GPIO_setAnalogMode(225, GPIO_ANALOG_DISABLED);
    GPIO_setQualificationMode(225, GPIO_QUAL_6SAMPLE);

    /*配置GPIO225采样周期为510*SYSCLKOUT*/
    GPIO_setQualificationPeriod(225, 510);

    /*配置AIO224为外部中断XINT1*/
    GPIO_setInterruptPin(224, GPIO_INT_XINT1);
    /*配置AIO225为外部中断XINT2*/
    GPIO_setInterruptPin(225, GPIO_INT_XINT2);

    /*配置外部中断GPIO_INT_XINT1-XINT1为GPIO_INT_TYPE_FALLING_EDGE-下降沿触发*/
    GPIO_setInterruptType(GPIO_INT_XINT1, GPIO_INT_TYPE_FALLING_EDGE);
    /*配置外部中断GPIO_INT_XINT2-XINT2为GPIO_INT_TYPE_RISING_EDGE-上升沿触发*/
    GPIO_setInterruptType(GPIO_INT_XINT2, GPIO_INT_TYPE_RISING_EDGE);

    /*使能外部中断XINT1*/
    GPIO_enableInterrupt(GPIO_INT_XINT1);
    /*使能外部中断XINT2*/
    GPIO_enableInterrupt(GPIO_INT_XINT2);

    /*配置GPIO31/34为IO输出，以指示外部中断XINT1/XIN2触发*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    GPIO_setPinConfig(GPIO_34_GPIO34);
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);

    while(1)
    {
        xint1CountTemp = xint1Count;
        xint2CountTemp = xint2Count;

        /*写入GPIO34置高 */
        GPIO_writePin(34, 1);
        /*写入GPIO30置低，以触发外部中断XINT1 */
        GPIO_writePin(30, 0);

        /*等待外部中断XINT1触发*/
        while(xint1Count == xint1CountTemp)
        {;}

        /*写入GPIO34置高 */
        GPIO_writePin(34, 1);
        /*等待6个采样周期，GPIO225采样周期延时 */
        DEVICE_DELAY_US(DELAY);
        /*写入GPIO11置高，以触发外部中断XINT2 */
        GPIO_writePin(11, 1);

        /*等待外部中断XINT2触发*/
        while(xint2Count == xint2CountTemp)
        {;}


        /*若外部中断XINT1与XINT2均触发完成后，重新形成新的下降/上升沿触发*/
        if((xint1Count == (xint1CountTemp + 1)) &&
           (xint2Count == (xint2CountTemp + 1)))
        {
            loopCount++;
            /*写入GPIO30置高，以准备触发外部中断XINT1 */
            GPIO_writePin(30, 1);
            /*写入GPIO11置低，以准备触发外部中断XINT2 */
            GPIO_writePin(11, 0);
        }
        else
        {
        }
    }
}

