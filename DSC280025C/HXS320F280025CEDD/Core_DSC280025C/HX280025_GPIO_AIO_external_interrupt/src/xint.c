#include "system.h"



__interrupt void xint1ISR(void)
{
	/*GPIO31翻转闪灯，以指示外部中断XINT1触发*/
    GPIO_togglePin(31);
    xint1Count++;

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}


__interrupt void xint2ISR(void)
{
	/*GPIO34翻转闪灯，以指示外部中断XINT2触发*/
    GPIO_togglePin(34);
    xint2Count++;

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}
