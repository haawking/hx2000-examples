/******************************************************************
 文 档 名：       HX_DSC280025_ADC_ppb_offset
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC顺序采样与后处理
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用软件触发ADC顺序采用SOC0与SOC1，
 经后处理PPB模块偏移校准输出

连接：A2，C2连接一可调的电位器

 现象:
 （1）A2上所读出的采样结果与电位器两端电压一致
 即Vo=myADC0Result/4096*3.3V
经后处理的结果myADC0PPBResult更接近于其电压值
（2）C2上所读出的采样结果与电位器两端电压一致
 即Vo=myADC1Result/4096*3.3V
经后处理的结果myADC1PPBResult更接近于其电压值

 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/
#include "system.h"

//
// Globals
//
uint16_t myADC0Result[3];
uint16_t adc_A2Result;

uint16_t myADC0PPBResult;
uint16_t myADC1Result[3];
uint16_t adc_C2Result;

uint16_t myADC1PPBResult;

uint32_t i;

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定解除*/
    Device_initGPIO();
    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    AdcA_config();
    AdcC_config();
    EDIS;

    /*打开全局中断*/
    EINT;
    ERTM;

    do
    {
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER0-SOC0*/
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER1-SOC1*/
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER1);

        /*等待ADC中断状态退出ADC_INT_NUMBER1-ADCINT1中断，即完成转换*/
        while(ADC_getInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1) == false);
        /*清除ADC中断：ADC_INT_NUMBER1-ADCINT1*/
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

      	/*强制转换ADC_SOC:ADC_SOC_NUMBER0-SOC0*/
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER0);
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER1-SOC1*/
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER1);


        /*等待ADC中断状态退出ADC_INT_NUMBER1-ADCINT1中断，即完成转换*/
        while(ADC_getInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1) == false);
        /*清除ADC中断：ADC_INT_NUMBER1-ADCINT1*/
        ADC_clearInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1);

        /*读取ADC_SOC初始结果：ADC_SOC_NUMBER0-SOC0*/
        for(i=0;i<3;i++)
        {
        	myADC0Result[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        	if(i>=2)
        	{
        		adc_A2Result=myADC0Result[i];
        	}
        }

        /*读取ADC_SOC后处理PPB处理结果：ADC_PPB_NUMBER1-PPB1*/
        myADC0PPBResult = ADC_readPPBResult(ADCARESULT_BASE, ADC_PPB_NUMBER1);
        /*读取ADC_SOC初始结果：ADC_SOC_NUMBER0-SOC0*/
        for(i=0;i<3;i++)
        {
        	myADC1Result[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
        	if(i>=2)
        	{
        		adc_C2Result=myADC1Result[i];
        	}
        }
        /*读取ADC_SOC后处理PPB处理结果：ADC_PPB_NUMBER1-PPB1*/
        myADC1PPBResult = ADC_readPPBResult(ADCCRESULT_BASE, ADC_PPB_NUMBER1);
    }
    while(1);
}

//
// End of file
//
