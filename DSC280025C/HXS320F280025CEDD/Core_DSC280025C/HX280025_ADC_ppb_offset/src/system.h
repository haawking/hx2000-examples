/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Globals
//
extern uint16_t myADC0Result[3];
extern uint16_t myADC0PPBResult;
extern uint16_t myADC1Result[3];
extern uint16_t myADC1PPBResult;


/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void AdcA_config(void);
void AdcC_config(void);


#endif /* SYSTEM_H_ */
