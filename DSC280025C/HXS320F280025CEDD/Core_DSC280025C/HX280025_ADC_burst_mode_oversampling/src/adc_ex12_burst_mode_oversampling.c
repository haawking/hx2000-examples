/******************************************************************
 文 档 名：       HX_DSC280025_ADC_burst_mode_oversampling
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC突发模式平均值采样
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：（1）采用PWM SOCA在向上计数CTR=CMPA时,
 A.通过A0/A1通道采用突发模式采样输入电压
 B.A2通道采用SOC12-SOC15平均值采样实现

 现象:
通过A0/A1/A2通道连接相应电位器元件,
可读出正确的电压值Vi=adcAResultx/4096*3.3V

 版 本：      V1.0.0
 时 间：      2024年10月25日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"


//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置INT_ADCA1中断入口地址，指向执行adcA1ISR中断服务程序*/
	Interrupt_register(INT_ADCA1, &adcA1ISR);
    EDIS;

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();
    EDIS;

    /*EPWM模块配置*/
    initEPWM();

	/*中断使能INT_ADCA1*/
	Interrupt_enable(INT_ADCA1);

    /*打开全局中断*/
    EINT;
    ERTM;


    /*清零中断计数：保证求平均值采样数量为4*/
    isrCount = 0;

    /*使能EPWM的SOCA触发*/
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    /*配置EPWM计数模式：EPWM_COUNTER_MODE_UP-向上计数*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    do
    {}
    while(1);
}




//
// End of file
//
