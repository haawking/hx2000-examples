#include "system.h"

void InitAdcAio(void)
{
	/*配置GPIO231为模拟量输入A0/C15*/
	GPIO_setPinConfig(GPIO_231_GPIO231);
	/*配置GPIO231模拟量输入*/
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);

	/*配置GPIO232为模拟量输入A1*/
	GPIO_setPinConfig(GPIO_232_GPIO232);
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);
	/*配置GPIO230为模拟量输入A10/C10*/
	GPIO_setPinConfig(GPIO_230_GPIO230);
	GPIO_setAnalogMode(230, GPIO_ANALOG_ENABLED);
	/*配置GPIO237为模拟量输入A11/C0*/
	GPIO_setPinConfig(GPIO_237_GPIO237);
	GPIO_setAnalogMode(237, GPIO_ANALOG_ENABLED);
	/*配置GPIO238为模拟量输入A12/C1*/
	GPIO_setPinConfig(GPIO_238_GPIO238);
	GPIO_setAnalogMode(238, GPIO_ANALOG_ENABLED);
	/*配置GPIO239为模拟量输入A14/C4*/
	GPIO_setPinConfig(GPIO_239_GPIO239);
	GPIO_setAnalogMode(239, GPIO_ANALOG_ENABLED);
	/*配置GPIO233为模拟量输入A15/C7*/
	GPIO_setPinConfig(GPIO_233_GPIO233);
	GPIO_setAnalogMode(233, GPIO_ANALOG_ENABLED);
	/*配置GPIO224为模拟量输入A2/C9*/
	GPIO_setPinConfig(GPIO_224_GPIO224);
	GPIO_setAnalogMode(224, GPIO_ANALOG_ENABLED);
	/*配置GPIO242为模拟量输入A3/C5/VDAC*/
	GPIO_setPinConfig(GPIO_242_GPIO242);
	GPIO_setAnalogMode(242, GPIO_ANALOG_ENABLED);
	/*配置GPIO225为模拟量输入A4/C14*/
	GPIO_setPinConfig(GPIO_225_GPIO225);
	GPIO_setAnalogMode(225, GPIO_ANALOG_ENABLED);
	/*配置GPIO244为模拟量输入A5/C2*/
	GPIO_setPinConfig(GPIO_244_GPIO244);
	GPIO_setAnalogMode(244, GPIO_ANALOG_ENABLED);
	/*配置GPIO228为模拟量输入A6*/
	GPIO_setPinConfig(GPIO_228_GPIO228);
	GPIO_setAnalogMode(228, GPIO_ANALOG_ENABLED);
	/*配置GPIO245为模拟量输入A7/C3*/
	GPIO_setPinConfig(GPIO_245_GPIO245);
	GPIO_setAnalogMode(245, GPIO_ANALOG_ENABLED);
	/*配置GPIO241为模拟量输入A8/C11*/
	GPIO_setPinConfig(GPIO_241_GPIO241);
	GPIO_setAnalogMode(241, GPIO_ANALOG_ENABLED);
	/*配置GPIO227为模拟量输入A9/C8*/
	GPIO_setPinConfig(GPIO_227_GPIO227);
	GPIO_setAnalogMode(227, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_226_GPIO226);
	GPIO_setAnalogMode(226, GPIO_ANALOG_ENABLED);
}


void InitAdc(void)
{
	/*屏蔽ADC温度传感器功能*/
	ASysCtl_disableTemperatureSensor();
	/*ADC参考电压配置为:ADC_REFERENCE_INTERNAL-内部参考电压,
	* ADC_REFERENCE_3_3V-3.3V*/
	ADC_setVREF(ADCA_BASE,ADC_REFERENCE_INTERNAL,ADC_REFERENCE_VDDA);
}


void Adc_config(void)
{
	/*配置ADC时钟为ADC_CLK_DIV_8_0-8分频对应SYSCLK/8=20M*/
	ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_8_0);
	/*配置ADC中断脉冲模式：ADC_PULSE_END_OF_CONV-转换完成产生脉冲触发中断*/
	ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
	/*ADC上电使能转换*/
	ADC_enableConverter(ADCA_BASE);
	/*延时1ms完成ADC模块上电*/
	DEVICE_DELAY_US(5000);

	/*使能ADC突发模式*/
	ADC_enableBurstMode(ADCA_BASE);
	/*使能ADC突发数量：1个（最大可达16个）*/
	ADC_setBurstModeConfig(ADCA_BASE, ADC_TRIGGER_EPWM1_SOCA, 1);

	/*配置ADC_SOC转换优先级：
	 * ADC_PRI_THRU_SOC6_HIPRI-以SOC11为最高优先级*/
	ADC_setSOCPriority(ADCA_BASE, ADC_PRI_THRU_SOC11_HIPRI);

	/*配置ADC采样：ADC_SOC_NUMBER0-SOC0，
	 * ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
	 * ADC_CH_ADCIN0-采用A0通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER0-SOC0
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER1-SOC1，
	 * ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
	 * ADC_CH_ADCIN1-采用A1通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER1-SOC1
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER12-SOC12，
	 * ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER12,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER12-SOC12
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER12,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER13-SOC13，
	 * ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER13,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER13-SOC13
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER13,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER14-SOC14，
	 * ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER14,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER14-SOC14
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER14,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER15-SOC15，
	 * ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER15,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER15-SOC15
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER15,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置SOC中断触源：ADC_INT_NUMBER1-ADCINT1
	 * 通过ADC_SOC_NUMBER1-SOC1触发*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1,
			ADC_SOC_NUMBER1);
	/*使能ADC中断：ADC_INT_NUMBER1-ADCINT1*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
	/*清ADC中断状态：ADC_INT_NUMBER1-ADCINT1*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
	/*关闭ADC连续模式：ADC_INT_NUMBER1-ADCINT1*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);
}

//
// Globals
//读取结果
uint16_t adcAResult0[3];
uint16_t adcAResult1[3];
uint16_t adcAResult2[3];
uint16_t adcAResult3[3];
uint16_t adcAResult4[3];
uint16_t adcAResult5[3];
//结果取出
uint16_t adc_result12;
uint16_t adc_result13;
uint16_t adc_result14;
uint16_t adc_result15;

uint16_t adc_A0result;
uint16_t adc_A1result;
uint16_t adc_A2result;
volatile uint16_t isrCount;

uint16_t i;

__interrupt void adcA1ISR(void)
{
	/*读取ADCIN0-A0通道的SOC0采样结果*/
	for(i=0;i<3;i++)
	{
		adcAResult0[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
		if(i>=2)
		{
			adc_A0result=adcAResult0[i];
		}
	}

	/*读取ADCIN1-A1通道的SOC1采样结果*/
	for(i=0;i<3;i++)
	{
		adcAResult1[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
		if(i>=2)
		{
			adc_A1result=adcAResult1[i];
		}
	}


    /*读取ADCINA2通道采样结果*/
	for(i=0;i<3;i++)
	{
		adcAResult2[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER12);
		if(i>=2)
		{
			adc_result12=adcAResult2[i];
		}
	}

	for(i=0;i<3;i++)
	{
		adcAResult3[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER13);
		if(i>=2)
		{
			adc_result13=adcAResult3[i];
		}
	}

	for(i=0;i<3;i++)
	{
		adcAResult4[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14);
		if(i>=2)
		{
			adc_result14=adcAResult4[i];
		}
	}

	for(i=0;i<3;i++)
	{
		adcAResult5[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER15);
		if(i>=2)
		{
			adc_result15=adcAResult5[i];
		}
	}
   /*使用SOC12-SOC15进行平均值采样实现*/
	adc_A2result =(adc_result12+adc_result13
			+adc_result14+adc_result15) >> 2;

    /*清除中断标志：ADC_INT_NUMBER1-ADCINT1中断*/
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    /*检测ADCINTx中断溢出，若溢出则清除中断标志*/
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    /*清除中断应答PIEACK*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

