#include "system.h"

void configureEPWM(uint32_t epwmBase)
{
	/*关闭ADC触发：EPWM_SOC_A-EPWM_SOCA*/
    EPWM_disableADCTrigger(epwmBase, EPWM_SOC_A);
	/*ADC触发时刻：EPWM_SOC_A-SOCA在EPWM_SOC_TBCTR_U_CMPA-向上计数CTR=CMPA时触发*/
    EPWM_setADCTriggerSource(epwmBase, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    /*配置ADC触发事件分频=EPWM_TBCLK/1*/
    EPWM_setADCTriggerEventPrescale(epwmBase, EPWM_SOC_A, 1U);

    /*配置EPWM比较点：CMPA=2048-对应脉宽50%*/
    EPWM_setCounterCompareValue(epwmBase, EPWM_COUNTER_COMPARE_A, 2048U);

    /*配置EPWM周期=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)=39kHz*/
    EPWM_setTimeBasePeriod(epwmBase, 4096U);

    /*配置EPWM时基计数模式：EPWM_COUNTER_MODE_STOP_FREEZE-冻结计数器*/
    EPWM_setTimeBaseCounterMode(epwmBase, EPWM_COUNTER_MODE_STOP_FREEZE);
}
