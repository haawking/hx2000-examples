/*
 * system.h
 *
 *  Created on: 2024年10月25日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define RESULTS_BUFFER_SIZE     256

extern volatile uint16 bufferFull;                // Flag to indicate buffer is full
extern uint16_t result;                              // Index into result buffer
extern uint16_t myADC0Results[RESULTS_BUFFER_SIZE];   // Buffer for results
//
// Function Prototypes
//
void initEPWM(void);

__interrupt void adcA1ISR(void);

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);

#endif /* SYSTEM_H_ */
