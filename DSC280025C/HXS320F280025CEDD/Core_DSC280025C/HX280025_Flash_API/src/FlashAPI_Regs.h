#ifndef __FLASH_API_REGS_H_
#define __FLASH_API_REGS_H_

#include "driverlib.h"

#ifndef HWREG
#define HWREG(x)                			(*((volatile Uint32 *)((x))))
#endif

#ifndef FLASH0CTRL_BASE
	#define FLASH0CTRL_BASE           	0x007AF000U
#endif

#ifndef FLASH_O_FT100N
	#define FLASH_O_FT100N          0x80U    // 100ns Control Word
	#define FLASH_O_FT4U            0x84U    // 4us Control Word
	#define FLASH_O_FT5U            0x88U    // 5us Control Word
	#define FLASH_O_FT9U            0x8CU    // 9us Control Word
	#define FLASH_O_FT20U           0x90U    // 20us Control Word
	#define FLASH_O_FT25U           0x94U    // 25us Control Word
	#define FLASH_O_FT50U           0x98U    // 50us Control Word
	#define FLASH_O_FT900U          0x9CU    // 900us Control Word
	#define FLASH_O_FT4M            0xA0U    // 3600us Control Word
	#define FLASH_O_FT9M            0xA4U    // 9ms Control Word
#endif

#ifndef FLASH_O_FMERCTRL
#define FLASH_O_FMERCTRL        		0x10CU   // FLASH Memrory ERASE
#endif

#ifndef FLASH_O_FPERCTRL
#define FLASH_O_FPERCTRL        		0x110U   // FLASH Memrory Sector ERASE
#endif

#ifndef FLASH_O_WR
#define FLASH_O_WR              		0x11CU   // Flash write enable
#endif

#ifndef DCSMBANK0_Z1_BASE
	#define DCSMBANK0_Z1_BASE         	0x0039400U
	#define DCSMBANK0_Z2_BASE         	0x0039450U
	#define DCSMCOMMON_BASE     		0x00394A0U
#endif

#ifndef DCSM_O_Z1_CR
	#define DCSM_O_Z1_CR             	0x30U   // Zone 1 CSM Control Register
	#define DCSM_O_Z2_CR          		0x30U   // Zone 2 CSM Control Register
#endif

#ifndef DCSM_Z1_CR_UNSECURE
	#define DCSM_Z1_CR_UNSECURE   		0x20U     // CSMPSWD Match CSMKEY
	#define DCSM_Z2_CR_UNSECURE   		0x20U     // CSMPSWD Match CSMKEY
#endif

#ifndef DCSM_O_B0_SECTSTAT
	#define DCSM_O_B0_SECTSTAT   		0x4U    // Flash BANK0 Sectors Status Register
#endif

#endif
