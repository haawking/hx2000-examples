#include "driverlib.h"
#include "FlashAPI_Regs.h"
#include "FlashAPI.h"

// 函数名  ：Fapi_GetSectorIndexFromAddress
// 功能说明：将Flash范围内的任意地址，转换为该地址所在的物理Sector索引
// 参数说明：
//           addr - Uint32，输入参数，要查询的绝对地址，可用范围为0x600000~0x63FFFF
// 返回值  ：
//           SECTOR_INDEX_INVALID - 0xFF，查询的地址不在Flash中
//           0~127，查询的地址对应的扇区索引号
static __always_inline Uint32 Fapi_GetSectorIndexFromAddress(Uint32 addr)
{
	if(addr < FLASH_B0_START_ADDR)
	{
		return SECTOR_INDEX_INVALID;
	}
	else if(addr >= (FLASH_B0_START_ADDR + FLASH_BANK_SIZE))
	{
		return SECTOR_INDEX_INVALID;
	}
	else
	{
		return ((addr - FLASH_B0_START_ADDR) / FLASH_SECTOR_SIZE);
	}
}


// This function can get the zone lock status
static __always_inline Uint32 Fapi_CheckZoneLockStatus(Uint32 zone)
{
	Uint32 ret = DCSM_STATUS_LOCK;
	Uint32 status = 0xFFFFFFFF;

	if(0b01 == zone)
	{
		status = HWREG(DCSMBANK0_Z1_BASE + DCSM_O_Z1_CR);
	    // if ARMED bit is set and UNSECURED bit is set then CSM is unsecured. Else it is secure.
	    if( (status & DCSM_Z1_CR_UNSECURE) != 0U )
	    {
	    	ret = DCSM_STATUS_UNLOCK;
	    }
	}
	else if(0b10 == zone)
	{
		status = HWREG(DCSMBANK0_Z2_BASE + DCSM_O_Z2_CR);
	    // if ARMED bit is set and UNSECURED bit is set then CSM is unsecured. Else it is secure.
	    if((status & DCSM_Z2_CR_UNSECURE) != 0U)
	    {
	    	ret = DCSM_STATUS_UNLOCK;
	    }
	}

	return ret;
}

static __always_inline Uint32 Fapi_PhySector2LogicSector(Uint32 sectorIndex)
{
	Uint32 Phy2LogicMap[LOGIC_SECTOR_COUNT][2] =
	{
		// bank 0
		{0,  8 }, {9,  16}, {17, 24}, {25, 32}, {33, 40 }, {41, 48 }, {49, 56 }, {57, 64 },
		{65, 72}, {73, 80}, {81, 88}, {89, 96}, {97, 104}, {105,112}, {113,120}, {121,128},
	};

	Uint32 loop;
	Uint32 ret = 0;

	for(loop = 0; loop < LOGIC_SECTOR_COUNT; loop++)
	{
		if((sectorIndex >= Phy2LogicMap[loop][0]) && (sectorIndex <= Phy2LogicMap[loop][1]))
		{
			ret = loop;
			break;
		}
	}

	return ret;
}

/***
 * if the return value is 00 represents the sector is inaccessible
 *                        01                          zone1
 *                        10                          zone2
 *                        11                          full accessible
 */
static __always_inline Uint32 Fapi_CheckFlashSectorZoneStatus(Uint32 SectorIdx)
{
	Uint32 LogicSector = 0;
	Uint32 SectorStatus = 0;
	uint32_t shift;

	if (SectorIdx >= FLASH_SECTOR_COUNT)
	{
		return RET_SECTOR_ERROR;
	}

	// 转换逻辑扇区号
	LogicSector = Fapi_PhySector2LogicSector(SectorIdx);
	SectorStatus = HWREG(DCSMCOMMON_BASE + DCSM_O_B0_SECTSTAT);
	shift = (uint32_t)LogicSector * 2U;

	// 通过DCSM COMMON SECTSTAT确定Sector所属Zone
	SectorStatus = ((SectorStatus >> shift) & 0x3U);

	return SectorStatus;
}

// Get the sector lock status according to the sector index
// Each sector status may be in-accessible (00), unsecure(11), or belong to zone1 & 2 (01 | 10),
// then calling the Fapi_CheckZoneLockStatus() function to get the zone lock status
// If the return value is SECTOR_LOCKED represents the sector is locked
static __always_inline Uint32 Fapi_CheckSectorLockStatus(Uint32 sectorIdx)
{
	Uint32 sectorStatus 	= 0xFFFFFFFF;
	// get the Sector status (0b00 == inaccessible，0b01|0b10 == zone1|zone2，0b11 == unsecurity)
	sectorStatus = Fapi_CheckFlashSectorZoneStatus( sectorIdx );

	//判断Sector在DCSM中的配置及权限
	if(0b00 == sectorStatus)
	{
		// 0b00 indicates the sector is in-accessable
		return DCSM_STATUS_LOCK;
	}
	else if(0b11 == sectorStatus)
	{
		// 0b11 indicates the sector is insecurity
		return DCSM_STATUS_UNLOCK;
	}
	else
	{
		// now the parameter 'sectorStatus' indicates the zone number
		return Fapi_CheckZoneLockStatus(sectorStatus);
	}

}
