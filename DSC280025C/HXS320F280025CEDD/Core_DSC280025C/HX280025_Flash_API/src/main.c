/******************************************************************
 文 档 名：      HX_DSC280025_Flash_API
 开 发 环 境：   Haawking IDE V2.3.8Pre
 开 发 板：      Core_DSC280025_V1.0
 D S P：         DSC280025
 使 用 库：
 作     用：     调用ROM中的Flash API实现对片内的flash的读写擦除等操作
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz

 连接方式：

 现象：       读写正确结束的时候，LED1亮， 否则，核心板上LED1闪烁
                  在调试时，可以使用Memory视图填入Flash的地址(比如Sector 124的地址0x63E000)，
                  观察Flash中的内容。。

 版 本：      V1.0.1
 时 间：      2025年2月14日
 作 者：      liyuyao
 @ mail：     support@mail.haawking.com
 ******************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "FlashAPI.h"

#define BUFF_LEN					8						// Buffer的大小，是32bit数据的个数
#define BUFF_BYTE_NUM				(BUFF_LEN * 4)			// Buffer的字节数，是8bit数据的个数
//#define FLASH_BASE_ADDR				0x600000				// Flash Bank0的起始地址
//#define FLASH_SECTOR_SIZE			2048					// 一个Sector的大小，单位为Byte
#define TEST_SECTOR_START			120
#define TEST_SECTOR_END				128

// 向Flash中写入数据，每次写一个Buffer 大小的数据，写完一个Sector的循环次数
#define SECTOR_PROG_LOOP		(FLASH_SECTOR_SIZE/BUFF_BYTE_NUM)

uint16 Erase_result,Program_result;
uint32 program_buffer[BUFF_LEN];
uint32 i,j;
uint32 err_cnt;

void CODE_SECTION("ramfuncs")
Success(void)
{
	GPIO_writePin(6,0);			// LED1 ON
	GPIO_writePin(5,1);			// LED2 OFF
}

void CODE_SECTION("ramfuncs")
Fail(void)
{
	while(1)
	{
		GPIO_writePin(31,1);
		GPIO_writePin(34,1);
		for(i = 0; i < 100; i++)
			for(j = 0; j < 16000; j++);

		GPIO_writePin(31,0);
		GPIO_writePin(34,0);
		for(i = 0; i < 100; i++)
			for(j = 0; j < 16000; j++);
	}
}


//
// 因为需要操作芯片内部的Flash，需要将部分的代码存放在芯片的RAM中
//
void CODE_SECTION("ramfuncs")
Flash_API_test1()
{
	// 初始化Flash API， DSP的运行频率为160MHz
	Fapi_Initialize(160);

	/*   擦除第120到127扇区
	 *  HXS320F280025C总共有128个Sector，每个Sector的大小是2048 Byte
	 *  Flash Addr的范围为0x600000~0x63FFFF
	 *  扇区120的起始地址是0x63C000
	 * */
	for(i = TEST_SECTOR_START;i < TEST_SECTOR_END;i++)
	{
		Erase_result = Fapi_SectorErase(i);
	    if(Erase_result != 0)
		{
	    	Fail();
		}
	}
	for(i = 0;i<BUFF_LEN;i++)
	{
		program_buffer[i] = 0x55AA1234 + i;
	}
	/*  写Buffer[LEN]到第120到127扇区,
	 * Flash280025_Program()写入的时候，地址需要32 Byte对齐，一次写入32个Byte
	 * 因此，写满一个2k Byte大小的Sector，需要调用64次Flash280025_Program()
	 * 120扇区的偏移地址是120 * 2048 = 245760（0x3C000）
	 * Flash280025_Program
	 *  */
	for(i = TEST_SECTOR_START;i < TEST_SECTOR_END;i++)
	{
		for(j = 0;j < SECTOR_PROG_LOOP;j++)
		{
			//每次编程，地址必须0x20对齐，长度必须是0x8的倍数
			   Program_result = Fapi_SectorProgram		\
					   ((FLASH_B0_START_ADDR + FLASH_SECTOR_SIZE*i + 32*j),program_buffer,BUFF_LEN);
		       if(Program_result != 0)
		       {
		    	  Fail();
		       }
		}
	}

    Success();
}
void GPIO_init()
{
	/*配置GPIO6为IO输出*/
    GPIO_setPinConfig(GPIO_6_GPIO6);
    GPIO_setDirectionMode(6, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(6, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(6, GPIO_QUAL_SYNC);

	/*配置GPIO5为IO输出*/
    GPIO_setPinConfig(GPIO_5_GPIO5);
    GPIO_setDirectionMode(5, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(5, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(5, GPIO_QUAL_SYNC);
}
int main(void)
{
    Device_init();
    GPIO_init();

    Flash_API_test1();

   while(1);
   //return 0;
}

//
// End of File
//
