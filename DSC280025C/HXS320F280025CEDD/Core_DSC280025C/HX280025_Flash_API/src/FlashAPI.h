#ifndef __FLASH_API_H_
#define __FLASH_API_H_

#include "driverlib.h"

/* Flash information */
#define SECTOR_INDEX_INVALID				(0xFFFFFFFFU)
#define LOGIC_SECTOR_COUNT					(32)
#define BANK_SECTOR_COUNT					(128UL)
#define FLASH_SECTOR_COUNT 					(BANK_SECTOR_COUNT)
#define FLASH_SECTOR_SIZE					(0x800)
#define FLASH_B0_START_ADDR					(0x600000UL)
#define FLASH_BANK_SIZE						(0x40000UL)
#define FLASH_FULL_SIZE						(FLASH_SECTOR_COUNT * FLASH_SECTOR_SIZE)
#define FLASH_END_ADDRESS					(FLASH_B0_START_ADDR + FLASH_FULL_SIZE - 1)
#define FLASH_PROGRAM_ALIGN_BYTES			(0x20UL)
#define FLASH_PROGRAM_ALIGN_WORDS			(FLASH_PROGRAM_ALIGN_BYTES / 4)

typedef enum{
	BANK_0 = 0,
	BANK_1,
	BANK_MAX
}FAPI_BANK_INDEX;

typedef enum{
	DCSM_STATUS_UNLOCK = 0,
	DCSM_STATUS_LOCK
}FAPI_DCSM_STATUS;

typedef enum {
	RET_NO_ERROR = 0,
	RET_DCSM_LOCK = 0xE0,
	RET_PTR_NULL = 0xE1,
	RET_ADDRESS_ERROR = 0xE2,
	RET_ALIGN_ERROR = 0xE3,
	RET_LENGTH_ERROR = 0xE4,
	RET_SECTOR_ERROR = 0xE5,
	RET_VERIFY_ERROR = 0xE6,
	RET_PRECHECK_ERROR = 0xE7,
	RET_ERASE_ERROR = 0xE8,
	RET_PROGRAM_ERROR = 0xE9,
	RET_FREQ_ERROR = 0xEA,
	RET_BANK_ERROR = 0xEB,
	RET_CROSS_SECTOR = 0xEC
}FAPI_RET_STATUS;


// 函数名  ：Fapi_Initialize
// 功能说明：根据系统时钟频率（单位MHz），配置对应的Flash时序控制寄存器
//           在擦除/编程之前，必须运行初始化函数
//           在擦除/编程之后，需要将时序寄存器配置为0
// 参数说明：u32HclkFrequency - Uint32，系统运行频率，小数需要向上取整，可配置范围0~160
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_FREQ_ERROR - 0xEA，配置的频率超范围
extern Uint32 Fapi_Initialize(Uint32 u32HclkFrequency);

// 函数名  ：Fapi_Verify
// 功能说明：指定Flash中的起始地址，校验与给定的数组指针中的数据是否一致
//           用于校验编程结果，用户也可以自由调用
//           由于此函数不限制Bank、Sector范围，所以需要DCSM Zone1/Zone2全部解密
// 参数说明：
//           addr - Uint32，输入参数，Flash中数据的起始地址，可用范围为0x600000~0x63FFFF，且0x4对齐
//           data - Uint32 *，输入参数，校验对比数据的起始地址
//           length - Uint32，输入参数，对比的Uint32数据个数，必须大于0，且结尾的地址在Flash地址范围内
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，DCSM Zone1/Zone2未解密
//           RET_PTR_NULL - 0xE1，指针为空
//           RET_ADDRESS_ERROR - 0xE2，起始地址或结束地址错误
//           RET_LENGTH_ERROR - 0xE4，长度为0
//           RET_VERIFY_ERROR - 0xE6，数据校验不一致
extern Uint32 Fapi_Verify(Uint32 addr, Uint32 *data, Uint32 length);

// 函数名  ：Fapi_EraseVerify
// 功能说明：指定Flash中的起始地址，校验是否为0xFFFFFFFF
//           用于编程前校验数据或验证擦除结果，用户也可以自由调用
//           由于此函数不限制Bank、Sector范围，所以需要DCSM Zone1/Zone2全部解密
// 参数说明：
//           addr - Uint32，输入参数，Flash中数据的起始地址，可用范围为0x600000~0x63FFFF，且0x4对齐
//           length - Uint32，输入参数，对比的Uint32数据个数，必须大于0，且结尾的地址在Flash地址范围内
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，DCSM Zone1/Zone2未解密
//           RET_ADDRESS_ERROR - 0xE2，起始地址或结束地址错误
//           RET_LENGTH_ERROR - 0xE4，长度为0
//           RET_VERIFY_ERROR - 0xE6，数据校验不一致
extern Uint32 Fapi_EraseVerify(Uint32 addr, Uint32 length);

// 函数名  ：Fapi_MassErase
// 功能说明：执行Flash全片擦除
//           此函数需要DCSM Zone1/Zone2全部解密，若无法保证全部解密，请使用SectorErase函数
// 参数说明：无
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，DCSM Zone1/Zone2未解密
//           RET_ERASE_ERROR - 0xE8，擦除失败
extern Uint32 Fapi_MassErase(void);

// 函数名  ：Fapi_SectorErase
// 功能说明：执行Flash扇区擦除，此扇区为物理扇区，每个扇区占用0x800地址
// 参数说明：
//           sectorIndex - Uint32，输入参数，可选扇区范围0~128
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，DCSM Zone1/Zone2未解密
//           RET_SECTOR_ERROR - 0xE5，扇区号错误
//           RET_ERASE_ERROR - 0xE8，擦除失败
extern Uint32 Fapi_SectorErase(Uint32 sectorIndex);

// 函数名  ：Fapi_SectorProgram
// 功能说明：单Sector内编程函数
//           Flash需要0x20地址对齐编程，编程的数据长度也需要0x20对齐，即length为8的倍数，0x20内的数据不允许编程2次
//           编程前需要确保本次编程的范围内，所有地址为0xFFFFFFFF
//           如果有非0x20对齐编程数据的需求，需要应用自行保存、拼接
//           此函数限制编程地址在同一个逻辑Sector范围（1逻辑Sector=8个物理Sector，长度0x4000字节）
//           此函数不需要DCSM Zone1/Zone2全部解密，Sector所在Zone解密即可
// 参数说明：
//           addr - Uint32，输入参数，Flash中数据的起始地址，可用范围为0x600000~0x63FFFF，且0x20对齐
//           data - Uint32 *，输入参数，编程数据的起始地址
//           length - Uint32，输入参数，对比的Uint32数据个数，必须大于0，为8的倍数，且结尾的地址在与起始地址在相同的逻辑Sector内
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，Sector所在DCSM Zone未解密
//           RET_PTR_NULL - 0xE1，指针为空
//           RET_ADDRESS_ERROR - 0xE2，起始地址或结束地址错误
//           RET_ALIGN_ERROR - 0xE3，起始地址不为0x12对齐或length不为8的倍数
//           RET_LENGTH_ERROR - 0xE4，长度为0
//           RET_VERIFY_ERROR - 0xE6，数据校验不一致
//           RET_PRECHECK_ERROR - 0xE7，编程前数据不为0xFFFFFFFF
//           RET_PROGRAM_ERROR - 0xE9，编程失败
//           RET_CROSS_SECTOR - 0xEC，编程地址跨越逻辑扇区（0x4000对齐）
extern Uint32 Fapi_SectorProgram(Uint32 addr, Uint32 *data, Uint32 length);

// 函数名  ：Fapi_Program
// 功能说明：基础的不限制Flash地址的编程函数
//           Flash需要0x20地址对齐编程，编程的数据长度也需要0x20对齐，即length为8的倍数，0x20内的数据不允许编程2次
//           编程前需要确保本次编程的范围内，所有地址为0xFFFFFFFF
//           如果有非0x20对齐编程数据的需求，需要应用自行保存、拼接
//           由于此函数不限制Bank、Sector范围，所以需要DCSM Zone1/Zone2全部解密
//           若无法保证全部解密，请使用SectorProgram函数
// 参数说明：
//           addr - Uint32，输入参数，Flash中数据的起始地址，可用范围为0x600000~0x63FFFF，且0x20对齐
//           data - Uint32 *，输入参数，编程数据的起始地址
//           length - Uint32，输入参数，对比的Uint32数据个数，必须大于0，为8的倍数，且结尾的地址在Flash地址范围内
// 返回值  ：
//           RET_NO_ERROR - 0x0，运行正常
//           RET_DCSM_LOCK - 0xE0，DCSM Zone1/Zone2未解密
//           RET_PTR_NULL - 0xE1，指针为空
//           RET_ADDRESS_ERROR - 0xE2，起始地址或结束地址错误
//           RET_ALIGN_ERROR - 0xE3，起始地址不为0x20对齐或length不为8的倍数
//           RET_LENGTH_ERROR - 0xE4，长度为0
//           RET_VERIFY_ERROR - 0xE6，数据校验不一致
//           RET_PRECHECK_ERROR - 0xE7，编程前数据不为0xFFFFFFFF
//           RET_PROGRAM_ERROR - 0xE9，编程失败
extern Uint32 Fapi_Program(Uint32 addr, Uint32 *data, Uint32 length);

// 函数名  ：Fapi_GetVersion
// 功能说明：获取当前FlashAPI的软件版本
// 参数说明：无
// 返回值  ：Uint32，版本号，0xaabbcc格式，表示aa.bb.cc版本
extern Uint32 Fapi_GetVersion(void);

#endif
