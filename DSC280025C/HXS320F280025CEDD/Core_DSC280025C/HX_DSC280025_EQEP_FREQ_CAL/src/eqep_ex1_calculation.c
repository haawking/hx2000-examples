/******************************************************************
 文 档 名：     eqep_ex1_calculation.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供EQEP脉冲捕获频率计算
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "eqep_ex1_calculation.h"
#include "system.h"

//
// FreqCal_calculate - Function to calculate the frequency of the input signal using both the unit
// timer and the quadrature capture units.
//
// For a more detailed explanation of the calculations, read the description at
// the top of this file.
//
void FreqCal_calculate(FreqCal_Object *p, uint32_t *c)
{
    uint32_t temp;
    _iq newPosCnt, oldPosCnt;

    //
    // **** Frequency calculation using eQEP position counter ****
    //
    //  Check for unit time out event
    //
    /*当单位时间事件输出，索引计数+1，从QPOSLAT读取正交编码器计数*/
    if((EQEP_getInterruptStatus(EQEP1_BASE) & EQEP_INT_UNIT_TIME_OUT) != 0)
    {
        (*c)++; // Incrementing the count value
        /*从QPOSLAT读取正交编码器计数，并存储，用于计算增量*/
        newPosCnt = EQEP_getPositionLatch(EQEP1_BASE);
        oldPosCnt = p->oldPos;
        /*计算脉冲增量，即频率*/
        if(newPosCnt > oldPosCnt)
        {
            //
            // x2 - x1 in v = (x2 - x1) / T equation
            //
            temp = newPosCnt - oldPosCnt;
        }
        else
        {
            temp = (0xFFFFFFFF - oldPosCnt) + newPosCnt;
        }

        /*除10000脉冲，计算实际位移r，对应2500线*/
        p->freqFR = _IQdiv(temp, p->freqScalerFR);
        temp=p->freqFR;

        /*每转1圈，清零，以控制输出位置值在-1到1之间*/
        if(temp >= _IQ(1))
        {
            p->freqFR = _IQ(1);
        }
        else
        {
            p->freqFR = temp;
        }

        /*M法求解实际频率p->freqHzFR = (p->freqFR) * 10kHz = (x2 - x1) / T*/
        p->freqHzFR = _IQmpy(p->baseFreq,p->freqFR);

        /*脉冲计数更新，以实时连续读取*/
        p->oldPos = newPosCnt;
        /*测量结束，单位时间中断状态清零*/
        EQEP_clearInterruptStatus(EQEP1_BASE, EQEP_INT_UNIT_TIME_OUT);
    }

    /*检测单位位置事件*/
    if((EQEP_getStatus(EQEP1_BASE) & EQEP_STS_UNIT_POS_EVNT) != 0)
    {
        /*确认捕获未溢出，从QCPRDLAT读取捕获周期数*/
        if((EQEP_getStatus(EQEP1_BASE) & EQEP_STS_CAP_OVRFLW_ERROR) == 0)
        {
            temp = (uint32_t)EQEP_getCapturePeriodLatch(EQEP1_BASE);
        }
        else
        {
            //
            // Capture overflow, saturate the result
            //
            temp = 0xFFFF;
        }

        //
        // p->freqPR = X / [(t2 - t1) * 10kHz]
        //
        p->freqPR = _IQdiv(p->freqScalerPR, temp);
        temp = p->freqPR;

        if(temp > _IQ(1))
        {
            p->freqPR = _IQ(1);
        }
        else
        {
            p->freqPR = temp;
        }

        /*低速下采用T法测量求解实际频率：p->freqHzPR = (p->freqPR) * 10kHz = X / (t2 - t1)*/
        p->freqHzPR = _IQmpy(p->baseFreq, p->freqPR);

        /*清除单位位置事件与溢出错误*/
        EQEP_clearStatus(EQEP1_BASE, (EQEP_STS_UNIT_POS_EVNT |
                                      EQEP_STS_CAP_OVRFLW_ERROR));
    }
}

//
// End of File
//

