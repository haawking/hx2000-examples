/******************************************************************
 文 档 名：     eqep.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供EQEP的GPIO配置与模块功能配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "system.h"

void eqep_gpio(void)
{
    /*配置GPIO25为QEPA，上拉翻转输出或浮点输入，同步采样*/
    GPIO_setPinConfig(GPIO_25_EQEP1_A);
    GPIO_setPadConfig(25, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(25, GPIO_QUAL_SYNC);
    /*配置GPIO11为QEPB，上拉翻转输出或浮点输入，同步采样*/
    GPIO_setPinConfig(GPIO_11_EQEP1_B);
    GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(11, GPIO_QUAL_SYNC);
    /*配置GPIO12为QEPS，上拉翻转输出或浮点输入，同步采样*/
    GPIO_setPinConfig(GPIO_12_EQEP1_STROBE);
    GPIO_setPadConfig(12, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(12, GPIO_QUAL_SYNC);
    /*配置GPIO13为QEPI，上拉翻转输出或浮点输入，同步采样*/
    GPIO_setPinConfig(GPIO_13_EQEP1_INDEX);
    GPIO_setPadConfig(13, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(13, GPIO_QUAL_SYNC);
}

void eqep_config(void)
{
    /*EQEP的源选择定义*/
    EQEP_SourceSelect source_myEQEP0 =
    {
       EQEP_SOURCE_DEVICE_PIN,         // eQEPA source
       EQEP_SOURCE_DEVICE_PIN,     // eQEPB source
       EQEP_SOURCE_DEVICE_PIN,     // eQEP Index source
     };
     EQEP_selectSource(EQEP1_BASE, source_myEQEP0);
     /*EQEP的选通源选择：来自选通GPIO引脚*/
     EQEP_setStrobeSource(EQEP1_BASE,EQEP_STROBE_FROM_GPIO);
     /*EQEP极性输入配置：保留极性输入*/
     EQEP_setInputPolarity(EQEP1_BASE,false,false,false,false);
     /*EQEP解码配置：增计数|双边沿采样|无交换|门索引信号屏蔽*/
     EQEP_setDecoderConfig(EQEP1_BASE, (EQEP_CONFIG_UP_COUNT | EQEP_CONFIG_2X_RESOLUTION | EQEP_CONFIG_NO_SWAP | EQEP_CONFIG_IGATE_DISABLE));
     /*EQEP仿真模式：自由运行*/
     EQEP_setEmulationMode(EQEP1_BASE,EQEP_EMULATIONMODE_RUNFREE);
     /*EQEP模块位置计数单元配置：EQEP_POSITION_RESET_1ST_IDX第一次索引脉冲复位
      * 最大计数：4294967295U*/
     EQEP_setPositionCounterConfig(EQEP1_BASE,EQEP_POSITION_RESET_1ST_IDX,4294967295U);
     /*EQEP计数零点配置*/
     EQEP_setPosition(EQEP1_BASE,0U);
     /*EQEP单位时间使能：周期1000000，对应2500线*/
     EQEP_enableUnitTimer(EQEP1_BASE,1000000U);
     /*EQEP看门狗屏蔽*/
     EQEP_disableWatchdog(EQEP1_BASE);
     /*EQEP模式配置：单位时间超时事件|上升沿选通|上升沿索引*/
     EQEP_setLatchMode(EQEP1_BASE,(EQEP_LATCH_UNIT_TIME_OUT|EQEP_LATCH_RISING_STROBE|EQEP_LATCH_RISING_INDEX));
     /*EQEP自适应模式配置屏蔽*/
     EQEP_setQMAModuleMode(EQEP1_BASE,EQEP_QMA_MODE_BYPASS);
     /*EQEP索引期间方向改变屏蔽*/
     EQEP_disableDirectionChangeDuringIndex(EQEP1_BASE);
     /*EQEP位置初始化模式配置：参数初始化行为关闭*/
     EQEP_setPositionInitMode(EQEP1_BASE,(EQEP_INIT_DO_NOTHING));
     /*EQEP软件复位*/
     EQEP_setSWPositionInit(EQEP1_BASE,true);
     /*EQEP初始位置配置：零点*/
     EQEP_setInitialPosition(EQEP1_BASE,0U);
     /*EQEP模块使能*/
     EQEP_enableModule(EQEP1_BASE);
     /*EQEP捕获配置：128分频SYSCLK，事件8分频QCLK*/
     EQEP_setCaptureConfig(EQEP1_BASE,EQEP_CAPTURE_CLK_DIV_128,EQEP_UNIT_POS_EVNT_DIV_8);
     /*EQEP捕获单元使能*/
     EQEP_enableCapture(EQEP1_BASE);
}
