/******************************************************************
 文 档 名：       HX_DSC280025_ECAP_CAPTURE_PWM
 开 发 环 境：  Haawking IDE V2.3.4Pre
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ECAP捕获PWM波
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用ECAP捕获一20k-320k变化的PWM波
连线：ECAP1/GPIO16与捕获输入EPWM3A/GPIO4相连
现象：
//! - \b ecap1PassCount - 成功计数不等于等
//! - \b ecap1IntCount -中断计数
 * cap2Count、ecap3Count、ecap4Count均为捕获周期值，与其两倍周期值相等
 *
 * 捕获成功，LED1亮，否则，LED2亮
 *
 版 本：      V1.0.1
 时 间：      2024年10月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "system.h"

//
// Globals
//
uint32_t ecap1IntCount;
uint32_t ecap1PassCount;
uint32_t epwm3TimerDirection;
volatile uint16_t cap2Count;
volatile uint16_t cap3Count;
volatile uint16_t cap4Count;
volatile uint16_t epwm3PeriodCount;
//
// Main
//
int main(void)
{
    /*系统时钟初始化*/
    Device_init();

    /*GPIO端口配置锁定关闭*/
    Device_initGPIO();

    /*GPIO配置，用于显示捕获状态*/
    GPIO_config();

    /*初始化PIE与清PIE寄存器，关CPU中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*GPIO4的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(4,GPIO_PIN_TYPE_STD);
    /*GPIO4的EPWM3A功能配置*/
    GPIO_setPinConfig(GPIO_4_EPWM3_A);

    /*GPIO5的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(5,GPIO_PIN_TYPE_STD);
    /*GPIO5的EPWM3B功能配置*/
    GPIO_setPinConfig(GPIO_5_EPWM3_B);
    
    EALLOW;
    /*GPIO16的IO功能配置*/
    GPIO_setPinConfig(GPIO_16_GPIO16);
    /*XBAR输入引脚配置GPIO16*/
    XBAR_setInputPin(INPUTXBAR_BASE, XBAR_INPUT7, 16);

    ecap_cap_config();

    /*GPIO16的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(16, GPIO_DIR_MODE_IN);
    /*GPIO16的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(16, GPIO_PIN_TYPE_STD);
    /*GPIO16的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(16, GPIO_QUAL_ASYNC);

    /*中断入口INT_ECAP1指向相应中断服务程序，执行INT_myECAP0_ISR*/
    Interrupt_register(INT_ECAP1, &INT_myECAP0_ISR);
    /*ecap相应的PIE与CPU中断使能：中断INT_ECAPx*/
    Interrupt_enable(INT_ECAP1);

    EDIS;

    /*epwm配置*/
    initEPWM();

    /*初始化参数清零*/
    cap2Count = 0U;
    cap3Count = 0U;
    cap4Count = 0U;
    ecap1IntCount = 0U;
    ecap1PassCount = 0U;
    epwm3PeriodCount = 0U;

    /*打开全局中断*/
    EINT;

    //
    // Loop forever. Suspend or place breakpoints to observe the buffers.
    //
    for(;;)
    {
       NOP;
    }

    return 0;
}

//
// initEPWM - Configure ePWM
//
void initEPWM()
{
    /*关闭外设EPWM的TBCLKSYNC时基同步*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    /*EPWM时基计数模式：向上*/
    EPWM_setTimeBaseCounterMode(EPWM3_BASE, EPWM_COUNTER_MODE_UP);
    /*EPWM周期配置：PWM3_TIMER_MIN+1个TBCLK*/
    EPWM_setTimeBasePeriod(EPWM3_BASE, PWM3_TIMER_MIN);
    /*EPWM相位偏移：0*/
    EPWM_setPhaseShift(EPWM3_BASE, 0U);

    /*EPWM的AQ动作配置：在EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD-CTR=PRD时，
     * EPWM_AQ_OUTPUT_A-EPWMA，EPWM_AQ_OUTPUT_TOGGLE翻转*/
    EPWM_setActionQualifierAction(EPWM3_BASE,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_TOGGLE,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);

    /*EPWM预分频配置：EPWM_CLOCK_DIVIDER_1-低速时钟不分频
     *EPWM_HSCLOCK_DIVIDER_2-高速时钟不分频 */
    EPWM_setClockPrescaler(EPWM3_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_2);

    /*EPWM周期变化方向：向上增*/
    epwm3TimerDirection = EPWM_TIMER_UP;

    /*启动外设EPWM的TBCLKSYNC时基同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
}

//
// myECAP0 ISR
//
__interrupt void INT_myECAP0_ISR(void)
{
    /*读取捕获事件计数，用于判断捕获成功*/
    cap2Count = ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_2);
    cap3Count = ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_3);
    cap4Count = ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_4);

    /*读取捕获输入PWM周期，用于判断捕获成功*/
    epwm3PeriodCount = EPWM_getTimeBasePeriod(EPWM3_BASE);

    /*判断捕获是否成功，失败进入错误：仿真停止*/
    if(cap2Count > ((epwm3PeriodCount *2) + 2U) ||
       cap2Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    if(cap3Count > ((epwm3PeriodCount *2) + 2U) ||
       cap3Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    if(cap4Count > ((epwm3PeriodCount *2) + 2U) ||
       cap4Count < ((epwm3PeriodCount *2) - 2U))
    {
        error();
    }

    /*捕获成功计数+1*/
    ecap1IntCount++;

    /*模拟一个变频的PWM波：20K到320K，测试ECAP的捕获性能*/
    if(epwm3TimerDirection == EPWM_TIMER_UP)
    {
        if(epwm3PeriodCount < PWM3_TIMER_MAX)
        {
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
        else
        {
           epwm3TimerDirection = EPWM_TIMER_DOWN;
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
    }
    else
    {
        if(epwm3PeriodCount > PWM3_TIMER_MIN)
        {
            EPWM_setTimeBasePeriod(EPWM3_BASE, --epwm3PeriodCount);
        }
        else
        {
           epwm3TimerDirection = EPWM_TIMER_UP;
           EPWM_setTimeBasePeriod(EPWM3_BASE, ++epwm3PeriodCount);
        }
    }

    /*捕获中断计数+1*/
    ecap1PassCount++;

    if(ecap1PassCount!=0)
    {
 	    /*GPIO31写入0,点亮LED1*/
 	    GPIO_writePin(31, 0);
    }
    else
    {
 	    /*GPIO34写入0,点亮LED2*/
 	    GPIO_writePin(34, 0);
    }

    /*清除ECAP捕获中断事件4*/
    ECAP_clearInterrupt(ECAP1_BASE,ECAP_ISR_SOURCE_CAPTURE_EVENT_4);
    /*清除ECAP捕获全局中断*/
    ECAP_clearGlobalInterrupt(ECAP1_BASE);

    /*ECAP重载计数*/
    ECAP_reArm(ECAP1_BASE);

    /*中断应答：清除ECAP对应的PIE中断*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP4);
}

//
// error - Error function
//
void error()
{
    /*GPIO34写入0,点亮LED2*/
    GPIO_writePin(34, 0);
}
