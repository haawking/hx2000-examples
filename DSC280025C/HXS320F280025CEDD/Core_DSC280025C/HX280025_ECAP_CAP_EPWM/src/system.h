/*
 * system.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define PWM3_TIMER_MIN     500U
#define PWM3_TIMER_MAX     8000U
#define EPWM_TIMER_UP      1U
#define EPWM_TIMER_DOWN    0U

//
// Globals
//
extern uint32_t ecap1IntCount;
extern uint32_t ecap1PassCount;
extern uint32_t epwm3TimerDirection;
extern volatile uint16_t cap2Count;
extern volatile uint16_t cap3Count;
extern volatile uint16_t cap4Count;
extern volatile uint16_t epwm3PeriodCount;

//
// Function Prototypes
//
void error(void);
void initECAP(void);
void initEPWM(void);
void ecap_cap_config();

void GPIO_config(void);

__interrupt void INT_myECAP0_ISR(void);

#endif /* SYSTEM_H_ */
