#include "board.h"

void Board_init()
{
	EALLOW;

	PinMux_init();
	INPUTXBAR_init();
	SYNC_init();
	EPWM_init();
	GPIO_init();
	INTERRUPT_init();

	EDIS;
}

void PinMux_init()
{

	// EPWM1 -> myEPWM1 引脚复用
	GPIO_setPinConfig(GPIO_0_EPWM1_A);
	// EPWM2 -> myEPWM2 引脚复用
	GPIO_setPinConfig(GPIO_2_EPWM2_A);
	// GPIO12 -> myGPIO12 引脚复用
	GPIO_setPinConfig(GPIO_12_GPIO12);
	// GPIO11 -> myGPIO11 引脚复用
	GPIO_setPinConfig(GPIO_11_GPIO11);

}

void EPWM_init(){

	//ePWM1
    //ePWM1设置时钟分频，时钟分频/4，高速时钟分频/4     160M/4/4=10M
    EPWM_setClockPrescaler(myEPWM1_BASE, EPWM_CLOCK_DIVIDER_4, EPWM_HSCLOCK_DIVIDER_4);
    //ePWM1设置时基周期，10000  ，10M/(10000*2)=500Hz
    EPWM_setTimeBasePeriod(myEPWM1_BASE, 10000);
    //ePWM1设置时基计数器，0
    EPWM_setTimeBaseCounter(myEPWM1_BASE, 0);
    //ePWM1设置时基计数器模式，上下计数
    EPWM_setTimeBaseCounterMode(myEPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    //ePWM1关闭相位偏移负载
    EPWM_disablePhaseShiftLoad(myEPWM1_BASE);
    //ePWM1设置相位偏移
    EPWM_setPhaseShift(myEPWM1_BASE, 0);	
    //ePWM1设置计数器比较值A，6000
    EPWM_setCounterCompareValue(myEPWM1_BASE, EPWM_COUNTER_COMPARE_A, 6000);
    //ePWM1设置计数器比较影子装载模式A，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM1_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM1设置计数器比较值B，0
    EPWM_setCounterCompareValue(myEPWM1_BASE, EPWM_COUNTER_COMPARE_B, 0);
    //ePWM1设置计数器比较影子装载模式B，计数器等于零时装载
    EPWM_setCounterCompareShadowLoadMode(myEPWM1_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器等于周期
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚置高，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚置低，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器上升至COMPB时
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM1设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器下降至COMPB时
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器等于周期
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器上升至COMPB时
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM1设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器下降至COMPB时
    EPWM_setActionQualifierAction(myEPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM1设置TZ动作，事件 TZ_A，高电平状态
    EPWM_setTripZoneAction(myEPWM1_BASE, EPWM_TZ_ACTION_EVENT_TZA, EPWM_TZ_ACTION_HIGH);	
    //ePWM1设置TZ信号模式，TZ1_oneshot
    EPWM_enableTripZoneSignals(myEPWM1_BASE, EPWM_TZ_SIGNAL_OSHT1);	
    //ePWM1使能TZ中断，TZ_oneshot中断
    EPWM_enableTripZoneInterrupt(myEPWM1_BASE, EPWM_TZ_INTERRUPT_OST);	

    //ePWM2
    //ePWM2设置时钟分频，时钟分频/4，高速时钟分频/4，160M/4/4=10M
    EPWM_setClockPrescaler(myEPWM2_BASE, EPWM_CLOCK_DIVIDER_4, EPWM_HSCLOCK_DIVIDER_4);	
    //ePWM2设置时基周期，5000，10M/(5000*2)=1K Hz
    EPWM_setTimeBasePeriod(myEPWM2_BASE, 5000);
    //ePWM2设置时基计数器
    EPWM_setTimeBaseCounter(myEPWM2_BASE, 0);	
    //ePWM2设置时基计数器模式，上下计数
    EPWM_setTimeBaseCounterMode(myEPWM2_BASE, EPWM_COUNTER_MODE_UP_DOWN);	
    //ePWM2关闭相位偏移装载
    EPWM_disablePhaseShiftLoad(myEPWM2_BASE);	
    //ePWM2设置相位偏移
    EPWM_setPhaseShift(myEPWM2_BASE, 0);	
    //ePWM2设置计数器比较值，计数器比较A，3000
    EPWM_setCounterCompareValue(myEPWM2_BASE, EPWM_COUNTER_COMPARE_A, 3000);	
    //ePWM2设置计数器比较影子装载模式
    EPWM_setCounterCompareShadowLoadMode(myEPWM2_BASE, EPWM_COUNTER_COMPARE_A, EPWM_COMP_LOAD_ON_CNTR_ZERO);	
    //ePWM2设置计数器比较值，计数器比较B，0
    EPWM_setCounterCompareValue(myEPWM2_BASE, EPWM_COUNTER_COMPARE_B, 0);	
    //ePWM2设置计数器比较影子装载模式
    EPWM_setCounterCompareShadowLoadMode(myEPWM2_BASE, EPWM_COUNTER_COMPARE_B, EPWM_COMP_LOAD_ON_CNTR_ZERO);
    //（ePWMx，ePWMxA，动作，条件）
    //ePWM2设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM2设置动作限定，ePWMxA输出，输出引脚没有变化，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM2设置动作限定，ePWMxA输出，将输出引脚设置为高，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_HIGH, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM2设置动作限定，ePWMxA输出，将输出引脚设置为低，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_LOW, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM2设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM2设置动作限定，ePWMxA输出，输出引脚没有变化，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_A, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器等于零
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时基计数器等于周期
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器上升等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器下降等于COMPA
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器上升等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB);	
    //ePWM2设置动作限定，ePWMxB输出，输出引脚没有变化，时间基准计数器下降等于COMPB
    EPWM_setActionQualifierAction(myEPWM2_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_OUTPUT_NO_CHANGE, EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB);	
    //ePWM2设置TZ动作,事件 TZA，高电平状态
    EPWM_setTripZoneAction(myEPWM2_BASE, EPWM_TZ_ACTION_EVENT_TZA, EPWM_TZ_ACTION_HIGH);	
    //ePWM2使能TZ信号模式，TZ1_CBC
    EPWM_enableTripZoneSignals(myEPWM2_BASE, EPWM_TZ_SIGNAL_CBC1);	
    //ePWM2使能TZ中断，TZ1_CBC中断
    EPWM_enableTripZoneInterrupt(myEPWM2_BASE, EPWM_TZ_INTERRUPT_CBC);	
}

void GPIO_init(){
		
	//myGPIO12 初始化
	GPIO_setDirectionMode(myGPIO12, GPIO_DIR_MODE_IN);
	GPIO_setPadConfig(myGPIO12, GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(myGPIO12, GPIO_QUAL_ASYNC);
		
	//myGPIO11 初始化
	GPIO_setDirectionMode(myGPIO11, GPIO_DIR_MODE_OUT);
	GPIO_setPadConfig(myGPIO11, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(myGPIO11, GPIO_QUAL_ASYNC);
}
void INPUTXBAR_init(){
	
	//myINPUTXBAR1 初始化
	XBAR_setInputPin(INPUTXBAR_BASE, XBAR_INPUT1, 12);
}
void INTERRUPT_init(){
	
	// INT_myEPWM1_TZ中断设置
	Interrupt_register(INT_myEPWM1_TZ, &epwm1TZISR);
	Interrupt_enable(INT_myEPWM1_TZ);
	
	//INT_myEPWM2_TZ中断设置
	Interrupt_register(INT_myEPWM2_TZ, &epwm2TZISR);
	Interrupt_enable(INT_myEPWM2_TZ);
}

void SYNC_init(){
	SysCtl_setSyncOutputConfig(SYSCTL_SYNC_OUT_SRC_EPWM1SYNCOUT);
	// SOCA
	SysCtl_enableExtADCSOCSource(0);
	// SOCB
	SysCtl_enableExtADCSOCSource(0);
}
