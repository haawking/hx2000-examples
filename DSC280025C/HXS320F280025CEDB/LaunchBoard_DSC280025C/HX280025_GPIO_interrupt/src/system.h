/*
 * system.h
 *
 *  Created on: 2023��2��24��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_
//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Interrupt Handler
//
__interrupt void gpioInterruptHandler(void);

void XINT_IOinit(void);


#endif /* SYSTEM_H_ */
