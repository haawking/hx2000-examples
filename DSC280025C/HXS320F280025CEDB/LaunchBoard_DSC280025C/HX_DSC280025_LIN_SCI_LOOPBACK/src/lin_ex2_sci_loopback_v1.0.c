/******************************************************************
 文 档 名：       HX_DSC280025_LIN_SCI_LOOPBACK
 开 发 环 境：  Haawking IDE V2.1.0
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      LIN_SCI中断方式收发数据
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，
 采用内环连接，通过LIN_SCI中断方式收发数据

外部连接：无

现象：
//!  - rxCount - 接收数据计数
//!  - transmitChar - 发送数据
//!  - receivedChar -接收数据
 * 接收与发送数据相等receivedChar=transmitChar GPIO31/LED1点亮
 * 否则 GPIO34/LED2点亮
 *

 版 本：      V1.0.1
 时 间：      2023年1月18日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define CHAR_LENGTH     8
#define FRAME_LENGTH    1

//
// Globals
//
volatile uint32_t rxCount = 0;
volatile uint32_t vectorOffset = 0;
volatile uint16_t error = 0;
uint16_t transmitChar = 0x0;
uint16_t receivedChar = 0x0;

//
// Function Prototypes
//
void GPIO_config(void);
__interrupt void dataRxISR(void);
void configureSCIMode(void);

//
// Main
//
void main(void)
{
    /*初始化系统时钟*/
    Device_init();

    /*GPIO锁定解除*/
    Device_initGPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*关中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();
    /*打开全局中断*/
    EINT;
    ERTM;

    /*中断入口地址INT_LINA_0指向,执行dataRxISR中断服务程序*/
    Interrupt_register(INT_LINA_0, &dataRxISR);

    /*中断使能INT_LINA_0*/
    Interrupt_enable(INT_LINA_0);

    /*初始化LINA模块*/
    LIN_initModule(LINA_BASE);

    /*初始化LINA的SCI配置*/
    configureSCIMode();

    /*使能LINA全局中断为LIN_INTERRUPT_LINE0中断线0*/
    LIN_enableGlobalInterrupt(LINA_BASE, LIN_INTERRUPT_LINE0);
    /*清除LINA全局中断为LIN_INTERRUPT_LINE0中断线0*/
    LIN_clearGlobalInterruptStatus(LINA_BASE, LIN_INTERRUPT_LINE0);

    //
    // Continuously transmit an 8-bit character, wait for ISR to run, and
    // verify everything was received correctly
    //
    for(;;)
    {
        vectorOffset = 0;

        /*初始化定义发送数据*/
        transmitChar++;
        if(transmitChar > 0xFF)
        {
            transmitChar = 0;
        }

        /*等待SCI数据非空*/
        while(!LIN_isSCIReceiverIdle(LINA_BASE));

        /*写入数据到SCI块*/
        LIN_writeSCICharBlocking(LINA_BASE, transmitChar);

        /*等待LIN_SCI接收中断触发*/
        while(vectorOffset != LIN_VECT_RX);
    }
}

//
// Configure SCI Mode - This function configures the LIN module to operate as 
// an SCI with the specified settings.
//
void configureSCIMode(void)
{
    /*SCI软复位*/
    LIN_enterSoftwareReset(LINA_BASE);

    /*LIN_SCI模式使能*/
    LIN_enableSCIMode(LINA_BASE);

    /*LIN_SCI模式：空闲线收发*/
    LIN_setSCICommMode(LINA_BASE, LIN_COMM_SCI_IDLELINE);

    /*LIN_SCI停止位1位*/
    LIN_setSCIStopBits(LINA_BASE,LIN_SCI_STOP_ONE);

    /*LIN_SCI关闭奇偶校验*/
    LIN_disableSCIParity(LINA_BASE);

    /*LIN_SCI关闭多缓冲模式*/
    LIN_disableMultibufferMode(LINA_BASE);

    /*LIN_SCI仿真暂停模式：LIN_DEBUG_COMPLETE-停止于LIN调试完成*/
    LIN_setDebugSuspendMode(LINA_BASE, LIN_DEBUG_COMPLETE);

    /*发送数据字符长度：8位*/
    LIN_setSCICharLength(LINA_BASE, CHAR_LENGTH);

    /*发送数据帧长度：8位*/
    LIN_setSCIFrameLength(LINA_BASE, FRAME_LENGTH);

    /*SCI自回环*/
    LIN_enableIntLoopback(LINA_BASE);

    /*SCI中断接收使能*/
    LIN_enableSCIInterrupt(LINA_BASE, LIN_SCI_INT_RX);

    /*SCI接收中断LIN_SCI_INT_RX配置为中断线0*/
    LIN_setSCIInterruptLevel0(LINA_BASE, LIN_SCI_INT_RX);

    /*SCI软复位释放*/
    LIN_exitSoftwareReset(LINA_BASE);
}

//
// Received Data ISR - An interrupt service routine (ISR) to handle when new
// data is received. Once received, the data is read and interrupt status 
// cleared.
//
__interrupt void dataRxISR(void)
{
    /*接收中断计数+1*/
    rxCount++;

    /*获取中断偏移向量*/
    vectorOffset = LIN_getInterruptLine0Offset(LINA_BASE);

    /*读取SCI模块接收数据*/
    receivedChar = LIN_readSCICharBlocking(LINA_BASE, false);

    /*判断接收数据与发送数据是否一致，一致则LED1亮，否则LED2亮*/
    if(receivedChar != transmitChar)
    {
        /*GPIO34写入1,点亮LED2*/
        GPIO_writePin(34, 0);
    }
    else
    {
        /*GPIO31写入1,点亮LED1*/
        GPIO_writePin(31, 0);
    }

    /*清除LIN_RX接收中断状态*/
    LIN_clearInterruptStatus(LINA_BASE, LIN_INT_RX);
    /*清除LIN_RX接收中断对应中断线0-LIN_INTERRUPT_LINE0中断状态*/
    LIN_clearGlobalInterruptStatus(LINA_BASE, LIN_INTERRUPT_LINE0);

    //
    // Acknowledge this interrupt located in group 8
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}

//
// End of File
//
void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED1灭*/
    GPIO_writePin(31, 1);


    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

    /*GPIO34写入1,初始化LED2灭*/
    GPIO_writePin(34, 1);
    EDIS;
}
