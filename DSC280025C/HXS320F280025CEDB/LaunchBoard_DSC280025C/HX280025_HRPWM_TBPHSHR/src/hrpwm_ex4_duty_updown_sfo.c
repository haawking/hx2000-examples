/******************************************************************
 文 档 名：       HX_DSC280025_HRPWM_TBPHSHR
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      HRPWM与EPWM的脉宽精度对比
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用HRPWM与EPWM输出占空比800kHz,占空比50%的互补波形
 相位偏移在54-56度间调节，精度可达0.18ns-对应0.05度
现象：
需连接高精度示波器或逻辑分析仪查看
EPWM1
 *
 版 本：      V1.0.0
 时 间：      2023年2月3日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include <system.h>

int main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定配置解除*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    Interrupt_register(INT_EPWM1, &epwm1ISR);

	/*关闭时基时钟TBCLKSYNC同步使能，以便EPWM初始化*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EALLOW;
	/*配置GPIO0-GPIO5为EPWM1A-EPWM3B*/
    epwm_gpio();
	/*配置HRPWM*/
    hrpwm1_config();
    hrpwm2_config();
    hrpwm3_config();
    EDIS;

	/*使能时基时钟TBCLKSYNC同步，完成EPWM初始化*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    Interrupt_enable(INT_EPWM1);

	/*使能打开全局中断*/
    EINT;
    ERTM;


    for(;;)
    {

     }

    return 0;
}

