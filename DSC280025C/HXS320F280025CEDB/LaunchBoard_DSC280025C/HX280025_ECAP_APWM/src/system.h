/*
 * system.h
 *
 *  Created on: 2022��11��17��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define DECREASE_FREQUENCY  0
#define INCREASE_FREQUENCY  1

//
// Globals
//
extern volatile uint32_t cap1Count;
void ECAP_apwm_config();
void OUTPUTXBAR_config();

void GPIO_config(void);

#endif /* SYSTEM_H_ */
