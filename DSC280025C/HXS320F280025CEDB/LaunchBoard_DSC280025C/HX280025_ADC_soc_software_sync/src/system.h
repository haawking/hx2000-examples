/*
 * system.h
 *
 *  Created on: 2023年2月23日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Globals
//
extern uint16_t myADC0Result0[3];
extern uint16_t myADC0Result1[3];
extern uint16_t myADC1Result0[3];
extern uint16_t myADC1Result1[3];

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void AdcA_config(void);
void AdcC_config(void);
/*按键输入，强制ADC软件触发*/
void InitKEY(void);

#endif /* SYSTEM_H_ */
