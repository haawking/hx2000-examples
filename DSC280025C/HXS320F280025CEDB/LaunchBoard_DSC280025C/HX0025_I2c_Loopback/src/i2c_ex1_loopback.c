/******************************************************************
 文 档 名：       HX0025_Sci_Loopback
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      带FIFO中断的数字回环
 说     明：      RAM工程
 -------------------------- 例程使用说明 --------------------------
//!本程序使用I2C模块的内部环回测试模式。
//!TX和RX I2C fifo和它们的中断都被使用。
//!pinmux和i2c初始化是通过sysconfig文件完成的。
//!
//! The sent data looks like this: \n
//!  0000 0001 \n
//!  0001 0002 \n
//!  0002 0003 \n
//!  .... \n
//!  00FE 00FF \n
//!  00FF 0000 \n
//!  etc.. \n
//! 此模式一直循环
//!
//! 外部连接
//!  无- None
//!
//! 观测变量
//!  sData - 发送数据
//!  rData - 接收数据
//!  rDataPoint -  用于跟踪接收数据流中的末尾位置进行错误检查
//!
//!   LED1 GPIO31亮表示正常
//!   LED2 GPIO34亮表示失败
****************************************************************************************/

//
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "board.h"

#include"system.h"
//
// Defines
//
#define SLAVE_ADDRESS   0x3C

//
// Globals
//
uint16_t sData[2];                  // Send data buffer
uint16_t rData[2];                  // Receive data buffer
uint16_t rDataPoint = 0;            // To keep track of where we are in the data stream to check received data

uint16_t errorCount=0;
//
// Function Prototypes
//
__interrupt void i2cFIFOISR(void);


#define MB_ADDR 0x21FFC

void Success(void)
{

 GPIO_writePin(31,0);
 GPIO_writePin(34,1);
 HWREG(MB_ADDR) = 0x5555AAAA;

}

void Fail(void)
{
 GPIO_writePin(31,1);
 GPIO_writePin(34,0);
 HWREG(MB_ADDR) = 0xAAAA5555;
}


//
// Main
//
int main(void)
{
    uint16_t i;

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();
    setup1GPIO();
    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Board initialization
    //
    Board_init();

    //
    // For loopback mode only
    //
    I2C_setOwnSlaveAddress(myI2C0_BASE, SLAVE_ADDRESS);

    //
    // Interrupts that are used in this example are re-mapped to ISR functions
    // found within this file.
    //
    Interrupt_register(INT_I2CA_FIFO, &i2cFIFOISR);

    //
    // Initialize the data buffers
    //
    for(i = 0; i < 2; i++)
    {
        sData[i] = i;
        rData[i]= 0;
    }

    //
    // Enable interrupts required for this example
    //
    Interrupt_enable(INT_I2CA_FIFO);

    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;
    ERTM;

    //
    // Loop forever. Suspend or place breakpoints to observe the buffers.
    //
    while(1)
    {
    	//根据配置的中断级别，将为每个Tx和Rx生成FIFO中断。
    	//ISR将处理从TX和RX fifo响应中推/拉数据。
    }

    return 0;

}

//
// I2C A Transmit & Receive FIFO ISR.
//
 __interrupt void i2cFIFOISR(void)
{
    uint16_t i;

    //
    // If receive FIFO interrupt flag is set, read data
    //
    if((I2C_getInterruptStatus(myI2C0_BASE) & I2C_INT_RXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            rData[i] = I2C_getData(myI2C0_BASE);
        }

        //
        // Check received data
        //
        for(i = 0; i < 2; i++)
        {
            if(rData[i] != ((rDataPoint + i) & 0xFF))
            {
                //
                // Something went wrong. rData doesn't contain expected data.
                //
                //ESTOP0;
            	errorCount++;
                //GPIO_writePin(34, 0); //LED2 GPIO34亮表示失败
            	Fail();
            }
        }

        rDataPoint = (rDataPoint + 1) & 0xFF;

        //
        // Clear interrupt flag
        //
        I2C_clearInterruptStatus(myI2C0_BASE, I2C_INT_RXFF);

    }
    //
    // If transmit FIFO interrupt flag is set, put data in the buffer
    //
    else if((I2C_getInterruptStatus(myI2C0_BASE) & I2C_INT_TXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            I2C_putData(myI2C0_BASE, sData[i]);
        }

        //
        // Send the start condition
        //
        I2C_sendStartCondition(myI2C0_BASE);

        //
        // Increment data for next cycle
        //
        for(i = 0; i < 2; i++)
        {
           sData[i] = (sData[i] + 1) & 0xFF;
        }
        //GPIO_writePin(31, 0); //LED1 GPIO31亮表示成功
        Success();

        //
        // Clear interrupt flag
        //
        I2C_clearInterruptStatus(myI2C0_BASE, I2C_INT_TXFF);
    }

    //
    // Issue ACK
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);


}

//
// End of File
//
