.section .text, "ax", @progbits

.global __enable_interrupts

__enable_interrupts:

csrrsi a0,mstatus,8

ret

.size  __enable_interrupts,   .-__enable_interrupts