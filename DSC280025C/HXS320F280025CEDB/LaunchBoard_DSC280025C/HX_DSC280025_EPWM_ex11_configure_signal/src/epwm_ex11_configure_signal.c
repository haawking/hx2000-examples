/******************************************************************************************
 文 档 名：       HX_DSC280025_EPWM_ex11_configure_signal
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：           DSC280025
 使 用 库：
 作     用：       配置所需的EPWM频率和占空比
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------
本例配置ePWM1、ePWM2、ePWM3产生所需信号频率和占空比

在ePWMxA和ePWMxB上配置10kHz信号，占空比0.5，与ePWMxB反向。
相位之间配置为120度ePWM1 ~ ePWM3信号。

 - ePWM1A GPIO0
 - ePWM1B GPIO1
 - ePWM2A GPIO2
 - ePWM2B GPIO3
 - ePWM3A GPIO4
 - ePWM3B GPIO5
-------------------------------------------------------------------------------------------
版 本：V1.0.0
 时 间：2023年1月1日
 作 者：
 @ mail：support@mail.haawking.com
******************************************************************************************/

#include "driverlib.h"
#include "device.h"
#include "board.h"


EPWM_SignalParams pwmSignal =
            {10000, 0.5f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

void configurePhase(uint32_t base, uint32_t masterBase, uint16_t phaseVal);

//
// Main
//
int main(void)
{

    Device_init();

    Device_initGPIO();

    Interrupt_initModule();

    Interrupt_initVectorTable();

    Board_init();


    //禁用同步(冻结PWM时钟)
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    //配置ePWM模块
    EPWM_configureSignal(myEPWM1_BASE, &pwmSignal);
    EPWM_configureSignal(myEPWM2_BASE, &pwmSignal);
    EPWM_configureSignal(myEPWM3_BASE, &pwmSignal);


    //配置PWM1, PWM2和PWM3。
    //配置PWM1为master，配置ePWM2 & 3被配置为slave。
    EPWM_disablePhaseShiftLoad(myEPWM1_BASE);
    EPWM_setPhaseShift(myEPWM1_BASE, 0U);


    //ePWM1 SYNCO在CTR=0时生成
    EPWM_enableSyncOutPulseSource(myEPWM1_BASE, EPWM_SYNC_OUT_PULSE_ON_CNTR_ZERO);

    //配置EPWM2和3相位偏移120，240
    configurePhase(myEPWM2_BASE, myEPWM1_BASE, 120);
    configurePhase(myEPWM3_BASE, myEPWM1_BASE, 240);
   //同步脉冲，同步输入源为EPWM1同步输出信号
    EPWM_setSyncInPulseSource(myEPWM2_BASE, EPWM_SYNC_IN_PULSE_SRC_SYNCOUT_EPWM1);
    EPWM_setSyncInPulseSource(myEPWM3_BASE, EPWM_SYNC_IN_PULSE_SRC_SYNCOUT_EPWM1);
    //启用相位偏移
    EPWM_enablePhaseShiftLoad(myEPWM2_BASE);
    EPWM_enablePhaseShiftLoad(myEPWM3_BASE);


    //使能同步和时钟PWM
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    //启用中断
    Interrupt_enable(INT_EPWM1);

    //启用全局中断、实时中断
    EINT;
    ERTM;


    for(;;)
    {
        asm ("  NOP");
    }

    return 0;
}


//配置相位
void configurePhase(uint32_t base, uint32_t masterBase, uint16_t phaseVal)
{
    uint32_t readPrdVal, phaseRegVal;


    //读取周期值以计算相位寄存器的值
    readPrdVal = EPWM_getTimeBasePeriod(masterBase);


    //计算相位寄存器值
    if((HWREG(base + EPWM_O_TBCTL) & 0x3U) == EPWM_COUNTER_MODE_UP_DOWN)
    {
        phaseRegVal = (2U * readPrdVal * phaseVal) / 360U;
    }
    else if((HWREG(base + EPWM_O_TBCTL) & 0x3U) < EPWM_COUNTER_MODE_UP_DOWN)
    {
        phaseRegVal = (readPrdVal * phaseVal) / 360U;
    }

    EPWM_selectPeriodLoadEvent(base, EPWM_SHADOW_LOAD_MODE_SYNC);
    EPWM_setPhaseShift(base, phaseRegVal);
    EPWM_setTimeBaseCounter(base, phaseRegVal);
}

//
// End of file
//
