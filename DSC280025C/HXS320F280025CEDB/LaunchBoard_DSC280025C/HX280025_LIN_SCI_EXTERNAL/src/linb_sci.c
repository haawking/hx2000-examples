#include "system.h"
volatile uint32_t txCount=0;

volatile uint32_t vectorOffset_tx=0;

//
// Configure SCI Mode - This function configures the LIN module to operate as
// an SCI with the specified settings.
//
void configureSCIBMode(void)
{
    /*SCI软复位*/
    LIN_enterSoftwareReset(LINB_BASE);

    /*LIN_SCI模式使能*/
    LIN_enableSCIMode(LINB_BASE);

    /*LIN_SCI模式：空闲线收发*/
    LIN_setSCICommMode(LINB_BASE, LIN_COMM_SCI_IDLELINE);

    /*LIN_SCI停止位1位*/
    LIN_setSCIStopBits(LINB_BASE,LIN_SCI_STOP_ONE);

    /*LIN_SCI关闭奇偶校验*/
    LIN_disableSCIParity(LINB_BASE);

    /*LIN_SCI多缓冲模式*/
    LIN_disableMultibufferMode(LINB_BASE);

    /*LIN_SCI仿真暂停模式：LIN_DEBUG_COMPLETE-停止于LIN调试完成*/
    LIN_setDebugSuspendMode(LINB_BASE, LIN_DEBUG_COMPLETE);

    /*发送数据字符长度：8位*/
    LIN_setSCICharLength(LINB_BASE, CHAR_LENGTH);

    /*发送数据帧长度：8位*/
    LIN_setSCIFrameLength(LINB_BASE, FRAME_LENGTH);

    /*SCI自回环关闭*/
    LIN_disableIntLoopback(LINB_BASE);

    /*SCI软复位释放*/
    LIN_exitSoftwareReset(LINB_BASE);
}
