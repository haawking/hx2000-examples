/******************************************************************
 文 档 名：       HX_DSC280025_GPIO_toggle
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      GPIO闪灯
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：GPIO31/LED1翻转闪灯，周期1s

 *
 版 本：      V1.0.1
 时 间：      2023年3月23日
 作 者：      liyuyao
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Main
//
void main(void)
{
	uint16_t i;

	/*系统时钟初始化*/
    Device_init();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置GPIO31为IO输出*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);

	GPIO_writePin(31,1);
    EDIS;

    /*使能CPU中断*/
    Interrupt_enableMaster();

    for(;;)
    {
        /*GPIO31/LED1循环闪灯，周期1s*/
    	GPIO_togglePin(31);
		for(i = 0; i < 500; i ++)
			// The maximum input parameter for DEVICE_DELAY_US() is 34000
			DEVICE_DELAY_US(1000);
    }
}


//
// End of File
//

