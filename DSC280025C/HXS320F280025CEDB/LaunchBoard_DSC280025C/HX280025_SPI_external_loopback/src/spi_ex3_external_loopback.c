/******************************************************************
 文 档 名：       HX_DSC280025_SPI_external_loopback
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：     SPI外环通讯
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：SPIA作为从机，SPIB为主机
 （1）从SPIA从机向SPIB主机发送数据，SPIB主机接收数据
 （2）从SPIB主机向SPIA从机发送数据，SPIA从机接收数据

 连接方式：
 SPIB   SPIA
GPIO40-GPIO8  - SPISIMO
GPIO41-GPIO10 - SPISOMI
GPIO22-GPIO9  - SPICLK
GPIO23-GPIO11 - SPISTE

现象：
（1）SPIB主机接收到的数据与SPIA从机发送的数据一致，则GPIO31/LED1灯亮
（2）SPIA从机接收到的数据与SPIB主机发送的数据一致，则GPIO34/LED2灯亮
 *
 版 本：      V1.0.0
 时 间：      2023年2月24日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "system.h"

//
// Main
//
void main(void)
{
    uint16_t i;

    uint16_t TxData_SPIA[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
    uint16_t RxData_SPIA[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    uint16_t TxData_SPIB[] = {0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
    uint16_t RxData_SPIB[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /*系统时钟初始化*/
    Device_init();
    /*GPIO配置锁定解除*/
    Device_initGPIO();
    /*配置GPIO30/31为IO输出*/
    setup1GPIO();

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
    /*SPIA/B的GPIO引脚配置*/
    spia_gpio();
    spib_gpio();
    /*SPIA/B的从机/主机配置*/
    SPIA_slave_init();
    SPIB_master_init();
    EDIS;

    for(i = 0; i < 16; i++)
    {
    	/*向从机SPI写入发送数据TX buffer*/
        SPI_writeDataNonBlocking(SPIA_BASE, TxData_SPIA[i]);

    	/*向主机SPI写入发送数据TX buffer*/
        SPI_writeDataNonBlocking(SPIB_BASE, TxData_SPIB[i]);

        /*读取SPIA从机接收到的SPIB主机数据*/
        RxData_SPIA[i] = SPI_readDataBlockingNonFIFO(SPIA_BASE);
        /*读取SPIB主机接收到的SPIA从机数据*/
        RxData_SPIB[i] = SPI_readDataBlockingNonFIFO(SPIB_BASE);

        /*检测从机SPIA接收到的SPIB主机发送数据，
         * 是否与发送数据一致，若一致则GPIO31/LED1灯亮*/
        if(RxData_SPIA[i] != TxData_SPIB[i])
        {
        	GPIO_writePin(31,1);
        }
        else
        {
        	GPIO_writePin(31,0);
        }

        /*检测主机SPIB接收到的SPIA从机发送数据，
         * 是否与发送数据一致，若一致则GPIO34/LED2灯亮*/
        if(RxData_SPIB[i] != TxData_SPIA[i])
        {
        	GPIO_writePin(34,1);
        }
        else
        {
        	GPIO_writePin(34,0);
        }
    }

    while(1);
}
