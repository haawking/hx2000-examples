/******************************************************************
 文 档 名：       HX_DSC280025_LIN_LIN_EXTERNAL
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      LIN中断方式收发数据
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，通过LIN总线中断方式收发数据

外部连接：
LINA TX/RX与LINBTX/RX通过LIN收发器相连

现象：
  - txData - 发送数据
  - rxData - 接收数据
 * 接收与发送数据相等时，点亮GPIO31/LED1
 *不一致时，点亮GPIO34/LED2

 版 本：      V1.0.1
 时 间：      2023年1月18日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/


//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define FRAME_LENGTH    0x8

//
// Globals
//
volatile uint32_t level0Count = 0;
volatile uint32_t level1Count = 0;
volatile uint32_t vectorOffset = 0;
uint16_t txID = 0x1A, rxID = 0x1A;
uint16_t txData[8] = {0x11, 0x34, 0x56, 0x78, 0x9A, 0xAB, 0xCD, 0xEF};
uint16_t rxData[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
void GPIO_config(void);
void LIN_GPIO(void);
//
// Main
//
int main(void)
{
    uint32_t dataIndex;

    /*初始化系统时钟*/
    Device_init();

    /*GPIO锁定解除*/
    Device_initGPIO();
    /*LIN的GPIO配置*/
    LIN_GPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*LIN初始化配置*/
    LIN_initModule(LINA_BASE);
    LIN_initModule(LINB_BASE);

    /*LINB作为从机，LINA作为主机模式配置*/
    LIN_setLINMode(LINA_BASE, LIN_MODE_LIN_MASTER);
    LIN_setLINMode(LINB_BASE, LIN_MODE_LIN_SLAVE);

    /*LINA与LINB使能奇偶校验*/
    LIN_enableParity(LINA_BASE);
    LIN_enableParity(LINB_BASE);

    /*写入合适的LINB从机接收rxID匹配帧头*/
    LIN_setIDSlaveTask(LINB_BASE, rxID);
    LIN_setIDByte(LINB_BASE, rxID);

    /*配置传输帧长度：8字节*/
    LIN_setFrameLength(LINA_BASE, 8);
    LIN_setFrameLength(LINB_BASE, 8);

    /*通过LINA写入TX发送数据*/
    LIN_sendData(LINA_BASE, txData);   //

    /*生成合适的匹配校验接收ID*/
    txID = LIN_generateParityID(txID);
    /*写入发送ID匹配LINB的接收ID*/
    LIN_setIDByte(LINA_BASE, txID);

    /*等待LINA发送缓冲区非空，表明完成传输*/
    while(!LIN_isTxBufferEmpty(LINA_BASE));

    /*等待LINB接收匹配ID，表明完成接收*/
    while(!LIN_isRxMatch(LINB_BASE));

    /*清中断状态*/
    LIN_clearInterruptStatus(LINA_BASE,LIN_INT_ALL);
    LIN_clearInterruptStatus(LINB_BASE,LIN_INT_ALL);

    /*读取接收数据*/
    LIN_getData(LINB_BASE, rxData);

    while(1)
    {
    	/*验证接收数据是否与发送数据相匹配*/
    	    for (dataIndex=0; dataIndex < 8; dataIndex++)
    	    {
    	        if (rxData[dataIndex] != txData[dataIndex])
    	        {
    	              /*GPIO34写入0点亮LED2*/
    	              GPIO_writePin(34, 0);
    	        }
    	        else
    	        {
    	        	  /*GPIO31写入0点亮LED1*/
    	        		GPIO_writePin(31, 0);
    	        }
    	    }
    }
    return 0;
}

//
// End of File
//

void LIN_GPIO(void)
{
    GPIO_setPinConfig(GPIO_46_LINA_TX);
    GPIO_setPinConfig(GPIO_42_LINA_RX);
    GPIO_setPinConfig(GPIO_40_LINB_TX);
    GPIO_setPinConfig(GPIO_41_LINB_RX);

    GPIO_setDirectionMode(42, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(42, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(42, GPIO_QUAL_ASYNC);
    GPIO_setDirectionMode(41, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(41, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(41, GPIO_QUAL_ASYNC);

    GPIO_setDirectionMode(46, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(46, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(46, GPIO_QUAL_ASYNC);
    GPIO_setDirectionMode(40, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(40, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(40, GPIO_QUAL_ASYNC);
}

void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    /*GPIO31写入1,初始化LED1灭*/
    GPIO_writePin(31, 1);


    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

    /*GPIO34写入1,初始化LED2灭*/
    GPIO_writePin(34, 1);
    EDIS;
}
