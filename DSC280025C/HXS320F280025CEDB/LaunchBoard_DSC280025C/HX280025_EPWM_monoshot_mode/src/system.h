/*
 * system.h
 *
 *  Created on: 2023年2月6日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

	/*配置GPIO0-EPWM1A，GPIO2-EPWM2A*/
void epwm_gpio(void);
	/*配置GPIO2为XBAR_INPUT5交叉开关输入5*/
void inputxbar_init(void);
void sync_init(void);
	/*PWM配置*/
void epwm1_config(void);
void epwm2_config(void);

//*****************************************************************************
//
// EPWM Configurations
//
//*****************************************************************************
#define myEPWM1_BASE EPWM1_BASE
#define myEPWM2_BASE EPWM2_BASE

#endif /* SYSTEM_H_ */
