//###########################################################################
//
// FILE:    hw_i2s.h
//
// TITLE:   Definitions for the I2S registers.
//
//###########################################################################
// $HAAWKING Release: DSP28002x Support Library V1.0.4 $
// $Release Date: 2023-03-14 07:25:04 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef HW_I2S_H
#define HW_I2S_H

//*************************************************************************************************
//
// The following are defines for the I2S register offsets
//
//*************************************************************************************************

#define I2S_O_I2SSCTRL         0x0U      // I2S Serializer Control Register
#define I2S_O_I2SSRATE         0x4U      // I2S Sample Rate Generator Register
#define I2S_O_I2STXLT0         0x8U      // I2S Transmit Left Data 0 Register
#define I2S_O_I2STXRT0         0xCU      // I2S Transmit Right Data 0 Register
#define I2S_O_I2SINTFL        0x10U      // I2S Interrupt Flag Register
#define I2S_O_I2SINTMASK      0x14U      // I2S Interrupt Mask Register
#define I2S_O_I2SRXLT0        0x18U      // I2S Receive Left Data 0 Register
#define I2S_O_I2SRXRT0        0x1CU      // I2S Receive Right Data 0 Register
#define I2S_O_I2SMCR1         0x20U      // I2S Multichannel Register 1
#define I2S_O_I2SMCR2         0x24U      // I2S Multichannel Register 2
#define I2S_O_I2SRCERA        0x28U      // I2S Receive Channel Enable Register Partition A
#define I2S_O_I2SXCERA        0x2CU      // I2S Transmit Channel Enable Register Partition A
#define I2S_O_I2SRXDRP        0x30U      // I2S Rx Drop Data Function Control Reister
#define I2S_O_I2STXTDMDRP     0x34U      // I2S TX TDM mode Drop Data Function Control Register

#endif
