#include "system.h"
uint32_t DBhr;

__interrupt void epwm1ISR(void)
{
	for(DBhr=1;DBhr<128;DBhr++)
	{
		HRPWM_setRisingEdgeDelay(EPWM1_BASE,DBhr<<9);
		HRPWM_setFallingEdgeDelay(EPWM1_BASE,DBhr<<9);
	}


	EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

