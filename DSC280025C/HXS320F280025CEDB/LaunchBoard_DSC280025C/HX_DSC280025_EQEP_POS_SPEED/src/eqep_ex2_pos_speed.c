/******************************************************************
 文 档 名：       HX_DSC280025_EQEP_POS_SPEED
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EQEP编码器转速测量
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用EQEP索引脉冲复位,
 正交计数模式捕获脉冲计算电机转速

连线：
GPIO25/eQEP1A -GPIO0/ePWM1A
GPIO29/eQEP1B-GPIO1/ePWM1B
GPIO23/eQEP1Z-GPIO2索引信号连接

采用5kHz的PWM方波输出，EPWM1A/EPWM1B互差90度，
模拟正交信号EQEP模拟编码器输入
采用索引脉冲复位进行捕获测量，编码器模拟线数2500线，上升沿计数两倍频
转1圈产生5000个脉冲计数

现象：
//!  - \b posSpeed.speedRPMFR - M法测量电机转速rpm
//!  - \b posSpeed.speedRPMPR - T法测量电机转速rpm
//!  - \b posSpeed.thetaMech  机械角度Q15值
//!  - \b posSpeed.thetaElec  - 电角度Q15值
 * 可捕获到连续增长的脉冲计数
 * 即测量转子角度与转速不为零，则成功，点亮GPIO31/LED1
 * 失败，则点亮GPIO34/LED2
 *
 *
 版 本：      V1.0.0
 时 间：      2022年11月18日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
//

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "IQmathLib.h"
#include "eqep_ex2_calculation.h"
#include "system.h"

 void GPIO_config(void);

//
// Globals
//
PosSpeed_Object posSpeed =
{
    0, 0, 0, 0,     // Initialize outputs to zero
    MECH_SCALER,    // mechScaler
    POLE_PAIRS,     // polePairs
    CAL_ANGLE,      // calAngle
    SPEED_SCALER,   // speedScaler
    0,              // Initialize output to zero
    BASE_RPM,       // baseRPM
    0, 0, 0, 0      // Initialize outputs to zero
};

uint16_t interruptCount = 0;
uint32_t count =0;  // counter to check measurement gets saturated
uint32_t pass=0, fail =0; // Pass or fail indicator
//
// Main
//
void main(void)
{
    /*系统时钟初始化*/
    Device_init();

    /*GPIO配置锁定关闭*/
    Device_initGPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*清中断,关中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
    /*EQEP的GPIO配置*/
    eqep_gpio();
    /*EQEP的功能配置*/
    eqep_config();
    /*GPIO2配置：IO，同步采样，上拉翻转输出*/
    gpio_config();
    EDIS;

    /*初始化GPIO0配置为EPWM1A，上拉翻转输出，用于模拟编码脉冲信号输入测试EQEP*/
    GPIO_setPinConfig(GPIO_0_EPWM1_A);
    GPIO_setPadConfig(0, GPIO_PIN_TYPE_STD);
    /*初始化GPIO1配置为EPWM1B，上拉翻转输出，用于模拟编码脉冲信号输入测试EQEP*/
    GPIO_setPinConfig(GPIO_1_EPWM1_B);
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    
    /*中断入口地址INT_EPWM1指向执行epwmISR中断服务程序*/
    Interrupt_register(INT_EPWM1, &epwmISR);

    /*配置5KHz的PWM方波脉冲输出，EPWM1A与EPWM1B相差90度，模拟正交输出*/
    initEPWM();

    /*中断使能INT_EPWM1*/
    Interrupt_enable(INT_EPWM1);

    /*打开全局中断*/
    EINT;
    ERTM;

    while(1)
    {
        /*判断是否捕获到连续增长的脉冲，
         * 可测量转子角度与转速，不为零，则点亮GPIO31/LED1，失败则点亮GPIO34/LED2*/
    	if((posSpeed.speedRPMFR!=0)&&(posSpeed.thetaMech!=0))
	    {
	        /*GPIO31写入1,点亮LED1*/
	        GPIO_writePin(31, 0);
	        /*GPIO34写入1,LED2灭*/
	        GPIO_writePin(34, 1);
	    }
	    else
	    {
	        /*GPIO34写入1,点亮LED2*/
	        GPIO_writePin(34, 0);
	        /*GPIO31写入1,LED1灭*/
	        GPIO_writePin(31, 1);
	    }
    }
}

//
// Function to configure ePWM1 to generate a 5 kHz signal.
//
void initEPWM(void)
{
    /*外设时基同步关闭*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    /*相位偏移TBPHS=0*/
    EPWM_setPhaseShift(EPWM1_BASE, 0);
    /*时基计数初值TBCTR=0*/
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0);

    /*禁止装载影子寄存器，采用立即模式装载*/
    EPWM_disableCounterCompareShadowLoadMode(EPWM1_BASE,
                                             EPWM_COUNTER_COMPARE_A);
    EPWM_disableCounterCompareShadowLoadMode(EPWM1_BASE,
                                             EPWM_COUNTER_COMPARE_B);

    /*配置比较点CMPA=PRD/2，脉宽=50%*/
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, PRD_VAL/2);
    /*配置比较点CMPB=0*/
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_B, 0);

    /*配置动作模块行为:向上CTR=CMPA时，EPWM1A置高*/
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    /*配置动作模块行为:向下CTR=CMPA时，EPWM1A置低*/
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);

    /*配置动作模块行为:向上CTR=PRD时，EPWM1B置高*/
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD);
    /*配置动作模块行为:向上CTR=ZERO时，EPWM1B拉低*/
    EPWM_setActionQualifierAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO);

    /*配置事件模块触发时刻TBCTR=0*/
    EPWM_setInterruptSource(EPWM1_BASE, EPWM_INT_TBCTR_PERIOD);
    /*配置事件模块使能*/
    EPWM_enableInterrupt(EPWM1_BASE);

    /*配置事件模块触发次数，一周期触发一次，即中断周期与PWM周期相等*/
    EPWM_setInterruptEventCount(EPWM1_BASE, 1);

    /*配置EPWM的高速与低速时钟分频，不分频*/
    EPWM_setClockPrescaler(EPWM1_BASE, EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    /*配置EPWM周期装载模式：EPWM_PERIOD_DIRECT_LOAD直接装载
     * EPWM_PERIOD_SHADOW_LOAD影子装载*/
    EPWM_setPeriodLoadMode(EPWM1_BASE, EPWM_PERIOD_DIRECT_LOAD);
    /*配置EPWM周期，实际频率=SYSCLK：160M/低速时钟分频*高速时钟分频/PRD_VAL/2=5kHz*/
    EPWM_setTimeBasePeriod(EPWM1_BASE, PRD_VAL);

    /*配置EPWM时基计数模式：向上向下计数*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);

    /*外设时基同步打开*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
}

//
// ePWM1 ISR--interrupts once every 4 QCLK counts (one period)
//
 __interrupt void epwmISR(void)
{
    uint16_t i;

    /*EQEP脉冲计数到实际转速的计算*/
    PosSpeed_calculate(&posSpeed, &count);

    /*超饱和处理：控制测量频率与实际频率偏差在5以内*/
    if (count >= 2){
        if (((posSpeed.speedRPMFR - 300) < 5) &&
             ((posSpeed.speedRPMFR - 300) > -5))
        {
            pass = 1; fail = 0;
        }
        else {
            fail = 1; pass = 0;
        }
       }

    /*索引计数达到1000时，向GPIO2写入1，产生索引脉冲信号*/
    interruptCount++;
    if(interruptCount == 1000)
    {
        //
        // Pulse index signal (1 pulse/rev)
        //
        GPIO_writePin(2, 1);
        for(i = 0; i < 700; i++)
        {
            ;
        }
        GPIO_writePin(2, 0);
    }

    /*清EPWM事件触发标志*/
    EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    /*清中断应答ack的第3组*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

 void GPIO_config(void)
 {
     EALLOW;
     /*GPIO31的IO功能配置*/
     GPIO_setPinConfig(GPIO_31_GPIO31);
     /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
     GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
     /*GPIO31的上拉翻转配置:
      * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
      * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
     GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
     /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
      * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
     GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
     /*GPIO31写入1,初始化LED1灭*/
     GPIO_writePin(31, 1);


     GPIO_setPinConfig(GPIO_34_GPIO34);
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
     GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
     GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

     /*GPIO34写入1,初始化LED2灭*/
     GPIO_writePin(34, 1);
     EDIS;
 }
