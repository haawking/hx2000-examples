/******************************************************************
 文 档 名：     eqep_ex1_calculation.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供EQEP脉冲捕获频率计算
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月18日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "eqep_ex2_calculation.h"
#include "system.h"

//
// Function to calculate the frequency of the input signal using both the unit
// timer and the quadrature capture units.
//
// For a more detailed explanation of the calculations, read the description at
// the top of this file.
//
void PosSpeed_calculate(PosSpeed_Object *p, uint32_t *c)
{
    int32_t temp;
    uint16_t pos16bVal, temp1;
    _iq temp2, newPosCnt, oldPosCnt;

    /*从EQEPSTS[QDF]读取编码器的方向信号:-1逆时针 1顺时针*/
    p->directionQEP = EQEP_getDirection(EQEP1_BASE);

    /*从QPOSCNT读取QEP的角度增量*/
    pos16bVal = (uint16_t)EQEP_getPosition(EQEP1_BASE);

    /*测量电机机械角度=角度增量+初始角度*/
    p->thetaRaw = pos16bVal + p->calAngle;

    /*测量电机实际机械角度=机械角度*4000计数/圈*/
    temp = (int32_t)p->thetaRaw * (int32_t)p->mechScaler;   // Q0 * Q26 = Q26
    temp &= 0x03FFF000;

    p->thetaMech = (int16_t)(temp >> 11);                    // Q26 -> Q15
    p->thetaMech &= 0x7FFF;

    /*测量电机电角度=极对数*机械角度*/
    p->thetaElec = p->polePairs * p->thetaMech;               // Q0 * Q15 = Q15
    p->thetaElec &= 0x7FFF;

    /*检测索引脉冲事件的发生，若发生，清除状态*/
    if((EQEP_getInterruptStatus(EQEP1_BASE) & EQEP_INT_INDEX_EVNT_LATCH) != 0U)
    {
        EQEP_clearInterruptStatus(EQEP1_BASE, EQEP_INT_INDEX_EVNT_LATCH);
    }

    /*当单位时间事件输出，索引计数+1，从QPOSLAT读取正交编码器计数*/
    if((EQEP_getInterruptStatus(EQEP1_BASE) & EQEP_INT_UNIT_TIME_OUT) != 0)
    {
        (*c)++; // Incrementing the count value
        /*从QPOSLAT读取正交编码器计数，并存储，用于计算增量*/
        pos16bVal = (uint16_t)EQEP_getPositionLatch(EQEP1_BASE);
        temp = (int32_t)pos16bVal * (int32_t)p->mechScaler; // Q0 * Q26 = Q26
        temp &= 0x03FFF000;

        temp = (int16_t)(temp >> 11);                        // Q26 -> Q15
        temp &= 0x7FFF;

        newPosCnt = _IQ15toIQ(temp);
        oldPosCnt = p->oldPos;

        /*计算脉冲增量，即频率*/
        if(p->directionQEP == -1)
        {
            if(newPosCnt > oldPosCnt)
            {
                //
                // x2 - x1 should be negative
                //
                temp2 = -(_IQ(1) - newPosCnt + oldPosCnt);
            }
            else
            {
                temp2 = newPosCnt -oldPosCnt;
            }
        }
        //
        // POSCNT is counting up
        //
        else if(p->directionQEP == 1)
        {
            if(newPosCnt < oldPosCnt)
            {
                temp2 = _IQ(1) + newPosCnt - oldPosCnt;
            }
            else
            {
                //
                // x2 - x1 should be positive
                //
                temp2 = newPosCnt - oldPosCnt;
            }
        }

        if(temp2 > _IQ(1))
        {
            p->speedFR = _IQ(1);
        }
        else if(temp2 < _IQ(-1))
        {
            p->speedFR = _IQ(-1);
        }
        else
        {
            p->speedFR = temp2;
        }

        /*脉冲计数更新，以实时连续读取*/
        p->oldPos = newPosCnt;

        /*计算电机转速：n=60f/p*/
        p->speedRPMFR = _IQmpy(p->baseRPM, p->speedFR);

        /*测量结束，单位时间中断状态清零*/
        EQEP_clearInterruptStatus(EQEP1_BASE, EQEP_INT_UNIT_TIME_OUT);
    }

    /*低速下，采用QEP捕获单元计算，检查单位位置事件*/
    if((EQEP_getStatus(EQEP1_BASE) & EQEP_STS_UNIT_POS_EVNT) != 0)
    {
        /*无捕获溢出*/
        if((EQEP_getStatus(EQEP1_BASE) & EQEP_STS_CAP_OVRFLW_ERROR) == 0)
        {
            temp1 = (uint32_t)EQEP_getCapturePeriodLatch(EQEP1_BASE);
        }
        else
        {
            //
            // Capture overflow, saturate the result
            //
            temp1 = 0xFFFF;
        }

        //
        // p->speedPR = p->speedScaler / temp1
        //
        p->speedPR = _IQdiv(p->speedScaler, temp1);
        temp2 = p->speedPR;

        if(temp2 > _IQ(1))
        {
           p->speedPR = _IQ(1);
        }
        else
        {
           p->speedPR = temp2;
        }

        //
        // Convert p->speedPR to RPM
        //
        // Reverse direction = negative
        //
        if(p->directionQEP == -1)
        {
            //
            // Q0 = Q0 * GLOBAL_Q => _IQXmpy(), X = GLOBAL_Q
            //
            p->speedRPMPR = -_IQmpy(p->baseRPM, p->speedPR);
        }
        //
        // Forward direction = positive
        //
        else
        {
            //
            // Q0 = Q0 * GLOBAL_Q => _IQXmpy(), X = GLOBAL_Q
            //
            p->speedRPMPR = _IQmpy(p->baseRPM, p->speedPR);
        }

        /*清除位置事件单元标志与溢出错误标志*/
        EQEP_clearStatus(EQEP1_BASE, (EQEP_STS_UNIT_POS_EVNT |
                                      EQEP_STS_CAP_OVRFLW_ERROR));
    }
}
