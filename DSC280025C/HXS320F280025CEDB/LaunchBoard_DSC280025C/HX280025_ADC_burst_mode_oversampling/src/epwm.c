#include "system.h"

void initEPWM(void)
{
	/*关闭ADC的EPWM_SOCA触发*/
    EPWM_disableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    /*配置EPWM_SOC_A-EPWM_SOCA触发：
     * 在EPWM_SOC_TBCTR_U_CMPA-向上计数CTR=CMPA时触发产生SOCA事件*/
    EPWM_setADCTriggerSource(EPWM1_BASE, EPWM_SOC_A, EPWM_SOC_TBCTR_U_CMPA);
    /*ADC事件分频=EPWM_TBCLK/1*/
    EPWM_setADCTriggerEventPrescale(EPWM1_BASE, EPWM_SOC_A, 1);

    /*配置EPWM比较点：CMPA=1000-对应脉宽50%*/
    EPWM_setCounterCompareValue(EPWM1_BASE, EPWM_COUNTER_COMPARE_A, 1000);
    /*配置EPWM周期=SYSCLK/HSPCLKDIV/CLKDIV/(TBPRD+1)=80kHz*/
    EPWM_setTimeBasePeriod(EPWM1_BASE, 1999);

    /*配置EPWM时钟分频：EPWM_CLOCK_DIVIDER_1-低速时钟1分频
     * EPWM_HSCLOCK_DIVIDER_1-高速时钟1分频*/
    EPWM_setClockPrescaler(EPWM1_BASE,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    /*配置EPWM计数模式：EPWM_COUNTER_MODE_STOP_FREEZE-冻结计数器*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}
