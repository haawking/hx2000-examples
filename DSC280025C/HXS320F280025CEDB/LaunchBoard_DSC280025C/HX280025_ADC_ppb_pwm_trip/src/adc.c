#include "system.h"

//
// Globals
//
volatile uint16_t indexA;                // Index into result buffer
volatile uint16_t bufferFull;                // Flag to indicate buffer is full
uint16_t adcA2Results[RESULTS_BUFFER_SIZE];  // ADC result buffer

void configureADC(uint32_t adcBase)
{
	/*配置ADC时钟=SYSCLK/8=20M*/
    ADC_setPrescaler(adcBase, ADC_CLK_DIV_8_0);

	/*配置ADC中断脉冲模式为：
	 * ADC_PULSE_END_OF_CONV-转换完成产生脉冲触发中断*/
    ADC_setInterruptPulseMode(adcBase, ADC_PULSE_END_OF_CONV);

    /*ADC上电以使能启动转换*/
    ADC_enableConverter(adcBase);

    /*延时1ms完成ADC模块启动*/
    DEVICE_DELAY_US(1000);
}

void configureADCSOC(uint32_t adcBase, uint16_t channel,uint32_t acqps)
{
    /*配置ADC_SOC:ADC_SOC_NUMBER0-SOC0
     * ADC_TRIGGER_SW_ONLY-采用软件触发
          * 采样通道，采样周期*/
	ADC_setupSOC(adcBase, ADC_SOC_NUMBER0,
			ADC_TRIGGER_SW_ONLY,
			(ADC_Channel)channel, acqps);

    /*配置中断源SOC触发：ADC_SOC_NUMBER0-SOC0
     * ADC_INT_SOC_TRIGGER_ADCINT1-ADCINT1触发*/
    ADC_setInterruptSOCTrigger(adcBase,
    		ADC_SOC_NUMBER0,ADC_INT_SOC_TRIGGER_ADCINT1);

    /*使能连续模式：ADC_INT_NUMBER1-ADCINT1*/
    ADC_enableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);

    /*清ADC中断源：中断源ADC_INT_NUMBER1-ADCINT1，
     * EOC源ADC_SOC_NUMBER0-EOC0*/
    ADC_setInterruptSource(adcBase, ADC_INT_NUMBER1, ADC_SOC_NUMBER0);
    /*清ADC中断状态：ADC_INT_NUMBER1-ADCINT1*/
    ADC_clearInterruptStatus(adcBase, ADC_INT_NUMBER1);
    /*ADC使能中断：ADC_INT_NUMBER1-ADCINT1*/
    ADC_enableInterrupt(adcBase, ADC_INT_NUMBER1);
}


__interrupt void adcA1ISR(void)
{
#ifdef ENABLE_PROFILING
    //
    // Setting Profiling GPIO12 : Takes 3 cycles
    //
    HWREG(GPIODATA_BASE  + GPIO_O_GPASET) = 0x1000;
#endif

    /*读取ADC_SOC_NUMBER0-SOC0采样结果*/
    adcA2Results[indexA++] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    if(RESULTS_BUFFER_SIZE <= indexA)
    {
        indexA = 0;
    }

    /*清PIEACK应答中断*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);

#ifdef ENABLE_PROFILING
    //
    // Resetting Profiling GPIO12
    //
    HWREG(GPIODATA_BASE  + GPIO_O_GPACLEAR) = 0x1000;
#endif
}
