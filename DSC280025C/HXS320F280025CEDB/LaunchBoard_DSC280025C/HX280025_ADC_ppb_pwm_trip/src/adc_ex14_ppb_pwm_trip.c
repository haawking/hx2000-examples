/******************************************************************
 文 档 名：       HX_DSC280025_ADC_PPB_PWM_TRIP
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC顺序采样与后处理触发事件
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 （1）采用软件强制触发, 通过A0通道顺序采样输入电压，
 （2）在超越阈值2.9V时刻，通过后处理器模块向PWM输出错误联防TZ事件信号，
 （3）通过错误联防信号TZ，触发相应动作：
 A.单次触发，通过交叉开关TRIP7事件输入置高信号，触发产生单次触发事件，使EPWM1A置低，EPWM1B置高
 B.周期触发，通过交叉开关TRIP8事件输入置高信号，触发产生周期触发事件，使EPWM2A置低，EPWM2B置高
 C.直接触发，通过交叉开关TRIP8事件输入置高信号，触发产生数字比较事件，使EPWM3A置低，EPWM3B置高
 现象:
通过A0通道连接相应电位器元件,可读出正确的电压值Vi=result/4096*3.3V
当超越阈值2.9V时，触发EPWMxA置低，EPWMxB置高

 版 本：      V1.0.0
 时 间：      2023年2月21日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "system.h"

/*配置EPWM1：频率=10kHz，占空比CMPA/CMPB=50%
 * 计数模式：EPWM_COUNTER_MODE_UP_DOWN-上下计数
 * 时钟分频：EPWM_CLOCK_DIVIDER_1-低速时钟1分频
 * EPWM_HSCLOCK_DIVIDER_1-高速时钟1分频*/
EPWM_SignalParams pwmSignal1 =
            {10000, 0.5f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

/*配置EPWM2：频率=2.5kHz，占空比CMPA=20%,CMPB=50%
 * 计数模式：EPWM_COUNTER_MODE_UP_DOWN-上下计数
 * 时钟分频：EPWM_CLOCK_DIVIDER_1-低速时钟1分频
 * EPWM_HSCLOCK_DIVIDER_1-高速时钟1分频*/
EPWM_SignalParams pwmSignal2 =
            {25000, 0.2f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

/*配置EPWM3：频率=5kHz，占空比CMPA=70%,CMPB=50%
 * 计数模式：EPWM_COUNTER_MODE_UP_DOWN-上下计数
 * 时钟分频：EPWM_CLOCK_DIVIDER_1-低速时钟1分频
 * EPWM_HSCLOCK_DIVIDER_1-高速时钟1分频*/
EPWM_SignalParams pwmSignal3 =
            {5000, 0.7f, 0.5f, true, DEVICE_SYSCLK_FREQ,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();
    /*关中断,清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口地址INT_ADCA1,指向执行adcA1ISR中断服务程序*/
    Interrupt_register(INT_ADCA1, &adcA1ISR);

#ifdef ENABLE_PROFILING
    //
    // Setup profiling GPIO
    //
    setupProfileGpio();
#endif

    /*配置EPWM_GPIO*/
    initEPWMGPIO();

    /*配置ADC模块电压：ADC_REFERENCE_INTERNAL-内部参考电压
     *ADC_REFERENCE_3_3V-参考电压3.3V */
    ADC_setVREF(ADCA_BASE, ADC_REFERENCE_INTERNAL, ADC_REFERENCE_3_3V);
    ADC_setVREF(ADCC_BASE, ADC_REFERENCE_INTERNAL, ADC_REFERENCE_3_3V);

    /*ADC模块上电配置*/
    configureADC(ADCA_BASE);

    /*关闭外设时钟TBCLKSYNC，以便于EPWM配置*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    /*配置EPWM1-EPWM3频率及SOC事件*/
    EPWM_configureSignal(EPWM1_BASE, &pwmSignal1);
    EPWM_configureSignal(EPWM2_BASE, &pwmSignal2);
    EPWM_configureSignal(EPWM3_BASE, &pwmSignal3);

    /*单次错误联防事件配置*/
    configureOSHTripSignal(EPWM1_BASE);

    /*周期错误联防事件配置*/
    configureCBCTripSignal(EPWM2_BASE);

    /*直接事件触发配置*/
    configureDirectTripSignal(EPWM3_BASE);

    /*配置ADC_SOC*/
    configureADCSOC(ADCA_BASE, 2U, 8);

    /*配置极限探测PPB后处理：SOC0，
     * 高极限-3600-对应3.3V*3600/4096=2.9V
     * 低极限-0-对应3.3V*0/4096=0V*/
    configureLimitDetectionPPB(0U, 3600U, 0U);

    /*中断使能INT_ADCA1*/
    Interrupt_enable(INT_ADCA1);

    /*实时打开全局中断*/
    EINT;
    ERTM;

    /*使能外设时钟TBCLKSYNC，以完成EPWM配置，且时基同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    /*ADC_SOC_NUMBER0-SOC0通过软件强制触发*/
    ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);

    do
    {}
    while(1);
}
//
// End of file
//
