#include "system.h"

void configureLimitDetectionPPB(uint16_t soc, uint16_t limitHigh,
                                uint16_t limitLow)
{
	/*SOC的PPB后处理模块选择:ADC_PPB_NUMBER1-后处理模块1*/
    ADC_setupPPB(ADCA_BASE, ADC_PPB_NUMBER1, (ADC_SOCNumber)soc);

	/*SOC的PPB后处理模块极限配置:模块X，高阈值，低阈值*/
    ADC_setPPBTripLimits(ADCA_BASE, ADC_PPB_NUMBER1, limitHigh, limitLow);

    /*ADC后处理PPB事件使能：模块X，
     * 事件名：ADC_EVT_TRIPHI-超过高阈值输出事件
     *  ADC_EVT_TRIPLO-超过低阈值输出事件*/
    ADC_enablePPBEvent(ADCA_BASE, ADC_PPB_NUMBER1, (ADC_EVT_TRIPHI |
                                                    ADC_EVT_TRIPLO));
    EALLOW;
    /*周期CBC触发使能*/
    HWREG(ADCA_BASE + ADC_O_PPB1CONFIG) |= ADC_PPB2CONFIG_CBCEN;
    EDIS;
}
