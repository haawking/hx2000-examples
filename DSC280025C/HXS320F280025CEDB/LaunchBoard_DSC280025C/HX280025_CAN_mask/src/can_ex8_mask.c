/******************************************************************
 文 档 名：       HX_DSC280025_CAN_MASK
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      CAN掩码接收
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，通过CAN掩码方式接收数据

连线：GPIO33/CANRX、GPIO32/CANTX与CAN收发器相连
 现象:
//!  - rxMsgCount - 接收数据中断计数--
 * rxMsgData 接收数据

 版 本：      V1.0.0
 时 间：      2023年2月24日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "system.h"

//
// Globals
//
uint16_t rxMsgData[8];          // Buffer for received data
uint16_t Toggle_ctr;            // GPIO toggle counter
volatile uint32_t rxMsgCount;


//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();

    /*配置GPIO33为CANA_RX*/
    GPIO_setPinConfig(GPIO_33_CANA_RX);
    /*配置GPIO32为CANA_TX*/
    GPIO_setPinConfig(GPIO_32_CANA_TX);

    /*配置GPIO33允许上拉翻转与方向输出*/
    GPIO_setPadConfig(33, GPIO_PIN_TYPE_STD);
    GPIO_setDirectionMode(33, GPIO_DIR_MODE_OUT);

    /*初始化定义接收数组*/
    rxMsgData[0] = 0xDEAD;
    rxMsgData[1] = 0xDEAD;
    rxMsgData[2] = 0xDEAD;
    rxMsgData[3] = 0xDEAD;
    rxMsgData[4] = 0xDEAD;
    rxMsgData[5] = 0xDEAD;
    rxMsgData[6] = 0xDEAD;
    rxMsgData[7] = 0xDEAD;

    /*CAN模块初始化*/
    CAN_initModule(CANA_BASE);

    /*CAN波特率配置：时钟分频=SYSCLK，波特率：500kbps, 位比特=16*/
    CAN_setBitRate(CANA_BASE, DEVICE_SYSCLK_FREQ, 500000, 16);

    /*CAN模块配置：RX_MSG_OBJ_ID-接收邮箱采用Mailbox1
     * ID信息码，帧类型：CAN_MSG_FRAME_EXT-扩展帧
     * 帧类型：CAN_MSG_OBJ_TYPE_RX-接收帧，ID掩码
     * 对象信息标志：CAN_MSG_OBJ_USE_ID_FILTER-使用滤波
     *CAN_MSG_OBJ_NO_FLAGS-无对象标志
     *MSG_DATA_LENGTH-接收对象长度0-无关  */
    CAN_setupMessageObject(CANA_BASE, RX_MSG_OBJ_ID, 0x1F9FFFFA,
                           CAN_MSG_FRAME_EXT, CAN_MSG_OBJ_TYPE_RX, 0x1F000000,
                           (CAN_MSG_OBJ_USE_ID_FILTER | CAN_MSG_OBJ_NO_FLAGS),
   						   MSG_DATA_LENGTH);

    /*CAN模块启动*/
    CAN_startModule(CANA_BASE);

    while(1)
    {
    	/*检测接收是否完成*/
        if((HWREG_BP(CANA_BASE + CAN_O_NDAT_21))  == 0x00000001)
        {
         	/*从rxMsgData读取接收数据*/
            CAN_readMessage(CANA_BASE, RX_MSG_OBJ_ID, rxMsgData);

            rxMsgCount++;
            for(Toggle_ctr=0; Toggle_ctr<100; Toggle_ctr++)
            {
                GPIO_togglePin(DEVICE_GPIO_PIN_LED1);
                DEVICE_DELAY_US(100000);
            }
        }
    }
}

//
// End of File
//
