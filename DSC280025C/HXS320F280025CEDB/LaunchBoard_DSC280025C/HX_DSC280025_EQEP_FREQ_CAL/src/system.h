/*
 * system.h
 *
 *  Created on: 2022��11��18��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_


#include "driverlib.h"
#include "device.h"
#include "IQmathLib.h"

//
// Defines
//
#define TB_CLK    DEVICE_SYSCLK_FREQ        // Time base clock is SYSCLK
#define PWM_CLK   5000                      // We want to output at 5 kHz
#define PRD_VAL   (TB_CLK / (PWM_CLK * 2))  // Calculate value period value
                                            // for up-down count mode



//
// Function Prototypes
//
void initEPWM(void);
__interrupt void epwmISR(void);
void eqep_gpio(void);
void eqep_config(void);

void GPIO_config(void);


#endif /* SYSTEM_H_ */
