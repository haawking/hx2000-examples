#include "system.h"

void InitAdcAio(void)
{
	/*配置GPIO231为模拟量输入A0/C15*/
	GPIO_setPinConfig(GPIO_231_GPIO231);
	/*配置GPIO231模拟量输入*/
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);

	/*配置GPIO232为模拟量输入A1*/
	GPIO_setPinConfig(GPIO_232_GPIO232);
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);
	/*配置GPIO230为模拟量输入A10/C10*/
	GPIO_setPinConfig(GPIO_230_GPIO230);
	GPIO_setAnalogMode(230, GPIO_ANALOG_ENABLED);
	/*配置GPIO237为模拟量输入A11/C0*/
	GPIO_setPinConfig(GPIO_237_GPIO237);
	GPIO_setAnalogMode(237, GPIO_ANALOG_ENABLED);
	/*配置GPIO238为模拟量输入A12/C1*/
	GPIO_setPinConfig(GPIO_238_GPIO238);
	GPIO_setAnalogMode(238, GPIO_ANALOG_ENABLED);
	/*配置GPIO239为模拟量输入A14/C4*/
	GPIO_setPinConfig(GPIO_239_GPIO239);
	GPIO_setAnalogMode(239, GPIO_ANALOG_ENABLED);
	/*配置GPIO233为模拟量输入A15/C7*/
	GPIO_setPinConfig(GPIO_233_GPIO233);
	GPIO_setAnalogMode(233, GPIO_ANALOG_ENABLED);
	/*配置GPIO224为模拟量输入A2/C9*/
	GPIO_setPinConfig(GPIO_224_GPIO224);
	GPIO_setAnalogMode(224, GPIO_ANALOG_ENABLED);
	/*配置GPIO242为模拟量输入A3/C5/VDAC*/
	GPIO_setPinConfig(GPIO_242_GPIO242);
	GPIO_setAnalogMode(242, GPIO_ANALOG_ENABLED);
	/*配置GPIO225为模拟量输入A4/C14*/
	GPIO_setPinConfig(GPIO_225_GPIO225);
	GPIO_setAnalogMode(225, GPIO_ANALOG_ENABLED);
	/*配置GPIO244为模拟量输入A5/C2*/
	GPIO_setPinConfig(GPIO_244_GPIO244);
	GPIO_setAnalogMode(244, GPIO_ANALOG_ENABLED);
	/*配置GPIO228为模拟量输入A6*/
	GPIO_setPinConfig(GPIO_228_GPIO228);
	GPIO_setAnalogMode(228, GPIO_ANALOG_ENABLED);
	/*配置GPIO245为模拟量输入A7/C3*/
	GPIO_setPinConfig(GPIO_245_GPIO245);
	GPIO_setAnalogMode(245, GPIO_ANALOG_ENABLED);
	/*配置GPIO241为模拟量输入A8/C11*/
	GPIO_setPinConfig(GPIO_241_GPIO241);
	GPIO_setAnalogMode(241, GPIO_ANALOG_ENABLED);
	/*配置GPIO227为模拟量输入A9/C8*/
	GPIO_setPinConfig(GPIO_227_GPIO227);
	GPIO_setAnalogMode(227, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_226_GPIO226);
	GPIO_setAnalogMode(226, GPIO_ANALOG_ENABLED);
}


void InitAdc(void)
{
	/*屏蔽ADC温度传感器功能*/
	ASysCtl_disableTemperatureSensor();
	/*ADC参考电压配置为:ADC_REFERENCE_INTERNAL-内部参考电压,
	 * ADC_REFERENCE_3_3V-3.3V*/
	ADC_setVREF(ADCA_BASE,ADC_REFERENCE_INTERNAL,ADC_REFERENCE_3_3V);
}


void Adc_config(void)
{
	/*配置ADC时钟为ADC_CLK_DIV_8_0-8分频对应SYSCLK/8=20M*/
	ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_8_0);
	/*配置ADC中断脉冲模式：ADC_PULSE_END_OF_CONV-转换完成产生脉冲触发中断*/
	ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
	/*ADC上电使能转换*/
	ADC_enableConverter(ADCA_BASE);
	/*延时1ms完成ADC模块上电*/
	DEVICE_DELAY_US(5000);

	/*使能ADC突发模式*/
	ADC_enableBurstMode(ADCA_BASE);
	/*使能ADC突发数量：3个（最大可达16个）*/
	ADC_setBurstModeConfig(ADCA_BASE, ADC_TRIGGER_EPWM1_SOCA, 3);

	/*配置ADC_SOC转换优先级：
	 * ADC_PRI_THRU_SOC6_HIPRI-以SOC6为最高优先级*/
	ADC_setSOCPriority(ADCA_BASE, ADC_PRI_THRU_SOC6_HIPRI);

	/*配置ADC采样：ADC_SOC_NUMBER7-SOC7，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN0-采用A0通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER7,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN0, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER7-SOC7
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER7, ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER8-SOC8，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN1-采用A1通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER8,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN1, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER8-SOC8
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER8, ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER9-SOC9，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER9,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER9-SOC9
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER9, ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER10-SOC10，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN0-采用A0通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER10,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN0, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER10-SOC10
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER10,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER11-SOC11，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN1-采用A1通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER11,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN1, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER11-SOC11
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER11,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER12-SOC12，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN3-采用A3通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER12,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN3, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER12-SOC12
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER12,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER13-SOC13，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN0-采用A0通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER13,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN0, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER13-SOC13
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER13,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER14-SOC14，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN1-采用A1通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER14,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN1, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER14-SOC14
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER14,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER15-SOC15，ADC_TRIGGER_SW_ONLY-采样软件触发
	 * ADC_CH_ADCIN4-采用A4通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER15,
			ADC_TRIGGER_SW_ONLY, ADC_CH_ADCIN4, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER15-SOC15
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER15,
			ADC_INT_SOC_TRIGGER_NONE);


	/*配置SOC中断触源：ADC_INT_NUMBER1-ADCINT1
	 * 通过ADC_SOC_NUMBER9-SOC9触发*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1,
			ADC_SOC_NUMBER9);
	/*使能ADC中断：ADC_INT_NUMBER1-ADCINT1*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
	/*清ADC中断状态：ADC_INT_NUMBER1-ADCINT1*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
	/*关闭ADC连续模式：ADC_INT_NUMBER1-ADCINT1*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);

	/*配置SOC中断触源：ADC_INT_NUMBER2-ADCINT2
	 * 通过ADC_SOC_NUMBER12-SOC12触发*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER2,
			ADC_SOC_NUMBER12);
	/*使能ADC中断：ADC_INT_NUMBER2-ADCINT2*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER2);
	/*清ADC中断状态：ADC_INT_NUMBER2-ADCINT2*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);
	/*关闭ADC连续模式：ADC_INT_NUMBER2-ADCINT2*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER2);


	/*配置SOC中断触源：ADC_INT_NUMBER3-ADCINT3
	 * 通过ADC_SOC_NUMBER15-SOC15触发*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER3,
			ADC_SOC_NUMBER15);
	/*使能ADC中断：ADC_INT_NUMBER3-ADCINT3*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER3);
	/*清ADC中断状态：ADC_INT_NUMBER3-ADCINT3*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER3);
	/*关闭ADC连续模式：ADC_INT_NUMBER3-ADCINT3*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER3);
}

//
// Globals
//
uint16_t adcAResult0;
uint16_t adcAResult1;
uint16_t adcAResult2;
uint16_t adcAResult3;
uint16_t adcAResult4;

uint16_t adcA0Result[3];
uint16_t adcA1Result[3];
uint16_t adcA2Result[3];
uint16_t adcA3Result[3];
uint16_t adcA4Result[3];

__interrupt void adcABurstISR(void)
{
    uint16_t rrPointer;
    ADC_IntNumber burstIntSource;
    uint16_t i;


    /*读取SOC寄存器当前优先级，以判断突发模式对应的SOC组合*/
    rrPointer = (HWREG(ADCA_BASE + ADC_O_SOCPRICTL) & 0x03E0) >> 5;
    switch(rrPointer){

    /*若优先级指向SOC9，则读取SOC7-SOC9采样结果*/
    case 9:
    	for(i=0;i<3;i++)
    	{
    		adcA0Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER7);
    		if(i>=2)
    		{
    			adcAResult0=adcA0Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcA1Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER8);
    		if(i>=2)
    		{
    			adcAResult1=adcA1Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcAResult2 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER9);
    		if(i>=2)
    		{
    			adcAResult2=adcA2Result[i];
    		}
    	}
       burstIntSource = ADC_INT_NUMBER1;
    break;

    /*若优先级指向SOC12，则读取SOC10-SOC12采样结果*/
    case 12:
    	for(i=0;i<3;i++)
    	{
    		adcA0Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER10);
    		if(i>=2)
    		{
    			adcAResult0=adcA0Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcA1Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER11);
    		if(i>=2)
    		{
    			adcAResult1=adcA1Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcAResult3 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER12);
    		if(i>=2)
    		{
    			adcAResult3=adcA3Result[i];
    		}
    	}
       burstIntSource = ADC_INT_NUMBER2;
    break;

    /*若优先级指向SOC15，则读取SOC13-SOC15采样结果*/
    case 15:
    	for(i=0;i<3;i++)
    	{
    		adcA0Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER13);
    		if(i>=2)
    		{
    			adcAResult0=adcA0Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcA1Result[i]=ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14);
    		if(i>=2)
    		{
    			adcAResult1=adcA1Result[i];
    		}
    	}

    	for(i=0;i<3;i++)
    	{
    		adcAResult4 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER15);
    		if(i>=2)
    		{
    			adcAResult4=adcA4Result[i];
    		}
    	}
       burstIntSource = ADC_INT_NUMBER3;
    break;

    default:
       ESTOP0; //handle error for unexpected RR pointer value
    }


    /*清除对应中断INT_ADCA1/A2/A3*/
    ADC_clearInterruptStatus(ADCA_BASE, burstIntSource);

    /*检测中断是否发生溢出，若溢出则清除溢出与中断状态*/
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, burstIntSource))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, burstIntSource);
        ADC_clearInterruptStatus(ADCA_BASE, burstIntSource);
    }

    /*清除对应中断应答PIEACK*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP10);
}
