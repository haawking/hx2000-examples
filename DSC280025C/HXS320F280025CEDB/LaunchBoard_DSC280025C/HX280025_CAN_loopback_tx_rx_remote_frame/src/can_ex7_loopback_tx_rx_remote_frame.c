/******************************************************************
 文 档 名：       HX_DSC280025_CAN_loopback_tx_rx_remote_frame
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      CAN远程帧传输
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，通过CAN远程帧传输

连线：无
 现象:
 * rxMsgData 接收数据
 * txMsgData 发送数据
 *
 版 本：      V1.0.0
 时 间：      2023年2月24日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"


uint16_t rxMsgData[8];
uint16_t txMsgData[8];
uint16_t i;
//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();
    /*配置GPIO31/34为IO输出，以指示数据传输状态*/
    setup1GPIO();

    EALLOW;
    /*CAN的IO引脚配置:GPIO12-CANA_RX接收
     * GPIO13-CANA_TX发送*/
    CAN_IOinit();
    /*CAN模块配置*/
    CAN_init();
    EDIS;


    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*打开全局中断*/
    EINT;
    ERTM;

    /*CAN模块启动*/
    CAN_startModule(CANA_BASE);

    /*接收缓冲区清零*/
    *(uint16_t *)rxMsgData = 0;
    /*等待CAN模块空闲*/
    while((HWREGH(CANA_BASE + CAN_O_IF1CMD) & CAN_IF1CMD_BUSY) ==
          CAN_IF1CMD_BUSY)
    {}

    /*写入CAN模块IF1DATA/B数据*/
    HWREG_BP(CANA_BASE + CAN_O_IF1DATA)  =  0x76543210UL;
    HWREG_BP(CANA_BASE + CAN_O_IF1DATB)  =  0xFEDCBA98UL;


    /*写入MBX RAM
     * 配置信箱2回复远程帧，长度8字节*/
    HWREG_BP(CANA_BASE + CAN_O_IF1CMD)  =  0x00830002UL;

    /*等待CAN模块空闲*/
    while((HWREGH(CANA_BASE + CAN_O_IF1CMD) & CAN_IF1CMD_BUSY) ==
           CAN_IF1CMD_BUSY)
    {}

    for(;;)
    {
    	/*CAN发送远程帧请求:邮箱-3*/
        CAN_sendRemoteRequestMessage(CANA_BASE, 3);
        /*延时1s以准备好接收*/
        DEVICE_DELAY_US(1000000);

        /*读取信箱1的接收数据rxMsgData*/
        if (CAN_readMessage(CANA_BASE, 1, rxMsgData))
        {
        	/*发送txMsgData数据：发送信箱-3,数据长度：4字节，
             * 发送数据txMsgData*/
            CAN_sendMessage(CANA_BASE, 3, MSG_DATA_LENGTH,
                            txMsgData);
        	for(i=0;i<8;i++)
        	{
        		if(txMsgData[i]==rxMsgData[i])
        		{
        			GPIO_writePin(31, 0);
        		}
        	}
        }
        else
        {
         	GPIO_writePin(34, 0);
        }
    }
}

//
// End of File
//
