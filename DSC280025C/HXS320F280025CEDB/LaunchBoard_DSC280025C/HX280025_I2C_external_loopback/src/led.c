#include "system.h"

void InitLED(void)
{
    EALLOW;
	/*配置GPIO31为IO输出*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);

	/*配置GPIO34为IO输出*/
    GPIO_setPinConfig(GPIO_34_GPIO34);
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);

	GPIO_writePin(31,1);
	GPIO_writePin(34,1);
//	/*配置GPIO1为IO输出，以控制I2C传输*/
//    GPIO_setPadConfig(DEVICE_GPIO_PIN_LED1, GPIO_PIN_TYPE_STD);
//    GPIO_setDirectionMode(DEVICE_GPIO_PIN_LED1, GPIO_DIR_MODE_OUT);
    EDIS;
}
