/******************************************************************
 文 档 名：       HX_DSC280025_I2C_external_loopback
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      I2C串行寻址通讯
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 （1）采用I2CA作为主机，I2CB为从机
 （2）由I2CA主机向I2B从机发送数据，
 （3）I2CB作为从机，接收主机发送的数据
连线 GPIO26/SDAA-GPIO2/SDAB
GPIO27/SCLA-GPIO3/SCLB

现象：接收数据与发送数据一致，则GPIO34/LED2灯亮
不一致，则GPIO31/LED1灯亮
 *
 版 本：      V1.0.0
 时 间：      2023年3月2日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

uint16_t sData[2] = {0,0};                  // Send data buffer
uint16_t rData[2] = {0,0};                  // Receive data buffer
uint16_t rDataPoint = 0;                    // To keep track of where we are in the
                                            // data stream to check received data


//
// Main
//
void main(void)
{
    uint16_t i;
    /*系统时钟初始化*/
    Device_init();
    /*GPIO锁定配置解除*/
    Device_initGPIO();
    /*配置GPIO26-SDAA GPIO27-SCLA*/
    I2CA_GPIO();
    /*配置GPIO2-SDAB GPIO3-SCLB*/
    I2CB_GPIO();
    /*配置GPIO31为IO输出，以指示传输状态*/
    InitLED();

    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口地址INT_I2CA_FIFO,指向执行i2cFIFOISR中断服务程序*/
    Interrupt_register(INT_I2CA_FIFO, &i2cFIFOISR);
    /*中断入口地址INT_I2CB_FIFO,指向执行i2cFIFOISR中断服务程序*/
    Interrupt_register(INT_I2CB_FIFO, &i2cFIFOISR);


    /*配置I2C的FIFO功能*/
    initI2CFIFO();

    /*初始化数据缓冲区*/
    for(i = 0; i < 2; i++)
    {
        sData[i] = i;
        rData[i]= 0;
    }

    /*中断使能INT_I2CA_FIFO*/
    Interrupt_enable(INT_I2CA_FIFO);
    /*中断使能INT_I2CB_FIFO*/
    Interrupt_enable(INT_I2CB_FIFO);

    /*打开全局中断*/
    EINT;
    ERTM;

    while(1)
    {
    }
}

//
// End of File
//

