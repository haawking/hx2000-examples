/******************************************************************************************
 文 档 名：       HX_DSC280025_dcc_ex1_single_shot_verification
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：           DSC280025
 使 用 库：
 作     用：      DCC single shot时钟校验
 说     明：       FLASH工程
 -------------------------- 例程使用说明 --------------------------------------------------
使用XTAL时钟作为参考时钟PLLRAW时钟的频率。

 双时钟比较器模块DCC0用于时钟校验。

 clocksource0是参考时钟(Fclk0 = 20Mhz)
clocksource1是需要校验的时钟(Fclk1 = 160Mhz)。

Seed是加载到Counter中的值。


外部连接：
-无
观测变量：
status/result - PLLRAW时钟校验状态
result=Pass=0测试通过

-------------------------------------------------------------------------------------------
版 本：V1.0.0
 时 间：2023年1月1日
 作 者：
 @ mail：support@mail.haawking.com
******************************************************************************************/
// Included
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"

// Defines
#define PASS 0
#define FAIL 1


// Globals
uint32_t result = FAIL;


// Main
int main(void)
{
    bool status=0;

    //初始化设备时钟和外设
    Device_init();

    //初始化PIE和清除PIE寄存器。关闭CPU中断
    Interrupt_initModule();

    //初始化PIE向量表
    Interrupt_initVectorTable();

    //启用全局中断（INTM）和实时中断（DBGM）
    EINT;
    ERTM;

    //
    // 使用XTAL作为参考时钟，验证PLL时钟的频率
    // FClk1 = PLL 频率 = 160MHz
    // FClk0 = XTAL 频率 = 20MHz
    // 公差 = 1%
    // 容许频率公差 = 0% (根据XTAL频率中的错误更新)
    // SysClk Freq = 160MHz
    //
    // 注意:如果使用不同的PLL或XTAL，参考TRM更新参数频率
    //
    status = DCC_verifyClockFrequency(DCC0_BASE,
                                      DCC_COUNT1SRC_PLL, 160.0F,
                                      DCC_COUNT0SRC_XTAL, 20.0F,
                                      1.0F, 0.0F, 160.0F);


    //PLLRAW时钟校验状态
    if (!status)
    {
        result = FAIL;
    }
    else
    {
        result = PASS;
    }

    ESTOP0;

    return 0;
}

