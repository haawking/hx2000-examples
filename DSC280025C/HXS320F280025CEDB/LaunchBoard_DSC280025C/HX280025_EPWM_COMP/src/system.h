/*
 * system.h
 *
 *  Created on: 2023��2��9��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"


//
// Globals
//
extern uint32_t  epwm1TZIntCount;
//
// Function Prototypes
//
__interrupt void epwm1TZISR(void);

/*EPWM��GPIO��������*/
void epwm_gpio(void);
/*EPWM�Ĵ�������TZ��������*/
void epwm_tz_gpio(void);
/*ͬ������*/
void sync_init(void);
/*EPWM����*/
void epwm_config(void);


__interrupt void adcA1ISR(void);

/*����AdcAioģ������������*/
void InitAdcAio(void);
/*����Adc�ο���ѹ*/
void InitAdc(void);
/*����Adcģ��*/
void Adc_config(void);

extern uint16_t myADC0;

void comp_gpio(void);
void comp_init(void);

#endif /* SYSTEM_H_ */
