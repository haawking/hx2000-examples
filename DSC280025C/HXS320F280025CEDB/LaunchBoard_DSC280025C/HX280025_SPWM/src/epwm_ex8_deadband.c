/******************************************************************
 文 档 名：       HX_DSC280025_SPWM
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM相位偏移与死区输出
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，
 采用EPWM1-3输出频率13kHz，相位偏移120度的三组六路PWM波，带有2us死区
现象：

 *
 版 本：      V1.0.0
 时 间：      2023年1月31日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/



#include "system.h"

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*中断入口地址INT_EPWM1，指向执行epwm1ISR中断服务程序*/
    Interrupt_register(INT_EPWM1, &epwm1ISR);

	/*屏蔽TBCLK时基同步，便于PWM初始化配置写入*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);
    
    EALLOW;
	/*配置GPIO0为EPWM1A,GPIO1为EPWM1B*/
	/*配置GPIO2为EPWM2A,GPIO3为EPWM2B*/
	/*配置GPIO4为EPWM3A,GPIO5为EPWM3B*/
    epwm_gpio();
	/*同步策略*/
    sync_config();
	/*EPWM配置*/
    epwm1_config();
    epwm2_config();
    epwm3_config();
    EDIS;

	/*使能TBCLK时基同步，以使PWM配置写入，并实现多PWM同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

	/*使能打开INT_EPWM1中断*/
    Interrupt_enable(INT_EPWM1);

	/*打开全局中断*/
    EINT;
    ERTM;

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}


