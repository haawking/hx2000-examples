/*
 * system.h
 *
 *  Created on: 2023年2月23日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Function Prototypes
//
void initEPWM();
__interrupt void adcA1ISR(void);

//
// Globals
//
extern uint16_t adcAResult0[3];
extern uint16_t adcAResult1[3];
extern uint16_t adcAResult2[3];
extern uint16_t adcAResult3[3];
extern uint16_t adcAResult4[3];
extern uint16_t adcAResult5[3];
extern volatile uint16_t isrCount;

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);

#endif /* SYSTEM_H_ */
