//###########################################################################
//
// FILE:    hw_hrcap.h
//
// TITLE:   Definitions for the HRCAP registers.
//
//###########################################################################
// $HAAWKING Release: DSP28002x Support Library V1.0.4 $
// $Release Date: 2023-03-14 07:25:04 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#ifndef HW_HRCAP_H
#define HW_HRCAP_H

//*************************************************************************************************
//
// The following are defines for the HRCAP register offsets
//
//*************************************************************************************************
#define HRCAP_A_HCCTL           0x0U
#define HRCAP_A_HCIFR           0x4U
#define HRCAP_A_HCICLR          0x8U
#define HRCAP_A_HCIFRC          0xCU
#define HRCAP_A_HCCOUNTER       0x10U
#define HRCAP_A_HCCAL           0x14U
#define HRCAP_A_HCCALMEP        0x18U
#define HRCAP_A_HCMEPSTATUS     0x1CU
#define HRCAP_A_HCCAPCNTRISE0   0x40U
#define HRCAP_A_HCCAPCNTFALL0   0x48U
#define HRCAP_A_HCCAPCNTRISE1   0x60U
#define HRCAP_A_HCCAPCNTFALL1   0x68U

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCCTL register
//
//*************************************************************************************************
#define HRCAP_HCCTL_SOFTRESET_S    0U
#define HRCAP_HCCTL_SOFTRESET_M    0x1U    // Writing "1" to this bit will clear HCCOUNTER,
                                           // all capture registers and IFR register bits.
#define HRCAP_HCCTL_RISEINTE_S     1U
#define HRCAP_HCCTL_RISEINTE_M     0x2U    // Rising edge capture interrupt enable bit
                                           // 0:Disable rising edge capture interrupt
                                           // 1:Enable rising edge capture interrupt
#define HRCAP_HCCTL_FALLINTE_S     2U
#define HRCAP_HCCTL_FALLINTE_M     0x4U    // Falling edge capture interrupt enable bit
                                           // 0:Disable falling edge capture interrupt
                                           // 1:Enable falling edge capture interrupt
#define HRCAP_HCCTL_OVFINTE_S      3U
#define HRCAP_HCCTL_OVFINTE_M      0x8U    // Counter overflow interrupt enable bit
                                           // 0:Disable counter overflow interrupt
                                           // 1:Enable counter overflow interrupt
#define HRCAP_HCCTL_HCCAPCLKSEL_S  8U
#define HRCAP_HCCTL_HCCAPCLKSEL_M  0x100U  // 0:HCCAPCLK=SYSCLK
                                           // 1:HCCAPCLK=PLLCLK

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCIFR register
//
//*************************************************************************************************
#define HRCAP_HCIFR_INT_S           0U
#define HRCAP_HCIFR_INT_M           0x1U   // Global interrupt flag
                                           // 0:No HRCAP interrupt occurred
                                           // 1:Enable rise, fall or CONTROVF interrupt has been generated.
#define HRCAP_HCIFR_RISE_S          1U
#define HRCAP_HCIFR_RISE_M          0x2U   // Rising edge capture interrupt flag
                                           // 0:No Rising edge interruption
                                           // 1:A Rising edge input capture event occurs.
#define HRCAP_HCIFR_FALL_S          2U
#define HRCAP_HCIFR_FALL_M          0x4U   // Falling edge capture interrupt flag
                                           // 0:No Falling edge interruption
                                           // 1:A Falling edge input capture event occurs.
#define HRCAP_HCIFR_COUNTEROVF_S    3U
#define HRCAP_HCIFR_COUNTEROVF_M    0x8U   // Counter overflow interrupt flag
                                           // 0:HCCOUNTER does not overflow
                                           // 1:When 16-bit HCCOUNTER overflows , this position is 1.
#define HRCAP_HCIFR_RISEOVF_S       4U
#define HRCAP_HCIFR_RISEOVF_M       0x10U  // Rising edge interrupt overflow event flag
                                           // 0:No rising edge interrupt overflow event occurred.
                                           // 1:When a new rising event occurs, if the rising flag is "1", the bit is set to "1".

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCICLR register
//
//*************************************************************************************************
#define HRCAP_HCICLR_INT_S           0U
#define HRCAP_HCICLR_INT_M           0x1U   // Global interrupt clear bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will clear the corresponding
                                            // INT flag bit in the HCIFR register to "0".
#define HRCAP_HCICLR_RISE_S          1U
#define HRCAP_HCICLR_RISE_M          0x2U   // Rising edge capture interrupt clear bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will clear the corresponding
                                            // rising flag bit in the HCIFR register to "0".
#define HRCAP_HCICLR_FALL_S          2U
#define HRCAP_HCICLR_FALL_M          0x4U   // Falling edge capture interrupt clear bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will clear the corresponding
                                            // falling flag bit in the HCIFR register to "0".
#define HRCAP_HCICLR_COUNTEROVF_S    3U
#define HRCAP_HCICLR_COUNTEROVF_M    0x8U   // Counter overflow interrupt clear bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will clear the corresponding
                                            // CONTROLVF flag bit in the HCIFR register to "0".
#define HRCAP_HCICLR_RISEOVF_S       4U
#define HRCAP_HCICLR_RISEOVF_M       0x10U  // Rising edge interrupt overflow clear bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will clear the corresponding
                                            // RISEOVF flag bit in the HCIFR register to "0".

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCIFRC register
//
//*************************************************************************************************
#define HRCAP_HCIFRC_RISE_S          1U
#define HRCAP_HCIFRC_RISE_M          0x2U   // Rising edge interrupt forcing bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will force the corresponding
                                            // rising flag bit in the HCIFR register to be "1".
#define HRCAP_HCIFRC_FALL_S          2U
#define HRCAP_HCIFRC_FALL_M          0x4U   // Falling edge interrupt forcing bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will force the corresponding
                                            // falling flag bit in the HCIFR register to be "1".
#define HRCAP_HCIFRC_COUNTEROVF_S    3U
#define HRCAP_HCIFRC_COUNTEROVF_M    0x8U   // Counter overflow interrupt forcing bit
                                            // 0:Writing of '0' will be ignored. This bit is always "0".
                                            // 1:Writing "1" to this bit will force the corresponding
                                            // CONTROLVF flag bit in the HCIFR register to be "1".

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCCAL register
//
//*************************************************************************************************
#define HRCAP_HCCAL_DLL_START_POINTS_S       0U
#define HRCAP_HCCAL_DLL_START_POINTS_M       0xFFU    // Dll initial value
#define HRCAP_HCCAL_PHASE_DETECT_SEL_S       8U
#define HRCAP_HCCAL_PHASE_DETECT_SEL_M       0x700U   //And DLL START Point is used together to calculate
                                // how many delay units need to be used in series to detect a clock cycle.
#define HRCAP_HCCAL_HRCAPMODE_S              14
#define HRCAP_HCCAL_HRCAPMODE_M              0x4000U  //When HRPWMSEL is equal to 1, HRCAP belongs to
                                                      //high-precision calibration mode;
                                                      //When HRPWMSEL is equal to 0 and HRCAPMODE is
                                                      //equal to 0, HRCAP belongs to normal mode
#define HRCAP_HCCAL_HRPWMSEL_S               15
#define HRCAP_HCCAL_HRPWMSEL_M               0x8000U  //When HRPWMSEL is equal to 1, HRCAP belongs
                                                      //to high-precision calibration mode.
                                                      //When HRPWMSEL is equal to 0, HRCAP belongs to capture mode.

//*************************************************************************************************
//
// The following are defines for the bit fields in the HCMEPSTATUS register
//
//*************************************************************************************************
#define HRCAP_HCMEPSTATUS_LOCK_S                 0U
#define HRCAP_HCMEPSTATUS_LOCK_M                 0x3U    // Lock status bit
#define HRCAP_HCMEPSTATUS_MEP_SCALE_FACTOR_S     2U
#define HRCAP_HCMEPSTATUS_MEP_SCALE_FACTOR_M     0x3FCU  // MEP Scale Factor bit




#endif
