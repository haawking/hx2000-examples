//###########################################################################
//
// FILE:   version.c
//
// TITLE:  API to return the version number of the driverlib.lib in use.
//
//###########################################################################
// $HAAWKING Release: DSP28002x Support Library V1.0.4 $
// $Release Date: 2023-03-14 07:25:04 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "version.h"

//*****************************************************************************
//
// Version_getLibVersion
//
//*****************************************************************************
uint32_t
Version_getLibVersion(void)
{
    return(VERSION_NUMBER);
}
