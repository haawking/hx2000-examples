/******************************************************************
 文 档 名：       HX0025_Sci_Loopback
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      Sci内回环测试
 说     明：      RAM工程
 -------------------------- 例程使用说明 --------------------------
  该程序采用外围设备的内部回环测试模式。除了启动模式引脚配置，不需要其他硬件配置。
     pinmux和SCI模块通过sysconfig文件配置。
本测试使用SCI模块的回环测试模式，发送从0x00到0xFF的字符
测试将发送一个字符，然后检查接收缓冲区是否一致。

观察变量
   loopCount - 发送字符数
   errorCount - 检测到的错误数
   sendChar - 字符发送
   receivedChar -字符接收

   Success LED1 GPIO31亮表示正常
   Fail    LED2 GPIO34亮表示失败
****************************************************************************************/
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "board.h"

#include"system.h"
//
// Globals
//
uint16_t loopCount;
uint16_t errorCount;

uint16_t sendChar;
uint16_t receivedChar;

//
// Function Prototypes
//
void error();

#define MB_ADDR 0x21FFC

void Success(void)
{

 GPIO_writePin(31,0);
 GPIO_writePin(34,1);
 HWREG(MB_ADDR) = 0x5555AAAA;

}

void Fail(void)
{
 GPIO_writePin(31,1);
 GPIO_writePin(34,0);
 HWREG(MB_ADDR) = 0xAAAA5555;
}

//
// Main
//
int main(void)
{
//    uint16_t sendChar;
//    uint16_t receivedChar;

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Setup GPIO by disabling pin locks and enabling pullups
    //
    Device_initGPIO();
    setup1GPIO();
    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Board Initialization
    //
    Board_init();

    //
    // Enables CPU interrupts
    //
    Interrupt_enableMaster();

    //
    // Initialize counts
    //
    loopCount = 0;
    errorCount = 0;

    //
    // Send a character starting with 0
    //
    sendChar = 0;

    //
    // Send Characters forever starting with 0x00 and going through 0xFF.
    // After sending each, check the receive buffer for the correct value.
    //
    for(;;)
    {
        SCI_writeCharNonBlocking(mySCI0_BASE, sendChar);

        //
        // Wait for RRDY/RXFFST = 1 for 1 data available in FIFO
        //
        while(SCI_getRxFIFOStatus(mySCI0_BASE) == SCI_FIFO_RX0)
        {
            ;
        }

        //
        // Check received character
        //
        receivedChar = SCI_readCharBlockingFIFO(mySCI0_BASE);

        //
        // Received character not correct
        //
        if(receivedChar != sendChar)
        {
            errorCount++;
            //GPIO_writePin(34, 0); //LED2 GPIO34亮表示失败
            Fail();
            // asm("     ESTOP0");  // Uncomment to stop the test here
            //for (;;);
        }
        else
        {
        	 //GPIO_writePin(31, 0); //LED1 GPIO31亮表示成功
        	 Success();
        }

        //
        // Move to the next character and repeat the test
        //
        sendChar++;

        //
        // Limit the character to 8-bits
        //
        sendChar &= 0x00FF;
        loopCount++;

    }
    return 0;
}

//
// End of file
//
