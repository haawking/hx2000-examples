#include "system.h"



uint16_t epwm1_ctr;
uint16_t epwm2_ctr;
uint16_t epwm3_ctr;
uint16_t epwm4_ctr;

float32_t periodFine = 0.2;
int32 compCount;

__interrupt void epwm1ISR(void)
{
   epwm1_ctr++;

   for(periodFine = 0.2; periodFine < 0.9; periodFine += 0.01)
            {
                DEVICE_DELAY_US(1000);
                float32_t count = ((EPWM_TIMER_TBPRD-1) << 8UL) + (float32_t)(periodFine * 256);
                uint32_t compCount = count;
                HRPWM_setTimeBasePeriod(EPWM1_BASE, compCount);
            }
	EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}


__interrupt void epwm2ISR(void)
{
   epwm2_ctr++;

//   float32_t dutyFine = 50.0;
//   uint16_t i;
//	//
//    // Sweep DutyFine
//    //
//    for(dutyFine = MIN_HRPWM_DUTY_PERCENT; dutyFine < (100.0-MIN_HRPWM_DUTY_PERCENT); dutyFine += 0.01)
//    {
//        DEVICE_DELAY_US(1000);
//        for(i=1; i<LAST_EPWM_INDEX_FOR_EXAMPLE; i++)
//        {
//            float32_t count = ((100.0 - dutyFine) * (float32_t)(EPWM_TIMER_TBPRD << 8))/100.0;
//            uint32_t compCount = (count);
//            uint32_t hrCompCount = (compCount & (0x000000FF));
//            if (hrCompCount == 0)
//            {
//                //
//                // Add 1 to not have CMPxHR = 0
//                //
//                compCount |= 0x00000001;
//            }
//            HRPWM_setCounterCompareValue(EPWM2_BASE, HRPWM_COUNTER_COMPARE_A, compCount);
//#if CHANNEL_B_AS_ZRO_PRD_REF == 0
//            HRPWM_setCounterCompareValue(EPWM2_BASE, HRPWM_COUNTER_COMPARE_B, compCount);
//#endif
//        }
//    }


	EPWM_clearEventTriggerInterruptFlag(EPWM2_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}


__interrupt void epwm3ISR(void)
{
    epwm3_ctr++;

//    float32_t dutyFine = 50.0;
//    uint16_t i;
//
//	//
//    // Sweep DutyFine
//    //
//    for(dutyFine = MIN_HRPWM_DUTY_PERCENT; dutyFine < (100.0-MIN_HRPWM_DUTY_PERCENT); dutyFine += 0.01)
//    {
//        DEVICE_DELAY_US(1000);
//        for(i=1; i<LAST_EPWM_INDEX_FOR_EXAMPLE; i++)
//        {
//            float32_t count = ((100.0 - dutyFine) * (float32_t)(EPWM_TIMER_TBPRD << 8))/100.0;
//            uint32_t compCount = (count);
//            uint32_t hrCompCount = (compCount & (0x000000FF));
//            if (hrCompCount == 0)
//            {
//                //
//                // Add 1 to not have CMPxHR = 0
//                //
//                compCount |= 0x00000001;
//            }
//            HRPWM_setCounterCompareValue(EPWM3_BASE, HRPWM_COUNTER_COMPARE_A, compCount);
//#if CHANNEL_B_AS_ZRO_PRD_REF == 0
//            HRPWM_setCounterCompareValue(EPWM3_BASE, HRPWM_COUNTER_COMPARE_B, compCount);
//#endif
//        }
//    }

	EPWM_clearEventTriggerInterruptFlag(EPWM3_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}


__interrupt void epwm4ISR(void)
{
	epwm4_ctr++;

//	float32_t dutyFine = 50.0;
//	uint16_t i;
//
//    //
//    // Sweep DutyFine
//    //
//    for(dutyFine = MIN_HRPWM_DUTY_PERCENT; dutyFine < (100.0-MIN_HRPWM_DUTY_PERCENT); dutyFine += 0.01)
//    {
//        DEVICE_DELAY_US(1000);
//        for(i=1; i<LAST_EPWM_INDEX_FOR_EXAMPLE; i++)
//        {
//            float32_t count = ((100.0 - dutyFine) * (float32_t)(EPWM_TIMER_TBPRD << 8))/100.0;
//            uint32_t compCount = (count);
//            uint32_t hrCompCount = (compCount & (0x000000FF));
//            if (hrCompCount == 0)
//            {
//                //
//                // Add 1 to not have CMPxHR = 0
//                //
//                compCount |= 0x00000001;
//            }
//            HRPWM_setCounterCompareValue(EPWM4_BASE, HRPWM_COUNTER_COMPARE_A, compCount);
//#if CHANNEL_B_AS_ZRO_PRD_REF == 0
//            HRPWM_setCounterCompareValue(EPWM4_BASE, HRPWM_COUNTER_COMPARE_B, compCount);
//#endif
//        }
//    }

	EPWM_clearEventTriggerInterruptFlag(EPWM4_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}
