/******************************************************************
 文 档 名：       HX_DSC280025_HRPWM_TBPRDHR
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      HRPWM与EPWM的频率精度对比
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用HRPWM与EPWM输出频率800-806kHz
现象：
需连接高精度示波器或逻辑分析仪查看
EPWM1采用TBPRDHR输出，精度可达0.1kHz
EPWM2采用TBPRD输出，精度仅为8kHz
 *
 版 本：      V1.0.0
 时 间：      2023年1月31日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include <system.h>

int main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定配置解除*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    Interrupt_register(INT_EPWM1, &epwm1ISR);
    Interrupt_register(INT_EPWM2, &epwm2ISR);
    Interrupt_register(INT_EPWM3, &epwm3ISR);
    Interrupt_register(INT_EPWM4, &epwm4ISR);


	/*关闭时基时钟TBCLKSYNC同步使能，以便EPWM初始化*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EALLOW;
	/*配置GPIO0-GPIO7为EPWM1A-EPWM4B*/
    epwm_gpio();
    /*HRPWM配置:TBPRD=99-频率800kHz,CMPA=50,CMPB=50-占空比50%
     * 相位偏移：TBPHS=0,
     * 事件中断:EPWM_INT_TBCTR_ZERO-在CTR=0时触发,1-每周期触发一次中断
     * 通道选择：HRPWM_CHANNEL_A-通道A
     * MEP微边沿定位：HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE-双边沿定位*/
    hrpwm_config(EPWM1_BASE,99,50,50,0,
    		EPWM_INT_TBCTR_ZERO,1,
    		HRPWM_CHANNEL_A,HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE);
    /*HRPWM配置:TBPRD=99-频率800kHz,CMPA=50,CMPB=50-占空比50%
    * 相位偏移：TBPHS=0,
    * 事件中断:EPWM_INT_TBCTR_PERIOD-在CTR=周期值时触发,1-每周期触发一次中断
    * 通道选择：HRPWM_CHANNEL_A-通道A
     * MEP微边沿定位：HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE-双边沿定位*/
    hrpwm_config(EPWM2_BASE,99,50,50,0,
    		EPWM_INT_TBCTR_PERIOD,1,
    		HRPWM_CHANNEL_A,HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE);
    /*HRPWM配置:TBPRD=99-频率800kHz,CMPA=50,CMPB=50-占空比50%
     * 相位偏移：TBPHS=0,
     * 事件中断:EPWM_INT_TBCTR_U_CMPA-向上计数时CTR=CMPA时触发,1-每周期触发一次中断
     * 通道选择：HRPWM_CHANNEL_A-通道A
     * MEP微边沿定位：HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE-双边沿定位*/
    hrpwm_config(EPWM3_BASE,99,50,50,0,
    		EPWM_INT_TBCTR_U_CMPA,1,
    		HRPWM_CHANNEL_A,HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE);
   /*HRPWM配置:TBPRD=99-频率800kHz,CMPA=50,CMPB=50-占空比50%
     * 相位偏移：TBPHS=0,
     * 事件中断:EPWM_INT_TBCTR_D_CMPA-向下计数时CTR=CMPA时触发,1-每周期触发一次中断
     * 通道选择：HRPWM_CHANNEL_A-通道A
     * MEP微边沿定位：HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE-双边沿定位*/
    hrpwm_config(EPWM4_BASE,99,50,50,0,
    		EPWM_INT_TBCTR_D_CMPA,1,
    		HRPWM_CHANNEL_A,HRPWM_MEP_CTRL_RISING_AND_FALLING_EDGE);
    EDIS;

	/*使能时基时钟TBCLKSYNC同步，完成EPWM初始化*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    Interrupt_enable(INT_EPWM1);
    Interrupt_enable(INT_EPWM2);
    Interrupt_enable(INT_EPWM3);
    Interrupt_enable(INT_EPWM4);

	/*使能打开全局中断*/
    EINT;
    ERTM;


    for(;;)
    {

     }

    return 0;
}

