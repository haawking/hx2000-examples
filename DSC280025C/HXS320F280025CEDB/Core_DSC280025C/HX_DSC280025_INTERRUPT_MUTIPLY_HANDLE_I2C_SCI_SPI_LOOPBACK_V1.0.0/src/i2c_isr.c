/******************************************************************
 文 档 名：     i2c_isr.c
 D S P：       DSC280025
 使 用 库：
 作     用：
 说     明：      提供I2C的FIFO中断收发程序
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年11月23日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

__interrupt void i2cFIFOISR(void)
{
    uint16_t i;

    /*判断I2C的接收FIFO中断状态，若置位，读取接收到的数据*/
    if((I2C_getInterruptStatus(I2CA_BASE) & I2C_INT_RXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            /*从I2CDRR读取接收到的数据*/
            rDatai2cA[i] = I2C_getData(I2CA_BASE);
        }

        /*判断接收数据与发送数据一致,一致则点亮LED1,否则灭*/
        for(i = 0; i < 2; i++)
        {
            if(rDatai2cA[i] != ((rDataPoint + i) & 0xFF))
            {
                /*GPIO31写入1,初始化LED1灭*/
                GPIO_writePin(31, 1);
            }
            else
            {
                /*GPIO31写入0,点亮LED1*/
                GPIO_writePin(31, 0);
            }
        }

        rDataPoint = (rDataPoint + 1) & 0xFF;

        /*清除接收FIFO状态中断标志*/
        I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_RXFF);
    }

    /*判断发送FIFO中断标志，若置位，则向缓冲区发送数据*/
    else if((I2C_getInterruptStatus(I2CA_BASE) & I2C_INT_TXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            /*发送数据sDatai2cA[i]到缓冲区I2CDXR*/
            I2C_putData(I2CA_BASE, sDatai2cA[i]);
        }

        /*I2C发送启动条件*/
        I2C_sendStartCondition(I2CA_BASE);

        /*发送数据循环增加*/
        for(i = 0; i < 2; i++)
        {
           sDatai2cA[i] = (sDatai2cA[i] + 1) & 0xFF;
        }

        /*清除发送FIFO状态中断标志*/
        I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_TXFF);
    }

    /*清除中断应答：第8组*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}
