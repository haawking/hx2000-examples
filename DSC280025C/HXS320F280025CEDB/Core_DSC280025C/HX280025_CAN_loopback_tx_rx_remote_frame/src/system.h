/*
 * system.h
 *
 *  Created on: 2023年2月24日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define MSG_DATA_LENGTH    2

//
// Globals
//
extern volatile unsigned long msgCount;

extern uint16_t rxMsgData[8];
extern uint16_t txMsgData[8];

/*CAN的IO引脚配置:GPIO12-CANA_RX接收
 * GPIO13-CANA_TX发送*/
void CAN_IOinit(void);
/*CAN模块配置*/
void CAN_init(void);

void setup1GPIO(void);

#endif /* SYSTEM_H_ */
