#include "system.h"

void CAN_IOinit(void)
{
	/*配置GPIO12为CANA_RX接收*/
	GPIO_setPinConfig(GPIO_12_CANA_RX);
	GPIO_setPadConfig(12, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(12, GPIO_QUAL_ASYNC);
	/*配置GPIO13为CANA_TX发送*/
	GPIO_setPinConfig(GPIO_13_CANA_TX);
	GPIO_setPadConfig(13, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(13, GPIO_QUAL_ASYNC);
}

void CAN_init(void)
{
	/*CAN模块初始化*/
	CAN_initModule(CANA_BASE);

	/*CAN波特率配置：预分频，扩展分频，TSEG1=15，TSEG2=7，sjw=3*/
	CAN_setBitTiming(CANA_BASE, 7, 0, 15, 7, 3);

	/*CAN模块连接：CAN_TEST_EXL-外环连接*/
	CAN_enableTestMode(CANA_BASE, CAN_TEST_EXL);


	/*CAN模块配置：ID邮箱-1，ID信息码-5
	 * 帧类型：CAN_MSG_FRAME_EXT-扩展帧
	 * 信息类型：CAN_MSG_OBJ_TYPE_RX-接收帧
	 * ID掩码-0，信息标志-0，数据长度-8字节*/
	CAN_setupMessageObject(CANA_BASE, 1,
			5, CAN_MSG_FRAME_EXT,
			CAN_MSG_OBJ_TYPE_RX, 0, 0,8);


	/*CAN模块配置：ID邮箱-2，ID信息码-5
	 * 帧类型：CAN_MSG_FRAME_EXT-扩展帧
	 * 信息类型：CAN_MSG_OBJ_TYPE_RXTX_REMOTE-远程帧传输
	 * ID掩码-0，信息标志-0，数据长度-8字节*/
	CAN_setupMessageObject(CANA_BASE, 2,
			5, CAN_MSG_FRAME_EXT,
			CAN_MSG_OBJ_TYPE_RXTX_REMOTE, 0, 0,8);

	/*CAN模块配置：ID邮箱-3，ID信息码-7
	 * 帧类型：CAN_MSG_FRAME_EXT-扩展帧
	 * 信息类型：CAN_MSG_OBJ_TYPE_TX_REMOTE-远程帧发送
	 * ID掩码-0，信息标志-0，数据长度-0字节*/
	CAN_setupMessageObject(CANA_BASE, 3,
			7, CAN_MSG_FRAME_EXT,
			CAN_MSG_OBJ_TYPE_TX_REMOTE, 0, 0,0);

	/*CAN模块启动*/
	CAN_startModule(CANA_BASE);
}
