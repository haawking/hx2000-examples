//###########################################################################
//
// FILE:   hrpwm.c
//
// TITLE:  H28x HRPWM driver.
//
//###########################################################################
// $HAAWKING Release: DSP28002x Support Library V1.0.1 $
// $Release Date: 2023-02-17 10:29:58 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#include "hrpwm.h"

//
// All the API functions are in-lined in hrpwm.h
//
