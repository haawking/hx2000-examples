#include "system.h"

uint32_t phaseFine=1;
uint32_t phaseref;

__interrupt void epwm1ISR(void)
{
	/*拓宽相位精度，以0.18ns步长精调*/
   for(phaseFine=1;phaseFine<256;phaseFine++)
   {
	   phaseref=(EPWM3_TIMER_TBPHS<<8)+phaseFine;
	   HRPWM_setPhaseShift(EPWM3_BASE,phaseref);
   }

	EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}
