//#################################################################################################################
//
// FILE:    hw_pie.h
//
// TITLE:   Definitions for the PIE registers.
//
//#################################################################################################################
// $HAAWKING Release: Hal Driver Library V1.0.5 $
// $Release Date: 2023.04.27 16:42:29 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//#################################################################################################################

#ifndef HW_PIE_H
#define HW_PIE_H

//*************************************************************************************************
//
// The following are defines for the PIE register offsets
//
//*************************************************************************************************
#define PIE_O_CTRL    0x0U    // ePIE Control Register
#define PIE_O_ACK     0x4U    // Interrupt Acknowledge Register
#define PIE_O_IER1    0x8U    // Interrupt Group 1 Enable Register
#define PIE_O_IFR1    0xCU    // Interrupt Group 1 Flag Register
#define PIE_O_IER2    0x10U   // Interrupt Group 2 Enable Register
#define PIE_O_IFR2    0x14U   // Interrupt Group 2 Flag Register
#define PIE_O_IER3    0x18U   // Interrupt Group 3 Enable Register
#define PIE_O_IFR3    0x1CU   // Interrupt Group 3 Flag Register
#define PIE_O_IER4    0x20U   // Interrupt Group 4 Enable Register
#define PIE_O_IFR4    0x24U   // Interrupt Group 4 Flag Register
#define PIE_O_IER5    0x28U   // Interrupt Group 5 Enable Register
#define PIE_O_IFR5    0x2CU   // Interrupt Group 5 Flag Register
#define PIE_O_IER6    0x30U   // Interrupt Group 6 Enable Register
#define PIE_O_IFR6    0x34U   // Interrupt Group 6 Flag Register
#define PIE_O_IER7    0x38U   // Interrupt Group 7 Enable Register
#define PIE_O_IFR7    0x3CU   // Interrupt Group 7 Flag Register
#define PIE_O_IER8    0x40U   // Interrupt Group 8 Enable Register
#define PIE_O_IFR8    0x44U   // Interrupt Group 8 Flag Register
#define PIE_O_IER9    0x48U   // Interrupt Group 9 Enable Register
#define PIE_O_IFR9    0x4CU   // Interrupt Group 9 Flag Register
#define PIE_O_IER10   0x50U   // Interrupt Group 10 Enable Register
#define PIE_O_IFR10   0x54U   // Interrupt Group 10 Flag Register
#define PIE_O_IER11   0x58U   // Interrupt Group 11 Enable Register
#define PIE_O_IFR11   0x5CU   // Interrupt Group 11 Flag Register
#define PIE_O_IER12   0x60U   // Interrupt Group 12 Enable Register
#define PIE_O_IFR12   0x64U   // Interrupt Group 12 Flag Register


//*************************************************************************************************
//
// The following are defines for the bit fields in the PIECTRL register
//
//*************************************************************************************************
#define PIE_CTRL_ENPIE       0x1U      // PIE Enable
#define PIE_CTRL_PIEVECT_S   1U
#define PIE_CTRL_PIEVECT_M   0xFFFEU   // PIE Vector Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEACK register
//
//*************************************************************************************************
#define PIE_PIEACK_ACK1    0x1U     // Acknowledge PIE Interrupt Group 1
#define PIE_PIEACK_ACK2    0x2U     // Acknowledge PIE Interrupt Group 2
#define PIE_PIEACK_ACK3    0x4U     // Acknowledge PIE Interrupt Group 3
#define PIE_PIEACK_ACK4    0x8U     // Acknowledge PIE Interrupt Group 4
#define PIE_PIEACK_ACK5    0x10U    // Acknowledge PIE Interrupt Group 5
#define PIE_PIEACK_ACK6    0x20U    // Acknowledge PIE Interrupt Group 6
#define PIE_PIEACK_ACK7    0x40U    // Acknowledge PIE Interrupt Group 7
#define PIE_PIEACK_ACK8    0x80U    // Acknowledge PIE Interrupt Group 8
#define PIE_PIEACK_ACK9    0x100U   // Acknowledge PIE Interrupt Group 9
#define PIE_PIEACK_ACK10   0x200U   // Acknowledge PIE Interrupt Group 10
#define PIE_PIEACK_ACK11   0x400U   // Acknowledge PIE Interrupt Group 11
#define PIE_PIEACK_ACK12   0x800U   // Acknowledge PIE Interrupt Group 12

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER1 register
//
//*************************************************************************************************
#define PIE_PIEIER1_INTX1    0x1U      // Enable for Interrupt 1.1
#define PIE_PIEIER1_INTX2    0x2U      // Enable for Interrupt 1.2
#define PIE_PIEIER1_INTX3    0x4U      // Enable for Interrupt 1.3
#define PIE_PIEIER1_INTX4    0x8U      // Enable for Interrupt 1.4
#define PIE_PIEIER1_INTX5    0x10U     // Enable for Interrupt 1.5
#define PIE_PIEIER1_INTX6    0x20U     // Enable for Interrupt 1.6
#define PIE_PIEIER1_INTX7    0x40U     // Enable for Interrupt 1.7
#define PIE_PIEIER1_INTX8    0x80U     // Enable for Interrupt 1.8
#define PIE_PIEIER1_INTX9    0x100U    // Enable for Interrupt 1.9
#define PIE_PIEIER1_INTX10   0x200U    // Enable for Interrupt 1.10
#define PIE_PIEIER1_INTX11   0x400U    // Enable for Interrupt 1.11
#define PIE_PIEIER1_INTX12   0x800U    // Enable for Interrupt 1.12
#define PIE_PIEIER1_INTX13   0x1000U   // Enable for Interrupt 1.13
#define PIE_PIEIER1_INTX14   0x2000U   // Enable for Interrupt 1.14
#define PIE_PIEIER1_INTX15   0x4000U   // Enable for Interrupt 1.15
#define PIE_PIEIER1_INTX16   0x8000U   // Enable for Interrupt 1.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR1 register
//
//*************************************************************************************************
#define PIE_PIEIFR1_INTX1    0x1U      // Flag for Interrupt 1.1
#define PIE_PIEIFR1_INTX2    0x2U      // Flag for Interrupt 1.2
#define PIE_PIEIFR1_INTX3    0x4U      // Flag for Interrupt 1.3
#define PIE_PIEIFR1_INTX4    0x8U      // Flag for Interrupt 1.4
#define PIE_PIEIFR1_INTX5    0x10U     // Flag for Interrupt 1.5
#define PIE_PIEIFR1_INTX6    0x20U     // Flag for Interrupt 1.6
#define PIE_PIEIFR1_INTX7    0x40U     // Flag for Interrupt 1.7
#define PIE_PIEIFR1_INTX8    0x80U     // Flag for Interrupt 1.8
#define PIE_PIEIFR1_INTX9    0x100U    // Flag for Interrupt 1.9
#define PIE_PIEIFR1_INTX10   0x200U    // Flag for Interrupt 1.10
#define PIE_PIEIFR1_INTX11   0x400U    // Flag for Interrupt 1.11
#define PIE_PIEIFR1_INTX12   0x800U    // Flag for Interrupt 1.12
#define PIE_PIEIFR1_INTX13   0x1000U   // Flag for Interrupt 1.13
#define PIE_PIEIFR1_INTX14   0x2000U   // Flag for Interrupt 1.14
#define PIE_PIEIFR1_INTX15   0x4000U   // Flag for Interrupt 1.15
#define PIE_PIEIFR1_INTX16   0x8000U   // Flag for Interrupt 1.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER2 register
//
//*************************************************************************************************
#define PIE_PIEIER2_INTX1    0x1U      // Enable for Interrupt 2.1
#define PIE_PIEIER2_INTX2    0x2U      // Enable for Interrupt 2.2
#define PIE_PIEIER2_INTX3    0x4U      // Enable for Interrupt 2.3
#define PIE_PIEIER2_INTX4    0x8U      // Enable for Interrupt 2.4
#define PIE_PIEIER2_INTX5    0x10U     // Enable for Interrupt 2.5
#define PIE_PIEIER2_INTX6    0x20U     // Enable for Interrupt 2.6
#define PIE_PIEIER2_INTX7    0x40U     // Enable for Interrupt 2.7
#define PIE_PIEIER2_INTX8    0x80U     // Enable for Interrupt 2.8
#define PIE_PIEIER2_INTX9    0x100U    // Enable for Interrupt 2.9
#define PIE_PIEIER2_INTX10   0x200U    // Enable for Interrupt 2.10
#define PIE_PIEIER2_INTX11   0x400U    // Enable for Interrupt 2.11
#define PIE_PIEIER2_INTX12   0x800U    // Enable for Interrupt 2.12
#define PIE_PIEIER2_INTX13   0x1000U   // Enable for Interrupt 2.13
#define PIE_PIEIER2_INTX14   0x2000U   // Enable for Interrupt 2.14
#define PIE_PIEIER2_INTX15   0x4000U   // Enable for Interrupt 2.15
#define PIE_PIEIER2_INTX16   0x8000U   // Enable for Interrupt 2.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR2 register
//
//*************************************************************************************************
#define PIE_PIEIFR2_INTX1    0x1U      // Flag for Interrupt 2.1
#define PIE_PIEIFR2_INTX2    0x2U      // Flag for Interrupt 2.2
#define PIE_PIEIFR2_INTX3    0x4U      // Flag for Interrupt 2.3
#define PIE_PIEIFR2_INTX4    0x8U      // Flag for Interrupt 2.4
#define PIE_PIEIFR2_INTX5    0x10U     // Flag for Interrupt 2.5
#define PIE_PIEIFR2_INTX6    0x20U     // Flag for Interrupt 2.6
#define PIE_PIEIFR2_INTX7    0x40U     // Flag for Interrupt 2.7
#define PIE_PIEIFR2_INTX8    0x80U     // Flag for Interrupt 2.8
#define PIE_PIEIFR2_INTX9    0x100U    // Flag for Interrupt 2.9
#define PIE_PIEIFR2_INTX10   0x200U    // Flag for Interrupt 2.10
#define PIE_PIEIFR2_INTX11   0x400U    // Flag for Interrupt 2.11
#define PIE_PIEIFR2_INTX12   0x800U    // Flag for Interrupt 2.12
#define PIE_PIEIFR2_INTX13   0x1000U   // Flag for Interrupt 2.13
#define PIE_PIEIFR2_INTX14   0x2000U   // Flag for Interrupt 2.14
#define PIE_PIEIFR2_INTX15   0x4000U   // Flag for Interrupt 2.15
#define PIE_PIEIFR2_INTX16   0x8000U   // Flag for Interrupt 2.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER3 register
//
//*************************************************************************************************
#define PIE_PIEIER3_INTX1    0x1U      // Enable for Interrupt 3.1
#define PIE_PIEIER3_INTX2    0x2U      // Enable for Interrupt 3.2
#define PIE_PIEIER3_INTX3    0x4U      // Enable for Interrupt 3.3
#define PIE_PIEIER3_INTX4    0x8U      // Enable for Interrupt 3.4
#define PIE_PIEIER3_INTX5    0x10U     // Enable for Interrupt 3.5
#define PIE_PIEIER3_INTX6    0x20U     // Enable for Interrupt 3.6
#define PIE_PIEIER3_INTX7    0x40U     // Enable for Interrupt 3.7
#define PIE_PIEIER3_INTX8    0x80U     // Enable for Interrupt 3.8
#define PIE_PIEIER3_INTX9    0x100U    // Enable for Interrupt 3.9
#define PIE_PIEIER3_INTX10   0x200U    // Enable for Interrupt 3.10
#define PIE_PIEIER3_INTX11   0x400U    // Enable for Interrupt 3.11
#define PIE_PIEIER3_INTX12   0x800U    // Enable for Interrupt 3.12
#define PIE_PIEIER3_INTX13   0x1000U   // Enable for Interrupt 3.13
#define PIE_PIEIER3_INTX14   0x2000U   // Enable for Interrupt 3.14
#define PIE_PIEIER3_INTX15   0x4000U   // Enable for Interrupt 3.15
#define PIE_PIEIER3_INTX16   0x8000U   // Enable for Interrupt 3.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR3 register
//
//*************************************************************************************************
#define PIE_PIEIFR3_INTX1    0x1U      // Flag for Interrupt 3.1
#define PIE_PIEIFR3_INTX2    0x2U      // Flag for Interrupt 3.2
#define PIE_PIEIFR3_INTX3    0x4U      // Flag for Interrupt 3.3
#define PIE_PIEIFR3_INTX4    0x8U      // Flag for Interrupt 3.4
#define PIE_PIEIFR3_INTX5    0x10U     // Flag for Interrupt 3.5
#define PIE_PIEIFR3_INTX6    0x20U     // Flag for Interrupt 3.6
#define PIE_PIEIFR3_INTX7    0x40U     // Flag for Interrupt 3.7
#define PIE_PIEIFR3_INTX8    0x80U     // Flag for Interrupt 3.8
#define PIE_PIEIFR3_INTX9    0x100U    // Flag for Interrupt 3.9
#define PIE_PIEIFR3_INTX10   0x200U    // Flag for Interrupt 3.10
#define PIE_PIEIFR3_INTX11   0x400U    // Flag for Interrupt 3.11
#define PIE_PIEIFR3_INTX12   0x800U    // Flag for Interrupt 3.12
#define PIE_PIEIFR3_INTX13   0x1000U   // Flag for Interrupt 3.13
#define PIE_PIEIFR3_INTX14   0x2000U   // Flag for Interrupt 3.14
#define PIE_PIEIFR3_INTX15   0x4000U   // Flag for Interrupt 3.15
#define PIE_PIEIFR3_INTX16   0x8000U   // Flag for Interrupt 3.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER4 register
//
//*************************************************************************************************
#define PIE_PIEIER4_INTX1    0x1U      // Enable for Interrupt 4.1
#define PIE_PIEIER4_INTX2    0x2U      // Enable for Interrupt 4.2
#define PIE_PIEIER4_INTX3    0x4U      // Enable for Interrupt 4.3
#define PIE_PIEIER4_INTX4    0x8U      // Enable for Interrupt 4.4
#define PIE_PIEIER4_INTX5    0x10U     // Enable for Interrupt 4.5
#define PIE_PIEIER4_INTX6    0x20U     // Enable for Interrupt 4.6
#define PIE_PIEIER4_INTX7    0x40U     // Enable for Interrupt 4.7
#define PIE_PIEIER4_INTX8    0x80U     // Enable for Interrupt 4.8
#define PIE_PIEIER4_INTX9    0x100U    // Enable for Interrupt 4.9
#define PIE_PIEIER4_INTX10   0x200U    // Enable for Interrupt 4.10
#define PIE_PIEIER4_INTX11   0x400U    // Enable for Interrupt 4.11
#define PIE_PIEIER4_INTX12   0x800U    // Enable for Interrupt 4.12
#define PIE_PIEIER4_INTX13   0x1000U   // Enable for Interrupt 4.13
#define PIE_PIEIER4_INTX14   0x2000U   // Enable for Interrupt 4.14
#define PIE_PIEIER4_INTX15   0x4000U   // Enable for Interrupt 4.15
#define PIE_PIEIER4_INTX16   0x8000U   // Enable for Interrupt 4.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR4 register
//
//*************************************************************************************************
#define PIE_PIEIFR4_INTX1    0x1U      // Flag for Interrupt 4.1
#define PIE_PIEIFR4_INTX2    0x2U      // Flag for Interrupt 4.2
#define PIE_PIEIFR4_INTX3    0x4U      // Flag for Interrupt 4.3
#define PIE_PIEIFR4_INTX4    0x8U      // Flag for Interrupt 4.4
#define PIE_PIEIFR4_INTX5    0x10U     // Flag for Interrupt 4.5
#define PIE_PIEIFR4_INTX6    0x20U     // Flag for Interrupt 4.6
#define PIE_PIEIFR4_INTX7    0x40U     // Flag for Interrupt 4.7
#define PIE_PIEIFR4_INTX8    0x80U     // Flag for Interrupt 4.8
#define PIE_PIEIFR4_INTX9    0x100U    // Flag for Interrupt 4.9
#define PIE_PIEIFR4_INTX10   0x200U    // Flag for Interrupt 4.10
#define PIE_PIEIFR4_INTX11   0x400U    // Flag for Interrupt 4.11
#define PIE_PIEIFR4_INTX12   0x800U    // Flag for Interrupt 4.12
#define PIE_PIEIFR4_INTX13   0x1000U   // Flag for Interrupt 4.13
#define PIE_PIEIFR4_INTX14   0x2000U   // Flag for Interrupt 4.14
#define PIE_PIEIFR4_INTX15   0x4000U   // Flag for Interrupt 4.15
#define PIE_PIEIFR4_INTX16   0x8000U   // Flag for Interrupt 4.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER5 register
//
//*************************************************************************************************
#define PIE_PIEIER5_INTX1    0x1U      // Enable for Interrupt 5.1
#define PIE_PIEIER5_INTX2    0x2U      // Enable for Interrupt 5.2
#define PIE_PIEIER5_INTX3    0x4U      // Enable for Interrupt 5.3
#define PIE_PIEIER5_INTX4    0x8U      // Enable for Interrupt 5.4
#define PIE_PIEIER5_INTX5    0x10U     // Enable for Interrupt 5.5
#define PIE_PIEIER5_INTX6    0x20U     // Enable for Interrupt 5.6
#define PIE_PIEIER5_INTX7    0x40U     // Enable for Interrupt 5.7
#define PIE_PIEIER5_INTX8    0x80U     // Enable for Interrupt 5.8
#define PIE_PIEIER5_INTX9    0x100U    // Enable for Interrupt 5.9
#define PIE_PIEIER5_INTX10   0x200U    // Enable for Interrupt 5.10
#define PIE_PIEIER5_INTX11   0x400U    // Enable for Interrupt 5.11
#define PIE_PIEIER5_INTX12   0x800U    // Enable for Interrupt 5.12
#define PIE_PIEIER5_INTX13   0x1000U   // Enable for Interrupt 5.13
#define PIE_PIEIER5_INTX14   0x2000U   // Enable for Interrupt 5.14
#define PIE_PIEIER5_INTX15   0x4000U   // Enable for Interrupt 5.15
#define PIE_PIEIER5_INTX16   0x8000U   // Enable for Interrupt 5.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR5 register
//
//*************************************************************************************************
#define PIE_PIEIFR5_INTX1    0x1U      // Flag for Interrupt 5.1
#define PIE_PIEIFR5_INTX2    0x2U      // Flag for Interrupt 5.2
#define PIE_PIEIFR5_INTX3    0x4U      // Flag for Interrupt 5.3
#define PIE_PIEIFR5_INTX4    0x8U      // Flag for Interrupt 5.4
#define PIE_PIEIFR5_INTX5    0x10U     // Flag for Interrupt 5.5
#define PIE_PIEIFR5_INTX6    0x20U     // Flag for Interrupt 5.6
#define PIE_PIEIFR5_INTX7    0x40U     // Flag for Interrupt 5.7
#define PIE_PIEIFR5_INTX8    0x80U     // Flag for Interrupt 5.8
#define PIE_PIEIFR5_INTX9    0x100U    // Flag for Interrupt 5.9
#define PIE_PIEIFR5_INTX10   0x200U    // Flag for Interrupt 5.10
#define PIE_PIEIFR5_INTX11   0x400U    // Flag for Interrupt 5.11
#define PIE_PIEIFR5_INTX12   0x800U    // Flag for Interrupt 5.12
#define PIE_PIEIFR5_INTX13   0x1000U   // Flag for Interrupt 5.13
#define PIE_PIEIFR5_INTX14   0x2000U   // Flag for Interrupt 5.14
#define PIE_PIEIFR5_INTX15   0x4000U   // Flag for Interrupt 5.15
#define PIE_PIEIFR5_INTX16   0x8000U   // Flag for Interrupt 5.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER6 register
//
//*************************************************************************************************
#define PIE_PIEIER6_INTX1    0x1U      // Enable for Interrupt 6.1
#define PIE_PIEIER6_INTX2    0x2U      // Enable for Interrupt 6.2
#define PIE_PIEIER6_INTX3    0x4U      // Enable for Interrupt 6.3
#define PIE_PIEIER6_INTX4    0x8U      // Enable for Interrupt 6.4
#define PIE_PIEIER6_INTX5    0x10U     // Enable for Interrupt 6.5
#define PIE_PIEIER6_INTX6    0x20U     // Enable for Interrupt 6.6
#define PIE_PIEIER6_INTX7    0x40U     // Enable for Interrupt 6.7
#define PIE_PIEIER6_INTX8    0x80U     // Enable for Interrupt 6.8
#define PIE_PIEIER6_INTX9    0x100U    // Enable for Interrupt 6.9
#define PIE_PIEIER6_INTX10   0x200U    // Enable for Interrupt 6.10
#define PIE_PIEIER6_INTX11   0x400U    // Enable for Interrupt 6.11
#define PIE_PIEIER6_INTX12   0x800U    // Enable for Interrupt 6.12
#define PIE_PIEIER6_INTX13   0x1000U   // Enable for Interrupt 6.13
#define PIE_PIEIER6_INTX14   0x2000U   // Enable for Interrupt 6.14
#define PIE_PIEIER6_INTX15   0x4000U   // Enable for Interrupt 6.15
#define PIE_PIEIER6_INTX16   0x8000U   // Enable for Interrupt 6.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR6 register
//
//*************************************************************************************************
#define PIE_PIEIFR6_INTX1    0x1U      // Flag for Interrupt 6.1
#define PIE_PIEIFR6_INTX2    0x2U      // Flag for Interrupt 6.2
#define PIE_PIEIFR6_INTX3    0x4U      // Flag for Interrupt 6.3
#define PIE_PIEIFR6_INTX4    0x8U      // Flag for Interrupt 6.4
#define PIE_PIEIFR6_INTX5    0x10U     // Flag for Interrupt 6.5
#define PIE_PIEIFR6_INTX6    0x20U     // Flag for Interrupt 6.6
#define PIE_PIEIFR6_INTX7    0x40U     // Flag for Interrupt 6.7
#define PIE_PIEIFR6_INTX8    0x80U     // Flag for Interrupt 6.8
#define PIE_PIEIFR6_INTX9    0x100U    // Flag for Interrupt 6.9
#define PIE_PIEIFR6_INTX10   0x200U    // Flag for Interrupt 6.10
#define PIE_PIEIFR6_INTX11   0x400U    // Flag for Interrupt 6.11
#define PIE_PIEIFR6_INTX12   0x800U    // Flag for Interrupt 6.12
#define PIE_PIEIFR6_INTX13   0x1000U   // Flag for Interrupt 6.13
#define PIE_PIEIFR6_INTX14   0x2000U   // Flag for Interrupt 6.14
#define PIE_PIEIFR6_INTX15   0x4000U   // Flag for Interrupt 6.15
#define PIE_PIEIFR6_INTX16   0x8000U   // Flag for Interrupt 6.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER7 register
//
//*************************************************************************************************
#define PIE_PIEIER7_INTX1    0x1U      // Enable for Interrupt 7.1
#define PIE_PIEIER7_INTX2    0x2U      // Enable for Interrupt 7.2
#define PIE_PIEIER7_INTX3    0x4U      // Enable for Interrupt 7.3
#define PIE_PIEIER7_INTX4    0x8U      // Enable for Interrupt 7.4
#define PIE_PIEIER7_INTX5    0x10U     // Enable for Interrupt 7.5
#define PIE_PIEIER7_INTX6    0x20U     // Enable for Interrupt 7.6
#define PIE_PIEIER7_INTX7    0x40U     // Enable for Interrupt 7.7
#define PIE_PIEIER7_INTX8    0x80U     // Enable for Interrupt 7.8
#define PIE_PIEIER7_INTX9    0x100U    // Enable for Interrupt 7.9
#define PIE_PIEIER7_INTX10   0x200U    // Enable for Interrupt 7.10
#define PIE_PIEIER7_INTX11   0x400U    // Enable for Interrupt 7.11
#define PIE_PIEIER7_INTX12   0x800U    // Enable for Interrupt 7.12
#define PIE_PIEIER7_INTX13   0x1000U   // Enable for Interrupt 7.13
#define PIE_PIEIER7_INTX14   0x2000U   // Enable for Interrupt 7.14
#define PIE_PIEIER7_INTX15   0x4000U   // Enable for Interrupt 7.15
#define PIE_PIEIER7_INTX16   0x8000U   // Enable for Interrupt 7.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR7 register
//
//*************************************************************************************************
#define PIE_PIEIFR7_INTX1    0x1U      // Flag for Interrupt 7.1
#define PIE_PIEIFR7_INTX2    0x2U      // Flag for Interrupt 7.2
#define PIE_PIEIFR7_INTX3    0x4U      // Flag for Interrupt 7.3
#define PIE_PIEIFR7_INTX4    0x8U      // Flag for Interrupt 7.4
#define PIE_PIEIFR7_INTX5    0x10U     // Flag for Interrupt 7.5
#define PIE_PIEIFR7_INTX6    0x20U     // Flag for Interrupt 7.6
#define PIE_PIEIFR7_INTX7    0x40U     // Flag for Interrupt 7.7
#define PIE_PIEIFR7_INTX8    0x80U     // Flag for Interrupt 7.8
#define PIE_PIEIFR7_INTX9    0x100U    // Flag for Interrupt 7.9
#define PIE_PIEIFR7_INTX10   0x200U    // Flag for Interrupt 7.10
#define PIE_PIEIFR7_INTX11   0x400U    // Flag for Interrupt 7.11
#define PIE_PIEIFR7_INTX12   0x800U    // Flag for Interrupt 7.12
#define PIE_PIEIFR7_INTX13   0x1000U   // Flag for Interrupt 7.13
#define PIE_PIEIFR7_INTX14   0x2000U   // Flag for Interrupt 7.14
#define PIE_PIEIFR7_INTX15   0x4000U   // Flag for Interrupt 7.15
#define PIE_PIEIFR7_INTX16   0x8000U   // Flag for Interrupt 7.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER8 register
//
//*************************************************************************************************
#define PIE_PIEIER8_INTX1    0x1U      // Enable for Interrupt 8.1
#define PIE_PIEIER8_INTX2    0x2U      // Enable for Interrupt 8.2
#define PIE_PIEIER8_INTX3    0x4U      // Enable for Interrupt 8.3
#define PIE_PIEIER8_INTX4    0x8U      // Enable for Interrupt 8.4
#define PIE_PIEIER8_INTX5    0x10U     // Enable for Interrupt 8.5
#define PIE_PIEIER8_INTX6    0x20U     // Enable for Interrupt 8.6
#define PIE_PIEIER8_INTX7    0x40U     // Enable for Interrupt 8.7
#define PIE_PIEIER8_INTX8    0x80U     // Enable for Interrupt 8.8
#define PIE_PIEIER8_INTX9    0x100U    // Enable for Interrupt 8.9
#define PIE_PIEIER8_INTX10   0x200U    // Enable for Interrupt 8.10
#define PIE_PIEIER8_INTX11   0x400U    // Enable for Interrupt 8.11
#define PIE_PIEIER8_INTX12   0x800U    // Enable for Interrupt 8.12
#define PIE_PIEIER8_INTX13   0x1000U   // Enable for Interrupt 8.13
#define PIE_PIEIER8_INTX14   0x2000U   // Enable for Interrupt 8.14
#define PIE_PIEIER8_INTX15   0x4000U   // Enable for Interrupt 8.15
#define PIE_PIEIER8_INTX16   0x8000U   // Enable for Interrupt 8.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR8 register
//
//*************************************************************************************************
#define PIE_PIEIFR8_INTX1    0x1U      // Flag for Interrupt 8.1
#define PIE_PIEIFR8_INTX2    0x2U      // Flag for Interrupt 8.2
#define PIE_PIEIFR8_INTX3    0x4U      // Flag for Interrupt 8.3
#define PIE_PIEIFR8_INTX4    0x8U      // Flag for Interrupt 8.4
#define PIE_PIEIFR8_INTX5    0x10U     // Flag for Interrupt 8.5
#define PIE_PIEIFR8_INTX6    0x20U     // Flag for Interrupt 8.6
#define PIE_PIEIFR8_INTX7    0x40U     // Flag for Interrupt 8.7
#define PIE_PIEIFR8_INTX8    0x80U     // Flag for Interrupt 8.8
#define PIE_PIEIFR8_INTX9    0x100U    // Flag for Interrupt 8.9
#define PIE_PIEIFR8_INTX10   0x200U    // Flag for Interrupt 8.10
#define PIE_PIEIFR8_INTX11   0x400U    // Flag for Interrupt 8.11
#define PIE_PIEIFR8_INTX12   0x800U    // Flag for Interrupt 8.12
#define PIE_PIEIFR8_INTX13   0x1000U   // Flag for Interrupt 8.13
#define PIE_PIEIFR8_INTX14   0x2000U   // Flag for Interrupt 8.14
#define PIE_PIEIFR8_INTX15   0x4000U   // Flag for Interrupt 8.15
#define PIE_PIEIFR8_INTX16   0x8000U   // Flag for Interrupt 8.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER9 register
//
//*************************************************************************************************
#define PIE_PIEIER9_INTX1    0x1U      // Enable for Interrupt 9.1
#define PIE_PIEIER9_INTX2    0x2U      // Enable for Interrupt 9.2
#define PIE_PIEIER9_INTX3    0x4U      // Enable for Interrupt 9.3
#define PIE_PIEIER9_INTX4    0x8U      // Enable for Interrupt 9.4
#define PIE_PIEIER9_INTX5    0x10U     // Enable for Interrupt 9.5
#define PIE_PIEIER9_INTX6    0x20U     // Enable for Interrupt 9.6
#define PIE_PIEIER9_INTX7    0x40U     // Enable for Interrupt 9.7
#define PIE_PIEIER9_INTX8    0x80U     // Enable for Interrupt 9.8
#define PIE_PIEIER9_INTX9    0x100U    // Enable for Interrupt 9.9
#define PIE_PIEIER9_INTX10   0x200U    // Enable for Interrupt 9.10
#define PIE_PIEIER9_INTX11   0x400U    // Enable for Interrupt 9.11
#define PIE_PIEIER9_INTX12   0x800U    // Enable for Interrupt 9.12
#define PIE_PIEIER9_INTX13   0x1000U   // Enable for Interrupt 9.13
#define PIE_PIEIER9_INTX14   0x2000U   // Enable for Interrupt 9.14
#define PIE_PIEIER9_INTX15   0x4000U   // Enable for Interrupt 9.15
#define PIE_PIEIER9_INTX16   0x8000U   // Enable for Interrupt 9.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR9 register
//
//*************************************************************************************************
#define PIE_PIEIFR9_INTX1    0x1U      // Flag for Interrupt 9.1
#define PIE_PIEIFR9_INTX2    0x2U      // Flag for Interrupt 9.2
#define PIE_PIEIFR9_INTX3    0x4U      // Flag for Interrupt 9.3
#define PIE_PIEIFR9_INTX4    0x8U      // Flag for Interrupt 9.4
#define PIE_PIEIFR9_INTX5    0x10U     // Flag for Interrupt 9.5
#define PIE_PIEIFR9_INTX6    0x20U     // Flag for Interrupt 9.6
#define PIE_PIEIFR9_INTX7    0x40U     // Flag for Interrupt 9.7
#define PIE_PIEIFR9_INTX8    0x80U     // Flag for Interrupt 9.8
#define PIE_PIEIFR9_INTX9    0x100U    // Flag for Interrupt 9.9
#define PIE_PIEIFR9_INTX10   0x200U    // Flag for Interrupt 9.10
#define PIE_PIEIFR9_INTX11   0x400U    // Flag for Interrupt 9.11
#define PIE_PIEIFR9_INTX12   0x800U    // Flag for Interrupt 9.12
#define PIE_PIEIFR9_INTX13   0x1000U   // Flag for Interrupt 9.13
#define PIE_PIEIFR9_INTX14   0x2000U   // Flag for Interrupt 9.14
#define PIE_PIEIFR9_INTX15   0x4000U   // Flag for Interrupt 9.15
#define PIE_PIEIFR9_INTX16   0x8000U   // Flag for Interrupt 9.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER10 register
//
//*************************************************************************************************
#define PIE_PIEIER10_INTX1    0x1U      // Enable for Interrupt 10.1
#define PIE_PIEIER10_INTX2    0x2U      // Enable for Interrupt 10.2
#define PIE_PIEIER10_INTX3    0x4U      // Enable for Interrupt 10.3
#define PIE_PIEIER10_INTX4    0x8U      // Enable for Interrupt 10.4
#define PIE_PIEIER10_INTX5    0x10U     // Enable for Interrupt 10.5
#define PIE_PIEIER10_INTX6    0x20U     // Enable for Interrupt 10.6
#define PIE_PIEIER10_INTX7    0x40U     // Enable for Interrupt 10.7
#define PIE_PIEIER10_INTX8    0x80U     // Enable for Interrupt 10.8
#define PIE_PIEIER10_INTX9    0x100U    // Enable for Interrupt 10.9
#define PIE_PIEIER10_INTX10   0x200U    // Enable for Interrupt 10.10
#define PIE_PIEIER10_INTX11   0x400U    // Enable for Interrupt 10.11
#define PIE_PIEIER10_INTX12   0x800U    // Enable for Interrupt 10.12
#define PIE_PIEIER10_INTX13   0x1000U   // Enable for Interrupt 10.13
#define PIE_PIEIER10_INTX14   0x2000U   // Enable for Interrupt 10.14
#define PIE_PIEIER10_INTX15   0x4000U   // Enable for Interrupt 10.15
#define PIE_PIEIER10_INTX16   0x8000U   // Enable for Interrupt 10.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR10 register
//
//*************************************************************************************************
#define PIE_PIEIFR10_INTX1    0x1U      // Flag for Interrupt 10.1
#define PIE_PIEIFR10_INTX2    0x2U      // Flag for Interrupt 10.2
#define PIE_PIEIFR10_INTX3    0x4U      // Flag for Interrupt 10.3
#define PIE_PIEIFR10_INTX4    0x8U      // Flag for Interrupt 10.4
#define PIE_PIEIFR10_INTX5    0x10U     // Flag for Interrupt 10.5
#define PIE_PIEIFR10_INTX6    0x20U     // Flag for Interrupt 10.6
#define PIE_PIEIFR10_INTX7    0x40U     // Flag for Interrupt 10.7
#define PIE_PIEIFR10_INTX8    0x80U     // Flag for Interrupt 10.8
#define PIE_PIEIFR10_INTX9    0x100U    // Flag for Interrupt 10.9
#define PIE_PIEIFR10_INTX10   0x200U    // Flag for Interrupt 10.10
#define PIE_PIEIFR10_INTX11   0x400U    // Flag for Interrupt 10.11
#define PIE_PIEIFR10_INTX12   0x800U    // Flag for Interrupt 10.12
#define PIE_PIEIFR10_INTX13   0x1000U   // Flag for Interrupt 10.13
#define PIE_PIEIFR10_INTX14   0x2000U   // Flag for Interrupt 10.14
#define PIE_PIEIFR10_INTX15   0x4000U   // Flag for Interrupt 10.15
#define PIE_PIEIFR10_INTX16   0x8000U   // Flag for Interrupt 10.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER11 register
//
//*************************************************************************************************
#define PIE_PIEIER11_INTX1    0x1U      // Enable for Interrupt 11.1
#define PIE_PIEIER11_INTX2    0x2U      // Enable for Interrupt 11.2
#define PIE_PIEIER11_INTX3    0x4U      // Enable for Interrupt 11.3
#define PIE_PIEIER11_INTX4    0x8U      // Enable for Interrupt 11.4
#define PIE_PIEIER11_INTX5    0x10U     // Enable for Interrupt 11.5
#define PIE_PIEIER11_INTX6    0x20U     // Enable for Interrupt 11.6
#define PIE_PIEIER11_INTX7    0x40U     // Enable for Interrupt 11.7
#define PIE_PIEIER11_INTX8    0x80U     // Enable for Interrupt 11.8
#define PIE_PIEIER11_INTX9    0x100U    // Enable for Interrupt 11.9
#define PIE_PIEIER11_INTX10   0x200U    // Enable for Interrupt 11.10
#define PIE_PIEIER11_INTX11   0x400U    // Enable for Interrupt 11.11
#define PIE_PIEIER11_INTX12   0x800U    // Enable for Interrupt 11.12
#define PIE_PIEIER11_INTX13   0x1000U   // Enable for Interrupt 11.13
#define PIE_PIEIER11_INTX14   0x2000U   // Enable for Interrupt 11.14
#define PIE_PIEIER11_INTX15   0x4000U   // Enable for Interrupt 11.15
#define PIE_PIEIER11_INTX16   0x8000U   // Enable for Interrupt 11.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR11 register
//
//*************************************************************************************************
#define PIE_PIEIFR11_INTX1    0x1U      // Flag for Interrupt 11.1
#define PIE_PIEIFR11_INTX2    0x2U      // Flag for Interrupt 11.2
#define PIE_PIEIFR11_INTX3    0x4U      // Flag for Interrupt 11.3
#define PIE_PIEIFR11_INTX4    0x8U      // Flag for Interrupt 11.4
#define PIE_PIEIFR11_INTX5    0x10U     // Flag for Interrupt 11.5
#define PIE_PIEIFR11_INTX6    0x20U     // Flag for Interrupt 11.6
#define PIE_PIEIFR11_INTX7    0x40U     // Flag for Interrupt 11.7
#define PIE_PIEIFR11_INTX8    0x80U     // Flag for Interrupt 11.8
#define PIE_PIEIFR11_INTX9    0x100U    // Flag for Interrupt 11.9
#define PIE_PIEIFR11_INTX10   0x200U    // Flag for Interrupt 11.10
#define PIE_PIEIFR11_INTX11   0x400U    // Flag for Interrupt 11.11
#define PIE_PIEIFR11_INTX12   0x800U    // Flag for Interrupt 11.12
#define PIE_PIEIFR11_INTX13   0x1000U   // Flag for Interrupt 11.13
#define PIE_PIEIFR11_INTX14   0x2000U   // Flag for Interrupt 11.14
#define PIE_PIEIFR11_INTX15   0x4000U   // Flag for Interrupt 11.15
#define PIE_PIEIFR11_INTX16   0x8000U   // Flag for Interrupt 11.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIER12 register
//
//*************************************************************************************************
#define PIE_PIEIER12_INTX1    0x1U      // Enable for Interrupt 12.1
#define PIE_PIEIER12_INTX2    0x2U      // Enable for Interrupt 12.2
#define PIE_PIEIER12_INTX3    0x4U      // Enable for Interrupt 12.3
#define PIE_PIEIER12_INTX4    0x8U      // Enable for Interrupt 12.4
#define PIE_PIEIER12_INTX5    0x10U     // Enable for Interrupt 12.5
#define PIE_PIEIER12_INTX6    0x20U     // Enable for Interrupt 12.6
#define PIE_PIEIER12_INTX7    0x40U     // Enable for Interrupt 12.7
#define PIE_PIEIER12_INTX8    0x80U     // Enable for Interrupt 12.8
#define PIE_PIEIER12_INTX9    0x100U    // Enable for Interrupt 12.9
#define PIE_PIEIER12_INTX10   0x200U    // Enable for Interrupt 12.10
#define PIE_PIEIER12_INTX11   0x400U    // Enable for Interrupt 12.11
#define PIE_PIEIER12_INTX12   0x800U    // Enable for Interrupt 12.12
#define PIE_PIEIER12_INTX13   0x1000U   // Enable for Interrupt 12.13
#define PIE_PIEIER12_INTX14   0x2000U   // Enable for Interrupt 12.14
#define PIE_PIEIER12_INTX15   0x4000U   // Enable for Interrupt 12.15
#define PIE_PIEIER12_INTX16   0x8000U   // Enable for Interrupt 12.16

//*************************************************************************************************
//
// The following are defines for the bit fields in the PIEIFR12 register
//
//*************************************************************************************************
#define PIE_PIEIFR12_INTX1    0x1U      // Flag for Interrupt 12.1
#define PIE_PIEIFR12_INTX2    0x2U      // Flag for Interrupt 12.2
#define PIE_PIEIFR12_INTX3    0x4U      // Flag for Interrupt 12.3
#define PIE_PIEIFR12_INTX4    0x8U      // Flag for Interrupt 12.4
#define PIE_PIEIFR12_INTX5    0x10U     // Flag for Interrupt 12.5
#define PIE_PIEIFR12_INTX6    0x20U     // Flag for Interrupt 12.6
#define PIE_PIEIFR12_INTX7    0x40U     // Flag for Interrupt 12.7
#define PIE_PIEIFR12_INTX8    0x80U     // Flag for Interrupt 12.8
#define PIE_PIEIFR12_INTX9    0x100U    // Flag for Interrupt 12.9
#define PIE_PIEIFR12_INTX10   0x200U    // Flag for Interrupt 12.10
#define PIE_PIEIFR12_INTX11   0x400U    // Flag for Interrupt 12.11
#define PIE_PIEIFR12_INTX12   0x800U    // Flag for Interrupt 12.12
#define PIE_PIEIFR12_INTX13   0x1000U   // Flag for Interrupt 12.13
#define PIE_PIEIFR12_INTX14   0x2000U   // Flag for Interrupt 12.14
#define PIE_PIEIFR12_INTX15   0x4000U   // Flag for Interrupt 12.15
#define PIE_PIEIFR12_INTX16   0x8000U   // Flag for Interrupt 12.16



#endif
