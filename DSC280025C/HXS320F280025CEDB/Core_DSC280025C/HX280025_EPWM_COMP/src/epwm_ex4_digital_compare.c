/******************************************************************
 文 档 名：       HX_DSC280025_EPWM_COMP
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      EPWM COMP超阈值数字比较错误联防事件触发
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，
 （1）通过EPWM1A/B输出10kHz，脉宽50%互补的PWM波形
 （2）通过A2通道在CTR=周期值时刻采样电流;
 （3）当采样电流对应电压值超过1.65V-2048时，通过COMP1与交叉开关在GPIO14/XBAR3输出低电平信号；
 （4）采用EPWM_DC数字比较错误错误联防触发
        通过错误联防引脚GPIO25-TZ1，通过交叉开关XBAR输入低电平信号，
 使EPWM1A/B置低，从而实现EPWM封波

 外部连接：EPWM1A/B连接示波器 GPIO25初始时不接或接3.3V
 输出PWM正常后，GPIO25-GPIO14，A2-RG(RG端电压不低于1.45V)
 察看EPWM1A/B置低封波，A2输入端电压低于1.45V时EPWM1A/B恢复正常输出

 版 本：      V1.0.0
 时 间：      2023年2月9日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "system.h"


void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定*/
    Device_initGPIO();

	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

	/*中断入口地址INT_EPWM1_TZ,指向执行epwm1TZISR中断服务程序*/
    Interrupt_register(INT_EPWM1_TZ, &epwm1TZISR);
	/*配置INT_ADCA1中断入口地址，指向执行adcA1ISR中断服务程序*/
	Interrupt_register(INT_ADCA1, &adcA1ISR);


	/*屏蔽TBCLK时基同步，便于PWM初始化配置写入*/
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();

    /*EPWM的GPIO引脚配置*/
    epwm_gpio();
    /*EPWM的错误联防TZ引脚配置*/
    epwm_tz_gpio();

	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();

	/*配置GPIO14为交叉开关输出XBAR3*/
	GPIO_setPinConfig(GPIO_14_OUTPUTXBAR3);

    /*同步策略*/
    sync_init();
    /*EPWM配置*/
    epwm_config();

	/*配置COMP比较器H*/
    comp_init();
	/*配置COMP比较器H时钟分频=SYSCLK/16*/
    CMPSS_setClockPrescaler(CMPSS1_BASE,CMPSS_CLOCK_DIVIDER_16);

	/*配置比较器COMP的GPIO*/
    comp_gpio();
    EDIS;

	/*使能TBCLK时基同步，以使PWM配置写入，并实现多PWM同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

	/*中断使能INT_EPWM1_TZ*/
    Interrupt_enable(INT_EPWM1_TZ);
	/*配置INT_ADCA1中断使能*/
	Interrupt_enable(INT_ADCA1);

	/*打开全局中断*/
    EINT;
    ERTM;

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}




