#include "system.h"

void comp_gpio(void)
{
	/*配置GPIO14/OUTPUT-XBAR3输出使能*/
	XBAR_setOutputLatchMode(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);
	/*配置GPIO14/OUTPUT-XBAR3输出翻转*/
	XBAR_invertOutputSignal(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);

	/*配置GPIO14/OUTPUT-XBAR3输出：XBAR_OUT_MUX04_CMPSS3_CTRIPOUTH-比较器输出*/
	XBAR_setOutputMuxConfig(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_OUT_MUX00_CMPSS1_CTRIPOUTH);
	/*配置GPIO14/OUTPUT-XBAR3输出选择：XBAR_MUX00-通过MUX00输出*/
	XBAR_enableOutputMux(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_MUX00);
}

void comp_init(void)
{
		/*配置比较器高复用使能：COMP1_HP0-Pin13-对应A2引脚*/
		ASysCtl_selectCMPHNMux(ASYSCTL_CMPHNMUX_SELECT_1);
		/*配置比较器高复用选择：COMP1_HP0-Pin13-对应A2引脚*/
	    ASysCtl_selectCMPHPMux(ASYSCTL_CMPHPMUX_SELECT_1,0U);
		/*配置比较器高：CMPSS_INSRC_DAC-通过内部DAC比较*/
	    CMPSS_configHighComparator(CMPSS1_BASE,CMPSS_INSRC_DAC);

		/*配置比较器高：CMPSS_DACVAL_SYSCLK-采用SYSCLK更新
		 * CMPSS_DACREF_VDDA-VDDA为电压参考
		 * CMPSS_DACSRC_SHDW-通过影子寄存器更新*/
	    CMPSS_configDAC(CMPSS1_BASE,
	    		(CMPSS_DACVAL_SYSCLK | CMPSS_DACREF_VDDA| CMPSS_DACSRC_SHDW));

		/*配置比较器高：内部DAC值为1800-1.45V*/
	    CMPSS_setDACValueHigh(CMPSS1_BASE,1800U);

		/*配置比较器高滤波：采样分频=1023，采样窗口=32，极值=31*/
	    CMPSS_configFilterHigh(CMPSS1_BASE, 1023U, 32U, 31U);
		/*配置比较器高滤波*/
	    CMPSS_initFilterHigh(CMPSS1_BASE);

		/*配置比较器输出：通过CMPSS_TRIPOUT_ASYNC_COMP-COMP异步TRIPOUT-对应XBAR输出*/
	    CMPSS_configOutputsHigh(CMPSS1_BASE,(CMPSS_TRIPOUT_FILTER | CMPSS_TRIP_FILTER));

		/*配置比较器滞后：0*/
	    CMPSS_setHysteresis(CMPSS1_BASE,0U);

		/*配置比较器斜坡：最大值=0，递减值=0，延迟值=0
		 * 同步值=1，使用斜坡*/
	    CMPSS_configRamp(CMPSS1_BASE,0U,0U,0U,1U,true);

		/*屏蔽比较器高通过PWMSYNC复位*/
	    CMPSS_disableLatchResetOnPWMSYNCHigh(CMPSS1_BASE);
		/*比较器空窗=1*/
	    CMPSS_configBlanking(CMPSS1_BASE,1U);
		/*屏蔽比较器空窗*/
	    CMPSS_disableBlanking(CMPSS1_BASE);
		/*屏蔽比较器高通过PWMSYNC复位*/
	    CMPSS_configLatchOnPWMSYNC(CMPSS1_BASE,false,false);
		/*使能比较器模块*/
	    CMPSS_enableModule(CMPSS1_BASE);
		/*延时500us，完成COMP比较器上电*/
	    DEVICE_DELAY_US(500);
}
