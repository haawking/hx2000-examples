/******************************************************************
 文 档 名：       HX_DSC280025_CPUTIMER
 开 发 环 境：  Haawking IDE V2.1.0
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      定时器中断触发闪灯
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，定时器中断触发

 现象:1s触发定时器0中断， 2s触发定时器1中断，4s触发定时器中断
 计数变量
//! - cpuTimer0IntCount 定时器0触发产生计数，GPIO31/LED1翻转闪灯一次
//! - cpuTimer1IntCount 定时器1触发产生计数，GPIO34/LED2翻转闪灯一次
//! - cpuTimer2IntCount 定时器2触发产生计数，GPIO31/LED1 GPIO34/LED2翻转闪灯一次

 版 本：      V1.0.1
 时 间：      2023年11月17日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Globals
//
uint16_t cpuTimer0IntCount;
uint16_t cpuTimer1IntCount;
uint16_t cpuTimer2IntCount;

//
// Function Prototypes
//
__interrupt void cpuTimer0ISR(void);
__interrupt void cpuTimer1ISR(void);
__interrupt void cpuTimer2ISR(void);
void initCPUTimers(void);
void configCPUTimer(uint32_t, float, float);
void GPIO_config(void);

uint16_t bss;

//
// Main
//
int main(void)
{
    /*系统时钟初始化*/
    Device_init();
    /*GPIO配置，用于显示中断状态*/
    GPIO_config();

    /*初始化PIE与清PIE寄存器，关CPU中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口INT_TIMER0指向相应中断服务程序，执行cpuTimer0ISR*/
    Interrupt_register(INT_TIMER0, &cpuTimer0ISR);
    /*中断入口INT_TIMER1指向相应中断服务程序，执行cpuTimer1ISR*/
    Interrupt_register(INT_TIMER1, &cpuTimer1ISR);
    /*中断入口INT_TIMER2指向相应中断服务程序，执行cpuTimer2ISR*/
    Interrupt_register(INT_TIMER2, &cpuTimer2ISR);

    /*初始化定时器时钟cpuTimer*/
    initCPUTimers();

    /*配置定时器时钟cpuTimer：基地址CPUTIMERx_BASE,频率MHz,周期值us*/
    configCPUTimer(CPUTIMER0_BASE, DEVICE_SYSCLK_FREQ, 1000000);
    configCPUTimer(CPUTIMER1_BASE, DEVICE_SYSCLK_FREQ, 2000000);
    configCPUTimer(CPUTIMER2_BASE, DEVICE_SYSCLK_FREQ, 4000000);

    /*定时器时钟cpuTimer使能：基地址CPUTIMERx_BASE*/
    CPUTimer_enableInterrupt(CPUTIMER0_BASE);
    CPUTimer_enableInterrupt(CPUTIMER1_BASE);
    CPUTimer_enableInterrupt(CPUTIMER2_BASE);

    /*cpuTimer相应的定时器时钟PIE与CPU中断使能：中断INT_TIMERx*/
    Interrupt_enable(INT_TIMER0);
    Interrupt_enable(INT_TIMER1);
    Interrupt_enable(INT_TIMER2);

    /*cpuTimer定时器启动：基地址CPUTIMERx_BASE*/
    CPUTimer_startTimer(CPUTIMER0_BASE);
    CPUTimer_startTimer(CPUTIMER1_BASE);
    CPUTimer_startTimer(CPUTIMER2_BASE);

    /*打开全局中断*/
    EINT;

    while(1)
    {
    }

    return 0;
}

//
// initCPUTimers - This function initializes all three CPU timers
// to a known state.
//
void initCPUTimers(void)
{
    /*时钟周期初始化，配置成最大值*/
    CPUTimer_setPeriod(CPUTIMER0_BASE, 0xFFFFFFFF);
    CPUTimer_setPeriod(CPUTIMER1_BASE, 0xFFFFFFFF);
    CPUTimer_setPeriod(CPUTIMER2_BASE, 0xFFFFFFFF);

    /*时钟周期分频配置：基地址CPUTIMERx_BASE,n+1分频*/
    CPUTimer_setPreScaler(CPUTIMER0_BASE, 0);
    CPUTimer_setPreScaler(CPUTIMER1_BASE, 0);
    CPUTimer_setPreScaler(CPUTIMER2_BASE, 0);

    /*CpuTimer定时器暂停，便于配置：基地址CPUTIMERx_BASE*/
    CPUTimer_stopTimer(CPUTIMER0_BASE);
    CPUTimer_stopTimer(CPUTIMER1_BASE);
    CPUTimer_stopTimer(CPUTIMER2_BASE);

    /*CpuTimer定时器暂停，便于配置：基地址CPUTIMERx_BASE*/
    CPUTimer_reloadTimerCounter(CPUTIMER0_BASE);
    CPUTimer_reloadTimerCounter(CPUTIMER1_BASE);
    CPUTimer_reloadTimerCounter(CPUTIMER2_BASE);

    /*重置定时器CpuTimer定时器中断计数*/
    cpuTimer0IntCount = 0;
    cpuTimer1IntCount = 0;
    cpuTimer2IntCount = 0;
}

//
// configCPUTimer - This function initializes the selected timer to the
// period specified by the "freq" and "period" parameters. The "freq" is
// entered as Hz and the period in uSeconds. The timer is held in the stopped
// state after configuration.
//
void configCPUTimer(uint32_t cpuTimer, float freq, float period)
{
    uint32_t temp;

    //
    // Initialize timer period:
    //
    temp = (uint32_t)((freq / 1000000) * period);
    CPUTimer_setPeriod(cpuTimer, temp);

    //
    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    //
    CPUTimer_setPreScaler(cpuTimer, 0);

    //
    // Initializes timer control register. The timer is stopped, reloaded,
    // free run disabled, and interrupt enabled.
    // Additionally, the free and soft bits are set
    //
    CPUTimer_stopTimer(cpuTimer);
    CPUTimer_reloadTimerCounter(cpuTimer);
    CPUTimer_setEmulationMode(cpuTimer,
                              CPUTIMER_EMULATIONMODE_STOPAFTERNEXTDECREMENT);
    CPUTimer_enableInterrupt(cpuTimer);

    //
    // Resets interrupt counters for the three cpuTimers
    //
    if (cpuTimer == CPUTIMER0_BASE)
    {
        cpuTimer0IntCount = 0;
    }
    else if(cpuTimer == CPUTIMER1_BASE)
    {
        cpuTimer1IntCount = 0;
    }
    else if(cpuTimer == CPUTIMER2_BASE)
    {
        cpuTimer2IntCount = 0;
    }
}

//
// cpuTimer0ISR - Counter for CpuTimer0
//
__interrupt void cpuTimer0ISR(void)
{
    cpuTimer0IntCount++;
//    /*GPIO31翻转闪灯，每进一次中断，翻转一次*/
    GPIO_togglePin(31);

    //
    // Acknowledge this interrupt to receive more interrupts from group 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// cpuTimer1ISR - Counter for CpuTimer1
//
__interrupt void cpuTimer1ISR(void)
{
    cpuTimer1IntCount++;
//    /*GPIO34翻转闪灯，每进一次中断，翻转一次*/
    GPIO_togglePin(34);
}

//
// cpuTimer2ISR - Counter for CpuTimer2
//
__interrupt void cpuTimer2ISR(void)
{
    cpuTimer2IntCount++;
//    /*GPIO31与GPIO34翻转闪灯，每进一次中断，翻转一次*/
   GPIO_togglePin(31);
  GPIO_togglePin(34);
}

void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);

    GPIO_setPinConfig(GPIO_34_GPIO34);
   GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);
    EDIS;
}

//
// End of File
//
