/*
 * system.h
 *
 *  Created on: 2023��2��24��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define MSG_DATA_LENGTH    0    // "Don't care" for a Receive mailbox
#define RX_MSG_OBJ_ID      1    // Use mailbox 1

//
// Globals
//
extern uint16_t rxMsgData[8];          // Buffer for received data
extern uint16_t Toggle_ctr;            // GPIO toggle counter
extern volatile uint32_t rxMsgCount;



#endif /* SYSTEM_H_ */
