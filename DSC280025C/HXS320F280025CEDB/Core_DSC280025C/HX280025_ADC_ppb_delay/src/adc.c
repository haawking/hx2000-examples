#include "system.h"

void InitAdcAio(void)
{
	/*配置GPIO231为模拟量输入A0/C15*/
	GPIO_setPinConfig(GPIO_231_GPIO231);
	/*配置GPIO231模拟量输入*/
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);

	/*配置GPIO232为模拟量输入A1*/
	GPIO_setPinConfig(GPIO_232_GPIO232);
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);
	/*配置GPIO230为模拟量输入A10/C10*/
	GPIO_setPinConfig(GPIO_230_GPIO230);
	GPIO_setAnalogMode(230, GPIO_ANALOG_ENABLED);
	/*配置GPIO237为模拟量输入A11/C0*/
	GPIO_setPinConfig(GPIO_237_GPIO237);
	GPIO_setAnalogMode(237, GPIO_ANALOG_ENABLED);
	/*配置GPIO238为模拟量输入A12/C1*/
	GPIO_setPinConfig(GPIO_238_GPIO238);
	GPIO_setAnalogMode(238, GPIO_ANALOG_ENABLED);
	/*配置GPIO239为模拟量输入A14/C4*/
	GPIO_setPinConfig(GPIO_239_GPIO239);
	GPIO_setAnalogMode(239, GPIO_ANALOG_ENABLED);
	/*配置GPIO233为模拟量输入A15/C7*/
	GPIO_setPinConfig(GPIO_233_GPIO233);
	GPIO_setAnalogMode(233, GPIO_ANALOG_ENABLED);
	/*配置GPIO224为模拟量输入A2/C9*/
	GPIO_setPinConfig(GPIO_224_GPIO224);
	GPIO_setAnalogMode(224, GPIO_ANALOG_ENABLED);
	/*配置GPIO242为模拟量输入A3/C5/VDAC*/
	GPIO_setPinConfig(GPIO_242_GPIO242);
	GPIO_setAnalogMode(242, GPIO_ANALOG_ENABLED);
	/*配置GPIO225为模拟量输入A4/C14*/
	GPIO_setPinConfig(GPIO_225_GPIO225);
	GPIO_setAnalogMode(225, GPIO_ANALOG_ENABLED);
	/*配置GPIO244为模拟量输入A5/C2*/
	GPIO_setPinConfig(GPIO_244_GPIO244);
	GPIO_setAnalogMode(244, GPIO_ANALOG_ENABLED);
	/*配置GPIO228为模拟量输入A6*/
	GPIO_setPinConfig(GPIO_228_GPIO228);
	GPIO_setAnalogMode(228, GPIO_ANALOG_ENABLED);
	/*配置GPIO245为模拟量输入A7/C3*/
	GPIO_setPinConfig(GPIO_245_GPIO245);
	GPIO_setAnalogMode(245, GPIO_ANALOG_ENABLED);
	/*配置GPIO241为模拟量输入A8/C11*/
	GPIO_setPinConfig(GPIO_241_GPIO241);
	GPIO_setAnalogMode(241, GPIO_ANALOG_ENABLED);
	/*配置GPIO227为模拟量输入A9/C8*/
	GPIO_setPinConfig(GPIO_227_GPIO227);
	GPIO_setAnalogMode(227, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_226_GPIO226);
	GPIO_setAnalogMode(226, GPIO_ANALOG_ENABLED);
}


void InitAdc(void)
{
	/*屏蔽ADC温度传感器功能*/
	ASysCtl_disableTemperatureSensor();
	/*ADC参考电压配置为:ADC_REFERENCE_INTERNAL-内部参考电压,
	* ADC_REFERENCE_3_3V-3.3V*/
	ADC_setVREF(ADCA_BASE,ADC_REFERENCE_INTERNAL,ADC_REFERENCE_3_3V);
}

void Adc_config(void)
{
	/*配置ADC时钟为ADC_CLK_DIV_8_0-8分频对应SYSCLK/8=20M*/
	ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_8_0);
	/*配置ADC中断脉冲模式：ADC_PULSE_END_OF_CONV-转换完成产生脉冲触发中断*/
	ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
	/*ADC上电使能转换*/
	ADC_enableConverter(ADCA_BASE);
	/*延时1ms完成ADC模块上电*/
	DEVICE_DELAY_US(5000);

	/*屏蔽ADC突发模式*/
	ADC_disableBurstMode(ADCA_BASE);
	/*配置ADC_SOC转换优先级：ADC_PRI_ALL_ROUND_ROBIN-从A0开始循环转换*/
	ADC_setSOCPriority(ADCA_BASE, ADC_PRI_ALL_ROUND_ROBIN);


	/*配置SOC：ADC_SOC_NUMBER0-SOC0,
	 * ADC_TRIGGER_EPWM1_SOCA-通过EPWM1_SOCA触发
	 * ADC_CH_ADCIN0-通过通道A0输入采样，
	 * 采样周期为8*SYSCLK（最大512*SYSCLK）*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	/*配置SOC0触发源：ADC_INT_SOC_TRIGGER_NONE-不通过ADCINT内部触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置SOC：ADC_SOC_NUMBER1-SOC1,
	 * ADC_TRIGGER_EPWM1_SOCA-通过EPWM1_SOCA触发
	 * ADC_CH_ADCIN2-通过通道A2输入采样，
	 * 采样周期为8*SYSCLK（最大512*SYSCLK）*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	/*配置SOC1触发源：ADC_INT_SOC_TRIGGER_NONE-不通过ADCINT内部触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC_SOC_NUMBER0-SOC0中断源为ADC_INT_NUMBER1-ADCINT1*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1,
			ADC_SOC_NUMBER0);
	/*配置ADC_INT_NUMBER1-ADCINT1中断使能*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
	/*清除ADC_INT_NUMBER1-ADCINT1中断状态*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
	/*屏蔽ADC_INT_NUMBER1-ADCINT1连续模式*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);

	/*配置ADC_SOC_NUMBER1-SOC1中断源为ADC_INT_NUMBER2-ADCINT2*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER2,
			ADC_SOC_NUMBER1);
	/*配置ADC_INT_NUMBER1-ADCINT2中断使能*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER2);
	/*清除ADC_INT_NUMBER1-ADCINT2中断状态*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);
	/*屏蔽ADC_INT_NUMBER1-ADCINT2连续模式*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER2);

	/*SOC的PPB后处理模块选择:ADC_PPB_NUMBER1-后处理模块1*/
	ADC_setupPPB(ADCA_BASE, ADC_PPB_NUMBER1, ADC_SOC_NUMBER0);
    /*ADC后处理PPB事件关闭：模块X，
     * 事件名：ADC_EVT_TRIPHI-超过高阈值输出事件
     *  ADC_EVT_TRIPLO-超过低阈值输出事件
     * ADC_EVT_ZERO-计数等于零事件 */
	ADC_disablePPBEvent(ADCA_BASE, ADC_PPB_NUMBER1,
			(ADC_EVT_TRIPHI | ADC_EVT_TRIPLO | ADC_EVT_ZERO));
    /*ADC后处理PPB事件中断使能：模块X，
     * 事件名：ADC_EVT_TRIPHI-超过高阈值输出事件
     *  ADC_EVT_TRIPLO-超过低阈值输出事件*/
	ADC_enablePPBEventInterrupt(ADCA_BASE, ADC_PPB_NUMBER1,
			(ADC_EVT_TRIPHI | ADC_EVT_TRIPLO));
    /*ADC后处理PPB事件中断屏蔽：模块X，
     * 事件名：ADC_EVT_ZERO-计数等于零事件 */
	ADC_disablePPBEventInterrupt(ADCA_BASE, ADC_PPB_NUMBER1,
			(ADC_EVT_ZERO));
	/*配置ADC的PPB后处理校准偏移：ADC_PPB_NUMBER1-模块1，偏移值=0 */
	ADC_setPPBCalibrationOffset(ADCA_BASE, ADC_PPB_NUMBER1, 0);
	/*配置ADC的PPB后处理参考偏移：ADC_PPB_NUMBER1-模块1，偏移值=0*/
	ADC_setPPBReferenceOffset(ADCA_BASE, ADC_PPB_NUMBER1, 0);
	/*关闭ADC的PPB后处理部分*/
	ADC_disablePPBTwosComplement(ADCA_BASE, ADC_PPB_NUMBER1);
	/*SOC的PPB后处理模块极限配置:模块X，高阈值，低阈值*/
	ADC_setPPBTripLimits(ADCA_BASE, ADC_PPB_NUMBER1, 0, 0);
    /*周期CBC触发清除关闭：ADC_PPB_NUMBER1-后处理模块1*/
	ADC_disablePPBEventCBCClear(ADCA_BASE, ADC_PPB_NUMBER1);
}

uint32_t conversion_count;
uint32_t conversion[DELAY_BUFFER_SIZE];
uint16_t delay[DELAY_BUFFER_SIZE];
volatile uint16_t delay_index;

uint32_t myADC0Result[3];

uint32_t adc_A0Result;

__interrupt void adcA1ISR(void)
{
	uint32_t i;

	/*若ADC采样延时大于2*SYSCLK，读取采样时间delay[delay_index]*/
    if(2 < ADC_getPPBDelayTimeStamp(ADCA_BASE, ADC_PPB_NUMBER1))
    {
    	/*读取转换计时*/
        conversion[delay_index] = conversion_count;
    	/*读取采样时间*/
        delay[delay_index] = ADC_getPPBDelayTimeStamp(ADCA_BASE,
        		ADC_PPB_NUMBER1) - 2;
        delay_index++;
    }
    conversion_count++;

    /*读取ADC_SOC初始结果：ADC_SOC_NUMBER0-SOC0*/
    for(i=0;i<3;i++)
    {
    	myADC0Result[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    	if(i>=2)
    	{
    		adc_A0Result=myADC0Result[i];
    	}
    }



    /*清除ADC中断标志：ADC_INT_NUMBER1-ADCINT1*/
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    /*检测ADC中断是否产生溢出*/
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
    	/*清除中断溢出：ADC_INT_NUMBER1-ADCINT1中断*/
    	ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
    	/*清除中断状态：ADC_INT_NUMBER1-ADCINT1中断*/
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    /*清除相应的中断应答PIEACK*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

uint32_t myADC1Result[3];

uint32_t adc_A1Result;

__interrupt void adcA2ISR(void)
{
	uint32_t i;

	/*读取ADC_SOC初始结果：ADC_SOC_NUMBER1-SOC1*/
	for(i=0;i<3;i++)
	{
		myADC1Result[i] = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);
		if(i>=2)
		{
			 adc_A1Result=myADC1Result[i];
		}
	}


	/*清除ADC中断标志：ADCINT2中断*/
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);

    /*检测ADC中断是否产生溢出*/
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER2))
    {
        /*清除中断溢出：ADC_INT_NUMBER1-ADCINT1中断*/
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER2);
        /*清除中断状态：ADC_INT_NUMBER1-ADCINT1中断*/
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER2);
    }

    /*清除相应的中断应答PIEACK*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP10);
}
