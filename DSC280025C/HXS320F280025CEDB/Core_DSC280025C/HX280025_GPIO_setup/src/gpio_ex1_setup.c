/******************************************************************
 文 档 名：       HX_DSC280025_GPIO_setup
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      GPIO按键
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：GPIO9按键输入，控制GPIO31/LED1灯亮灭

 *
 版 本：      V1.0.1
 时 间：      2023年3月23日
 作 者：      liyuyao
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置GPIO9为IO输入*/
    GPIO_setPinConfig(GPIO_9_GPIO9);
    GPIO_setDirectionMode(9, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(9, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(9, GPIO_QUAL_SYNC);
	/*配置GPIO31为IO输出*/
    GPIO_setPinConfig(GPIO_31_GPIO31);
    GPIO_setDirectionMode(31, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(31, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(31, GPIO_QUAL_SYNC);
    EDIS;

    /*使能CPU中断*/
    Interrupt_enableMaster();

    for(;;)
    {
        if(GPIO_readPin(9)==0)
        {
        	/*GPIO31/LED1亮*/
        	GPIO_writePin(31,0);
        }
        else
        {
        	/*GPIO31/LED1灭*/
        	GPIO_writePin(31,1);
        }

    }
}


//
// End of File
//

