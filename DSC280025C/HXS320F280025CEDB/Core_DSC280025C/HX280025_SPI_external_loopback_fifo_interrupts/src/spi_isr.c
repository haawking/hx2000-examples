#include "system.h"

__interrupt void spibTxFIFOISR(void)
{
    uint16_t i;

    /*发送数据sData[i]*/
    for(i = 0; i < 2; i++)
    {
       SPI_writeDataNonBlocking(SPIB_BASE, sData[i]);
    }

    /*更新发送数据*/
    for(i = 0; i < 2; i++)
    {
       sData[i] = sData[i] + 1;
    }

    /*SPI清中断状态：SPI_INT_TXFF-SPI发送FIFO*/
    SPI_clearInterruptStatus(SPIB_BASE, SPI_INT_TXFF);
    /*清中断PIEACK应答*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
}

 __interrupt void spiaRxFIFOISR(void)
{
    uint16_t i;

    /*读取接收数据rData[i]*/
    for(i = 0; i < 2; i++)
    {
        rData[i] = SPI_readDataNonBlocking(SPIA_BASE);
    }

    /*检测接收数据rData[i]是否与发送数据一致*/
    for(i = 0; i < 2; i++)
    {
        /*不一致，则点亮GPIO31/LED1*/
    	if(rData[i] != rDataPoint)
        {
      		GPIO_writePin(31,0);
        }
        else
        {
        	/*一致，则点亮GPIO34/LED2*/
        	GPIO_writePin(34,0);
        }
    }

    rDataPoint++;

    /*SPI清中断状态：SPI_INT_RXFF-SPI接收FIFO*/
    SPI_clearInterruptStatus(SPIA_BASE, SPI_INT_RXFF);
    /*清中断PIEACK应答*/
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
}
