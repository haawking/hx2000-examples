/*
 * system.h
 *
 *  Created on: 2023年2月27日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Globals
//
extern volatile uint16_t sData[2];
extern volatile uint16_t rData[2];
extern volatile uint16_t rDataPoint;

//
// Function Prototypes
//
__interrupt void spibTxFIFOISR(void);
__interrupt void spiaRxFIFOISR(void);

/*SPIA/B的GPIO引脚配置*/
void spia_gpio(void);
void spib_gpio(void);
/*SPIA/B的从机/主机配置*/
void SPIA_slave_init(void);
void SPIB_master_init(void);

void setup1GPIO(void);

#endif /* SYSTEM_H_ */
