/******************************************************************
 文 档 名：       HX_DSC280025_XINT
 开 发 环 境：  Haawking IDE V2.1.0
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      GPIO10与GPIO11产生一循环方波, XINT1/GPIO0上升沿触发外部中断
 	 	 	 	 	 	 XINT2/GPIO2下降沿触发外部中断
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，触发外部中断

 连接方式：GPIO0-GPIO10,GPIO2-GPIO9
 XINT1 同步采样
 XINT2 6个采样510系统时钟周期异步采样

 现象:连线后，到达边沿，触发相应外部中断XINTn，连续插拔几次接线，计数有明显增长
 计数变量
 //!  - xint1Count 代表XINT1中断触发次数,进入一次外部中断
//!  - xint2Count 代表XINT2中断触发次数,进入一次外部中断
//!  - loopCount 代表循环方波次数
 *当XINT1与XINT2外部中断均触发时，LED1/GPIO31翻转闪灯一次
 * 错误标志：翻转闪灯LED2/GPIO34
 版 本：      V1.0.1
 时 间：      2022年11月17日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
//
// Included Files
//
#include "device.h"
#include "driverlib.h"

//
// Defines
//

// Qualification period at 6 samples in microseconds
#define DELAY   (6.0 * 510.0 * 1000000.0 * (1.0 / DEVICE_SYSCLK_FREQ))

void GPIO_config(void);
//
// Globals
//
volatile uint32_t xint1Count;
volatile uint32_t xint2Count;
volatile uint32_t loopCount;

//
// Function Prototypes
//
__interrupt  void xint1ISR(void);
__interrupt  void xint2ISR(void);

//
// Main
//
int main(void)
{
    uint32_t tempX1Count;
    uint32_t tempX2Count;

//    /*系统时钟初始化*/
    Device_init();

    /*GPIO配置，用于显示中断状态*/
    GPIO_config();

    /*初始化PIE与清PIE寄存器，关CPU中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断入口INT_XINT1指向相应中断服务程序，执行xint1ISR*/
    Interrupt_register(INT_XINT1, &xint1ISR);
    /*中断入口INT_XINT1指向相应中断服务程序，执行xint1ISR*/
    Interrupt_register(INT_XINT2, &xint2ISR);

    /*重置中断计数*/
    xint1Count = 0; // Count XINT1 interrupts
    xint2Count = 0; // Count XINT2 interrupts
    loopCount = 0;  // Count times through idle loop

    /*xint相应的定时器时钟PIE与CPU中断使能：中断INT_XINTx*/
    Interrupt_enable(INT_XINT1);
    Interrupt_enable(INT_XINT2);

    /*打开全局中断*/
    EINT;
    /*关闭全局中断*/
    ERTM;

    /*GPIO10写入1置高*/
    GPIO_writePin(10, 1);
    /*配置GPIO10的方向为输出*/
    GPIO_setDirectionMode(10, GPIO_DIR_MODE_OUT);           // output

    /*GPIO9写入0置低*/
    GPIO_writePin(9, 0);
    /*配置GPIO9的方向为输出*/
    GPIO_setDirectionMode(9, GPIO_DIR_MODE_OUT);           // output

    /*配置GPIO0的方向为输入*/
    GPIO_setDirectionMode(0, GPIO_DIR_MODE_IN);             // input

    //
    // XINT1 Synch to SYSCLKOUT only
    //
    /*配置GPIO0的采样模式为同步采样*/
    GPIO_setQualificationMode(0, GPIO_QUAL_SYNC);
    /*配置GPIO2的方向为输入*/
    GPIO_setDirectionMode(2, GPIO_DIR_MODE_IN);             // input

    /*配置GPIO2的采样模式为6采样*/
    GPIO_setQualificationMode(2, GPIO_QUAL_6SAMPLE);

    /*配置GPIO0-GPIO7的采样周期为510周期系统时钟*/
    GPIO_setQualificationPeriod(3, 510);

    /*配置GPIO0的功能复用为XINT1外部中断*/
    GPIO_setInterruptPin(0, GPIO_INT_XINT1);
    /*配置GPIO2的功能复用为XINT2外部中断*/
    GPIO_setInterruptPin(2, GPIO_INT_XINT2);

    /*配置XINT1外部中断为GPIO_INT_TYPE_FALLING_EDGE下降沿触发*/
    GPIO_setInterruptType(GPIO_INT_XINT1, GPIO_INT_TYPE_FALLING_EDGE);

    /*配置XINT2外部中断为GPIO_INT_TYPE_RISING_EDGE上升沿触发*/
    GPIO_setInterruptType(GPIO_INT_XINT2, GPIO_INT_TYPE_RISING_EDGE);

    /*XINT相应的PIE与CPU中断使能：中断GPIO_INT_XINTx*/
    GPIO_enableInterrupt(GPIO_INT_XINT1);
    GPIO_enableInterrupt(GPIO_INT_XINT2);

    /*配置GPIO16的方向为输出*/
    GPIO_setDirectionMode(16, GPIO_DIR_MODE_OUT);  // output


    for(;;)
    {
        /*存储外部中断计数，记录上一次触发点*/
        tempX1Count = xint1Count;
        tempX2Count = xint2Count;

        /*GPIO16写入1置高*/
        GPIO_writePin(16, 1);

        /*GPIOA组的GPIO10端口清零，以触发外部中断XINT1*/
        GPIO_clearPortPins(GPIO_PORT_A, GPIO_GPADIR_GPIO10);
        /*等待外部中断计数不等于上一次计数值时，触发*/
        while(xint1Count == tempX1Count) {}

        /*GPIO16写入1置高*/
        GPIO_writePin(16, 1);

        /*等待6个采样周期*/
      DEVICE_DELAY_US(DELAY);

        /*GPIO9写1置高，以触发外部中断XINT1*/
        GPIO_writePin(9, 1);
        /*等待外部中断计数不等于上一次计数值时，触发*/
        while(xint2Count == tempX2Count) {}

        /*外部中断XINT1与XINT2两计数触发时，loopCount计数++，以指示外部中断触发*/
        if((xint1Count == tempX1Count + 1) && (xint2Count == tempX2Count + 1))
        {
            loopCount++;

            /*GPIO10写入1置高*/
            GPIO_writePin(10, 1);

            /*GPIO9写入0置低*/
            GPIO_writePin(9, 0);

            GPIO_togglePin(31);
        }
        else
        {
        	GPIO_togglePin(34);
        }
    }

    return 0;
}

//
// xint1ISR - External Interrupt 1 ISR
//
__interrupt  void xint1ISR(void)
{
    /*GPIO16写入0置低*/
    GPIO_writePin(16, 0); // GPIO16 is lowered
    xint1Count++;




    //
    // Acknowledge this interrupt to get more from group 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// xint2ISR - External Interrupt 2 ISR
//
__interrupt  void xint2ISR(void)
{
    /*GPIO16写入0置低*/
    GPIO_writePin(16, 0); // GPIO16 is lowered
    xint2Count++;

    //
    // Acknowledge this interrupt to get more from group 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// End of file
//
void GPIO_config(void)
{
    EALLOW;
    /*GPIO31的IO功能配置*/
    GPIO_setPinConfig(GPIO_1_GPIO1);
    /*GPIO31的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(1, GPIO_DIR_MODE_OUT);
    /*GPIO31的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    /*GPIO31的采样配置:GPIO_QUAL_SYNC同步采样,GPIO_QUAL_3SAMPLE-3采样
     * GPIO_QUAL_6SAMPLE-6采样，GPIO_QUAL_ASYNC异步采样*/
    GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);

    GPIO_setPinConfig(GPIO_34_GPIO34);
    GPIO_setDirectionMode(34, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(34, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(34, GPIO_QUAL_SYNC);
    EDIS;
}
