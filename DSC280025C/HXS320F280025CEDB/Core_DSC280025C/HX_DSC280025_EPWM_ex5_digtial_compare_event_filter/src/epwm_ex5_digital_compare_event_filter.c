/**************************************************************************************
 文 档 名：       HX_DSC280025_EPWM_ex5_digital_compare_event_filter
 开 发 环 境：  Haawking IDE V2.1.2
 开 发 板：
 D S P：        DSC280025
 使 用 库：
 作     用：      ePWM 使用数字比较器模块（Digital Compare ）事件触发器消窗模式（Blanking window)
 说     明：       FLASH工程
 -------------------------- 例程使用说明 ------------------------------------------

 ePWM1与DCBEVT1强制ePWM输出低
 GPIO25被用作input XBAR INPUT1的输入，INPUT1(来自INPUT XBAR)被用作DCAEVT1的源，
 GPIO25的PULL- up电阻被启用，为了测试跳闸，将这个引脚拉到GND
 DCBEVT1使用DCBEVT1的过滤器模式。
 在DC blanking window期间 DCFILT信号使用blanking Window 忽略 DCBEVT1

 外部连接：
 无

 观测变量：
 GPIO0 EPWM1A
 GPIO1 EPWM1B
 GPIO25 TRIPIN1, 该引脚拉低 PWM 跳闸

-------------------------------------------------------------------------------------
版 本：V1.0.0
 时 间：2023年1月1日
 作 者：
 @ mail：support@mail.haawking.com
*************************************************************************************/
//
// Included Files
//

#include "IQmathLib.h"
#include "syscalls.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"

//
// 全局变量
//
uint32_t  epwm1TZIntCount;

//
// 函数自定义
//
__interrupt void epwm1TZISR(void);

int main(void)
{
    //
    // 初始化全局变量
    //
    epwm1TZIntCount = 0U;

    //
    // 初始化时钟和外设
    //
    Device_init();

    //
    // 关闭引脚锁定和使能内部上拉
    //
    Device_initGPIO();

    //初始化PIE和清除PIE寄存器，关闭CPU中断
    Interrupt_initModule();

    //初始化PIE向量表
    Interrupt_initVectorTable();

    //中断在ISR函数重新映射
    Interrupt_register(INT_EPWM1_TZ, &epwm1TZISR);

    //关闭同步（也是停止PWM时钟）取消注释GTBCLKSYNC适用
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    // 配置 ePWM1, 和 TZ GPIO
       Board_init();

    //使能PWM同步和时钟
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //使能中断请求
    Interrupt_enable(INT_EPWM1_TZ);

    //使能全局中断和实时中断
    EINT;
    ERTM;

    // IDLE loop.
    for(;;)
    {
        NOP;
    }

    return 0;
}

//
// epwm1TZISR - ePWM1 TZ ISR
//
__interrupt void epwm1TZISR(void)
{
    epwm1TZIntCount++;

    //重新使能中断
    //EPWM_clearTripZoneFlag(EPWM1_BASE, EPWM_TZ_FLAG_DCAEVT1 | EPWM_TZ_FLAG_DCBEVT1 | EPWM_TZ_INTERRUPT);

    //确认中断在group2
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP2);
}

