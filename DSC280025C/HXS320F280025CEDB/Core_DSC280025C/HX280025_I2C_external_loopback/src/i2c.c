#include "system.h"

void I2CA_GPIO(void)
{
	/*配置GPIO26为I2CA数据引脚SDAA*/
	GPIO_setPinConfig(DEVICE_GPIO_CFG_SDAA);
	GPIO_setPadConfig(DEVICE_GPIO_PIN_SDAA, GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(DEVICE_GPIO_PIN_SDAA, GPIO_QUAL_ASYNC);
	/*配置GPIO27为I2CA时钟引脚SCLA*/
	GPIO_setPinConfig(DEVICE_GPIO_CFG_SCLA);
	GPIO_setPadConfig(DEVICE_GPIO_PIN_SCLA, GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCLA, GPIO_QUAL_ASYNC);
}

void I2CB_GPIO(void)
{
	/*配置GPIO2为I2B数据线SDAB*/
	GPIO_setPinConfig(DEVICE_GPIO_CFG_SDAB);
	GPIO_setPadConfig(DEVICE_GPIO_PIN_SDAB, GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(DEVICE_GPIO_PIN_SDAB, GPIO_QUAL_ASYNC);
	/*配置GPIO3为I2B时钟线SCLB*/
	GPIO_setPinConfig(DEVICE_GPIO_CFG_SCLB);
	GPIO_setPadConfig(DEVICE_GPIO_PIN_SCLB, GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCLB, GPIO_QUAL_ASYNC);
}

void initI2CFIFO()
{
	/*复位I2CA模块，以配置I2CA*/
    I2C_disableModule(I2CA_BASE);
	/*复位I2CB模块，以配置I2CB*/
    I2C_disableModule(I2CB_BASE);

    /*配置I2C主机：分频=SYSCLK，速率=400KHz，
     * I2C_DUTYCYCLE_50-时钟占比55%*/
    I2C_initMaster(I2CA_BASE, DEVICE_SYSCLK_FREQ,
    		400000, I2C_DUTYCYCLE_50);
    /*配置I2CA为：I2C_MASTER_SEND_MODE-主机发送*/
    I2C_setConfig(I2CA_BASE, I2C_MASTER_SEND_MODE);

    /*配置I2CA发送数据量-2个字节*/
    I2C_setDataCount(I2CA_BASE, 2);
    /*配置I2CA发送数据位数：I2C_BITCOUNT_8-8位*/
    I2C_setBitCount(I2CA_BASE, I2C_BITCOUNT_8);

    /*配置I2CB为：I2C_SLAVE_RECEIVE_MODE-从机接收*/
    I2C_setConfig(I2CB_BASE, I2C_SLAVE_RECEIVE_MODE);
    /*配置I2CB接收数据量-2个字节*/
    I2C_setDataCount(I2CB_BASE, 2);
    /*配置I2CB接收数据位数：I2C_BITCOUNT_8-8位*/
    I2C_setBitCount(I2CB_BASE, I2C_BITCOUNT_8);

    /*配置I2CA从机地址：SLAVE_ADDRESS-0x3C*/
    I2C_setSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);
    /*配置I2CB主机寻址地址：SLAVE_ADDRESS-0x3C*/
    I2C_setOwnSlaveAddress(I2CB_BASE, SLAVE_ADDRESS);
    /*配置I2C仿真模式：I2C_EMULATION_FREE_RUN-与运行状态无关*/
    I2C_setEmulationMode(I2CA_BASE, I2C_EMULATION_FREE_RUN);
    I2C_setEmulationMode(I2CB_BASE, I2C_EMULATION_FREE_RUN);

    /*I2CA_FIFO增强使能I2C_FFTX_I2CFFEN，
     * I2C_FFTX_TXFFRST-TX_FIFO发送FIFO复位
     * I2C_FFRX_RXFFRST-RX_FIFO接收FIFO复位*/
    I2C_enableFIFO(I2CA_BASE);
    /*清中断状态：I2C_INT_TXFF-发送FIFO*/
    I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_TXFF);

    /*配置I2C FIFO中断队列深度：I2C_FIFO_TX2-发送深度2个字节
     * I2C_FIFO_RX2-接收深度2个字节*/
    I2C_setFIFOInterruptLevel(I2CA_BASE, I2C_FIFO_TX2, I2C_FIFO_RX2);
    /*I2C中断使能：I2C_INT_TXFF-发送FIFO中断
     *I2C_INT_STOP_CONDITION-停止条件中断 */
    I2C_enableInterrupt(I2CA_BASE, I2C_INT_TXFF | I2C_INT_STOP_CONDITION);

    /*I2CB_FIFO增强使能I2C_FFTX_I2CFFEN，
     * I2C_FFTX_TXFFRST-TX_FIFO发送FIFO复位
     * I2C_FFRX_RXFFRST-RX_FIFO接收FIFO复位*/
    I2C_enableFIFO(I2CB_BASE);
    /*清中断状态：I2C_INT_RXFF-接收FIFO*/
    I2C_clearInterruptStatus(I2CB_BASE, I2C_INT_RXFF);

    /*配置I2C FIFO中断队列深度：I2C_FIFO_TX2-发送深度2个字节
     * I2C_FIFO_RX2-接收深度2个字节*/
    I2C_setFIFOInterruptLevel(I2CB_BASE, I2C_FIFO_TX2, I2C_FIFO_RX2);
    /*I2C中断使能：I2C_INT_RXFF-接收FIFO中断
     *I2C_INT_STOP_CONDITION-停止条件中断 */
    I2C_enableInterrupt(I2CB_BASE, I2C_INT_RXFF | I2C_INT_STOP_CONDITION);

	/*释放I2CA模块，以完成I2CA配置*/
    I2C_enableModule(I2CA_BASE);
	/*释放I2CB模块，以完成I2CB配置*/
    I2C_enableModule(I2CB_BASE);
}

__interrupt void i2cFIFOISR(void)
{
   uint16_t i;

   /*若FIFO中断状态为接收，则读取接收到的数据*/
   if((I2C_getInterruptStatus(I2CB_BASE) & I2C_INT_RXFF) != 0)
   {
       for(i = 0; i < 2; i++)
       {
           /*读取接收到的数据rData*/
    	   rData[i] = I2C_getData(I2CB_BASE);
       }

       /*校验接收到的数据是否与发送一致*/
       for(i = 0; i < 2; i++)
       {
           if(rData[i] != ((rDataPoint + i) & 0xFF))
           {
        	   GPIO_writePin(31,0);
           }
           else
           {
        	   GPIO_writePin(34,0);
           }
       }

       /*追踪接收数据*/
       rDataPoint = (rDataPoint + 1) & 0xFF;

//       GPIO_writePin(DEVICE_GPIO_PIN_LED1, 0);

       /*清除接收中断标志I2C_INT_RXFF*/
       I2C_clearInterruptStatus(I2CB_BASE, I2C_INT_RXFF);
   }

   /*若FIFO中断状态为发送，则发送数据*/
   else if((I2C_getInterruptStatus(I2CA_BASE) & I2C_INT_TXFF) != 0)
   {
       for(i = 0; i < 2; i++)
       {
    	   /*发送数据sData*/
    	   I2C_putData(I2CA_BASE, sData[i]);
       }

       /*I2C_STT启动条件发送：SCL为高时，SDA由高变低*/
       I2C_sendStartCondition(I2CA_BASE);

       /*发送数据更新*/
       for(i = 0; i < 2; i++)
       {
          sData[i] = (sData[i] + 1) & 0xFF;
       }

       /*清除I2C中断：I2C_INT_TXFF-发送FIFO状态*/
       I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_TXFF);


//       GPIO_writePin(DEVICE_GPIO_PIN_LED1, 1);
   }

   /*清除中断应答PIEACK*/
   Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}

