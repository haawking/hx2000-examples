/*
 * system.h
 *
 *  Created on: 2023��3��2��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define SLAVE_ADDRESS   0x3C

//
// Globals
//
extern uint16_t sData[2];                  // Send data buffer
extern uint16_t rData[2];                  // Receive data buffer
extern uint16_t rDataPoint;                    // To keep track of where we are in the
                                            // data stream to check received data

//
// Function Prototypes
//
void initI2CFIFO(void);
__interrupt void i2cFIFOISR(void);

void InitLED(void);

/*����GPIO26-SDAA GPIO27-SCLA*/
void I2CA_GPIO(void);
/*����GPIO2-SDAB GPIO3-SCLB*/
void I2CB_GPIO(void);

#endif /* SYSTEM_H_ */
