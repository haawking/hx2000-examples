/******************************************************************
 文 档 名：       HX_DSC280025_ADC_ppb_limits
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：
 D S P：          DSC280025
 使 用 库：
 作     用：      ADC顺序采样与后处理
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：
 (1)采用EWM_SOCA（向上计数）在向上计数CTR=CMPA时，触发A0通道ADC采样；
 (2)经后处理PPB模块产生事件，并触发中断：
 A.当采样值高于高阈值3000/4096*3.3V=2.4V时，产生超越高阈值事件，触发中断，使LED1灯亮
 B.当采样值低于低阈值1000/4096*3.3V=0.8V时，产生超越低阈值事件，触发中断，使LED2灯亮

连接：A0连接一可调的电位器

 版 本：      V1.0.0
 时 间：      2023年2月22日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "system.h"


void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定配置解除*/
    Device_initGPIO();
	/*配置GPIO31/GPIO34为IO输出，以指示ADC后处理器PPB事件触发*/
    GPIO_config();

	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*中断向量入口地址INT_ADCA_EVT，指向执行adcAEvtISR中断服务程序*/
	Interrupt_register(INT_ADCA_EVT, &adcAEvtISR);
	EDIS;

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();
    EDIS;

    /*配置EPWM模块*/
    configureEPWM(EPWM1_BASE);
	/*中断使能INT_ADCA_EVT*/
	Interrupt_enable(INT_ADCA_EVT);

	/*打开全局中断*/
    EINT;
    ERTM;

    /*使能EPWM时基时钟TBCLK，以完成PWM配置与同步*/
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    /*使能ADC触发：EPWM_SOC_A-SOCA*/
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);

    /*配置EPWM计数模式：EPWM_COUNTER_MODE_UP-向上计数*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    do
    {}
    while(1);
}
//
// End of file
//
