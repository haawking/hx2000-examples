/******************************************************************************************
 文 档 名：       HX_DSC2800137_EPWM_ex10_chopper
 开 发 环 境：    Haawking IDE V2.3.8Pre
 开 发 板：
 D S P：           DSC2800137
 使 用 库：
 作     用：       ePWM使用斩波模块
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------

 ePWM1, ePWM2, ePWM3  ePWM4 如下设置：
  - ePWM1 关闭斩波（参考）
  - ePWM2 启用斩波在 1/8 占空比
  - ePWM3 启用斩波在 6/8 占空比
  - ePWM4 启用斩波在 1/2 占空比 与One-Shot 脉冲


外部连接：
 - GPIO0  EPWM1A
 - GPIO1  EPWM1B
 - GPIO2  EPWM2A
 - GPIO3  EPWM2B
 - GPIO4  EPWM3A
 - GPIO5  EPWM3B
 - GPIO6  EPWM4A
 - GPIO7  EPWM4B

观测变量：
 - 无
------------------------------------------------------------------------------------------
版 本：V1.0.0
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
******************************************************************************************/
//
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"
#include "driverlib.h"
#include "device.h"
#include "board.h"


void main(void)
{

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pull-ups.
    //
    Device_initGPIO();

    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Disable sync(Freeze clock to PWM as well). GTBCLKSYNC is applicable
    // only for multiple core devices. Uncomment the below statement if
    // applicable.
    //
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // Configure ePWM GPIOs and Modules
    //
    Board_init();

    //
    // Enable sync and clock to PWM
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // Enable Global Interrupt (INTM) and real time interrupt (DBGM)
    //
    EINT;
    ERTM;

    //
    // IDLE loop. Just sit and loop forever (optional):
    //
    for(;;)
    {
        NOP;
    }
}


