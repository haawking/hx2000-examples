/*************************************************************************************
 文 档 名：       HX_DSC2800137_EPWM_ex7_edge_filter
 开 发 环 境：    Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：           DSC2800137
 使 用 库：
 作     用：       ePWM 数字比较子模块使用边缘过滤器
 说     明：       FLASH工程
 -------------------------- 例程使用说明 ---------------------------------------------

 本示例如下配置ePWM1
 带有DCBEVT2的ePWM1强制ePWM输出低作为CBC源
 GPIO24用作input XBAR 的INPUT1输入
 INPUT1(来自INPUT XBAR)被用作DCBEVT2的源
 GPIO24设置为输出，并在主循环中翻转，用于触发PWM跳闸
 DCFILT的源是DCBEVT2
 DCFILT对DCBEVT2的边沿计数，并生成一个信号，在DCBEVT2的第4个边沿时，触发ePWM跳闸

 外部连接：
 GPIO0 EPWM1A
 GPIO1 EPWM1B
 GPIO24 TRIPIN1, 程序中翻转

 观测变量：
 无
-------------------------------------------------------------------------------------
版 本：V1.0.1
时 间：2025年2月12日
作 者：liyuyao
@ mail：support@mail.haawking.com
*************************************************************************************/
//
// Included Files
//

#include "syscalls.h"
#include "IQmathLib.h"
#include "syscalls.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"


//
// Function Prototypes
//
__interrupt void epwm1ISR(void);

int main(void)
{

	//初始化时钟和外设
    Device_init();

    //关闭引脚锁定并使能内部上拉
    Device_initGPIO();

    //初始化PIE和清除PIE寄存器，关闭CPU中断
    Interrupt_initModule();

    //初始化PIE向量表
    Interrupt_initVectorTable();


    //使用的中断被重新映射到ISR函数
    Interrupt_register(INT_EPWM1, &epwm1ISR);

    ////关闭同步（也是停止PWM时钟)
    //取消注释适应GTBCLKSYNC
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    //配置ePWM1,TZ
    Board_init();
    GPIO_writePin(myGPIO24, 1);


    //使能PWM时钟和同步
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    //使能中断请求
    Interrupt_enable(INT_EPWM1);

    //使能全局中断和实时中断
    EINT;
    ERTM;


    // 延迟ePWM不间断循环
    SysCtl_delay(600000U);


    //空闲循环
    for(;;)
    {

        //多次翻转GPIO，在第四个边沿就会发生跳闸
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 4000);
        GPIO_writePin(myGPIO24, 0);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 5000);
        GPIO_writePin(myGPIO24, 1);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 6000);
        GPIO_writePin(myGPIO24, 0);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 7000);
        GPIO_writePin(myGPIO24, 1);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 8000);
        GPIO_writePin(myGPIO24, 0);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 9000);
        GPIO_writePin(myGPIO24, 1);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 10000);
        GPIO_writePin(myGPIO24, 0);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) < 11000);
        GPIO_writePin(myGPIO24, 1);
        while(HWREG(myEPWM1_BASE + EPWM_O_TBCTR) > 3000);
        GPIO_writePin(myGPIO24, 1);
        //NOP;
    }

    return 0;
}

__interrupt void epwm1ISR(void){

    EPWM_clearTripZoneFlag(myEPWM1_BASE, EPWM_TZ_FLAG_CBC | EPWM_TZ_FLAG_DCBEVT2 | EPWM_TZ_INTERRUPT);
    EPWM_clearCycleByCycleTripZoneFlag(myEPWM1_BASE, EPWM_TZ_CBC_FLAG_DCBEVT2);

    //清除中断标志
    EPWM_clearEventTriggerInterruptFlag(myEPWM1_BASE);

    //确认中断组
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);

}

