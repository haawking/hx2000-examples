/*
 * system.h
 *
 *  Created on: 2023年2月7日
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define ADC_RESULT_NUM     16

extern volatile uint16 bufferFull;                // Flag to indicate buffer is full
extern uint16_t myIndex;                              // Index into result buffer
extern uint16_t myADC0Results[ADC_RESULT_NUM];   // Buffer for results
//
// Function Prototypes
//
void initEPWM(void);

__interrupt void adcA1ISR(void);

/*配置AdcAio模拟量输入引脚*/
void InitAdcAio(void);
/*配置Adc参考电压*/
void InitAdc(void);
/*配置Adc模块*/
void Adc_config(void);

#endif /* SYSTEM_H_ */
