/******************************************************************
 文 档 名：       HX_DSC2800137_ADC
 开 发 环 境：  Haawking IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      ADC顺序采样
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用PWM SOCA在向上计数CTR=CMPA时,
 通过A0通道顺序采样输入电压

 现象:
通过A0通道连接相应电位器元件,可读出正确的电压值Vi=myADC0Results[0]/4096*3.3V

 版 本：      V1.0.0
 时 间：      2025年2月11日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/


//
// Included Files
//
#include "system.h"

//
// Main
//
void main(void)
{
	/*配置AdcAio模拟量输入引脚*/
    Device_init();
	/*GPIO锁定解除*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();
	/*配置INT_ADCA1中断入口地址，指向执行adcA1ISR中断服务程序*/
	Interrupt_register(INT_ADCA1, &adcA1ISR);
	/*配置INT_ADCA1中断使能*/
	Interrupt_enable(INT_ADCA1);
    EDIS;

	/*配置EPWM初始化*/
    initEPWM();

	/*使能打开全局中断*/
    EINT;
    ERTM;

    //
    // Start ePWM1, enabling SOCA and putting the counter in up-count mode
    //
	/*使能启动ADC触发：EPWM_SOC_A-EPWM_SOCA*/
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
	/*配置时钟计数模式：EPWM_COUNTER_MODE_UP-向上模式*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // Loop indefinitely
    //
    while(1)
    {

    }
}

//
// Function to configure ePWM1 to generate the SOC.
//
