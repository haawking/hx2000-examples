/******************************************************************
 文 档 名：       HX_DSC2800137_watchdog_service
 开 发 环 境：    Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：     看门狗触发唤醒中断
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，看门狗触发唤醒中断

外部接线：无

现象： 注释掉主循环的SysCtl_serviceWatchdog()，会导致看门狗复位，触发唤醒中断动作，
       翻转GPIO20/LED1灯快速闪烁

 版 本：      V1.0.1
 时 间：      2025年2月11日
 作 者：      liyuyao
 @ mail：     support@mail.haawking.com
 ******************************************************************/

//
//
//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Globals
//
uint32_t wakeCount;
uint32_t loopCount;

//
// Function Prototypes
//
__interrupt void wakeupISR(void);
void GPIO_config(void);

//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*配置GPIO20为IO输出,以指示看门狗触发唤醒中断状态*/
    GPIO_config();

    /*关中断,清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*中断向量入口地址INT_WAKE,指向执行wakupISR中断服务程序*/
    Interrupt_register(INT_WAKE, &wakeupISR);

    /*配置看门狗模式触发功能:
     * SYSCTL_WD_MODE_INTERRUPT-看门狗触发中断*/
    SysCtl_setWatchdogMode(SYSCTL_WD_MODE_INTERRUPT);

    /*唤醒中断INT_WAKE使能*/
    Interrupt_enable(INT_WAKE);

    /*打开全局中断*/
    EINT;
    ERTM;

    /*复位看门狗计数器*/
    SysCtl_serviceWatchdog();
    /*看门狗使能*/
    SysCtl_enableWatchdog();
    for(;;)
    {
        loopCount++;
        //
        // Uncomment SysCtl_serviceWatchdog to just loop here.
        // Comment SysCtl_serviceWatchdog to have watchdog timeout and trigger
        // an interrupt signal to execute the wakeupISR
        //
//        SysCtl_serviceWatchdog();
    }
}

__interrupt void wakeupISR(void)
{
    wakeCount++;
    GPIO_togglePin(20);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

void GPIO_config(void)
{
    EALLOW;
    /*GPIO20的IO功能配置*/
    GPIO_setPinConfig(GPIO_20_GPIO20);
    /*GPIO20的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(20, GPIO_DIR_MODE_OUT);
    /*GPIO20的上拉翻转配置:
     * GPIO_PIN_TYE_STD上拉翻转输出或浮点输入,GPIO_PIN_TYPE_PULLUP-上拉翻转输入
     * GPIO_PIN_TYPE_INVERT翻转极性输入，GPIO_PIN_TYPE_OD开漏输出*/
    GPIO_setPadConfig(20, GPIO_PIN_TYPE_STD);

    GPIO_writePin(20,1);
    EDIS;
}


//
// End of File
//
