/******************************************************************************************
 文 档 名：       HX_DSC2800137_EPWM_ex1_Trip-zone
 开 发 环 境：    Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：           DSC2800137
 使 用 库：
 作     用：       ePWM使用TripZone子模块
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------

  - ePWM1 是 TZ1  one shot trip source
  - ePWM2 是 TZ1  cycle by cycle trip source

  最初TZ1置高。在测试期间，示波器观察ePWM1或ePWM2输出。
  拉低TZ1置低，ePWM1,ePWM2全置高。
  复原TZ1置高，ePWM1依旧置高，ePWM2恢复1KHz波形

外部连接：
  - ePWM1A ---------GPIO0
  - ePWM2A ---------GPIO2
  - TZ1  ---------- GPIO12


本例还使用了Input X-BAR。GPIO12(外部触发器)被路由到输入X-BAR，从X-BAR到TZ1。

TZ-Event的定义是，ePWM1A将进行单次跳闸，而ePWM2A将进行逐周期跳闸。
              _____________             __________________
              |           |             |                |
  GPIO12 -----| I/P X-BAR |-----TZ1-----| ePWM TZ Module |-----TZ-Event
              |___________|             |________________|


版 本：V1.0.0
时 间：2025年2月12日
作 者：liyuyao
@ mail：support@mail.haawking.com
******************************************************************************************/
//
// Included Files
//
#include <syscalls.h>
#include "IQmathLib.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"

//
// Globals
//
uint32_t  epwm1TZIntCount;
uint32_t  epwm2TZIntCount;

//
// 函数自定义
//
__interrupt void epwm1TZISR(void);
__interrupt void epwm2TZISR(void);

int main(void)
{
    //
    //初始化全局参数
    //
    epwm1TZIntCount = 0U;
    epwm2TZIntCount = 0U;

    //初始化时钟和外设
    Device_init();

    //关闭GPIO引脚锁定和开启内部上拉
    Device_initGPIO();

    //初始化PIE和清除PIE寄存器，关闭CPU中断
    Interrupt_initModule();

    /*用指向中断服务例程（ISR）初始化PIE向量表*/
    Interrupt_initVectorTable();


    //关闭同步（也是停止PWM时钟）
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);


    //配置ePWM1，ePWM2，TZ GPIO
    Board_init();


    //使能PWM时钟同步
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //使能全局中断和实时中断
    EINT;
    ERTM;

    //空闲循环。永远等待和循环（可选）：
    for(;;)
    {
        NOP;
    }

    return 0;
}

//
// epwm1TZISR - ePWM1 TZ ISR
//
__interrupt void epwm1TZISR(void)
{
    epwm1TZIntCount++;

    //若要重新启用OST中断，取消注释
//     EPWM_clearTripZoneFlag(EPWM1_BASE, (EPWM_TZ_INTERRUPT | EPWM_TZ_FLAG_OST));

    //确认中断来自group2
    Interrupt_clearACKGroup(INT_myEPWM1_TZ_INTERRUPT_ACK_GROUP);
}

//
// epwm2TZISR - ePWM2 TZ ISR
//
__interrupt void epwm2TZISR(void)
{
    epwm2TZIntCount++;

    //当TZ信号输入GPIO翻转
    GPIO_togglePin(myGPIO11);

    //清除标志，一直中断直至TZ引脚置高
    EPWM_clearTripZoneFlag(myEPWM2_BASE, (EPWM_TZ_INTERRUPT | EPWM_TZ_FLAG_CBC));

    //确认中断来自group2
    Interrupt_clearACKGroup(INT_myEPWM2_TZ_INTERRUPT_ACK_GROUP);
}

