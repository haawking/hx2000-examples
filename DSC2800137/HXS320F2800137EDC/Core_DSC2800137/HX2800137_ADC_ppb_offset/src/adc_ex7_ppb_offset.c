/******************************************************************
 文 档 名：       HX_DSC2800137_ADC_ppb_offset
 开 发 环 境：    Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      ADC顺序采样与后处理
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用软件触发ADC顺序采用SOC0与SOC1，
 经后处理PPB模块偏移校准输出

连接：A2，C2连接一可调的电位器

 现象:
 （1）A2上所读出的采样结果与电位器两端电压一致
 即Vo=myADC0Result/4096*3.3V
经后处理的结果myADC0PPBResult为myADC0Result - 100 LSB
（2）C2上所读出的采样结果与电位器两端电压一致
 即Vo=myADC1Result/4096*3.3V
经后处理的结果myADC1PPBResult为myADC1Result + 100 LSB

 版 本：      V1.0.0
 时 间：      2025年2月12日
 作 者：	  liyuyao
 @ mail：     support@mail.haawking.com
 ******************************************************************/
#include "system.h"

//
// Globals
//
uint16_t myADC0Result;
uint16_t adc_A2Result;

uint16_t myADC0PPBResult;
uint16_t myADC1Result;
uint16_t adc_C2Result;

uint16_t myADC1PPBResult;

uint32_t i;

void main(void)
{
	/*系统时钟初始化*/
    Device_init();
    /*GPIO锁定解除*/
    Device_initGPIO();
    /*关中断，清中断*/
    Interrupt_initModule();
    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    AdcA_config();
    AdcC_config();
    EDIS;

    /*打开全局中断*/
    EINT;
    ERTM;

    while(1)
    {
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER0-SOC0*/
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER1-SOC1*/
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER1);

        /*等待ADC中断状态退出ADC_INT_NUMBER1-ADCINT1中断，即完成转换*/
        while(ADC_getInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1) == false);
        /*清除ADC中断：ADC_INT_NUMBER1-ADCINT1*/
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

      	/*强制转换ADC_SOC:ADC_SOC_NUMBER0-SOC0*/
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER0);
    	/*强制转换ADC_SOC:ADC_SOC_NUMBER1-SOC1*/
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER1);


        /*等待ADC中断状态退出ADC_INT_NUMBER1-ADCINT1中断，即完成转换*/
        while(ADC_getInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1) == false);
        /*清除ADC中断：ADC_INT_NUMBER1-ADCINT1*/
        ADC_clearInterruptStatus(ADCC_BASE, ADC_INT_NUMBER1);

        /*读取ADC_SOC初始结果：ADC_SOC_NUMBER0 - SOC0*/
        myADC0Result = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);

        /*读取ADC_SOC后处理PPB处理结果：ADC_PPB_NUMBER1-PPB1*/
        myADC0PPBResult = ADC_readPPBResult(ADCARESULT_BASE, ADC_PPB_NUMBER1);

        /*读取ADC_SOC初始结果：ADC_SOC_NUMBER1 - SOC0*/
        myADC1Result = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER0);

        /*读取ADC_SOC后处理PPB处理结果：ADC_PPB_NUMBER1-PPB1*/
        myADC1PPBResult = ADC_readPPBResult(ADCCRESULT_BASE, ADC_PPB_NUMBER1);
    }
}

//
// End of file
//
