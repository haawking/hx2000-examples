/******************************************************************
 文 档 名：       HX_DSC2800137_GPIO_AIO_external_interrupt
 开 发 环 境：    Haawking IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      GPIO224和GPIO226触发外部中断XINT1和XINT2
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：

 - Connect GPIO0 to AIO224(A2/C9).  AIO224 will be assigned to XINT1
 - Connect GPIO1 to AIO226(A6). 	AIO226 will be assigned to XINT2


现象：
（1）GPIO5在中断期间拉高，中断外拉低
（2）可以在Liveview中观察xint1Count和xint2Count的值

 *
 版 本：      V1.0.0
 时 间：      2025年2月13日
 作 者：      liyuyao
 @ mail：     support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Globals
//
volatile uint32_t xint1Count = 0;
volatile uint32_t xint2Count = 0;
uint32_t loopCount = 0;

//
// Function Prototypes
//
__interrupt void xint1ISR(void);
__interrupt void xint2ISR(void);

//
// Main
//
int main(void)
{
    uint32_t xint1CountTemp;
    uint32_t xint2CountTemp;

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();

    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Interrupts that are used in this example are re-mapped to ISR functions
    // found within this file.
    //
    Interrupt_register(INT_XINT1, &xint1ISR);
    Interrupt_register(INT_XINT2, &xint2ISR);

    //
    // Enable XINT interrupts
    //
    Interrupt_enable(INT_XINT1);
    Interrupt_enable(INT_XINT2);

    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;
    ERTM;

    //
    // GPIO0 & GPIO1 are outputs that will trigger the interrupts through
    // AIO224 and AIO226.  Starting with GPIO0 as high and GPIO1 as low.
    //
    GPIO_writePin(0, 1);
    GPIO_setPinConfig(GPIO_0_GPIO0);
    GPIO_setDirectionMode(0, GPIO_DIR_MODE_OUT);

    GPIO_writePin(1, 0);
    GPIO_setPinConfig(GPIO_1_GPIO1);
    GPIO_setDirectionMode(1, GPIO_DIR_MODE_OUT);


    //
    // AIO224 and AIO226 are inputs and the pins tied to the external interrupts.
    // AIO224 will be synchronous to SYSCLKOUT only.  AIO226 will use a
    // qualification mode of 6 samples. Configure AIO in digital input mode.
    // These AIO pins do not have digital output capability. Setting direction
	// of the AIO pin is not required.
    //
    GPIO_setPinConfig(GPIO_224_GPIO224);
    GPIO_setAnalogMode(224, GPIO_ANALOG_DISABLED);
    GPIO_setQualificationMode(224, GPIO_QUAL_SYNC);

    GPIO_setPinConfig(GPIO_226_GPIO226);
    GPIO_setAnalogMode(226, GPIO_ANALOG_DISABLED);
    GPIO_setQualificationMode(226, GPIO_QUAL_6SAMPLE);

    //
    // Each sampling window will be 510 SYSCLKOUT cycles.  Note that this
    // function actually sets the qualification period for GPIOs 0 through 7
    // (if they are using qualification).
    //
    GPIO_setQualificationPeriod(226, 510);

    //
    // Select AIO224 as XINT1 and AIO226 as XINT2
    //
    GPIO_setInterruptPin(224, GPIO_INT_XINT1);
    GPIO_setInterruptPin(226, GPIO_INT_XINT2);

    //
    // Configure XINT1 to be a triggered by a falling edge and XINT2 to be
    // triggered by a rising edge.
    //
    GPIO_setInterruptType(GPIO_INT_XINT1, GPIO_INT_TYPE_FALLING_EDGE);
    GPIO_setInterruptType(GPIO_INT_XINT2, GPIO_INT_TYPE_RISING_EDGE);

    //
    // Enable XINT1 and XINT2
    //
    GPIO_enableInterrupt(GPIO_INT_XINT1);
    GPIO_enableInterrupt(GPIO_INT_XINT2);

    //
    // GPIO5 will go low inside each interrupt.  Monitor this on a scope.
    //
    GPIO_setPinConfig(GPIO_5_GPIO5);
    GPIO_setDirectionMode(5, GPIO_DIR_MODE_OUT);

    //
    // Loop indefinitely
    //
    while(1)
    {
        xint1CountTemp = xint1Count;
        xint2CountTemp = xint2Count;

        //
        // Trigger XINT1
        //
        GPIO_writePin(5, 1);       // GPIO5 is high
        GPIO_writePin(0, 0);       // Lower GPIO0, trigger XINT1

        //
        // Wait until ISR has finished
        //
        while(xint1Count == xint1CountTemp)
        {
            ;
        }

        //
        // Trigger XINT2
        //
        GPIO_writePin(5, 1);       // GPIO5 is high
        DEVICE_DELAY_US(DELAY);     // Wait for qual period
        GPIO_writePin(1, 1);       // Raise GPIO1, trigger XINT2

        //
        // Wait until ISR has finished
        //
        while(xint2Count == xint2CountTemp)
        {
            ;
        }

        //
        // Check that the counts were incremented properly and get ready
        // to start over.
        //
        if((xint1Count == (xint1CountTemp + 1)) &&
           (xint2Count == (xint2CountTemp + 1)))
        {
            loopCount++;
            GPIO_writePin(0, 1);   // Raise GPIO0
            GPIO_writePin(1, 0);   // Lower GPIO1
        }
        else
        {
            //
            // Something went wrong
            //
            ESTOP0;
        }
    }
    return 0;
}

//
// xint1ISR - XINT1 ISR
//
__interrupt void xint1ISR(void)
{
    //
    // Lower GPIO5 and increment interrupt count
    //
    GPIO_writePin(5, 0);
    xint1Count++;

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

//
// xint2ISR -  XINT2 ISR
//
__interrupt void xint2ISR(void)
{
    //
    // Lower GPIO5 and increment interrupt count
    //
    GPIO_writePin(5, 0);
    xint2Count++;

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

