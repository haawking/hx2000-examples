/******************************************************************************************
 文 档 名：       HX_DSC2800137_EPWM_ex8_deadband
 开 发 环 境：    Haawking IDE V2.3.8Pre
 开 发 板：
 D S P：           DSC2800137
 使 用 库：
 作     用：       ePWM 使用 死区模块（DeadBand）
 说     明：       FLASH工程
 -------------------------- 例程使用说明 -------------------------------------------------

  - ePWM1 with Deadband disabled (Reference)
  - ePWM2 with Deadband Active High
  - ePWM3 with Deadband Active Low
  - ePWM4 with Deadband Active High Complimentary
  - ePWM5 with Deadband Active Low Complimentary
  - ePWM6 with Deadband Output Swap (switch A and B outputs)
本例对ePWM1到ePWM6的配置如下:
 ePWM1 禁用死区(参考)
 ePWM2 死区 Active High
 ePWM3 死区 Active Low
 ePWM4 死区 Active High Complimentary
 ePWM5 死区 Active Low Complimentary
 ePWM6 死区 输出交换 (切换A和B输出)

外部连接：
 - GPIO0 EPWM1A
 - GPIO1 EPWM1B
 - GPIO2 EPWM2A
 - GPIO3 EPWM2B
 - GPIO4 EPWM3A
 - GPIO5 EPWM3B
 - GPIO6 EPWM4A
 - GPIO7 EPWM4B
 - GPIO8 EPWM5A
 - GPIO9 EPWM5B
 - GPIO10 EPWM6A
 - GPIO11 EPWM6B
观测变量：
无

版 本：V1.0.0
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
************************************************************************************/
//
// Included Files
//

#include "IQmathLib.h"
#include "syscalls.h"
#include "stdlib.h"
#include "time.h"

#include "driverlib.h"
#include "device.h"
#include "board.h"

int main(void)
{

	//初始化时钟和外设
    Device_init();

    //关闭引脚锁定并使能内部上拉
    Device_initGPIO();

    //初始化PIE和清除PIE寄存器，关闭CPU中断
    Interrupt_initModule();

    //本例中使用的中断被重新映射到ISR函数
    Interrupt_initVectorTable();

    //取消注释适应GTBCLKSYNC
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    //关闭同步（也是停止PWM时钟)

    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //配置 ePWM
    Board_init();
    
    //使能PWM同步和时钟
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //使能全局中断和实时中断
    EINT;
    ERTM;

    //空闲循环
    for(;;)
    {
        NOP;
    }

    return 0;
}


