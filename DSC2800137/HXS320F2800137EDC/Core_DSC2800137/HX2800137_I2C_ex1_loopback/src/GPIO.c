/*
 * GPIO.c
 *
 *  Created on: 2024��10��25��
 *      Author: Administrator
 */
#include "device.h"

void setup1GPIO(void){

	GPIO_setPinConfig(GPIO_20_GPIO20);
	GPIO_setPadConfig(20, GPIO_PIN_TYPE_PULLUP);
	GPIO_setDirectionMode(20,GPIO_DIR_MODE_OUT );

	GPIO_setPinConfig(GPIO_22_GPIO22);
	GPIO_setPadConfig(22, GPIO_PIN_TYPE_PULLUP);
	GPIO_setDirectionMode(22,GPIO_DIR_MODE_OUT );

	GPIO_writePin(20, 1);
	GPIO_writePin(22,1);

}
