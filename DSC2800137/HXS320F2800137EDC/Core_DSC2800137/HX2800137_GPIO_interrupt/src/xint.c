#include "system.h"

void XINT_IOinit(void)
{
	/*配置GPIO1为IO输入，用于外部中断触发*/
	GPIO_setPinConfig(GPIO_1_GPIO1);
	GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD | GPIO_PIN_TYPE_PULLUP);
	GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(1, GPIO_DIR_MODE_IN);

	/*配置GPIO20为IO输出*/
	GPIO_setPinConfig(GPIO_20_GPIO20);
	GPIO_setPadConfig(20, GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(20, GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(20, GPIO_DIR_MODE_OUT);
}
