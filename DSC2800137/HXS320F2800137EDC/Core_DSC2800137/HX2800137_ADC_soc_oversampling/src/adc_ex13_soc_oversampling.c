/******************************************************************
 文 档 名：       HX_DSC2800137_ADC_soc_oversampling
 开 发 环 境：	  Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      ADC多路SOC单通道采样
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：采用PWM SOCA在向上计数CTR=CMPA时,
 通过A0/A1通道，采样SOC输入电压
 A2通道，采用SOC2-SOC5四路输入电压平均值实现采样

 现象:
通过A0/A1/A2通道连接相应电位器元件,
可读出正确的电压值Vi=result/4096*3.3V

 版 本：      V1.0.0
 时 间：      2025年2月11日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Main
//
int main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*GPIO锁定解除*/
    Device_initGPIO();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置INT_ADCA1中断入口地址，指向执行adcA1ISR中断服务程序*/
	Interrupt_register(INT_ADCA1, &adcA1ISR);
    EDIS;

    EALLOW;
	/*配置AdcAio模拟量输入引脚*/
    InitAdcAio();
	/*配置Adc参考电压*/
    InitAdc();
	/*配置Adc模块*/
    Adc_config();
    EDIS;

	/*配置EPWM模块及ADC_SOCA触发*/
    initEPWM();

	/*中断使能INT_ADCA1*/
    Interrupt_enable(INT_ADCA1);

	/*关闭全局中断*/
    EINT;
    ERTM;

    /*使能ADC通过EPWM_SOC_A-EPWM_SOCA触发*/
    EPWM_enableADCTrigger(EPWM1_BASE, EPWM_SOC_A);
    /*配置EPWM计数模式：EPWM_COUNTER_MODE_UP-向上计数*/
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP);

    //
    // Loop indefinitely
    //
    while(1)
    {

    }
    return 0;
}

//
// End of file
//
