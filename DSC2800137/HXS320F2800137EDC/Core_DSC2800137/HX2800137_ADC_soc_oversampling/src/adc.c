#include "system.h"

void InitAdcAio(void)
{
	/*配置GPIO231为模拟量输入A0/C15*/
	GPIO_setPinConfig(GPIO_231_GPIO231);
	/*配置GPIO231模拟量输入*/
	GPIO_setAnalogMode(231, GPIO_ANALOG_ENABLED);

	/*配置GPIO232为模拟量输入A1*/
	GPIO_setPinConfig(GPIO_232_GPIO232);
	GPIO_setAnalogMode(232, GPIO_ANALOG_ENABLED);
	/*配置GPIO230为模拟量输入A10/C10*/
	GPIO_setPinConfig(GPIO_230_GPIO230);
	GPIO_setAnalogMode(230, GPIO_ANALOG_ENABLED);
	/*配置GPIO237为模拟量输入A11/C0*/
	GPIO_setPinConfig(GPIO_237_GPIO237);
	GPIO_setAnalogMode(237, GPIO_ANALOG_ENABLED);
	/*配置GPIO238为模拟量输入A12/C1*/
	GPIO_setPinConfig(GPIO_238_GPIO238);
	GPIO_setAnalogMode(238, GPIO_ANALOG_ENABLED);
	/*配置GPIO239为模拟量输入A14/C4*/
	GPIO_setPinConfig(GPIO_239_GPIO239);
	GPIO_setAnalogMode(239, GPIO_ANALOG_ENABLED);
	/*配置GPIO233为模拟量输入A15/C7*/
	GPIO_setPinConfig(GPIO_233_GPIO233);
	GPIO_setAnalogMode(233, GPIO_ANALOG_ENABLED);
	/*配置GPIO224为模拟量输入A2/C9*/
	GPIO_setPinConfig(GPIO_224_GPIO224);
	GPIO_setAnalogMode(224, GPIO_ANALOG_ENABLED);
	/*配置GPIO242为模拟量输入A3/C5/VDAC*/
	GPIO_setPinConfig(GPIO_242_GPIO242);
	GPIO_setAnalogMode(242, GPIO_ANALOG_ENABLED);
	/*配置GPIO225为模拟量输入A4/C14*/
	GPIO_setPinConfig(GPIO_225_GPIO225);
	GPIO_setAnalogMode(225, GPIO_ANALOG_ENABLED);
	/*配置GPIO244为模拟量输入A5/C2*/
	GPIO_setPinConfig(GPIO_244_GPIO244);
	GPIO_setAnalogMode(244, GPIO_ANALOG_ENABLED);
	/*配置GPIO228为模拟量输入A6*/
	GPIO_setPinConfig(GPIO_228_GPIO228);
	GPIO_setAnalogMode(228, GPIO_ANALOG_ENABLED);
	/*配置GPIO245为模拟量输入A7/C3*/
	GPIO_setPinConfig(GPIO_245_GPIO245);
	GPIO_setAnalogMode(245, GPIO_ANALOG_ENABLED);
	/*配置GPIO241为模拟量输入A8/C11*/
	GPIO_setPinConfig(GPIO_241_GPIO241);
	GPIO_setAnalogMode(241, GPIO_ANALOG_ENABLED);
	/*配置GPIO227为模拟量输入A9/C8*/
	GPIO_setPinConfig(GPIO_227_GPIO227);
	GPIO_setAnalogMode(227, GPIO_ANALOG_ENABLED);
	/*配置GPIO226为模拟量输入C6*/
	GPIO_setPinConfig(GPIO_226_GPIO226);
	GPIO_setAnalogMode(226, GPIO_ANALOG_ENABLED);
}

void InitAdc(void)
{
	/*屏蔽ADC温度传感器功能*/
	ASysCtl_disableTemperatureSensor();
	/*ADC参考电压配置为:ADC_REFERENCE_INTERNAL-内部参考电压,
	* ADC_REFERENCE_3_3V - VDDA*/
	ADC_setVREF(ADCA_BASE,ADC_REFERENCE_INTERNAL,ADC_REFERENCE_VDDA);
}

void Adc_config(void)
{
	/*配置ADC时钟为ADC_CLK_DIV_4_0 -- 4分频对应SYSCLK/4=40M*/
	ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_4_0);
	/*配置ADC中断脉冲模式: ADC_PULSE_END_OF_CONV-转换完成产生脉冲触发中断*/
	ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
	/*ADC上电使能转换*/
	ADC_enableConverter(ADCA_BASE);
	/*延时1ms完成ADC模块上电*/
	DEVICE_DELAY_US(5000);

	/*屏蔽ADC突发模式*/
	ADC_disableBurstMode(ADCA_BASE);
	/*配置ADC_SOC转换优先级：ADC_PRI_ALL_ROUND_ROBIN-从A0开始循环转换*/
	ADC_setSOCPriority(ADCA_BASE, ADC_PRI_ALL_ROUND_ROBIN);

	/*配置ADC采样：ADC_SOC_NUMBER0-SOC0，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
	 * ADC_CH_ADCIN0-采用A0通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN0, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER0-SOC0
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER0,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER1-SOC1，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
		 * ADC_CH_ADCIN1-采用A1通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN1, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER1-SOC1
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER1,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER2-SOC2，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
		 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER2,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER2-SOC2
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER2,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER3-SOC3，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
		 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER3,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER3-SOC3
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER3,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER4-SOC4，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
		 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER4,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER4-SOC4
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER4,
			ADC_INT_SOC_TRIGGER_NONE);

	/*配置ADC采样：ADC_SOC_NUMBER5-SOC5，ADC_TRIGGER_EPWM1_SOCA-采样EPWM1_SOCA触发
		 * ADC_CH_ADCIN2-采用A2通道采样，采样周期8*SYSCLK*/
	ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER5,
			ADC_TRIGGER_EPWM1_SOCA, ADC_CH_ADCIN2, 8U);
	/*配置ADC_SOC中断触发：ADC_SOC_NUMBER5-SOC5
	 * ADC_INT_SOC_TRIGGER_NONE-不通过ADC中断触发*/
	ADC_setInterruptSOCTrigger(ADCA_BASE, ADC_SOC_NUMBER5,
			ADC_INT_SOC_TRIGGER_NONE);

	//
	// ADC Interrupt 1 Configuration
	// 		SOC/EOC number	: 5
	// 		Interrupt Source: enabled
	// 		Continuous Mode	: disabled
	//
	/*配置SOC中断触源：ADC_INT_NUMBER1-ADCINT1
	 * 通过ADC_SOC_NUMBER5-SOC5触发*/
	ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1,
			ADC_SOC_NUMBER5);
	/*使能ADC中断：ADC_INT_NUMBER1-ADCINT1*/
	ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
	/*清ADC中断状态：ADC_INT_NUMBER1-ADCINT1*/
	ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
	/*关闭ADC连续模式：ADC_INT_NUMBER1-ADCINT1*/
	ADC_disableContinuousMode(ADCA_BASE, ADC_INT_NUMBER1);
}

//
// Globals
//
uint16_t adcAResult0;
uint16_t adcAResult1;
uint16_t adcAResult2;

//取出结果
uint16_t adc_soc2result;
uint16_t adc_soc3result;
uint16_t adc_soc4result;
uint16_t adc_soc5result;

uint16_t i;

//
// adcA1ISR - ADC A Interrupt 1 ISR
//
__interrupt void adcA1ISR(void)
{
    //
    // Store results for A0 and A1
    //
    adcAResult0 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
    adcAResult1 = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER1);

    //
    // Average the 4 oversampled A2 results together
    //
    adcAResult2 =
        (ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2) +
        ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER3) +
        ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER4) +
        ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER5)) >> 2;

    //
    // Clear the interrupt flag
    //
    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);

    //
    // Check if overflow has occurred
    //
    if(true == ADC_getInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1))
    {
        ADC_clearInterruptOverflowStatus(ADCA_BASE, ADC_INT_NUMBER1);
        ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
    }

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}
