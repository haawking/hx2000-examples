#include "version.h"

//*****************************************************************************
//
// Version_getLibVersion
//
//*****************************************************************************
uint32_t
Version_getLibVersion(void)
{
    return(VERSION_NUMBER);
}
