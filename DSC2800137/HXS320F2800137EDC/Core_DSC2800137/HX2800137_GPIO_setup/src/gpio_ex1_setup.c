/******************************************************************
 文 档 名：       HX_DSC2800137_GPIO_setup
 开 发 环 境：    Haawking IDE IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      GPIO按键
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：GPIO1按键输入，控制GPIO20/LED1灯亮灭

 *
 版 本：      V1.0.1
 时 间：      2025年2月11日
 作 者：      liyuyao
 @ mail：     support@mail.haawking.com
 ******************************************************************/


#include "system.h"

//
// Main
//
void main(void)
{
	/*系统时钟初始化*/
    Device_init();
	/*关中断，清中断*/
    Interrupt_initModule();
	/*初始化中断向量表*/
    Interrupt_initVectorTable();

    EALLOW;
	/*配置GPIO1为IO输入*/
    GPIO_setPinConfig(GPIO_1_GPIO1);
    GPIO_setDirectionMode(1, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(1, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(1, GPIO_QUAL_SYNC);
	/*配置GPIO20为IO输出*/
    GPIO_setPinConfig(GPIO_20_GPIO20);
    GPIO_setDirectionMode(20, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(20, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(20, GPIO_QUAL_SYNC);
    EDIS;

    /*使能CPU中断*/
    Interrupt_enableMaster();

    for(;;)
    {
        if(GPIO_readPin(1)==0)
        {
        	/*GPIO20/LED1亮*/
        	GPIO_writePin(20,0);
        }
        else
        {
        	/*GPIO20/LED1灭*/
        	GPIO_writePin(20,1);
        }

    }
}


//
// End of File
//

