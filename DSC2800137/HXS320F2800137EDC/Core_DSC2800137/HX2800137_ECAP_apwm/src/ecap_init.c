/******************************************************************
 文 档 名：     ecap_init.c
 D S P：       DSC280025
 使 用 库：
 作     用：		产生一非对称8Hz的PWM波
 说     明：      提供ECAP的APWM配置与交叉开关输入配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2024年10月25日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "system.h"

void ECAP_apwm_config(){
    /*ECAP暂停时间戳计数*/
    ECAP_stopCounter(ECAP1_BASE);
    /*ECAP的APWM模式使能*/
    ECAP_enableAPWMMode(ECAP1_BASE);
    /*ECAP的APWM模式周期配置:160MHz/20M=8Hz*/
    ECAP_setAPWMPeriod(ECAP1_BASE,20000000U);
    /*ECAP的APWM模式比较值配置:50%*/
    ECAP_setAPWMCompare(ECAP1_BASE,10000000U);
    /*ECAP的APWM输出极性配置:ECAP_APWM_ACTIVE_LOW低,ECAP_APWM_ACTIVE_HIGH高*/
    ECAP_setAPWMPolarity(ECAP1_BASE,ECAP_APWM_ACTIVE_HIGH);
    /*ECAP的APWM相位偏移配置:0*/
    ECAP_setPhaseShiftCount(ECAP1_BASE,0U);
    /*ECAP的APWM的相位偏移装载关闭*/
    ECAP_disableLoadCounter(ECAP1_BASE);
    /*ECAP的同步配置:ECAP_SYNC_OUT_SYNCI计数=0同步输出,
     * ECAP_SYNC_OUT_COUNTER_PRD计数=周期值同步输出,ECAP_SYNC_OUT_DISABLED关闭同步输出*/
    ECAP_setSyncOutMode(ECAP1_BASE,ECAP_SYNC_OUT_SYNCI);
    /*ECAP的APWM的仿真模式停止*/
    ECAP_setEmulationMode(ECAP1_BASE,ECAP_EMULATION_STOP);
    /*ECAP的APWM的同步脉冲源配置：ECAP_SYNC_IN_PULSE_SRC_DISABLE关闭*/
    ECAP_setSyncInPulseSource(ECAP1_BASE,ECAP_SYNC_IN_PULSE_SRC_DISABLE);
    /*ECAP启动时间戳计数*/
    ECAP_startCounter(ECAP1_BASE);
}
void OUTPUTXBAR_config(){

    /*交叉开关输出OUTPUTXBAR3启动模式关闭*/
    XBAR_setOutputLatchMode(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);
    /*交叉开关输出OUTPUTXBAR3输出信号反转关闭*/
    XBAR_invertOutputSignal(OUTPUTXBAR_BASE, XBAR_OUTPUT3, false);

    /*交叉开关输出OUTPUTXBAR3的位置配置：XBAR_OUT_MUX00_ECAP1_OUT输出到ECAP*/
    XBAR_setOutputMuxConfig(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_OUT_MUX00_ECAP1_OUT);
    /*交叉开关输出OUTPUTXBAR3使能XBAR_MUX00使能*/
    XBAR_enableOutputMux(OUTPUTXBAR_BASE, XBAR_OUTPUT3, XBAR_MUX00);
}



