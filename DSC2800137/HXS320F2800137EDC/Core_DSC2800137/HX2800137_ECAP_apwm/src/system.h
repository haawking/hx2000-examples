/*
 * system.h
 *
 *  Created on: 2024��10��25��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "driverlib.h"
#include "device.h"

//
// Defines
//
//
// Defines
//
#define PWM_CLK   5UL                      // PWM frequency as 5Hz
#define PRD_VAL   (DEVICE_SYSCLK_FREQ / PWM_CLK)  // Calculate period value

#define DECREASE_FREQUENCY  0
#define INCREASE_FREQUENCY  1

//
// Globals
//
extern volatile uint32_t cap1Count;
void ECAP_apwm_config();
void OUTPUTXBAR_config();

void GPIO_config(void);

#endif /* SYSTEM_H_ */
