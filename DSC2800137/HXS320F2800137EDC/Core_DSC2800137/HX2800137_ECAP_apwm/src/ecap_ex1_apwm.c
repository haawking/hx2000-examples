/******************************************************************
 文 档 名：       HX_DSC2800137_ECAP_APWM
 开 发 环 境：    Haawking IDE V2.3.8Pre
 开 发 板：
 D S P：          DSC2800137
 使 用 库：
 作     用：      ECAP_APWM产生非对称PWM方波
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频160MHz，采用ECAP_APWM模式产生一个频率为8-16Hz的PWM波


 版 本：      V1.0.1
 时 间：      2025年2月13日
 作 者：
 @ mail：     support@mail.haawking.com
 ******************************************************************/

// Included Files
//
#include "driverlib.h"
#include "device.h"

#include "system.h"

//
// Globals
//
uint16_t direction;
volatile uint32_t cap1Count;

//
// Main
//
int main(void)
{
    /*系统时钟初始化*/
    Device_init();

    /*GPIO端口配置锁定关闭*/
    Device_initGPIO();

    /*GPIO配置，用于显示输出状态*/
    GPIO_config();

    /*初始化PIE与清PIE寄存器，关CPU中断*/
    Interrupt_initModule();

    /*初始化中断向量表*/
    Interrupt_initVectorTable();

    /*ECAP的GPIO引脚与APWM模式配置*/
    EALLOW;
    /*GPIO5的交叉开关输出到ECAP功能输入复用配置*/
    GPIO_setPinConfig(GPIO_5_OUTPUTXBAR3);
    /*ECAP的APWM功能配置*/
    ECAP_apwm_config();
    /*交叉开关输出功能配置*/
    OUTPUTXBAR_config();

    EDIS;

    for(;;)
    {
        //
        // Set the duty cycle to 50%
        //
        ECAP_setAPWMShadowCompare(ECAP1_BASE,
                      (ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_1) >> 1U));

        cap1Count = ECAP_getEventTimeStamp(ECAP1_BASE, ECAP_EVENT_1);

        //
        // Vary frequency
        //
        if(cap1Count >= PRD_VAL)
        {
           direction = INCREASE_FREQUENCY;
        }
       else if (cap1Count <= PRD_VAL/2)
        {
           direction = DECREASE_FREQUENCY;
        }

        if(direction == INCREASE_FREQUENCY)
        {
           ECAP_setAPWMShadowPeriod(ECAP1_BASE, (cap1Count - 500000U));
        }
        else
        {
           ECAP_setAPWMShadowPeriod(ECAP1_BASE, (cap1Count + 500000U));
        }
    }

    return 0;
}


void GPIO_config(void)
{
    EALLOW;
    /*GPIO20的IO功能配置*/
    GPIO_setPinConfig(GPIO_20_GPIO20);
    /*GPIO20的IO方向配置：GPIO_DIR_MODE_OUT输出,GPIO_DIR_MODE_IN输入*/
    GPIO_setDirectionMode(20, GPIO_DIR_MODE_OUT);
    /*GPIO20写入1,初始化LED1灭*/
    GPIO_writePin(20, 1);


    GPIO_setPinConfig(GPIO_22_GPIO22);
   GPIO_setDirectionMode(22, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(22, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(22, GPIO_QUAL_SYNC);

    /*GPIO22写入1,初始化LED2灭*/
    GPIO_writePin(22, 1);
    EDIS;
}
