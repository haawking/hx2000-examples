/******************************************************************
 文 档 名 ：   HX_DSC28027_SCI
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：   Core_DSC28027_V1.4
                     Start_DSC28027_V1.1
                     Start_DSC28027PTT_Rev1.2
 D S P：        DSC28027
 使 用 库：   无
 作     用：	SCI自动波特率数据收发
 ----------------------例程使用说明-----------------------------
 *
 *       使用USB数据线连接PC和28027核心板USB1
 *       通过串口调试助手，打开已连接串口，
 *       先发送0x41(A)或0x61(a),可以自动检测波特率
 *       然后可连续发送多组数据
 *
 现   象：发送接收数据
  版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

void Scia_Config(void);
void Scia_Gpio(void);
void Scia_AutoBaudLock(void);

int main(void)
{
	    /*初始化系统控制：PLL,WatchDog，使能外设时钟*/
		InitSysCtrl();
		/*初始化pie中断控制*/
		InitPieCtrl();
		/*禁止CPU中断并清除所有中断标志*/
		IER = 0x0000;
		IFR = 0x0000;
		/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
		InitPieVectTable();

		/*配置Scia_Gpio*/
		Scia_Gpio();

		/*SCIA初始化*/
		Scia_Config();

		Scia_AutoBaudLock();

		while(1)
		{
			while(SciaRegs.SCIRXST.bit.RXRDY != 1);
			SciaRegs.SCITXBUF = SciaRegs.SCIRXBUF.bit.RXDT;
		}


	return 0;
}

// ----------------------------------------------------------------------------
