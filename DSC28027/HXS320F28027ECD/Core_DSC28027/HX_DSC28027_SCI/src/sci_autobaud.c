#include "dsc_config.h"

void Scia_AutoBaudLock(void)
{
    Uint16 byteData;

    SciaRegs.SCILBAUD=1;
    /*使能波特率自动对齐*/
    SciaRegs.SCIFFCT.bit.CDC=1;
   /* 清除第15位的ABD标志*/
    SciaRegs.SCIFFCT.bit.ABDCLR=1;

    while(SciaRegs.SCIFFCT.bit.ABD!=1) { }
    /*清除第15位的ABD标志*/
    SciaRegs.SCIFFCT.bit.ABDCLR=1;
   /* 禁用波特自动对齐*/
    SciaRegs.SCIFFCT.bit.CDC=0;

    while(SciaRegs.SCIRXST.bit.RXRDY!=1){ }
    byteData=SciaRegs.SCIRXBUF.bit.RXDT;
    /*传输数据缓存*/
    SciaRegs.SCITXBUF=byteData;

    return;
}
