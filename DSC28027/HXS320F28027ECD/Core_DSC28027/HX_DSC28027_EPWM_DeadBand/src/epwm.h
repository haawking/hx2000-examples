#ifndef EPWM_H_
#define EPWM_H_

#include "dsc_config.h"

typedef struct
{
		volatile struct EPWM_REGS *EPwmRegHandle;

		uint16_t EPWM_DB_Direction;
		uint16_t EPWMMinDB;
		uint16_t EPWMMaxDB;

} EPWM_INFO;

extern EPWM_INFO epwm1_info;
extern EPWM_INFO epwm2_info;
extern EPWM_INFO epwm3_info;

void InitEpwm1_Example(void);
void InitEpwm2_Example(void);
void InitEpwm3_Example(void);

void epwm1_pc(void);
void epwm2_pc(void);

void update_compare(EPWM_INFO *epwm_info);
void update_db(EPWM_INFO *epwm_info);

void INTERRUPT epwm1_isr(void);
void INTERRUPT epwm2_isr(void);
void INTERRUPT epwm3_isr(void);

void INTERRUPT epwm1_tz_isr(void);
void INTERRUPT epwm2_tz_isr(void);
void INTERRUPT epwm3_tz_isr(void);

#endif/*EPWM_H_*/
