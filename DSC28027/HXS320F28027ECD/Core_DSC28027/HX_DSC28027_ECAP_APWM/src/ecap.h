#ifndef ECAP_H_
#define ECAP_H_

#include "dsc_config.h"

void ECap_Gpio(void);
void ECap_Init(void);
void INTERRUPT ecap_isr(void);

#endif/*ECAP_H_*/
