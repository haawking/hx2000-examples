//###########################################################################
//
// FILE:   F2802x_Device.h
//
// TITLE:  F2802x Device Definitions.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.0.0 $
// $Release Date: 2022-07-09 04:26:57 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#ifndef F2802x_DEVICE_H
#define F2802x_DEVICE_H

#include <stdio.h>



#define	_CPU_M0_BASE_ADDR	((uint32_t)0x00000000)
#define	_CPU_M1_BASE_ADDR	((uint32_t)0x00000800)
#define	_ADC_BASE_ADDR	    ((uint32_t)0x00001400) 
#define	_TIMER_PIE_BASE_ADDR	((uint32_t)0x00001800)
#define	_DMA_BASE_ADDR		((uint32_t)0x00001C00)
#define	_DEBUG_BASE_ADDR	((uint32_t)0x00002000)

#define	_APB_1_BASE_ADDR		((uint32_t)0x0000E000)
#define	_L0_BASE_ADDR		((uint32_t)0x00010000)


#define	_BOOT_ROM_BASE_ADDR	((uint32_t)0x007FC000)
#define	_CMP_BASE_ADDR		((uint32_t)0x00C000)

#define	_L0_DUAL_MAPPED_BASE_ADDR	((uint32_t)0x00010000)

/***********************************************************************
APB-0  PERIPHERALS
*************************************************************************/
#define	_COMPARATOR_BASE_ADDR	((uint32_t)0x0000C000)
#define	_ECAP_BASE_ADDR		((uint32_t)0x0000D000)
#define	_EQEP_BASE_ADDR		((uint32_t)0x0000D400)
#define	_GPIO_BASE_ADDR		((uint32_t)0x0000D800)
#define	_SYS_CTRL_BASE_ADDR	((uint32_t) 0xDC00)



/***********************************************************************
APB-1  PERIPHERALS
*************************************************************************/
#define	_SCI_BASE_ADDR		((uint32_t)0x0000E000)
#define	_I2C_BASE_ADDR		((uint32_t)0x0000E400)
#define	_SPI_BASE_ADDR		((uint32_t)0x0000E800)


/**************************************************************************
TIMER AND PIE PERIPHERALS
**************************************************************************/

#define	_TIMER_0_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0)  
#define	_TIMER_1_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x10)
#define	_TIMER_2_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x20)


/*************************************************************************
	PIE PERIPHERALS
*************************************************************************/
#define	_PIE_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x100)
#define _VECTOR_PIE_BASE_ADDR  (_TIMER_PIE_BASE_ADDR + 0x200)
#define _PIE_EMU_REG_ADDR  ((uint32_t)0x1BC0)


#define _EPWM_BASE_ADDR	  ((uint32_t)0xB000)

/*************************************************************************
	EPWM PERIPHERALS
*************************************************************************/

#define	_EPWM_PER1_BASE_ADDR  _EPWM_BASE_ADDR
#define	_EPWM_PER2_BASE_ADDR  (_EPWM_PER1_BASE_ADDR + 0x400)
#define	_EPWM_PER3_BASE_ADDR  (_EPWM_PER2_BASE_ADDR + 0x400)
#define	_EPWM_PER4_BASE_ADDR  (_EPWM_PER3_BASE_ADDR + 0x400)

/*************************************************************************
	CMP PERIPHERALS
*************************************************************************/




#define _DMA_PER1_ADDR (_DMA_BASE_ADDR)
#define _DMA_PER2_ADDR (_DMA_BASE_ADDR + 0x50)
#define _DMA_PER3_ADDR (_DMA_BASE_ADDR + 0x50)
#define _DMA_PER4_ADDR (_DMA_BASE_ADDR + 0x50)

//0x7DFFF0 -0x7DFFFF
#define  _CSM_PASSWORD_BASE_ADDR	((uint32_t)0x7DFFF0)
#define	_CMP_PER1_ADDR	_CMP_BASE_ADDR
#define	_CMP_PER2_ADDR	(_CMP_PER1_ADDR + 0x80)





#define _DEV_EMU_BASE_ADDR	(_SYS_CTRL_BASE_ADDR  +  0x80)

#define _FLASH_PER_REG_ADDR		((uint32_t)0x7AF800)



/*************************************************************************
	GPIO PERIPHERALS
*************************************************************************/

#define _GPIO_DATA_ADDR		(_GPIO_BASE_ADDR + 0x38)

#define _GPIO_INT_ADDR		(_GPIO_BASE_ADDR + 0x68)


#define CODE_SECTION(v) __attribute__((section(v)))




#include "hx_rv32_dev.h"

#include "hx_rv32_type.h"





#define   TARGET   1
//---------------------------------------------------------------------------
// User To Select Target Device:


#define   DSP28_28027PT   1
#define   DSP28_28027DA   0


//---------------------------------------------------------------------------
// Common CPU Definitions:
//

extern  volatile unsigned int IFR;
extern  volatile unsigned int IER;


#define  EINT	asm("csrsi mstatus, 0x8") //enable_interrupt(1)
#define  DINT   asm("csrci mstatus, 0x8") //disable_interrupt(1)


//#define  ERTM   asm(" clrc DBGM")//enable  debug   not support real-time debug
//#define  DRTM   asm(" csrs DBGM")//disable debug   not support real-time debug


#define  ERTM 

#define  DRTM 

extern Uint32 ier_set(Uint32 group);
extern Uint32 ier_unset(Uint32 group);

extern Uint32 ifr_set(Uint32 group);
extern Uint32 ifr_unset(Uint32 group);


extern uint32_t read_reg32(uintptr_t addr);
extern uint32_t read_reg16(uintptr_t addr);
extern uint8_t read_reg8(uintptr_t addr);

extern void write_reg32(uintptr_t addr,uint32_t val);
extern void write_reg16(uintptr_t addr,uint16_t val);
extern void write_reg8(uintptr_t addr,uint8_t val);

/*************************************************************
you can use it  like this

IER_ENABLE(M_INT1|M_INT3);
IER_DISABLE(M_INT1|M_INT3);

***********************************************************************/

#define IER_ENABLE(v)   ier_set(v)
#define IER_DISABLE(v)  ier_unset(v)

#define IFR_ENABLE(v)   ifr_set(v)
#define IFR_DISABLE(v)  ifr_unset(v)









#define M_INT1  0x0001
#define M_INT2  0x0002
#define M_INT3  0x0004
#define M_INT4  0x0008
#define M_INT5  0x0010
#define M_INT6  0x0020
#define M_INT7  0x0040
#define M_INT8  0x0080
#define M_INT9  0x0100
#define M_INT10 0x0200
#define M_INT11 0x0400
#define M_INT12 0x0800
#define M_INT13 0x1000
#define M_INT14 0x2000
#define M_DLOG  0x4000
#define M_RTOS  0x8000

#define BIT0    0x0001
#define BIT1    0x0002
#define BIT2    0x0004
#define BIT3    0x0008
#define BIT4    0x0010
#define BIT5    0x0020
#define BIT6    0x0040
#define BIT7    0x0080
#define BIT8    0x0100
#define BIT9    0x0200
#define BIT10   0x0400
#define BIT11   0x0800
#define BIT12   0x1000
#define BIT13   0x2000
#define BIT14   0x4000
#define BIT15   0x8000


//---------------------------------------------------------------------------
// Include All Peripheral Header Files:
//

#include "F2802x_Adc.h"                // ADC Registers
#include "F2802x_BootVars.h"           // Boot ROM Variables
#include "F2802x_Comp.h"               // Comparator Registers
#include "F2802x_CpuTimers.h"          // 32-bit CPU Timers
#include "F2802x_ECap.h"               // Enhanced Capture
#include "F2802x_EPwm.h"               // Enhanced PWM
#include "F2802x_Gpio.h"               // General Purpose I/O Registers
#include "F2802x_I2c.h"                // I2C Registers
#include "F2802x_PieCtrl.h"            // PIE Control Registers
#include "F2802x_PieVect.h"            // PIE Vector Table
#include "F2802x_Spi.h"                // SPI Registers
#include "F2802x_Sci.h"                // SCI Registers
#include "F2802x_Eqep.h"               // SCI Registers
#include "F2802x_SysCtrl.h"            // System Control/Power Modes
#include "F2802x_Dma.h"                // External Interrupts
#include "F2802x_DevEmu.h"             // DevEmu Registers




#if (DSP28_28020PT||DSP28_28021PT||DSP28_28022PT||DSP28_28023PT||DSP28_28026PT||DSP28_28027PT)
#define DSP28_COMP1    1
#define DSP28_COMP2    1
#define DSP28_EPWM1    1
#define DSP28_EPWM2    1
#define DSP28_EPWM3    1
#define DSP28_EPWM4    1
#define DSP28_EQEP1    1
#define DSP28_ECAP1    1
#define DSP28_SPIA     1
#define DSP28_SCIA     1
#define DSP28_I2CA     1
#endif  // end 2802x PT package

#if (DSP28_28020DA||DSP28_28021DA||DSP28_28022DA||DSP28_28023DA||DSP28_28026DA||DSP28_28027DA)
#define DSP28_COMP1    1
#define DSP28_COMP2    0
#define DSP28_EPWM1    1
#define DSP28_EPWM2    1
#define DSP28_EPWM3    1
#define DSP28_EPWM4    1
#define DSP28_ECAP1    1
#define DSP28_SPIA     1
#define DSP28_SCIA     1
#define DSP28_I2CA     1
#endif  // end 2802x DA package





#endif

