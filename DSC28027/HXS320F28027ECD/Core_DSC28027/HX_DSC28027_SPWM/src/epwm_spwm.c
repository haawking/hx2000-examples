/******************************************************************
 文 档 名：     epwm_spwm.c
 D S P：       DSC28027
 使 用 库：     
 作     用：      EPWM1/2/3的输出占空比变化配置
 说     明：      配置pwm波占空比按正弦规律变化，转角假设Ud=0,Uq=15
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年1月19日
 作 者：何洋
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "epwm.h"
#include "IQmathLib.h"

#define pi 3.1415926

_iq p;
_iq sa,sb,sc;  //实际转角（含偏置）
_iq pha;
_iq the,theta;
_iq Qa,Qb,Qc;  //脉宽

long int j=1;

/*function1:实际转角模拟输出
 *function1: 假定转速1500rpm（即25rps），积分得到转角输出
 *****************************************************************/
void thei(void)
{
	j++;
	the =_IQ9div(j,200);
	if(j==200)
	{
		j=0;
	}
	p = _IQ9(pi);
	theta = _IQ9mpy(the, 2*p);  //弧度值
}

/*function1:epwm1_compare占空比按正弦规律变化配置
 *function1: 配置正弦按转角比例分割
 *****************************************************************/
void epwm1_compare(void)
{
	/*转角的弧度值模拟*/
	thei();
	/*实际偏置转角求解*/
	sa = theta + p;
	/*转角的正弦规律求解*/
	Qa = _IQ9sin(sa);

	/*相位偏置*/
	pha = _IQ9div(p, 3);
	/*实际偏置转角求解*/
	sb = theta + pha;
	/*转角的正弦规律求解*/
	Qb = _IQ9sin(sb);

	/*相位偏置*/
	pha = _IQ9div(p, 3);
	/*实际偏置转角求解*/
	sc = theta - pha;
	/*转角的正弦规律求解*/
	Qc =_IQ9sin(sc);
	/*配置占空比按转角的正弦规律变化输出*/
	EPwm1Regs.CMPA.half.CMPA = 1500+ 1500/512* Qa;

	/*配置占空比按转角的正弦规律变化输出*/
	EPwm2Regs.CMPA.half.CMPA = 1500 + 1500/512* Qb;

	/*配置占空比按转角的正弦规律变化输出*/
	EPwm3Regs.CMPA.half.CMPA = 1500 + 1500/512 * Qc;
}

