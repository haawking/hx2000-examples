#include "adc.h"


/******************************************************************
 *函数名：void ADC_Config()
 *参 数：无
 *返回值：无
 *作 用：配置ADC
 ******************************************************************/
void ADC_Config(void)
{
	EALLOW;
	AdcRegs.ADCCTL1.bit.ADCREFSEL = 0;
	/*NT脉冲产生在ADC结果锁定到其结果寄存器之前发生一个周期*/
	AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 3;		// ADC clock = SYSCLK/8
	/*中断选择3寄存器*/
	AdcRegs.INTSEL3N4.bit.INT3E = 1;
	/*每当产生EOC脉冲时，都会产生附加脉冲，而不管是否清除了标记位*/
	AdcRegs.INTSEL3N4.bit.INT3CONT = 0;
	/*EOC12是ADCINT3的触发器*/
	AdcRegs.INTSEL3N4.bit.INT3SEL = 0;

	/*将SOC0设置为样本A0*/
	AdcRegs.ADCSOC0CTL.bit.CHSEL = 0;
	/*将SOC1设置为样本A1*/
	AdcRegs.ADCSOC1CTL.bit.CHSEL = 1;
	/*将SOC2设置为样本A2*/
	AdcRegs.ADCSOC2CTL.bit.CHSEL = 2;
	/*将SOC3设置为样本A3*/
	AdcRegs.ADCSOC3CTL.bit.CHSEL = 3;
	/*将SOC4设置为样本A4*/
	AdcRegs.ADCSOC4CTL.bit.CHSEL = 4;
	/*将SOC5设置为样本A6*/
	AdcRegs.ADCSOC5CTL.bit.CHSEL = 5;
	/*将SOC6设置为样本A7*/
	AdcRegs.ADCSOC6CTL.bit.CHSEL = 6;
	/*将SOC7设置为样本B1*/
	AdcRegs.ADCSOC7CTL.bit.CHSEL =7;
	/*将SOC8设置为样本B2*/
	AdcRegs.ADCSOC8CTL.bit.CHSEL = 8;
	/*将SOC9设置为样本B3*/
	AdcRegs.ADCSOC9CTL.bit.CHSEL = 9;
	/*将SOC10设置为样本B4*/
	AdcRegs.ADCSOC10CTL.bit.CHSEL = 10;
	/*将SOC11设置为样本B6*/
	AdcRegs.ADCSOC11CTL.bit.CHSEL = 11;
	/*将SOC12设置为样本B7*/
	AdcRegs.ADCSOC12CTL.bit.CHSEL = 12;

	AdcRegs.ADCSOC13CTL.bit.CHSEL = 13;
	AdcRegs.ADCSOC14CTL.bit.CHSEL = 14;
	AdcRegs.ADCSOC15CTL.bit.CHSEL = 15;

	/*SOCx触发源选择:ADCTRIG5-ePWM1，ADCSOCA*/
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC1CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC3CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC4CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC5CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC6CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC7CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC8CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC9CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC10CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC11CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC12CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC13CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC14CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC15CTL.bit.TRIGSEL = 0x05;

	/*采样窗口长达6个周期（2n+6个时钟周期）*/
	AdcRegs.ADCSOC0CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC1CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC2CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC3CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC4CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC5CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC6CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC7CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC8CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC9CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC10CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC11CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC12CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC13CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC14CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC15CTL.bit.ACQPS = 0;

	EDIS;
}
