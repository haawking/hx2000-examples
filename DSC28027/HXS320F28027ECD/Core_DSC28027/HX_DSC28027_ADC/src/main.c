/******************************************************************
 文 档 名 ：HX_DSC28027_ADC
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
                  Start_DSC28027_V1.1
                  Start_DSC28027PTT_Rev1.2
 D S P：     DSC28027
 使 用 库：无
 作 用：单触发源，多通道ADC顺序采样
 说 明：①在Haawking IDE调试界面上输入adcValA和adcValB数组变量；
 ②将Start_DSC28027_V1.1或Start_DSC28027PTT_Rev1.2
 开发板的电位器负端RG与ADC通道采集输入引脚
 ADCIN
 A1/A2/A3/A4/A6/A7/
 B1/B3/B4/B6/B7连接，
 Core_DSC28027_V1.4板可以将被测电压接入对应引脚，最好接一个小容量100nF电容稳压。
 从liveview窗口查看adcValA和adcValB可正确读出电位器电阻两端
 电压对应的数字量值X
 转换结果为Vi=X/4096*3.3V。
 ----------------------例程使用说明-----------------------------
 *
 *            测试ADC功能
 *
 *
 * 现象：可正确读出电位器电阻两端电压对应的数字量值X
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com

 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"
#include "adc.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化Flash*/
	InitFlash();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	/*初始化模拟IO*/
	InitAdcAio();
	/*ADC初始化*/
	InitAdc();
	/*关中断*/
	DINT;
	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();

	EALLOW;
	/*清除中断*/
	PieCtrlRegs.PIEACK.all = 0xFFFF;
	/*将adc_isr入口地址赋给ADCINT3*/
	PieVectTable.ADCINT3 = &adc_isr;
	/*开启对应中断*/
	PieCtrlRegs.PIEIER10.bit.INTx3 = 1;
	EDIS;
	/*使能CPU中断*/
	IER |= M_INT10;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*配置epwm1*/
	epwm1_config();

	EALLOW;
	/*所有使能的ePWM模块时钟都是在TBCLK的第一个上升沿对齐的情况下开始的*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*配置ADC*/
	ADC_Config();

	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
