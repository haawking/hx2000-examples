//###########################################################################
//
// FILE:    _f2802x_usdelay.S
//
// TITLE:   F2802x delay Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.0.0 $
// $Release Date: 2022-07-09 04:26:25 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


.section  ramfuncs, "ax", @progbits

.global	 _f2802x_usdelay


_f2802x_usdelay:

.align 1
	addi  sp,sp,-20
	sw    a1,16(sp) 
	csrr  a1,0x7c0
	sw    a1,12(sp)
	csrr  a1,mstatus
	
.align 2
	rpt a0,4
	nop
   
	csrw  mstatus,a1
	lw    a1,12(sp)
	csrw  0x7c0,a1
	lw    a1,16(sp)
	addi  sp,sp,20

	ret
	
.size  _f2802x_usdelay,   .-_f2802x_usdelay

