//###########################################################################
//
// FILE:   f2802x_globalprototypes.h
//
// TITLE:  Global prototypes for F2802x Examples
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.0.0 $
// $Release Date: 2022-07-09 04:26:25 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef F2802x_GLOBALPROTOTYPES_H
#define F2802x_GLOBALPROTOTYPES_H

#ifdef __cplusplus
extern "C" {
#endif

/*---- shared global function prototypes -----------------------------------*/
//
// shared global function prototypes
//
extern void InitAdc(void);
extern void DMAInitialize(void);

//
// DMA Channel 1
//
extern void DMACH1AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH1BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH1TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH1WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH1ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH1(void);

//
// DMA Channel 2
//
extern void DMACH2AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH2BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH2TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH2WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH2ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH2(void);

//
// DMA Channel 3
//
extern void DMACH3AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH3BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH3TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH3WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH3ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH3(void);

//
// DMA Channel 4
//
extern void DMACH4AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH4BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH4TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH4WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH4ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH4(void);

//
// DMA Channel 5
//
extern void DMACH5AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH5BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH5TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH5WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH5ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH5(void);

//
// DMA Channel 6
//
extern void DMACH6AddrConfig(volatile Uint32 *DMA_Dest,
                             volatile Uint32 *DMA_Source);
extern void DMACH6BurstConfig(Uint32 bsize, int32 srcbstep, int32 desbstep);
extern void DMACH6TransferConfig(Uint32 tsize, int32 srctstep, int32 deststep);
extern void DMACH6WrapConfig(Uint32 srcwsize, int32 srcwstep, Uint32 deswsize,
                             int32 deswstep);
extern void DMACH6ModeConfig(Uint32 persel, Uint32 perinte, Uint32 oneshot,
                             Uint32 cont, Uint32 synce, Uint32 syncsel,
                             Uint32 ovrinte, Uint32 datasize, Uint32 chintmode,
                             Uint32 chinte);
extern void StartDMACH6(void);
extern void InitAdc(void);
extern void InitAdcAio(void);
extern void AdcOffsetSelfCal(void);
extern void AdcChanSelect(Uint16 ch_no);
extern Uint16 AdcConversion (void);
extern void InitPeripherals(void);
extern void InitECap(void);
extern void InitECapGpio(void);
extern void InitECap1Gpio(void);
extern void InitEPwm(void);
extern void InitEPwmGpio(void);
extern void InitEPwm1Gpio(void);
extern void InitEPwm2Gpio(void);
extern void InitEPwm3Gpio(void);
#if DSP28_EPWM4
extern void InitEPwm4Gpio(void);
#endif // endif 
#if DSP28_EQEP1
extern void InitEQep(void);
extern void InitEQepGpio(void);
extern void InitEQep1Gpio(void);
#endif // endif DSP28_EQEP1
extern void InitCompGpio(void);
extern void InitComp1Gpio(void);
#if DSP28_COMP2
extern void InitComp2Gpio(void);
#endif // endif 
extern void InitGpio(void);
extern void InitI2CGpio(void);
extern void InitPieCtrl(void);
extern void InitPieVectTable(void);
extern void InitSci(void);
extern void InitSciGpio(void);
extern void InitSciaGpio(void);
extern void InitSpi(void);
extern void InitSpiGpio(void);
extern void InitSpiaGpio(void);
extern void InitSysCtrl(void);
extern void InitTzGpio(void);
extern void InitXIntrupt(void);
extern void InitPll(Uint16 PLLCR_F_val,Uint16 PLLCR_N_val,Uint16 PLLCR_K_val, Uint16 divsel);
extern void InitPeripheralClocks(void);
extern void EnableInterrupts(void);
extern void DSC28x_usDelay(Uint32 Count);
#define KickDog ServiceDog     // For compatiblity with previous versions
extern void ServiceDog(void);
extern void DisableDog(void);
extern void EnableDog(void);
extern Uint16 CsmUnlock(void);
extern void IntOsc1Sel (void);
extern void IntOsc2Sel (void);
extern void XtalOscSel(void);
extern void ExtClkSel (void);
extern int16 GetTemperatureC(int16 sensorSample); // returns temp in deg. C
extern int16 GetTemperatureK(int16 sensorSample); // returns temp in deg. K
extern void Osc1Comp(int16 sensorSample);
extern void Osc2Comp(int16 sensorSample);



//                 CAUTION
// This function MUST be executed out of RAM. Executing it
// out of OTP/Flash will yield unpredictable results
extern void InitFlash(void);

void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);



#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  

//===========================================================================
// End of file.
//===========================================================================
