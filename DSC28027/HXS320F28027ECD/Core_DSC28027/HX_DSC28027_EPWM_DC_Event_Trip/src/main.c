﻿/******************************************************************
 文 档 名 ：HX_DSC28027_EPWM_DC_Event_Trip
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
 D S P： DSC28027
 使 用 库：无

 ----------------------例程使用说明-----------------------------
 *
 *            测试epwm功能
 *   说 明： 初始时GPIO16/GPIO17均接3.3V
 *   采用DC时间比较模块与错误联防模块编程实现DC比较事件触发
 *   注意：先接GPIO16接地 再接GPIO17接地,CBC与DC事件按顺序依次触发
 *   ①　ePWM1 DCAEVT1事件单次触发A路置高，B路置低；GPO17接地 GPIO16接3.3V
 *   ②　ePWM2 DCAEVT2事件周期循环触发A路置高，B路置低；GPO16接地 GPIO17接3.3V
 *   ③　初始时GPIO12/GPIO17均接3.3V
 *   ePWM3 DCAEVT2事件中断触发A路置高，DCBEVT1事件中断触发B路置低。
 *   GPO12接地 GPIO17接3.3V  LED1亮
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();
	/*初始化TZ错误联防GPIO配置*/
	InitTzGpio();
	/*初始化LED*/
	InitLED();
	/*关中断*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER=0x0000;
	IFR=0x0000;

	/*初始化PIE向量表*/
	InitPieVectTable();

	EALLOW;
	/*将epwm1_tz_isr入口地址赋给EPWM1_TZINT,执行OST单次触发动作*/
	PieVectTable.EPWM1_TZINT = &epwm1_tz_isr;
	/*将epwm2_tz_isr入口地址赋给EPWM2_TZINT，执行CBC周期触发动作*/
	PieVectTable.EPWM2_TZINT = &epwm2_tz_isr;
	/*将epwm2_tz_isr入口地址赋给EPWM3_TZINT，执行DC数字比较事件触发动作*/
	PieVectTable.EPWM3_TZINT = &epwm3_tz_isr;
	EDIS;
	//禁止访问受保护的空间

	/*TBCLK被禁止，此时，ePWM模块的时钟使能通过PCLKCR1寄存器进行配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*初始化EPWM1，错误联防配置为OST*/
	InitEpwm1_Example();
	/*初始化EPWM2，错误联防配置为CBC*/
	InitEpwm2_Example();
	/*初始化EPWM3，错误联防配置为DC数字比较*/
	InitEpwm3_Example();

	/*所有使能的ePWM模块同步使用TBCLK*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*使能打开对应CPU IER第2组中断向量*/
	IER|=M_INT2;

	/*使能相对应的中断*/
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx3 = 1;

	EINT;

	while (1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
