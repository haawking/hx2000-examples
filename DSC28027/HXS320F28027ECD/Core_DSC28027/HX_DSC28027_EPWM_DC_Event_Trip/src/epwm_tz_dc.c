#include "epwm.h"

Uint16 EPwm_TZ_CBC_flag;
Uint16 EPwm_TZ_DC_flag;
/******************************************************************
 *函数名：void INTERRUPT epwm1_tz_isr()
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm1_tz_isr(void)
{
	EALLOW;
	/*清除中断标志位*/
	EPwm1Regs.TZCLR.bit.INT = 1;
	EDIS;
	/*OST事件触发*/
	if (EPwm1Regs.TZFLG.bit.OST == 1)
	{
		/*GPIO32置0*/
		GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;    /*  LED1亮 */
	}
	else
	{
		/*GPIO32置1*/
		GpioDataRegs.GPBSET.bit.GPIO32= 1;     /*  LED1灭  */
	}

	/*PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
/******************************************************************
 *函数名：void INTERRUPT epwm2_tz_isr()
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm2_tz_isr(void)
{
	EALLOW;
	/*清除周期性触发事件标志位*/
	EPwm2Regs.TZCLR.bit.CBC = 1;
	/*清除中断标志位*/
	EPwm2Regs.TZCLR.bit.INT = 1;
	EDIS;
	/*CBC事件触发*/
	if (EPwm2Regs.TZFLG.bit.CBC == 1)
	{
//		/*GPIO7置0*/
//		GpioDataRegs.GPACLEAR.bit.GPIO7 = 1;    /*  D401  */
	}
	else
	{
//		/*GPIO7置0*/
//		GpioDataRegs.GPASET.bit.GPIO7 = 1;    /*  D401  */
	}
	/*PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
/******************************************************************
 *函数名：void INTERRUPT epwm3_tz_isr()
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm3_tz_isr(void)
{
	EALLOW;
	/*清除数字比较输出 A 事件 2 的标志*/
	EPwm3Regs.TZCLR.bit.DCAEVT2 = 1;
	/*清除数字比较输出 B 事件 1 的标志*/
	EPwm3Regs.TZCLR.bit.DCBEVT1 = 1;
	/*清除中断标志位*/
	EPwm3Regs.TZCLR.bit.INT = 1;

	EDIS;

	if (EPwm3Regs.TZFLG.bit.DCAEVT2 == 1)
	{
//		/*GPIO6置0*/
//		GpioDataRegs.GPACLEAR.bit.GPIO6 = 1;    /*  D401  */
//		/*GPIO6置0*/
//		GpioDataRegs.GPACLEAR.bit.GPIO7 = 1;    /*  D401  */
	}
	else
	{
//		/*GPIO6置0*/
//		GpioDataRegs.GPASET.bit.GPIO6 = 1;    /*  D401  */
//		/*GPIO6置0*/
//		GpioDataRegs.GPASET.bit.GPIO7 = 1;    /*  D401  */
	}
	/*PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
