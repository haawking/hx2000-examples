/*
 * flash.h
 *
 *  Created on: 2022��8��30��
 *      Author: HX
 */

#ifndef SRC_FLASH_H_
#define SRC_FLASH_H_


#include "dsc_config.h"

typedef struct{
	Uint32 *StartAddr;
	Uint32 *EndAddr;
	}SECTOR;

Uint16 Flash28027_ECD_Program(Uint32 *FlashAddr, Uint32*BufAddr, Uint32 Length, FLASH_ST *FProgStatus);
Uint16 Flash28027_ECD_OtpProgram(Uint32 *OtpAddr, Uint32*BufAddr, Uint32 Length, FLASH_ST *FProgStatus);
Uint16 Flash28027_ECD_Verify(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28027_ECD_ProgVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28027_ECD_Erase(Uint8 SectorIdx, FLASH_ST *FEraseStat);
Uint16 Flash28027_ECD_MassErase(FLASH_ST *FEraseStat);
Uint16 Flash28027_ECD_EraseVerify(Uint32 *FlashAddr, Uint16 Length, FLASH_ST *FVerifyStat);
Uint16 Flash28027_ECD_APIVersionHex();



extern SECTOR Sector[128];


#endif /* SRC_FLASH_H_ */
