#include "system.h"
/******************************************************************
 *函数名：void ADC_Init()
 *参 数：无
 *返回值：无
 *作 用：ADC初始化
 ******************************************************************/
void ADC_Init(void)
{
	EALLOW;
	AdcRegs.ADCCTL1.bit.ADCREFSEL = 0;
	/*NT脉冲产生在ADC结果锁定到其结果寄存器之前发生一个周期*/
	AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	/*中断选择3寄存器*/
	AdcRegs.INTSEL3N4.bit.INT3E = 1;
	/*每当产生EOC脉冲时，都会产生附加脉冲，而不管是否清除了标记位*/
	AdcRegs.INTSEL3N4.bit.INT3CONT = 0;

	/*EOC0是ADCINT3的触发器*/
	AdcRegs.INTSEL3N4.bit.INT3SEL = 0;

	/*将SOC0设置为样本A2*/
	AdcRegs.ADCSOC0CTL.bit.CHSEL = 2;
	/*SOC0触发源选择:ADCTRIG7-ePWM2，ADCSOCA*/
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 7;
	/*采样窗口长达7个周期（6+1个时钟周期）*/
	AdcRegs.ADCSOC0CTL.bit.ACQPS = 0;

	EDIS;
}
