/*
 * system.h
 *
 *  Created on: 2022��7��19��
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"

//adc_soc
void ADC_Init(void);
void INTERRUPT adc_isr();

//comp
void InitComp1(void);
void InitComp2(void);

//epwm
void InitEpwm2_Example(void);
void InitEpwm3_Example(void);

//epwm_tz
void INTERRUPT epwm2_tz_isr(void);
void INTERRUPT epwm3_tz_isr(void);

void InitLED(void);

#endif /* SYSTEM_H_ */
