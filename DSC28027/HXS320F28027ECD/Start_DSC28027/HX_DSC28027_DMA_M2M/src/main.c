/******************************************************************
 文 档 名 ：HX_DSC28027_DMA
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
                  Start_DSC28027_V1.1
                  Start_DSC28027PTT_Rev1.2
 D S P：     DSC28027
 使 用 库：无
 作 用：DMA，数据迁移
 说 明：在Haawking IDE调试界面上输入DMABuf1与DMAf2数组，查看数组数据
 ----------------------例程使用说明-----------------------------
 *
 *            测试DMA功能
 *
 *
 * 现象：可将在同一内存分区中将DMABuf1数组数据复制到DMABuf2中,
 * 数据一致，LED2(D400)亮；数据不一致，LED3(D401)亮
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com

 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

//
// Defines
//
#define BUF_SIZE   512     // 缓冲区大小：
//传输数据量=内环BURST_SIZE*外环TRANSFER_SIZE=32*16=512个16位数;
//
// DMA 定义
//
#define CH1_TOTAL               DATA_POINTS_PER_CHANNEL
#define CH1_WORDS_PER_BURST     ADC_CHANNELS_TO_CONVERT


volatile Uint16 CODE_SECTION("ramfuncs1")  DMABuf1[BUF_SIZE];
volatile Uint16 CODE_SECTION("ramfuncs1")  DMABuf2[BUF_SIZE];

volatile Uint32 *DMADest;
volatile Uint32 *DMASource;

//
// Function Prototypes
//
__interrupt void local_DINTCH1_ISR(void);
uint32 i;


void LED_INIT(){
  EALLOW;
  GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;
  GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;
  GpioDataRegs.GPADAT.bit.GPIO6= 1;
  GpioDataRegs.GPADAT.bit.GPIO7 = 1;
  GpioDataRegs.GPASET.bit.GPIO6 = 1;
  GpioDataRegs.GPASET.bit.GPIO7 = 1;
  EDIS;
}
//
// Main
//
int main(void)
{
    /*初始化系统时钟配置*/
    InitSysCtrl();
    Uint16 i;
    /*初始化LED，用于判断DMA数据迁移状态*/
    LED_INIT();
    /*关中断*/
     InitPieCtrl();
     /*清中断*/
     IER = 0x0000;
     IFR = 0x0000;
     /*初始化中断向量表*/
     InitPieVectTable();

     EALLOW;	// Allow access to EALLOW protected registers
     /*DINT1CH1中断入口地址，指向执行数据迁移程序*/
     PieVectTable.DINTCH1= &local_DINTCH1_ISR;
     EDIS;   // Disable access to EALLOW protected registers

     /*关闭定时器0,防止初始化时触发DMA传输*/
     CpuTimer0Regs.TCR.bit.TSS  = 1;
     /*DMA初始化配置*/
     DMAInitialize();
     /*DMA迁移数组变量初值定义*/
     for (i=0; i<BUF_SIZE; i++)
     {
         DMABuf1[i] = 0;
         DMABuf2[i] = i;
     }
     /*配置DMA数据迁移方向的源与目的*/
     DMADest   = &DMABuf1[0];
     DMASource = &DMABuf2[0];
     DMACH1AddrConfig(DMADest,DMASource);
     /*DMA通道CH1突发配置(内环数据量)*/
     //长度32位,
     //源地址增大2个字,目标地址增大2个字
     DMACH1BurstConfig(31,2,2);
     /*DMA通道CH1传输配置(外环数据量)*/
     //长度16位,
     //源地址增大2个字,目标地址增大2个字
     DMACH1TransferConfig(15,2,2);
     /*DMA通道CH1地址返回配置*/
     //源与目的地址传输65536次数完成，发生地址返回，归零时字个数不变
     DMACH1WrapConfig(0xFFFF,0,0xFFFF,0);
     /*DMA通道CH1模式与功能配置*/
     //DMA中断触发源采用TINT0,单次触发模式,传输计数为零时停止计数
     //屏蔽中断同步信号,屏蔽源/目的同步影响,溢出中断屏蔽
     //通道传输字的宽度为32位,传输结束时产生中断,通道1中断使能
     DMACH1ModeConfig(DMA_TINT0,PERINT_ENABLE,ONESHOT_ENABLE,CONT_DISABLE,
                      SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,THIRTYTWO_BIT,
                      CHINT_END,CHINT_ENABLE);
     /*DMA通道CH1启动*/
     StartDMACH1();
     /*定时器0倒计数值定义*/
     //计数到零时触发DMA传输，即定义DMA传输速度计数一个计数传输2个数
     CpuTimer0Regs.TIM.half.LSW = 512;
     /*定时器0中断使能*/
     CpuTimer0Regs.TCR.bit.TIE  = 1;
     /*重启定时器0*/
     CpuTimer0Regs.TCR.bit.TSS  = 0;
     /*使能打开对应的中断*/
     IER = M_INT7 ;
     PieCtrlRegs.PIEIER7.bit.INTx1 = 1;
     /*使能打开全局中断*/
     EINT;
     while(1)
     {
    	 DMABuf2[1]++;
         DMACH1AddrConfig(DMADest,DMASource);
         /*DMA通道CH1启动*/
         StartDMACH1();
         DELAY_US(100);
     }
 }

 //
 // local_DINTCH1_ISR - INT7.1(DMA Channel 1)
 //
 __interrupt void local_DINTCH1_ISR(void)
 {
	  /*判断被迁移数组DMABuf1与迁移到的数组DMABuf1数据是否一致*/

	GpioDataRegs.GPACLEAR.bit.GPIO7 = 1;//GPIO7(D401)常亮
	GpioDataRegs.GPASET.bit.GPIO6 = 1; //GPIO6(D400)灭
	 for (i = 0;i<BUF_SIZE;i++)
     {
     	if(DMABuf1[i] != DMABuf2[i])
     	{
     		GpioDataRegs.GPACLEAR.bit.GPIO6 = 1;//不一致则GPIO6(D400)常亮
		GpioDataRegs.GPASET.bit.GPIO7 = 1;//GPIO7(D401)灭
     	}
	else
	{

	}
     }
     
     /*中断应答锁定DMA中断*/
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
 }

 //
 // End of File
 //
// ----------------------------------------------------------------------------
