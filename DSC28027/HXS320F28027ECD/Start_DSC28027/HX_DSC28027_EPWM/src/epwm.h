#ifndef epwm_h_
#define epwm_h_

#include "dsc_config.h"

typedef struct
{
	volatile struct EPWM_REGS *EPwmRegHandle;
	uint16_t EPwm_CMPA_Direction;
	uint16_t EPwm_CMPB_Direction;
	uint16_t EPwmTimerIntCount;
	uint16_t EPwmMaxCMPA;
	uint16_t EPwmMinCMPA;
	uint16_t EPwmMaxCMPB;
	uint16_t EPwmMinCMPB;
} EPWM_INFO;


void InitEPwm1Example(void);
void InitEPwm2Example(void);
void InitEPwm3Example(void);

void INTERRUPT epmw1_isr(void);
void INTERRUPT epmw2_isr(void);
void INTERRUPT epmw3_isr(void);

void INTERRUPT epwm1_tz_isr(void);
void INTERRUPT epwm2_tz_isr(void);

void update_compare(EPWM_INFO*);

extern EPWM_INFO epwm1_info;
extern EPWM_INFO epwm2_info;
extern EPWM_INFO epwm3_info;

void InitLED(void);

#endif/*epwm_h_*/
