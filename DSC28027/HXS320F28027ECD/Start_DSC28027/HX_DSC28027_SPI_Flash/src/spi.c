/*
 * spi.c
 *
 *  Created on: 2021年8月20日
 *      Author: dingxy
 */
#include "spi.h"

uint8_t upper_128[128] ={ 0 };
W25Q64Flash SPIFlash ={ 0, 0, 0, 0 };

/******************************************************************
 *函数名：void SPI_IOinit(void)
 *参 数：无
 *返回值：无
 *作 用：GPIO16、GPIO17、GPIO18为SPI功能，GPIO19为片选引脚
 ******************************************************************/

void SPI_IOinit(void)
{
	EALLOW;
	/*将 GPIO19 引脚配置为:通用输入/输出*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0;
	/*将 GPIO19引脚配置为输出*/
	GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;
	/*//使能内部上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0;
	/*异步*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 3;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO18 = 3;
	/*SPISIMOA-SPI-A 从机输入，主机输出 (I/O)*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 1;
	GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 1;
	GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 1;
	EDIS;
}

/******************************************************************
 *函数名：void spi_fifo_init(void)
 *参 数：无
 *返回值：无
 *作 用：初始化SPI的FIFO寄存器
 ******************************************************************/

void SPI_fifo_init(void)
{
	SpiaRegs.SPIFFTX.bit.SPIRST = 1;//1h (R/W) = SPI FIFO 可以恢复发送或接收。对 SPI 寄存 器位没有影响。
	SpiaRegs.SPIFFTX.bit.SPIFFENA = 1;		//使能SPIFIFO增强型功能。
	SpiaRegs.SPIFFTX.bit.TXFFIL = 1;		//发送 FIFO 小于等于1时产生TX FIFO中断
	SpiaRegs.SPIFFTX.bit.TXFFINTCLR = 1;	//写 1 以清除 SPIFFTX[TXFFINT]标志。
	SpiaRegs.SPIFFTX.bit.TXFFIENA = 0;		//TX FIFO 中断使能

	SpiaRegs.SPIFFRX.bit.RXFFOVFCLR = 1;	//给此位写 1 清除 SPIFFRX[RXFFOVF]。
	SpiaRegs.SPIFFRX.bit.RXFFINTCLR = 1;	//写 1 以清除 SPIFFRX[RXFFINT]标志。
	SpiaRegs.SPIFFRX.bit.RXFFST = 1;			//接收 FIFO 有一个字
	SpiaRegs.SPIFFRX.bit.RXFFIENA = 0;	//接收 FIFO 中断基于 RXFFIL 匹配(大于或 等于)的将被启用

	SpiaRegs.SPIFFRX.bit.RXFFIL = 4;		//=一个 RX FIFO 中断请求产生时，有 4 个字 在接收缓冲区

	SpiaRegs.SPIFFCT.all = 0;	//上一个字传输完成后，TX FIFO 缓冲区 中的下一个字立刻被传输到 SPITXBUF。
}

/******************************************************************
 *函数名：void spi_init(void)
 *参 数：无
 *返回值：无
 *作 用：初始化SPI的控制寄存器
 ******************************************************************/
void spi_init(void)
{
	GpioDataRegs.GPASET.bit.GPIO19 = 1;	//GPIO19置1

	SpiaRegs.SPICCR.bit.SPISWRESET = 0;	//清除接收端溢出标志 位(SPISTS.7)、SPI INT 标志位(SPISTS.6)
	SpiaRegs.SPICCR.bit.SPICHAR = 7;		//字符长度控制位7h (R/W) = 8-bit word
	SpiaRegs.SPICCR.bit.CLKPOLARITY = 1;	//数据在下降边输出，上升边输入。

	SpiaRegs.SPICTL.bit.CLK_PHASE = 0;		//正常的 SPI 时钟方案，取决于 时钟极性位(SPICCR.6)
	SpiaRegs.SPICTL.bit.MASTER_SLAVE = 1;	//设置为主机

	SpiaRegs.SPIBRR = 2;								//SPI 波特率= LSPCLK / 4。
	SpiaRegs.SPICCR.bit.SPISWRESET = 1;		//准备发生或接收下一字

	SpiaRegs.SPIPRI.bit.FREE = 1;				//自由运行，无论挂起或何时发生挂起，继续 SPI 操作。
	SpiaRegs.SPIPRI.bit.STEINV = 0;				//SPISTEn 活跃低(正常)
}

/******************************************************************
 *函数名：unsigned long Jedec_ID_Read(void)
 *参 数：无
 *返回值：W25Q16 ID参数
 *作 用：*《W25Q16BV.pdf》
 *P17  W25Q16 ID参数
 *P20  ID 指令表格与读取指令的通信格式
 ******************************************************************/
unsigned long Jedec_ID_Read(void)
{
	unsigned long temp1, temp2, temp3;
	temp1 = 0;
	temp2 = 0;
	temp3 = 0;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x9F);
	temp1 = Get_Byte();
	temp2 = Get_Byte();
	temp3 = Get_Byte();
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
	/* 数据在SPIRXBUF中是右对齐存储的*/
	temp1 = ((temp1 << 16) | (temp2 << 8) | (temp3)) & 0x00FFFFFF;
	return temp1;
}

/******************************************************************
 *函数名：uint16 Read_Status_2Reg(void)
 *参 数：无
 *返回值：状态寄存器1和状态寄存器2中的值
 *作 用：读状态寄存器1和状态寄存器2的值
 ******************************************************************/
uint16 Read_Status_2Reg(void)
{
	uint16 wbyte1, wbyte2;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	/*读状态寄存器1指令，返回Status Register1*/
	Send_Byte(0x05);
	wbyte1 = Get_Byte();
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	/*READ_STATUS_REG_IST  读状态寄存器2指令，返回Status Register2*/
	Send_Byte(0x35);
	wbyte2 = Get_Byte();
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;

	wbyte2 <<= 6;
	wbyte2 &= 0xff00;
	wbyte2 += wbyte1 & 0xff;
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;

	return wbyte2;
}

unsigned char Read_Status_Register(void)
{
	uint16 byte;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x05);
	byte = Get_Byte();
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
	return byte;
}

void WREN(void)
{
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x06);
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

void WrSReg(uint16 setReg)
{
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x01);
	Send_Byte(setReg & 0x00ff);
	Send_Byte((setReg & 0x0300) >> 8);

	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

void Wait_Busy(void)
{
	uint32 waitcnt;
	unsigned char Wbusy;
	Wbusy = Read_Status_Register();
	waitcnt = 600000;
	while ((Wbusy & 0x01) == 0x01)
	{
		Wbusy = Read_Status_2Reg();
		if (waitcnt == 0)
		{
			WREN();
			WrSReg(0x0000);
			SPIFlash.SReg = Read_Status_2Reg();
		}
		else
			waitcnt--;
	}
}

void Chip_Erase(void)
{
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x60);
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

void ReadData(unsigned char Dst, unsigned char *Rxbuf, unsigned long len)
{
	unsigned long i = 0;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x03);
	Send_Byte((Dst & 0xFFFFFF) << 16);
	Send_Byte((Dst & 0xFFFF) << 8);
	Send_Byte(Dst & 0xff);
	for (i = 0; i < len; i++)
	{
		*(Rxbuf++) = Get_Byte();
	}

	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

void PageProgram(unsigned char Dst, unsigned char *byte, unsigned char len)
{
	unsigned char i = 0;
	/*GPIO19置0*/
	GpioDataRegs.GPACLEAR.bit.GPIO19 = 1;
	Send_Byte(0x02);
	Send_Byte((Dst & 0xFFFFFF) << 16);
	Send_Byte((Dst & 0xFFFF) << 8);
	Send_Byte(Dst & 0xff);
	for (i = 0; i < len; i++)
	{
		Send_Byte(*(byte++));
	}
	/*GPIO19置1*/
	GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

uint8_t Send_Byte(uint16 a)
{
	uint16 rdata;
	SpiaRegs.SPICTL.bit.TALK = 1;
	SpiaRegs.SPITXBUF = (a << 8) & 0xff00;
	while (SpiaRegs.SPIFFRX.bit.RXFFST == 0)
	{
	}
	rdata = SpiaRegs.SPIRXBUF;
	return rdata;
}

uint16 Get_Byte(void)
{
	uint8_t rdata;
	SpiaRegs.SPICTL.bit.TALK = 0;
	SpiaRegs.SPITXBUF = DummyData
	;
	while (SpiaRegs.SPIFFRX.bit.RXFFST == 0)
	{
	}
	rdata = SpiaRegs.SPIRXBUF;
	return rdata;
}

