/******************************************************************
 文 档 名：     epwm.h
 D S P：       DSC28027
 使 用 库：     
 作     用：      
 说     明：      提供epwm.h接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年1月19日
 作 者：何洋
 @ mail：support@mail.haawking.com
 ******************************************************************/
#ifndef EPWM_H_
#define EPWM_H_

#include "dsc_config.h"

void InitEPwm1Example(void);
void InitEPwm2Example(void);
void InitEPwm3Example(void);

void INTERRUPT epmw1_isr(void);

void epwm1_compare(void);

void thei(void);



#endif /* EPWM_H_ */
