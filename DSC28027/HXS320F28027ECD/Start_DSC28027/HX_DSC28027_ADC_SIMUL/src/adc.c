#include "adc.h"
/******************************************************************
 *函数名：void ADC_Config()
 *参 数：无
 *返回值：无
 *作 用：配置ADC
 ******************************************************************/
void ADC_Config(void)
{
	EALLOW;
	AdcRegs.ADCCTL1.bit.ADCREFSEL = 0;
	/*NT脉冲产生在ADC结果锁定到其结果寄存器之前发生一个周期*/
	AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 3;		// ADC clock = SYSCLK/8
	/*中断选择3寄存器*/
	AdcRegs.INTSEL3N4.bit.INT3E = 1;
	/*每当产生EOC脉冲时，都会产生附加脉冲，而不管是否清除了标记位*/
	AdcRegs.INTSEL3N4.bit.INT3CONT = 0;
	/*EOC0是ADCINT3的触发器*/
	AdcRegs.INTSEL3N4.bit.INT3SEL = 0;
	/*SOC0与SOC1同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN0=1;
	/*SOC2与SOC3同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN2=1;
	/*SOC4与SOC5同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN4=1;
	/*SOC6与SOC7同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN6=1;
	/*SOC8与SOC9同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN8=1;
	/*SOC10与SOC11同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN10=1;
	/*SOC12与SOC13同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN12=1;
	/*SOC14与SOC15同时采样*/
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN14=1;


	/*将SOC0设置为样本A0/B0,结果分别存储在RESULT0/RESULT1中*/
	AdcRegs.ADCSOC0CTL.bit.CHSEL = 0;
	/*将SOC2设置为样本A1/B1,结果分别存储在RESULT2/RESULT3中*/
	AdcRegs.ADCSOC2CTL.bit.CHSEL = 1;
	/*将SOC4设置为样本A2/B2,结果分别存储在RESULT4/RESULT5中*/
	AdcRegs.ADCSOC4CTL.bit.CHSEL = 2;
	/*将SOC6设置为样本A3/B3,结果分别存储在RESULT6/RESULT7中*/
	AdcRegs.ADCSOC6CTL.bit.CHSEL = 3;
	/*将SOC8设置为样本A4/B4,结果分别存储在RESULT8/RESULT9中*/
	AdcRegs.ADCSOC8CTL.bit.CHSEL = 4;
	/*将SOC10设置为样本A5/B5,结果分别存储在RESULT10/RESULT11中*/
	AdcRegs.ADCSOC10CTL.bit.CHSEL = 5;
	/*将SOC12设置为样本A6/B6,结果分别存储在RESULT12/RESULT13中*/
	AdcRegs.ADCSOC12CTL.bit.CHSEL = 6;
	/*将SOC14设置为样本A7/B7,结果分别存储在RESULT14/RESULT15中*/
	AdcRegs.ADCSOC14CTL.bit.CHSEL = 7;

	/*SOCx触发源选择:ADCTRIG5-ePWM1，ADCSOCA*/
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC4CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC6CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC8CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC10CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC12CTL.bit.TRIGSEL = 0x05;
	AdcRegs.ADCSOC14CTL.bit.TRIGSEL = 0x05;

	/*采样窗口长达6个周期（2n+6个时钟周期）*/
	AdcRegs.ADCSOC0CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC2CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC4CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC6CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC8CTL.bit.ACQPS = 0;
	AdcRegs.ADCSOC10CTL.bit.ACQPS= 0;
	AdcRegs.ADCSOC12CTL.bit.ACQPS =0;
	AdcRegs.ADCSOC14CTL.bit.ACQPS =0;

	EDIS;
}
