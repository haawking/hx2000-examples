#include "epwm.h"
#include "adc.h"

Uint32 adcValA[8];
Uint32 adcValB[8];
/******************************************************************
 *函数名：void INTERRUPT adc_isr()
 *参 数：无
 *返回值：无
 *作 用：adc中断服务函数
 ******************************************************************/
void INTERRUPT adc_isr()
{
	while(AdcRegs.ADCCTL1.bit.ADCBSY == 1)
	{
	}

	adcValA[0] = AdcResult.ADCRESULT0;
	adcValA[1] = AdcResult.ADCRESULT2;
	adcValA[2] = AdcResult.ADCRESULT4;
	adcValA[3] = AdcResult.ADCRESULT6;
	adcValA[4] = AdcResult.ADCRESULT8;
	adcValA[7] = AdcResult.ADCRESULT14;

	adcValB[1] = AdcResult.ADCRESULT3;
	adcValB[2] = AdcResult.ADCRESULT5;
	adcValB[3] = AdcResult.ADCRESULT7;
	adcValB[4] = AdcResult.ADCRESULT9;
	adcValB[6] = AdcResult.ADCRESULT13;
	adcValB[7] = AdcResult.ADCRESULT15;

	EALLOW;
	/*清除ADCINTFLG寄存器中各自的标志位*/
	AdcRegs.ADCINTFLGCLR.bit.ADCINT3 = 0x1;
	/* 清除ADCINTOVF寄存器中相应的溢出位*/
	AdcRegs.ADCINTOVFCLR.bit.ADCINT3 = 0x1;
	/* 中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
	EDIS;

}

