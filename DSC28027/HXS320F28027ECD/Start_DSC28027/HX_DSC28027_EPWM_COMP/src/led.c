#include "system.h"
/******************************************************************
*鍑芥暟鍚嶏細void InitLED()
*鍙� 鏁帮細鏃�
*杩斿洖鍊硷細鏃�
*浣� 鐢細鍒濆鍖朙ED
 ******************************************************************/
void InitLED(void)
{
		/*鍏佽璁块棶鍙椾繚鎶ょ殑绌洪棿*/
		EALLOW;
		/*灏咲400 GPIO7閰嶇疆涓烘暟瀛桰O;*/
		GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;
		/* 灏咲400 GPIO7閰嶇疆涓鸿緭鍑�;*/
		GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;

		GpioDataRegs.GPASET.bit.GPIO7=1;
		/* 灏咲401 GPIO6閰嶇疆涓烘暟瀛桰O;*/
		GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;
		/*灏咲401 GPIO6閰嶇疆涓鸿緭鍑�;*/
		GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;
		GpioDataRegs.GPASET.bit.GPIO6=1;
		EDIS;												//绂佹璁块棶鍙椾繚鎶ょ殑绌洪棿
}
