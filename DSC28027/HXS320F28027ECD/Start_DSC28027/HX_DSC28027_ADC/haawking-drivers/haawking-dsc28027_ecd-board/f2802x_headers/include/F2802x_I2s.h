//###########################################################################
//
// FILE:   F2802x_I2s.h
//
// TITLE:  F2802x Inter-IC Sound(I2S) Module
//         Register Bit Definitions.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.0.0 $
// $Release Date: 2022-07-09 04:26:57 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#ifndef F2802x_I2S_H
#define F2802x_I2S_H

#ifdef __cplusplus
extern "C" {
#endif


#ifdef  DSC28029

//---------------------------------------------------------------------------
// I2S Individual Register Bit Definitions:
//

struct I2SSCTRL_BITS {        //bit description
   Uint32   FRMT:2;             //1:0  Sets the serializer data format
   Uint32   MODE:1;           //2   Sets the serializer in master or slave mode
   Uint32   WDLEN:4;         //6:3 Choose serializer word length
   Uint32   PACK:1;            //7   Enable data packing,Divides down the generation of interrupts so that data is packed
   Uint32   DATDLY:2;        //9:8  Sets the I2S receive/transmit data delay
   Uint32   CLKPOL:1;        //10  Controls I2S clock polarity
   Uint32   FSPOL:1;          //11  Inverts I2S frame-synchronization polarity
   Uint32   LOOPBACK:1;  //12   Routes data from transmit shift register back to receive shift register for internal digital loopback
   Uint32   MONO:1;        //13   Sets I2S into meno or Stereo mode
   Uint32   rsvd1:2;           //15:14  Reserved
   Uint32   ENABLE:1;       //16     Resets or enables the serializer transmission or reception
   Uint32   SLTLEN:4;       //20:17   Choose serializer slot length
   Uint32   FRLEN:2;        //22:21   frame length 1(1 to 16 time slots)  FRLEN in I2SSCTRL selects the number of serial slots
   Uint32   RESERVE_BITOUT:1;   //23  Resverve bit output control
   Uint32   RSV_PAD_EN:1;         //24   I2S_DX PAD output enable control for reserve bit
   Uint32   rsvd2:7;                    //31:25  Reservd
};

union I2SSCTRL_REG {
    Uint32           all;
    struct  I2SSCTRL_BITS bit;
};



struct I2SSRATE_BITS{
     Uint32   CLKDIV:8;            //7:0   Divider to generate I2Sn_CLK(bit - clock).the system clock (or DSP clock) to the I2S is divided down by the configured valued
     Uint32   FSDIV:4;              //11:8  divider to generate I2Sn_FS
     Uint32   FWID:8;              //19:12   Frame-synchronization pulse width bits for I2Sn_FS(frame - synchronization clock)
     Uint32   Rsvd:12;             //31:20    Reserved
};


union I2SSRATE_REG {
     Uint32          all;
     struct   I2SSRATE_BITS bit;
};


struct I2SINTFL_BITS {
     Uint32      RCVOUERREL:1;     //0 Receive overrun or Underrun condition .This bit is cleared on read
     Uint32      XMITOUERRFL:1;   //1 Transmit overrun or Underrun condition.This bit is cleared on read
     Uint32      FERREL:1;             //2   Frame-synchronization error flag.This bit is cleared on read
     Uint32      RCVMONFL:1;      //3   Mono data receive flag. Used only when the MONO bit 12 in the I2SSCTRL register = 1(Mono mode).This bit is cleared on read
     Uint32      RCVSTFL:1;           //4   Stereo data receive flag.Used only when the MONO bit 12 in the I2SSCTRL register = 0 (Stereo mode).This bit is cleared on read   
     Uint32      XMITMONFL:1;     //5    Mono data transmit flag.Used only when the MONO bit 12 in the I2SSCTRL register = 1(Mono mode).This bit is cleared on read
     Uint32      XMITSTFL:1;         //6     Stereo data transmit flag.Used only when the MONO bit 12 in the I2SSCTRL register = 0 (Stereo mode).This bit is cleared on read
     Uint32      Rsvd:24;               //31:7  Reserved
};


union I2SINTFL_REG {
     Uint32          all;
     struct    I2SINTFL_BITS  bit;
};

struct  I2SINTMASK_BITS {
      Uint32    RCVOUERR:1;      //0  Enable Recevie overrun or underrun condition
      Uint32    XMITOUERR:1;    //1   Enable Transmit overrun or underrun condition
      Uint32    FERR:1;               //2   Enable frame sync error
      Uint32    RCVMON:1;        //3   Enable mono left receive data interrupt. Used only when the MONO bit 12 in the I2SSCTRL register = 1 (Mono mode)..
      Uint32    RCVST:1;            //4    Enable stereo left/right receive data interrupt. Used only when the MONO bit 12 in the I2SSCTRL register = 0 (Stereo mode).
      Uint32    XMITMON:1;      //5    Enable mono left transmit data interrupt. Used only when the MONO bit 12 in the I2SSCTRL register = 1 (Mono mode).
      Uint32    XMITST:1;           //6    Enable stereo left/right transmit data interrupt. Used only when the MONO bit 12 in the I2SSCTRL register = 0 
      Uint32      Rsvd:24;               //31:7  Reserved   
};



union I2SINTMASK_REG {
      Uint32         all;
      struct    I2SINTMASK_BITS bit;
};



struct  I2SMCR1_BITS{
      Uint32      RMCM:1;      //0 Receive multichannel selection mode bit. RMCM determines whether all channels or only selected
      Uint32      rsvd:31;        //reserved

};




union  I2SMCR1_REG {
       Uint32        all;
       struct     I2SMCR1_BITS bit;
};

struct  I2SMCR2_BITS {
     Uint32      XMCM:2;     //1:0   Transmit multichannel selection mode bits. XMCM determines whether all channels or only selected channels are enabled and unmasked for transmission
     Uint32      rsvd:30;        //31:2    reservd
};



union  I2SMCR2_REG {
        Uint32         all;
        struct          I2SMCR2_BITS  bit;
};


struct  I2SRCERA_BITS {
        Uint32      RCE0:1;     //0   Receive channel enable bit.For receive multichannel selection mode (RMCM = 1):0 Disable the channel that is mapped to RCE0. 1 Enable the channel that is mapped to RCE0.
        Uint32      RCE1:1;     //1
        Uint32      RCE2:1;    //2
        Uint32      RCE3:1;    //3
        Uint32      RCE4:1;    //4
        Uint32      RCE5:1;    //5
        Uint32      RCE6:1;    //6
        Uint32      RCE7:1;    //7
        Uint32      RCE8:1;    //8
        Uint32      RCE9:1;    //9
        Uint32      RCE10:1;  //10
        Uint32      RCE11:1;  //11
        Uint32      RCE12:1;  //12
        Uint32      RCE13:1;  //13
        Uint32      RCE14:1;  //14
        Uint32      RCE15:1;  //15
        Uint32      rsvd:16;   //31:16  resvd

};



union I2SRCERA_REG {
        Uint32         all;
        struct   I2SRCERA_BITS   bit;
};


struct  I2SXCERA_BITS {
        Uint32      XCE0:1;     //0   Transmit channel enable bit. The role of this bit depends on which transmit multichannel selection mode is selected with the XMCM bits.
        Uint32      XCE1:1;     //1
        Uint32      XCE2:1;    //2
        Uint32      XCE3:1;    //3
        Uint32      XCE4:1;    //4
        Uint32      XCE5:1;    //5
        Uint32      XCE6:1;    //6
        Uint32      XCE7:1;    //7
        Uint32      XCE8:1;    //8
        Uint32      XCE9:1;    //9
        Uint32      XCE10:1;  //10
        Uint32      XCE11:1;  //11
        Uint32      XCE12:1;  //12
        Uint32      XCE13:1;  //13
        Uint32      XCE14:1;  //14
        Uint32      XCE15:1;  //15
        Uint32      rsvd:16;   //31:16  resvd

};



union I2SXCERA_REG {
        Uint32         all;
        struct     I2SXCERA_BITS   bit;
};


//---------------------------------------------------------------------------
// SPI Register File:
//
struct  I2S_REGS {
       union  I2SSCTRL_REG     I2SSCTRL;            //00   I2S Serializer Control Register
       union  I2SSRATE_REG     I2SSRATE;            //04   I2S Sample Rate Generator Register
       Uint32           I2STXLT0;                             //08   I2S Transmit Left Data 0 Register
       Uint32           I2STXRT0;                             //0C  I2S Transmit Right Data 0 Register
       union   I2SINTFL_REG     I2SINTFL;             //10   I2S Interrupt Flag Register
       union   I2SINTMASK_REG     I2SINTMASK;  //14   I2S Interrupt Mask Register
       Uint32           I2SRXLT0;                              //18   I2S Receive Left Data 0 Register
       Uint32           I2SRXRT0;                             //1C   I2S Receive Right Data 0 Register
       union   I2SMCR1_REG     I2SMCR1;            //20    I2S Multichannel Register 1
       union   I2SMCR2_REG     I2SMCR2;            //24    I2S Multichannel Register 2
       union   I2SRCERA_REG    I2SRCERA;           //28    I2S Receive Channel Enable Register Partition A 
       union   I2SXCERA_REG    I2SXCERA;           //2C    I2S Transmit Channel Enable Register Partition A
};






extern volatile struct I2S_REGS *const  P_I2saRegs;
extern volatile struct I2S_REGS  I2saRegs;


#endif


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  

//===========================================================================
// End of file.
//===========================================================================

