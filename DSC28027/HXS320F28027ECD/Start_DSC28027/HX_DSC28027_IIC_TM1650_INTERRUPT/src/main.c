/******************************************************************
文 档 名 ：HX_DSC28027_IIC_TM1650_INTERRUPT
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Start_DSC28027PTT_Rev1.2
 D S P：     DSC28027
 使 用 库：无
 作 用：(1)IIC控制TM1650数码管驱动和扫描按键S101 S102 S103
 (2)IIC应答接收中断测试
 说 明：FLASH工程
 ----------------------例程使用说明-----------------------------
 *
 *  通过IIC读写EEPROM，控制TM1650芯片的数字循环显示，
 *  清零、扫描按键输入控制显示与调节亮度执行主程序调用
 * InitI2C_Gpio();     //IO 初始化为IIC
 * I2CA_Init();          // IIC初始化，100KHz
 * TM1650_Send()单个数码管显示函数
 * TM1650_Read()读取TM1650扫描值，获取按键S101 S102 S103的状态
 *
 * 现象：左二到左四数码管循环显示0-999，LED3/D401闪烁
 *           按下按键S101 左1数码管显示数字+1
 *           按下按键S102 改变数码管显示亮度
 *           按下按键S103 四个数码管清零
 *           每按下一次按键S101/S102/S103进一次I2C应答接收中断 D400翻转
 *
  版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "TM1650_IIC.h"

int main(void)
{
	/*初始化系统控制*/
	InitSysCtrl();
	/*初始化LED*/
	InitLED();
	/*初始化IIC总线的Gpio*/
	InitI2C_Gpio();
	/*初始化PIE控制寄存器到默认状态，默认状态是全部PIE中断被禁用和标志位被清除*/
	InitPieCtrl();
	/*禁用CPU中断和清除所有CPU中断标志位*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE中断向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();

	/* 初始化CPU定时器*/
	InitCpuTimers();
	/* 配置CPU定时器：周期1ms*/
	ConfigCpuTimer(&CpuTimer0, 120, 1000);
	/*打开或重置定时器0*/
	StartCpuTimer0();

	/*定时器0的中断入口地址为中断向量表的INT0*/
	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	PieVectTable.I2CINT1A = &i2c_int1a_isr;
	EDIS;


	/*IIC初始化*/
	I2CA_Init();
	/*软件复位IIC从设备*/
	softResetIIC_BUS();
	//printf("\r\nTM1650 init...");//标准C的printf输出测试，串口调试助手设置波特率128000bps，ASCII格式显示

	/*BIT6到BIT4为亮度调节，BIT0是  1 开启/0关闭*/
	LigntVal = 0x11;
	/*1级亮度，开启显示*/
	TM1650_Send(CMD_SEG, LigntVal);
	/*DIG0-DIG3均显示0*/
	TM1650_Send(DIG0, SEG7Table[0]);
	TM1650_Send(DIG1, SEG7Table[0]);
	TM1650_Send(DIG2, SEG7Table[0]);
	TM1650_Send(DIG3, SEG7Table[0]);

	/*使能CPU的第一组中断*/
	IER |= M_INT1|M_INT8;
	/*使能PIE级的第一组中断的第七个小中断，即定时器0中断*/
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
	PieCtrlRegs.PIEIER8.bit.INTx1=1;

	/*使能全局中断*/
	EINT;

	while(1)
	{
		/*间隔为360ms*/
		if(CpuTimer0.InterruptCount >= 360)
		{
			/*定时器0中断次数清零*/
			CpuTimer0.InterruptCount = 0;
			/*D401翻转*/
			GpioDataRegs.GPATOGGLE.bit.GPIO6=1;
			/*数码管刷新显示*/
			TM1650_display();
			/*数码管控制*/
			TM1650_control();
		}
	}
}

// ----------------------------------------------------------------------------


