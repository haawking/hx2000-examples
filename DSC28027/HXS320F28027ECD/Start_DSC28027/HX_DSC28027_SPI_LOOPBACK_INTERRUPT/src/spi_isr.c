#include "spi.h"

Uint16 sdata;
Uint16 rdata;

#define true 1
#define false 0

bool_t status=true;

INTERRUPT void spiTxFifoIsr(void)
{
	 /*SPI发送数据赋值*/

		SpiaRegs.SPITXBUF=sdata;

	 /*SPI发送FIFO中断清零*/
	SpiaRegs.SPIFFTX.bit.TXFFINTCLR=1;
	 /*中断应答：锁定SPI中断*/
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP6;
}

INTERRUPT void spiRxFifoIsr(void)
{
	 /*SPI接收数据赋值*/

		rdata=SpiaRegs.SPIRXBUF;

	     	if(sdata==rdata)
	       	{
	       		status=true;
	       	}
	       	else
	       	{
	       		status=false;
	       	}


	    	if(status==true)
	    	{
	    		GpioDataRegs.GPACLEAR.bit.GPIO6=1;
	    	}
	    	else
	    	{
	    		GpioDataRegs.GPASET.bit.GPIO6=1;
	    	}


	/*SPI接收溢出FIFO中断清零*/
	SpiaRegs.SPIFFRX.bit.RXFFOVFCLR=1;
	/*SPI接收FIFO中断清零*/
	SpiaRegs.SPIFFRX.bit.RXFFINTCLR=1;
	 /*中断应答：锁定SPI中断*/
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP6;
}

