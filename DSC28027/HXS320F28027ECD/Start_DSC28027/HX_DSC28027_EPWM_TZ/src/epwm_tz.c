#include "epwm.h"

Uint16 EPwm_TZ_CBC_flag;
/******************************************************************
 *函数名：void INTERRUPT epwm1_tz_isr()
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm1_tz_isr(void)
{
	EALLOW;
	/*清除中断标志位*/
	EPwm1Regs.TZCLR.bit.INT = 1;
	EDIS;

	if(EPwm1Regs.TZFLG.bit.OST==1)
	{
		GpioDataRegs.GPACLEAR.bit.GPIO7=1;
	}
	else
	{
		GpioDataRegs.GPASET.bit.GPIO7=1;
	}

	/*PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
/******************************************************************
 *函数名：void INTERRUPT epwm2_tz_isr()
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm2_tz_isr(void)
{

	EALLOW;
	/*清除周期性触发事件标志位*/
	EPwm2Regs.TZCLR.bit.CBC = 1;
	/*清除中断标志位*/
	EPwm2Regs.TZCLR.bit.INT = 1;
	EDIS;

	if(EPwm2Regs.TZFLG.bit.CBC==1)
	{
		GpioDataRegs.GPACLEAR.bit.GPIO6=1;
	}
	else
	{
		GpioDataRegs.GPASET.bit.GPIO6=1;
	}

	/* PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
