#include "dsc_config.h"
/******************************************************************
 *函数名：void InitLED()
 *参 数：无
 *返回值：无
 *作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的空间*/
	EALLOW;
	/*将GPIO6配置为数字IO*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;
	/*将GPIO6配置为输出*/
	GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;

	GpioDataRegs.GPASET.bit.GPIO6=1;

	/*将GPIO6配置为数字IO*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;
	/*将GPIO6配置为输出*/
	GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;

	GpioDataRegs.GPASET.bit.GPIO7=1;
	/*禁止访问受保护的空间*/
	EDIS;
}
