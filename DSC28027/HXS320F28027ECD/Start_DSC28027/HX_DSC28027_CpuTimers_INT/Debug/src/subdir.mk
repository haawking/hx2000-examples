################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/timer.c 

OBJS += \
./src/main.o \
./src/timer.o 

C_DEPS += \
./src/main.d \
./src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T DSC28027_ECD_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_1_9_2=0 -DDEBUG -DDSC28027_ECD -DHAAWKING_DSC28027_BOARD -I"../haawking-drivers/haawking-dsc28027_ecd-board/common" -I"../haawking-drivers/haawking-dsc28027_ecd-board/f2802x_headers/include" -I"../haawking-drivers/haawking-dsc28027_ecd-board/f2802x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


