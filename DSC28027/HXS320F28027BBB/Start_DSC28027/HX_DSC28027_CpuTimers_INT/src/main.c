/******************************************************************
 文  档  名：  HX_DSC28027_CpuTimer_INT
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板：
                      Start_DSC28027_V1.1
                      Start_DSC28027PTT_Rev1.2
 D S P：       DSC28027
 使 用 库：
 作	    用：   定时器1中断例程
 说	    明：
 ----------------------例程使用说明-----------------------------
 *  CpuTimer1 作为500ms周期
 *  LED2 D400 闪烁由CpuTimer1中断控制
 *
 现        象：LED2 D400 闪烁
 版	    本：V1.0.0
 时	    间：2022年8月11日
 作	    者：heyang
 mail： support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "timer.h"

/*全局变量*/

/*函数原型*/
void InitLED(void);

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    /*关闭中断*/
    DINT;

    /*初始化PIE控制*/
    InitPieCtrl();

    /*关闭CPU中断，清除中断标志位*/
    IER = 0x0000;
    IFR = 0x0000;

    /*初始化PIE中断向量表*/
    InitPieVectTable();

    InitLED(); /*初始化LED*/

    Timer_init(); /*定时器0 定时器1 初始化，定时器0为1ms周期中断 定时器1为500ms */


    /*中断配置步骤-----5*/
    //PieCtrlRegs.PIECTRL.bit.ENPIE = 1;    /*Enable the PIE block*/
    EINT;


    while(1)
    {

    }

	return 0;
}

/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED()
{
	EALLOW;
	/*将D400 GPIO7配置为数字IO;*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;
	/* 将D400 GPIO7配置为输出;*/
	GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;
//	/* 将D401 GPIO6配置为数字IO;*/
//	GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;
//	/*将D401 GPIO6配置为输出;*/
//	GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;
	EDIS;
}


// ----------------------------------------------------------------------------
