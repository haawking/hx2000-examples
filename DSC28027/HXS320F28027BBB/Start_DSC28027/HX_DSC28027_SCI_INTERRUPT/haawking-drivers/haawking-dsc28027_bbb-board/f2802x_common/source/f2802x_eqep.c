//###########################################################################
//
// FILE:   f2802x_eqep.c
//
// TITLE:  F2802x Device enhanced quadrature encoder Initialization & Support Functions.
//
// DESCRIPTION:  Example initialization of system resources.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.1.0 $
// $Release Date: 2022-07-11 09:44:07 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "F2802x_Device.h"     //  Headerfile Include File
#include "f2802x_examples.h"   //  Examples Include File

//
// InitEQep - This function initializes the eQEP(s) to a known state.
//
//void 
//InitEQep(void)
//{
    //
    // Initialize eQEP1
    //
//}

//
// InitEQepGpio - This function initializes GPIO pins to function as eQEP pins
//
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//
// Caution:
// For each eQEP peripheral
// Only one GPIO pin should be enabled for EQEPxA operation.
// Only one GPIO pin should be enabled for EQEPxB operation.
// Only one GPIO pin should be enabled for EQEPxS operation.
// Only one GPIO pin should be enabled for EQEPxI operation.
// Comment out other unwanted lines.
//
void 
InitEQepGpio()
{
#if DSP28_EQEP1
    InitEQep1Gpio();
#endif
}

#if DSP28_EQEP1
// 
// InitEQep1Gpio
//
void 
InitEQep1Gpio(void)
{
    EALLOW;

    //
    // Enable internal pull-up for the selected pins
    // Pull-ups can be enabled or disabled by the user.
    // This will enable the pullups for the specified pins.
    // Comment out other unwanted lines.
    //
    GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;   // Enable pull-up on GPIO12 (EQEP1A)
    GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;   // Enable pull-up on GPIO16 (EQEP1B)
    GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;   // Enable pull-up on GPIO17 (EQEP1S)
    GpioCtrlRegs.GPBPUD.bit.GPIO34 = 0;   // Enable pull-up on GPIO34 (EQEP1I)

    //
    // Inputs are synchronized to SYSCLKOUT by default.
    // Comment out other unwanted lines.
    //
    GpioCtrlRegs.GPAQSEL1.bit.GPIO12 = 0;   //Sync to SYSCLKOUT GPIO12 (EQEP1A)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 0;   //Sync to SYSCLKOUT GPIO16 (EQEP1B)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 0;   //Sync to SYSCLKOUT GPIO17 (EQEP1S)
    GpioCtrlRegs.GPBQSEL1.bit.GPIO34 = 0;   //Sync to SYSCLKOUT GPIO34 (EQEP1I)

    //
    // Configure eQEP-1 pins using GPIO regs
    // This specifies which of the possible GPIO pins will be eQEP1
    // functional pins.
    // Comment out other unwanted lines.
    //
    GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 3;   // Configure GPIO12 as EQEP1A
    GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 2;   // Configure GPIO16 as EQEP1B
    GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 2;   // Configure GPIO17 as EQEP1S
    GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 3;   // Configure GPIO34 as EQEP1I

    EDIS;
}
#endif




//
// End of file
//

