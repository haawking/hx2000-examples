#include "ecap.h"

uint16 ecapcounter;
/******************************************************************
 *函数名：void ECap_Gpio()
 *参 数：无
 *返回值：无
 *作 用：配置ECap引脚
 ******************************************************************/
void ECap_Gpio(void)
{
	EALLOW;
	/*GPIO19使能上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;
	/*使用3采样*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO19 = 1;
	/*将GPIO19引脚配置为:ECAP1-eCAP1 (输入/输出)*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 3;
	EDIS;
}
/******************************************************************
 *函数名：void ECap_Init()
 *参 数：无
 *返回值：无
 *作 用：初始化ECap
 ******************************************************************/
void ECap_Init(void)
{
	EALLOW;
	/*ECAP模块在APWM模式下工作*/
	ECap1Regs.ECCTL2.bit.CAP_APWM = 1;
	ECap1Regs.CAP1 = 0x01312D00;
	ECap1Regs.CAP2 = 0x00989680;

	/*清除ECCLR各标志位*/
	ECap1Regs.ECCLR.all = 0x0FF;
	/*使能Compare Equal作为中断源*/
	ECap1Regs.ECEINT.bit.CTR_EQ_CMP = 1;
	/*TSCTR自由运行*/
	ECap1Regs.ECCTL2.bit.TSCTRSTOP = 1;
	EDIS;
}
/******************************************************************
 *函数名：void INTERRUPT ecap_isr(void)
 *参 数：无
 *返回值：无
 *作 用：ecap中断服务函数
 ******************************************************************/
void INTERRUPT ecap_isr(void)
{
	ecapcounter++;
	if(ECap1Regs.CAP1 >= 0x01312D00)
	{
		ECap1Regs.CAP3 = ECap1Regs.CAP1 - 500000;
	}
	else if(ECap1Regs.CAP1 <= 0x00989680)
	{
		ECap1Regs.CAP3 = ECap1Regs.CAP1 + 500000;
	}

	/*清除CTR=CMP标志*/
	ECap1Regs.ECCLR.bit.CTR_EQ_CMP = 1;
	/*ECAP全局中断状态清除*/
	ECap1Regs.ECCLR.bit.INT = 1;
	/*启动单次发射序列*/
	ECap1Regs.ECCTL2.bit.REARM = 1;
	/*//中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP4;
}
