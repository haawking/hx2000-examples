/******************************************************************
 文 档 名：     dma.c
 D S P：       DSC28027
 使 用 库：     
 作     用：      
 说     明：      提供dma.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V0.0.3
 时 间：2021年11月15日
 作 者：何洋
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

extern Uint32 AddSrc;
extern Uint32 AddDst;

void DmaConfig()
{
	EALLOW;

	/* clear all interrupts */
	DmaRegs.CLEARBLOCK.all = 0x0000000F;
	DmaRegs.CLEARTFR.all = 0x0000000F;
	DmaRegs.CLEARDSTTRAN.all = 0x0000000F;
	DmaRegs.CLEARERR.all = 0x0000000F;
	DmaRegs.CLEARSRCTRAN.all = 0x0000000F;

	/* Address Initialization */
	DmaRegs.CH0.SAR.bit.SAR = AddSrc; /* Source Address initialization */
	DmaRegs.CH0.DAR.bit.DAR = AddDst; /* destination Address initialization */

	/* Flow Type */
	DmaRegs.CH0.CTL.bit.TT_FC = 2;

	/* Block size */
	DmaRegs.CH0.CTL.bit.BLOCK_TS = 10;
	DmaRegs.CH0.CTL.bit.SRC_TR_WIDTH = 2;
	DmaRegs.CH0.CTL.bit.DST_TR_WIDTH = 2;

	/* burst size */
	DmaRegs.CH0.CTL.bit.SRC_MSIZE = 0;
	/*	DmaRegs.CH0.CTL.bit.SRC_MSIZE = 1; */
	/*DmaRegs.CH0.CTL.bit.DEST_MSIZE = 2; */

	/* address action */
	DmaRegs.CH0.CTL.bit.SINC = 3;
	DmaRegs.CH0.CTL.bit.DINC = 0;

	/* DMA Interrupt Enable */
	DmaRegs.CH0.CTL.bit.INT_EN = 1;
	DmaRegs.MASKTFR.all = 0x0101;

	/* handshaking type */
	DmaRegs.CH0.CFG.bit.HS_SEL_SRC = 0;
	/*DmaRegs.CH0.CFG.bit.HS_SEL_DST = 0; */
	DmaRegs.CH0.CFG.bit.SRC_PER = 0;

	/* Channel priority
	 DmaRegs.CH0.CFG.bit.CH_PRIOR = 7;  7-highest 0-lowest */

	/*DmaRegs.CH0.CFG.bit.FIFO_MODE = 1; */

	/* Trigger interrupt sources select.for Memory,ignored */
	DmaRegs.INTCTL0.CH_INT_CLR = 1;
	DmaRegs.INTCTL0.CH_INT_CTL = 0x17;
	DmaRegs.INTCTL0.CH_INT_EN = 1;

	/* Enable DMA */
	DmaRegs.DMACFGREG.bit.DMA_EN = 1;

	EDIS;

}

