/******************************************************************
 文 档 名：     HX_DSC28027_SCI_DMA
 开 发 环 境： Haawking IDE V2.0.0
 开 发 板：      Core_DSC28027_V1.4
                      Start_DSC28027_V1.1
                      Start_DSC28027PTT_Rev1.2
 D S P：         DSC28027
 使 用 库：
 作     用：      通过配置SCI模块寄存器，使能串口DMA接收传送到指定位置
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块DMA模式接收功能

 连接方式：通过CH340串口线连接电脑端的USB口

 现象：电脑端使用串口调试助手，按照SCIA配置中的参数设置波特率、数据位、校验
 位、和停止位。电脑一次发送10个字节，DSP收到后会通过DMA传输到目的地，
 然后重新读取目的地的数据，通过SCI 输出
 注意：不用hex显示

 版 本：V1.0.0
 时 间：2022年8月15日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "system.h"

uint32_t DstData[10] CODE_SECTION("L1");
uint32_t AddSrc;
uint32_t AddDst;

bool_t sci_dma_rxFlag = false;

INTERRUPT void dma_isr(void);

int main(void)
{
	    uint8_t i = 0;

		InitSysCtrl();
		/*
		 0000 低速时钟 = SYSCLKOUT/1
		 0001 低速时钟 = SYSCLKOUT/2
		 0010 低速时钟 = SYSCLKOUT/4 (重置默认值)
		 */
		EALLOW;
		SysCtrlRegs.LOSPCP.all = 0x0000;
		EDIS;

		InitSciaGpio();

		InitPieCtrl();

		InitPieVectTable();

		IER = 0x0000;
		IFR = 0x0000;

		EALLOW;
		PieVectTable.DINTCH1 = &dma_isr;
		EDIS;

		memset(DstData, 0, 10);

		AddSrc = (Uint32) (&(SciaRegs.SCIRXBUF));
		AddDst = (Uint32) (&DstData[0]);

		/*SCI初始化配置*/
		Scia_Config(9600);

		DmaConfig();

		IER |= M_INT7;
		PieCtrlRegs.PIEIER7.bit.INTx1 = 1;
		EINT;


		/*DMA CH0通道使能*/
		EALLOW;
		DmaRegs.CHENREG.all = 0x0101;
		EDIS;

		while(1)
		{
			if (sci_dma_rxFlag == true)
			{
				sci_dma_rxFlag = false;
				Scia_Print("Receive: ");
				for (i = 0; i < 10; i++)
				{
					while (SciaRegs.SCICTL2.bit.TXRDY == 0)
					{
					}
					SciaRegs.SCITXBUF = DstData[i];
				}
				Scia_Print("\r\n");
			}
		}


	return 0;
}


void CODE_SECTION("ramfuncs") INTERRUPT dma_isr()
{

	EALLOW;
	DmaRegs.CLEARTFR.bit.CLEAR = 0xF;
	DmaRegs.CH0.DAR.bit.DAR = AddDst;
	DmaRegs.CHENREG.all = 0x0101;
	EDIS;

	sci_dma_rxFlag = true;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
}


// ----------------------------------------------------------------------------
