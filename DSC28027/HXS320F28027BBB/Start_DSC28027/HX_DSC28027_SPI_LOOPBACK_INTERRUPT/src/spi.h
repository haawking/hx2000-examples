/*
 * spi.h
 *
 *  Created on: 2022��8��20��
 *      Author: HX
 */

#ifndef SPI_H_
#define SPI_H_

#include  "dsc_config.h"

void spi_fifo_init(void);
INTERRUPT void spiTxFifoIsr(void);
INTERRUPT void spiRxFifoIsr(void);
void InitLED(void);

extern Uint16 sdata;
extern Uint16 rdata;

#endif /* SPI_H_ */
