#include "spi.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.bit.GPIO6=0;
	GpioCtrlRegs.GPADIR.bit.GPIO6=1;
	GpioDataRegs.GPASET.bit.GPIO6=1;

	EDIS;
}

void spi_fifo_init(void)
{
    /*SPI软复位禁止，允许初始化配置*/
	SpiaRegs.SPICCR.bit.SPISWRESET=0;

    /*SPI传输长度配置:16位*/
	SpiaRegs.SPICCR.bit.SPICHAR=0xF;
    /*SPI内环模式配置*/
	SpiaRegs.SPICCR.bit.SPILBK=1;

    /*SPI中断使能*/
	SpiaRegs.SPICTL.bit.SPIINTENA=1;
    /*SPI主机模式配置*/
	SpiaRegs.SPICTL.bit.MASTER_SLAVE=1;
    /*SPI传输路径配置:发送*/
	SpiaRegs.SPICTL.bit.TALK=1;
	 /*SPI溢出中断使能*/
	SpiaRegs.SPICTL.bit.OVERRUNINTENA=1;

	 /*SPI波特率配置*/
	SpiaRegs.SPIBRR=0x0063;

	 /*SPI复位*/
	SpiaRegs.SPIFFTX.bit.SPIRST=1;
	 /*SPI FIFO使能*/
	SpiaRegs.SPIFFTX.bit.SPIFFENA=1;
	 /*SPI 发送FIFO使能*/
	SpiaRegs.SPIFFTX.bit.TXFIFO=1;
	 /*SPI 发送FIFO中断线使能*/
	SpiaRegs.SPIFFTX.bit.TXFFIENA=1;
	 /*SPI发送FIFO深度配置:2*/
	SpiaRegs.SPIFFTX.bit.TXFFIL=2;

	 /*SPI 接收FIFO使能*/
	SpiaRegs.SPIFFRX.bit.RXFIFORESET=1;
	 /*SPI 接收FIFO中断线使能*/
	SpiaRegs.SPIFFRX.bit.RXFFIENA=1;
	 /*SPI接收FIFO深度配置:2*/
	SpiaRegs.SPIFFRX.bit.RXFFIL=2;

	SpiaRegs.SPIPRI.bit.SOFT=1;
    /*SPI软复位，初始化配置完成*/
	SpiaRegs.SPICCR.bit.SPISWRESET=1;
}
