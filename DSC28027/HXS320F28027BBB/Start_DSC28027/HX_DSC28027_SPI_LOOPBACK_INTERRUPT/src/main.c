/******************************************************************
 文 档 名：     HX_DSC28027_SPI_LOOPBACK_INTERRUPT
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板：     Start_DSC28027_V1.1
                      Start_DSC28027PTT_Rev1.2
 D S P：       DSC28027
 使 用 库：
 作     用：     通过SPI内环与中断，测试数据的发送与接收功能
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述： 通过SPI内环与中断，测试数据的发送与接收功能，
 演示SPI的内环配置和中断使用

 现象：观察变量数组sdata与读出rdata数据是否一致，
 数据一致 LED3 D401点亮，数据不一致LED3 D401灭。

 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "spi.h"


int main(void)
{
	/*系统时钟初始化*/
	InitSysCtrl();
    /*LED初始化，用于判断SPI传输状态*/
	InitLED();
    /*关中断*/
    InitPieCtrl();
    /*清中断*/
    IER=0x0000;
    IFR=0x0000;
    /*初始化中断向量表*/
    InitPieVectTable();

    EALLOW;
    PieVectTable.SPIRXINTA= &spiRxFifoIsr;
    PieVectTable.SPITXINTA= &spiTxFifoIsr;
    EDIS;
    /*SPI FIFO功能配置*/
    spi_fifo_init();
    /*发送Buffer数组初始化配置*/
    sdata=1;
    /*使能打开相应的中断*/
    IER|=M_INT6;

    PieCtrlRegs.PIEIER6.bit.INTx1=1;
    PieCtrlRegs.PIEIER6.bit.INTx2=1;
    /*使能打开全局中断*/
    EINT;
    while(1){

    	DELAY_US(10000);
    	sdata+=1;


    }

	return 0;
}

// ----------------------------------------------------------------------------
