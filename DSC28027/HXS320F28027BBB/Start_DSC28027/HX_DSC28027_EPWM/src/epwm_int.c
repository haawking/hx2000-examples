#include "epwm.h"

void INTERRUPT epmw1_isr(void)
{
	update_compare(&epwm1_info);

    epwm1_info.EPwmTimerIntCount++;

	EPwm1Regs.ETCLR.bit.INT=1;

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}

void INTERRUPT epmw2_isr(void)
{
	update_compare(&epwm2_info);

	epwm2_info.EPwmTimerIntCount++;

	EPwm2Regs.ETCLR.bit.INT=1;

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}

void INTERRUPT epmw3_isr(void)
{
	update_compare(&epwm3_info);

	epwm3_info.EPwmTimerIntCount++;

	EPwm3Regs.ETCLR.bit.INT=1;

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP3;
}
