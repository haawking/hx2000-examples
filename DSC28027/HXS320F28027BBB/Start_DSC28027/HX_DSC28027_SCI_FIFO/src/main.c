/******************************************************************
 文 档 名：     HX_DSC28027_SCI_FIFO
 开 发 环 境： Haawking IDE V2.0.0
 开 发 板：     Core_DSC28027_V1.4
                      Start_DSC28027_V1.1
                      Start_DSC28027PTT_Rev1.2
 D S P：         DSC28027
 使 用 库：
 作     用：      通过配置SCI模块寄存器，使能串口发送和接收
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块FIFO模式的发
                   送和接收功能

 连接方式：通过CH340串口线连接电脑端的USB口

 现象：电脑端使用串口调试助手，按照SCIA配置中的参数设置波特率、数据位、校验
            位、和停止位。
            发送4个字节，DSP会返回收到的数据

注意：不要用hex显示

 版 本：V1.0.0
 时 间：2022年8月15日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

/*函数原型*/
void Scia_Config(uint32 baud);
void Scia_Send(uint8 data);
void Scia_Print(char *str);


int main(void)
{
	uint8 receiveData[4],i;

	/*初始化系统控制*/
	InitSysCtrl();

	/*初始化内存控制寄存器，使能内存流水线模式*/
	InitFlash();

	/*初始化串口通信的GPIO口*/
	/*GPIO28: SCIRXDA*/
	/*GPIO29: SCITXDA*/
	InitSciGpio();


	/*SCI寄存器配置*/
	Scia_Config(9600);

	/*通过SCI发送字符串*/
	Scia_Print("----------- sci fifo test ------------\r\n");
	Scia_Print("Baud Rate: 9600\r\n");
	Scia_Print("Data Bits: 8\r\n");
	Scia_Print("Parity:    none\r\n");
	Scia_Print("Stop Bits: 1\r\n");
	Scia_Print("\r\n");

	Scia_Print("Pls send some message, (level: 4)\r\n");

	for (;;)
	{
		/*判断SCI是否接收到数据*/
		if(SciaRegs.SCIFFRX.bit.RXFFST == 4)
		{
			for(i = 0; i < 4; i++)
			{
				receiveData[i] = SciaRegs.SCIRXBUF.bit.RXDT;
			}
			Scia_Print((char*)receiveData);
			Scia_Print("\r\n");
		}
	}


	return 0;
}


/******************************************************************
*函数名：Scia_Config(uint32 baud)
*参   数： baud，串口波特率
*返回值：无
*作   用： SCIA 初始化配置
******************************************************************/
void Scia_Config(uint32 baud)
{
	uint32 div = 0;
	uint32 divsel = 0;
	uint32 lospcp = 0;
	uint32 lspclk = 0;
	uint16 brr = 0;

	//获取系统时钟的倍频、分频和低速外部时钟的值
	div = SysCtrlRegs.PLLCR.bit.DIV;
	divsel = SysCtrlRegs.PLLSTS.bit.DIVSEL;
	lospcp = SysCtrlRegs.LOSPCP.bit.LSPCLK;


	if (lospcp != 0)
	{
		lospcp = lospcp * 2;
	}
	else
	{
		lospcp = 1;
	}

	/*分频值设置
	 divsel为 0时，系统时钟4分频
	 divsel为 1时，系统时钟4分频
	 divsel为 2时，系统时钟2分频
	 divsel为 3时，系统时钟1分频*/
	switch (divsel)
	{
	case 0:
	case 1:
		lspclk = 12000000 * div / 4 / lospcp;
		break;
	case 2:
		lspclk = 12000000 * div / 2 / lospcp;
		break;
	case 3:
		lspclk = 12000000 * div / 1 / lospcp;
		break;
	}

	brr = lspclk / (baud * 8) - 1;

	/*SCI 停止位设置    0：一个停止位   1：两个停止位*/
	SciaRegs.SCICCR.bit.STOPBITS = 0;

	/*SCI 奇偶校验位    0：奇偶校验   1：偶偶校验*/
	SciaRegs.SCICCR.bit.PARITY = 0;

	/*SCI 奇偶校验使能   0：关闭   1：启用*/
	SciaRegs.SCICCR.bit.PARITYENA = 0;

	/*SCI 字符长度   0：1个字长  1：2个字长 ... 7：8个字长*/
	SciaRegs.SCICCR.bit.SCICHAR = 7;

	/*使能SCI的发送机和接收机*/
	SciaRegs.SCICTL1.bit.TXENA = 1;
	SciaRegs.SCICTL1.bit.RXENA = 1;

	/*SCI 16位波特率选择寄存器 高8位*/
	SciaRegs.SCIHBAUD = (uint8) ((brr >> 8) & 0xff);
	/*SCI 16位波特率选择寄存器 低8位*/
	SciaRegs.SCILBAUD = (uint8) (brr & 0xff);

	/*SCI 复位*/
	SciaRegs.SCIFFTX.bit.SCIRST = 1;
	/*SCI FIFO 使能*/
	SciaRegs.SCIFFTX.bit.SCIFFENA = 1;
	/*SCI 传输FIFO重置*/
	SciaRegs.SCIFFTX.bit.TXFIFOXRESET = 1;
	/*SCI 传输FIFO清除*/
	SciaRegs.SCIFFTX.bit.TXFFINTCLR = 1;

	/*接收 FIFO 复位*/
	SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
	/*接收 FIFO 中断清除*/
	SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;
	/*接收 FIFO 中断深度设置*/
	SciaRegs.SCIFFRX.bit.RXFFIL = 4;

	/*FIFO 控制器寄存器设置*/
	SciaRegs.SCIFFCT.all = 0;

	/*SCI 软件复位，重新启动SCI*/
	SciaRegs.SCICTL1.bit.SWRESET = 1;

}

/******************************************************************
*函数名：Scia_Send(uint8 data)
*参   数： data，准备发送的字节
*返回值：无
*作   用： SCIA 发送一个字节
******************************************************************/
void Scia_Send(uint8 data)
{
	while (SciaRegs.SCICTL2.bit.TXRDY == 0)
	{
	}
	SciaRegs.SCITXBUF = data;
}

/******************************************************************
*函数名：Scia_Print(char *str)
*参   数： *str，要发送的字符串
*返回值：无
*作   用： SCIA 发送一个字符串
******************************************************************/
void Scia_Print(char *str)
{
	while (*str != '\0')
	{
		while (SciaRegs.SCICTL2.bit.TXRDY == 0)
		{
		}

		SciaRegs.SCITXBUF = *str++;
	}
}

// ----------------------------------------------------------------------------
