#include "TM1650_IIC.h"

//数码管控制
Uint16 Tmp = 0;
unsigned char keyReg = 0;

/******************************************************************
 *函数名：void TM1650_display(void)
 *参 数：无
 *返回值：无
 *作 用：用于数码管刷新显示，0-999循环，由定时器中断控制周期
 ******************************************************************/
void TM1650_display(void)
{
	/*Tmp累加，从0-999循环*/
	Tmp++;
	if(Tmp > 999)
		Tmp = 0;

	/*DIG1显示Tmp的百位*/
	TM1650_Send(DIG1, SEG7Table[Tmp / 100]);
	/*DIG2显示Tmp的十位*/
	TM1650_Send(DIG2, SEG7Table[(Tmp / 10) % 10]);
	/*DIG3显示Tmp的个位*/
	TM1650_Send(DIG3, SEG7Table[Tmp % 10]);
}
/******************************************************************
 *函数名：void TM1650_control(void)
 *参 数：无
 *返回值：无
 *作 用：数码管扫描按键控制
 ******************************************************************/
void TM1650_control(void)
{
	/*TM1650获取扫描按键S101 S102 S103，控制数码管数字显示及亮度调节*/
	TM1650_Read(CMD_KEY, &keyVal);

	/*S101按下时，DIG0显示内容+1*/
	if(keyVal == 0x44)
	{
		keyReg++;
		keyReg &= 0x0f;
		TM1650_Send(DIG0, SEG7Table[keyReg]);
	}

	/*S102按下时，改变数码管显示亮度*/
	if(keyVal == 0x4c)  //
	{
	/*高四位为亮度调节，最后1位是开启、关闭显示*/
		LigntVal = (LigntVal + 0x20) & 0x7F;

		/*1级亮度，开启显示*/
		TM1650_Send(CMD_SEG, LigntVal);
	}

	/*按下S103时*/
	if(keyVal == 0x54)
	{
		Tmp = 0;
		keyReg = 0;

		/*DIG0-DIG3清零*/
		TM1650_Send(DIG0, SEG7Table[0]);
		TM1650_Send(DIG1, SEG7Table[0]);
		TM1650_Send(DIG2, SEG7Table[0]);
		TM1650_Send(DIG3, SEG7Table[0]);
	}
}
