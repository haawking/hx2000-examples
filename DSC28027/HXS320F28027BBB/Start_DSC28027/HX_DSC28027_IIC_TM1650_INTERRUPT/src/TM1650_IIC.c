/*
 * TM1650_IIC.c
 *
 *  Created on: 2021年11月9日
 *      Author: liuq
 */

#include "DSP28x_Project.h"
#include "TM1650_IIC.h"

/*数码管显示设置：亮度、8段/7段显示、开/关显示*/
char keyVal, LigntVal;

I2CSlaveMSG ICF8591Msg[] = { { { 0 }, 0,
IIC_IDLE, 0, 0, 0, { 0 }, { 0 }, 0 } };

char *IIC_ISR_String[] = { "No", "ARB", "NACK", "ARDY", "RX", "Tx", "SCD", "AAS" };

/*定义一个TM1650结构体*/
I2CSlaveMSG TM1650;

/*定义一个AT24C02Msg结构体*/
I2CSlaveMSG AT24C02Msg;

/*定义一个指针数组，存放I2C结构体，这里包含TM1650和EEPROM*/
I2CSlaveMSG *PtrMsg[] = { &TM1650, &AT24C02Msg };

uint8_t SEG7Table[] = {
/*共阴极*/
/*对应在数码管上显示的数字为：0, 1, 2, 3,*/
0x3f, 0x06, 0x5b, 0x4f,
/*对应在数码管上显示的数字为：4, 5, 6, 7,*/
0x66, 0x6d, 0x7d, 0x07,
/*对应在数码管上显示的数字为：8, 9, a, b,*/
0x7f, 0x6f, 0x77, 0x7c,
/*对应在数码管上显示的数字为：c, d, e, f*/
0x39, 0x5e, 0x79, 0x71 };

Uint16 cnt;
Uint16 cnt_new;
Uint16 cnt_old;

/******************************************************************
 *函数名：void InitI2C_Gpio()
 *参 数：无
 *返回值：无
 *作 用：IO 初始化为IIC
 ******************************************************************/
void InitI2C_Gpio()
{
	EALLOW;

	/*SDAA(GPIO32)上拉*/
	SDAPUD = 0;
	/* IO 口*/
	SDAMUX = 1;
	/* 输出*/
	SDADIR = 1;
	/* 不同步*/
	SDAQSEL1 = 3;

	/*SCLA (GPIO33)上拉*/
	CLKPUD = 0;
	/* IO 口*/
	CLKMUX = 1;
	/* 输出*/
	CLKDIR = 1;
	/* 不同步*/
	CLKQSEL1 = 3;

	EDIS;
}

/*系统时钟为120MHZ*/
#define SYSCLK 120000000L

#define IICFRE 400000L

INTERRUPT void i2c_int1a_isr(void)
{
	GpioDataRegs.GPATOGGLE.bit.GPIO7=1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP8;
}

/******************************************************************
 *函数名：void I2CA_Init(void)
 *参 数：无
 *返回值：无
 *作 用：初始化IIC
 ******************************************************************/
void I2CA_Init(void)
{
	/*预分频*/
	I2caRegs.I2CPSC.all = 5;
	/*细分时钟*/
	I2caRegs.I2CCLKL = SYSCLK / (I2caRegs.I2CPSC.all + 1) / IICFRE / 2 - 5;
	I2caRegs.I2CCLKH = SYSCLK / (I2caRegs.I2CPSC.all + 1) / IICFRE / 2 - 5;

	/*IIC 复位*/
	I2caRegs.I2CMDR.bit.IRS=1;
	/*IIC FIFO使能*/
	I2caRegs.I2CFFTX.bit.I2CFFEN=1;
	/*IIC 发送FIFO使能*/
	I2caRegs.I2CFFTX.bit.TXFFRST=1;
	/*IIC 接收FIFO使能*/
	I2caRegs.I2CFFRX.bit.RXFFRST=1;
	/*IIC 接收FIFO中断清除*/
	I2caRegs.I2CFFRX.bit.RXFFINTCLR=1;
	/*IIC接收应答中断信号使能*/
	I2caRegs.I2CIER.bit.ARDY=1;
	/*IIC停止中断信号使能*/
	I2caRegs.I2CIER.bit.SCD=1;

	return;
}

#define DELAY_FOR 10

/******************************************************************
 *函数名：void softResetIIC_BUS()
 *参 数：无
 *返回值：无
 *作 用：配置IIC总线的时序
 ******************************************************************/
void softResetIIC_BUS()
{
	Uint16 i, j;

	InitI2C_Gpio();

	/*发送的时序*/
	/*SDA拉高*/
	SDA_H();
	for (j = 0; j < DELAY_FOR; j++);
	/*SCL拉高*/
	CLK_H();
	for (j = 0; j < DELAY_FOR; j++);
	SDA_L();
	for (j = 0; j < DELAY_FOR; j++);
	CLK_L();
	for (j = 0; j < DELAY_FOR; j++);
	SDA_H();
	for (j = 0; j < DELAY_FOR; j++);

	for (i = 0; i < 9; i++)
	{
		CLK_H();
		for (j = 0; j < DELAY_FOR; j++);
		CLK_L();
		for (j = 0; j < DELAY_FOR; j++);
	}

	/*接收的时序*/
	CLK_H();
	for (j = 0; j < DELAY_FOR; j++);
	SDA_L();
	for (j = 0; j < DELAY_FOR; j++);
	CLK_L();
	for (j = 0; j < DELAY_FOR; j++);
	CLK_H();
	for (j = 0; j < DELAY_FOR; j++);
	SDA_H();
	for (j = 0; j < DELAY_FOR; j++);
	CLK_L();
	for (j = 0; j < DELAY_FOR; j++);
}

/******************************************************************
 *函数名：Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
 *参 数：struct I2CSlaveMSG *msg
 *返回值：I2C_STP_NOT_READY_ERROR
 *             I2C_BUS_BUSY_ERROR
 *             I2C_SUCCESS
 *作 用：IIC总线的发送程序
 ******************************************************************/
Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
{
	Uint16 i;

	/*停止位=1，收到发送停止信号，返回“总线准备失败”*/
	if (I2caRegs.I2CMDR.bit.STP == 1)
	{
		return I2C_STP_NOT_READY_ERROR;
	}

	/*检测到“总线忙”状态，返回“I2C总线忙错误”*/
	if (I2caRegs.I2CSTR.bit.BB == 1)
	{
		return I2C_BUS_BUSY_ERROR;
	}

	/*读取I2C所访问的从机地址*/
	I2caRegs.I2CSAR = msg->SlavePHYAddress;

	/*向I2C模块发送数据，控制TM1650芯片*/
	for (i = 0; i < msg->Len; i++)
	{
		I2caRegs.I2CDXR = msg->MsgOutBuffer[i];
	}

	/* 定义I2C发送数据长度*/
	I2caRegs.I2CCNT = msg->Len;

	/*I2C模块复位*/
	I2caRegs.I2CMDR.bit.IRS=1;
	/*I2C主机模式*/
	I2caRegs.I2CMDR.bit.MST=1;
	/*I2C传输路径：发送*/
	I2caRegs.I2CMDR.bit.TRX=1;
	/*I2C起始信号延长*/
	I2caRegs.I2CMDR.bit.STB=1;
	/*I2C停止模式配置：自减到零自动发送停止*/
	I2caRegs.I2CMDR.bit.STP=1;
	/*I2C总线发送起始信号*/
	I2caRegs.I2CMDR.bit.STT=1;

	return I2C_SUCCESS;
}

/******************************************************************
 *函数名：Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
 *参 数：struct I2CSlaveMSG *msg
 *返回值：I2C_SUCCESS
 *作 用：读取EEPROM数据程序
 ******************************************************************/
Uint16 I2CA_Rxdata_STOP(struct I2CSlaveMSG *msg)
{
	I2caRegs.I2CSAR = msg->SlavePHYAddress;

	/*I2C数据长度定义*/
	I2caRegs.I2CCNT = msg->Len;

	/*master receiver*/
	/*I2C模块复位*/
	I2caRegs.I2CMDR.bit.IRS=1;
	/*I2C主机模式*/
	I2caRegs.I2CMDR.bit.MST=1;
	/*I2C传输路径：接收*/
	I2caRegs.I2CMDR.bit.TRX=0;
	/*I2C起始信号延长*/
	I2caRegs.I2CMDR.bit.STB=1;
	/*I2C停止模式配置：自减到零自动发送停止*/
	I2caRegs.I2CMDR.bit.STP=1;
	/*I2C总线发送起始信号*/
	I2caRegs.I2CMDR.bit.STT=1;

	return I2C_SUCCESS;
}

/******************************************************************
 *函数名：void TM1650_Send(char addr,char data)
 *参 数：无
 *返回值：无
 *作 用：通过访问EEPROM向芯片TM1650发送控制数码管显示程序
 ******************************************************************/
void TM1650_Send(char addr, char data)
{
	/*1个数据（不含物理地址）*/
	TM1650.Len = 1;

	/*EEPROM的从机地址*/
	TM1650.SlavePHYAddress = addr;

	/*向EEPROM发送要写入的数据*/
	TM1650.MsgOutBuffer[0] = data;

	/*I2C访问EEPROM写入数据，输出控制TM1650，带STOP*/
	I2CA_Tx_STOP(&TM1650);

	TM1650.IIC_TimerOUT = 0;

	/*主机状态为IIC_WRITE写入*/
	TM1650.MasterStatus = IIC_WRITE;

	/*模块检测到停止信号或复位时，停止发送数据*/
	while (I2caRegs.I2CSTR.bit.SCD == 0)
	{
		/*输出时长*/
		if (TM1650.IIC_TimerOUT > AT24CO2_TIMER_OUT)
		{
			softResetIIC_BUS();

			/*主机状态强制为IIC空闲*/
			TM1650.MasterStatus = IIC_IDLE;

			/*IIC发送停止*/
			I2caRegs.I2CMDR.bit.STP = 1;

			/*IIC状态置满，停止发送*/
			I2caRegs.I2CSTR.all = 0xFFFF;

			break;
		}
	}

	/*I2C总线发送停止*/
	I2caRegs.I2CSTR.all |= I2C_SCD_BIT;
}

/******************************************************************
 *函数名：void TM1650_Read(char addr, char *data)
 *参 数：char addr,
 *           char *data
 *返回值：无
 *作 用：通过读取EEPROM，可控制TM1650芯片扫描按键
 ******************************************************************/
void TM1650_Read(char addr, char *data)
{
	TM1650.Len = 1;

	TM1650.SlavePHYAddress = addr;

	/*读取EEPROM*/
	I2CA_Rxdata_STOP(&TM1650);

	TM1650.IIC_TimerOUT = 0;

	/*主机状态I2C读取IIC_READ_STEP2*/
	TM1650.MasterStatus = IIC_READ_STEP2;

	while (I2caRegs.I2CSTR.bit.SCD == 0)
	{
		if (TM1650.IIC_TimerOUT > AT24CO2_TIMER_OUT)
		{
			softResetIIC_BUS();
			TM1650.MasterStatus = IIC_IDLE;
			I2caRegs.I2CMDR.bit.STP = 1;
			I2caRegs.I2CSTR.all = 0xFFFF;
			//printf("\r\n Timer OUT in %d , Soft Reset",TM1650.MasterStatus);
			break;
		}
	}

	I2caRegs.I2CSTR.all |= I2C_SCD_BIT;
	TM1650.IIC_TimerOUT = 0;

	/*主机状态为I2C读取第二组数据IIC_READ_STEP3*/
	TM1650.MasterStatus = IIC_READ_STEP3;

	/*等待接收FIFO为空时，读取I2C模块接收的数据*/
	while (I2caRegs.I2CFFRX.bit.RXFFST)
	{
		*data = I2caRegs.I2CDRR;
	}
}
