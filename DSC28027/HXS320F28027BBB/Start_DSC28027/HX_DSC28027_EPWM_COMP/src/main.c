﻿/******************************************************************
 文 档 名 ：HX_DSC28027_EPWM_COMP
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Start_DSC28027_V1.1
                  Start_DSC28027PTT_Rev1.2
 D S P： DSC28027
 使 用 库：无
 作 用：比较器输出触发DC事件动作
 接线1：（1）按程序设置将Core_DSC28027_V1.4上3.3V输入端VREF
			与比较器输入引脚ADCINA4/AIO4(AA4)相连，
		（2）将比较器输出GPIO34与PWM TZ3-GPIO17相连，GPIO16-3.3V相连
 现象1：可观察到LED灭 TZ CBC与DC按优先级依次顺序触发
		（a）TZ的CBC事件触发 产生EPWM2A置高、EPWM2B置低动作；
		（b）DC数字比较超越阈值，LED2 D400常亮，触发产生EPWM3A置高，EPWM3B置低动作；
  接线2：更换GPIO16与GPIO17接线
   （1）GPIO16接比较器输出GPIO34，
   （2）GPIO17接3.3V
 现象2：在TZ事件触发后可观察到OST事件被触发，
   （1）LED3 D401常亮，产生EPWM1A置高，EPWM1B置低动作

 ----------------------例程使用说明-----------------------------
 *
 *            测试DC功能
 *
 *
 * 现象：
 版 本：V1.0.0
 时 间：2022年8月11日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "system.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();
	/*初始化TZ错误联防Gpio引脚配置*/
	InitTzGpio();
	/*初始化LED*/
	InitAdc();
	/*初始化GPIO，复用为comp功能*/
	InitComp2Gpio();
	/*初始化LED配置，用于指示DC数字比较错误联防动作*/
	InitLED();
	/*关中断*/
	InitPieCtrl();
	/*清中断*/
	IER=0x0000;
	IFR=0x0000;
	/*初始化PIE中断向量表*/
	InitPieVectTable();
	/*允许访问受保护的空间*/
	EALLOW;
	/*将epwm1_tz_isr入口地址赋给EPWM1_TZINT，执行OST单次触发*/
	PieVectTable.EPWM1_TZINT = &epwm1_tz_isr;
	/*将epwm2_tz_isr入口地址赋给EPWM2_TZINT，执行CBC周期触发*/
	PieVectTable.EPWM2_TZINT = &epwm2_tz_isr;
	/*将epwm3_tz_isr入口地址赋给EPWM3_TZINT，执行DC数字比较超越阈值触发*/
	PieVectTable.EPWM3_TZINT = &epwm3_tz_isr;
	/*将adc_isr入口地址赋给ADCINT3*/
	PieVectTable.ADCINT3 = &adc_isr;
	EDIS;

	EALLOW;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止。*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*	初始化EPWM1，错误联防配置为单次OST触发*/
	InitEpwm1_Example();
	/*	初始化EPWM2，错误联防配置为周期CBC触发*/
	InitEpwm2_Example();
	/*初始化EPWM3，错误联防配置为DC数字比较超越阈值触发*/
	InitEpwm3_Example();

	EALLOW;
	/*所有使能的ePWM模块同步使用TBCLK*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*初始化ADC*/
	ADC_Init();
	/*初始化comp2*/
	InitComp2();
	/*使能打开CPU IER相对应的中断*/
	IER|=M_INT2|M_INT10;

	/*使能打开PIE IER相对应的中断*/
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx3 = 1;

	PieCtrlRegs.PIEIER10.bit.INTx3 = 1;

	EINT;

	while (1)
	{
		/*判断CBC与DC错误联防的触发*/
		if(EPwm_CBC_flag>EPwm_DC_flag)
		{
			GpioDataRegs.GPASET.bit.GPIO7=1;    /*  D400  */
		}
		else
		{
			GpioDataRegs.GPACLEAR.bit.GPIO7=1;   /*  D400  */
		}
	}

	return 0;
}

// ----------------------------------------------------------------------------
