#include "system.h"

Uint16 Pwm1CMPA;
/******************************************************************
 *函数名：void EPWM1_Config(void)
 *参 数：无
 *返回值：无
 *作 用：配置epwm1
 ******************************************************************/
void EPWM1_Config(void)
{
	/*SysFreq*15000/SetSpeed*/
	EPwm1Regs.TBPRD = EPWM1_TBPRD;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））*/
	EPwm1Regs.TBPHS.half.TBPHS = 0;
	/*epwm时基计数器相位=0*/
	EPwm1Regs.TBCTR = 0;

	/*增减计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*从其影子寄存器中加载周期寄存器（TBPRD）*/
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	/*禁用EPWMxSYNCO信号*/
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
	/*时基时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*同步事件后向上计数*/
	EPwm1Regs.TBCTL.bit.PHSDIR = 0;
	/*自由运行*/
	EPwm1Regs.TBCTL.bit.FREE_SOFT = 0x2;

	/*设置CMPA值为Pwm1CMPA*/
	EPwm1Regs.CMPA.half.CMPA = Pwm1CMPA;
	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLA.bit.PRD = AQ_CLEAR;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLB.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLB.bit.CAU = AQ_SET;
}
