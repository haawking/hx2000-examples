#include "system.h"
/******************************************************************
 *函数名：void EQep1_Gpio(void)
 *参 数：无
 *返回值：无
 *作 用：初始化EQep1引脚
 ******************************************************************/
void EQep1_Gpio(void)
{
	EALLOW;
	/*使能GPIO12引脚内部上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;
	/*将GPIO12引脚配置为:EQEP1A*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 3;
	/*使能GPIO34引脚内部上拉*/
	GpioCtrlRegs.GPBPUD.bit.GPIO34 = 0;
	/*将GPIO34引脚配置为:EQEP1B*/
	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 3;

	/*使能GPIO16引脚内部上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;
	/*将GPIO34引脚配置为:EQEPI*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 2;

	/*使能GPIO17引脚内部上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;
	/*将GPIO17引脚配置为:EQEPS*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 2;
	EDIS;
}

void EQEP_pulseCap(void)
{
	EALLOW;
	/*用于存放计数寄存器的最大值0xFFFFFFFF*/
	EQep1Regs.QPOSMAX = 0xFFFFFFFF;
	/*单位周期设置为SysFreq*100*/
	EQep1Regs.QUPRD = SysFreq * 100;

	/*方向模式输入，QCLK = xCLK, QDIR = xDIR*/
	EQep1Regs.QDECCTL.bit.QSRC = 1;
	/*仅在上升沿计数；*/
	EQep1Regs.QDECCTL.bit.XCR = 1;

	/*位置计数器、看门狗计数器、单元时间计数器、捕获计数器不受影响；*/
	EQep1Regs.QEPCTL.bit.FREE_SOFT = 3;
	/*位置计数器在单元时间事件复位*/
	EQep1Regs.QEPCTL.bit.PCRM = 3;
	/*位置计数器使能*/
	EQep1Regs.QEPCTL.bit.QPEN = 1;
	/*单位超时时锁存*/
	EQep1Regs.QEPCTL.bit.QCLM = 1;
	/*单元时间中断时能*/
	EQep1Regs.QEPCTL.bit.UTE = 1;
	EDIS;
}

void EQEP_MSpeed(void)
{
	EALLOW;
	//用于存放计数寄存器的最大值0xFFFFFFFF
	EQep1Regs.QPOSMAX = 0xFFFFFFFF;
	//单位周期设置为SysFreq*100
	EQep1Regs.QUPRD = SysFreq * 100;

	//正交模式输入QCLK = iCLK, QDIR = iDIR
	EQep1Regs.QDECCTL.bit.QSRC = 0;
	//在上升沿下降沿均计数
	EQep1Regs.QDECCTL.bit.XCR = 0;

	//位置计数器、看门狗计数器、单元时间计数器、捕获计数器不受影响；
	EQep1Regs.QEPCTL.bit.FREE_SOFT = 3;
	//位置计数器在单元时间事件复位
	EQep1Regs.QEPCTL.bit.PCRM = 3;
	//位置计数器使能
	EQep1Regs.QEPCTL.bit.QPEN = 1;
	//单位超时时锁存
	EQep1Regs.QEPCTL.bit.QCLM = 1;
	//单元时间中断时能
	EQep1Regs.QEPCTL.bit.UTE = 1;
	EDIS;
}

void EQEP_TSpeed_Angle(void)
{
	EALLOW;
	/*用于存放计数寄存器的最大值4000*/
	EQep1Regs.QPOSMAX = 4000;
	/*单位周期设置为6000*/
	EQep1Regs.QUPRD = 6000;

	/*正交模式输入QCLK = iCLK, QDIR = iDIR*/
	EQep1Regs.QDECCTL.bit.QSRC = 0;
	/*在上升沿下降沿均计数*/
	EQep1Regs.QDECCTL.bit.XCR = 0;
	/*位置计数器、看门狗计数器、单元时间计数器、捕获计数器不受影响；*/
	EQep1Regs.QEPCTL.bit.FREE_SOFT = 3;
	/*位置计数器在单元时间事件复位*/
	EQep1Regs.QEPCTL.bit.PCRM = 3;
	/*位置计数器使能*/
	EQep1Regs.QEPCTL.bit.QPEN = 1;
	/*单位超时时锁存*/
	EQep1Regs.QEPCTL.bit.QCLM = 1;
	/*单元时间中断时能*/
	EQep1Regs.QEPCTL.bit.UTE = 1;

	/*单元位置事件分频QCLK/2*/
	EQep1Regs.QCAPCTL.bit.UPPS = 1;
	/*捕获时钟的预分频控制 CAPCLK = SYSCLKOUT/2*/
	EQep1Regs.QCAPCTL.bit.CCPS = 1;
	/*使能捕获*/
	EQep1Regs.QCAPCTL.bit.CEN = 1;
	EDIS;
}
