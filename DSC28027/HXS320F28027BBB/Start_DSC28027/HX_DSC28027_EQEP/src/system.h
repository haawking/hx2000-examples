#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"
#include "IQmathLib.h"

/*EPwm_motor*/
void EPWM1_Config(void);

#define EPWM1_TBPRD SysFreq*15000/SetSpeed
extern Uint16 Pwm1CMPA;
extern Uint16 Pwm1CMPB;

/*EQep_PulseCap */
void EQEP_pulseCap(void);
void EQEP_MSpeed(void);
void EQEP_TSpeed_Angle(void);

void EQep1_Gpio(void);
void EQEP_PulseCap_cal();
void EQEP_MSpeed_cal(void);
void EQEP_TSpeed_Angle_cal(void);
void InitLED(void);

#define SysFreq 120//MHz

/*xint_isr_EQep*/
extern _iq PulseNum;
extern Uint16 PulseDir;
extern _iq CapNum;
extern float MotorSpeed;
extern _iq MotorDir;
extern float MotorAngle;
extern float SetSpeed;

#define _FLASH      1
#define CW 			1
#define CCW			-1

#endif/*SYSTEM_H_*/
