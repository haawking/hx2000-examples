#include "xint.h"
#include "system.h"

uint16 xint2counter;
float SetSpeed;
/******************************************************************
 *函数名：void XINT2_Init(void)
 *参 数：无
 *返回值：无
 *作 用：XINT2初始化
 ******************************************************************/
void XINT2_Init(void)
{
	EALLOW;
	/*选择GPIO5引脚作为XINT2中断源*/
	GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = 5;
	/*上升沿产生的中断 (低到高过渡)*/
	XIntruptRegs.XINT2CR.bit.POLARITY = 1;
	/*使能中断*/
	XIntruptRegs.XINT2CR.bit.ENABLE = 1;
	EDIS;
}
/******************************************************************
 *函数名：void INTERRUPT xint2_isr(void)
 *参 数：无
 *返回值：无
 *作 用：XINT2中断服务函数
 ******************************************************************/
void INTERRUPT xint2_isr(void)
{
	xint2counter++;

	if(xint2counter < 5)
	{
		EQEP_PulseCap_cal();
	}
	else if(xint2counter < 10)
	{
		EQEP_MSpeed();
		EQEP_MSpeed_cal();
	}
	else
	{
		SetSpeed = 60;
		Pwm1CMPA = SysFreq * 7500 / SetSpeed;
		EPwm1Regs.TBPRD = SysFreq * 15000 / SetSpeed;
		EPwm1Regs.CMPA.half.CMPA = Pwm1CMPA;

		EQEP_TSpeed_Angle();
		EQEP_TSpeed_Angle_cal();
	}

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
