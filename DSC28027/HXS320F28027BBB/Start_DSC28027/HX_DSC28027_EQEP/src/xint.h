#ifndef XINT_H_
#define XINT_H_

#include "dsc_config.h"

void XINT2_Init(void);
void INTERRUPT xint2_isr(void);

extern uint16 xint2counter;

#endif/*ECAP_H_*/
