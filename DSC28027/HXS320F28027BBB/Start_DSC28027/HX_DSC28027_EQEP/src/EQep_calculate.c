#include "system.h"

_iq PulseNum;
Uint16 PulseDir;
_iq CapNum;
float MotorSpeed;
_iq MotorDir;
float MotorAngle;

void EQEP_PulseCap_cal(void)
{
	PulseNum = EQep1Regs.QPOSLAT;
	PulseDir = EQep1Regs.QEPSTS.bit.QDF;
}

void EQEP_MSpeed_cal(void)
{
	PulseNum = EQep1Regs.QPOSLAT;
	PulseDir = EQep1Regs.QEPSTS.bit.QDF;

	if(PulseDir == 1)
	{
		MotorDir = CW;
	}
	else
	{
		MotorDir = CCW;
		PulseNum = 0xFFFFFFFF - PulseNum;
	}

	MotorSpeed = MotorDir * PulseNum * 5000 * SysFreq / 4000;
}

void EQEP_TSpeed_Angle_cal(void)
{
	PulseNum = EQep1Regs.QPOSLAT;
	PulseDir = EQep1Regs.QEPSTS.bit.QDF;
	CapNum = EQep1Regs.QCPRDLAT;

	if(PulseDir == 1)
	{
		MotorDir = CW;
	}
	else
	{
		MotorDir = CCW;
		PulseNum = 4000 - PulseNum;
	}

	MotorSpeed = MotorDir * SysFreq * 500 * SysFreq / (4 * CapNum);
	MotorAngle = PulseNum * 360 / 4000;
}
