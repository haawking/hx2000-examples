#include "ecap.h"

uint16 ecapcounter;
/******************************************************************
 *函数名：void ECap_Gpio()
 *参 数：无
 *返回值：无
 *作 用：配置ECap引脚
 ******************************************************************/
void ECap_Gpio(void)
{
	EALLOW;
	/*将GPIO19引脚配置为:ECAP1-eCAP1 (输入/输出)*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 3;
	/*GPIO19使能上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;
	/*使用3采样*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO19 = 1;
	EDIS;
}
/******************************************************************
 *函数名：void ECap_Init()
 *参 数：无
 *返回值：无
 *作 用：初始化ECap
 ******************************************************************/
void ECap_Init(void)
{
	EALLOW;
	/*ECAP模块在APWM模式下工作*/
	ECap1Regs.ECCTL2.bit.CAP_APWM = 1;
	ECap1Regs.CAP1 = 0x01312D00;
	ECap1Regs.CAP2 = 0x00989680;

	/*清除ECCLR各标志位*/
	ECap1Regs.ECCLR.all = 0x0FF;
	/*使能Compare Equal作为中断源*/
	ECap1Regs.ECEINT.bit.CTR_EQ_CMP = 1;
	/*TSCTR自由运行*/
	ECap1Regs.ECCTL2.bit.TSCTRSTOP = 1;
	EDIS;
}
