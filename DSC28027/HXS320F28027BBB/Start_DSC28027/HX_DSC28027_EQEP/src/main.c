/******************************************************************
 文 档 名 ：HX_DSC28027_EQEP
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Start_DSC28027_V1.1
                  Start_DSC28027PTT_Rev1.2
 D S P： DSC28027
 使 用 库：无
 作 用：EQEP测速
 说 明： (1)通过epwm1输出SysFreq*15000*2/SetSpeed个TBCLK周期，
               占空比为SysFreq*7500/SetSpeed，波形(向上向下计数模式）

       (2)通过EQEP实现：a）b）两种功能下，设置转速 6000  c）功能下 设置转速 60
       a)脉冲捕获：采用最大编码值捕获，单位周期为SysFreq*100，编码器方向模式
              输入，仅在上升沿计数，仿真模式不受影响，位置计数器在单位时间事件复位，
              单位超时时锁存。
	   b)M法测速：采用最大编码值捕获，单位周期为SysFreq*100，编码器正交模式输入，
	     在上升沿下降沿均计数，仿真模式不受影响，位置计数器在单位时间事件复位，
	     单位超时时锁存。
	   c)T法测速：采用最大编码值4000，单位周期6000，编码器正交模式输入，
	     在上升沿下降沿均计数，仿真模式不受影响，位置计数器在单位时间事件复位，
	     单位超时时锁存，捕获控制：单位位置事件1分频，捕获时钟预分频1分频。

      (3) 通过ecap_apwm在GPIO19产生单路3.3Hz频率、占空比0.5的PWM波触发xint2（GPIO5）
             外部中断，并通过xint2中断计数选择实现三种不同的功能切换：xint2计数<5，
             进行脉冲捕获；5<xint2计数<10，M法测速；10<xint2计数<15

 ----------------------例程使用说明-----------------------------
 *
 *            GPIO0接GPIO12，GPIO1接GPIO34，GPIO5接GPIO19
 *
 *
 * 现象：LED3 D401经过一段时间后闪烁。
版本：V1.0.0
 时 间：2022年8月11日
 作 者：heyang
 @mail：support@mail.haawking.com

 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "system.h"
#include "ecap.h"
#include "xint.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为ECap功能*/
	ECap_Gpio();
	/*初始化中断XINT2*/
	XINT2_Init();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	/*EQEP1引脚配置*/
	
	EQep1_Gpio();
	/*初始化LED配置*/
	InitLED();
	/*关中断*/
	DINT;
	/*初始化pie中断控制*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;

	EALLOW;
	/*将xint2_isr入口地址赋给XINT2*/
	PieVectTable.XINT2 = &xint2_isr;
	EDIS;
	/*初始化ECAP*/
	ECap_Init();
	/*设置电机转速*/
	SetSpeed = 6000;
	/*设置占空比为50%*/
	Pwm1CMPA = SysFreq * 15000 / SetSpeed;

	EALLOW;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*配置epwm1*/
	EPWM1_Config();

	EALLOW;
	/*所有使能的ePWM模块时钟都是在TBCLK的第一个上升沿对齐的情况下开始的*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	EQEP_pulseCap();
	/*参数初始化*/
	PulseNum = 0;
	PulseDir = 0;
	MotorSpeed = 0;
	/*使能M_INT1*/
	IER |= M_INT1;

	PieCtrlRegs.PIEIER1.bit.INTx5 = 1;

	EINT;

	while(1)
	{
		/*电机转速达到给定转速时*/
		if(MotorSpeed == SetSpeed)
		{
			/*GPIO6置0*/
			GpioDataRegs.GPACLEAR.bit.GPIO6 = 1;
		}
		else
		{
			/*GPIO6置1*/
			GpioDataRegs.GPASET.bit.GPIO6 = 1;
		}
	}

	return 0;
}

// ----------------------------------------------------------------------------
