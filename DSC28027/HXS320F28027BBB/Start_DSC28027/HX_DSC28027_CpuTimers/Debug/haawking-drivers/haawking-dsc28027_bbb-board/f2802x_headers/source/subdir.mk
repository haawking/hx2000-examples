################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/source/F2802x_GlobalVariableDefs.c 

OBJS += \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/source/F2802x_GlobalVariableDefs.o 

C_DEPS += \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/source/F2802x_GlobalVariableDefs.d 


# Each subdirectory must supply rules for building sources it contributes
haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/source/%.o: ../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -T DSC28027_BBB_link_RAM.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_1_9_2=0 -DDEBUG -DDSC28027_BBB -DHAAWKING_DSC28027_BOARD -I"../haawking-drivers/haawking-dsc28027_bbb-board/common" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/include" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


