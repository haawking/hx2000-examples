/******************************************************************
 文 档 名 ：HX_DSC28027_IIC_TM1650
 开 发 环 境：Haawking IDE V1.8.2
 开 发 板 ：DSC28027开发板
 D S P： DSC28027
 使 用 库：无
 作 用：IIC控制TM1650数码管驱动和扫描按键S101 S102 S103
 说 明：RAM 工程
 ----------------------例程使用说明-----------------------------
 *
 *  通过IIC读写EEPROM，控制TM1650芯片的数字循环显示，
 *  清零、扫描按键输入控制显示与调节亮度执行主程序调用
 * InitI2C_Gpio();     //IO 初始化为IIC
 * I2CA_Init();          // IIC初始化，100KHz
 * TM1650_Send()单个数码管显示函数
 * TM1650_Read()读取TM1650扫描值，获取按键S101 S102 S103的状态
 *
 * 现象：左二到左四数码管循环显示0-999，D401闪烁
 *           按下按键S101 左1数码管显示数字+1
 *           按下按键S102 改变数码管显示亮度
 *           按下按键S103 四个数码管清零，D400翻转

 版本：V0.0.3
 时 间：2021年9月1日
 作 者：LiuQi
 @ mail：qi.liu@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "TM1650_IIC.h"

void InitLED(void);
void Timer0_init();
INTERRUPT void cpu_timer0_isr(void);

Uint16 Tmp = 0;
unsigned char keyReg = 0;

int main(void)
{
	/*初始化系统控制*/
	InitSysCtrl();

	/*初始化LED*/
	InitLED();

	/* 清除所有中断和初始化PIE向量表*/
	/*禁用CPU中断*/
	DINT;
	/*初始化PIE控制寄存器到默认状态，默认状态是全部PIE中断被禁用和标志位被清除*/
	InitPieCtrl();
	/*禁用CPU中断和清除所有CPU中断标志位*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE中断向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();

	/*定时器0初始化*/
	Timer0_init();

	/*初始化IIC总线的Gpio*/
	InitI2C_Gpio();
	/*IIC初始化*/
	I2CA_Init();
	/*BIT6到BIT4为亮度调节，BIT0是  1 开启/0关闭*/
	LigntVal = 0x11;
	/*1级亮度，开启显示*/
	TM1650_Send(CMD_SEG, LigntVal);
	/*DIG0-DIG3均显示0*/
	TM1650_Send(DIG0, SEG7Table[0]);
	TM1650_Send(DIG1, SEG7Table[0]);
	TM1650_Send(DIG2, SEG7Table[0]);
	TM1650_Send(DIG3, SEG7Table[0]);

	/*使能全局中断*/
	EINT;
	/*使能调试事件*/

	while(1)
	{
		/*间隔为360ms*/
		if(CpuTimer0.InterruptCount >= 360)
		{
			/*定时器0中断次数清零*/
			CpuTimer0.InterruptCount = 0;

			/*D401翻转*/
			GpioDataRegs.GPATOGGLE.bit.GPIO6 = 1;

			/*Tmp累加，从0-999循环*/
			Tmp++;
			if(Tmp > 999)
				Tmp = 0;

			/*DIG1显示Tmp的百位*/
			TM1650_Send(DIG1, SEG7Table[Tmp / 100]);
			/*DIG2显示Tmp的十位*/
			TM1650_Send(DIG2, SEG7Table[(Tmp / 10) % 10]);
			/*DIG3显示Tmp的个位*/
			TM1650_Send(DIG3, SEG7Table[Tmp % 10]);

			/*TM1650获取扫描按键S101 S102 S103，控制数码管数字显示及亮度调节*/
			TM1650_Read(CMD_KEY, &keyVal);

			/*S101按下时，DIG0显示内容+1*/
			if(keyVal == 0x44)
			{
				keyReg++;
				keyReg &= 0x0f;
				TM1650_Send(DIG0, SEG7Table[keyReg]);
			}

			/*S102按下时，改变数码管显示亮度*/
			if(keyVal == 0x4c)  //
			{
				/*高四位为亮度调节，最后1位是开启、关闭显示*/
				LigntVal = (LigntVal + 0x20) & 0x7F;

				/*1级亮度，开启显示*/
				TM1650_Send(CMD_SEG, LigntVal);
			}

			/*按下S103时*/
			if(keyVal == 0x54)
			{
				Tmp = 0;
				keyReg = 0;

				/*DIG0-DIG3清零*/
				TM1650_Send(DIG0, SEG7Table[0]);
				TM1650_Send(DIG1, SEG7Table[0]);
				TM1650_Send(DIG2, SEG7Table[0]);
				TM1650_Send(DIG3, SEG7Table[0]);

				/*D400翻转*/
				GpioDataRegs.GPATOGGLE.bit.GPIO7 = 1;
			}
		}
	}
}

// ----------------------------------------------------------------------------

/******************************************************************
 *函数名：void InitLED(void)
 *参 数：无
 *返回值：无
 *作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的寄存器*/
	EALLOW;

	/*GPIO7配置为通用IO*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;
	/*GPIO7配置为输出*/
	GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;

	/*GPIO6配置为通用IO*/
	GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;
	/*GPIO6配置为输出*/
	GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;

	/*禁止访问受保护的寄存器*/
	EDIS;
}

/******************************************************************
 *函数名：void Timer0_init()
 *参 数：无
 *返回值：无
 *作 用：定时器0初始化，1ms周期
 ******************************************************************/
void Timer0_init()
{
	/* 初始化CPU定时器*/
	InitCpuTimers();

	ConfigCpuTimer(&CpuTimer0, 120, 1000);

	/*使能定时器0中断*/
	CpuTimer0Regs.TCR.bit.TIE = 1;
	/*打开或重置定时器0*/
	CpuTimer0Regs.TCR.bit.TSS = 0;

	/*定时器0的中断入口地址为中断向量表的INT0*/
	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	EDIS;

	/*使能CPU的第一组中断*/
	IER |= M_INT1;
	/*使能PIE级的第一组中断的第七个小中断，即定时器0中断*/
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
}

/******************************************************************
 *函数名：INTERRUPT void cpu_timer0_isr(void)
 *参 数：无
 *返回值：无
 *作 用：定时器0中断服务函数
 ******************************************************************/
INTERRUPT void cpu_timer0_isr(void)
{
	/*定时器0中断次数累计*/
	CpuTimer0.InterruptCount++;

	/*检测IIC模块状态为空闲还是写入*/
	unsigned char i;

	for(i = 0; i < IIC_NODE_NUM; i++)
	{
		PtrMsg[i]->IIC_TimerOUT = (PtrMsg[i]->MasterStatus == IIC_IDLE) ? 0 : (PtrMsg[i]->IIC_TimerOUT + 1);
	}

	/*通知可以接收第一组中断的所有中断*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
