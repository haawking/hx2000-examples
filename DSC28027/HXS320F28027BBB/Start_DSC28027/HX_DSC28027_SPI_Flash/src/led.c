/******************************************************************
 文 档 名：     led.c
 D S P：       DSC28034
 使 用 库：     
 作     用：      
 说     明：      提供led.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2021年11月13日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;
	GpioDataRegs.GPASET.bit.GPIO6 = 1;

	GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;
	GpioDataRegs.GPASET.bit.GPIO7 = 1;
	EDIS;
}
