//###########################################################################
//
// FILE:   f2802x_otp.c
//
// TITLE:  F2802x otp file.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.1.0 $
// $Release Date: 2022-07-11 09:44:07 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#include "F2802x_Device.h"     //  Headerfile Include File

/***************************************************************
  To  uncomment ,you can use OTP
*****************************************************************/



/***************************************************************

volatile Uint32   CODE_SECTION(".User_Otp_Table") OTP_DATA[] = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};

*****************************************************************/



