#include "epwm.h"
/******************************************************************
 *函数名：void InitEpwm1_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm1
 ******************************************************************/
void InitEPwm1Example()
{
	/* Period = 4000*TBCLK counts*/
	EPwm1Regs.TBPRD = 2000;
	/*epwm时基计数器相位=0*/
	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器TBPRD（活 动）←TBPRD（影子）*/
	EPwm1Regs.TBCTR = 0x0000;

	/*设置CMPA值为EPWM1_MAX_CMPA*/
	EPwm1Regs.CMPA.half.CMPA = 1000;
	/*设置CMPB值为EPWM1_MAX_CMPB*/
	EPwm1Regs.CMPB = 0;

	/*增减计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPB主寄存器从映射寄存器中加载数据*/
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLB.bit.CBD = AQ_SET;

	/*使能双边沿延时死区*/
	EPwm1Regs.DBCTL.bit.OUT_MODE=DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm1Regs.DBCTL.bit.POLSEL=DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm1Regs.DBCTL.bit.IN_MODE=DBA_ALL;
	/* 死区上升沿延时50TBCLK*/
    EPwm1Regs.DBRED = 50;
    /*死区下降沿延时50TBCLK*/
    EPwm1Regs.DBFED = 50;

	EALLOW;
	/* 使能TZ3作为ePWM的单次故障保护触发输入*/
	EPwm1Regs.TZSEL.bit.OSHT3 = 1;
	/*当外部触发事件发生时，强制EPWMxA为高电平*/
	EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_HI;
	/*当外部触发事件发生时，强制EPWMxA为低电平*/
	EPwm1Regs.TZCTL.bit.TZB = TZ_FORCE_LO;
	/*允许单次触发事件产生中断*/
	EPwm1Regs.TZEINT.bit.OST = 1;
	EDIS;
}
/******************************************************************
 *函数名：void InitEpwm2_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm2
 ******************************************************************/
void InitEPwm2Example()
{
	/* Period = 4000*TBCLK counts*/
	EPwm2Regs.TBPRD = 2000;
	/*epwm时基计数器相位=0*/
	EPwm2Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））*/
	EPwm2Regs.TBCTR = 0x0000;

	/*设置CMPA值为EPWM2_MIN_CMPA*/
	EPwm2Regs.CMPA.half.CMPA = 1000;
	/*设置CMPB值为EPWM2_MAX_CMPB*/
	EPwm2Regs.CMPB = 0;

	/*增减计数模式*/
	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm2Regs.AQCTLB.bit.CBD = AQ_SET;

	/*使能双边沿延时死区*/
	EPwm2Regs.DBCTL.bit.OUT_MODE=DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm2Regs.DBCTL.bit.POLSEL=DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm2Regs.DBCTL.bit.IN_MODE=DBA_ALL;
	/* 死区上升沿延时50TBCLK*/
    EPwm2Regs.DBRED = 50;
    /*死区下降沿延时50TBCLK*/
    EPwm2Regs.DBFED = 50;

	EALLOW;
	/*启用 TZ2 作为此 ePWM 模块的 CBC 跳闸源*/
	EPwm2Regs.TZSEL.bit.CBC2 = 1;
	/*发生跳闸事件时,强制 EPWMxA 进入高电平*/
	EPwm2Regs.TZCTL.bit.TZA = TZ_FORCE_HI;
	/*发生跳闸事件时,将 EPWMxB 强制为低电平*/
	EPwm2Regs.TZCTL.bit.TZB = TZ_FORCE_LO;
	/*跳闸区域逐周期中断使能*/
	EPwm2Regs.TZEINT.bit.CBC = 1;
	EDIS;
}
