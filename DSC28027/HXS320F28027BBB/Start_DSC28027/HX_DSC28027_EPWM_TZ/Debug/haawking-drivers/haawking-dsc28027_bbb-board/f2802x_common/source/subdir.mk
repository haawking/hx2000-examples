################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_adc.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_comp.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_cputimers.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_csmpasswords.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_defaultisr.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_ecap.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_epwm.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_eqep.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_gpio.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_i2c.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_otp.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_piectrl.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_pievect.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sci.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_spi.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sysctrl.c \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_tempsensorconv.c 

S_UPPER_SRCS += \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/_f2802x_usdelay.S \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_set.S \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_unset.S \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_set.S \
../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_unset.S 

OBJS += \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/_f2802x_usdelay.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_adc.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_comp.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_cputimers.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_csmpasswords.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_defaultisr.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_ecap.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_epwm.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_eqep.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_gpio.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_i2c.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_otp.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_piectrl.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_pievect.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sci.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_spi.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sysctrl.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_tempsensorconv.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_set.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_unset.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_set.o \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_unset.o 

S_UPPER_DEPS += \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/_f2802x_usdelay.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_set.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ier_unset.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_set.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/ifr_unset.d 

C_DEPS += \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_adc.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_comp.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_cputimers.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_csmpasswords.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_defaultisr.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_ecap.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_epwm.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_eqep.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_gpio.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_i2c.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_otp.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_piectrl.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_pievect.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sci.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_spi.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_sysctrl.d \
./haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/f2802x_tempsensorconv.d 


# Each subdirectory must supply rules for building sources it contributes
haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/%.o: ../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross Assembler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T DSC28027_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_1_9_2=0 -x assembler-with-cpp -DDEBUG -DDSC28027_BBB -DHAAWKING_DSC28027_BOARD -I"../haawking-drivers/haawking-dsc28027_bbb-board/common" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/include" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/%.o: ../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-haawking-elf-gcc -march=rv32imc -D__RUNNING_IN_FLASH_ -T DSC28027_BBB_link_FLASH.ld -mabi=ilp32 -mcmodel=medlow -mno-save-restore --target=riscv32-unknown-elf --sysroot="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc/riscv64-unknown-elf" --gcc-toolchain="D:/haawking/soft/Haawking-IDE-win64-V2.0.0/haawking-tools/compiler/riscv-tc-gcc" -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-inline-functions -Wall -Wextra  -g3 -Wl,--defsym,IDE_VERSION_1_9_2=0 -DDEBUG -DDSC28027_BBB -DHAAWKING_DSC28027_BOARD -I"../haawking-drivers/haawking-dsc28027_bbb-board/common" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_headers/include" -I"../haawking-drivers/haawking-dsc28027_bbb-board/f2802x_common/include" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


