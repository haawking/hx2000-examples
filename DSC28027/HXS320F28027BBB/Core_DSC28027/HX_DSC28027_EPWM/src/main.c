﻿/******************************************************************
 文 档 名 ：HX_DSC28027_PWM
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
                  Start_DSC28027_V1.1
                  Start_DSC28027PTT_Rev1.2
 D S P： DSC28027
 使 用 库：无
 作 用：pwm波生成
 说 明：采用ePWM模块编程实现输出周期在2.2ms，
 * 采用事件触发中断脉冲(脉冲计数0-10循环）
 * 实现高电平在55us到2145us变化的PWM波，TBCTR采用向上向下计数；
 ----------------------例程使用说明-----------------------------
 *
 *            EPWM发波，事件功能测试
 *            GPIO0 GPIO1  GPIO2 GPIO3 GPIO4 GPIO5引脚
 *            用示波器可以看到PWM波形
 * 
 版 本：V1.0.0
 时 间：2022年8月11日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

 
#include <stdio.h>
#include "dsc_config.h"
#include "hx_rv32_dev.h"
#include "hx_rv32_type.h"
#include <syscalls.h>
#include "xcustom.h"
#include "IQmathLib.h"
#include "epwm.h"



int main(void)
{
	/*系统初始化控制*/
	InitSysCtrl();
	/*EPWM外设的GPIO配置*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();
	/*关中断*/
	InitPieCtrl();
	/*清中断*/
	IER_DISABLE(0xffff);
	IFR_DISABLE(0xffff);
	/*初始化中断向量表*/
	InitPieVectTable();
	/*EPWM_INT中断向量表地址指向执行相应的EPWM中断服务程序*/
	EALLOW;
	PieVectTable.EPWM1_INT=&epmw1_isr;
	PieVectTable.EPWM2_INT=&epmw2_isr;
	PieVectTable.EPWM3_INT=&epmw3_isr;
	EDIS;
	/*禁止EPWM的时基使能,此时允许进行EPWM初始化配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=0;
	EDIS;
	/*EPWM模块初始化配置*/
	InitEPwm1Example();
	InitEPwm2Example();
	InitEPwm3Example();
	/*EPWM的时基使能,此时EPWM的配置功能将开始起作用*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC=1;
	EDIS;
	/*PIEIER第3组中断向量使能*/
	IER_ENABLE(M_INT3);
	/*PIEIER第3组第1到3个中断向量使能*/
	PieCtrlRegs.PIEIER3.bit.INTx1=1;
	PieCtrlRegs.PIEIER3.bit.INTx2=1;
	PieCtrlRegs.PIEIER3.bit.INTx3=1;
	/*打开全局中断*/
	EINT;

    while(1){

    }


	return 0;
}

// ----------------------------------------------------------------------------

