#include "system.h"
/******************************************************************
 *函数名：void InitEpwm2_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm2
 ******************************************************************/
void InitEpwm2_Example(void)
{
	/* Period = 4000*TBCLK counts*/
	EPwm2Regs.TBPRD = 3000;
	/*epwm时基计数器相位=0*/
	EPwm2Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））。*/
	EPwm2Regs.TBCTR = 0x0000;

	/*设置CMPA值为EPWM2_MAX_CMPA*/
	EPwm2Regs.CMPA.half.CMPA = 2000;
	/*设置CMPB值为EPWM2_MAX_CMPB*/
	EPwm2Regs.CMPB = 1000;

	/*增减计数模式*/
	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPB主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*当时间基准计数器的值等于CMPA的值，且正在减计数时，使EPWMxA输出低电平*/
	EPwm2Regs.AQCTLA.bit.CAD=AQ_CLEAR;
	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxA输出高电平*/
	EPwm2Regs.AQCTLA.bit.CAU=AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm2Regs.AQCTLB.bit.CBD=AQ_CLEAR;
	/*当时间基准计数器的值等于CMPB的值，且正在增计数时，使EPWMxB输出高电平*/
	EPwm2Regs.AQCTLB.bit.CBU=AQ_SET;

	/*时基计数器等于周期时生成（TBCTR = TBPRD）*/
	EPwm2Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;
	/*使能 EPWMxSOCA 脉冲*/
	EPwm2Regs.ETSEL.bit.SOCAEN = 1;
	/*在第一个事件上生成EPWMxSOCA脉冲*/
	EPwm2Regs.ETPS.bit.SOCAPRD = ET_1ST;

	EALLOW;
	/*设置TZ3为DCAH输入源*/
	EPwm2Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TZ2;
	/*设置TZ2为DCAL输入源*/
	EPwm2Regs.DCTRIPSEL.bit.DCALCOMPSEL = DC_TZ3;
	/*DCAL =高，DCAH =低*/
	EPwm2Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCAL_LOW;

	EPwm2Regs.DCFCTL.bit.SRCSEL=DC_SRC_DCAEVT2;
	/*DCAEVT2进行滤波*/
	EPwm2Regs.DCACTL.bit.EVT2SRCSEL = DC_EVT_FLT;
	/*DCAEVT2源是异步信号*/
	EPwm2Regs.DCACTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC;

	EPwm2Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_TZ2;
	/*设置TZ2为DCAL输入源*/
	EPwm2Regs.DCTRIPSEL.bit.DCBLCOMPSEL = DC_TZ3;
	/*DCAL =高，DCAH =低*/
	EPwm2Regs.TZDCSEL.bit.DCBEVT1 = TZ_DCAL_LOW;

	EPwm2Regs.DCFCTL.bit.SRCSEL=DC_SRC_DCBEVT1;
	/*DCAEVT2进行滤波*/
	EPwm2Regs.DCBCTL.bit.EVT1SRCSEL = DC_EVT1;
	/*DCBEVT1源是异步信号*/
	EPwm2Regs.DCBCTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;

	/*信号空窗过滤从CTR=0时刻开始偏移*/
	EPwm2Regs.DCFCTL.bit.PULSESEL=DC_PULSESEL_ZERO;
	/*从CTR=0时偏移500TBCLK后产生空窗*/
	EPwm2Regs.DCFOFFSET=500;
	/*空窗长度200TBCLK*/
	EPwm2Regs.DCFWINDOW=200;
	/*空窗使能*/
	EPwm2Regs.DCFCTL.bit.BLANKE=DC_BLANK_ENABLE;
	EPwm2Regs.DCFCTL.bit.BLANKINV=DC_BLANK_NOTINV;

	/*数字比较输出A事件2,强制EPWMxA进入高电平状态*/
	EPwm2Regs.TZCTL.bit.DCAEVT2 = TZ_FORCE_HI;

	/*数字比较输出A事件2,强制EPWMxA进入高电平状态*/
	EPwm2Regs.TZCTL.bit.DCBEVT1= TZ_FORCE_LO;
	/*数字比较器输出A事件2中断使能*/
	EPwm2Regs.TZEINT.bit.DCAEVT2 = 1;
	EPwm2Regs.TZEINT.bit.DCBEVT1 = 1;
	EDIS;
}
