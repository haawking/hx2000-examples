//###########################################################################
//
// FILE:    f2802x_csmpasswords.c
//
// TITLE:    F2802x Code Security Module Passwords.
// 
// DESCRIPTION:
//
//         This file is used to specify password values to
//         program into the CSM password locations in Flash
//         at 0x7DFFF0 - 0x7DFFFF.
//
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.1.0 $
// $Release Date: 2022-07-11 09:44:07 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################



#include "F2802x_Device.h"     //  Headerfile Include File
/**********************************************
password file
*****************************************************************/

volatile struct CSM_PWL  CODE_SECTION(".CsmPwl") CsmPwl = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};



