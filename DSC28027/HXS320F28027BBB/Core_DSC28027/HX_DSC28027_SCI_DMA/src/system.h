/******************************************************************
 文 档 名：     system.h
 D S P：       DSC28027
 使 用 库：     
 作     用：      
 说     明：      提供system.h接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2021年11月15日
 作 者：何洋
 @ mail：support@mail.haawking.com
 ******************************************************************/
#ifndef SYSTEM_H_
#define SYSTEM_H_


#include "dsc_config.h"


void DmaConfig();
void Scia_Config(uint32_t baud);
void Scia_Print(char *str);



#endif /* SYSTEM_H_ */
