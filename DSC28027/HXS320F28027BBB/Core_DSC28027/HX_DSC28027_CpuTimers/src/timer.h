/******************************************************************
 文 档 名：     timer.h
 D S P：       DSC28027
 使 用 库：
 作     用：
 说     明：      提供epwm.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：功能一：EPWM1/2/3初始化配置
 	 	           功能二：配置pwm波10KHz、死区5us、相位偏差120度输出，
 	 	                         TBCTR采用向上向下计数

 版 本：V0.0.3
 时 间：2022年1月19日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#ifndef TIMER0_BASE_H_
#define TIMER0_BASE_H_

#include "dsc_config.h"

typedef struct timer0_Type
{
	union
	{
		Uint16 All;
		struct
		{
				Uint16 OnemsdFlag :1;
		} Status_Bits;
	} Mark_Para;

	Uint32 msCounter;
} timer0;

extern timer0 timer0Base;

void Timer0_init(void);

INTERRUPT void cpu_timer0_isr(void);

#endif /* TIMER0_BASE_H_ */
