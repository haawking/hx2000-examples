/******************************************************************
 文 档 名 ：HX_DSC28027_FLASH
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
 D S P： DSC28027
 使 用 库：无
 作 用：将数据写入flash Sector[62]扇区，并验证其是否烧写成功
 说 明： LED1灯亮数据烧写成功，LED1灯灭数据烧写失败
 ----------------------例程使用说明-----------------------------
 *
 *            测试flash 功能，涉及 FLASH 写操作，请把主频调为96MHz
 *
 *
 * 现象：LED1点亮
 版本：V1.0.0
 时 间：2022年8月11日
 作 者：changxin
 @ mail：support@mail.haawking.com

 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "flash.h"

#define LEN 16
Uint32 Buffer[LEN] = {1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16};

void GPIO_Init();

int main(void)
{
	int temp;
	uint16_t i;
	uint16_t status;
	FLASH_ST FlashProgStatus;

	/*初始化系统控制，主频96M*/
	InitSysCtrl();

	/*初始化GIPIO*/
	GPIO_Init();

	/*擦除第62个扇区*/
	status = erase(62);
	/*写Buffer[LEN]到62个扇区, 从起始地址0x7DF000*/
	status = flash_program(Sector[62].StartAddr, Buffer, LEN, &FlashProgStatus);
	temp = 0;
	for(i = 0; i < LEN; i++)
	{
		/*从62扇区读取数据,核对数据是否一致*/
		if(*(Uint32*)(Sector[62].StartAddr + i) != Buffer[i])
			temp++;
	}

	while(1)
	{
		if(temp == 0)
		{
			/*LED1亮 数据对比成功*/
			GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
		}
		else
		{
			/* LED1灭 数据对比失败*/
			GpioDataRegs.GPBSET.bit.GPIO32 = 1;
			for(i = 0; i < 500; i++)
			{
				DELAY_US(1000);
			}
		}
	}

	return 0;
}

/******************************************************************
 *函数名：void GPIO_INIT()
 *参 数：无
 *返回值：无
 *作 用：GPIO引脚配置
 ******************************************************************/
void GPIO_Init()
{
	/*允许访问受保护的空间*/
	EALLOW;
	/*将GPIO32引脚配置为输出*/
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;
	/*GPIO32置1*/
	GpioDataRegs.GPBDAT.bit.GPIO32 = 1;


	EDIS;
}

// ----------------------------------------------------------------------------
