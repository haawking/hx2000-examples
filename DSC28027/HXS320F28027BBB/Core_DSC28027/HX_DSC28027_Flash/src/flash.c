/******************************************************************
 文 档 名：     flash.c
 D S P：       DSC28027
 使 用 库：
 作     用：
 说     明：      提供flash.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年3月3日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "flash.h"

SECTOR_ST OTP_Sector[4] = {
	{(Uint32 *)0x7A0000, (Uint32 *)0x7A07FF},
	{(Uint32 *)0x7A0800, (Uint32 *)0x7A0FFF},
	{(Uint32 *)0x7A1000, (Uint32 *)0x7A17FF},
	{(Uint32 *)0x7A1800, (Uint32 *)0x7A1FFF}
};

// Total 64 Sectors
SECTOR_ST Sector[64] = {
	{(Uint32 *)0x7C0000, (Uint32 *)0x7C07FF}, // sector 0
	{(Uint32 *)0x7C0800, (Uint32 *)0x7C0FFF}, // sector 1
	{(Uint32 *)0x7C1000, (Uint32 *)0x7C17FF}, // sector 2
	{(Uint32 *)0x7C1800, (Uint32 *)0x7C1FFF}, // sector 3
	{(Uint32 *)0x7C2000, (Uint32 *)0x7C27FF}, // sector 4
	{(Uint32 *)0x7C2800, (Uint32 *)0x7C2FFF}, // sector 5
	{(Uint32 *)0x7C3000, (Uint32 *)0x7C37FF}, // sector 6
	{(Uint32 *)0x7C3800, (Uint32 *)0x7C3FFF}, // sector 7
	{(Uint32 *)0x7C4000, (Uint32 *)0x7C47FF}, // sector 8
	{(Uint32 *)0x7C4800, (Uint32 *)0x7C4FFF}, // sector 9
	{(Uint32 *)0x7C5000, (Uint32 *)0x7C57FF}, // sector 10
	{(Uint32 *)0x7C5800, (Uint32 *)0x7C5FFF}, // sector 11
	{(Uint32 *)0x7C6000, (Uint32 *)0x7C67FF}, // sector 12
	{(Uint32 *)0x7C6800, (Uint32 *)0x7C6FFF}, // sector 13
	{(Uint32 *)0x7C7000, (Uint32 *)0x7C77FF}, // sector 14
	{(Uint32 *)0x7C7800, (Uint32 *)0x7C7FFF}, // sector 15
	{(Uint32 *)0x7C8000, (Uint32 *)0x7C87FF}, // sector 16
	{(Uint32 *)0x7C8800, (Uint32 *)0x7C8FFF}, // sector 17
	{(Uint32 *)0x7C9000, (Uint32 *)0x7C97FF}, // sector 18
	{(Uint32 *)0x7C9800, (Uint32 *)0x7C9FFF}, // sector 19
	{(Uint32 *)0x7CA000, (Uint32 *)0x7CA7FF}, // sector 20
	{(Uint32 *)0x7CA800, (Uint32 *)0x7CAFFF}, // sector 21
	{(Uint32 *)0x7CB000, (Uint32 *)0x7CB7FF}, // sector 22
	{(Uint32 *)0x7CB800, (Uint32 *)0x7CBFFF}, // sector 23
	{(Uint32 *)0x7CC000, (Uint32 *)0x7CC7FF}, // sector 24
	{(Uint32 *)0x7CC800, (Uint32 *)0x7CCFFF}, // sector 25
	{(Uint32 *)0x7CD000, (Uint32 *)0x7CD7FF}, // sector 26
	{(Uint32 *)0x7CD800, (Uint32 *)0x7CDFFF}, // sector 27
	{(Uint32 *)0x7CE000, (Uint32 *)0x7CE7FF}, // sector 28
	{(Uint32 *)0x7CE800, (Uint32 *)0x7CEFFF}, // sector 29
	{(Uint32 *)0x7CF000, (Uint32 *)0x7CF7FF}, // sector 30
	{(Uint32 *)0x7CF800, (Uint32 *)0x7CFFFF}, // sector 31
	{(Uint32 *)0x7D0000, (Uint32 *)0x7D07FF}, // sector 32
	{(Uint32 *)0x7D0800, (Uint32 *)0x7D0FFF}, // sector 33
	{(Uint32 *)0x7D1000, (Uint32 *)0x7D17FF}, // sector 34
	{(Uint32 *)0x7D1800, (Uint32 *)0x7D1FFF}, // sector 35
	{(Uint32 *)0x7D2000, (Uint32 *)0x7D27FF}, // sector 36
	{(Uint32 *)0x7D2800, (Uint32 *)0x7D2FFF}, // sector 37
	{(Uint32 *)0x7D3000, (Uint32 *)0x7D37FF}, // sector 38
	{(Uint32 *)0x7D3800, (Uint32 *)0x7D3FFF}, // sector 39
	{(Uint32 *)0x7D4000, (Uint32 *)0x7D47FF}, // sector 40
	{(Uint32 *)0x7D4800, (Uint32 *)0x7D4FFF}, // sector 41
	{(Uint32 *)0x7D5000, (Uint32 *)0x7D57FF}, // sector 42
	{(Uint32 *)0x7D5800, (Uint32 *)0x7D5FFF}, // sector 43
	{(Uint32 *)0x7D6000, (Uint32 *)0x7D67FF}, // sector 44
	{(Uint32 *)0x7D6800, (Uint32 *)0x7D6FFF}, // sector 45
	{(Uint32 *)0x7D7000, (Uint32 *)0x7D77FF}, // sector 46
	{(Uint32 *)0x7D7800, (Uint32 *)0x7D7FFF}, // sector 47
	{(Uint32 *)0x7D8000, (Uint32 *)0x7D87FF}, // sector 48
	{(Uint32 *)0x7D8800, (Uint32 *)0x7D8FFF}, // sector 49
	{(Uint32 *)0x7D9000, (Uint32 *)0x7D97FF}, // sector 50
	{(Uint32 *)0x7D9800, (Uint32 *)0x7D9FFF}, // sector 51
	{(Uint32 *)0x7DA000, (Uint32 *)0x7DA7FF}, // sector 52
	{(Uint32 *)0x7DA800, (Uint32 *)0x7DAFFF}, // sector 53
	{(Uint32 *)0x7DB000, (Uint32 *)0x7DB7FF}, // sector 54
	{(Uint32 *)0x7DB800, (Uint32 *)0x7DBFFF}, // sector 55
	{(Uint32 *)0x7DC000, (Uint32 *)0x7DC7FF}, // sector 56
	{(Uint32 *)0x7DC800, (Uint32 *)0x7DCFFF}, // sector 57
	{(Uint32 *)0x7DD000, (Uint32 *)0x7DD7FF}, // sector 58
	{(Uint32 *)0x7DD800, (Uint32 *)0x7DDFFF}, // sector 59
	{(Uint32 *)0x7DE000, (Uint32 *)0x7DE7FF}, // sector 60
	{(Uint32 *)0x7DE800, (Uint32 *)0x7DEFFF}, // sector 61
	{(Uint32 *)0x7DF000, (Uint32 *)0x7DF7FF}, // sector 62
	{(Uint32 *)0x7DF800, (Uint32 *)0x7DFFFF}, // sector 63
};

//
// Flash Status Structure
//
FLASH_ST FlashStatus;
FLASH_ST FlashProgStatus, FlashEraseStatus;

// Erase only 1 Sector once, sector_ide: 0-127,total 128 Sectors
/******************************************************************
 *函数名：erase
 *参   数： sector_idx, 擦写的扇区索引值
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") erase(int sector_idx)
{
	Uint16 Status;
	Status = Flash28027_Erase(sector_idx, &FlashStatus);
	if (Status != SUCCESS)
		return FAIL;
	else
		return SUCCESS;
}

/******************************************************************
 *函数名：flash_program
 *参   数： FlashAddr, Flash烧写地址
 BufAddr,   Flash烧写数据
 Length,    烧写长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") flash_program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus)
{
	Uint16 Status;
	Status = Flash28027_Program((Uint32 *)FlashAddr, (Uint32 *)BufAddr, Length, FProgStatus);
	if (Status != SUCCESS)
		return FAIL;
	else
		return SUCCESS;
}

/******************************************************************
 *函数名：Flash28027_Program
 *参   数： FlashAddr, Flash烧写地址
 BufAddr,   Flash烧写数据
 Length,    烧写长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_Program(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FProgStatus)
{
	Uint32 i;
	FLASH_ST VerifyStatus;
	FLASH_ST ProgVerifyStat;
	Uint16 Verify_Status;

	// CSM Unlock
	if (FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	// Flash has been Written
	if (Flash28027_ProgVerify(FlashAddr, Length, &ProgVerifyStat) != SUCCESS)
		return STATUS_FAIL_ZERO_BIT_ERROR;

	EALLOW;
	FlashRegs.FMEMWREN.bit.FMEMWREN = 1;
	FlashRegs.FPROWAIT = FPROWAIT_96M;
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FPGSWAIT = FPGSWAIT_96M;
	FlashRegs.FPERWAIT = FPERWAIT_96M;
	FlashRegs.FBANKWAIT = FBANKWAIT_96M;
	EDIS;

	for (i = 0; i < Length; i++)
	{
		while (FlashRegs.FSTAT.bit.BUSY || FlashRegs.FSTAT.bit.PROGRAMING || FlashRegs.FSTAT.bit.ERASEING || FlashRegs.FSTAT.bit.READING || FlashRegs.FSTAT.bit.MASS_ERASE)
			;
		*(Uint32 *)(FlashAddr + i) = *(BufAddr + i);
	}
	//	while (FlashRegs.FSTAT.bit.BUSY || FlashRegs.FSTAT.bit.PROGRAMING);

	Verify_Status = Flash28027_Verify(FlashAddr, BufAddr, Length, &VerifyStatus);
	if (Verify_Status == SUCCESS)
		return SUCCESS;
	else
		return STATUS_FAIL_PROGRAM;
}

/******************************************************************
 *函数名：Flash28027_Verify
 *参   数： FlashAddr, Flash校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_Verify(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i, ECnt;
	ECnt = 0;
	for (i = 0; i < Length; i++)
	{
		if (*(BufAddr + i) != *(FlashAddr + i))
			ECnt++;
	}

	if (ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_VERIFY;
}

/******************************************************************
 *函数名：Flash28027_ProgVerify
 *参   数： FlashAddr, Flash校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_ProgVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i, ECnt;
	ECnt = 0;
	for (i = 0; i < Length; i++)
	{
		if (0xFFFFFFFF != *(FlashAddr + i))
			ECnt++;
	}

	if (ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_ZERO_BIT_ERROR;
}

/******************************************************************
 *函数名：Flash28027_EraseVerify
 *参   数： FlashAddr, Flash擦写校验地址
 BufAddr,   Flash校验数据
 Length,    校验长度，32bit数据的个数
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_EraseVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat)
{
	Uint32 i, ECnt;
	ECnt = 0;
	for (i = 0; i < Length; i++)
	{
		if ((Uint32)(FlashAddr + i) < 0x7DFFF0)
		{
			if (0xFFFFFFFF != *(FlashAddr + i))
				ECnt++;
		}
	}

	if (ECnt == 0)
		return SUCCESS;
	else
		return STATUS_FAIL_ERASE;
}

/******************************************************************
 *函数名：Flash28027_Erase
 *参   数： SectorIdx, Flash擦除扇区索引值
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用：
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_Erase(Uint32 SectorIdx, FLASH_ST *FEraseStat)
{
	Uint32 *FlashAddr;
	FLASH_ST EraseVerifyStatus;
	Uint16 EraseVerify_Status;

	// CSM Unlock
	if (FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	if (SectorIdx < 0x0 || SectorIdx > 0x3f)
		return STATUS_FAIL_NO_SECTOR_SPECIFIED;

	EALLOW;
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FPERWAIT = FPERWAIT_96M;
	FlashRegs.FBANKWAIT = FBANKWAIT_96M;
	EDIS;

	EALLOW;
	FlashRegs.FPERCTL.bit.erase = SectorIdx;
	EDIS;
	while (FlashRegs.FSTAT.bit.ERASEING)
		;
	// Verify Each Sector, Length = 512 * 4B
	EraseVerify_Status = Flash28027_EraseVerify(Sector[SectorIdx].StartAddr, 512, &EraseVerifyStatus);
	if (EraseVerify_Status != SUCCESS)
		return STATUS_FAIL_ERASE;
	return SUCCESS;
}

/******************************************************************
 *函数名：Flash28027_MassErase
 *参   数：
 *返回值：SUCCESS,成功   FAIL,失败
 *作   用： Flash区全部擦除操作
 ******************************************************************/
Uint16 CODE_SECTION("ramfuncs1") Flash28027_MassErase(FLASH_ST *FEraseStat)
{

	// CSM Unlock
	if (FlashRegs.SYSCsmRegs.CSMSCR.bit.SECURE == 1)
		return STATUS_FAIL_CSM_LOCKED;

	EALLOW;
	// FlashRegs.FPERCTL.bit.enable_erase = 1;
	// FlashRegs.FPROWAIT = 60; // T = OSC_CLK / 4
	FlashRegs.FNVSHWAIT = FNVSHWAIT_96M;
	FlashRegs.FNVH1WAIT = FNVH1WAIT_96M;
	FlashRegs.FMERWAIT = FMERWAIT_96M;
	EDIS;

	EALLOW;
	FlashRegs.FMERCTL.bit.MASS_ERASE = 1;
	EDIS;
	while (FlashRegs.FSTAT.bit.MASS_ERASE)
		;
	return SUCCESS;
}
