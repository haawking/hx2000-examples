 /******************************************************************
文  档  名：HX_DSC28027_GPIO
编  译  器：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
D S P：	 DSC28027
使  用  库：无
作	    用：GPIO输入输出例程
说	    明：按下KEY按键  LED1灯亮
版	    本：V1.0.0
时	    间：2022年8月11日
作	    者：heyang
mail：   support@mail.haawking.com
******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"


/*全局变量*/


/*函数原型*/
void InitLED(void);
void InitKEY(void);


int main(void)
{
	InitSysCtrl();

	DINT;

	InitPieCtrl(); /*初始化PIE控制寄存器到默认状态，即所有的PIE中断被关闭，中断标志被清理*/

	IER = 0x0000; /*关闭CPU中断，清理所有CPU中断标志*/
	IFR = 0x0000;

	InitPieVectTable(); /*初始化PIE向量表*/

	InitLED(); /*初始化LED*/

	InitKEY(); /*按键*/

	EINT;

	while(1)
	{
		/*按下KEY LED1灯亮*/

		if(GpioDataRegs.GPADAT.bit.GPIO12 == 0)
		{
			GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;  /*LED1亮*/
		}
		else
		{
			GpioDataRegs.GPBSET.bit.GPIO32 = 1;     /*LED1灭*/

		}


	}

	return 0;
}



/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED()
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1; /*输出*/
	EDIS;
}


/******************************************************************
 函数名：void InitKEY()
 参	数：无
 返回值：无
 作	用：初始化独立IO口按键 KEY
 说	明：复位状态下，默认就是IO功能，输入
 ******************************************************************/
void InitKEY()
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 0; /*普通IO功能*/
	GpioCtrlRegs.GPADIR.bit.GPIO12 = 0; /*输入*/
	EDIS;
}


// ----------------------------------------------------------------------------
