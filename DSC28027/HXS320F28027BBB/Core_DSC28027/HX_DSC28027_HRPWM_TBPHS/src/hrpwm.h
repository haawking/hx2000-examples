/******************************************************************
 文 档 名：     hrpwm.h
 D S P：       DSC28027/DSC28034
 使 用 库：     
 作     用：      
 说     明：      提供hrpwm.h接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年1月13日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#ifndef HRPWM_H_
#define HRPWM_H_


#include "dsc_config.h"

void HRPWM1_config(uint16 period);
void HRPWM2_config(uint16 period);
void HRPWM3_config(uint16 period);

void INTERRUPT EPWM1_ISR(void);
void INTERRUPT EPWM2_ISR(void);
void INTERRUPT EPWM3_ISR(void);




#endif /* HRPWM_H_ */
