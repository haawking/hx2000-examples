#include "epwm.h"
/******************************************************************
 *函数名：void INTERRUPT epwm1_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/

void INTERRUPT epwm1_isr(void)
{
	update_db(&epwm1_info);

	EPwm1Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************
 *函数名：void INTERRUPT epwm2_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm2_isr(void)
{


	update_db(&epwm2_info);


	EPwm2Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************
 *函数名：void INTERRUPT epwm3_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数
 ******************************************************************/
void INTERRUPT epwm3_isr(void)
{

	update_db(&epwm3_info);

	EPwm3Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
