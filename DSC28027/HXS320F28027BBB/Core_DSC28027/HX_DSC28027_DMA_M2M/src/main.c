/******************************************************************
 文 档 名：     HX28034_DMA_M2M
 开 发 环 境： Haawking IDE V2.0.0
 开 发 板：     Core_DSC28027_V1.4
 D S P：         DSC28027
 使 用 库：
 作     用：      通过DMA发送数据
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过DMA实现从内存到内存的数据传输

 连接方式：

 现象：DMA传输数据成功并且正确，LED1(GPIO32)点亮；
            DMA传输数据失败或不一致，LED1(GPIO32)熄灭.

 版 本：V1.0.0
 时 间：2022年8月11日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

uint32_t M0_Buffer CODE_SECTION("M0");
uint32_t M1_Buffer CODE_SECTION("M1");

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    EALLOW;
    	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;
    	GpioDataRegs.GPBSET.bit.GPIO32 = 1;
    	EDIS;

    	M0_Buffer = 0x11111111;

    	//===================== dmac: ch0 m0 --> m1====================//
    	EALLOW;

    	DmaRegs.CLEARBLOCK.all = 0x0000000F;
    	DmaRegs.CLEARTFR.all = 0x0000000F;
    	DmaRegs.CLEARDSTTRAN.all = 0x0000000F;
    	DmaRegs.CLEARERR.all = 0x0000000F;
    	DmaRegs.CLEARSRCTRAN.all = 0x0000000F;

    	DmaRegs.CH0.SAR.bit.SAR = (uint32_t) &M0_Buffer;
    	DmaRegs.CH0.DAR.bit.DAR = (uint32_t) &M1_Buffer;

    	DmaRegs.CH0.CTL.bit.DST_TR_WIDTH = 2;
    	DmaRegs.CH0.CTL.bit.SRC_TR_WIDTH = 2;
    	DmaRegs.CH0.CTL.bit.BLOCK_TS = 4;
    	DmaRegs.CH0.CTL.bit.TT_FC = 0;

    	DmaRegs.CH0.CFG.bit.FIFO_EMPTY = 1;
    	DmaRegs.CH0.CFG.bit.FCMODE = 1;
    	DmaRegs.CH0.CFG.bit.PROTCTL = 1;
    	DmaRegs.CH0.CFG.bit.DEST_PER = 1;

    	DmaRegs.MASKTFR.all = 0x0101;
    	DmaRegs.MASKBLOCK.all = 0x0101;
    	DmaRegs.MASKSRCTRAN.all = 0x0101;
    	DmaRegs.MASKDSTTRAN.all = 0x0101;
    	DmaRegs.MASKERR.all = 0x0101;

    	DmaRegs.DMACFGREG.bit.DMA_EN = 1;
    	DmaRegs.CHENREG.all = 0x0101;

    	EDIS;

    	while ((DmaRegs.RAWTFR.bit.RAW & 0x1) == 0)
    	{
    	}

    	if (M0_Buffer == M1_Buffer)
    	{
    		GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
    	}
    	else
    	{
    		GpioDataRegs.GPBSET.bit.GPIO32 = 1;
    	}

    while(1)
    {
    	M0_Buffer+=0x01;
    	EALLOW;
    	DmaRegs.CH0.SAR.bit.SAR = (uint32_t) &M0_Buffer;
    	DmaRegs.CH0.DAR.bit.DAR = (uint32_t) &M1_Buffer;
    	DmaRegs.DMACFGREG.bit.DMA_EN = 1;
    	DmaRegs.CHENREG.all = 0x0101;
    	EDIS;

    	while ((DmaRegs.RAWTFR.bit.RAW & 0x1) == 0)
    	    	{
    	    	}

    	    	if (M0_Buffer == M1_Buffer)
    	    	{
    	    		GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
    	    	}
    	    	else
    	    	{
    	    		GpioDataRegs.GPBSET.bit.GPIO32 = 1;
    	    	}
    	DELAY_US(100);
    }

	return 0;
}

// ----------------------------------------------------------------------------
