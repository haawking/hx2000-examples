//###########################################################################
//
// FILE:    ier_unset.S
//
// TITLE:   F2802x ier_unset Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.1.0 $
// $Release Date: 2022-07-11 09:44:07 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 ier_unset

ier_unset:
csrc 0x304,a0  //IER &= ~a0 
ret


	

