#include "xint.h"

uint16 xint2counter;
/******************************************************************
 *函数名：void XINT2_Init()
 *参 数：无
 *返回值：无
 *作 用：配置XINT2
 ******************************************************************/
void XINT2_Init(void)
{
	EALLOW;
	/*选择GPIO17引脚作为XINT2中断源*/
	GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = 17;
	/*上升沿产生的中断 (低到高过渡)*/
	XIntruptRegs.XINT2CR.bit.POLARITY = 1;
	/*使能中断*/
	XIntruptRegs.XINT2CR.bit.ENABLE = 1;
	EDIS;
}
/******************************************************************
 *函数名：void INTERRUPT xint2_isr()
 *参 数：无
 *返回值：无
 *作 用：XINT2中断服务函数
 ******************************************************************/
void INTERRUPT xint2_isr(void)
{
	/*中断次数加1*/
	xint2counter++;
	GpioDataRegs.GPBTOGGLE.bit.GPIO32 = 1;       /*LED1翻转*/
	/*中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
