﻿/******************************************************************
 文 档 名 ：HX_DSC28027_EPWM_TZ
 开 发 环 境：Haawking IDE V2.0.0
 开 发 板 ：Core_DSC28027_V1.4
 D S P： DSC28027
 使 用 库：无
 作 用：pwm波生成、错误联防

 ----------------------例程使用说明-----------------------------
 *
 *            测试epwm功能
 *
 * 说 明：初始时GPIO16  GPIO17接3.3V，
 *            (1)采用ePWM模块编程实现输出周期在33us，
 *                占空比50%的PWM波，TBCTR采用向上向下计数；
 *            (2)实现错误联防事件：
 *                ①　epwm1单次触发epwm1A置高，epwm1B置低（事件3）GPIO17接地，LED1亮
 *                ②　epwm2周期循环触发epwm2A置高，epwm2B置低（事件2）GPIO16接地
 * 现象：可实现pwm发波、错误联防
 *
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	/*初始化TZ错误联防引脚配置*/
	InitTzGpio();
	/*初始化LED，用于指示错误联防触发*/
	InitLED();
	/*关中断*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER=0x0000;
	IFR=0x0000;
	/*初始化PIE中断向量表*/
	InitPieVectTable();
	/*允许访问受保护的空间*/
	EALLOW;
	/*将epwm1_tz_isr入口地址赋给EPWM1_TZINT,执行OST错误联防触发*/
	PieVectTable.EPWM1_TZINT = &epwm1_tz_isr;
	/*将epwm2_tz_isr入口地址赋给EPWM2_TZINT,执行CBC错误联防触发*/
	PieVectTable.EPWM2_TZINT = &epwm2_tz_isr;
	/*禁止访问受保护的空间*/
	EDIS;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*初始化EPWM1,错误联防配置为单次OSHT3触发*/
	InitEPwm1Example();
	/*初始化EPWM2，错误联防配置为周期CBC2触发*/
	InitEPwm2Example();
	/*所有使能的ePWM模块同步使用TBCLK*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*使能CPU IER的第2组中断向量*/
	IER|=M_INT2;
	/*使能相对应的中断*/
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;
	/*使能全局中断*/
	EINT;
	while (1)
	{
	}
	return 0;
}

// ----------------------------------------------------------------------------
