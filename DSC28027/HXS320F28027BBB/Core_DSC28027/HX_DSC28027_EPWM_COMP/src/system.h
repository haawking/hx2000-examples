/*
 * system.h
 *
 *  Created on: 2022Äê4ÔÂ15ÈÕ
 *      Author: HX
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"
//EPwm

void InitEpwm1_Example(void);
void InitEpwm2_Example(void);
void InitEpwm3_Example(void);

void INTERRUPT epwm1_tz_isr(void);
void INTERRUPT epwm2_tz_isr(void);
void INTERRUPT epwm3_tz_isr(void);

extern uint32 EPwm_CBC_flag;
extern uint32 EPwm_DC_flag;

//adc
void ADC_Init(void);
void INTERRUPT adc_isr();

void InitLED(void);
void InitComp2(void);
#endif /* SYSTEM_H_ */
