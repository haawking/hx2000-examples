#include "system.h"
/******************************************************************
*函数名：void InitLED()
*参 数：无
*返回值：无
*作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
		/*允许访问受保护的空间*/
		EALLOW;
		GpioCtrlRegs.GPBMUX1.bit.GPIO32=0;//将GPIO7配置为数字IO
		GpioCtrlRegs.GPBDIR.bit.GPIO32=1;	//将GPIO7配置为输出
		GpioDataRegs.GPBSET.bit.GPIO32=1;
		EDIS;												//禁止访问受保护的空间
}
