#include "epwm.h"
/******************************************************************
 *函数名：void epwm1_config()
 *参 数：无
 *返回值：无
 *作 用：配置epwm1
 ******************************************************************/
void epwm1_config()
{
	/* Period = 0x599*TBCLK counts*/
	EPwm1Regs.TBPRD = 599;
	/*设置CMPA值为0x200*/
	EPwm1Regs.CMPA.half.CMPA = 0x200;
	/*设置CMPB值为0x300*/
	EPwm1Regs.CMPB = 0x300;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））。*/
	EPwm1Regs.TBCTR = 0;
	/*epwm时基计数器相位=0*/
	EPwm1Regs.TBPHS.half.TBPHS = 0;

	/*当时间基准计数器的值等于CMPA的值，且正在减计数时，使EPWMxA输出低电平*/
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxA输出高电平*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm1Regs.AQCTLB.bit.CBD = AQ_CLEAR;
	/*当时间基准计数器的值等于CMPB的值，且正在增计数时，使EPWMxB输出高电平*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;

	/*增减计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*从其影子寄存器中加载周期寄存器（TBPRD）*/
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	/*时基计数器等于零（TBCTR = 0x0000）作为选择EPWMxSYNCO信号源*/
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	/*软件强制同步脉冲*/
	EPwm1Regs.TBCTL.bit.SWFSYNC = 0;
	/*高速时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	//时基时钟分频 1倍分频
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	//时基计数器等于周期时生成（TBCTR = TBPRD）EPWMxSOCA脉冲
	EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;
	/*使能EPWMxSOCA脉冲*/
	EPwm1Regs.ETSEL.bit.SOCAEN = 1;
	/*在第一个事件上生成EPWMxSOCA脉冲*/
	EPwm1Regs.ETPS.bit.SOCAPRD = ET_1ST;
}
