//###########################################################################
//
// FILE:   F2802x_DevEmu.h
//
// TITLE:  F2802x Device Emulation Register Definitions.
//
//###########################################################################
// $HAAWKING Release: F2802x Support Library V1.1.0 $
// $Release Date: 2022-07-11 09:45:00 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################



#ifndef F2802x_DEV_EMU_H
#define F2802x_DEV_EMU_H

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------
// Device Emulation Register Bit Definitions:
//
// Device Configuration Register Bit Definitions
struct DEVICECNF_BITS  {     // bits  description
   Uint32 rsvd1:5;           // 4:0   reserved
   Uint32 XRSn:1;            // 5     XRSn Signal Status
   Uint32 rsvd3:21;          // 26:6  reserved
   Uint32 TRSTn:1;           // 27    Status of TRSTn signal
   Uint32 rsvd6:4;           // 31:28 reserved
};

union DEVICECNF_REG {
   uint32                 all;
   struct DEVICECNF_BITS  bit;
};

// CLASSID
struct CLASSID_BITS   {  // bits  description
   uint32 CLASSNO:8;     // 7:0   Class Number
   uint32 PARTTYPE:8;    // 15:8  Part Type
   Uint32 resvd1:16;     // 31:16 reserved
};

union CLASSID_REG {
   uint32               all;
   struct CLASSID_BITS  bit;
};



struct DEV_EMU_REGS {
   union DEVICECNF_REG DEVICECNF;  // Device Configuration
   union CLASSID_REG   CLASSID;    // Class ID
   uint32              REVID;      // Device ID
};



//---------------------------------------------------------------------------
// Device Emulation Register References & Function Declarations:
//
extern 	volatile struct DEV_EMU_REGS  *const P_DevEmuRegs;
extern  volatile struct DEV_EMU_REGS DevEmuRegs;

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  // end of F2802x_DEV_EMU_H definition

//===========================================================================
// End of file.
//===========================================================================
