#include "epwm.h"

void InitHC(void)
{
	GpioCtrlRegs.GPAMUX2.bit.GPIO28=0;
	GpioCtrlRegs.GPADIR.bit.GPIO28=0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO32=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO32=0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO33=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO33=0;
}
