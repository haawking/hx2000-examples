#include "epwm.h"

void InitKEY(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.bit.GPIO7=0;
	GpioCtrlRegs.GPADIR.bit.GPIO7=0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO6=0;
	GpioCtrlRegs.GPADIR.bit.GPIO6=0;
	EDIS;
}
