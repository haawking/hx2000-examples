/*function1: Gpio7 GPIO6启动与调速
 *function1: Gpio7首次按下，电机启动
 *function1: Gpio7按下，电机加速，按下一次加速500rpm(Speed_ref+500)
 *function1: Gpio6按下，电机减速，按下一次减速500rpm(Speed_ref-500)
 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : main.c
 * FILE   : main.c
 *****************************************************************/
#include "epwm.h"

void theta_config(void)
{
	if(GpioDataRegs.GPADAT.bit.GPIO7==0)
	{
		Speed_ref+=500;
	}
	else
	{

	}
	if(GpioDataRegs.GPADAT.bit.GPIO6==0)
	{
		Speed_ref-=500;
	}
	else
	{

	}
}
