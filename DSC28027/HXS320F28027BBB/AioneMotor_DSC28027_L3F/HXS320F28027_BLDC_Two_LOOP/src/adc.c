#include "epwm.h"
/******************************************************************
*函数名：void ADC_Init()
*参 数：无
*返回值：无
*作 用：配置ADC
*作 用：采样电流,A2通道连接直流母线电流,用于检测换相电流
******************************************************************/
void ADC_Init(void)
{
	EALLOW;
	AdcRegs.ADCCTL1.bit.ADCREFSEL=0;
	/*中断脉冲产生在ADC结果锁定到其结果寄存器之前发生一个周期*/
	AdcRegs.ADCCTL1.bit.INTPULSEPOS=1;
	/*中断选择3寄存器*/
	AdcRegs.INTSEL3N4.bit.INT3E=1;
	/*每当产生EOC脉冲时，都会产生附加脉冲，而不管是否清除了标记位*/
	AdcRegs.INTSEL3N4.bit.INT3CONT=0;
	/*EOC6是ADCINT3的触发器*/
	AdcRegs.INTSEL3N4.bit.INT3SEL=6;

	/*将SOC1设置为样本A2*/
	AdcRegs.ADCSOC1CTL.bit.CHSEL=2;
	/*将SOC2设置为样本B3*/
	AdcRegs.ADCSOC2CTL.bit.CHSEL=11;
	/*将SOC3设置为样本B6*/
	AdcRegs.ADCSOC3CTL.bit.CHSEL=15;
	/*将SOC4设置为样本A1*/
	AdcRegs.ADCSOC4CTL.bit.CHSEL=1;
	/*将SOC5设置为样本A3*/
	AdcRegs.ADCSOC5CTL.bit.CHSEL=3;
	/*将SOC6设置为样本A7*/
	AdcRegs.ADCSOC6CTL.bit.CHSEL=7;

	/*SOCx触发源选择:ADCTRIG5-ePWM1，ADCSOCA*/
	AdcRegs.ADCSOC1CTL.bit.TRIGSEL=0x05;
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL=0x05;
	AdcRegs.ADCSOC3CTL.bit.TRIGSEL=0x05;
	AdcRegs.ADCSOC4CTL.bit.TRIGSEL=0x05;
	AdcRegs.ADCSOC5CTL.bit.TRIGSEL=0x05;
	AdcRegs.ADCSOC6CTL.bit.TRIGSEL=0x05;

	/*采样窗口长达6个周期（2n+6个时钟周期）*/
	AdcRegs.ADCSOC0CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC1CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC2CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC3CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC4CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC5CTL.bit.ACQPS=0;
	AdcRegs.ADCSOC6CTL.bit.ACQPS=0;
	EDIS;
}
