/*function1:EPWM1/2/3的输出占空比变化配置
 *function1: 配置pwm波转速环PI追踪
 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : epwm_spwm.c
 *****************************************************************/

#include "epwm.h"
#include "IQmathLib.h"

float s=0.00;

/*function1:转速环的输出处理
 *function1: 转速追踪
 *****************************************************************/
void speed_output(void)
{
	s=speed_out>>8;
}

/*function1:epwm_compare占空比转速追踪
 * function1:转速追踪
 *function2: 六步换相相序配置程序
 *function2: 相序按六步换相切换导通顺序
 *****************************************************************/
void epwm_compare(void)
{
	/*转速输出*/
	speed_output();

	/*电流输出*/
	current_output();

	switch(Hall)
	{
	case 1:
			EPwm1Regs.CMPA.half.CMPA=1590+compare_i,EPwm1Regs.CMPB=0,
			EPwm2Regs.CMPA.half.CMPA=0,EPwm2Regs.CMPB=0,
			EPwm3Regs.CMPA.half.CMPA=0,EPwm3Regs.CMPB=1590+compare_i;
			break;
	case 2:
			EPwm1Regs.CMPA.half.CMPA=0,EPwm1Regs.CMPB=1590+compare_i,
			EPwm2Regs.CMPA.half.CMPA=1590+compare_i,EPwm2Regs.CMPB=0,
			EPwm3Regs.CMPA.half.CMPA=0,EPwm3Regs.CMPB=0;
			break;
	case 3:
			EPwm1Regs.CMPA.half.CMPA=0,EPwm1Regs.CMPB=0,
			EPwm2Regs.CMPA.half.CMPA=1590+compare_i,EPwm2Regs.CMPB=0,
			EPwm3Regs.CMPA.half.CMPA=0,EPwm3Regs.CMPB=1590+compare_i;
			break;
	case 4:
			EPwm1Regs.CMPA.half.CMPA=0,EPwm1Regs.CMPB=0,
			EPwm2Regs.CMPA.half.CMPA=0,EPwm2Regs.CMPB=1590+compare_i,
			EPwm3Regs.CMPA.half.CMPA=1590+compare_i,EPwm3Regs.CMPB=0;
			break;
	case 5:
			EPwm1Regs.CMPA.half.CMPA=1590+compare_i,EPwm1Regs.CMPB=0,
			EPwm2Regs.CMPA.half.CMPA=0,EPwm2Regs.CMPB=1590+compare_i,
			EPwm3Regs.CMPA.half.CMPA=0,EPwm3Regs.CMPB=0;
			break;
	case 6:
			EPwm1Regs.CMPA.half.CMPA=0,EPwm1Regs.CMPB=1590+compare_i,
			EPwm2Regs.CMPA.half.CMPA=0,EPwm2Regs.CMPB=0,
			EPwm3Regs.CMPA.half.CMPA=1590+compare_i,EPwm3Regs.CMPB=0;
			break;
	}
}
