/*function1:EPWM1/2/3初始化配置
 *function1: 配置pwm波20KHz,TBCTR采用向上向下计数
 * Authors: heyang
 * Email  : yang.he@mail.haawking.com
 * FILE   : epwm.c
 *****************************************************************/

#include "epwm.h"

void InitEPwm1Example()
{
	/*配置EPWM输出频率为3000*2*TBCLK=20kHz*/
	EPwm1Regs.TBPRD=3000;
	/*配置EPWM输出相位不偏移*/
	EPwm1Regs.TBPHS.half.TBPHS=0;
	/*配置EPWM的TBCTR计数初值为0*/
	EPwm1Regs.TBCTR=0x0000;

	/*上电未按下启动先封波*/
	EPwm1Regs.CMPA.half.CMPA=0;
	EPwm1Regs.CMPB=0;

	/*配置EPWM的TBCTR采用向上向下计数*/
	EPwm1Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*配置EPWM输出不装载相位偏移*/
	EPwm1Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	/*配置EPWM时基频率TBCLK为系统时钟，不进行分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	EPwm1Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	/*比较模块CMPA采用影子寄存器装载模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	/*比较模块CMPA从CTR=0时开始装载*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	EPwm1Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;

	/*TBCTR向上计数时，达到CMPA事件，EPWM1A产生置低动作*/
	EPwm1Regs.AQCTLA.bit.CAU=AQ_CLEAR;
	/*TBCTR向下计数时，达到CMPA事件，EPWM1A产生置高动作*/
	EPwm1Regs.AQCTLA.bit.CAD=AQ_SET;
	/*TBCTR向下计数时，达到CMPB事件，EPWM1B产生置高动作*/
	EPwm1Regs.AQCTLB.bit.CBD=AQ_SET;
	/*TBCTR向上计数时，达到CMPB事件，EPWM1B产生置低动作*/
	EPwm1Regs.AQCTLB.bit.CBU=AQ_CLEAR;

	/*中断事件选择，当CTR=0时开始产生事件中断*/
	EPwm1Regs.ETSEL.bit.INTSEL=ET_CTR_ZERO;
	/*中断事件选择，事件中断的使能信号*/
	EPwm1Regs.ETSEL.bit.INTEN=1;
	/*中断事件分频配置，每周期触发一次*/
	EPwm1Regs.ETPS.bit.INTPRD=ET_1ST;

    /*EPWM的SOCA事件产生*/
    /*EPWM的SOCA事件触发时间选择，在CTR=PRD时触发*/
    EPwm1Regs.ETSEL.bit.SOCASEL=ET_CTR_PRD;
    /*EPWM的SOCA事件使能*/
    EPwm1Regs.ETSEL.bit.SOCAEN=1;
    /*EPWM的SOCA事件触发次数配置，每周期触发一次*/
    EPwm1Regs.ETPS.bit.SOCAPRD=ET_1ST;
}

void InitEPwm2Example()
{
	EPwm2Regs.TBPRD=3000;
	/*配置EPWM输出相位偏移2*pi/3，一周期的1/3*/
	EPwm2Regs.TBPHS.half.TBPHS=0;
	EPwm2Regs.TBCTR=0x0000;

	EPwm2Regs.CMPA.half.CMPA=0;
	EPwm2Regs.CMPB=0;

	EPwm2Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*配置EPWM输出相位需装载*/
	EPwm2Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	EPwm2Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	EPwm2Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	EPwm2Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	EPwm2Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;

	/*TBCTR向上计数时，达到CMPA事件，EPWM2A产生置低动作*/
	EPwm2Regs.AQCTLA.bit.CAU=AQ_CLEAR;
	/*TBCTR向下计数时，达到CMPA事件，EPWM2A产生置高动作*/
	EPwm2Regs.AQCTLA.bit.CAD=AQ_SET;
	/*TBCTR向下计数时，达到CMPB事件，EPWM2B产生置高动作*/
	EPwm2Regs.AQCTLB.bit.CBD=AQ_SET;
	/*TBCTR向上计数时，达到CMPB事件，EPWM2B产生置低动作*/
	EPwm2Regs.AQCTLB.bit.CBU=AQ_CLEAR;
}

void InitEPwm3Example()
{
	EPwm3Regs.TBPRD=3000;
	/*配置EPWM输出相位偏移4*pi/3，一周期的2/3*/
	EPwm3Regs.TBPHS.half.TBPHS=0;
	EPwm3Regs.TBCTR=0x0000;

	EPwm3Regs.CMPA.half.CMPA=0;
	EPwm3Regs.CMPB=0;

	EPwm3Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*配置EPWM输出相位需装载*/
	EPwm3Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	EPwm3Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	EPwm3Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;

	/*TBCTR向上计数时，达到CMPA事件，EPWM3A产生置低动作*/
	EPwm3Regs.AQCTLA.bit.CAU=AQ_CLEAR;
	/*TBCTR向下计数时，达到CMPA事件，EPWM3A产生置高动作*/
	EPwm3Regs.AQCTLA.bit.CAD=AQ_SET;
	/*TBCTR向下计数时，达到CMPB事件，EPWM3B产生置高动作*/
	EPwm3Regs.AQCTLB.bit.CBD=AQ_SET;
	/*TBCTR向上计数时，达到CMPB事件，EPWM3B产生置低动作*/
	EPwm3Regs.AQCTLB.bit.CBU=AQ_CLEAR;
}
