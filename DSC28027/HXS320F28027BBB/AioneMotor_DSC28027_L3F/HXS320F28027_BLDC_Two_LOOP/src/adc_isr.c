#include "epwm.h"

Uint32 adcVal[6];
/******************************************************************
 *函数名：void INTERRUPT adc_isr()
 *参 数：无
 *返回值：无
 *作 用：adc中断服务函数：当ADC总线空闲时，开始采样，得到正确的采样值
 ******************************************************************/
void INTERRUPT adc_isr()
{
	/* 等待ADC总线处于空闲状态时，开始中断采样*/
	while (AdcRegs.ADCCTL1.bit.ADCBSY == 1)
	{
	}
	adcVal[0] = AdcResult.ADCRESULT1;
	adcVal[1] = AdcResult.ADCRESULT2;
	adcVal[2] = AdcResult.ADCRESULT3;
	adcVal[3] = AdcResult.ADCRESULT4;
	adcVal[4] = AdcResult.ADCRESULT5;
	adcVal[5] = AdcResult.ADCRESULT6;

	EALLOW;
	/*清除ADCINTFLG寄存器中各自的标志位*/
	AdcRegs.ADCINTFLGCLR.bit.ADCINT3 = 0x1;
	/* 清除ADCINTOVF寄存器中相应的溢出位*/
	AdcRegs.ADCINTOVFCLR.bit.ADCINT3 = 0x1;
	/* 中断应答，锁定IER的第10组中断向量*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
	EDIS;

}

