//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F280039CEDB, Hal DriverLib, 1.0.0
//
// Release time: 2023-12-14 09:30:37.805431
//
//#############################################################################


#ifndef HW_PIEVECT_H
#define HW_PIEVECT_H

//*************************************************************************************************
//
// The following are defines for the PIEVECT register offsets
//
//*************************************************************************************************
#define PIEVECT_O_PIE1_RESERVED_INT     0x0U     // Reserved
#define PIEVECT_O_PIE2_RESERVED_INT     0x4U     // Reserved
#define PIEVECT_O_PIE3_RESERVED_INT     0x8U     // Reserved
#define PIEVECT_O_PIE4_RESERVED_INT     0xCU     // Reserved
#define PIEVECT_O_PIE5_RESERVED_INT     0x10U    // Reserved
#define PIEVECT_O_PIE6_RESERVED_INT     0x14U    // Reserved
#define PIEVECT_O_PIE7_RESERVED_INT     0x18U    // Reserved
#define PIEVECT_O_PIE8_RESERVED_INT     0x1CU    // Reserved
#define PIEVECT_O_PIE9_RESERVED_INT     0x20U    // Reserved
#define PIEVECT_O_PIE10_RESERVED_INT    0x24U    // Reserved
#define PIEVECT_O_PIE11_RESERVED_INT    0x28U    // Reserved
#define PIEVECT_O_PIE12_RESERVED_INT    0x2CU    // Reserved
#define PIEVECT_O_PIE13_RESERVED_INT    0x30U    // Reserved
#define PIEVECT_O_TIMER1_INT            0x34U    // CPU Timer 1 Interrupt
#define PIEVECT_O_TIMER2_INT            0x38U    // CPU Timer 2 Interrupt
#define PIEVECT_O_RTOS_INT              0x40U    // RTOS Interrupt from ERAD
#define PIEVECT_O_NMI_INT               0x48U    // Non-Maskable Interrupt
#define PIEVECT_O_ILLEGAL_INT           0x4CU    // Illegal Operation Trap
#define PIEVECT_O_ADCA1_INT             0x80U    // 1.1 - ADCA Interrupt 1
#define PIEVECT_O_ADCB1_INT             0x84U    // 1.2 - ADCB Interrupt 1
#define PIEVECT_O_ADCC1_INT             0x88U    // 1.3 - ADCC Interrupt 1
#define PIEVECT_O_XINT1_INT             0x8CU    // 1.4 - XINT1 Interrupt
#define PIEVECT_O_XINT2_INT             0x90U    // 1.5 - XINT2 Interrupt
#define PIEVECT_O_PIE14_RESERVED_INT    0x94U    // 1.6 - Reserved
#define PIEVECT_O_TIMER0_INT            0x98U    // 1.7 - Timer 0 Interrupt
#define PIEVECT_O_WAKE_INT              0x9CU    // 1.8 - Standby and Halt Wakeup Interrupt
#define PIEVECT_O_PIE35_RESERVED_INT    0xA0U    // 1.9 - Reserved
#define PIEVECT_O_SYS_ERR_INT           0xA4U    // 1.10 - SYS_ERR interrupt
#define PIEVECT_O_PIE36_RESERVED_INT    0xA8U    // 1.11 - Reserved
#define PIEVECT_O_PIE37_RESERVED_INT    0xACU    // 1.12 - Reserved
#define PIEVECT_O_PIE38_RESERVED_INT    0xB0U    // 1.13 - Reserved
#define PIEVECT_O_PIE39_RESERVED_INT    0xB4U    // 1.14 - Reserved
#define PIEVECT_O_PIE40_RESERVED_INT    0xB8U    // 1.15 - Reserved
#define PIEVECT_O_PIE41_RESERVED_INT    0xBCU    // 1.16 - Reserved
#define PIEVECT_O_EPWM1_TZ_INT          0xC0U    // 2.1 - ePWM1 Trip Zone Interrupt
#define PIEVECT_O_EPWM2_TZ_INT          0xC4U    // 2.2 - ePWM2 Trip Zone Interrupt
#define PIEVECT_O_EPWM3_TZ_INT          0xC8U    // 2.3 - ePWM3 Trip Zone Interrupt
#define PIEVECT_O_EPWM4_TZ_INT          0xCCU    // 2.4 - ePWM4 Trip Zone Interrupt
#define PIEVECT_O_EPWM5_TZ_INT          0xD0U    // 2.5 - ePWM5 Trip Zone Interrupt
#define PIEVECT_O_EPWM6_TZ_INT          0xD4U    // 2.6 - ePWM6 Trip Zone Interrupt
#define PIEVECT_O_EPWM7_TZ_INT          0xD8U    // 2.7 - ePWM7 Trip Zone Interrupt
#define PIEVECT_O_EPWM8_TZ_INT          0xDCU    // 2.8 - ePWM8 Trip Zone Interrupt
#define PIEVECT_O_PIE42_RESERVED_INT    0xE0U    // 2.9 - Reserved
#define PIEVECT_O_PIE43_RESERVED_INT    0xE4U    // 2.10 - Reserved
#define PIEVECT_O_PIE44_RESERVED_INT    0xE8U    // 2.11 - Reserved
#define PIEVECT_O_PIE45_RESERVED_INT    0xECU    // 2.12 - Reserved
#define PIEVECT_O_PIE46_RESERVED_INT    0xF0U    // 2.13 - Reserved
#define PIEVECT_O_PIE47_RESERVED_INT    0xF4U    // 2.14 - Reserved
#define PIEVECT_O_PIE48_RESERVED_INT    0xF8U    // 2.15 - Reserved
#define PIEVECT_O_PIE49_RESERVED_INT    0xFCU    // 2.16 - Reserved
#define PIEVECT_O_EPWM1_INT             0x100U   // 3.1 - ePWM1 Interrupt
#define PIEVECT_O_EPWM2_INT             0x104U   // 3.2 - ePWM2 Interrupt
#define PIEVECT_O_EPWM3_INT             0x108U   // 3.3 - ePWM3 Interrupt
#define PIEVECT_O_EPWM4_INT             0x10CU   // 3.4 - ePWM4 Interrupt
#define PIEVECT_O_EPWM5_INT             0x110U   // 3.5 - ePWM5 Interrupt
#define PIEVECT_O_EPWM6_INT             0x114U   // 3.6 - ePWM6 Interrupt
#define PIEVECT_O_EPWM7_INT             0x118U   // 3.7 - ePWM7 Interrupt
#define PIEVECT_O_EPWM8_INT             0x11CU   // 3.8 - ePWM8 Interrupt
#define PIEVECT_O_PIE50_RESERVED_INT    0x120U   // 3.9 - Reserved
#define PIEVECT_O_PIE51_RESERVED_INT    0x124U   // 3.10 - Reserved
#define PIEVECT_O_PIE52_RESERVED_INT    0x128U   // 3.11 - Reserved
#define PIEVECT_O_PIE53_RESERVED_INT    0x12CU   // 3.12 - Reserved
#define PIEVECT_O_PIE54_RESERVED_INT    0x130U   // 3.13 - Reserved
#define PIEVECT_O_PIE55_RESERVED_INT    0x134U   // 3.14 - Reserved
#define PIEVECT_O_PIE56_RESERVED_INT    0x138U   // 3.15 - Reserved
#define PIEVECT_O_PIE57_RESERVED_INT    0x13CU   // 3.16 - Reserved
#define PIEVECT_O_ECAP1_INT             0x140U   // 4.1 - eCAP1 Interrupt
#define PIEVECT_O_ECAP2_INT             0x144U   // 4.2 - eCAP2 Interrupt
#define PIEVECT_O_ECAP3_INT             0x148U   // 4.3 - eCAP3 Interrupt
#define PIEVECT_O_PIE15_RESERVED_INT    0x14CU   // 4.4 - Reserved
#define PIEVECT_O_PIE16_RESERVED_INT    0x150U   // 4.5 - Reserved
#define PIEVECT_O_PIE17_RESERVED_INT    0x154U   // 4.6 - Reserved
#define PIEVECT_O_PIE18_RESERVED_INT    0x158U   // 4.7 - Reserved
#define PIEVECT_O_PIE19_RESERVED_INT    0x15CU   // 4.8 - Reserved
#define PIEVECT_O_PIE58_RESERVED_INT    0x160U   // 4.9 - Reserved
#define PIEVECT_O_PIE59_RESERVED_INT    0x164U   // 4.10 - Reserved
#define PIEVECT_O_ECAP3_2_INT           0x168U   // 4.11 - eCAP3_2 Interrupt
#define PIEVECT_O_PIE60_RESERVED_INT    0x16CU   // 4.12 - Reserved
#define PIEVECT_O_PIE61_RESERVED_INT    0x170U   // 4.13 - Reserved
#define PIEVECT_O_PIE62_RESERVED_INT    0x174U   // 4.14 - Reserved
#define PIEVECT_O_PIE63_RESERVED_INT    0x178U   // 4.15 - Reserved
#define PIEVECT_O_PIE64_RESERVED_INT    0x17CU   // 4.16 - Reserved
#define PIEVECT_O_EQEP1_INT             0x180U   // 5.1 - eQEP1 Interrupt
#define PIEVECT_O_EQEP2_INT             0x184U   // 5.2 - eQEP2 Interrupt
#define PIEVECT_O_PIE20_RESERVED_INT    0x188U   // 5.3 - Reserved
#define PIEVECT_O_PIE21_RESERVED_INT    0x18CU   // 5.4 - Reserved
#define PIEVECT_O_CLB1_INT              0x190U   // 5.5 - CLB1 (Reconfigurable Logic) Interrupt
#define PIEVECT_O_CLB2_INT              0x194U   // 5.6 - CLB2 (Reconfigurable Logic) Interrupt
#define PIEVECT_O_CLB3_INT              0x198U   // 5.7 - CLB3 (Reconfigurable Logic) Interrupt
#define PIEVECT_O_CLB4_INT              0x19CU   // 5.8 - CLB4 (Reconfigurable Logic) Interrupt
#define PIEVECT_O_SDFM1_INT             0x1A0U   // 5.9 - SDFM1 Interrupt
#define PIEVECT_O_SDFM2_INT             0x1A4U   // 5.10 - SDFM2 Interrupt
#define PIEVECT_O_PIE65_RESERVED_INT    0x1A8U   // 5.11 - Reserved
#define PIEVECT_O_PIE66_RESERVED_INT    0x1ACU   // 5.12 - Reserved
#define PIEVECT_O_SDFM1DR1_INT          0x1B0U   // 5.13 - SDFM1DR1 Interrupt
#define PIEVECT_O_SDFM1DR2_INT          0x1B4U   // 5.14 - SDFM1DR2 Interrupt
#define PIEVECT_O_SDFM1DR3_INT          0x1B8U   // 5.15 - SDFM1DR3 Interrupt
#define PIEVECT_O_SDFM1DR4_INT          0x1BCU   // 5.16 - SDFM1DR4 Interrupt
#define PIEVECT_O_SPIA_RX_INT           0x1C0U   // 6.1 - SPIA Receive Interrupt
#define PIEVECT_O_SPIA_TX_INT           0x1C4U   // 6.2 - SPIA Transmit Interrupt
#define PIEVECT_O_SPIB_RX_INT           0x1C8U   // 6.3 - SPIB Receive Interrupt
#define PIEVECT_O_SPIB_TX_INT           0x1CCU   // 6.4 - SPIB Transmit Interrupt
#define PIEVECT_O_PIE22_RESERVED_INT    0x1D0U   // 6.5 - Reserved
#define PIEVECT_O_PIE23_RESERVED_INT    0x1D4U   // 6.6 - Reserved
#define PIEVECT_O_PIE24_RESERVED_INT    0x1D8U   // 6.7 - Reserved
#define PIEVECT_O_PIE25_RESERVED_INT    0x1DCU   // 6.8 - Reserved
#define PIEVECT_O_PIE67_RESERVED_INT    0x1E0U   // 6.9 - Reserved
#define PIEVECT_O_PIE68_RESERVED_INT    0x1E4U   // 6.10 - Reserved
#define PIEVECT_O_PIE69_RESERVED_INT    0x1E8U   // 6.11 - Reserved
#define PIEVECT_O_PIE70_RESERVED_INT    0x1ECU   // 6.12 - Reserved
#define PIEVECT_O_SDFM2DR1_INT          0x1F0U   // 6.13 - SDFM2DR1 Interrupt
#define PIEVECT_O_SDFM2DR2_INT          0x1F4U   // 6.14 - SDFM2DR2 Interrupt
#define PIEVECT_O_SDFM2DR3_INT          0x1F8U   // 6.15 - SDFM2DR3 Interrupt
#define PIEVECT_O_SDFM2DR4_INT          0x1FCU   // 6.16 - SDFM2DR4 Interrupt
#define PIEVECT_O_DMA_CH1_INT           0x200U   // 7.1 - DMA Channel 1 Interrupt
#define PIEVECT_O_DMA_CH2_INT           0x204U   // 7.2 - DMA Channel 2 Interrupt
#define PIEVECT_O_DMA_CH3_INT           0x208U   // 7.3 - DMA Channel 3 Interrupt
#define PIEVECT_O_DMA_CH4_INT           0x20CU   // 7.4 - DMA Channel 4 Interrupt
#define PIEVECT_O_DMA_CH5_INT           0x210U   // 7.5 - DMA Channel 5 Interrupt
#define PIEVECT_O_DMA_CH6_INT           0x214U   // 7.6 - DMA Channel 6 Interrupt
#define PIEVECT_O_PIE26_RESERVED_INT    0x218U   // 7.7 - Reserved
#define PIEVECT_O_PIE27_RESERVED_INT    0x21CU   // 7.8 - Reserved
#define PIEVECT_O_PIE71_RESERVED_INT    0x220U   // 7.9 - Reserved
#define PIEVECT_O_PIE72_RESERVED_INT    0x224U   // 7.10 - Reserved
#define PIEVECT_O_FSITXA1_INT           0x228U   // 7.11 - FSITXA_INT1 Interrupt
#define PIEVECT_O_FSITXA2_INT           0x22CU   // 7.12 - FSITXA_INT2 Interrupt
#define PIEVECT_O_FSIRXA1_INT           0x230U   // 7.13 - FSIRXA_INT1 Interrupt
#define PIEVECT_O_FSIRXA2_INT           0x234U   // 7.14 - FSIRXA_INT2 Interrupt
#define PIEVECT_O_PIE73_RESERVED_INT    0x238U   // 7.15 - Reserved
#define PIEVECT_O_DCC0_INT              0x23CU   // 7.16 - DCC0 Interrupt
#define PIEVECT_O_I2CA_INT              0x240U   // 8.1 - I2CA Interrupt 1
#define PIEVECT_O_I2CA_FIFO_INT         0x244U   // 8.2 - I2CA Interrupt 2
#define PIEVECT_O_I2CB_INT              0x248U   // 8.3 - I2CB Interrupt 1
#define PIEVECT_O_I2CB_FIFO_INT         0x24CU   // 8.4 - I2CB Interrupt 2
#define PIEVECT_O_PIE28_RESERVED_INT    0x250U   // 8.5 - Reserved
#define PIEVECT_O_PIE29_RESERVED_INT    0x254U   // 8.6 - Reserved
#define PIEVECT_O_PIE30_RESERVED_INT    0x258U   // 8.7 - Reserved
#define PIEVECT_O_PIE31_RESERVED_INT    0x25CU   // 8.8 - Reserved
#define PIEVECT_O_LINA_0_INT            0x260U   // 8.9 - LINA Interrupt0
#define PIEVECT_O_LINA_1_INT            0x264U   // 8.10 - LINA Interrupt1
#define PIEVECT_O_LINB_0_INT            0x268U   // 8.11 - LINB Interrupt0
#define PIEVECT_O_LINB_1_INT            0x26CU   // 8.12 - LINB Interrupt1
#define PIEVECT_O_PMBUSA_INT            0x270U   // 8.13 - PMBUSA Interrupt
#define PIEVECT_O_PIE74_RESERVED_INT    0x274U   // 8.14 - Reserved
#define PIEVECT_O_PIE75_RESERVED_INT    0x278U   // 8.15 - Reserved
#define PIEVECT_O_DCC1_INT              0x27CU   // 8.16 - DCC1 Interrupt
#define PIEVECT_O_SCIA_RX_INT           0x280U   // 9.1 - SCIA Receive Interrupt
#define PIEVECT_O_SCIA_TX_INT           0x284U   // 9.2 - SCIA Transmit Interrupt
#define PIEVECT_O_SCIB_RX_INT           0x288U   // 9.3 - SCIB Receive Interrupt
#define PIEVECT_O_SCIB_TX_INT           0x28CU   // 9.4 - SCIB Transmit Interrupt
#define PIEVECT_O_CANA0_INT             0x290U   // 9.5 - CANA Interrupt 0
#define PIEVECT_O_CANA1_INT             0x294U   // 9.6 - CANA Interrupt 1
#define PIEVECT_O_PIE32_RESERVED_INT    0x298U   // 9.7 - Reserved
#define PIEVECT_O_PIE33_RESERVED_INT    0x29CU   // 9.8 - Reserved
#define PIEVECT_O_MCANA_0_INT           0x2A0U   // 9.9 - MCAN Sub-System Interrupt 0
#define PIEVECT_O_MCANA_1_INT           0x2A4U   // 9.10 - MCAN Sub-System Interrupt 1
#define PIEVECT_O_MCANA_ECC_INT         0x2A8U   // 9.11 - MCAN Sub-System ECC error Interrupt
#define PIEVECT_O_MCANA_WAKE_INT        0x2ACU   // 9.12 - MCAN Sub-System wakeup Interrupt
#define PIEVECT_O_BGCRC_INT             0x2B0U   // 9.13 - BGCRC_CPU
#define PIEVECT_O_PIE76_RESERVED_INT    0x2B4U   // 9.14 - Reserved
#define PIEVECT_O_PIE77_RESERVED_INT    0x2B8U   // 9.15 - Reserved
#define PIEVECT_O_HICA_INT              0x2BCU   // 9.16 - HICA Interrupt
#define PIEVECT_O_ADCA_EVT_INT          0x2C0U   // 10.1 - ADCA Event Interrupt
#define PIEVECT_O_ADCA2_INT             0x2C4U   // 10.2 - ADCA Interrupt 2
#define PIEVECT_O_ADCA3_INT             0x2C8U   // 10.3 - ADCA Interrupt 3
#define PIEVECT_O_ADCA4_INT             0x2CCU   // 10.4 - ADCA Interrupt 4
#define PIEVECT_O_ADCB_EVT_INT          0x2D0U   // 10.5 - ADCB Event Interrupt
#define PIEVECT_O_ADCB2_INT             0x2D4U   // 10.6 - ADCB Interrupt 2
#define PIEVECT_O_ADCB3_INT             0x2D8U   // 10.7 - ADCB Interrupt 3
#define PIEVECT_O_ADCB4_INT             0x2DCU   // 10.8 - ADCB Interrupt 4
#define PIEVECT_O_ADCC_EVT_INT          0x2E0U   // 10.9 - ADCC Event Interrupt
#define PIEVECT_O_ADCC2_INT             0x2E4U   // 10.10 - ADCC Interrupt 2
#define PIEVECT_O_ADCC3_INT             0x2E8U   // 10.11 - ADCC Interrupt 3
#define PIEVECT_O_ADCC4_INT             0x2ECU   // 10.12 - ADCC Interrupt 4
#define PIEVECT_O_PIE78_RESERVED_INT    0x2F0U   // 10.13 - Reserved
#define PIEVECT_O_PIE79_RESERVED_INT    0x2F4U   // 10.14 - Reserved
#define PIEVECT_O_PIE80_RESERVED_INT    0x2F8U   // 10.15 - Reserved
#define PIEVECT_O_PIE81_RESERVED_INT    0x2FCU   // 10.16 - Reserved
#define PIEVECT_O_CLA1_1_INT            0x300U   // 11.1 - CLA1 Interrupt 1
#define PIEVECT_O_CLA1_2_INT            0x304U   // 11.2 - CLA1 Interrupt 2
#define PIEVECT_O_CLA1_3_INT            0x308U   // 11.3 - CLA1 Interrupt 3
#define PIEVECT_O_CLA1_4_INT            0x30CU   // 11.4 - CLA1 Interrupt 4
#define PIEVECT_O_CLA1_5_INT            0x310U   // 11.5 - CLA1 Interrupt 5
#define PIEVECT_O_CLA1_6_INT            0x314U   // 11.6 - CLA1 Interrupt 6
#define PIEVECT_O_CLA1_7_INT            0x318U   // 11.7 - CLA1 Interrupt 7
#define PIEVECT_O_CLA1_8_INT            0x31CU   // 11.8 - CLA1 Interrupt 8
#define PIEVECT_O_PIE82_RESERVED_INT    0x320U   // 11.9 - Reserved
#define PIEVECT_O_PIE83_RESERVED_INT    0x324U   // 11.10 - Reserved
#define PIEVECT_O_PIE84_RESERVED_INT    0x328U   // 11.11 - Reserved
#define PIEVECT_O_PIE85_RESERVED_INT    0x32CU   // 11.12 - Reserved
#define PIEVECT_O_PIE86_RESERVED_INT    0x330U   // 11.13 - Reserved
#define PIEVECT_O_PIE87_RESERVED_INT    0x334U   // 11.14 - Reserved
#define PIEVECT_O_PIE88_RESERVED_INT    0x338U   // 11.15 - Reserved
#define PIEVECT_O_PIE89_RESERVED_INT    0x33CU   // 11.16 - Reserved
#define PIEVECT_O_XINT3_INT             0x340U   // 12.1 - XINT3 Interrupt
#define PIEVECT_O_XINT4_INT             0x344U   // 12.2 - XINT4 Interrupt
#define PIEVECT_O_XINT5_INT             0x348U   // 12.3 - XINT5 Interrupt
#define PIEVECT_O_MPOST_INT             0x34CU   // 12.4 - MPOST Interrupt
#define PIEVECT_O_FMC_INT               0x350U   // 12.5 - Flash Wrapper Operation Done Interrupt
#define PIEVECT_O_PIE34_RESERVED_INT    0x354U   // 12.6 - Reserved
#define PIEVECT_O_FPU_OVERFLOW_INT      0x358U   // 12.7 - FPU Overflow Interrupt
#define PIEVECT_O_FPU_UNDERFLOW_INT     0x35CU   // 12.8 - FPU Underflow Interrupt
#define PIEVECT_O_PIE90_RESERVED_INT    0x360U   // 12.9 - Reserved
#define PIEVECT_O_RAM_CORR_ERR_INT      0x364U   // 12.10 - RAM Correctable Error Interrupt
#define PIEVECT_O_FLASH_CORR_ERR_INT    0x368U   // 12.11 - Flash Correctable Error Interrupt
#define PIEVECT_O_RAM_ACC_VIOL_INT      0x36CU   // 12.12 - RAM Access Violation Interrupt
#define PIEVECT_O_AES_SINTREQUEST_INT   0x370U   // 12.13 - AES Secure Interrupt request
#define PIEVECT_O_BGCRC_CLA1_INT        0x374U   // 12.14 - BGCRC CLA1
#define PIEVECT_O_CLA_OVERFLOW_INT      0x378U   // 12.15 - CLA Overflow Interrupt
#define PIEVECT_O_CLA_UNDERFLOW_INT     0x37CU   // 12.16 - CLA Underflow Interrupt




#endif
