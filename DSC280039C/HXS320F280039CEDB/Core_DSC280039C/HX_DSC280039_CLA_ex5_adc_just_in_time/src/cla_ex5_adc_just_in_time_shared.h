//#############################################################################
//
// FILE:   cla_ex5_adc_just_in_time_shared.h
//
// TITLE:  Just-in-time ADC sampling with CLA
//
// This header file contains defines, variables and prototypes that are shared
// among the C28x and the CLA
//
//#############################################################################

#ifndef _CLA_EX5_ADCJUSTINTIME_SHARED_H_
#define _CLA_EX5_ADCJUSTINTIME_SHARED_H_

#ifdef __cplusplus
extern "C" {
#endif

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define EPWM1_FREQ          1000000UL   // 1 MHz
#define EPWM1_PERIOD        (uint32_t)(DEVICE_SYSCLK_FREQ / (EPWM1_FREQ))

//
// Globals
//
//Task 1 (C) Variables

//Task 2 (C) Variables

//Task 3 (C) Variables

//Task 4 (C) Variables

//Task 5 (C) Variables

//Task 6 (C) Variables

//Task 7 (C) Variables

//Task 8 (C) Variables

//Common (C) Variables


//
// Function Prototypes
//
extern void _CLA_MTVEC_INIT();
extern void  CLA_delay(uint32_t count);

__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task1();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task2();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task3();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task4();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task5();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task6();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task7();
__interrupt  CODE_SECTION("Cla1Prog") void Cla1Task8();

#ifdef __cplusplus
}
#endif // extern "C"
#endif //_CLA_EX5_ADCJUSTINTIME_SHARED_H_

//
// End of File
//
