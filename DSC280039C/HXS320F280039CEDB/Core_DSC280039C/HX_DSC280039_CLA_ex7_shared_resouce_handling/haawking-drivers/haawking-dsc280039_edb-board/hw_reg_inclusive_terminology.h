//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F280039CEDB, Hal DriverLib, 1.0.0
//
// Release time: 2023-12-14 09:30:36.972564
//
//#############################################################################

#ifndef HW_REG_INCLUSIVE_TERMINOLOGY_H
#define HW_REG_INCLUSIVE_TERMINOLOGY_H

//*****************************************************************************
// PMBUS
//*****************************************************************************
#define PMBUS_O_PMBCCR                   PMBUS_O_PMBMC
#define PMBUS_O_PMBTCR                   PMBUS_O_PMBSC
#define PMBUS_O_PMBHTA                   PMBUS_O_PMBHSA

#define PMBUS_PMBCCR_RW                  PMBUS_PMBMC_RW
#define PMBUS_PMBCCR_TARGET_ADDR_S       PMBUS_PMBMC_SLAVE_ADDR_S
#define PMBUS_PMBCCR_TARGET_ADDR_M       PMBUS_PMBMC_SLAVE_ADDR_M
#define PMBUS_PMBCCR_BYTE_COUNT_S        PMBUS_PMBMC_BYTE_COUNT_S
#define PMBUS_PMBCCR_BYTE_COUNT_M        PMBUS_PMBMC_BYTE_COUNT_M
#define PMBUS_PMBCCR_CMD_ENA             PMBUS_PMBMC_CMD_ENA
#define PMBUS_PMBCCR_EXT_CMD             PMBUS_PMBMC_EXT_CMD
#define PMBUS_PMBCCR_PEC_ENA             PMBUS_PMBMC_PEC_ENA
#define PMBUS_PMBCCR_GRP_CMD             PMBUS_PMBMC_GRP_CMD
#define PMBUS_PMBCCR_PRC_CALL            PMBUS_PMBMC_PRC_CALL

#define PMBUS_PMBTCR_TARGET_ADDR_S       PMBUS_PMBSC_SLAVE_ADDR_S
#define PMBUS_PMBTCR_TARGET_ADDR_M       PMBUS_PMBSC_SLAVE_ADDR_M
#define PMBUS_PMBTCR_MAN_TARGET_ACK      PMBUS_PMBSC_MAN_SLAVE_ACK
#define PMBUS_PMBTCR_TARGET_MASK_S       PMBUS_PMBSC_SLAVE_MASK_S
#define PMBUS_PMBTCR_TARGET_MASK_M       PMBUS_PMBSC_SLAVE_MASK_M
#define PMBUS_PMBTCR_PEC_ENA             PMBUS_PMBSC_PEC_ENA
#define PMBUS_PMBTCR_TX_COUNT_S          PMBUS_PMBSC_TX_COUNT_S
#define PMBUS_PMBTCR_TX_COUNT_M          PMBUS_PMBSC_TX_COUNT_M
#define PMBUS_PMBTCR_TX_PEC              PMBUS_PMBSC_TX_PEC
#define PMBUS_PMBTCR_MAN_CMD             PMBUS_PMBSC_MAN_CMD
#define PMBUS_PMBTCR_RX_BYTE_ACK_CNT_S   PMBUS_PMBSC_RX_BYTE_ACK_CNT_S
#define PMBUS_PMBTCR_RX_BYTE_ACK_CNT_M   PMBUS_PMBSC_RX_BYTE_ACK_CNT_M

#define PMBUS_PMBHTA_TARGET_RW           PMBUS_PMBHSA_SLAVE_RW
#define PMBUS_PMBHTA_TARGET_ADDR_S       PMBUS_PMBHSA_SLAVE_ADDR_S
#define PMBUS_PMBHTA_TARGET_ADDR_M       PMBUS_PMBHSA_SLAVE_ADDR_M

#define PMBUS_PMBCTRL_CONTROLLER_EN      PMBUS_PMBCTRL_MASTER_EN
#define PMBUS_PMBCTRL_TARGET_EN          PMBUS_PMBCTRL_SLAVE_EN

#define PMBUS_PMBSTS_CONTROLLER          PMBUS_PMBSTS_MASTER
#define PMBUS_PMBSTS_TARGET_ADDR_READY   PMBUS_PMBSTS_SLAVE_ADDR_READY

#define PMBUS_PMBINTM_TARGET_ADDR_READY  PMBUS_PMBINTM_SLAVE_ADDR_READY

//*****************************************************************************
// FSI
//*****************************************************************************
#define FSI_O_TX_MAIN_CTRL               FSI_O_TX_MASTER_CTRL
#define FSI_O_RX_MAIN_CTRL               FSI_O_RX_MASTER_CTRL

#define FSI_TX_MAIN_CTRL_CORE_RST        FSI_TX_MASTER_CTRL_CORE_RST
#define FSI_TX_MAIN_CTRL_FLUSH           FSI_TX_MASTER_CTRL_FLUSH
#define FSI_TX_MAIN_CTRL_KEY_S           FSI_TX_MASTER_CTRL_KEY_S
#define FSI_TX_MAIN_CTRL_KEY_M           FSI_TX_MASTER_CTRL_KEY_M

#define FSI_RX_MAIN_CTRL_CORE_RST        FSI_RX_MASTER_CTRL_CORE_RST
#define FSI_RX_MAIN_CTRL_INT_LOOPBACK    FSI_RX_MASTER_CTRL_INT_LOOPBACK
#define FSI_RX_MAIN_CTRL_SPI_PAIRING     FSI_RX_MASTER_CTRL_SPI_PAIRING
#define FSI_RX_MAIN_CTRL_INPUT_ISOLATE   FSI_RX_MASTER_CTRL_INPUT_ISOLATE
#define FSI_RX_MAIN_CTRL_DATA_FILTER_EN  FSI_RX_MASTER_CTRL_DATA_FILTER_EN
#define FSI_RX_MAIN_CTRL_KEY_S           FSI_RX_MASTER_CTRL_KEY_S
#define FSI_RX_MAIN_CTRL_KEY_M           FSI_RX_MASTER_CTRL_KEY_M

//*****************************************************************************
// SPI
//*****************************************************************************
#define SPI_CTL_CONTROLLER_PERIPHERAL    SPI_CTL_MASTER_SLAVE
#define SPI_PRI_PTEINV                   SPI_PRI_STEINV

//*****************************************************************************
// I2C
//*****************************************************************************
#define I2C_O_TAR         I2C_O_SAR

#define I2C_TAR_TAR_S     I2C_SAR_SAR_S
#define I2C_TAR_TAR_M     I2C_SAR_SAR_M

#define I2C_IER_AAT       I2C_IER_AAS

#define I2C_STR_AAT       I2C_STR_AAS
#define I2C_STR_TDIR      I2C_STR_SDIR

#define I2C_MDR_CNT       I2C_MDR_MST

//*****************************************************************************
// LIN
//*****************************************************************************
#define LIN_SCIGCR1_CLK_COMMANDER       LIN_SCIGCR1_CLK_MASTER

#define LIN_ID_IDRESPONDERTASKBYTE_S    LIN_ID_IDSLAVETASKBYTE_S
#define LIN_ID_IDRESPONDERTASKBYTE_M    LIN_ID_IDSLAVETASKBYTE_M


#endif // HW_REG_INCLUSIVE_TERMINOLOGY_H
