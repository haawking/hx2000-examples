//#############################################################################
//
// FILE:   cla_ex7_shared_resource_handling.h
//
// TITLE:  Handling shared resources across C28x and CLA
//
// This header file contains defines, variables and prototypes that are shared
// among the C28x and the CLA
//
//#############################################################################

#ifndef _CLA_EX7_SHARED_RESOURCE_HANDLING__H_
#define _CLA_EX7_SHARED_RESOURCE_HANDLING__H_

#ifdef __cplusplus
extern "C" {
#endif

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

//
// Defines
//
#define EPWM4_FREQ          10000UL    // 10 KHz
#define EPWM5_FREQ          100000UL   // 100 KHz
#define EPWM4_PERIOD        (uint32_t)(DEVICE_SYSCLK_FREQ / EPWM4_FREQ)
#define EPWM5_PERIOD        (uint32_t)(DEVICE_SYSCLK_FREQ / EPWM5_FREQ)

//CLA C Tasks defined in Cla1Tasks_C.cla
extern void _CLA_MTVEC_INIT();
extern void  CLA_delay(uint32_t count);

__interrupt void CODE_SECTION("Cla1Prog") Cla1Task1();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task2();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task3();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task4();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task5();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task6();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task7();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task8();

#ifdef __cplusplus
}
#endif // extern "C"
#endif //_CLA_EX7_SHARED_RESOURCE_HANDLING__H_

//
// End of File
//
