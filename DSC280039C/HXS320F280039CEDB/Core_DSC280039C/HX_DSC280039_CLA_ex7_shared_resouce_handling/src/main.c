/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
//#############################################################################
//
// FILE:   cla_ex7_shared_resource_handling.c
//
// TITLE:  Handling shared resources across C28x and CLA
//
//! \addtogroup driver_example_list
//! <h1>  Handling shared resources across C28x and CLA </h1>
//!

//! 本示例展示了如何处理C28x和CLA之间的共享资源挑战。
//! 由于外设在CLA和CPU之间共享，它们对寄存器的重叠读-修改-写操作可能导致
//! 数据竞争条件，最终导致数据违规或不正确的功能。
//! 在本示例中，CPU ISR和CLA任务独立运行。CPU ISR由EPWM4 @10KHz触发，
//! 并通过软件控制AQCSFRC的CSFB位来切换EPWM1B输出。CLA任务由EPWM5 @100Khz触发，
//! 并通过软件控制AQCSFRC的CSFA位来切换EPWM1A输出。因此，在这个过程中，
//! CPU和CLA分别以不同的频率对AQCSFRC寄存器进行读-修改-写操作，因此存在竞争
//! 条件的可能性，并且由于其中一个更新可能会丢失/被覆盖。
//! 通过将"phase_shift_ON"更新为0U，并在示波器上探测EPWM1A和1B输出，
//! 可以清楚地观察到这一点。
//!

//! 这是一个标准的临界区问题，可以通过软件握手机制（如互斥锁等）来处理。
//! 但大多数实时控制应用程序都对时间敏感，无法承受额外的软件开销，
//! 因此本示例提出了一种基于硬件的替代技术，以避免CPU和CLA之间的共享资源冲突。
//! EPWM模块的相位移位机制被用来调度所需的CLA任务和CPU ISR。
//! EPWM4在每个ZERO事件生成同步脉冲，并为EPWM5提供20个周期的相位移位。
//! 这样，CLA任务和C28x ISR都以原始频率运行，即100KHz和10KHz，但CLA任务
//! 相对于CPU ISR领先20个周期的相位偏移。因此，永远不会发生对AQCSFRC的并发
//! 读-修改-写操作，而EPWM1A和EPWM1B输出的行为将如期望的那样，
//! 即在EPWM1A上产生一致的50 KHz PWM输出，在EPWM1B上产生5 KHz PWM输出，
//! 两者的占空比约为50%。为了在本示例中利用这种相位移位机制，
//! 请确保"phase_shift_ON"设置为1。
//!
//!
//! \b External \b Connections \n
//!  - Observe GPIO0 (EPWM1A Output) on oscilloscope
//!  - Observe GPIO1 (EPWM1B Output) on oscilloscope
//!  - Observe GPIO2 (CLA Task Profiling) on oscilloscope
//!  - Observe GPIO3 (CPU ISR Profiling) on oscilloscope
//!
//!
//! Note :- The phase offset value can easily be configured by updating TBPHS
//!         register to schedule the CLA task and C28x ISR as desired depending
//!         upon the application need so as to avoid overlapping register writes
//!         by CPU and CLA
//!
//! Note :- The optimization is on and set to O2 for the project and all the results
//!         quoted correspond to this case.
//!
//
//#############################################################################
 
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "hx_intrinsics.h"
#include "IQmathLib.h"
#include "syscalls.h"
#include "CLAmath.h"
#include <cla_ex7_shared_resource_handling.h>
#include "board.h"
//
// Defines
//
#define phase_shift_ON 1U

//
// Global variables
//
volatile uint16_t cpu_flag = 0; // Flag used in ISR to implement toggle of
                                // EPWM1B output

//
// Function Prototypes
//
void initEPWM(void);
void setupADCSOC_trigger(void);
void setuploop1ProfileGpio(void);
void setuploop2ProfileGpio(void);
void initCLA(void);
__interrupt void CODE_SECTION("ramfuncs") cpuISR(void);


//
// Main
//
int main(void)
{
    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();

    //
    // GPIO0 is set to EPWM1A and GPIO1 is set to EPWM1B
    //
    GPIO_setControllerCore(0, GPIO_CORE_CPU1);
    GPIO_setPadConfig(0,GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_0_EPWM1_A);
    GPIO_setControllerCore(1, GPIO_CORE_CPU1);
    GPIO_setPadConfig(1,GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_1_EPWM1_B);

    //
    // Setup Profiling GPIOs for loop1 and loop2
    //
    setuploop1ProfileGpio();
    setuploop2ProfileGpio();

    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Disable sync(Freeze clock to PWM as well)
    //
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // Initialize EPWM1, EPWM4 and EPWM5 modules
    //
    initEPWM();

    //
    // Initialize resources
    //
    Board_init();
    initCLA();

    //
    // Register the EPWM4 interrupt
    //
    Interrupt_register(INT_EPWM4, &cpuISR);

    //
    // Enable EPWM4 CPU interrupt
    //
    Interrupt_enable(INT_EPWM4);

    //
    // Enable global interrupts.
    //
    EINT;

    //
    // Enable sync and clock to PWM
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    for(;;)
    {

    }

    return 0;
}

//
// EPWM Initialization
// EPWM1 : generates software controlled outputs at A & B
// EPWM4 : triggers CPU interrupt @ 10KHz
// EPWM5 : triggers CLA task 1 @ 100KHz
//
void initEPWM(void)
{
    //
    // Set up EPWM1 to
    // - run on a base clock of SYSCLK
    // - run in count up-down mode
    // - Max period
    // - Immediate load of SW actions
    //
    EPWM_setClockPrescaler(EPWM1_BASE, EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);
    EPWM_setTimeBasePeriod(EPWM1_BASE, 0xFFFF);
    EPWM_setTimeBaseCounterMode(EPWM1_BASE, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_setTimeBaseCounter(EPWM1_BASE, 0U);
    EPWM_setActionQualifierContSWForceShadowMode(EPWM1_BASE, EPWM_AQ_SW_IMMEDIATE_LOAD);

    //
    // Run freely in emulation mode
    //
    EPWM_setEmulationMode(EPWM1_BASE, EPWM_EMULATION_FREE_RUN);


    //
    // Set up EPWM4 to
    // - run on a base clock of SYSCLK
    // - have a period of EPWM4_PERIOD (10 KHz)
    // - run in count up mode
    // - Generate a SYNCOUT pulse if phase shifting is used
    // - Generate an interrupt to CPU on very TBCTR=ZERO event
    EPWM_setClockPrescaler(EPWM4_BASE, EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);
    EPWM_setTimeBasePeriod(EPWM4_BASE, EPWM4_PERIOD - 1U);
    EPWM_setTimeBaseCounterMode(EPWM4_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBaseCounter(EPWM4_BASE, 0U);
#if phase_shift_ON == 1U
    EPWM_enableSyncOutPulseSource(EPWM4_BASE, EPWM_SYNC_OUT_PULSE_ON_CNTR_ZERO);
#endif
    EPWM_setInterruptSource(EPWM4_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM4_BASE);
    EPWM_setInterruptEventCount(EPWM4_BASE, 1U);

    //
    // Set up EPWM5 to
    // - run on a base clock of SYSCLK
    // - have a period of EPWM4_PERIOD (100 KHz)
    // - run in count up mode
    // - Enable phase shift of 20 cycles on receiving a
    // - SYNC pulse if phase shifting is used
    // - Trigger a CLA task on every TBCTR=ZERO event
    EPWM_setClockPrescaler(EPWM5_BASE, EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);
    EPWM_setTimeBasePeriod(EPWM5_BASE, EPWM5_PERIOD - 1U);
    EPWM_setTimeBaseCounterMode(EPWM5_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBaseCounter(EPWM5_BASE, 0U);
#if phase_shift_ON == 1U
    EPWM_enablePhaseShiftLoad(EPWM5_BASE);
    EPWM_setSyncInPulseSource(EPWM5_BASE, EPWM_SYNC_IN_PULSE_SRC_SYNCOUT_EPWM4);
    EPWM_setPhaseShift(EPWM5_BASE, 20U);
    EPWM_enableOneShotSync(EPWM5_BASE);
#endif
    EPWM_setInterruptSource(EPWM5_BASE, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(EPWM5_BASE);
    EPWM_setInterruptEventCount(EPWM5_BASE, 1U);
}

//
// CLA Initialization
//
void initCLA(void)
{
    //
    // Force task 8, the one time initialization task
    //
    CLA_forceTasks(CLA1_BASE, CLA_TASKFLAG_8);
}

//
// Setting up GPIO2 for profiling cla Task
//
void setuploop1ProfileGpio(void)
{
        GPIO_setDirectionMode(2,GPIO_DIR_MODE_OUT);
        GPIO_setQualificationMode(2,GPIO_QUAL_SYNC);
        GPIO_setPinConfig(GPIO_2_GPIO2);
        GPIO_writePin(2,0);
        GPIO_setControllerCore(2, GPIO_CORE_CPU1_CLA1);
}

//
// Setting up GPIO3 for profiling cpuISR
//
void setuploop2ProfileGpio(void)
{
        GPIO_setDirectionMode(3,GPIO_DIR_MODE_OUT);
        GPIO_setQualificationMode(3,GPIO_QUAL_SYNC);
        GPIO_setPinConfig(GPIO_3_GPIO3);
        GPIO_writePin(3,0);
}

//
// CPU ISR
//
__interrupt void cpuISR(void)
{
    //
    // Turn ON Profiling GPIO3
    //
    GPIO_writePin(3,1);

    //
    // Toggle the EPWM1B output using SW Force Action
    //
    if (cpu_flag == 1)
    {
        EPWM_setActionQualifierContSWForceAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_SW_OUTPUT_HIGH);
        cpu_flag = 0;
    }

    else
    {
        EPWM_setActionQualifierContSWForceAction(EPWM1_BASE, EPWM_AQ_OUTPUT_B, EPWM_AQ_SW_OUTPUT_LOW);
        cpu_flag = 1;
    }

    //
    // Turn OFF Profiling GPIO3
    //
    GPIO_writePin(3,0);

    //
    // Clear EPWM4 interrupt flag so that next interrupt can come in
    //
    EPWM_clearEventTriggerInterruptFlag(EPWM4_BASE);

    //
    // Acknowledge the interrupt
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

//
// End of File
//

