//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F280039CEDB, Hal DriverLib, 1.0.0
//
// Release time: 2023-12-14 09:31:07.788600
//
//#############################################################################

#ifndef HW_MEMMAP_H
#define HW_MEMMAP_H


//*****************************************************************************
//
// The following are defines for the base address of the memories and
// peripherals.
//
//*****************************************************************************
#define M0_RAM_BASE               0x00000000U
#define M1_RAM_BASE               0x00000800U
#define ADCARESULT_BASE           0x00108140U
#define ADCBRESULT_BASE           0x00108340U
#define ADCCRESULT_BASE           0x00108540U
#define CPUTIMER0_BASE            0x00101000U
#define CLA1_ONLY_BASE            0x00416000U
#define CPUTIMER1_BASE            0x00101010U
#define CPUTIMER2_BASE            0x00101020U
#define PIECTRL_BASE              0x00101100U
#define CLA1_SOFTINT_BASE         0x00416010U
#define PIEVECTTABLE_BASE         0x00102000U
#define DMA_BASE                  0x00100400U
#define DMA_CH1_BASE              0x00100480U
#define DMA_CH2_BASE              0x00100500U
#define DMA_CH3_BASE              0x00100580U
#define DMA_CH4_BASE              0x00100600U
#define DMA_CH5_BASE              0x00100680U
#define DMA_CH6_BASE              0x00100700U
#define CLA1_BASE                 0x00416040U
#define CLB1_BASE                 0x0021A000U
#define CLB1_LOGICCFG_BASE        0x0021A000U
#define CLB1_LOGICCTRL_BASE       0x0021A100U
#define CLB1_DATAEXCH_BASE        0x0021A180U
#define CLB2_BASE                 0x0021A400U
#define CLB2_LOGICCFG_BASE        0x0021A400U
#define CLB2_LOGICCTRL_BASE       0x0021A500U
#define CLB2_DATAEXCH_BASE        0x0021A580U
#define CLB3_BASE                 0x0021A800U
#define CLB3_LOGICCFG_BASE        0x0021A800U
#define CLB3_LOGICCTRL_BASE       0x0021A900U
#define CLB3_DATAEXCH_BASE        0x0021A980U
#define CLB4_BASE                 0x0021AC00U
#define CLB4_LOGICCFG_BASE        0x0021AC00U
#define CLB4_LOGICCTRL_BASE       0x0021AD00U
#define CLB4_DATAEXCH_BASE        0x0021AD80U
#define EPWM1_BASE                0x00200000U
#define EPWM2_BASE                0x00200400U
#define EPWM3_BASE                0x00200800U
#define EPWM4_BASE                0x00200C00U
#define EPWM5_BASE                0x00201000U
#define EPWM6_BASE                0x00201400U
#define EPWM7_BASE                0x00201800U
#define EPWM8_BASE                0x00201C00U
#define EQEP1_BASE                0x00205900U
#define EQEP2_BASE                0x00205980U
#define ECAP1_BASE                0x00205000U
#define ECAP2_BASE                0x00205080U
#define ECAP3_BASE                0x00205100U
#define HRCAP3_BASE               0x00205180U
#define DACA_BASE                 0x00205800U
#define DACB_BASE                 0x00205880U
#define CMPSS1_BASE               0x00205200U
#define CMPSS2_BASE               0x00205280U
#define CMPSS3_BASE               0x00205300U
#define CMPSS4_BASE               0x00205380U
#define SDFM1_BASE                0x00204000U
#define SDFM2_BASE                0x00204400U
#define SPIA_BASE                 0x00210000U
#define SPIB_BASE                 0x00210400U
#define BGCRC_CPU_BASE            0x00100800U
#define BGCRC_CLA1_BASE           0x00100A00U
#define PMBUSA_BASE               0x00210800U
#define HIC_BASE                  0x00100000U
#define FSITXA_BASE               0x00211000U
#define FSIRXA_BASE               0x00211400U
#define LINA_BASE                 0x00211800U
#define LINB_BASE                 0x00211C00U
#define WD_BASE                   0x00218E00U
#define NMI_BASE                  0x00101300U
#define XINT_BASE                 0x00101200U
#define SCIA_BASE                 0x00215000U
#define SCIB_BASE                 0x00215400U
#define I2CA_BASE                 0x00215800U
#define I2CB_BASE                 0x00215C00U
#define ADCA_BASE                 0x00108000U
#define ADCB_BASE                 0x00108200U
#define ADCC_BASE                 0x00108400U
#define INPUTXBAR_BASE            0x00213000U
#define XBAR_BASE                 0x00213080U
#define SYNCSOC_BASE              0x00218C00U
#define CLBINPUTXBAR_BASE         0x00213180U
#define DMACLASRCSEL_BASE         0x00218D00U
#define EPWMXBAR_BASE             0x00213400U
#define CLBXBAR_BASE              0x00213500U
#define OUTPUTXBAR_BASE           0x00213600U
#define CLBOUTPUTXBAR_BASE        0x00213B00U
#define GPIOCTRL_BASE             0x00214800U
#define GPIODATA_BASE             0x00214E00U
#define GPIODATAREAD_BASE         0x00214F00U
#define LFU_BASE                  0x00218C30U
#define LS0_RAM_BASE              0x00010000U
#define LS1_RAM_BASE              0x00012000U
#define LS2_RAM_BASE              0x00014000U
#define LS3_RAM_BASE              0x00016000U
#define LS4_RAM_BASE              0x00018000U
#define LS5_RAM_BASE              0x0001A000U
#define LS6_RAM_BASE              0x0001C000U
#define LS7_RAM_BASE              0x0001E000U
#define GS0_RAM_BASE              0x00020000U
#define GS1_RAM_BASE              0x00022000U
#define GS2_RAM_BASE              0x00024000U
#define GS3_RAM_BASE              0x00026000U
#define AESA_BASE                 0x00109000U
#define AESA_SS_BASE              0x00109200U
#define CANA_BASE                 0x00103000U
#define CANA_MSG_RAM_BASE         0x00103400U
#define MCAN_MSG_RAM_BASE         0x00104000U
#define MCANA_BASE                0x00106000U
#define MCANA_DRIVER_BASE         0x00104000U
#define MCANASS_BASE              0x00106200U
#define MCANA_ERROR_BASE          0x00106400U
#define DEVCFG_BASE               0x00218400U
#define CLKCFG_BASE               0x00218000U
#define CPUSYS_BASE               0x00218200U
#define SYSSTAT_BASE              0x00218C20U
#define PERIPHAC_BASE             0x00218800U
#define ANALOGSUBSYS_BASE         0x00219000U
#define HWBIST_BASE               0x00100D30U
#define DCC0_BASE                 0x00214400U
#define DCC1_BASE                 0x00214480U
#define ERAD_GLOBAL_BASE          0x00214000U
#define ERAD_HWBP1_BASE           0x002140FCU
#define ERAD_HWBP2_BASE           0x00214110U
#define ERAD_HWBP3_BASE           0x00214124U
#define ERAD_HWBP4_BASE           0x00214138U
#define ERAD_HWBP5_BASE           0x0021414CU
#define ERAD_HWBP6_BASE           0x00214160U
#define ERAD_HWBP7_BASE           0x00214174U
#define ERAD_HWBP8_BASE           0x00214188U
#define ERAD_COUNTER1_BASE        0x0021419CU
#define ERAD_COUNTER2_BASE        0x002141C0U
#define ERAD_COUNTER3_BASE        0x002141E4U
#define ERAD_COUNTER4_BASE        0x00214208U
#define ERAD_CRC_GLOBAL_BASE      0x0021422CU
#define ERAD_CRC1_BASE            0x00214230U
#define ERAD_CRC2_BASE            0x0021423CU
#define ERAD_CRC3_BASE            0x00214248U
#define ERAD_CRC4_BASE            0x00214254U
#define ERAD_CRC5_BASE            0x00214260U
#define ERAD_CRC6_BASE            0x0021426CU
#define ERAD_CRC7_BASE            0x00214278U
#define ERAD_CRC8_BASE            0x00214284U
#define EPG1_BASE                 0x00219800U
#define EPG1MUX_BASE              0x00219880U
#define DCSM_Z1_BASE              0x00219400U
#define DCSM_Z2_BASE              0x00219480U
#define DCSMCOMMON_BASE           0x00219500U
#define MEMCFG_BASE               0x00100C00U
#define ACCESSPROTECTION_BASE     0x00100CD4U
#define MEMORYERROR_BASE          0x00100C84U
#define TESTERROR_BASE            0x00100D24U
#define FLASH0CTRL_BASE           0x00641000U
#define FLASH0ECC_BASE            0x00641200U
#define FLASH1CTRL_BASE          0x006C1000U
#define FLASH1ECC_BASE             0x006C1200U
#define DCSM_Z1OTP_BASE           0x00640000U
#define DCSM_Z2OTP_BASE           0x00640400U
#endif

