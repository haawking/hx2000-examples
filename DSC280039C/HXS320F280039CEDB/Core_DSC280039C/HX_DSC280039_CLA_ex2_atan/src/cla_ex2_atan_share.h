//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F280039CEDB, Hal DriverLib, 1.0.0
//
// Release time: 2023-12-14 09:30:36.972564
//
//#############################################################################

#ifndef _CLA_SHARE_H_
#define _CLA_SHARE_H_


//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "CLAmath.h"
//
// Defines
//
#define BUFFER_SIZE       64
#define TABLE_SIZE        64
#define TABLE_SIZE_M_1    TABLE_SIZE-1
#define INV2PI            0.159154943
//
// Common Functions
//
//
extern void _CLA_MTVEC_INIT();
extern void  CLA_configDefaultClaMemory();
extern void  CLA_delay(uint32_t count);

//
//Task 1 (C) Variables
//
extern float y[];            //Result vector
extern float fVal;           //Holds the input argument to the task
extern float fResult;        //The arsine of the input argument

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//
extern float PIBYTWO;
extern float CLAatan2Table[];


//
// CLA C Tasks
//
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task1();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task2();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task3();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task4();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task5();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task6();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task7();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task8();

#endif /* _CLA_SHARE_H_ */

//
// End of file
//
