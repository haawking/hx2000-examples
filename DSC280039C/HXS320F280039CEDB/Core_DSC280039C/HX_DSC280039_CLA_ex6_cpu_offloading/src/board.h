
#ifndef BOARD_H
#define BOARD_H

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//
// Included Files
//

#include "driverlib.h"
#include "device.h"

//*****************************************************************************
//
// PinMux Configurations
//
//*****************************************************************************

//*****************************************************************************
//
// CLA Configurations
//
//*****************************************************************************
#define myCLA0_BASE CLA1_BASE

//
// The following are symbols defined in the CLA assembly code
// Including them in the shared header file makes them global
// and the main CPU can make use of them.
//
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task1();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task2();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task3();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task4();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task5();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task6();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task7();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task8();
void myCLA0_init();


//*****************************************************************************
//
// MEMCFG Configurations
//
//*****************************************************************************

//*****************************************************************************
//
// Board Configurations
//
//*****************************************************************************
void	Board_init();
void	CLA_init();
void	MEMCFG_init();
void	PinMux_init();

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif  // end of BOARD_H definition
