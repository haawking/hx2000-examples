//#############################################################################
//
// FILE:   cla_ex3_background_nesting_task_shared.h
//
// TITLE:  CLA ADC Sampling and Filtering with Buffering in a Background Task
//
// This header file contains defines, variables and prototypes that are shared
// among the C28x and the CLA
//
//#############################################################################


#ifndef _CLA_EX3_BACKGROUND_NESTING_TASK_H_
#define _CLA_EX3_BACKGROUND_NESTING_TASK_H_

#ifdef __cplusplus
extern "C" {
#endif
//
// Included Files
//

//
// Globals
//
//Task 1 (C) Variables

//Task 2 (C) Variables

//Task 3 (C) Variables

//Task 4 (C) Variables

//Task 5 (C) Variables

//Task 6 (C) Variables

//Task 7 (C) Variables

//Background Task (C) Variables

//Common (C) Variables

//
// Function Prototypes
//
extern void _CLA_MTVEC_INIT();
extern void  CLA_delay(uint32_t count);

//CLA C Tasks defined in Cla1Tasks_C.cla
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task1();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task2();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task3();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task4();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task5();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task6();
__interrupt CODE_SECTION("Cla1Prog") void Cla1Task7();
__interrupt CODE_SECTION("Cla1Prog") void Cla1BackgroundTask();

#ifdef __cplusplus
}
#endif // extern "C"
#endif //_CLA_EX3_BACKGROUND_NESTING_TASK_H_

//
// End of File
//
