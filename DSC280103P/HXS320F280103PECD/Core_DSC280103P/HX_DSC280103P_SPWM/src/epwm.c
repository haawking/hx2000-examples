/******************************************************************
 文 档 名：     epwm.c
 D S P：       DSC28027
 使 用 库：     
 作     用：      
 说     明：      提供epwm.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：功能一：EPWM1/2/3初始化配置
 	 	   功能二：配置pwm波10KHz、死区5us、相位偏差120度输出，TBCTR采用向上向下计数


 版 本：V1.0.x
 时 间：2022年1月19日
 作 者：何洋
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "epwm.h"

void InitEPwm1Example()
{
	/*时钟分频标志位。	0: 不分频  	1: 2分频*/
	EPwm1Regs.CLKDIV.bit.CLKDIV = 1;
	/*配置EPWM输出频率为TBCLK/3000/2=10kHz*/
	EPwm1Regs.TBPRD = 3000;
	/*配置EPWM输出相位不偏移*/
	EPwm1Regs.TBPHS.half.TBPHS = 0;
	/*配置EPWM的TBCTR计数初值为0*/
	EPwm1Regs.TBCTR = 0x0000;

	/*配置EPWM的占空比输出为50%*/
	EPwm1Regs.CMPA.half.CMPA = 1500;
	EPwm1Regs.CMPB = 0;

	/*配置EPWM的TBCTR采用向上向下计数*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*配置EPWM输出不装载相位偏移*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*配置EPWM时基频率TBCLK为系统时钟，不进行分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*比较模块CMPA采用影子寄存器装载模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*比较模块CMPA从CTR=0时开始装载*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*TBCTR向上计数时，达到CMPA事件，EPWM1A产生置高动作*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*TBCTR向下计数时，达到CMPA事件，EPWM1A产生置低动作*/
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*TBCTR向下计数时，达到CMPB事件，EPWM1B产生置低动作*/
	EPwm1Regs.AQCTLB.bit.CBD = AQ_CLEAR;
	/*TBCTR向上计数时，达到CMPB事件，EPWM1B产生置高动作*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;

	/*中断事件选择，当CTR=0时开始产生事件中断*/
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	/*中断事件选择，事件中断的使能信号*/
	EPwm1Regs.ETSEL.bit.INTEN = 1;
	/*中断事件分频配置，一周期产生1次中断*/
	EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;

	/*
	 HALFCYCLE 位无用， 配置无效；
	 HALFEN 位=0： 死区采用 2 倍 TBCLK 时钟；
	 HALFEN 位=1： 死区采用 TBCLK 时钟；
	 */
//	EPwm1Regs.DBCTL.bit.HALFEN = 1;
//	EPwm1Regs.DBCTL.bit.HALFCYCLE = 1;

	/*死区输出模式配置，配置输出双边沿延时*/
	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*死区极性配置，配置EPWMxA输出置高，EPWMxB输出置低*/
	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/*死区输入模式配置，配置输入的上升沿与下降沿延时来源为EPWMxA*/
	EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;

	/*死区时间配置，配置输出上升沿延时5us*/
	EPwm1Regs.DBRED = 300;
	/*死区时间配置，配置输出下降沿延时5us*/
	EPwm1Regs.DBFED = 300;
}

void InitEPwm2Example()
{
	/*时钟分频标志位。	0: 不分频  	1: 2分频*/
	EPwm2Regs.CLKDIV.bit.CLKDIV = 1;
	/*配置EPWM输出频率为TBCLK/3000/2=10kHz*/
	EPwm2Regs.TBPRD = 3000;
	/*配置EPWM输出相位偏移2*pi/3，一周期的1/3*/
	EPwm2Regs.TBPHS.half.TBPHS = 1000;
	EPwm2Regs.TBCTR = 0x0000;

	EPwm2Regs.CMPA.half.CMPA = 1500;
	EPwm2Regs.CMPB = 0;

	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*配置EPWM输出不装载相位偏移，已配置输出相位偏移，故无需装载*/
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	EPwm2Regs.AQCTLB.bit.CBD = AQ_CLEAR;
	EPwm2Regs.AQCTLB.bit.CBU = AQ_SET;

	/*
	 HALFCYCLE 位无用， 配置无效；
	 HALFEN 位=0： 死区采用 2 倍 TBCLK 时钟；
	 HALFEN 位=1： 死区采用 TBCLK 时钟；
	 */
//	EPwm2Regs.DBCTL.bit.HALFEN = 1;
//	EPwm2Regs.DBCTL.bit.HALFCYCLE = 1;

	/*死区输出模式配置，配置输出双边沿延时*/
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*死区极性配置，配置EPWMxA输出置高，EPWMxB输出置低*/
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/*死区输入模式配置，配置输入的上升沿与下降沿延时来源为EPWMxA*/
	EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;

	/*死区时间配置，配置输出上升沿延时5us*/
	EPwm2Regs.DBRED = 300;
	/*死区时间配置，配置输出下降沿延时5us*/
	EPwm2Regs.DBFED = 300;
}

void InitEPwm3Example()
{
	/*时钟分频标志位。	0: 不分频  	1: 2分频*/
	EPwm3Regs.CLKDIV.bit.CLKDIV = 1;
	/*配置EPWM输出频率为TBCLK/3000/2=10kHz*/
	EPwm3Regs.TBPRD = 3000;
	/*配置EPWM输出相位偏移4*pi/3，一周期的2/3*/
	EPwm3Regs.TBPHS.half.TBPHS = 2000;
	EPwm3Regs.TBCTR = 0x0000;

	EPwm3Regs.CMPA.half.CMPA = 1500;
	EPwm3Regs.CMPB = 0;

	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*配置EPWM输出不装载相位偏移，已配置输出相位偏移，故无需装载*/
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	EPwm3Regs.AQCTLB.bit.CBD = AQ_CLEAR;
	EPwm3Regs.AQCTLB.bit.CBU = AQ_SET;

	/*
//	 HALFCYCLE 位无用， 配置无效；
//	 HALFEN 位=0： 死区采用 2 倍 TBCLK 时钟；
//	 HALFEN 位=1： 死区采用 TBCLK 时钟；
//	 */
//	EPwm3Regs.DBCTL.bit.HALFEN = 1;
	//	EPwm3Regs.DBCTL.bit.HALFCYCLE = 1;

	/*死区输出模式配置，配置输出双边沿延时*/
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*死区极性配置，配置EPWMxA输出置高，EPWMxB输出置低*/
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/*死区输入模式配置，配置输入的上升沿与下降沿延时来源为EPWMxA*/
	EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;

	/*死区时间配置，配置输出上升沿延时5us*/
	EPwm3Regs.DBRED = 300;
	/*死区时间配置，配置输出下降沿延时5us*/
	EPwm3Regs.DBFED = 300;
}

