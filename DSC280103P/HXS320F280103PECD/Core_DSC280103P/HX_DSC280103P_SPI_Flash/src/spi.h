/*
 * spi.h
 *
 *  Created on: 2021��8��20��
 *      Author: dingxy
 */

#ifndef SPI_H_
#define SPI_H_
#include "dsc_config.h"
#define DummyData 0xa5

extern uint8 upper_128[128];

typedef struct W25Q64Flash_Type
{
	long Jedec_ID;
	uint16 SReg;
	uint16 Addr;
	uint16 Len;
} W25Q64Flash;

extern W25Q64Flash SPIFlash;

void SPI_IOinit(void);
void SPI_fifo_init(void);
void spi_init(void);

unsigned long Jedec_ID_Read(void);
uint16 Read_Status_2Reg(void);

void WREN(void);
void WrSReg(uint16 setReg);

void Wait_Busy(void);
void Chip_Erase(void);

void ReadData(unsigned char Dst, unsigned char *Rxbuf, unsigned long len);
void PageProgram(unsigned char Dst, unsigned char *byte, unsigned char len);

uint8_t Send_Byte(uint16 a);
uint16 Get_Byte(void);

#endif /* SPI_H_ */
