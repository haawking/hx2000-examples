/*
 * TM1650_IIC.h
 *
 *  Created on: 2021年11月9日
 *      Author: liuq
 */

#ifndef TM1650_IIC_H_
#define TM1650_IIC_H_

/*IIC节点数为2个，分别是TM1650和AT24C02*/
#define IIC_NODE_NUM       2
/*ms为单位*/
#define AT24CO2_TIMER_OUT  3

extern char keyVal, keyReg, LigntVal;

// I2C State defines
#define IIC_IDLE               0
#define IIC_WRITE              1
#define IIC_READ_STEP1         2
#define IIC_READ_STEP2         3
#define IIC_READ_STEP3         4
#define IIC_BUS_BUSY           5
#define IIC_BUS_ERROR          6

/*与TM1650有关的物理地址（需要右移动1位进行标准的IIC操作；标准IIC的LSB为读写位，高7位为物理地址位*/
/*显示模式 ADDR*/
#define CMD_SEG  (0X48)>>1
/*按键模式 ADDR 0x24*/
#define CMD_KEY  (0X49)>>1

/*数码管从左到右依次为DIG0,DIG1,DIG2,DIG3*/
#define DIG0  (0X68)>>1
#define DIG1  (0X6A)>>1
#define DIG2  (0X6C)>>1
#define DIG3  (0X6E)>>1

// I2C Message Structure
typedef struct I2CSlaveMSG
{
	/*中断源，测试用*/
	Uint16 IntSRC[10];

	//中断源记录，测试用*/
	Uint16 IntSRCCnt;

	/*自行软件定义的IIC状态*/
	Uint16 MasterStatus;

	/*IIC物理地址（硬件地址）*/
	Uint16 SlavePHYAddress;

	/*类似于EEPROM，需要提供逻辑地址*/
	Uint16 LogicAddr;

	/*操作数据的长度（不含物理地址）*/
	Uint16 Len;

	/*发送数组，最大4个深度*/
	Uint16 MsgInBuffer[4];

	/*接收数组，最大4个深度*/
	Uint16 MsgOutBuffer[4];

	/*软件定义的超时变量，在1ms定时器中计数*/
	Uint16 IIC_TimerOUT;
} I2CSlaveMSG;

extern I2CSlaveMSG *PtrMsg[];

extern I2CSlaveMSG TM1650;
extern char *IIC_ISR_String[];
extern uint8_t SEG7Table[];

void InitI2C_Gpio(void);
void I2CA_Init(void);
Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg);
Uint16 I2CA_Rxdata_STOP(struct I2CSlaveMSG *msg);
void TM1650_Send(char addr, char data);
void TM1650_Read(char addr, char *data);

#endif /* TM1650_IIC_H_ */
