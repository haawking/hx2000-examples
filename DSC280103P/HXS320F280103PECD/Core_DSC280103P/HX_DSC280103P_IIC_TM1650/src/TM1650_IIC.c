/*
 * TM1650_IIC.c
 *
 *  Created on: 2021年11月9日
 *      Author: liuq
 */

#include "dsc_config.h"
#include "TM1650_IIC.h"

/*数码管显示设置：亮度、8段/7段显示、开/关显示*/
char keyVal, LigntVal;

I2CSlaveMSG ICF8591Msg[] = { { { 0 }, 0,
IIC_IDLE, 0, 0, 0, { 0 }, { 0 }, 0 } };

char *IIC_ISR_String[] = { "No", "ARB", "NACK", "ARDY", "RX", "Tx", "SCD", "AAS" };

/*定义一个TM1650结构体*/
I2CSlaveMSG TM1650;

/*定义一个AT24C02Msg结构体*/
I2CSlaveMSG AT24C02Msg;

/*定义一个指针数组，存放I2C结构体，这里包含TM1650和EEPROM*/
I2CSlaveMSG *PtrMsg[] = { &TM1650, &AT24C02Msg };

uint8_t SEG7Table[] = {
/*共阴极*/
/*对应在数码管上显示的数字为：0, 1, 2, 3,*/
0x3f, 0x06, 0x5b, 0x4f,
/*对应在数码管上显示的数字为：4, 5, 6, 7,*/
0x66, 0x6d, 0x7d, 0x07,
/*对应在数码管上显示的数字为：8, 9, a, b,*/
0x7f, 0x6f, 0x77, 0x7c,
/*对应在数码管上显示的数字为：c, d, e, f*/
0x39, 0x5e, 0x79, 0x71 };

/*系统时钟为60MHZ*/
#define SYSCLK 60000000L

#define IICFRE 400000L

INTERRUPT void i2c_int1a_isr(void)
{
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP8;
}

/******************************************************************
 *函数名：void I2CA_Init(void)
 *参 数：无
 *返回值：无
 *作 用：初始化IIC
 ******************************************************************/
void I2CA_Init(void)
{
	/*预分频*/
	I2caRegs.I2CPSC.all = 5;
	/*细分时钟*/
	I2caRegs.I2CCLKL = SYSCLK / (I2caRegs.I2CPSC.all + 1) / IICFRE / 2 - 5;
	I2caRegs.I2CCLKH = SYSCLK / (I2caRegs.I2CPSC.all + 1) / IICFRE / 2 - 5;

	/*Take I2C out of reset*/
	I2caRegs.I2CMDR.all = 0x0020;
	/*Enable FIFO mode and TXFIFO*/
	I2caRegs.I2CFFTX.all = 0x6000;
	/*Enable RXFIFO, clear RXFFINT*/
	I2caRegs.I2CFFRX.all = 0x2040;

	EALLOW;
	PieVectTable.I2CINT1A = &i2c_int1a_isr;
	EDIS;

	IER |= M_INT8;
	EINT;

	return;
}


/******************************************************************
 *函数名：Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
 *参 数：struct I2CSlaveMSG *msg
 *返回值：I2C_STP_NOT_READY_ERROR
 *             I2C_BUS_BUSY_ERROR
 *             I2C_SUCCESS
 *作 用：IIC总线的发送程序
 ******************************************************************/
Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
{
	Uint16 i;

	/*停止位=1，收到发送停止信号，返回“总线准备失败”*/
	if (I2caRegs.I2CMDR.bit.STP == 1)
	{
		return I2C_STP_NOT_READY_ERROR;
	}

	/*检测到“总线忙”状态，返回“I2C总线忙错误”*/
	if (I2caRegs.I2CSTR.bit.BB == 1)
	{
		return I2C_BUS_BUSY_ERROR;
	}

	/*读取I2C所访问的从机地址*/
	I2caRegs.I2CSAR = msg->SlavePHYAddress;

	/*向I2C模块发送数据，控制TM1650芯片*/
	for (i = 0; i < msg->Len; i++)
	{
		I2caRegs.I2CDXR = msg->MsgOutBuffer[i];
	}

	/* 定义I2C发送数据长度*/
	I2caRegs.I2CCNT = msg->Len;

	/*硬件IIC进行操作，STOP信号结束*/
	I2caRegs.I2CMDR.all = 0x2E20;

	return I2C_SUCCESS;
}

/******************************************************************
 *函数名：Uint16 I2CA_Tx_STOP(struct I2CSlaveMSG *msg)
 *参 数：struct I2CSlaveMSG *msg
 *返回值：I2C_SUCCESS
 *作 用：读取EEPROM数据程序
 ******************************************************************/
Uint16 I2CA_Rxdata_STOP(struct I2CSlaveMSG *msg)
{
	I2caRegs.I2CSAR = msg->SlavePHYAddress;

	/*Setup how many bytes to expect*/
	I2caRegs.I2CCNT = msg->Len;

	/*master receiver*/
	I2caRegs.I2CMDR.all = 0x2C20;

	return I2C_SUCCESS;
}

/******************************************************************
 *函数名：void TM1650_Send(char addr,char data)
 *参 数：无
 *返回值：无
 *作 用：通过访问EEPROM向芯片TM1650发送控制数码管显示程序
 ******************************************************************/
void TM1650_Send(char addr, char data)
{
	/*1个数据（不含物理地址）*/
	TM1650.Len = 1;

	/*EEPROM的从机地址*/
	TM1650.SlavePHYAddress = addr;

	/*向EEPROM发送要写入的数据*/
	TM1650.MsgOutBuffer[0] = data;

	/*I2C访问EEPROM写入数据，输出控制TM1650，带STOP*/
	I2CA_Tx_STOP(&TM1650);

	TM1650.IIC_TimerOUT = 0;

	/*主机状态为IIC_WRITE写入*/
	TM1650.MasterStatus = IIC_WRITE;

	/*模块检测到停止信号或复位时，停止发送数据*/
	while (I2caRegs.I2CSTR.bit.SCD== 0)
	{
		/*输出时长*/
		if (TM1650.IIC_TimerOUT > AT24CO2_TIMER_OUT)
		{
			/*主机状态强制为IIC空闲*/
			TM1650.MasterStatus = IIC_IDLE;

			/*IIC发送停止*/
			I2caRegs.I2CMDR.bit.STP = 1;

			/*IIC状态置满，停止发送*/
			I2caRegs.I2CSTR.all = 0xFFFF;

			break;
		}
	}

	/*I2C总线发送停止*/
	I2caRegs.I2CSTR.bit.SCD=1;
}

/******************************************************************
 *函数名：void TM1650_Read(char addr, char *data)
 *参 数：char addr,
 *           char *data
 *返回值：无
 *作 用：通过读取EEPROM，可控制TM1650芯片扫描按键
 ******************************************************************/
void TM1650_Read(char addr, char *data)
{
	TM1650.Len = 1;

	TM1650.SlavePHYAddress = addr;

	/*读取EEPROM*/
	I2CA_Rxdata_STOP(&TM1650);

	TM1650.IIC_TimerOUT = 0;

	/*主机状态I2C读取IIC_READ_STEP2*/
	TM1650.MasterStatus = IIC_READ_STEP2;

	while (I2caRegs.I2CSTR.bit.SCD== 0)
	{
		if (TM1650.IIC_TimerOUT > AT24CO2_TIMER_OUT)
		{
			TM1650.MasterStatus = IIC_IDLE;
			I2caRegs.I2CMDR.bit.STP = 1;
			I2caRegs.I2CSTR.all = 0xFFFF;
			break;
		}
	}

	I2caRegs.I2CSTR.bit.SCD=1;
	TM1650.IIC_TimerOUT = 0;

	/*主机状态为I2C读取第二组数据IIC_READ_STEP3*/
	TM1650.MasterStatus = IIC_READ_STEP3;

	/*等待接收FIFO为空时，读取I2C模块接收的数据*/
	while (I2caRegs.I2CFFRX.bit.RXFFST)
	{
		*data = I2caRegs.I2CDRR;
	}
}
