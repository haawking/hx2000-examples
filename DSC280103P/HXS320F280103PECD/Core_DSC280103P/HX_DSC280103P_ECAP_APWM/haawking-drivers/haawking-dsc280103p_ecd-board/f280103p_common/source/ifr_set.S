//###########################################################################
//
// FILE:    ifr_set.S
//
// TITLE:   F280103P ifr_set Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: F280103P Support Library V1.0.1 $
// $Release Date: 2023-02-03 01:13:57 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 ifr_set

ifr_set:
csrs 0x344,a0  //IFR |= a0 
 ret


	

