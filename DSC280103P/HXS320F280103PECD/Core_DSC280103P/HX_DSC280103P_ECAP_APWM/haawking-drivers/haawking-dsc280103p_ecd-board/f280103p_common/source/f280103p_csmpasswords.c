//###########################################################################
//
// FILE:    f280103p_csmpasswords.c
//
// TITLE:    F280103P Code Security Module Passwords.
// 
// DESCRIPTION:
//
//         This file is used to specify password values to
//         program into the CSM password locations in Flash
//         at 0x7DFFF0 - 0x7DFFFF.
//
//
//###########################################################################
// $HAAWKING Release: F280103P Support Library V1.0.1 $
// $Release Date: 2023-02-03 01:13:57 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#include "F280103P_Device.h"     //  Headerfile Include File
/**********************************************
password file
*****************************************************************/

volatile struct CSM_PWL  CODE_SECTION(".CsmPwl") CsmPwl = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};



