/******************************************************************
 文 档 名 ：HX_DSC280103P_ECAP_APWM
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：无
 作 用：多触发源多通道ADC采样
 说 明：按程序设置将ECAP_APWM模式的单路PWM输出信号线GPIO19
 与外部中断XINT2触发 引脚GPIO17相连；
 每个边沿触发一次外部中断，LED1 翻转一次。

版本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com

 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "ecap.h"
#include "xint.h"

void InitLED(void);

int main(void)
{
	    /*初始化系统控制：PLL,WatchDog，使能外设时钟*/
		InitSysCtrl();
		/*初始化GPIO，复用为ECap功能*/
		ECap_Gpio();
		/*初始化ECap*/
		ECap_Init();
		/*初始化中断XINT2*/
		XINT2_Init();
		/*初始化LED*/
		InitLED();

		/*关中断*/
		DINT;
		/*禁止CPU中断并清除所有中断标志*/
		IER = 0x0000;
		IFR = 0x0000;
		/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
		InitPieVectTable();

		EALLOW;
		PieVectTable.ECAP1_INT = &ecap_isr;  //将ecap_isr入口地址赋给ECAP1_INT
		PieVectTable.XINT2 = &xint2_isr;  //将xint2_isr入口地址赋给XINT2
		EDIS;

		/*使能M_INT1、M_INT4*/
		IER |= M_INT1 |  M_INT4 ;

		/*开启对应的中断*/
		PieCtrlRegs.PIEIER1.bit.INTx5 = 1;
		PieCtrlRegs.PIEIER4.bit.INTx1 = 1;

		EINT;

		while(1)
		{
		}

	return 0;
}

/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED()
{
	EALLOW;
	/*将LED1 GPIO32配置为数字IO;*/
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;
	/* 将LED1 GPIO32配置为输出;*/
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;
	EDIS;
}

// ----------------------------------------------------------------------------
