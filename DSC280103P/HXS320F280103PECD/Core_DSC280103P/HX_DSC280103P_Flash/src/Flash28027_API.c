/*
 * Copyright (c) 2020 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email: junning.wu@mail.haawking.com
 *     
 */

#include "dsc_config.h"
#include "Flash28027_API_Library.h"
#include "flash.h"

#define CODE_SECTION(v) __attribute__((section(v)))

#define FLASH_START_ADDR 0x7C0000
#define FLASH_END_ADDR   0x7E0000

//Total 128 Sectors
SECTOR Sector[128]={
	{(Uint32 *)0x7C0000,(Uint32 *)0x7C03FF},
	{(Uint32 *)0x7C0400,(Uint32 *)0x7C07FF},
	{(Uint32 *)0x7C0800,(Uint32 *)0x7C0BFF},
	{(Uint32 *)0x7C0C00,(Uint32 *)0x7C0FFF},
	{(Uint32 *)0x7C1000,(Uint32 *)0x7C13FF},
	{(Uint32 *)0x7C1400,(Uint32 *)0x7C17FF},
	{(Uint32 *)0x7C1800,(Uint32 *)0x7C1BFF},
	{(Uint32 *)0x7C1C00,(Uint32 *)0x7C1FFF},
	{(Uint32 *)0x7C2000,(Uint32 *)0x7C23FF},
	{(Uint32 *)0x7C2400,(Uint32 *)0x7C27FF},
	{(Uint32 *)0x7C2800,(Uint32 *)0x7C2BFF},
	{(Uint32 *)0x7C2C00,(Uint32 *)0x7C2FFF},
	{(Uint32 *)0x7C3000,(Uint32 *)0x7C33FF},
	{(Uint32 *)0x7C3400,(Uint32 *)0x7C37FF},
	{(Uint32 *)0x7C3800,(Uint32 *)0x7C3BFF},
	{(Uint32 *)0x7C3C00,(Uint32 *)0x7C3FFF},
	{(Uint32 *)0x7C4000,(Uint32 *)0x7C43FF},
	{(Uint32 *)0x7C4400,(Uint32 *)0x7C47FF},
	{(Uint32 *)0x7C4800,(Uint32 *)0x7C4BFF},
	{(Uint32 *)0x7C4C00,(Uint32 *)0x7C4FFF},
	{(Uint32 *)0x7C5000,(Uint32 *)0x7C53FF},
	{(Uint32 *)0x7C5400,(Uint32 *)0x7C57FF},
	{(Uint32 *)0x7C5800,(Uint32 *)0x7C5BFF},
	{(Uint32 *)0x7C6000,(Uint32 *)0x7C63FF},
	{(Uint32 *)0x7C6400,(Uint32 *)0x7C67FF},
	{(Uint32 *)0x7C6800,(Uint32 *)0x7C6BFF},
	{(Uint32 *)0x7C6C00,(Uint32 *)0x7C6FFF},
	{(Uint32 *)0x7C7000,(Uint32 *)0x7C73FF},
	{(Uint32 *)0x7C7400,(Uint32 *)0x7C77FF},
	{(Uint32 *)0x7C5C00,(Uint32 *)0x7C5FFF},
	{(Uint32 *)0x7C7800,(Uint32 *)0x7C7BFF},
	{(Uint32 *)0x7C7C00,(Uint32 *)0x7C7FFF},
	{(Uint32 *)0x7C8000,(Uint32 *)0x7C83FF},
	{(Uint32 *)0x7C8400,(Uint32 *)0x7C87FF},
	{(Uint32 *)0x7C8800,(Uint32 *)0x7C8BFF},
	{(Uint32 *)0x7C8C00,(Uint32 *)0x7C8FFF},
	{(Uint32 *)0x7C9000,(Uint32 *)0x7C93FF},
	{(Uint32 *)0x7C9400,(Uint32 *)0x7C97FF},
	{(Uint32 *)0x7C9800,(Uint32 *)0x7C9BFF},
	{(Uint32 *)0x7C9C00,(Uint32 *)0x7C9FFF},
	{(Uint32 *)0x7CA000,(Uint32 *)0x7CA3FF},
	{(Uint32 *)0x7CA400,(Uint32 *)0x7CA7FF},
	{(Uint32 *)0x7CA800,(Uint32 *)0x7CABFF},
	{(Uint32 *)0x7CAC00,(Uint32 *)0x7CAFFF},
	{(Uint32 *)0x7CB000,(Uint32 *)0x7CB3FF},
	{(Uint32 *)0x7CB400,(Uint32 *)0x7CB7FF},
	{(Uint32 *)0x7CB800,(Uint32 *)0x7CBBFF},
	{(Uint32 *)0x7CBC00,(Uint32 *)0x7CBFFF},
	{(Uint32 *)0x7CC000,(Uint32 *)0x7CC3FF},
	{(Uint32 *)0x7CC400,(Uint32 *)0x7CC7FF},
	{(Uint32 *)0x7CC800,(Uint32 *)0x7CCBFF},
	{(Uint32 *)0x7CCC00,(Uint32 *)0x7CCFFF},
	{(Uint32 *)0x7CD000,(Uint32 *)0x7CD3FF},
	{(Uint32 *)0x7CD400,(Uint32 *)0x7CD7FF},
	{(Uint32 *)0x7CD800,(Uint32 *)0x7CDBFF},
	{(Uint32 *)0x7CDC00,(Uint32 *)0x7CDFFF},
	{(Uint32 *)0x7CE000,(Uint32 *)0x7CE3FF},
	{(Uint32 *)0x7CE400,(Uint32 *)0x7CE7FF},
	{(Uint32 *)0x7CE800,(Uint32 *)0x7CEBFF},
	{(Uint32 *)0x7CEC00,(Uint32 *)0x7CEFFF},
	{(Uint32 *)0x7CF000,(Uint32 *)0x7CF3FF},
	{(Uint32 *)0x7CF400,(Uint32 *)0x7CF7FF},
	{(Uint32 *)0x7CF800,(Uint32 *)0x7CFBFF},
	{(Uint32 *)0x7CFC00,(Uint32 *)0x7CFFFF},
	{(Uint32 *)0x7D0000,(Uint32 *)0x7D03FF},
	{(Uint32 *)0x7D0400,(Uint32 *)0x7D07FF},
	{(Uint32 *)0x7D0800,(Uint32 *)0x7D0BFF},
	{(Uint32 *)0x7D0C00,(Uint32 *)0x7D0FFF},
	{(Uint32 *)0x7D1000,(Uint32 *)0x7D13FF},
	{(Uint32 *)0x7D1400,(Uint32 *)0x7D17FF},
	{(Uint32 *)0x7D1800,(Uint32 *)0x7D1BFF},
	{(Uint32 *)0x7D1C00,(Uint32 *)0x7D1FFF},
	{(Uint32 *)0x7D2000,(Uint32 *)0x7D23FF},
	{(Uint32 *)0x7D2400,(Uint32 *)0x7D27FF},
	{(Uint32 *)0x7D2800,(Uint32 *)0x7D2BFF},
	{(Uint32 *)0x7D2C00,(Uint32 *)0x7D2FFF},
	{(Uint32 *)0x7D3000,(Uint32 *)0x7D33FF},
	{(Uint32 *)0x7D3400,(Uint32 *)0x7D37FF},
	{(Uint32 *)0x7D3800,(Uint32 *)0x7D3BFF},
	{(Uint32 *)0x7D3C00,(Uint32 *)0x7D3FFF},
	{(Uint32 *)0x7D4000,(Uint32 *)0x7D43FF},
	{(Uint32 *)0x7D4400,(Uint32 *)0x7D47FF},
	{(Uint32 *)0x7D4800,(Uint32 *)0x7D4BFF},
	{(Uint32 *)0x7D4C00,(Uint32 *)0x7D4FFF},
	{(Uint32 *)0x7D5000,(Uint32 *)0x7D53FF},
	{(Uint32 *)0x7D5400,(Uint32 *)0x7D57FF},
	{(Uint32 *)0x7D5800,(Uint32 *)0x7D5BFF},
	{(Uint32 *)0x7D5C00,(Uint32 *)0x7D5FFF},
	{(Uint32 *)0x7D6000,(Uint32 *)0x7D63FF},
	{(Uint32 *)0x7D6400,(Uint32 *)0x7D67FF},
	{(Uint32 *)0x7D6800,(Uint32 *)0x7D6BFF},
	{(Uint32 *)0x7D6C00,(Uint32 *)0x7D6FFF},
	{(Uint32 *)0x7D7000,(Uint32 *)0x7D73FF},
	{(Uint32 *)0x7D7400,(Uint32 *)0x7D77FF},
	{(Uint32 *)0x7D7800,(Uint32 *)0x7D7BFF},
	{(Uint32 *)0x7D7C00,(Uint32 *)0x7D7FFF},
	{(Uint32 *)0x7D8000,(Uint32 *)0x7D83FF},
	{(Uint32 *)0x7D8400,(Uint32 *)0x7D87FF},
	{(Uint32 *)0x7D8800,(Uint32 *)0x7D8BFF},
	{(Uint32 *)0x7D8C00,(Uint32 *)0x7D8FFF},
	{(Uint32 *)0x7D9000,(Uint32 *)0x7D93FF},
	{(Uint32 *)0x7D9400,(Uint32 *)0x7D97FF},
	{(Uint32 *)0x7D9800,(Uint32 *)0x7D9BFF},
	{(Uint32 *)0x7D9C00,(Uint32 *)0x7D9FFF},
	{(Uint32 *)0x7DA000,(Uint32 *)0x7DA3FF},
	{(Uint32 *)0x7DA400,(Uint32 *)0x7DA7FF},
	{(Uint32 *)0x7DA800,(Uint32 *)0x7DABFF},
	{(Uint32 *)0x7DAC00,(Uint32 *)0x7DAFFF},
	{(Uint32 *)0x7DB000,(Uint32 *)0x7DB3FF},
	{(Uint32 *)0x7DB400,(Uint32 *)0x7DB7FF},
	{(Uint32 *)0x7DB800,(Uint32 *)0x7DBBFF},
	{(Uint32 *)0x7DBC00,(Uint32 *)0x7DBFFF},
	{(Uint32 *)0x7DC000,(Uint32 *)0x7DC3FF},
	{(Uint32 *)0x7DC400,(Uint32 *)0x7DC7FF},
	{(Uint32 *)0x7DC800,(Uint32 *)0x7DCBFF},
	{(Uint32 *)0x7DCC00,(Uint32 *)0x7DCFFF},
	{(Uint32 *)0x7DD000,(Uint32 *)0x7DD3FF},
	{(Uint32 *)0x7DD400,(Uint32 *)0x7DD7FF},
	{(Uint32 *)0x7DD800,(Uint32 *)0x7DDBFF},
	{(Uint32 *)0x7DDC00,(Uint32 *)0x7DDFFF},
	{(Uint32 *)0x7DE000,(Uint32 *)0x7DE3FF},
	{(Uint32 *)0x7DE400,(Uint32 *)0x7DE7FF},
	{(Uint32 *)0x7DE800,(Uint32 *)0x7DEBFF},
	{(Uint32 *)0x7DEC00,(Uint32 *)0x7DEFFF},
	{(Uint32 *)0x7DF000,(Uint32 *)0x7DF3FF},
	{(Uint32 *)0x7DF400,(Uint32 *)0x7DF7FF},
	{(Uint32 *)0x7DF800,(Uint32 *)0x7DFBFF},
	{(Uint32 *)0x7DFC00,(Uint32 *)0x7DFFFF},
	};
	
Uint16 CODE_SECTION("L1") Flash28027_ECD_Program(Uint32 *FlashAddr, Uint32*BufAddr, Uint32 Length, FLASH_ST *FProgStatus){
  Uint32 i;
  FLASH_ST VerifyStatus;
  FLASH_ST ProgVerifyStat;
  Uint16 Verify_Status;
  
  	
  //CSM Unlock
  if(P_FlashRegs->SYSCsmRegs.CSMSCR.bit.SECURE == 1) return STATUS_FAIL_CSM_LOCKED;
  //FlashAddr Invalid
  if(((Uint32)(FlashAddr) < 0x7C0000) || ((Uint32)(FlashAddr) >= 0x7DFFFF) || (((Uint32)(FlashAddr) + Length) >= 0x7DFFFF)) return STATUS_FAIL_ADDR_INVALID;
  //Flash has been Written
	if(Flash28027_ECD_ProgVerify(FlashAddr,Length,&ProgVerifyStat) != STATUS_SUCCESS) return STATUS_FAIL_ZERO_BIT_ERROR;
		
  
	EALLOW;
	P_FlashRegs->FMEMWREN.bit.FMEMWREN = 1;
	EDIS;
	
	for(i=0; i<Length; i++){
	  *(Uint32 *)(FlashAddr + i) = *(BufAddr+i);
    }
//	while(P_FlashRegs->FSTAT.bit.BUSY);
	
	Verify_Status = Flash28027_ECD_Verify(FlashAddr,BufAddr,Length,&VerifyStatus);
	if(Verify_Status == STATUS_SUCCESS)	
	  return STATUS_SUCCESS;
	else
	  return STATUS_FAIL_PROGRAM;  	
	}
	
Uint16 CODE_SECTION("L1") Flash28027_ECD_OtpProgram(Uint32 *OtpAddr, Uint32*BufAddr, Uint32 Length, FLASH_ST *FProgStatus){
  Uint32 i;
  FLASH_ST VerifyStatus;
  FLASH_ST ProgVerifyStat;
  Uint16 Verify_Status;

  	
  //CSM Unlock
  if(P_FlashRegs->SYSCsmRegs.CSMSCR.bit.SECURE == 1) return STATUS_FAIL_CSM_LOCKED;
  //FlashAddr Invalid
  if(((Uint32)(OtpAddr) < 0x7A0000) || ((Uint32)(OtpAddr) >= 0x7A3000) || (((Uint32)(OtpAddr) + Length) >= 0x7A3000)) return STATUS_FAIL_ADDR_INVALID;
  //Flash has been Written
	if(Flash28027_ECD_ProgVerify(OtpAddr,Length,&ProgVerifyStat) != STATUS_SUCCESS) return STATUS_FAIL_ZERO_BIT_ERROR;
	//Flash has been Written
	if(Length%4 != 0) return STATUS_FAIL_PROGRAM;
	if((Uint32)(OtpAddr)%4 != 0) return STATUS_FAIL_PROGRAM;
		
  
	EALLOW;
	P_FlashRegs->OTPWREN.bit.OTPWREN = 1;
	EDIS;
	
	for(i=0; i<Length; i=i+4){
		*(Uint32 *)(OtpAddr + i) = *(BufAddr + i);
		*(Uint32 *)(OtpAddr + i + 1) = *(BufAddr + i + 1);
		*(Uint32 *)(OtpAddr + i + 2) = *(BufAddr + i + 2);
		*(Uint32 *)(OtpAddr + i + 3) = *(BufAddr + i + 3);
		}
	while(P_FlashRegs->FSTAT.bit.BUSY);
	
	Verify_Status = Flash28027_ECD_Verify(OtpAddr,BufAddr,Length,&VerifyStatus);
	if(Verify_Status == STATUS_SUCCESS)	
	  return STATUS_SUCCESS;
	else
	  return STATUS_FAIL_PROGRAM;
	}

Uint16 CODE_SECTION("L1") Flash28027_ECD_Verify(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length, FLASH_ST *FVerifyStat){
	Uint32 i;
	Uint8 ECnt = 0;
	for(i=0;i<Length;i++){
  	if(*(BufAddr+i) != *(FlashAddr + i)) ECnt++;
  }
  
  if(ECnt == 0) return STATUS_SUCCESS;
  	else return STATUS_FAIL_VERIFY;
	}
	
Uint16 CODE_SECTION("L1") Flash28027_ECD_ProgVerify(Uint32 *FlashAddr, Uint32 Length, FLASH_ST *FVerifyStat){
  Uint32 i;
  Uint8 ECnt = 0;
  for(i=0;i<Length;i++){
    if(0xFFFFFFFF != *(FlashAddr + i)) ECnt++;
  }
  
  if(ECnt == 0) return STATUS_SUCCESS;
  else return STATUS_FAIL_ZERO_BIT_ERROR;
}	
	
Uint16 CODE_SECTION("L1") Flash28027_ECD_EraseVerify(Uint32 *FlashAddr, Uint16 Length, FLASH_ST *FVerifyStat){
  Uint16 i;
  Uint8 ECnt = 0;
  for(i=0;i<Length;i++){
    if(0xFFFFFFFF != *(FlashAddr + i)) ECnt++;
  }
  
  if(ECnt == 0) return STATUS_SUCCESS;
  else return STATUS_FAIL_ERASE;
}
	
Uint16 CODE_SECTION("L1") Flash28027_ECD_Erase(Uint8 SectorIdx, FLASH_ST *FEraseStat){
  FLASH_ST EraseVerifyStatus;
  Uint16 EraseVerify_Status;
  	
  //CSM Unlock
  if(P_FlashRegs->SYSCsmRegs.CSMSCR.bit.SECURE == 1) return STATUS_FAIL_CSM_LOCKED;
  
  if(SectorIdx >= 128) return STATUS_FAIL_NO_SECTOR_SPECIFIED;

  EALLOW;
  P_FlashRegs->FPERCTL.bit.erase = SectorIdx;
  EDIS;
  while(P_FlashRegs->FSTAT.bit.BUSY);
  //Verify Each Sector, Length = 256 * 4B
  EraseVerify_Status = Flash28027_ECD_EraseVerify(Sector[SectorIdx].StartAddr,256,&EraseVerifyStatus);
  if(EraseVerify_Status != STATUS_SUCCESS) return STATUS_FAIL_ERASE;
  return STATUS_SUCCESS;
}//End of Flash28027_ECD_Erase

Uint16 CODE_SECTION("L1") Flash28027_ECD_MassErase(FLASH_ST *FEraseStat){
  FLASH_ST EraseVerifyStatus;
  Uint8 SectorIdx;
  Uint16 EraseVerify_Status;

  //CSM Unlock
  if(P_FlashRegs->SYSCsmRegs.CSMSCR.bit.SECURE == 1) return STATUS_FAIL_CSM_LOCKED;
	
  EALLOW;
  P_FlashRegs->FMERCTL = 1;
  EDIS;
  while(P_FlashRegs->FSTAT.bit.BUSY);
  for(SectorIdx = 0; SectorIdx < 128; SectorIdx++){
    //Verify Each Sector, Length = 512 * 4B
    EraseVerify_Status = Flash28027_ECD_EraseVerify(Sector[SectorIdx].StartAddr,256,&EraseVerifyStatus);
    if(EraseVerify_Status != STATUS_SUCCESS) return STATUS_FAIL_ERASE;
  }
  return STATUS_SUCCESS;
}//End of Flash28027_ECD_MassErase
