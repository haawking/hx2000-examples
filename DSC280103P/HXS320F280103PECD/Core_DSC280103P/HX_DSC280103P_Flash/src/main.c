 /******************************************************************
文  档  名：HX_DSC280103P_FLASH
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
使  用  库：无
作	    用：FLASH读写数据，向FLASH的22扇区写入8个不同的数
说	    明：主频运行于96M
现     象：数据一致LED1亮，不一致LED1灭
版	    本：V1.0.0
时	    间：2022年9月1日
作	    者：heyang
mail：   support@mail.haawking.com
******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "Flash28027_API_Library.h"
#include "flash.h"

#define LEN 8//写入FLASH数组长度

void InitLED(void);

uint32 FLASH_Data[LEN]={};//写入FLASH数据定义

int temp;

int main(void)
{
    /*
     * If the user need Flash Pipeline mode to improve performance, The user can uncomment InitFlash();
     * If the system clock goes from low to high, the user needs to configure FBANKWAIT at
     * low clock to suit the control word at high clock.
     * The default FBANKWAIT value is 4.
     * For the configuration of other clocks,Please refer to the following link:
     * http://haawking.com/zyxz
     */
	//InitFlash();

	uint16_t i;
	uint16_t status;
	FLASH_ST FlashProgstatus;
	FLASH_ST FlashStatus;

    InitSysCtrl();  //Initializes the System Control registers to a known state.

    InitLED();
    /*擦除FLASH的第22个扇区，HX28027可填入0-31 */
    status= (*Flash28027_ECD_Erase)(22,&FlashStatus);
    FLASH_Data[0]=0x33;
    for(i=1;i<LEN;i++)
    {
    	FLASH_Data[i]=FLASH_Data[i-1]+1;
    }
    /*向第22个扇区写入不同的8个数*/
    status=(*Flash28027_ECD_Program)(Sector[22].StartAddr,FLASH_Data,LEN,&FlashProgstatus);
    temp=0;
    /*对比写入的数据,比对不一致,则temp++*/
    for(i=0;i<LEN;i++)
    {
    	if(*(Uint32*)(Sector[22].StartAddr+i)!=FLASH_Data[i])
    		temp++;
    }

    while(1)
    {
        /*对比写入的数据,比对一致,则LED1亮,否则,LED1灭*/
    	if(temp==0)
    	{
    		GpioDataRegs.GPBCLEAR.bit.GPIO32=1;
    	}
    	else
    	{
    		GpioDataRegs.GPBSET.bit.GPIO32=1;
    	}

    }

	return 0;
}

// ----------------------------------------------------------------------------

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO32=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO32=1;
	GpioDataRegs.GPBSET.bit.GPIO32=1;

	EDIS;
}
