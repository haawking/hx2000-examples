#include "dsc_config.h"

//void InitComp2Gpio()
//{
//    EALLOW;
//	GpioCtrlRegs.GPBPUD.bit.GPIO34 = 0;  //Disable pull-up on GPIO34(CMP2OUT)
//    GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 1;   // GPIO1 for CMP2OUT operation
//
//    GpioCtrlRegs.AIOMUX1.bit.AIO4 = 2;    // AIO4 for CMP2A operation
//    EDIS;
//}


/******************************************************************
 *函数名：InitComp1(void)
 *参 数：无
 *返回值：无
 *作 用：比较器初始化
 ******************************************************************/

void InitComp2(void)
{
	EALLOW;
	/*比较器2被时钟使能*/
	SysCtrlRegs.PCLKCR3.bit.COMP2ENCLK = 1;
	/*ADC模块被时钟使能*/
	SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;

	/*比较器/DAC逻辑已上电*/
	Comp2Regs.COMPCTL.bit.COMPDACEN = 1;
	/*	传递比较器的同步值*/
	Comp2Regs.COMPCTL.bit.QUALSEL = 0;
	/*传递了比较器输出的异步版本*/
	Comp2Regs.COMPCTL.bit.SYNCSEL = 0;
	/*比较器的反相输入连接到内部DAC*/
	Comp2Regs.COMPCTL.bit.COMPSOURCE = 0;
	/*DAC位值设置为256,对应比较阈值为256/1024*3.3=0.825V*/
	Comp2Regs.DACVAL.bit.DACVAL = 256;
	/*DACVAL控制DAC*/
	Comp2Regs.DACCTL.bit.DACSOURCE = 0;
	/*输出信号取反,根据比较器逻辑实现超过阈值,输出DC触发信号动作*/
	Comp2Regs.COMPCTL.bit.CMPINV=1;
	EDIS;
}
