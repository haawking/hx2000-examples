#include "dsc_config.h"
/******************************************************************
 *函数名：void InitLED()
 *参 数：无
 *返回值：无
 *作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的空间*/
	EALLOW;
	/*将GPIO32配置为数字IO*/
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0; /*LED1 MUX = 0;*/
	/*将GPIO32配置为输出*/
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1; /*LED1 DIR = 1;*/

	GpioDataRegs.GPBSET.bit.GPIO32 = 1; /*LED1灭*/

	/*禁止访问受保护的空间*/
	EDIS;
}
