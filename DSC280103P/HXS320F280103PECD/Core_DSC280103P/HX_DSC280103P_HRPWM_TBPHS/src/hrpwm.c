/******************************************************************
 文 档 名：     hrpwm.c
 D S P：       DSC28027/DSC28034
 使 用 库：
 作     用：
 说     明：      提供hrpwm.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2022年1月13日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "hrpwm.h"

/******************************************************************
 函数名：HRPWM1_config(period)
 参	数：period:周期值
 返回值：无
 作	用：EPWM1配置
 ******************************************************************/
void HRPWM1_config(uint16 period)
{
	/*影子寄存器的立即装载*/
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;

	/*PWM周期值向上计数时为TBCLK+1个TBCLK，period-1表示其周期值与实际周期数相等*/
	EPwm1Regs.TBPRDM.half.TBPRD = period - 1;

	/*CC比较寄存器的占空比配置*/
	EPwm1Regs.CMPA.half.CMPA = period / 2;
	EPwm1Regs.CMPB = period / 2;

	/*EPWM1的相位偏移：整数部分与小数部分，*/
	EPwm1Regs.TBPHS.half.TBPHS = 16;
	EPwm1Regs.TBPHS.half.TBPHSHR = 0xA9;
	/*TB时基计数初始值从0开始计数*/
	EPwm1Regs.TBCTR = 0;
	/*仿真模式时,时基计数器自由运行*/
	EPwm1Regs.TBCTL.bit.FREE_SOFT = 3;

	/*时基计数器向上计数*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
	/*使能装载相位*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_ENABLE;
	/*在CTR=CMPB时产生与EPWM2的同步事件*/
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_CMPB;
	/*时基计数器高速时钟与低速时钟采用1分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*在CTR=0时装载影子寄存器的值*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
	/*CC比较寄存器采用影子寄存器模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

	/*在CTR=0时EPWMxA清零*/
	EPwm1Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
	/*EPWMxA向上计数时当CTR=CMPA时EPWMxA置位*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*当CTR=0时EPWMxB清零*/
	EPwm1Regs.AQCTLB.bit.ZRO = AQ_CLEAR;
	/*EPWMxB向上计数时当CTR=CMPB时EPWMxB置位*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;

	/*事件中断配置*/
	/*从CTR=0处开始执行事件中断*/
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	/*ET事件中断INT使能*/
	EPwm1Regs.ETSEL.bit.INTEN = 1;
	/*ET中断事件次数配置:一中断周期内执行3个事件*/
	EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;

	EALLOW;
	/*使能生成,相位调节*/
	EPwm1Regs.HRPCTL.bit.HRPE = 0;
	/*相位调节使能*/
	EPwm1Regs.HRPCTL.bit.TBPHSHRLOADE = 1;

	/*MEP校准作用在双边沿，占空比控制模式*/
	EPwm1Regs.HRCNFG.all = 0x0;
	EPwm1Regs.HRCNFG.bit.EDGMODE = HR_BEP;
	EPwm1Regs.HRCNFG.bit.CTLMODE = HR_PHS;

	/*自CTR=0开始装载高频CMPAHR寄存器的值*/
	EPwm1Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO;
	/*使能自动校准延迟功能*/
	EPwm1Regs.HRCNFG.bit.AUTOCONV = 1;

	/*自动延迟校准延迟数设置为1*/
	EPwm1Regs.HRMSTEP = 0x00FC;
	EDIS;
}

/******************************************************************
 函数名：HRPWM2_config(uint16 period)
 参	数：period:周期值
 返回值：无
 作	用：EPWM2配置
 ******************************************************************/
void HRPWM2_config(uint16 period)
{
	EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;

	EPwm2Regs.TBPRD = period - 1;

	EPwm2Regs.CMPA.half.CMPA = period / 2;
	EPwm2Regs.CMPB = period / 2;

	EPwm2Regs.TBPHS.half.TBPHS = 16;

	EPwm2Regs.TBCTR = 0;

	EPwm2Regs.TBCTL.bit.FREE_SOFT = 3;

	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
	EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;

	EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_CTR_CMPB;

	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

	EPwm2Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
	EPwm2Regs.AQCTLB.bit.ZRO = AQ_CLEAR;
	EPwm2Regs.AQCTLB.bit.CBU = AQ_SET;

	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	EPwm2Regs.ETSEL.bit.INTEN = 1;
	EPwm2Regs.ETPS.bit.INTPRD = ET_3RD;
}

/******************************************************************
 函数名：HRPWM3_config(uint16 period)
 参	数：period:周期值
 返回值：无
 作	用：EPWM3配置
 ******************************************************************/
void HRPWM3_config(uint16 period)
{
	EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;

	EPwm3Regs.TBPRDM.half.TBPRD = period - 1;

	EPwm3Regs.CMPA.half.CMPA = period / 2;
	EPwm3Regs.CMPB = period / 2;

	EPwm3Regs.TBPHS.half.TBPHS = 0;
	EPwm3Regs.TBPHS.half.TBPHSHR = 0;

	EPwm3Regs.TBCTR = 0;

	EPwm3Regs.TBCTL.bit.FREE_SOFT = 3;

	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;

	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;

	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

	EPwm3Regs.AQCTLA.bit.ZRO = AQ_CLEAR;
	EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;
	EPwm3Regs.AQCTLB.bit.ZRO = AQ_CLEAR;
	EPwm3Regs.AQCTLB.bit.CBU = AQ_SET;

	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	EPwm3Regs.ETSEL.bit.INTEN = 1;
	EPwm3Regs.ETPS.bit.INTPRD = ET_3RD;
}

Uint16 i,Phase;
uint16 a;

/******************************************************************
 函数名：EPWM1_ISR(void)
 参	数：无
 返回值：无
 作	用：EPWM1中断处理
 ******************************************************************/
void INTERRUPT EPWM1_ISR(void)
{
	for(Phase=1;Phase<256;Phase++)
	{
		EPwm1Regs.TBPHS.half.TBPHSHR=Phase<<8;
		for(i = 0; i < 10000; i++)
		{
		}
	}

	/*清除EPWM的事件全局中断INT置位,使得第二次及以后的中断能持续触发*/
	EPwm1Regs.ETCLR.bit.INT = 1;
	/*PIEACK中断锁定，屏蔽其他中断的执行*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

/******************************************************************
 函数名：EPWM2_ISR(void)
 参	数：无
 返回值：无
 作	用：EPWM2事件中断：产生占空比从1到256变化的常规PWM波形
 ******************************************************************/
void INTERRUPT EPWM2_ISR(void)
{
	EPwm2Regs.ETCLR.bit.INT = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

/******************************************************************
 函数名：EPWM3_ISR(void)
 参	数：无
 返回值：无
 作	用：EPWM3事件中断：产生占空比从1到256变化的不含SFO校准的HRPWM波形
 ******************************************************************/
void INTERRUPT EPWM3_ISR(void)
{

	EPwm3Regs.ETCLR.bit.INT = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

