/******************************************************************
 文 档 名：     HX_DSC280103P_SPI_FLASH_INTERRUPT
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：
 作     用：     SPI访问flash；DSC28027的SPI端口连接W25Q16芯片，
 采用接收中断，实现对W25Q16的读写操作
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：  通过对spiflash的读写效果，演示SPI接口的配置和中断使用

 连接方式：外接FLASH芯片W25Q16
 GPIO16/SPISIMO-FLASH_W（写入）
 GPIO17/SPISOMI-FLASH_R（读取）
 GPIO18/SPICLK-FLASH_CLK（时钟）
 GPIO19/CS-FLASH_CS（片选）

 现象：（1）观察变量数组upper_100[128]与读出数组upper_128[128]数据是否一致，
数据一致 LED2/GPIO32点亮，数据不一致LED2/GPIO32灭
 （2）每接收到一组数据， LED2/GPIO32闪灯一次

 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "spi.h"
uint8_t upper_100[128] = { 0x55, 0x55, 0x55, 0x98, 0x56, 0x78, 0x45, 0x34 };

uint8_t upper_99[128];

uint8_t cnt=0x52;

uint16 k = 0;
uint16 n=0;
bool_t status = true;

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化LED*/
	InitLED();

	/*初始化GPIO为SPI引脚功能*/
	spi_ioinit();
	/*初始化SPI的FIFO寄存器*/
	spi_fifo_init();
	/*初始化SPI的控制寄存器*/
	spi_init();

	/*写入SPI数组赋值*/
	upper_99[0]=cnt;
	for(n=1;n<128;n++)
	{
		upper_99[n+1]=upper_99[n]+0x22;
	}
	/*关中断*/
	InitPieCtrl();
	/*清中断*/
	IER=0x0000;
	IFR=0x0000;
	/*初始化中断向量表*/
	InitPieVectTable();

	EALLOW;
	/*中断入口地址指向SPI_RX中断入口，执行接收中断：读取接收到的数据*/
	PieVectTable.SPIRXINTA=&spi_rx_int;
	EDIS;

	/*读取ID信息*/
	SPIFlash.Jedec_ID = Jedec_ID_Read();
	/*读取flash状态*/
	SPIFlash.SReg = Read_Status_2Reg();
	/*flash写使能*/
	WREN();
	/*写状态*/
	WrSReg(0x0000);
	/*读取flash状态*/
	SPIFlash.SReg = Read_Status_2Reg();
	/*等待延时*/
	Wait_Busy();
	/*读取flash状态*/
	SPIFlash.SReg = Read_Status_2Reg();

	/*写使能*/
	WREN();
	/*擦除*/
	Chip_Erase();
	Wait_Busy();
	Wait_Busy();
	Wait_Busy();
	Wait_Busy();
	/*读取数据*/
	ReadData(0, upper_128, 128);

	/*写使能*/
	WREN();
	/*写入*/
	PageProgram(0, upper_100, 128);

	/*使能打开对应的CPU IER中断*/
	IER|=M_INT6;
	/*使能打开对应的PIE IER中断*/
   PieCtrlRegs.PIEIER6.bit.INTx1=1;
	/*使能打开全局中断*/
   EINT;

	for (k = 0; k < 128; k++)
	{
		/*判断数组内的元素是否相同*/
		if (upper_128[k] != upper_100[k])
		{
			status = false;
		}
	}

	if (status == true)
	{
		GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
	}
	else
	{
		GpioDataRegs.GPBSET.bit.GPIO32 = 1;
	}

	while (1)
	{
		cnt++;//写入不同的数组数据
		spi_write();//写入第n组(n>=2)数据
	}

	return 0;
}
// ----------------------------------------------------------------------------

