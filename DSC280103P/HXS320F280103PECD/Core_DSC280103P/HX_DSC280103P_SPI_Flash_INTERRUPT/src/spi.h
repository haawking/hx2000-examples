/******************************************************************
 文 档 名：     spi.h
 D S P：       DSC28027
 使 用 库：     
 作     用：      
 说     明：      提供spi.h接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2021年11月13日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#ifndef SPI_H_
#define SPI_H_
#include "dsc_config.h"

#define DummyData 0xa5

#define EECSGPIO GPIO39

extern uint8_t upper_128[128];

typedef struct W25Q64Flash_Type
{
	long Jedec_ID;
	uint16 SReg;
	uint32 Addr;
	uint8 CMD;
	uint32 Len;
} W25Q64Flash;

extern W25Q64Flash SPIFlash;

void spi_ioinit(void);
void spi_fifo_init(void);
void spi_init(void);
uint8 Send_Byte(uint16 a);
uint16 Get_Byte(void);
unsigned long Jedec_ID_Read(void);
uint16 Read_Status_2Reg(void);
void ReadData(unsigned long Dst, unsigned char *Rxbuf, unsigned long len);
void PageProgram(unsigned long Dst, unsigned char *byte, unsigned char len);
void WREN(void);
void WrSReg(uint16 setReg);
void Chip_Erase(void);
void Wait_Busy(void);

void INTERRUPT spi_rx_int(void);

#define WREN_IST  0x06
#define CHIP_ERASE_IST 0x60
#define READ_STATUS_REG_IST 0x05
#define PAGE_PRG_IST 0x02
#define READ_DATA_IST 0x03
#define WRITE_SREG_IST 0X01
#define SECTOR_ERASE_IST 0x20

#define SPIFLASH_ID 0x64
#define SECTOR_SIZE 0x1000

#define FLASH_ID_I 0
#define FLASH_CMD_I 1
#define FLASH_ADDRH_I 2
#define FLASH_ADDRM_I 3
#define FLASH_ADDRL_I 4
#define FLASH_DATALENH_I 5
#define FLASH_DATALENM_I 6
#define FLASH_DATALENL_I 7
#define FLASH_CY_DATA_I 8

extern uint8_t upper_99[128];
extern uint16 k;
extern bool_t status;

void spi_write(void);

void InitLED(void);

#endif /* SPI_H_ */
