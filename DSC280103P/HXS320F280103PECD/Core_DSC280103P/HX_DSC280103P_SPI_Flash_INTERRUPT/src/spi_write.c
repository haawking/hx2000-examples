#include "spi.h"

void spi_write(void)
{
	/*写使能*/
	WREN();
	/*擦除*/
	Chip_Erase();
	Wait_Busy();
	Wait_Busy();
	Wait_Busy();
	Wait_Busy();
	/*读取数据*/
	ReadData(0, upper_128, 128);

	/*写使能*/
	WREN();
	/*写入*/
	PageProgram(0, upper_99, 128);
}
