/******************************************************************
 文  档  名：  HX_DSC280103P_CpuTimer_INT
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：
 作	    用：   定时器1中断例程
 说	    明：
 ----------------------例程使用说明-----------------------------
 *  CpuTimer1 作为500ms周期
 *  LED1 闪烁由CpuTimer1中断控制
 *
 现        象：LED1闪烁
 版	    本：V1.0.0
 时	    间：2022年8月25日
 作	    者：heyang
 mail： support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "timer.h"

/*全局变量*/

/*函数原型*/
void InitLED(void);

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    /*初始化PIE控制*/
    InitPieCtrl();

    /*关闭CPU中断，清除中断标志位*/
    IER = 0x0000;
    IFR = 0x0000;

    /*初始化PIE中断向量表*/
    InitPieVectTable();

    InitLED(); /*初始化LED*/

    Timer_init(); /*定时器0 定时器1 初始化，定时器0为1ms周期中断 定时器1为500ms */


    /*中断配置步骤-----5*/
    //PieCtrlRegs.PIECTRL.bit.ENPIE = 1;    /*Enable the PIE block*/
    EINT;


    while(1)
    {

    }

	return 0;
}

/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED()
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0; /*LED1 MUX = 0;*/
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1; /*LED1 DIR = 1;*/
	EDIS;
}


// ----------------------------------------------------------------------------
