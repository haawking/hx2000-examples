//###########################################################################
//
// FILE:    f280103p_dma_defines.h
//
// TITLE:   F280103P Devices Default Interrupt Service Routines Definitions.
//
//###########################################################################
// $HAAWKING Release: F280103P Support Library V1.0.1 $
// $Release Date: 2023-02-03 01:13:57 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef F280103P_DMA_DEFINES_H
#define F280103P_DMA_DEFINES_H


#ifdef __cplusplus
extern "C" {
#endif

//
// MODE
//
// PERINTSEL bits
//

#define DMA_XINT1         1
#define DMA_XINT2         2
#define DMA_XINT3         3
#define DMA_TINT0         4
#define DMA_TINT1         5
#define DMA_TINT2         6
#define DMA_ADC0          7
#define DMA_ADC1          8
#define DMA_ADC2          9
#define DMA_ADC3          10
#define DMA_ADC4          11
#define DMA_ADC5          12
#define DMA_ADC6          13
#define DMA_ADC7          14
#define DMA_ADC8          15
#define DMA_SPI1          16
#define DMA_SPI2          17
//#define DMA_I2S1            18
//#define DMA_I2S2           19
#define DMA_I2C1          20
#define DMA_I2C2          21
#define DMA_SCI1          22
#define DMA_SCI2          23
#define DMA_EPWM1_SOCA    24
#define DMA_EPWM1_SOCB    25
#define DMA_EPWM2_SOCA    26
#define DMA_EPWM2_SOCB    27
#define DMA_EPWM3_SOCA    28
#define DMA_EPWM3_SOCB    29
#define DMA_EPWM4_SOCA    30
#define DMA_EPWM4_OSCB    31


//
// OVERINTE bit
//
#define	OVRFLOW_DISABLE	0x0
#define	OVEFLOW_ENABLE	0x1

//
// PERINTE bit
//
#define	PERINT_DISABLE	0x0
#define	PERINT_ENABLE   0x1

//
// CHINTMODE bits
//
#define	CHINT_BEGIN		0x0
#define	CHINT_END     	0x1

//
// ONESHOT bits
//
#define	ONESHOT_DISABLE	0x0
#define	ONESHOT_ENABLE	0x1

//
// CONTINOUS bit
//
#define	CONT_DISABLE	0x0
#define	CONT_ENABLE 	0x1

//
// SYNCE bit
//
#define	SYNC_DISABLE	0x0
#define	SYNC_ENABLE     0x1

//
// SYNCSEL bit
//
#define	SYNC_SRC		0x0
#define	SYNC_DST        0x1

//
// DATASIZE bit
//
#define	SIXTEEN_BIT    	0x0
#define	THIRTYTWO_BIT   0x1

//
// CHINTE bit
//
#define	CHINT_DISABLE	0x0
#define	CHINT_ENABLE   	0x1

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif

//
// End of file
//

