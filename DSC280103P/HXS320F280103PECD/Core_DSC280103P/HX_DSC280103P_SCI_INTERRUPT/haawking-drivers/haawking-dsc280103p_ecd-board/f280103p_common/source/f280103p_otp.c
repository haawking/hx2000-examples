//###########################################################################
//
// FILE:   f280103p_otp.c
//
// TITLE:  F280103P otp file.
//
//###########################################################################
// $HAAWKING Release: F280103P Support Library V1.0.1 $
// $Release Date: 2023-02-03 01:13:57 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#include "F280103P_Device.h"     //  Headerfile Include File

/***************************************************************
  To  uncomment ,you can use OTP
*****************************************************************/



/***************************************************************

volatile Uint32   CODE_SECTION(".User_Otp_Table") OTP_DATA[] = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};

*****************************************************************/



