#include "dsc_config.h"

/******************************************************************
*函数名：void Scia_Config(void)
*参 数：无
*返回值：无
*作 用：SCIA初始化
******************************************************************/
void Scia_Config(void)
{
	EALLOW;
	/*使能SC1时钟*/
	SysCtrlRegs.PCLKCR0.bit.SCIAENCLK=1;
	/*低速时钟 = SYSCLKOUT/4*/
	SysCtrlRegs.LOSPCP.all=0x0002;
	/*SCI FIFO可以恢复发送或接收*/
	SciaRegs.SCIFFTX.bit.SCIRST=1;
	/*SCICHAR_LENGTH_8*/
	SciaRegs.SCICCR.bit.SCICHAR=0x7;
	/*将接收到的字符发送到SCIRXEMU和SCIRXBUF*/
	SciaRegs.SCICTL1.bit.RXENA=1;
	/*发射机启用*/
	SciaRegs.SCICTL1.bit.TXENA=1;

	/*关闭RXRDY/BRKDT 中断*/
	SciaRegs.SCICTL2.bit.RXBKINTENA=0;
	/*发射机缓存或移位寄存器或两者都装载数据*/
	SciaRegs.SCICTL2.bit.TXEMPTY=0;
	/*禁用TXRDY中断*/
	SciaRegs.SCICTL2.bit.TXINTENA=0;
	/*SCITXBUF已满*/
	SciaRegs.SCICTL2.bit.TXRDY=0;

	/*将接收到的字符发送到SCIRXEMU和SCIRXBUF*/
	SciaRegs.SCICTL1.bit.RXENA=1;
	/*发射机启用*/
	SciaRegs.SCICTL1.bit.TXENA=1;

	/*SCI软件复位(低电平有效)*/
	SciaRegs.SCICTL1.bit.SWRESET=1;

    EDIS;
}
/******************************************************************
*函数名：void Scia_Gpio(void)
*参 数：无
*返回值：无
*作 用：SCIA引脚配置
******************************************************************/
void Scia_Gpio(void)
{
	EALLOW;
	/*GPIO28上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO28=0;
	/*GPIO29上拉*/
	GpioCtrlRegs.GPAPUD.bit.GPIO29=0;

	/*将GPIO28引脚配置为:SCIRXDA-SCI-A接收(I)*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO28=1;
	/*将GPIO29引脚配置为:SCITXDA-SCI-A传输(O)*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO29=1;
	/*异步（非同步或限定）*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO28=3;
	EDIS;
}


