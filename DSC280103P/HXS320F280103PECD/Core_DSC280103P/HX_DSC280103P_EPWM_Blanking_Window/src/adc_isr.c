#include "system.h"

Uint32 adcVal;
/******************************************************************
 *函数名：void INTERRUPT adc_isr()
 *参 数：无
 *返回值：无
 *作 用：adc中断服务函数
 ******************************************************************/
void INTERRUPT adc_isr()
{
	while (AdcRegs.ADCCTL1.bit.ADCBSY == 1)
	{
	}
	adcVal= AdcResult.ADCRESULT0;

	EALLOW;
	/*清除ADCINTFLG寄存器中各自的标志位*/
	AdcRegs.ADCINTFLGCLR.all = 0x4;
	/*清除ADCINTOVF寄存器中相应的溢出位*/
	AdcRegs.ADCINTOVFCLR.all = 0x4;
	/* 中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
	EDIS;

}

