/******************************************************************
 文 档 名：     timer.c
 D S P：       DSC28034
 使 用 库：
 作     用：
 说     明：      提供timer.c接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V1.0.x
 时 间：2021年11月13日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/

#include "system.h"

Uint32 msCounter;
/******************************************************************
 函数名：void  Timer0_init(void)
 参	数：无
 返回值：无
 作	用：定时器配置
 ******************************************************************/
void Timer0_init(void)
{
	/*调用官方库函数，初始化CPU定时器*/
	InitCpuTimers();
	/*配置CPU定时器*/
	ConfigCpuTimer(&CpuTimer0, 120, 1000);
	/*CPU定时器运行*/
	StartCpuTimer0();
}

/******************************************************************
 函数名：void  INTERRUPT TINT0_ISR(void)
 参	数：无
 返回值：无
 作	用：定时器中断，每进一次中断，msCounter加1
 ******************************************************************/

void INTERRUPT timer0_ISR(void)
{
	msCounter++;

	if(msCounter < 5000)
		{
			EQEP_PulseCap_cal();
		}
		else if(msCounter < 10000)
		{
			EQEP_MSpeed();
			EQEP_MSpeed_cal();
		}
		else
		{
			SetSpeed = 60;
			Pwm1CMPA = SysFreq * 3750 / SetSpeed;
			EPwm1Regs.TBPRD = SysFreq * 7500 / SetSpeed;
			EPwm1Regs.CMPA.half.CMPA = Pwm1CMPA;

			EQEP_TSpeed_Angle();
			EQEP_TSpeed_Angle_cal();
		}
		
	/*判断电机转速是否到达给定转速,到达时LED灯亮*/
	if((MotorSpeed == SetSpeed)&&((PulseNum!=0)|(CapNum!=0)))
	{

		GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
	}
	else
	{

		GpioDataRegs.GPBSET.bit.GPIO32 = 1;
	}

	/* Acknowledge this interrupt to receive more interrupts from group 1*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

}

