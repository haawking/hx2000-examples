#include "epwm.h"

uint32_t EPwm3TimerDirection;

#define  PWM3_TIMER_MIN 10
#define  PWM3_TIMER_MAX 80

#define EPwm_TIMER_UP 1
#define EPwm_TIMER_DOWN 0
/******************************************************************
*函数名：void InitEPwmTimer()
*参 数：无
*返回值：无
*作 用：配置EPwm定时器
******************************************************************/
void InitEPwmTimer(void)
{
	/*增计数模式*/
	EPwm3Regs.TBCTL.bit.CTRMODE=TB_COUNT_UP;
	/*周期为10*TBCLK counts*/
	EPwm3Regs.TBPRD=PWM3_TIMER_MIN;
	/*时间基准计数器相位为0*/
	EPwm3Regs.TBPHS.all=0;
	/*当时间基准计数器的值等于周期寄存器的值，翻转EPWMxA的状态*/
	EPwm3Regs.AQCTLA.bit.PRD=AQ_TOGGLE;
	/*高速时钟分频2倍分频*/
	EPwm3Regs.TBCTL.bit.HSPCLKDIV=1;
	/*时基时钟分频 1倍分频*/
	EPwm3Regs.TBCTL.bit.CLKDIV=0;

	EPwm3TimerDirection=EPwm_TIMER_UP;
}
