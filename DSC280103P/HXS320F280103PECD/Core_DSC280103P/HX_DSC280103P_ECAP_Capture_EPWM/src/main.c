/******************************************************************
 文 档 名 ：HX_DSC280103P_ECAP_CAPTURE_EPWM
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：无
 作 用：ECAP捕获PWM波输出
 说 明：利用ECAP脉冲输出模块的单次捕获模式事件4捕获EPWM3周期，产生1个周期
 在10-80个TBCLK变化的向上计数PWM波形。
 ----------------------例程使用说明-----------------------------
 *
 *     测试ECAP功能
 *     GPIO4接GPIO5
 *     GPIO5测GPIO4引脚的PWM波
 *
 * 现象：产生1个周期在10-80个TBCLK变化的向上计数PWM波形
 *			 正常捕获到PWM时LED1亮。
 版本：V1.0.0
 时 间：2022年8月30日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"
#include "ecap.h"

uint32_t ECap1IntCount_new;
uint32_t ECap1IntCount_old;

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
		InitSysCtrl();
		/*初始化LED*/
		InitLED();
		/*初始化GPIO，复用为EPwm功能*/
		InitEPwm3Gpio();
		/*初始化ECap1Gpio*/
		InitECap1Gpio();
		/*初始化PIE控制*/
		InitPieCtrl();
		/*禁止CPU中断并清除所有中断标志*/
		IER = 0x0000;
		IFR = 0x0000;
		/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
		InitPieVectTable();

		EALLOW;
		/*将ecap1_isr入口地址赋给ECAP1_INT*/
		PieVectTable.ECAP1_INT = &ecap1_isr;
		EDIS;

		/*TBCLK被禁止，此时，ePWM模块的时钟使能通过PCLKCR1寄存器进行配置*/
		EALLOW;
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
		EDIS;
		/*初始化EPwm定时器*/
		InitEPwmTimer();
		/*所有使能的ePWM模块同步使用TBCLK*/
		EALLOW;
		SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
		EDIS;
		/*初始化Ecap*/
		InitECapture();

		ECap1IntCount = 0;
		ECap1PassCount = 0;
		/*使能cpu对应的中断*/
		IER |= M_INT4;

		PieCtrlRegs.PIEIER4.bit.INTx1 = 1;

		EINT;

		while(1)
		{
//判断是否进入捕获成功中断，进入则在连续6us内计数有增长，LED灯亮，否则LED灭
			ECap1IntCount_old=ECap1IntCount;
			DELAY_US(6);
			ECap1IntCount_new=ECap1IntCount;
			if(ECap1IntCount_new==ECap1IntCount_old)
			{
				GpioDataRegs.GPBSET.bit.GPIO32=1;/*LED1灭*/
			}
			else
			{
				GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1; /*LED1灭*/
			}
		}

	return 0;
}

// ----------------------------------------------------------------------------
