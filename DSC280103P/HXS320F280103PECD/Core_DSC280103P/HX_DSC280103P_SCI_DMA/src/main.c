/******************************************************************
 文 档 名 ：HX_DSC280103P_SCI_DMA
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：无
 功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块DMA模式接收功能

 连接方式：通过CH340串口线连接电脑端的USB口

 现象：电脑端使用串口调试助手，按照SCIA配置中的参数设置波特率、数据位、校验
 位、和停止位。PC端任意发送一组数据，DSP收到后会通过DMA传输到目的地，
 然后重新读取目的地的数据，通过SCI 输出
 ----------------------例程使用说明-----------------------------
 *
 *            测试SCIA模块DMA模式接收功能
 *
 *
  版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "system.h"

//传输数据量=内环BURST_SIZE*外环TRANSFER_SIZE=1*1=1个16位数;
//
// DMA 定义
//
#define CH1_TOTAL               DATA_POINTS_PER_CHANNEL
#define CH1_WORDS_PER_BURST     ADC_CHANNELS_TO_CONVERT


volatile Uint16 CODE_SECTION("ramfuncs1")  DMAVal;

volatile Uint32 *DMADest;
volatile Uint32 *DMASource;

bool_t sci_dma_rxFlag = false;

//
// Function Prototypes
//
__interrupt void local_DINTCH1_ISR(void);
//
// Main
//
int main(void)
{
    /*初始化系统时钟配置*/
    InitSysCtrl();
    /*初始化SCI配置*/
    InitSciaGpio();
    /*关中断*/
     InitPieCtrl();
     /*清中断*/
     IER = 0x0000;
     IFR = 0x0000;
     /*初始化中断向量表*/
     InitPieVectTable();

     EALLOW;	// Allow access to EALLOW protected registers
     /*DINT1CH1中断入口地址，指向执行数据迁移程序*/
     PieVectTable.DINTCH1= &local_DINTCH1_ISR;
     EDIS;   // Disable access to EALLOW protected registers
     //SCI发送与接收线屏蔽，防止初始化时触发DMA传输
     SciaRegs.SCICTL1.bit.TXENA=0;
     SciaRegs.SCICTL1.bit.RXENA=0;

     /*DMA初始化配置*/
     DMAInitialize();
     /*配置DMA数据迁移方向的源与目的*/
     DMADest   =(Uint32)(&DMAVal);
     DMASource = (Uint32)(&(SciaRegs.SCIRXBUF));

     /*DMA通道CH1突发配置(内环数据量)*/
     //长度1个16位,
     //源地址增大2个字,目标地址增大2个字
     DMACH1BurstConfig(0,2,2);
     /*DMA通道CH1传输配置(外环数据量)*/
     //长度1个16位,
     //源地址增大2个字,目标地址增大2个字
     DMACH1TransferConfig(0,2,2);
     /*DMA通道CH1地址返回配置*/
     //源与目的地址传输65536次数完成，发生地址返回，归零时字个数不变
     DMACH1WrapConfig(0xFFFF,0,0xFFFF,0);
     /*DMA通道CH1模式与功能配置*/
     //DMA中断触发源采用SCI_RX接收,单次触发模式,传输计数为零时停止计数
     //屏蔽中断同步信号,屏蔽源/目的同步影响,溢出中断屏蔽
     //通道传输字的宽度为32位,传输结束时产生中断,通道1中断使能
     DMACH1ModeConfig(DMA_SCI2,PERINT_ENABLE,ONESHOT_ENABLE,CONT_DISABLE,
                      SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,THIRTYTWO_BIT,
                      CHINT_END,CHINT_ENABLE);

     Scia_Config(9600);
     /*使能打开对应的中断*/
     IER = M_INT7 ;
     PieCtrlRegs.PIEIER7.bit.INTx1 = 1;
     /*使能打开全局中断*/
     EINT;

     /*DMA通道CH1启动*/
     StartDMACH1();

     for(;;)
     {
    	 if(sci_dma_rxFlag ==true)
    	 {
    		 sci_dma_rxFlag =false;
			 while(SciaRegs.SCICTL2.bit.TXRDY==0)
    			 {

    			 }
    			 SciaRegs.SCITXBUF=DMAVal;
    	 }
     }
 }

 //
 // local_DINTCH1_ISR - INT7.1(DMA Channel 1)
 //
 __interrupt void local_DINTCH1_ISR(void)
 {
     /*配置DMA数据迁移方向的源与目的*/
	 DMACH1AddrConfig(DMADest,DMASource);
     /*DMA通道CH1启动*/
     StartDMACH1();
	 sci_dma_rxFlag =true;

     /*中断应答锁定DMA中断*/
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
 }

 //
 // End of File
 //
// ----------------------------------------------------------------------------
