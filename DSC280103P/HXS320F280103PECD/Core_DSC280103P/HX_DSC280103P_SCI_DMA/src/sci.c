#include "system.h"

/******************************************************************
*函数名：Scia_Config(uint32 baud)
*参   数： baud，串口波特率
*返回值：无
*作   用： SCIA 初始化配置
******************************************************************/
void Scia_Config(uint32 baud)
{
	uint32 divN = 0;
	uint32 divF = 0;
	uint32 divK = 0;
	uint32 divsel = 0;
	uint32 lospcp = 0;
	uint32 lspclk = 0;
	uint16 brr = 0;

	//获取系统时钟的倍频、分频和低速外部时钟的值
	divF = SysCtrlRegs.PLLCR.bit.DIV;
	divN = SysCtrlRegs.PLLCR.bit.DIVN;
	divK  = SysCtrlRegs.PLLCR.bit.DIVK;
	divsel = SysCtrlRegs.PLLSTS.bit.DIVSEL;
	lospcp = SysCtrlRegs.LOSPCP.bit.LSPCLK;


	if (lospcp != 0)
	{
		lospcp = lospcp * 2;
	}
	else
	{
		lospcp = 1;
	}

	/*分频值设置
	divsel为 0时，系统时钟4分频
	divsel为 1时，系统时钟4分频
	divsel为 2时，系统时钟2分频
	divsel为 3时，系统时钟1分频*/
	switch (divsel)
	{
		case 0:
		case 1:
		lspclk = 12000000 *(divF+15)/(divN+1)/(divK +1)/ 4 / lospcp;
		break;
		case 2:
		lspclk = 12000000 *(divF+15)/(divN+1)/(divK +1) / 2 / lospcp;
		break;
		case 3:
		lspclk = 12000000 *(divF+15)/(divN+1)/(divK +1)/ 1 / lospcp;
		break;
	}

	brr = lspclk / (baud * 8) - 1;

	/*SCI 停止位设置    0：一个停止位   1：两个停止位*/
	SciaRegs.SCICCR.bit.STOPBITS = 0;

	/*SCI 奇偶校验位    0：奇偶校验   1：偶偶校验*/
	SciaRegs.SCICCR.bit.PARITY = 0;

	/*SCI 奇偶校验使能   0：关闭   1：启用*/
	SciaRegs.SCICCR.bit.PARITYENA = 0;

	/*SCI 字符长度   0：1个字长  1：2个字长 ... 7：8个字长*/
	SciaRegs.SCICCR.bit.SCICHAR = 7;

	/*使能SCI的发送机和接收机*/
	SciaRegs.SCICTL1.bit.TXENA = 1;
	SciaRegs.SCICTL1.bit.RXENA = 1;

	/*SCI 16位波特率选择寄存器 高8位*/
	SciaRegs.SCIHBAUD = (uint8) ((brr >> 8) & 0xff);
	/*SCI 16位波特率选择寄存器 低8位*/
	SciaRegs.SCILBAUD = (uint8) (brr & 0xff);

	/*SCI 软件复位，重新启动SCI*/
	SciaRegs.SCICTL1.bit.SWRESET = 1;

}
