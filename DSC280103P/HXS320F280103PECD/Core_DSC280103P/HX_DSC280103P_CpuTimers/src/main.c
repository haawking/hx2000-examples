/******************************************************************
 文  档  名：  HX_DSC280103P_CpuTimer
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板：Core_DSC280103PMST_V1.0
 D S P：     DSC280103P
 使 用 库：
 作	    用：   定时器例程
 说	    明：
 ----------------------例程使用说明-----------------------------
 *  CpuTimer1 作为500ms周期，采用查询方式
 *  CpuTimer2 作为250ms周期，采用查询方式
 *
 *  ConfigCpuTimer(&CpuTimer2, 120, 250000);
 *  第一个参数是定时器编号，
 *  第二个是CPU运行速度MHz，
 *  第三个是设定周期us为单位
 *  CpuTimerxRegs.TCR.bit.TSS = 1 关闭定时器x;  TSS = 0 启动并且重启计数
 *
 *  LED1 亮灭由CpuTimer1控制
 *
 现        象：LED1闪烁
 版	    本：V1.0.0
 时	    间：2022年8月25日
 作	    者：heyang
 mail： support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "timer.h"

/*全局变量*/

/*函数原型*/
void InitLED(void);

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    /*关闭中断*/
    DINT;

    /*初始化PIE控制*/
    InitPieCtrl();

    /*关闭CPU中断，清除中断标志位*/
    IER = 0x0000;
    IFR = 0x0000;

    /*初始化PIE中断向量表*/
    InitPieVectTable();

    InitLED(); /*初始化LED*/

    Timer0_init(); /*定时器0初始化，1ms周期中断*/

    ConfigCpuTimer(&CpuTimer1, 120, 500000); /*定时器1初始化，第二个参数为CPU SYSCLK = 120M， 第三个参数为设定的周期，us单位*/
    CpuTimer1Regs.TCR.bit.TSS = 0; /*打开定时器*/

    ConfigCpuTimer(&CpuTimer2, 120, 250000); /*定时器2初始化，第二个参数为CPU SYSCLK = 120M， 第三个参数为设定的周期，us单位*/
    CpuTimer2Regs.TCR.bit.TSS = 0; /*打开定时器*/

    /*中断配置步骤-----5*/
    //PieCtrlRegs.PIECTRL.bit.ENPIE = 1;    /*Enable the PIE block*/
    EINT;


    while(1)
    {
    	        if(CpuTimer1Regs.TCR.bit.TIF == 1) /*定时器1查询周期*/
    			{
    				CpuTimer1Regs.TCR.bit.TIF = 1; /*标志位写1清0*/
    				CpuTimer1Regs.TCR.bit.TSS = 0; /*启动定时器1*/

    				GpioDataRegs.GPBTOGGLE.bit.GPIO32 = 1; /*LED1翻转*/
    			}

    			if(CpuTimer2Regs.TCR.bit.TIF == 1) /*定时器2查询周期*/
    			{
    				CpuTimer2Regs.TCR.bit.TIF = 1; /*标志位写1清0*/
    				CpuTimer2Regs.TCR.bit.TSS = 0; /*关闭定时器2*/

    			}
    }

	return 0;
}

/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED()
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0; /*LED1 MUX = 0;*/
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1; /*LED1 DIR = 1;*/
	EDIS;
}


// ----------------------------------------------------------------------------
