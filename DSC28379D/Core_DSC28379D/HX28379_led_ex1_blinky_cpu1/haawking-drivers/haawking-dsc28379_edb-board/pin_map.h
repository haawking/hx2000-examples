//###########################################################################
//
// FILE:   pin_map.h
//
// TITLE:  Definitions of pin mux info for gpio.c.
//
//###########################################################################

#ifndef __PIN_MAP_H__
#define __PIN_MAP_H__

//*****************************************************************************
// 0x00000003 = MUX register value
// 0x0000000C = GMUX register value
// 0x0000FF00 = Shift amount within mux registers
// 0xFFFF0000 = Offset of MUX register
//*****************************************************************************


#define GPIO_0_GPIO0                    0x000C0000U
#define GPIO_0_EPWM1A                   0x000C0001U
#define GPIO_0_SDAA                     0x000C0006U

#define GPIO_1_GPIO1                    0x000C0200U
#define GPIO_1_EPWM1B                   0x000C0201U
#define GPIO_1_MFSRB                    0x000C0203U
#define GPIO_1_SCLA                     0x000C0206U

#define GPIO_2_GPIO2                    0x000C0400U
#define GPIO_2_EPWM2A                   0x000C0401U
#define GPIO_2_OUTPUTXBAR1              0x000C0405U
#define GPIO_2_SDAB                     0x000C0406U

#define GPIO_3_GPIO3                    0x000C0600U
#define GPIO_3_EPWM2B                   0x000C0601U
#define GPIO_3_OUTPUTXBAR2              0x000C0602U
#define GPIO_3_MCLKRB                   0x000C0603U
#define GPIO_3_SCLB                     0x000C0606U

#define GPIO_4_GPIO4                    0x000C0800U
#define GPIO_4_EPWM3A                   0x000C0801U
#define GPIO_4_OUTPUTXBAR3              0x000C0805U
#define GPIO_4_CANTXA                   0x000C0806U

#define GPIO_5_GPIO5                    0x000C0A00U
#define GPIO_5_EPWM3B                   0x000C0A01U
#define GPIO_5_MFSRA                    0x000C0A02U
#define GPIO_5_OUTPUTXBAR3              0x000C0A03U
#define GPIO_5_CANRXA                   0x000C0A06U

#define GPIO_6_GPIO6                    0x000C0C00U
#define GPIO_6_EPWM4A                   0x000C0C01U
#define GPIO_6_OUTPUTXBAR4              0x000C0C02U
#define GPIO_6_EPWMSYNCO                0x000C0C03U
#define GPIO_6_EQEP3A                   0x000C0C05U
#define GPIO_6_CANTXB                   0x000C0C06U

#define GPIO_7_GPIO7                    0x000C0E00U
#define GPIO_7_EPWM4B                   0x000C0E01U
#define GPIO_7_MCLKRA                   0x000C0E02U
#define GPIO_7_OUTPUTXBAR5              0x000C0E03U
#define GPIO_7_EQEP3B                   0x000C0E05U
#define GPIO_7_CANRXB                   0x000C0E06U

#define GPIO_8_GPIO8                    0x000C1000U
#define GPIO_8_EPWM5A                   0x000C1001U
#define GPIO_8_CANTXB                   0x000C1002U
#define GPIO_8_ADCSOCAO                 0x000C1003U
#define GPIO_8_EQEP3S                   0x000C1005U
#define GPIO_8_SCITXDA                  0x000C1006U

#define GPIO_9_GPIO9                    0x000C1200U
#define GPIO_9_EPWM5B                   0x000C1201U
#define GPIO_9_SCITXDB                  0x000C1202U
#define GPIO_9_OUTPUTXBAR6              0x000C1203U
#define GPIO_9_EQEP3I                   0x000C1205U
#define GPIO_9_SCIRXDA                  0x000C1206U

#define GPIO_10_GPIO10                  0x000C1400U
#define GPIO_10_EPWM6A                  0x000C1401U
#define GPIO_10_CANRXB                  0x000C1402U
#define GPIO_10_ADCSOCBO                0x000C1403U
#define GPIO_10_EQEP1A                  0x000C1405U
#define GPIO_10_SCITXDB                 0x000C1406U
#define GPIO_10_UPP_WAIT                0x000C140FU

#define GPIO_11_GPIO11                  0x000C1600U
#define GPIO_11_EPWM6B                  0x000C1601U
#define GPIO_11_SCIRXDB                 0x000C1602U
#define GPIO_11_OUTPUTXBAR7             0x000C1603U
#define GPIO_11_EQEP1B                  0x000C1605U
#define GPIO_11_UPP_STRT                0x000C160FU

#define GPIO_12_GPIO12                  0x000C1800U
#define GPIO_12_EPWM7A                  0x000C1801U
#define GPIO_12_CANTXB                  0x000C1802U
#define GPIO_12_MDXB                    0x000C1803U
#define GPIO_12_EQEP1S                  0x000C1805U
#define GPIO_12_SCITXDC                 0x000C1806U
#define GPIO_12_UPP_ENA                 0x000C180FU

#define GPIO_13_GPIO13                  0x000C1A00U
#define GPIO_13_EPWM7B                  0x000C1A01U
#define GPIO_13_CANRXB                  0x000C1A02U
#define GPIO_13_MDRB                    0x000C1A03U
#define GPIO_13_EQEP1I                  0x000C1A05U
#define GPIO_13_SCIRXDC                 0x000C1A06U
#define GPIO_13_UPP_D7                  0x000C1A0FU

#define GPIO_14_GPIO14                  0x000C1C00U
#define GPIO_14_EPWM8A                  0x000C1C01U
#define GPIO_14_SCITXDB                 0x000C1C02U
#define GPIO_14_MCLKXB                  0x000C1C03U
#define GPIO_14_OUTPUTXBAR3             0x000C1C06U
#define GPIO_14_UPP_D6                  0x000C1C0FU

#define GPIO_15_GPIO15                  0x000C1E00U
#define GPIO_15_EPWM8B                  0x000C1E01U
#define GPIO_15_SCIRXDB                 0x000C1E02U
#define GPIO_15_MFSXB                   0x000C1E03U
#define GPIO_15_OUTPUTXBAR4             0x000C1E06U
#define GPIO_15_UPP_D5                  0x000C1E0FU

#define GPIO_16_GPIO16                  0x00100000U
#define GPIO_16_SPISIMOA                0x00100001U
#define GPIO_16_CANTXB                  0x00100002U
#define GPIO_16_OUTPUTXBAR7             0x00100003U
#define GPIO_16_EPWM9A                  0x00100005U
#define GPIO_16_SD1_D1                  0x00100007U
#define GPIO_16_UPP_D4                  0x0010000FU

#define GPIO_17_GPIO17                  0x00100200U
#define GPIO_17_SPISOMIA                0x00100201U
#define GPIO_17_CANRXB                  0x00100202U
#define GPIO_17_OUTPUTXBAR8             0x00100203U
#define GPIO_17_EPWM9B                  0x00100205U
#define GPIO_17_SD1_C1                  0x00100207U
#define GPIO_17_UPP_D3                  0x0010020FU

#define GPIO_18_GPIO18                  0x00100400U
#define GPIO_18_SPICLKA                 0x00100401U
#define GPIO_18_SCITXDB                 0x00100402U
#define GPIO_18_CANRXA                  0x00100403U
#define GPIO_18_EPWM10A                 0x00100405U
#define GPIO_18_SD1_D2                  0x00100407U
#define GPIO_18_UPP_D2                  0x0010040FU

#define GPIO_19_GPIO19                  0x00100600U
#define GPIO_19_SPISTEA                 0x00100601U
#define GPIO_19_SCIRXDB                 0x00100602U
#define GPIO_19_CANTXA                  0x00100603U
#define GPIO_19_EPWM10B                 0x00100605U
#define GPIO_19_SD1_C2                  0x00100607U
#define GPIO_19_UPP_D1                  0x0010060FU

#define GPIO_20_GPIO20                  0x00100800U
#define GPIO_20_EQEP1A                  0x00100801U
#define GPIO_20_MDXA                    0x00100802U
#define GPIO_20_CANTXB                  0x00100803U
#define GPIO_20_EPWM11A                 0x00100805U
#define GPIO_20_SD1_D3                  0x00100807U
#define GPIO_20_UPP_D0                  0x0010080FU

#define GPIO_21_GPIO21                  0x00100A00U
#define GPIO_21_EQEP1B                  0x00100A01U
#define GPIO_21_MDRA                    0x00100A02U
#define GPIO_21_CANRXB                  0x00100A03U
#define GPIO_21_EPWM11B                 0x00100A05U
#define GPIO_21_SD1_C3                  0x00100A07U
#define GPIO_21_UPP_CLK                 0x00100A0FU

#define GPIO_22_GPIO22                  0x00100C00U
#define GPIO_22_EQEP1S                  0x00100C01U
#define GPIO_22_MCLKXA                  0x00100C02U
#define GPIO_22_SCITXDB                 0x00100C03U
#define GPIO_22_EPWM12A                 0x00100C05U
#define GPIO_22_SPICLKB                 0x00100C06U
#define GPIO_22_SD1_D4                  0x00100C07U

#define GPIO_23_GPIO23                  0x00100E00U
#define GPIO_23_EQEP1I                  0x00100E01U
#define GPIO_23_MFSXA                   0x00100E02U
#define GPIO_23_SCIRXDB                 0x00100E03U
#define GPIO_23_EPWM12B                 0x00100E05U
#define GPIO_23_SPISTEB                 0x00100E06U
#define GPIO_23_SD1_C4                  0x00100E07U

#define GPIO_24_GPIO24                  0x00101000U
#define GPIO_24_OUTPUTXBAR1             0x00101001U
#define GPIO_24_EQEP2A                  0x00101002U
#define GPIO_24_MDXB                    0x00101003U
#define GPIO_24_SPISIMOB                0x00101006U
#define GPIO_24_SD2_D1                  0x00101007U

#define GPIO_25_GPIO25                  0x00101200U
#define GPIO_25_OUTPUTXBAR2             0x00101201U
#define GPIO_25_EQEP2B                  0x00101202U
#define GPIO_25_MDRB                    0x00101203U
#define GPIO_25_SPISOMIB                0x00101206U
#define GPIO_25_SD2_C1                  0x00101207U

#define GPIO_26_GPIO26                  0x00101400U
#define GPIO_26_OUTPUTXBAR3             0x00101401U
#define GPIO_26_EQEP2I                  0x00101402U
#define GPIO_26_MCLKXB                  0x00101403U
#define GPIO_26_SPICLKB                 0x00101406U
#define GPIO_26_SD2_D2                  0x00101407U

#define GPIO_27_GPIO27                  0x00101600U
#define GPIO_27_OUTPUTXBAR4             0x00101601U
#define GPIO_27_EQEP2S                  0x00101602U
#define GPIO_27_MFSXB                   0x00101603U
#define GPIO_27_SPISTEB                 0x00101606U
#define GPIO_27_SD2_C2                  0x00101607U

#define GPIO_28_GPIO28                  0x00101800U
#define GPIO_28_SCIRXDA                 0x00101801U
#define GPIO_28_EM1CS4N                 0x00101802U
#define GPIO_28_OUTPUTXBAR5             0x00101805U
#define GPIO_28_EQEP3A                  0x00101806U
#define GPIO_28_SD2_D3                  0x00101807U

#define GPIO_29_GPIO29                  0x00101A00U
#define GPIO_29_SCITXDA                 0x00101A01U
#define GPIO_29_EM1SDCKE                0x00101A02U
#define GPIO_29_OUTPUTXBAR6             0x00101A05U
#define GPIO_29_EQEP3B                  0x00101A06U
#define GPIO_29_SD2_C3                  0x00101A07U

#define GPIO_30_GPIO30                  0x00101C00U
#define GPIO_30_CANRXA                  0x00101C01U
#define GPIO_30_EM1CLK                  0x00101C02U
#define GPIO_30_OUTPUTXBAR7             0x00101C05U
#define GPIO_30_EQEP3S                  0x00101C06U
#define GPIO_30_SD2_D4                  0x00101C07U

#define GPIO_31_GPIO31                  0x00101E00U
#define GPIO_31_CANTXA                  0x00101E01U
#define GPIO_31_EM1WEN                  0x00101E02U
#define GPIO_31_OUTPUTXBAR8             0x00101E05U
#define GPIO_31_EQEP3I                  0x00101E06U
#define GPIO_31_SD2_C4                  0x00101E07U

#define GPIO_32_GPIO32                  0x008C0000U
#define GPIO_32_SDAA                    0x008C0001U
#define GPIO_32_EM1CS0N                 0x008C0002U

#define GPIO_33_GPIO33                  0x008C0200U
#define GPIO_33_SCLA                    0x008C0201U
#define GPIO_33_EM1RNW                  0x008C0202U

#define GPIO_34_GPIO34                  0x008C0400U
#define GPIO_34_OUTPUTXBAR1             0x008C0401U
#define GPIO_34_EM1CS2N                 0x008C0402U
#define GPIO_34_SDAB                    0x008C0406U
#define GPIO_34_OFSD_2_N                0x008C040FU

#define GPIO_35_GPIO35                  0x008C0600U
#define GPIO_35_SCIRXDA                 0x008C0601U
#define GPIO_35_EM1CS3N                 0x008C0602U
#define GPIO_35_SCLB                    0x008C0606U
#define GPIO_35_IID                     0x008C060FU

#define GPIO_36_GPIO36                  0x008C0800U
#define GPIO_36_SCITXDA                 0x008C0801U
#define GPIO_36_EM1WAIT                 0x008C0802U
#define GPIO_36_CANRXA                  0x008C0806U
#define GPIO_36_ISESSEND                0x008C080FU

#define GPIO_37_GPIO37                  0x008C0A00U
#define GPIO_37_OUTPUTXBAR2             0x008C0A01U
#define GPIO_37_EM1OEN                  0x008C0A02U
#define GPIO_37_CANTXA                  0x008C0A06U
#define GPIO_37_IAVALID                 0x008C0A0FU

#define GPIO_38_GPIO38                  0x008C0C00U
#define GPIO_38_EM1A0                   0x008C0C02U
#define GPIO_38_SCITXDC                 0x008C0C05U
#define GPIO_38_CANTXB                  0x008C0C06U

#define GPIO_39_GPIO39                  0x008C0E00U
#define GPIO_39_EM1A1                   0x008C0E02U
#define GPIO_39_SCIRXDC                 0x008C0E05U
#define GPIO_39_CANRXB                  0x008C0E06U

#define GPIO_40_GPIO40                  0x008C1000U
#define GPIO_40_EM1A2                   0x008C1002U
#define GPIO_40_SDAB                    0x008C1006U

#define GPIO_41_GPIO41                  0x008C1200U
#define GPIO_41_EM1A3                   0x008C1202U
#define GPIO_41_EMU1                    0x008C1203U
#define GPIO_41_SCLB                    0x008C1206U

#define GPIO_42_GPIO42                  0x008C1400U
#define GPIO_42_SDAA                    0x008C1406U
#define GPIO_42_SCITXDA                 0x008C140FU

#define GPIO_43_GPIO43                  0x008C1600U
#define GPIO_43_SCLA                    0x008C1606U
#define GPIO_43_SCIRXDA                 0x008C160FU

#define GPIO_44_GPIO44                  0x008C1800U
#define GPIO_44_EM1A4                   0x008C1802U
#define GPIO_44_IXRCV                   0x008C180FU

#define GPIO_45_GPIO45                  0x008C1A00U
#define GPIO_45_EM1A5                   0x008C1A02U
#define GPIO_45_IDM                     0x008C1A0FU

#define GPIO_46_GPIO46                  0x008C1C00U
#define GPIO_46_EM1A6                   0x008C1C02U
#define GPIO_46_SCIRXDD                 0x008C1C06U
#define GPIO_46_IDP                     0x008C1C0FU

#define GPIO_47_GPIO47                  0x008C1E00U
#define GPIO_47_EM1A7                   0x008C1E02U
#define GPIO_47_SCITXDD                 0x008C1E06U
#define GPIO_47_OFSD_1_N                0x008C1E0FU

#define GPIO_48_GPIO48                  0x00900000U
#define GPIO_48_OUTPUTXBAR3             0x00900001U
#define GPIO_48_EM1A8                   0x00900002U
#define GPIO_48_SCITXDA                 0x00900006U
#define GPIO_48_SD1_D1                  0x00900007U

#define GPIO_49_GPIO49                  0x00900200U
#define GPIO_49_OUTPUTXBAR4             0x00900201U
#define GPIO_49_EM1A9                   0x00900202U
#define GPIO_49_SCIRXDA                 0x00900206U
#define GPIO_49_SD1_C1                  0x00900207U

#define GPIO_50_GPIO50                  0x00900400U
#define GPIO_50_EQEP1A                  0x00900401U
#define GPIO_50_EM1A10                  0x00900402U
#define GPIO_50_SPISIMOC                0x00900406U
#define GPIO_50_SD1_D2                  0x00900407U

#define GPIO_51_GPIO51                  0x00900600U
#define GPIO_51_EQEP1B                  0x00900601U
#define GPIO_51_EM1A11                  0x00900602U
#define GPIO_51_SPISOMIC                0x00900606U
#define GPIO_51_SD1_C2                  0x00900607U

#define GPIO_52_GPIO52                  0x00900800U
#define GPIO_52_EQEP1S                  0x00900801U
#define GPIO_52_EM1A12                  0x00900802U
#define GPIO_52_SPICLKC                 0x00900806U
#define GPIO_52_SD1_D3                  0x00900807U

#define GPIO_53_GPIO53                  0x00900A00U
#define GPIO_53_EQEP1I                  0x00900A01U
#define GPIO_53_EM1D31                  0x00900A02U
#define GPIO_53_EM2D15                  0x00900A03U
#define GPIO_53_SPISTEC                 0x00900A06U
#define GPIO_53_SD1_C3                  0x00900A07U

#define GPIO_54_GPIO54                  0x00900C00U
#define GPIO_54_SPISIMOA                0x00900C01U
#define GPIO_54_EM1D30                  0x00900C02U
#define GPIO_54_EM2D14                  0x00900C03U
#define GPIO_54_EQEP2A                  0x00900C05U
#define GPIO_54_SCITXDB                 0x00900C06U
#define GPIO_54_SD1_D4                  0x00900C07U

#define GPIO_55_GPIO55                  0x00900E00U
#define GPIO_55_SPISOMIA                0x00900E01U
#define GPIO_55_EM1D29                  0x00900E02U
#define GPIO_55_EM2D13                  0x00900E03U
#define GPIO_55_EQEP2B                  0x00900E05U
#define GPIO_55_SCIRXDB                 0x00900E06U
#define GPIO_55_SD1_C4                  0x00900E07U

#define GPIO_56_GPIO56                  0x00901000U
#define GPIO_56_SPICLKA                 0x00901001U
#define GPIO_56_EM1D28                  0x00901002U
#define GPIO_56_EM2D12                  0x00901003U
#define GPIO_56_EQEP2S                  0x00901005U
#define GPIO_56_SCITXDC                 0x00901006U
#define GPIO_56_SD2_D1                  0x00901007U

#define GPIO_57_GPIO57                  0x00901200U
#define GPIO_57_SPISTEA                 0x00901201U
#define GPIO_57_EM1D27                  0x00901202U
#define GPIO_57_EM2D11                  0x00901203U
#define GPIO_57_EQEP2I                  0x00901205U
#define GPIO_57_SCIRXDC                 0x00901206U
#define GPIO_57_SD2_C1                  0x00901207U

#define GPIO_58_GPIO58                  0x00901400U
#define GPIO_58_MCLKRA                  0x00901401U
#define GPIO_58_EM1D26                  0x00901402U
#define GPIO_58_EM2D10                  0x00901403U
#define GPIO_58_OUTPUTXBAR1             0x00901405U
#define GPIO_58_SPICLKB                 0x00901406U
#define GPIO_58_SD2_D2                  0x00901407U
#define GPIO_58_SPISIMOA                0x0090140FU

#define GPIO_59_GPIO59                  0x00901600U
#define GPIO_59_MFSRA                   0x00901601U
#define GPIO_59_EM1D25                  0x00901602U
#define GPIO_59_EM2D9                   0x00901603U
#define GPIO_59_OUTPUTXBAR2             0x00901605U
#define GPIO_59_SPISTEB                 0x00901606U
#define GPIO_59_SD2_C2                  0x00901607U
#define GPIO_59_SPISOMIA                0x0090160FU

#define GPIO_60_GPIO60                  0x00901800U
#define GPIO_60_MCLKRB                  0x00901801U
#define GPIO_60_EM1D24                  0x00901802U
#define GPIO_60_EM2D8                   0x00901803U
#define GPIO_60_OUTPUTXBAR3             0x00901805U
#define GPIO_60_SPISIMOB                0x00901806U
#define GPIO_60_SD2_D3                  0x00901807U
#define GPIO_60_SPICLKA                 0x0090180FU

#define GPIO_61_GPIO61                  0x00901A00U
#define GPIO_61_MFSRB                   0x00901A01U
#define GPIO_61_EM1D23                  0x00901A02U
#define GPIO_61_EM2D7                   0x00901A03U
#define GPIO_61_OUTPUTXBAR4             0x00901A05U
#define GPIO_61_SPISOMIB                0x00901A06U
#define GPIO_61_SD2_C3                  0x00901A07U
#define GPIO_61_SPISTEA                 0x00901A0FU

#define GPIO_62_GPIO62                  0x00901C00U
#define GPIO_62_SCIRXDC                 0x00901C01U
#define GPIO_62_EM1D22                  0x00901C02U
#define GPIO_62_EM2D6                   0x00901C03U
#define GPIO_62_EQEP3A                  0x00901C05U
#define GPIO_62_CANRXA                  0x00901C06U
#define GPIO_62_SD2_D4                  0x00901C07U

#define GPIO_63_GPIO63                  0x00901E00U
#define GPIO_63_SCITXDC                 0x00901E01U
#define GPIO_63_EM1D21                  0x00901E02U
#define GPIO_63_EM2D5                   0x00901E03U
#define GPIO_63_EQEP3B                  0x00901E05U
#define GPIO_63_CANTXA                  0x00901E06U
#define GPIO_63_SD2_C4                  0x00901E07U
#define GPIO_63_SPISIMOB                0x00901E0FU

#define GPIO_64_GPIO64                  0x010C0000U
#define GPIO_64_EM1D20                  0x010C0002U
#define GPIO_64_EM2D4                   0x010C0003U
#define GPIO_64_EQEP3S                  0x010C0005U
#define GPIO_64_SCIRXDA                 0x010C0006U
#define GPIO_64_SPISOMIB                0x010C000FU

#define GPIO_65_GPIO65                  0x010C0200U
#define GPIO_65_EM1D19                  0x010C0202U
#define GPIO_65_EM2D3                   0x010C0203U
#define GPIO_65_EQEP3I                  0x010C0205U
#define GPIO_65_SCITXDA                 0x010C0206U
#define GPIO_65_SPICLKB                 0x010C020FU

#define GPIO_66_GPIO66                  0x010C0400U
#define GPIO_66_EM1D18                  0x010C0402U
#define GPIO_66_EM2D2                   0x010C0403U
#define GPIO_66_SDAB                    0x010C0406U
#define GPIO_66_SPISTEB                 0x010C040FU

#define GPIO_67_GPIO67                  0x010C0600U
#define GPIO_67_EM1D17                  0x010C0602U
#define GPIO_67_EM2D1                   0x010C0603U

#define GPIO_68_GPIO68                  0x010C0800U
#define GPIO_68_EM1D16                  0x010C0802U
#define GPIO_68_EM2D0                   0x010C0803U

#define GPIO_69_GPIO69                  0x010C0A00U
#define GPIO_69_EM1D15                  0x010C0A02U
#define GPIO_69_EMU0                    0x010C0A03U
#define GPIO_69_SCLB                    0x010C0A06U
#define GPIO_69_SPISIMOC                0x010C0A0FU

#define GPIO_70_GPIO70                  0x010C0C00U
#define GPIO_70_EM1D14                  0x010C0C02U
#define GPIO_70_EMU0                    0x010C0C03U
#define GPIO_70_CANRXA                  0x010C0C05U
#define GPIO_70_SCITXDB                 0x010C0C06U
#define GPIO_70_SPISOMIC                0x010C0C0FU

#define GPIO_71_GPIO71                  0x010C0E00U
#define GPIO_71_EM1D13                  0x010C0E02U
#define GPIO_71_EMU1                    0x010C0E03U
#define GPIO_71_CANTXA                  0x010C0E05U
#define GPIO_71_SCIRXDB                 0x010C0E06U
#define GPIO_71_SPICLKC                 0x010C0E0FU

#define GPIO_72_GPIO72                  0x010C1000U
#define GPIO_72_EM1D12                  0x010C1002U
#define GPIO_72_CANTXB                  0x010C1005U
#define GPIO_72_SCITXDC                 0x010C1006U
#define GPIO_72_SPISTEC                 0x010C100FU

#define GPIO_73_GPIO73                  0x010C1200U
#define GPIO_73_EM1D11                  0x010C1202U
#define GPIO_73_XCLKOUT                 0x010C1203U
#define GPIO_73_CANRXB                  0x010C1205U
#define GPIO_73_SCIRXDC                 0x010C1206U

#define GPIO_74_GPIO74                  0x010C1400U
#define GPIO_74_EM1D10                  0x010C1402U

#define GPIO_75_GPIO75                  0x010C1600U
#define GPIO_75_EM1D9                   0x010C1602U

#define GPIO_76_GPIO76                  0x010C1800U
#define GPIO_76_EM1D8                   0x010C1802U
#define GPIO_76_SCITXDD                 0x010C1806U

#define GPIO_77_GPIO77                  0x010C1A00U
#define GPIO_77_EM1D7                   0x010C1A02U
#define GPIO_77_SCIRXDD                 0x010C1A06U

#define GPIO_78_GPIO78                  0x010C1C00U
#define GPIO_78_EM1D6                   0x010C1C02U
#define GPIO_78_EQEP2A                  0x010C1C06U

#define GPIO_79_GPIO79                  0x010C1E00U
#define GPIO_79_EM1D5                   0x010C1E02U
#define GPIO_79_EQEP2B                  0x010C1E06U

#define GPIO_80_GPIO80                  0x01100000U
#define GPIO_80_EM1D4                   0x01100002U
#define GPIO_80_EQEP2S                  0x01100006U

#define GPIO_81_GPIO81                  0x01100200U
#define GPIO_81_EM1D3                   0x01100202U
#define GPIO_81_EQEP2I                  0x01100206U

#define GPIO_82_GPIO82                  0x01100400U
#define GPIO_82_EM1D2                   0x01100402U

#define GPIO_83_GPIO83                  0x01100600U
#define GPIO_83_EM1D1                   0x01100602U

#define GPIO_84_GPIO84                  0x01100800U
#define GPIO_84_SCITXDA                 0x01100805U
#define GPIO_84_MDXB                    0x01100806U
#define GPIO_84_MDXA                    0x0110080FU

#define GPIO_85_GPIO85                  0x01100A00U
#define GPIO_85_EM1D0                   0x01100A02U
#define GPIO_85_SCIRXDA                 0x01100A05U
#define GPIO_85_MDRB                    0x01100A06U
#define GPIO_85_MDRA                    0x01100A0FU

#define GPIO_86_GPIO86                  0x01100C00U
#define GPIO_86_EM1A13                  0x01100C02U
#define GPIO_86_EM1CAS                  0x01100C03U
#define GPIO_86_SCITXDB                 0x01100C05U
#define GPIO_86_MCLKXB                  0x01100C06U
#define GPIO_86_MCLKXA                  0x01100C0FU

#define GPIO_87_GPIO87                  0x01100E00U
#define GPIO_87_EM1A14                  0x01100E02U
#define GPIO_87_EM1RAS                  0x01100E03U
#define GPIO_87_SCIRXDB                 0x01100E05U
#define GPIO_87_MFSXB                   0x01100E06U
#define GPIO_87_MFSXA                   0x01100E0FU

#define GPIO_88_GPIO88                  0x01101000U
#define GPIO_88_EM1A15                  0x01101002U
#define GPIO_88_EM1DQM0                 0x01101003U

#define GPIO_89_GPIO89                  0x01101200U
#define GPIO_89_EM1A16                  0x01101202U
#define GPIO_89_EM1DQM1                 0x01101203U
#define GPIO_89_SCITXDC                 0x01101206U

#define GPIO_90_GPIO90                  0x01101400U
#define GPIO_90_EM1A17                  0x01101402U
#define GPIO_90_EM1DQM2                 0x01101403U
#define GPIO_90_SCIRXDC                 0x01101406U

#define GPIO_91_GPIO91                  0x01101600U
#define GPIO_91_EM1A18                  0x01101602U
#define GPIO_91_EM1DQM3                 0x01101603U
#define GPIO_91_SDAA                    0x01101606U

#define GPIO_92_GPIO92                  0x01101800U
#define GPIO_92_EM1A19                  0x01101802U
#define GPIO_92_EM1BA1                  0x01101803U
#define GPIO_92_SCLA                    0x01101806U

#define GPIO_93_GPIO93                  0x01101A00U
#define GPIO_93_EM1A20                  0x01101A02U
#define GPIO_93_EM1BA0                  0x01101A03U
#define GPIO_93_SCITXDD                 0x01101A06U

#define GPIO_94_GPIO94                  0x01101C00U
#define GPIO_94_EM1A21                  0x01101C02U
#define GPIO_94_SCIRXDD                 0x01101C06U

#define GPIO_95_GPIO95                  0x01101E00U

#define GPIO_96_GPIO96                  0x018C0000U
#define GPIO_96_EM2DQM1                 0x018C0003U
#define GPIO_96_EQEP1A                  0x018C0005U

#define GPIO_97_GPIO97                  0x018C0200U
#define GPIO_97_EM2DQM0                 0x018C0203U
#define GPIO_97_EQEP1B                  0x018C0205U

#define GPIO_98_GPIO98                  0x018C0400U
#define GPIO_98_EM2A0                   0x018C0403U
#define GPIO_98_EQEP1S                  0x018C0405U

#define GPIO_99_GPIO99                  0x018C0600U
#define GPIO_99_EM2A1                   0x018C0603U
#define GPIO_99_EQEP1I                  0x018C0605U

#define GPIO_100_GPIO100                0x018C0800U
#define GPIO_100_EM2A2                  0x018C0803U
#define GPIO_100_EQEP2A                 0x018C0805U
#define GPIO_100_SPISIMOC               0x018C0806U

#define GPIO_101_GPIO101                0x018C0A00U
#define GPIO_101_EM2A3                  0x018C0A03U
#define GPIO_101_EQEP2B                 0x018C0A05U
#define GPIO_101_SPISOMIC               0x018C0A06U

#define GPIO_102_GPIO102                0x018C0C00U
#define GPIO_102_EM2A4                  0x018C0C03U
#define GPIO_102_EQEP2S                 0x018C0C05U
#define GPIO_102_SPICLKC                0x018C0C06U

#define GPIO_103_GPIO103                0x018C0E00U
#define GPIO_103_EM2A5                  0x018C0E03U
#define GPIO_103_EQEP2I                 0x018C0E05U
#define GPIO_103_SPISTEC                0x018C0E06U

#define GPIO_104_GPIO104                0x018C1000U
#define GPIO_104_SDAA                   0x018C1001U
#define GPIO_104_EM2A6                  0x018C1003U
#define GPIO_104_EQEP3A                 0x018C1005U
#define GPIO_104_SCITXDD                0x018C1006U

#define GPIO_105_GPIO105                0x018C1200U
#define GPIO_105_SCLA                   0x018C1201U
#define GPIO_105_EM2A7                  0x018C1203U
#define GPIO_105_EQEP3B                 0x018C1205U
#define GPIO_105_SCIRXDD                0x018C1206U

#define GPIO_106_GPIO106                0x018C1400U
#define GPIO_106_EM2A8                  0x018C1403U
#define GPIO_106_EQEP3S                 0x018C1405U
#define GPIO_106_SCITXDC                0x018C1406U

#define GPIO_107_GPIO107                0x018C1600U
#define GPIO_107_EM2A9                  0x018C1603U
#define GPIO_107_EQEP3I                 0x018C1605U
#define GPIO_107_SCIRXDC                0x018C1606U

#define GPIO_108_GPIO108                0x018C1800U
#define GPIO_108_EM2A10                 0x018C1803U

#define GPIO_109_GPIO109                0x018C1A00U
#define GPIO_109_EM2A11                 0x018C1A03U

#define GPIO_110_GPIO110                0x018C1C00U
#define GPIO_110_EM2WAIT                0x018C1C03U

#define GPIO_111_GPIO111                0x018C1E00U
#define GPIO_111_EM2BA0                 0x018C1E03U

#define GPIO_112_GPIO112                0x01900000U
#define GPIO_112_EM2BA1                 0x01900003U

#define GPIO_113_GPIO113                0x01900200U
#define GPIO_113_EM2CAS                 0x01900203U

#define GPIO_114_GPIO114                0x01900400U
#define GPIO_114_EM2RAS                 0x01900403U

#define GPIO_115_GPIO115                0x01900600U
#define GPIO_115_EM2CS0N                0x01900603U

#define GPIO_116_GPIO116                0x01900800U
#define GPIO_116_EM2CS2N                0x01900803U

#define GPIO_117_GPIO117                0x01900A00U
#define GPIO_117_EM2SDCKE               0x01900A03U

#define GPIO_118_GPIO118                0x01900C00U
#define GPIO_118_EM2CLK                 0x01900C03U

#define GPIO_119_GPIO119                0x01900E00U
#define GPIO_119_EM2RNW                 0x01900E03U

#define GPIO_120_GPIO120                0x01901000U
#define GPIO_120_EM2WEN                 0x01901003U
#define GPIO_120_USB0PFLT               0x0190100FU

#define GPIO_121_GPIO121                0x01901200U
#define GPIO_121_EM2OEN                 0x01901203U
#define GPIO_121_USB0EPEN               0x0190120FU

#define GPIO_122_GPIO122                0x01901400U
#define GPIO_122_SPISIMOC               0x01901406U
#define GPIO_122_SD1_D1                 0x01901407U
#define GPIO_122_ODISCHRGVBUS           0x0190140FU

#define GPIO_123_GPIO123                0x01901600U
#define GPIO_123_SPISOMIC               0x01901606U
#define GPIO_123_SD1_C1                 0x01901607U
#define GPIO_123_OCHRGVBUS              0x0190160FU

#define GPIO_124_GPIO124                0x01901800U
#define GPIO_124_SPICLKC                0x01901806U
#define GPIO_124_SD1_D2                 0x01901807U
#define GPIO_124_ODMPULLDN              0x0190180FU

#define GPIO_125_GPIO125                0x01901A00U
#define GPIO_125_SPISTEC                0x01901A06U
#define GPIO_125_SD1_C2                 0x01901A07U
#define GPIO_125_ODPPULLDN              0x01901A0FU

#define GPIO_126_GPIO126                0x01901C00U
#define GPIO_126_SD1_D3                 0x01901C07U
#define GPIO_126_OLSD_2_N               0x01901C0FU

#define GPIO_127_GPIO127                0x01901E00U
#define GPIO_127_SD1_C3                 0x01901E07U
#define GPIO_127_OLSD_1_N               0x01901E0FU

#define GPIO_128_GPIO128                0x020C0000U
#define GPIO_128_SD1_D4                 0x020C0007U
#define GPIO_128_OIDPULLUP              0x020C000FU

#define GPIO_129_GPIO129                0x020C0200U
#define GPIO_129_SD1_C4                 0x020C0207U
#define GPIO_129_OSPEED                 0x020C020FU

#define GPIO_130_GPIO130                0x020C0400U
#define GPIO_130_SD2_D1                 0x020C0407U
#define GPIO_130_OSUSPEND               0x020C040FU

#define GPIO_131_GPIO131                0x020C0600U
#define GPIO_131_SD2_C1                 0x020C0607U
#define GPIO_131_OOE                    0x020C060FU

#define GPIO_132_GPIO132                0x020C0800U
#define GPIO_132_SD2_D2                 0x020C0807U
#define GPIO_132_ODMSE1                 0x020C080FU

#define GPIO_133_GPIO133                0x020C0A00U
#define GPIO_133_SD2_C2                 0x020C0A07U
#define GPIO_133_ODPDAT                 0x020C0A0FU

#define GPIO_134_GPIO134                0x020C0C00U
#define GPIO_134_SD2_D3                 0x020C0C07U
#define GPIO_134_IVBUSVALID             0x020C0C0FU

#define GPIO_135_GPIO135                0x020C0E00U
#define GPIO_135_SCITXDA                0x020C0E06U
#define GPIO_135_SD2_C3                 0x020C0E07U

#define GPIO_136_GPIO136                0x020C1000U
#define GPIO_136_SCIRXDA                0x020C1006U
#define GPIO_136_SD2_D4                 0x020C1007U

#define GPIO_137_GPIO137                0x020C1200U
#define GPIO_137_SCITXDB                0x020C1206U
#define GPIO_137_SD2_C4                 0x020C1207U

#define GPIO_138_GPIO138                0x020C1400U
#define GPIO_138_SCIRXDB                0x020C1406U

#define GPIO_139_GPIO139                0x020C1600U
#define GPIO_139_SCIRXDC                0x020C1606U

#define GPIO_140_GPIO140                0x020C1800U
#define GPIO_140_SCITXDC                0x020C1806U

#define GPIO_141_GPIO141                0x020C1A00U
#define GPIO_141_SCIRXDD                0x020C1A06U

#define GPIO_142_GPIO142                0x020C1C00U
#define GPIO_142_SCITXDD                0x020C1C06U

#define GPIO_143_GPIO143                0x020C1E00U

#define GPIO_144_GPIO144                0x02100000U

#define GPIO_145_GPIO145                0x02100200U
#define GPIO_145_EPWM1A                 0x02100201U

#define GPIO_146_GPIO146                0x02100400U
#define GPIO_146_EPWM1B                 0x02100401U

#define GPIO_147_GPIO147                0x02100600U
#define GPIO_147_EPWM2A                 0x02100601U

#define GPIO_148_GPIO148                0x02100800U
#define GPIO_148_EPWM2B                 0x02100801U

#define GPIO_149_GPIO149                0x02100A00U
#define GPIO_149_EPWM3A                 0x02100A01U

#define GPIO_150_GPIO150                0x02100C00U
#define GPIO_150_EPWM3B                 0x02100C01U

#define GPIO_151_GPIO151                0x02100E00U
#define GPIO_151_EPWM4A                 0x02100E01U

#define GPIO_152_GPIO152                0x02101000U
#define GPIO_152_EPWM4B                 0x02101001U

#define GPIO_153_GPIO153                0x02101200U
#define GPIO_153_EPWM5A                 0x02101201U

#define GPIO_154_GPIO154                0x02101400U
#define GPIO_154_EPWM5B                 0x02101401U

#define GPIO_155_GPIO155                0x02101600U
#define GPIO_155_EPWM6A                 0x02101601U

#define GPIO_156_GPIO156                0x02101800U
#define GPIO_156_EPWM6B                 0x02101801U

#define GPIO_157_GPIO157                0x02101A00U
#define GPIO_157_EPWM7A                 0x02101A01U

#define GPIO_158_GPIO158                0x02101C00U
#define GPIO_158_EPWM7B                 0x02101C01U

#define GPIO_159_GPIO159                0x02101E00U
#define GPIO_159_EPWM8A                 0x02101E01U

#define GPIO_160_GPIO160                0x028C0000U
#define GPIO_160_EPWM8B                 0x028C0001U

#define GPIO_161_GPIO161                0x028C0200U
#define GPIO_161_EPWM9A                 0x028C0201U

#define GPIO_162_GPIO162                0x028C0400U
#define GPIO_162_EPWM9B                 0x028C0401U

#define GPIO_163_GPIO163                0x028C0600U
#define GPIO_163_EPWM10A                0x028C0601U

#define GPIO_164_GPIO164                0x028C0800U
#define GPIO_164_EPWM10B                0x028C0801U

#define GPIO_165_GPIO165                0x028C0A00U
#define GPIO_165_EPWM11A                0x028C0A01U

#define GPIO_166_GPIO166                0x028C0C00U
#define GPIO_166_EPWM11B                0x028C0C01U

#define GPIO_167_GPIO167                0x028C0E00U
#define GPIO_167_EPWM12A                0x028C0E01U

#define GPIO_168_GPIO168                0x028C1000U
#define GPIO_168_EPWM12B                0x028C1001U

#endif // PIN_MAP_H
