//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//#############################################################################
//
// Release for HXS320F28379D, Hal DriverLib, 1.0.0
//
// Release time: 2023-08-30 03:07:28.027222
//
//#############################################################################


#ifndef HW_CLA_H
#define HW_CLA_H

//*************************************************************************************************
//
// The following are defines for the CLA register offsets
//
//*************************************************************************************************
#ifndef __HXS320F28XX_CLA__
#define CLA_O_MVECT1             0x0U    // Task Interrupt Vector
#define CLA_O_MVECT2             0x4U    // Task Interrupt Vector
#define CLA_O_MVECT3             0x8U    // Task Interrupt Vector
#define CLA_O_MVECT4             0xCU    // Task Interrupt Vector
#define CLA_O_MVECT5             0x10U   // Task Interrupt Vector
#define CLA_O_MVECT6             0x14U   // Task Interrupt Vector
#define CLA_O_MVECT7             0x18U   // Task Interrupt Vector
#define CLA_O_MVECT8             0x1CU   // Task Interrupt Vector
#define CLA_O_MCTL               0x20U   // Control Register
#define CLA_O_MVECTBGRNDACTIVE   0x24U   // Active register for MVECTBGRND
#define CLA_O_MSTSBGRND          0x2CU   // Status register for the back ground task
#define CLA_O_MCTLBGRND          0x30U   // Control register for the back ground task
#define CLA_O_MVECTBGRND         0x34U   // Vector for the back ground task
#define CLA_O_MIFR               0x38U   // Interrupt Flag Register
#define CLA_O_MIOVF              0x3CU   // Interrupt Overflow Flag Register
#define CLA_O_MIFRC              0x40U   // Interrupt Force Register
#define CLA_O_MICLR              0x44U   // Interrupt Flag Clear Register
#define CLA_O_MICLROVF           0x48U   // Interrupt Overflow Flag Clear Register
#define CLA_O_MIER               0x4CU   // Interrupt Enable Register
#define CLA_O_MIRUN              0x50U   // Interrupt Run Status Register
#define CLA_O_MPC                0x54U   // CLA Program Counter
#define CLA_O_MSTF               0x58U   // CLA Floating-Point Status Register
#define CLA_O_MPSACTL            0x5CU   // CLA PSA Control Register
#define CLA_O_MPSA1              0x60U   // CLA PSA1 Register
#define CLA_O_MPSA2              0x64U   // CLA PSA2 Register
#define CLA_O_RPC                0x64U   // Return program counter Register
#endif

#define CLA_O_SOFTINTEN   0x0U   // CLA Software Interrupt Enable Register

#ifdef __HXS320F28XX_CLA__
#define CLA_O_MVECTBGRNDACTIVE   0x0U   // Active register for MVECTBGRND.
#define CLA_O_MPSACTL            0x4U   // CLA PSA Control Register
#define CLA_O_MPSA1              0x8U   // CLA PSA1 Register
#define CLA_O_MPSA2              0xCU   // CLA PSA2 Register
#define CLA_O_SOFTINTEN          0x10U   // CLA Software Interrupt Enable Register
#define CLA_O_SOFTINTFRC         0x14U   // CLA Software Interrupt Force Register
#endif
//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT1 register
//
//*************************************************************************************************
#define CLA_MVECT1_MVECT_S   0U
#define CLA_MVECT1_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT2 register
//
//*************************************************************************************************
#define CLA_MVECT2_MVECT_S   0U
#define CLA_MVECT2_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT3 register
//
//*************************************************************************************************
#define CLA_MVECT3_MVECT_S   0U
#define CLA_MVECT3_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT4 register
//
//*************************************************************************************************
#define CLA_MVECT4_MVECT_S   0U
#define CLA_MVECT4_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT5 register
//
//*************************************************************************************************
#define CLA_MVECT5_MVECT_S   0U
#define CLA_MVECT5_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT6 register
//
//*************************************************************************************************
#define CLA_MVECT6_MVECT_S   0U
#define CLA_MVECT6_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT7 register
//
//*************************************************************************************************
#define CLA_MVECT7_MVECT_S   0U
#define CLA_MVECT7_MVECT_M   0xFFFFFFFFU   // MPC Start Address

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECT8 register
//
//*************************************************************************************************
#define CLA_MVECT8_MVECT_S   0U
#define CLA_MVECT8_MVECT_M   0xFFFFFFFFU   // MPC Start Address

#ifndef __HXS320F28XX_CLA__
//*************************************************************************************************
//
// The following are defines for the bit fields in the MCTL register
//
//*************************************************************************************************
#define CLA_MCTL_HARDRESET   0x1U   // Hard Reset
#define CLA_MCTL_SOFTRESET   0x2U   // Soft Reset
#define CLA_MCTL_IACKE       0x4U   // IACK enable

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECTBGRNDACTIVE register
//
//*************************************************************************************************
#define CLA_MVECTBGRNDACTIVE_I16_S   0U
#define CLA_MVECTBGRNDACTIVE_I16_M   0xFFFFU   // Gives the current interrupted MPC value of the background task

//*************************************************************************************************
//
// The following are defines for the bit fields in the MSTSBGRND register
//
//*************************************************************************************************
#define CLA_MSTSBGRND_RUN      0x1U   // Value of 1 indicates that background task is running.Value of 0 indicates that background task is not running
#define CLA_MSTSBGRND_BGINTM   0x2U   // Value of 1 indicates that backgroiund task will not be interrupted
#define CLA_MSTSBGRND_BGOVF    0x4U   // Value of 1 indicates a hardware trigger (which is enabled) occurred while the MCTLBGRND.BGSTART bit is set.

//*************************************************************************************************
//
// The following are defines for the bit fields in the MCTLBGRND register
//
//*************************************************************************************************
#define CLA_MCTLBGRND_BGSTART   0x1U      // Value of 1 will start the background task, provided there are no other pending tasks
#define CLA_MCTLBGRND_TRIGEN    0x2U      // Hardware trigger enable for the background task
#define CLA_MCTLBGRND_BGEN      0x8000U   // 0 Background task is disabled, BGSTART will not be set either in a hardware trigger or by writing 1 to BGSTART bit.1 Background task is enabled and MIER[INT8] will be cleared

//*************************************************************************************************
//
// The following are defines for the bit fields in the MVECTBGRND register
//
//*************************************************************************************************
#define CLA_MVECTBGRND_I16_S   0U
#define CLA_MVECTBGRND_I16_M   0xFFFFU   // MPC Start Address: These bits specify the start address for the background task

//*************************************************************************************************
//
// The following are defines for the bit fields in the MIFR register
//
//*************************************************************************************************
#define CLA_MIFR_INT1   0x1U    // Task 1 Interrupt Flag
#define CLA_MIFR_INT2   0x2U    // Task 2 Interrupt Flag
#define CLA_MIFR_INT3   0x4U    // Task 3 Interrupt Flag
#define CLA_MIFR_INT4   0x8U    // Task 4 Interrupt Flag
#define CLA_MIFR_INT5   0x10U   // Task 5 Interrupt Flag
#define CLA_MIFR_INT6   0x20U   // Task 6 Interrupt Flag
#define CLA_MIFR_INT7   0x40U   // Task 7 Interrupt Flag
#define CLA_MIFR_INT8   0x80U   // Task 8 Interrupt Flag

//*************************************************************************************************
//
// The following are defines for the bit fields in the MIOVF register
//
//*************************************************************************************************
#define CLA_MIOVF_INT1   0x1U    // Task 1 Interrupt Overflow Flag
#define CLA_MIOVF_INT2   0x2U    // Task 2 Interrupt Overflow Flag
#define CLA_MIOVF_INT3   0x4U    // Task 3 Interrupt Overflow Flag
#define CLA_MIOVF_INT4   0x8U    // Task 4 Interrupt Overflow Flag
#define CLA_MIOVF_INT5   0x10U   // Task 5 Interrupt Overflow Flag
#define CLA_MIOVF_INT6   0x20U   // Task 6 Interrupt Overflow Flag
#define CLA_MIOVF_INT7   0x40U   // Task 7 Interrupt Overflow Flag
#define CLA_MIOVF_INT8   0x80U   // Task 8 Interrupt Overflow Flag

//*************************************************************************************************
//
// The following are defines for the bit fields in the MIFRC register
//
//*************************************************************************************************
#define CLA_MIFRC_INT1   0x1U    // Task 1 Interrupt Force
#define CLA_MIFRC_INT2   0x2U    // Task 2 Interrupt Force
#define CLA_MIFRC_INT3   0x4U    // Task 3 Interrupt Force
#define CLA_MIFRC_INT4   0x8U    // Task 4 Interrupt Force
#define CLA_MIFRC_INT5   0x10U   // Task 5 Interrupt Force
#define CLA_MIFRC_INT6   0x20U   // Task 6 Interrupt Force
#define CLA_MIFRC_INT7   0x40U   // Task 7 Interrupt Force
#define CLA_MIFRC_INT8   0x80U   // Task 8 Interrupt Force

//*************************************************************************************************
//
// The following are defines for the bit fields in the MICLR register
//
//*************************************************************************************************
#define CLA_MICLR_INT1   0x1U    // Task 1 Interrupt Flag Clear
#define CLA_MICLR_INT2   0x2U    // Task 2 Interrupt Flag Clear
#define CLA_MICLR_INT3   0x4U    // Task 3 Interrupt Flag Clear
#define CLA_MICLR_INT4   0x8U    // Task 4 Interrupt Flag Clear
#define CLA_MICLR_INT5   0x10U   // Task 5 Interrupt Flag Clear
#define CLA_MICLR_INT6   0x20U   // Task 6 Interrupt Flag Clear
#define CLA_MICLR_INT7   0x40U   // Task 7 Interrupt Flag Clear
#define CLA_MICLR_INT8   0x80U   // Task 8 Interrupt Flag Clear

//*************************************************************************************************
//
// The following are defines for the bit fields in the MICLROVF register
//
//*************************************************************************************************
#define CLA_MICLROVF_INT1   0x1U    // Task 1 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT2   0x2U    // Task 2 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT3   0x4U    // Task 3 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT4   0x8U    // Task 4 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT5   0x10U   // Task 5 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT6   0x20U   // Task 6 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT7   0x40U   // Task 7 Interrupt Overflow Flag Clear
#define CLA_MICLROVF_INT8   0x80U   // Task 8 Interrupt Overflow Flag Clear

//*************************************************************************************************
//
// The following are defines for the bit fields in the MIER register
//
//*************************************************************************************************
#define CLA_MIER_INT1   0x1U    // Task 1 Interrupt Enable
#define CLA_MIER_INT2   0x2U    // Task 2 Interrupt Enable
#define CLA_MIER_INT3   0x4U    // Task 3 Interrupt Enable
#define CLA_MIER_INT4   0x8U    // Task 4 Interrupt Enable
#define CLA_MIER_INT5   0x10U   // Task 5 Interrupt Enable
#define CLA_MIER_INT6   0x20U   // Task 6 Interrupt Enable
#define CLA_MIER_INT7   0x40U   // Task 7 Interrupt Enable
#define CLA_MIER_INT8   0x80U   // Task 8 Interrupt Enable

//*************************************************************************************************
//
// The following are defines for the bit fields in the MIRUN register
//
//*************************************************************************************************
#define CLA_MIRUN_INT1   0x1U    // Task 1 Run Status
#define CLA_MIRUN_INT2   0x2U    // Task 2 Run Status
#define CLA_MIRUN_INT3   0x4U    // Task 3 Run Status
#define CLA_MIRUN_INT4   0x8U    // Task 4 Run Status
#define CLA_MIRUN_INT5   0x10U   // Task 5 Run Status
#define CLA_MIRUN_INT6   0x20U   // Task 6 Run Status
#define CLA_MIRUN_INT7   0x40U   // Task 7 Run Status
#define CLA_MIRUN_INT8   0x80U   // Task 8 Run Status

//*************************************************************************************************
//
// The following are defines for the bit fields in the MPC register
//
//*************************************************************************************************
#define CLA_MPC_MPC_S   0U
#define CLA_MPC_MPC_M   0xFFFFU   // Program Counter

//*************************************************************************************************
//
// The following are defines for the bit fields in the MSTF register
//
//*************************************************************************************************
#define CLA_MSTF_LVF        0x1U     // Latched Overflow Flag
#define CLA_MSTF_LUF        0x2U     // Latched Underflow Flag
#define CLA_MSTF_RNDF32_S   7U
#define CLA_MSTF_RNDF32_M   0x380U   // Round 32-bit Floating-Point Mode
#define CLA_MSTF_MEALLOW    0x800U   // MEALLOW Status

//*************************************************************************************************
//
// The following are defines for the bit fields in the MPSACTL register
//
//*************************************************************************************************
#define CLA_MPSACTL_MPABSTART    0x1U    // CLA Program Address Bus PSA1 Start/Stop Bit
#define CLA_MPSACTL_MPABCYC      0x2U    // CLA Program Address Bus PSA1 Cycle/Event Based Bit
#define CLA_MPSACTL_MDWDBSTART   0x4U    // CLA Data Write Data Bus PSA2 Start/Stop Bit
#define CLA_MPSACTL_MDWDBCYC     0x8U    // CLA Data Write Data Bus PSA2 Cycle or Event Based Bit
#define CLA_MPSACTL_MPSA1CLEAR   0x10U   // CLA PSA1 Clear Bit
#define CLA_MPSACTL_MPSA2CLEAR   0x20U   // CLA PSA2 Clear Bit
#define CLA_MPSACTL_MPSA2CFG     0x40U   // CLA PSA2 Polynomial Configuration Bits

#endif

#ifdef __HXS320F28XX_CLA__
//*************************************************************************************************
//
// The following are defines for the bit fields in the RPC register
//
//*************************************************************************************************
#define CLA_RPC_RPC_S   0U
#define CLA_RPC_RPC_M   0xFFFFFFFFU   // Return program counter

//*************************************************************************************************
//
// The following are defines for the bit fields in the SOFTINTEN register
//
//*************************************************************************************************
#define CLA_SOFTINTEN_TASK1   0x1U    // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK2   0x2U    // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK3   0x4U    // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK4   0x8U    // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK5   0x10U   // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK6   0x20U   // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK7   0x40U   // Configure Software Interrupt or End of Task interrupt.
#define CLA_SOFTINTEN_TASK8   0x80U   // Configure Software Interrupt or End of Task interrupt.


#endif

#endif
