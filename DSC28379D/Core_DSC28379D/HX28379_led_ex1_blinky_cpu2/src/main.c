/******************************************************************
 文 档 名：      led_ex1_blinky_cpu2
 开 发 环 境：   Haawking IDE V2.2.7
 开 发 板：       Core_DSC28379_V1.0
 D S P：          DSC28379
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：
	 该示例演示如何使用CPU1闪烁一个LED，并使用CPU2闪烁另一个LED（led_ex1_blinky_cpu2.c）。


 连接方式：
	 无

 现象：


 版 本：      V1.0.0
 时 间：      2024年06月12日
 作 者：      duohan
 @ mail：    support@mail.haawking.com
 *******************************************************************************/
/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"

#define _STANDALONE
#define _FLASH

void delay_loop(void);

int main(void)
{
	//
    // Intialize device clock and peripherals
    //
    Device_init();


    //
	// Initialize GPIO and configure the GPIO pin as a push-pull output
	//
	Device_initGPIO();
//	GPIO_setPadConfig(DEVICE_GPIO_PIN_LED2, GPIO_PIN_TYPE_STD);
//	GPIO_setDirectionMode(DEVICE_GPIO_PIN_LED2, GPIO_DIR_MODE_OUT);
	GPIO_setPadConfig(DEVICE_GPIO_PIN_LED2, GPIO_PIN_TYPE_STD);
	GPIO_setDirectionMode(DEVICE_GPIO_PIN_LED2, GPIO_DIR_MODE_OUT);

	//
	// Configure CPU2 to control the LED GPIO
	//
	GPIO_setMasterCore(DEVICE_GPIO_PIN_LED2, GPIO_CORE_CPU2);

	//
	// Initialize PIE and clear PIE registers. Disables CPU interrupts.
	//
	Interrupt_initModule();

	//
	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	//
	Interrupt_initVectorTable();

	//
	// Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
	//
	EINT;
	ERTM;

	//
	// Loop Forever
	//
	for(;;)
	{
		//
		// Turn on LED
		//
		GPIO_writePin(DEVICE_GPIO_PIN_LED2, 0);

		//
		// Delay for a bit.
		//
		//DEVICE_DELAY_US(500000);
		delay_loop();
		//
		// Turn off LED
		//
		GPIO_writePin(DEVICE_GPIO_PIN_LED2, 1);

		//
		// Delay for a bit.
		//
		//DEVICE_DELAY_US(500000);
		delay_loop();
	}
}

void delay_loop()
{
	int   i;
	for (i = 0; i < 20000; i++)
	{
		asm volatile(".align 2; rpti 3000, 4; NOP;");
	}
}
//
// End of File
//

