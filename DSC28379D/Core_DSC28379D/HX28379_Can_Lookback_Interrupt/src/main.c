/******************************************************************
 文 档 名：      can_ex2_loopback_interrupts
 开 发 环 境：   Haawking IDE V2.2.7
 开 发 板：       Core_DSC28379_V1.0
 D S P：          DSC28379
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：
  该示例展示了CAN的基本设置，以便在CAN总线上发送和接收消息。CAN外设被
  配置为使用特定的CAN ID发送消息。然后，使用简单的延迟循环进行定时，
  每秒发送一次消息。发送的消息是一个包含递增模式的4字节消息。使用CAN中
  断处理程序来确认消息发送并计算已发送的消息数量。

  该示例将CAN控制器设置为外部环回测试模式。传输的数据可在CANTXA引脚上
  看到，并在内部接收回到CAN核心。请参阅CAN章节中关于外部环回测试模式的
  详细信息，这些信息可在技术参考手册中找到。。


 连接方式：无
 现象：
  - txMsgCount - 发送的消息数量计数器
  - rxMsgCount - 接收的消息数量计数器
  - txMsgData - 发送数据的数组
  - rxMsgData - 接收到的数据的数组
  - errorFlag - 表示发生错误的标志

 版 本：      V1.0.0
 时 间：      2024年06月12日
 作 者：      duohan
 @ mail：    support@mail.haawking.com
 ****************************************************************************************/
/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"


//
// Defines
//
#define MSG_DATA_LENGTH    4
#define TX_MSG_OBJ_ID      1
#define RX_MSG_OBJ_ID      2

//
// Globals
//
volatile uint32_t txMsgCount= 0;
volatile uint32_t rxMsgCount = 0;
volatile uint32_t errorFlag = 0;
uint16_t txMsgData[4];
uint16_t rxMsgData[4];

//
// Function Prototypes
//
__interrupt void canISR(void);


int main(void)
{
    Device_init();

    //
	// Initialize GPIO and configure GPIO pins for CANTX/CANRX
	//
	Device_initGPIO();
	GPIO_setPinConfig(DEVICE_GPIO_CFG_CANRXA);
	GPIO_setPinConfig(DEVICE_GPIO_CFG_CANTXA);

	//
	// Initialize the CAN controller
	//
	// Puts the module in Initialization mode, Disables the parity function
	// Initializes the MBX RAM, Initiates a S/W reset of the module
	// Seeks write-access to configuration registers.
	//
	CAN_initModule(CANA_BASE);

	//
	// Set up the CAN bus bit rate to 500 kbps
	// Refer to the Driver Library User Guide for information on how to set
	// tighter timing control. Additionally, consult the device data sheet
	// for more information about the CAN module clocking.
	//
	CAN_setBitRate(CANA_BASE, DEVICE_SYSCLK_FREQ, 500000, 20);

	//
	// Enable interrupts on the CAN peripheral.
	// Enables Interrupt line 0, Error & Status Change interrupts in CAN_CTL
	// register.
	//
	CAN_enableInterrupt(CANA_BASE, CAN_INT_IE0 | CAN_INT_ERROR |
					   CAN_INT_STATUS);

	//
	// Initialize PIE and clear PIE registers. Disables CPU interrupts.
	//
	Interrupt_initModule();

	//
	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	//
	Interrupt_initVectorTable();

	//
	// Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
	//
	EINT;
	ERTM;

	//
	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	// This registers the interrupt handler in PIE vector table.
	//
	Interrupt_register(INT_CANA0, &canISR);

	//
	// Enable the CAN interrupt signal
	//
	Interrupt_enable(INT_CANA0);
	CAN_enableGlobalInterrupt(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

	//
	// Enable CAN test mode with external loopback
	//
	CAN_enableTestMode(CANA_BASE, CAN_TEST_EXL);

	//
	// Initialize the transmit message object used for sending CAN messages.
	// Message Object Parameters:
	//      Message Object ID Number: 1
	//      Message Identifier: 0x1
	//      Message Frame: Standard
	//      Message Type: Transmit
	//      Message ID Mask: 0x0
	//      Message Object Flags: Transmit Interrupt
	//      Message Data Length: 4 Bytes
	//
	CAN_setupMessageObject(CANA_BASE, TX_MSG_OBJ_ID, 0x1, CAN_MSG_FRAME_STD,
						  CAN_MSG_OBJ_TYPE_TX, 0, CAN_MSG_OBJ_TX_INT_ENABLE,
						  MSG_DATA_LENGTH);

	//
	// Initialize the receive message object used for receiving CAN messages.
	// Message Object Parameters:
	//      Message Object ID Number: 2
	//      Message Identifier: 0x1
	//      Message Frame: Standard
	//      Message Type: Receive
	//      Message ID Mask: 0x0
	//      Message Object Flags: Receive Interrupt
	//      Message Data Length: 4 Bytes (Note that DLC field is a "don't care"
	//      for a Receive mailbox)
	//
	CAN_setupMessageObject(CANA_BASE, RX_MSG_OBJ_ID, 0x1, CAN_MSG_FRAME_STD,
						  CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
						  MSG_DATA_LENGTH);

	//
	// Initialize the transmit message object data buffer to be sent
	//
	txMsgData[0] = 0x12;
	txMsgData[1] = 0x34;
	txMsgData[2] = 0x56;
	txMsgData[3] = 0x78;

	//
	// Start CAN module operations
	//
	CAN_startModule(CANA_BASE);

	// Check the error flag to see if errors occurred
	//
	if(errorFlag)
	{
		// Fail
	   while(1);
	}

	//
	// Verify that the number of transmitted messages equal the number of
	// messages received before sending a new message
	//
	if(txMsgCount == rxMsgCount)
	{
	   CAN_sendMessage(CANA_BASE, TX_MSG_OBJ_ID, MSG_DATA_LENGTH,
					   txMsgData);


	}
	else
	{
	   errorFlag = 1;
	}

	//
	// Delay 1 second before continuing
	//
	DEVICE_DELAY_US(10);

    while(1){};
    return 0;
}

//
// CAN ISR - The interrupt service routine called when a CAN interrupt is
//           triggered.  It checks for the cause of the interrupt, and
//           maintains a count of all messages that have been transmitted.
//
__interrupt void
canISR(void)
{
    uint32_t status;

    //
    // Read the CAN interrupt status to find the cause of the interrupt
    //
    status = CAN_getInterruptCause(CANA_BASE);

    //
    // If the cause is a controller status interrupt, then get the status
    //
    if(status == CAN_INT_INT0ID_STATUS)
    {
        //
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors.  Error processing
        // is not done in this example for simplicity.  Refer to the
        // API documentation for details about the error status bits.
        // The act of reading this status will clear the interrupt.
        //
        status = CAN_getStatus(CANA_BASE);

        //
        // Check to see if an error occurred.
        //
        if(((status  & ~(CAN_STATUS_TXOK | CAN_STATUS_RXOK)) != 7) &&
           ((status  & ~(CAN_STATUS_TXOK | CAN_STATUS_RXOK)) != 0))
        {
            //
            // Set a flag to indicate some errors may have occurred.
            //
            errorFlag = 1;
        }
    }

    //
    // Check if the cause is the transmit message object 1
    //
    else if(status == TX_MSG_OBJ_ID)
    {
        //
        // Getting to this point means that the TX interrupt occurred on
        // message object 1, and the message TX is complete.  Clear the
        // message object interrupt.
        //
        CAN_clearInterruptStatus(CANA_BASE, TX_MSG_OBJ_ID);

        //
        // Increment a counter to keep track of how many messages have been
        // sent.  In a real application this could be used to set flags to
        // indicate when a message is sent.
        //
        txMsgCount++;

        //
        // Since the message was sent, clear any error flags.
        //
        errorFlag = 0;
    }

    //
    // Check if the cause is the receive message object 2
    //
    else if(status == RX_MSG_OBJ_ID)
    {
        //
        // Get the received message
        //
        CAN_readMessage(CANA_BASE, RX_MSG_OBJ_ID, rxMsgData);

        //
        // Getting to this point means that the RX interrupt occurred on
        // message object 2, and the message RX is complete.  Clear the
        // message object interrupt.
        //
        CAN_clearInterruptStatus(CANA_BASE, RX_MSG_OBJ_ID);

        //
        // Increment a counter to keep track of how many messages have been
        // received. In a real application this could be used to set flags to
        // indicate when a message is received.
        //
        rxMsgCount++;

        //
        // Since the message was received, clear any error flags.
        //
        errorFlag = 0;
        if((txMsgData[0] != rxMsgData[0]) ||
           (txMsgData[1] != rxMsgData[1]) ||
		   (txMsgData[2] != rxMsgData[2]) ||
		   (txMsgData[3] != rxMsgData[3]))
        {
        	// fail
        	while(1);
        }
        else
        {
        	// Case Success;
        	while(1);
        }

    }

    //
    // If something unexpected caused the interrupt, this would handle it.
    //
    else
    {
        //
        // Spurious interrupt handling can go here.
        //
    }

    //
    // Clear the global interrupt flag for the CAN interrupt line
    //
    CAN_clearGlobalInterruptStatus(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

    //
    // Acknowledge this interrupt located in group 9
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}

//
// End of File
//

