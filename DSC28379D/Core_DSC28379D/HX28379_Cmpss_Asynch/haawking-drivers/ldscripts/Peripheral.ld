/*#############################################################################*/
/*                                                                             */
/* $Copyright:                                                                 */
/* Copyright (C) 2019-2024 Beijing Haawking Technology Co.,Ltd               */
/* http://www.haawking.com/ All rights reserved.                               */
/*                                                                             */
/* Redistribution and use in source and binary forms, with or without          */
/* modification, are permitted provided that the following conditions          */
/* are met:                                                                    */
/*                                                                             */
/*   Redistributions of source code must retain the above copyright            */
/*   notice, this list of conditions and the following disclaimer.             */
/*                                                                             */
/*   Redistributions in binary form must reproduce the above copyright         */
/*   notice, this list of conditions and the following disclaimer in the       */
/*   documentation and/or other materials provided with the                    */
/*   distribution.                                                             */
/*                                                                             */
/*   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of  */
/*   its contributors may be used to endorse or promote products derived       */
/*   from this software without specific prior written permission.             */
/*                                                                             */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS         */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT           */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR       */
/* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT            */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY       */
/* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT         */
/* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE       */
/* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.        */
/*                                                                             */
/*#############################################################################*/
/*                                                                             */
/* Release for HXS320F28379D, Peripheral Linker, 1.0.0                                    */
/*                                                                             */
/* Release time: 2024-03-01 15:58:08.484688                                                         */
/*                                                                             */
/*#############################################################################*/


/*----------------------------------------------------------------------*/
/* Memories                                                             */
/*----------------------------------------------------------------------*/
MEMORY
{
    AdcaRegs_FILE                   (rwx)  : ORIGIN = 0x10008000, LENGTH = 0x120              /* Adca registers */               
    AdcbRegs_FILE                   (rwx)  : ORIGIN = 0x10008200, LENGTH = 0x120              /* Adcb registers */               
    AdccRegs_FILE                   (rwx)  : ORIGIN = 0x10008400, LENGTH = 0x120              /* Adcc registers */               
    AdcdRegs_FILE                   (rwx)  : ORIGIN = 0x10008600, LENGTH = 0x120              /* Adcd registers */               
    AdcaResultRegs_FILE             (rwx)  : ORIGIN = 0x10008140, LENGTH = 0x50               /* AdcaResult registers */         
    AdcbResultRegs_FILE             (rwx)  : ORIGIN = 0x10008340, LENGTH = 0x50               /* AdcbResult registers */         
    AdccResultRegs_FILE             (rwx)  : ORIGIN = 0x10008540, LENGTH = 0x50               /* AdccResult registers */         
    AdcdResultRegs_FILE             (rwx)  : ORIGIN = 0x10008740, LENGTH = 0x50               /* AdcdResult registers */    
    AdcaCtl3Regs_FILE               (rwx)  : ORIGIN = 0x10008190, LENGTH = 0x4                /* AdcaCtl3Regs registers */
    AdcbCtl3Regs_FILE               (rwx)  : ORIGIN = 0x10008390, LENGTH = 0x4                /* AdcbCtl3Regs registers */         
    AdccCtl3Regs_FILE               (rwx)  : ORIGIN = 0x10008590, LENGTH = 0x4                /* AdccCtl3Regs registers */
    AdcdCtl3Regs_FILE               (rwx)  : ORIGIN = 0x10008790, LENGTH = 0x4                /* AdcdCtl3Regs registers */              
    AnalogSubsysRegs_FILE           (rwx)  : ORIGIN = 0x10214800, LENGTH = 0x2C               /* AnalogSubsys registers */       
    CanaRegs_FILE                   (rwx)  : ORIGIN = 0x10000000, LENGTH = 0x228              /* Cana registers */               
    CanbRegs_FILE                   (rwx)  : ORIGIN = 0x10001000, LENGTH = 0x228              /* Canb registers */               
    Cla1Regs_FILE                   (rwx)  : ORIGIN = 0x00416040, LENGTH = 0x88               /* Cla1 registers */     
    Cla1OnlyRegs_FILE               (rwx)  : ORIGIN = 0x00416000, LENGTH = 0x40               /* Cla1Only registers */              
    ClaSoftintRegs_FILE             (rwx)  : ORIGIN = 0x00416010, LENGTH = 0x8                /* ClaSoftint registers */         
    Clb1LogicCfgRegs_FILE           (rwx)  : ORIGIN = 0x1021A000, LENGTH = 0xAC               /* Clb1LogicCfg registers */       
    Clb1LogicCtrlRegs_FILE          (rwx)  : ORIGIN = 0x1021A100, LENGTH = 0x80               /* Clb1LogicCtrl registers */      
    Clb1DataExchRegs_FILE           (rwx)  : ORIGIN = 0x1021A180, LENGTH = 0x50               /* Clb1DataExch registers */       
    Clb2LogicCfgRegs_FILE           (rwx)  : ORIGIN = 0x1021A400, LENGTH = 0xAC               /* Clb2LogicCfg registers */       
    Clb2LogicCtrlRegs_FILE          (rwx)  : ORIGIN = 0x1021A500, LENGTH = 0x80               /* Clb2LogicCtrl registers */      
    Clb2DataExchRegs_FILE           (rwx)  : ORIGIN = 0x1021A580, LENGTH = 0x50               /* Clb2DataExch registers */       
    Clb3LogicCfgRegs_FILE           (rwx)  : ORIGIN = 0x1021A800, LENGTH = 0xAC               /* Clb3LogicCfg registers */       
    Clb3LogicCtrlRegs_FILE          (rwx)  : ORIGIN = 0x1021A900, LENGTH = 0x80               /* Clb3LogicCtrl registers */      
    Clb3DataExchRegs_FILE           (rwx)  : ORIGIN = 0x1021A980, LENGTH = 0x50               /* Clb3DataExch registers */       
    Clb4LogicCfgRegs_FILE           (rwx)  : ORIGIN = 0x1021AC00, LENGTH = 0xAC               /* Clb4LogicCfg registers */       
    Clb4LogicCtrlRegs_FILE          (rwx)  : ORIGIN = 0x1021AD00, LENGTH = 0x80               /* Clb4LogicCtrl registers */      
    Clb4DataExchRegs_FILE           (rwx)  : ORIGIN = 0x1021AD80, LENGTH = 0x50               /* Clb4DataExch registers */       
    ClbXbarRegs_FILE                (rwx)  : ORIGIN = 0x10211500, LENGTH = 0x78               /* ClbXbar registers */            
    Cmpss1Regs_FILE                 (rwx)  : ORIGIN = 0x10202C00, LENGTH = 0x58               /* Cmpss1 registers */             
    Cmpss2Regs_FILE                 (rwx)  : ORIGIN = 0x10202C80, LENGTH = 0x58               /* Cmpss2 registers */             
    Cmpss3Regs_FILE                 (rwx)  : ORIGIN = 0x10202D00, LENGTH = 0x58               /* Cmpss3 registers */             
    Cmpss4Regs_FILE                 (rwx)  : ORIGIN = 0x10202D80, LENGTH = 0x58               /* Cmpss4 registers */             
    Cmpss5Regs_FILE                 (rwx)  : ORIGIN = 0x10202E00, LENGTH = 0x58               /* Cmpss5 registers */             
    Cmpss6Regs_FILE                 (rwx)  : ORIGIN = 0x10202E80, LENGTH = 0x58               /* Cmpss6 registers */             
    Cmpss7Regs_FILE                 (rwx)  : ORIGIN = 0x10202F00, LENGTH = 0x58               /* Cmpss7 registers */             
    Cmpss8Regs_FILE                 (rwx)  : ORIGIN = 0x10202F80, LENGTH = 0x58               /* Cmpss8 registers */             
    CpuTimer0Regs_FILE              (rwx)  : ORIGIN = 0x00101000, LENGTH = 0x10               /* CpuTimer0 registers */          
    CpuTimer1Regs_FILE              (rwx)  : ORIGIN = 0x00101010, LENGTH = 0x10               /* CpuTimer1 registers */          
    CpuTimer2Regs_FILE              (rwx)  : ORIGIN = 0x00101020, LENGTH = 0x10               /* CpuTimer2 registers */          
    DacaRegs_FILE                   (rwx)  : ORIGIN = 0x10202400, LENGTH = 0x1C               /* Daca registers */               
    DacbRegs_FILE                   (rwx)  : ORIGIN = 0x10202480, LENGTH = 0x1C               /* Dacb registers */               
    DaccRegs_FILE                   (rwx)  : ORIGIN = 0x10202500, LENGTH = 0x1C               /* Dacc registers */               
    DcsmZ1Regs_FILE                 (rwx)  : ORIGIN = 0x00218000, LENGTH = 0x50               /* DcsmZ1 registers */             
    DcsmZ2Regs_FILE                 (rwx)  : ORIGIN = 0x00218050, LENGTH = 0x50               /* DcsmZ2 registers */             
    DcsmCommonRegs_FILE             (rwx)  : ORIGIN = 0x002180A0, LENGTH = 0x20               /* DcsmCommon registers */         
    DmaRegs_FILE                    (rwx)  : ORIGIN = 0x01020000, LENGTH = 0x380              /* Dma registers */                
    ECap1Regs_FILE                  (rwx)  : ORIGIN = 0x10202000, LENGTH = 0x40               /* ECap1 registers */              
    ECap2Regs_FILE                  (rwx)  : ORIGIN = 0x10202080, LENGTH = 0x40               /* ECap2 registers */              
    ECap3Regs_FILE                  (rwx)  : ORIGIN = 0x10202100, LENGTH = 0x40               /* ECap3 registers */              
    ECap4Regs_FILE                  (rwx)  : ORIGIN = 0x10202180, LENGTH = 0x40               /* ECap4 registers */              
    ECap5Regs_FILE                  (rwx)  : ORIGIN = 0x10202200, LENGTH = 0x40               /* ECap5 registers */              
    ECap6Regs_FILE                  (rwx)  : ORIGIN = 0x10202280, LENGTH = 0x40               /* ECap6 registers */                           
    Emif1Regs_FILE                  (rwx)  : ORIGIN = 0x40800000, LENGTH = 0x3C               /* Emif1 registers */              
    Emif1ConfigRegs_FILE            (rwx)  : ORIGIN = 0x40800800, LENGTH = 0x10               /* Emif1Config registers */        
    Emif2Regs_FILE                  (rwx)  : ORIGIN = 0x55000000, LENGTH = 0x3C               /* Emif2 registers */              
    Emif2ConfigRegs_FILE            (rwx)  : ORIGIN = 0x55000800, LENGTH = 0x10               /* Emif2Config registers */        
    EPwm1Regs_FILE                  (rwx)  : ORIGIN = 0x10224000, LENGTH = 0x3FC              /* EPwm1 registers */              
    EPwm2Regs_FILE                  (rwx)  : ORIGIN = 0x10224400, LENGTH = 0x3FC              /* EPwm2 registers */              
    EPwm3Regs_FILE                  (rwx)  : ORIGIN = 0x10224800, LENGTH = 0x3FC              /* EPwm3 registers */              
    EPwm4Regs_FILE                  (rwx)  : ORIGIN = 0x10224C00, LENGTH = 0x3FC              /* EPwm4 registers */              
    EPwm5Regs_FILE                  (rwx)  : ORIGIN = 0x10225000, LENGTH = 0x3FC              /* EPwm5 registers */              
    EPwm6Regs_FILE                  (rwx)  : ORIGIN = 0x10225400, LENGTH = 0x3FC              /* EPwm6 registers */              
    EPwm7Regs_FILE                  (rwx)  : ORIGIN = 0x10225800, LENGTH = 0x3FC              /* EPwm7 registers */              
    EPwm8Regs_FILE                  (rwx)  : ORIGIN = 0x10225C00, LENGTH = 0x3FC              /* EPwm8 registers */              
    EPwm9Regs_FILE                  (rwx)  : ORIGIN = 0x10226000, LENGTH = 0x3FC              /* EPwm9 registers */              
    EPwm10Regs_FILE                 (rwx)  : ORIGIN = 0x10226400, LENGTH = 0x3FC              /* EPwm10 registers */             
    EPwm11Regs_FILE                 (rwx)  : ORIGIN = 0x10226800, LENGTH = 0x3FC              /* EPwm11 registers */             
    EPwm12Regs_FILE                 (rwx)  : ORIGIN = 0x10226C00, LENGTH = 0x3FC              /* EPwm12 registers */             
    EPwmXbarRegs_FILE               (rwx)  : ORIGIN = 0x10211400, LENGTH = 0x80               /* EPwmXbar registers */           
    EQep1Regs_FILE                  (rwx)  : ORIGIN = 0x10202600, LENGTH = 0x70               /* EQep1 registers */              
    EQep2Regs_FILE                  (rwx)  : ORIGIN = 0x10202680, LENGTH = 0x70               /* EQep2 registers */              
    EQep3Regs_FILE                  (rwx)  : ORIGIN = 0x10202700, LENGTH = 0x70               /* EQep3 registers */              
    Flash0CtrlRegs_FILE             (rwx)  : ORIGIN = 0x00708000, LENGTH = 0x140              /* Flash0Ctrl registers */         
    Flash0EccRegs_FILE              (rwx)  : ORIGIN = 0x00708200, LENGTH = 0x84               /* Flash0Ecc registers */          
    GpioCtrlRegs_FILE               (rwx)  : ORIGIN = 0x1021C000, LENGTH = 0x408              /* GpioCtrl registers */           
    GpioDataRegs_FILE               (rwx)  : ORIGIN = 0x1021C400, LENGTH = 0x74               /* GpioData registers */           
    I2caRegs_FILE                   (rwx)  : ORIGIN = 0x10215C00, LENGTH = 0x3C               /* I2ca registers */               
    I2cbRegs_FILE                   (rwx)  : ORIGIN = 0x10216000, LENGTH = 0x3C               /* I2cb registers */               
    InputXbarRegs_FILE              (rwx)  : ORIGIN = 0x10211000, LENGTH = 0x7C               /* InputXbar registers */          
    IpcRegs_FILE                    (rwx)  : ORIGIN = 0x01022000, LENGTH = 0x44               /* Ipc registers */   
    McbspaRegs_FILE                 (rwx)  : ORIGIN = 0x10202A00, LENGTH = 0x90               /* Mcbspa registers */             
    McbspbRegs_FILE                 (rwx)  : ORIGIN = 0x10202B00, LENGTH = 0x90               /* Mcbspb registers */             
    MemCfgRegs_FILE                 (rwx)  : ORIGIN = 0x00100C00, LENGTH = 0x70               /* MemCfg registers */             
    MemoryErrorRegs_FILE            (rwx)  : ORIGIN = 0x00100C70, LENGTH = 0x50               /* MemoryError registers */   
    AccessProtectionRegs_FILE       (rwx)  : ORIGIN = 0x00100CD4  ,LENGTH = 0x50               /* AccessProtection registers */ 
    RomWaitStateRegs_FILE           (rwx)  : ORIGIN = 0x00100CFC, LENGTH = 0x4                /* RomWaitState registers */       
    RomPrefetchRegs_FILE            (rwx)  : ORIGIN = 0x00100CF8, LENGTH = 0x4                /* RomPrefetch registers */               
    NmiIntruptRegs_FILE             (rwx)  : ORIGIN = 0x00101300, LENGTH = 0x30               /* NmiIntrupt registers */         
    OutputXbarRegs_FILE             (rwx)  : ORIGIN = 0x10211600, LENGTH = 0x80               /* OutputXbar registers */         
    PieCtrlRegs_FILE                (rwx)  : ORIGIN = 0x00101100, LENGTH = 0x68               /* PieCtrl registers */            
    PieVectTable_FILE               (rwx)  : ORIGIN = 0x00101800, LENGTH = 0x780              /* PieVectT registers */           
    SciaRegs_FILE                   (rwx)  : ORIGIN = 0x10214C00, LENGTH = 0x34               /* Scia registers */               
    ScibRegs_FILE                   (rwx)  : ORIGIN = 0x10215000, LENGTH = 0x34               /* Scib registers */               
    ScicRegs_FILE                   (rwx)  : ORIGIN = 0x10215400, LENGTH = 0x34               /* Scic registers */               
    ScidRegs_FILE                   (rwx)  : ORIGIN = 0x10215800, LENGTH = 0x34               /* Scid registers */               
    Sdfm1Regs_FILE                  (rwx)  : ORIGIN = 0x10201000, LENGTH = 0x154              /* Sdfm1 registers */              
    Sdfm2Regs_FILE                  (rwx)  : ORIGIN = 0x10201200, LENGTH = 0x154              /* Sdfm2 registers */              
    SpiaRegs_FILE                   (rwx)  : ORIGIN = 0x10202800, LENGTH = 0x30               /* Spia registers */               
    SpibRegs_FILE                   (rwx)  : ORIGIN = 0x10202880, LENGTH = 0x30               /* Spib registers */               
    SpicRegs_FILE                   (rwx)  : ORIGIN = 0x10202900, LENGTH = 0x30               /* Spic registers */               
    DmaClaSrcSelRegs_FILE           (rwx)  : ORIGIN = 0x10212D00, LENGTH = 0x34               /* DmaClaSrcSel registers */       
    WdRegs_FILE                     (rwx)  : ORIGIN = 0x10212E00, LENGTH = 0x58               /* Wd registers */       
    OscTrimRegs_FILE                (rwx)  : ORIGIN = 0x10212F00, LENGTH = 0x38               /* OscTrim registers */                       
    ClkCfgRegs_FILE                 (rwx)  : ORIGIN = 0x10212000, LENGTH = 0x100               /* ClkCfg registers */               
    CpuSysRegs_FILE                 (rwx)  : ORIGIN = 0x10212200, LENGTH = 0x140              /* CpuSys registers */             
    SyncSocRegs_FILE                (rwx)  : ORIGIN = 0x10212C00, LENGTH = 0xC                /* SyncSoc registers */            
    DevCfgRegs_FILE                 (rwx)  : ORIGIN = 0x10212400, LENGTH = 0x340              /* DevCfg registers */             
    UppRegs_FILE                    (rwx)  : ORIGIN = 0x10220000, LENGTH = 0x68               /* Upp registers */                
    XbarRegs_FILE                   (rwx)  : ORIGIN = 0x10211080, LENGTH = 0x20               /* Xbar registers */               
    XintRegs_FILE                   (rwx)  : ORIGIN = 0x00101200, LENGTH = 0x2C               /* Xint registers */               
}

/*----------------------------------------------------------------------*/
/* Sections                                                             */
/*----------------------------------------------------------------------*/
SECTIONS
{
    .AdcaRegs(NOLOAD)              : {*(.AdcaRegs)}              > AdcaRegs_FILE          
    .AdcbRegs(NOLOAD)              : {*(.AdcbRegs)}              > AdcbRegs_FILE          
    .AdccRegs(NOLOAD)              : {*(.AdccRegs)}              > AdccRegs_FILE          
    .AdcdRegs(NOLOAD)              : {*(.AdcdRegs)}              > AdcdRegs_FILE          
    .AdcaResultRegs(NOLOAD)        : {*(.AdcaResultRegs)}        > AdcaResultRegs_FILE    
    .AdcbResultRegs(NOLOAD)        : {*(.AdcbResultRegs)}        > AdcbResultRegs_FILE    
    .AdccResultRegs(NOLOAD)        : {*(.AdccResultRegs)}        > AdccResultRegs_FILE    
    .AdcdResultRegs(NOLOAD)        : {*(.AdcdResultRegs)}        > AdcdResultRegs_FILE    
    .AnalogSubsysRegs(NOLOAD)      : {*(.AnalogSubsysRegs)}      > AnalogSubsysRegs_FILE  
    .CanaRegs(NOLOAD)              : {*(.CanaRegs)}              > CanaRegs_FILE          
    .CanbRegs(NOLOAD)              : {*(.CanbRegs)}              > CanbRegs_FILE          
    .Cla1Regs(NOLOAD)              : {*(.Cla1Regs)}              > Cla1Regs_FILE          
    .Cla1OnlyRegs(NOLOAD)          : {*(.Cla1OnlyRegs)}          > Cla1OnlyRegs_FILE  
    .ClaSoftintRegs(NOLOAD)        : {*(.ClaSoftintRegs)}        > ClaSoftintRegs_FILE 
    .Clb1LogicCfgRegs(NOLOAD)      : {*(.Clb1LogicCfgRegs)}      > Clb1LogicCfgRegs_FILE  
    .Clb1LogicCtrlRegs(NOLOAD)     : {*(.Clb1LogicCtrlRegs)}     > Clb1LogicCtrlRegs_FILE 
    .Clb1DataExchRegs(NOLOAD)      : {*(.Clb1DataExchRegs)}      > Clb1DataExchRegs_FILE  
    .Clb2LogicCfgRegs(NOLOAD)      : {*(.Clb2LogicCfgRegs)}      > Clb2LogicCfgRegs_FILE  
    .Clb2LogicCtrlRegs(NOLOAD)     : {*(.Clb2LogicCtrlRegs)}     > Clb2LogicCtrlRegs_FILE 
    .Clb2DataExchRegs(NOLOAD)      : {*(.Clb2DataExchRegs)}      > Clb2DataExchRegs_FILE  
    .Clb3LogicCfgRegs(NOLOAD)      : {*(.Clb3LogicCfgRegs)}      > Clb3LogicCfgRegs_FILE  
    .Clb3LogicCtrlRegs(NOLOAD)     : {*(.Clb3LogicCtrlRegs)}     > Clb3LogicCtrlRegs_FILE 
    .Clb3DataExchRegs(NOLOAD)      : {*(.Clb3DataExchRegs)}      > Clb3DataExchRegs_FILE  
    .Clb4LogicCfgRegs(NOLOAD)      : {*(.Clb4LogicCfgRegs)}      > Clb4LogicCfgRegs_FILE  
    .Clb4LogicCtrlRegs(NOLOAD)     : {*(.Clb4LogicCtrlRegs)}     > Clb4LogicCtrlRegs_FILE 
    .Clb4DataExchRegs(NOLOAD)      : {*(.Clb4DataExchRegs)}      > Clb4DataExchRegs_FILE  
    .ClbXbarRegs(NOLOAD)           : {*(.ClbXbarRegs)}           > ClbXbarRegs_FILE       
    .Cmpss1Regs(NOLOAD)            : {*(.Cmpss1Regs)}            > Cmpss1Regs_FILE        
    .Cmpss2Regs(NOLOAD)            : {*(.Cmpss2Regs)}            > Cmpss2Regs_FILE        
    .Cmpss3Regs(NOLOAD)            : {*(.Cmpss3Regs)}            > Cmpss3Regs_FILE        
    .Cmpss4Regs(NOLOAD)            : {*(.Cmpss4Regs)}            > Cmpss4Regs_FILE        
    .Cmpss5Regs(NOLOAD)            : {*(.Cmpss5Regs)}            > Cmpss5Regs_FILE        
    .Cmpss6Regs(NOLOAD)            : {*(.Cmpss6Regs)}            > Cmpss6Regs_FILE        
    .Cmpss7Regs(NOLOAD)            : {*(.Cmpss7Regs)}            > Cmpss7Regs_FILE        
    .Cmpss8Regs(NOLOAD)            : {*(.Cmpss8Regs)}            > Cmpss8Regs_FILE        
    .CpuTimer0Regs(NOLOAD)         : {*(.CpuTimer0Regs)}         > CpuTimer0Regs_FILE     
    .CpuTimer1Regs(NOLOAD)         : {*(.CpuTimer1Regs)}         > CpuTimer1Regs_FILE     
    .CpuTimer2Regs(NOLOAD)         : {*(.CpuTimer2Regs)}         > CpuTimer2Regs_FILE     
    .DacaRegs(NOLOAD)              : {*(.DacaRegs)}              > DacaRegs_FILE          
    .DacbRegs(NOLOAD)              : {*(.DacbRegs)}              > DacbRegs_FILE          
    .DaccRegs(NOLOAD)              : {*(.DaccRegs)}              > DaccRegs_FILE          
    .DcsmZ1Regs(NOLOAD)            : {*(.DcsmZ1Regs)}            > DcsmZ1Regs_FILE        
    .DcsmZ2Regs(NOLOAD)            : {*(.DcsmZ2Regs)}            > DcsmZ2Regs_FILE        
    .DcsmCommonRegs(NOLOAD)        : {*(.DcsmCommonRegs)}        > DcsmCommonRegs_FILE    
    .DmaRegs(NOLOAD)               : {*(.DmaRegs)}               > DmaRegs_FILE           
    .ECap1Regs(NOLOAD)             : {*(.ECap1Regs)}             > ECap1Regs_FILE         
    .ECap2Regs(NOLOAD)             : {*(.ECap2Regs)}             > ECap2Regs_FILE         
    .ECap3Regs(NOLOAD)             : {*(.ECap3Regs)}             > ECap3Regs_FILE         
    .ECap4Regs(NOLOAD)             : {*(.ECap4Regs)}             > ECap4Regs_FILE         
    .ECap5Regs(NOLOAD)             : {*(.ECap5Regs)}             > ECap5Regs_FILE         
    .ECap6Regs(NOLOAD)             : {*(.ECap6Regs)}             > ECap6Regs_FILE         
    .Emif1Regs(NOLOAD)             : {*(.Emif1Regs)}             > Emif1Regs_FILE         
    .Emif1ConfigRegs(NOLOAD)       : {*(.Emif1ConfigRegs)}       > Emif1ConfigRegs_FILE   
    .Emif2Regs(NOLOAD)             : {*(.Emif2Regs)}             > Emif2Regs_FILE         
    .Emif2ConfigRegs(NOLOAD)       : {*(.Emif2ConfigRegs)}       > Emif2ConfigRegs_FILE   
    .EPwm1Regs(NOLOAD)             : {*(.EPwm1Regs)}             > EPwm1Regs_FILE         
    .EPwm2Regs(NOLOAD)             : {*(.EPwm2Regs)}             > EPwm2Regs_FILE         
    .EPwm3Regs(NOLOAD)             : {*(.EPwm3Regs)}             > EPwm3Regs_FILE         
    .EPwm4Regs(NOLOAD)             : {*(.EPwm4Regs)}             > EPwm4Regs_FILE         
    .EPwm5Regs(NOLOAD)             : {*(.EPwm5Regs)}             > EPwm5Regs_FILE         
    .EPwm6Regs(NOLOAD)             : {*(.EPwm6Regs)}             > EPwm6Regs_FILE         
    .EPwm7Regs(NOLOAD)             : {*(.EPwm7Regs)}             > EPwm7Regs_FILE         
    .EPwm8Regs(NOLOAD)             : {*(.EPwm8Regs)}             > EPwm8Regs_FILE         
    .EPwm9Regs(NOLOAD)             : {*(.EPwm9Regs)}             > EPwm9Regs_FILE         
    .EPwm10Regs(NOLOAD)            : {*(.EPwm10Regs)}            > EPwm10Regs_FILE        
    .EPwm11Regs(NOLOAD)            : {*(.EPwm11Regs)}            > EPwm11Regs_FILE        
    .EPwm12Regs(NOLOAD)            : {*(.EPwm12Regs)}            > EPwm12Regs_FILE        
    .EPwmXbarRegs(NOLOAD)          : {*(.EPwmXbarRegs)}          > EPwmXbarRegs_FILE      
    .EQep1Regs(NOLOAD)             : {*(.EQep1Regs)}             > EQep1Regs_FILE         
    .EQep2Regs(NOLOAD)             : {*(.EQep2Regs)}             > EQep2Regs_FILE         
    .EQep3Regs(NOLOAD)             : {*(.EQep3Regs)}             > EQep3Regs_FILE         
    .Flash0CtrlRegs(NOLOAD)        : {*(.Flash0CtrlRegs)}        > Flash0CtrlRegs_FILE    
    .Flash0EccRegs(NOLOAD)         : {*(.Flash0EccRegs)}         > Flash0EccRegs_FILE     
    .GpioCtrlRegs(NOLOAD)          : {*(.GpioCtrlRegs)}          > GpioCtrlRegs_FILE      
    .GpioDataRegs(NOLOAD)          : {*(.GpioDataRegs)}          > GpioDataRegs_FILE      
    .I2caRegs(NOLOAD)              : {*(.I2caRegs)}              > I2caRegs_FILE          
    .I2cbRegs(NOLOAD)              : {*(.I2cbRegs)}              > I2cbRegs_FILE          
    .InputXbarRegs(NOLOAD)         : {*(.InputXbarRegs)}         > InputXbarRegs_FILE     
    .IpcRegs(NOLOAD)               : {*(.IpcRegs)}               > IpcRegs_FILE      
    .McbspaRegs(NOLOAD)            : {*(.McbspaRegs)}            > McbspaRegs_FILE        
    .McbspbRegs(NOLOAD)            : {*(.McbspbRegs)}            > McbspbRegs_FILE        
    .MemCfgRegs(NOLOAD)            : {*(.MemCfgRegs)}            > MemCfgRegs_FILE        
    .MemoryErrorRegs(NOLOAD)       : {*(.MemoryErrorRegs)}       > MemoryErrorRegs_FILE   
    .RomWaitStateRegs(NOLOAD)      : {*(.RomWaitStateRegs)}      > RomWaitStateRegs_FILE  
    .RomPrefetchRegs(NOLOAD)       : {*(.RomPrefetchRegs)}       > RomPrefetchRegs_FILE   
    .AccessProtectionRegs(NOLOAD)  : {*(.AccessProtectionRegs)}  > AccessProtectionRegs_FILE
    .NmiIntruptRegs(NOLOAD)        : {*(.NmiIntruptRegs)}        > NmiIntruptRegs_FILE    
    .OutputXbarRegs(NOLOAD)        : {*(.OutputXbarRegs)}        > OutputXbarRegs_FILE    
    .PieCtrlRegs(NOLOAD)           : {*(.PieCtrlRegs)}           > PieCtrlRegs_FILE       
    .PieVectTable(NOLOAD)          : {*(.PieVectTable)}          > PieVectTable_FILE      
    .SciaRegs(NOLOAD)              : {*(.SciaRegs)}              > SciaRegs_FILE          
    .ScibRegs(NOLOAD)              : {*(.ScibRegs)}              > ScibRegs_FILE          
    .ScicRegs(NOLOAD)              : {*(.ScicRegs)}              > ScicRegs_FILE          
    .ScidRegs(NOLOAD)              : {*(.ScidRegs)}              > ScidRegs_FILE          
    .Sdfm1Regs(NOLOAD)             : {*(.Sdfm1Regs)}             > Sdfm1Regs_FILE         
    .Sdfm2Regs(NOLOAD)             : {*(.Sdfm2Regs)}             > Sdfm2Regs_FILE         
    .SpiaRegs(NOLOAD)              : {*(.SpiaRegs)}              > SpiaRegs_FILE          
    .SpibRegs(NOLOAD)              : {*(.SpibRegs)}              > SpibRegs_FILE          
    .SpicRegs(NOLOAD)              : {*(.SpicRegs)}              > SpicRegs_FILE          
    .DmaClaSrcSelRegs(NOLOAD)      : {*(.DmaClaSrcSelRegs)}      > DmaClaSrcSelRegs_FILE  
    .WdRegs(NOLOAD)                : {*(.WdRegs)}                > WdRegs_FILE            
    .OscTrimRegs(NOLOAD)           : {*(.OscTrimRegs)}           > OscTrimRegs_FILE       
    .ClkCfgRegs(NOLOAD)            : {*(.ClkCfgRegs)}            > ClkCfgRegs_FILE        
    .CpuSysRegs(NOLOAD)            : {*(.CpuSysRegs)}            > CpuSysRegs_FILE        
    .SyncSocRegs(NOLOAD)           : {*(.SyncSocRegs)}           > SyncSocRegs_FILE       
    .DevCfgRegs(NOLOAD)            : {*(.DevCfgRegs)}            > DevCfgRegs_FILE        
    .UppRegs(NOLOAD)               : {*(.UppRegs)}               > UppRegs_FILE           
    .XbarRegs(NOLOAD)              : {*(.XbarRegs)}              > XbarRegs_FILE          
    .XintRegs(NOLOAD)              : {*(.XintRegs)}              > XintRegs_FILE          
}
