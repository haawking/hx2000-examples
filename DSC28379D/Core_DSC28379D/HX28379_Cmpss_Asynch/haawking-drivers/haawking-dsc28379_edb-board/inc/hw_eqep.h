//###########################################################################
//
// FILE:    hw_eqep.h
//
// TITLE:   Definitions for the EQEP registers.
//
//###########################################################################
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F28379D, Hal DriverLib, 1.0.0
//
// Release time: 2023-08-30 03:07:28.402989
//
//#############################################################################


#ifndef HW_EQEP_H
#define HW_EQEP_H

//*************************************************************************************************
//
// The following are defines for the EQEP register offsets
//
//*************************************************************************************************
#define EQEP_O_QPOSCNT    0x0U    // Position Counter
#define EQEP_O_QPOSINIT   0x4U    // Position Counter Init
#define EQEP_O_QPOSMAX    0x8U    // Maximum Position Count
#define EQEP_O_QPOSCMP    0xCU    // Position Compare
#define EQEP_O_QPOSILAT   0x10U   // Index Position Latch
#define EQEP_O_QPOSSLAT   0x14U   // Strobe Position Latch
#define EQEP_O_QPOSLAT    0x18U   // Position Latch
#define EQEP_O_QUTMR      0x1CU   // QEP Unit Timer
#define EQEP_O_QUPRD      0x20U   // QEP Unit Period
#define EQEP_O_QWDTMR     0x24U   // QEP Watchdog Timer
#define EQEP_O_QWDPRD     0x28U   // QEP Watchdog Period
#define EQEP_O_QDECCTL    0x2CU   // Quadrature Decoder Control
#define EQEP_O_QEPCTL     0x30U   // QEP Control
#define EQEP_O_QCAPCTL    0x34U   // Qaudrature Capture Control
#define EQEP_O_QPOSCTL    0x38U   // Position Compare Control
#define EQEP_O_QEINT      0x3CU   // QEP Interrupt Control
#define EQEP_O_QFLG       0x40U   // QEP Interrupt Flag
#define EQEP_O_QCLR       0x44U   // QEP Interrupt Clear
#define EQEP_O_QFRC       0x48U   // QEP Interrupt Force
#define EQEP_O_QEPSTS     0x4CU   // QEP Status
#define EQEP_O_QCTMR      0x50U   // QEP Capture Timer
#define EQEP_O_QCPRD      0x54U   // QEP Capture Period
#define EQEP_O_QCTMRLAT   0x58U   // QEP Capture Latch
#define EQEP_O_QCPRDLAT   0x5CU   // QEP Capture Period Latch


//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSCNT register
//
//*************************************************************************************************
#define EQEP_QPOSCNT_QPOSCNT_S   0U
#define EQEP_QPOSCNT_QPOSCNT_M   0xFFFFFFFFU   // Position counter

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSINIT register
//
//*************************************************************************************************
#define EQEP_QPOSINIT_QPOSINIT_S   0U
#define EQEP_QPOSINIT_QPOSINIT_M   0xFFFFFFFFU   // Position Counter Init

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSMAX register
//
//*************************************************************************************************
#define EQEP_QPOSMAX_QPOSMAX_S   0U
#define EQEP_QPOSMAX_QPOSMAX_M   0xFFFFFFFFU   // Maximum Position Count

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSCMP register
//
//*************************************************************************************************
#define EQEP_QPOSCMP_QPOSCMP_S   0U
#define EQEP_QPOSCMP_QPOSCMP_M   0xFFFFFFFFU   // Position Compare

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSILAT register
//
//*************************************************************************************************
#define EQEP_QPOSILAT_QPOSILAT_S   0U
#define EQEP_QPOSILAT_QPOSILAT_M   0xFFFFFFFFU   // Index Position Latch

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSSLAT register
//
//*************************************************************************************************
#define EQEP_QPOSSLAT_QPOSSLAT_S   0U
#define EQEP_QPOSSLAT_QPOSSLAT_M   0xFFFFFFFFU   // Index Position Latch

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSLAT register
//
//*************************************************************************************************
#define EQEP_QPOSLAT_QPOSLAT_S   0U
#define EQEP_QPOSLAT_QPOSLAT_M   0xFFFFFFFFU   // Position Latch

//*************************************************************************************************
//
// The following are defines for the bit fields in the QUTMR register
//
//*************************************************************************************************
#define EQEP_QUTMR_QUTMR_S   0U
#define EQEP_QUTMR_QUTMR_M   0xFFFFFFFFU   // QEP Unit Timer

//*************************************************************************************************
//
// The following are defines for the bit fields in the QUPRD register
//
//*************************************************************************************************
#define EQEP_QUPRD_QUPRD_S   0U
#define EQEP_QUPRD_QUPRD_M   0xFFFFFFFFU   // QEP Unit Period

//*************************************************************************************************
//
// The following are defines for the bit fields in the QWDTMR register
//
//*************************************************************************************************
#define EQEP_QWDTMR_QWDTMR_S   0U
#define EQEP_QWDTMR_QWDTMR_M   0xFFFFU   // QEP Watchdog Timer

//*************************************************************************************************
//
// The following are defines for the bit fields in the QWDPRD register
//
//*************************************************************************************************
#define EQEP_QWDPRD_QWDPRD_S   0U
#define EQEP_QWDPRD_QWDPRD_M   0xFFFFU   // QEP Watchdog Period

//*************************************************************************************************
//
// The following are defines for the bit fields in the QDECCTL register
//
//*************************************************************************************************
#define EQEP_QDECCTL_QSP      0x20U     // QEPS input polarity
#define EQEP_QDECCTL_QIP      0x40U     // QEPI input polarity
#define EQEP_QDECCTL_QBP      0x80U     // QEPB input polarity
#define EQEP_QDECCTL_QAP      0x100U    // QEPA input polarity
#define EQEP_QDECCTL_IGATE    0x200U    // Index pulse gating option
#define EQEP_QDECCTL_SWAP     0x400U    // CLK/DIR Signal Source for Position Counter
#define EQEP_QDECCTL_XCR      0x800U    // External Clock Rate
#define EQEP_QDECCTL_SPSEL    0x1000U   // Sync output pin selection
#define EQEP_QDECCTL_SOEN     0x2000U   // Sync output-enable
#define EQEP_QDECCTL_QSRC_S   14U
#define EQEP_QDECCTL_QSRC_M   0xC000U   // Position-counter source selection

//*************************************************************************************************
//
// The following are defines for the bit fields in the QEPCTL register
//
//*************************************************************************************************
#define EQEP_QEPCTL_WDE           0x1U      // QEP watchdog enable
#define EQEP_QEPCTL_UTE           0x2U      // QEP unit timer enable
#define EQEP_QEPCTL_QCLM          0x4U      // QEP capture latch mode
#define EQEP_QEPCTL_QPEN          0x8U      // Quadrature postotion counter enable
#define EQEP_QEPCTL_IEL_S         4U
#define EQEP_QEPCTL_IEL_M         0x30U     // Index event latch
#define EQEP_QEPCTL_SEL           0x40U     // Strobe event latch
#define EQEP_QEPCTL_SWI           0x80U     // Software init position counter
#define EQEP_QEPCTL_IEI_S         8U
#define EQEP_QEPCTL_IEI_M         0x300U    // Index event init of position count
#define EQEP_QEPCTL_SEI_S         10U
#define EQEP_QEPCTL_SEI_M         0xC00U    // Strobe event init
#define EQEP_QEPCTL_PCRM_S        12U
#define EQEP_QEPCTL_PCRM_M        0x3000U   // Postion counter reset
#define EQEP_QEPCTL_FREE_SOFT_S   14U
#define EQEP_QEPCTL_FREE_SOFT_M   0xC000U   // Emulation mode

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCAPCTL register
//
//*************************************************************************************************
#define EQEP_QCAPCTL_UPPS_S   0U
#define EQEP_QCAPCTL_UPPS_M   0xFU      // Unit position event prescaler
#define EQEP_QCAPCTL_CCPS_S   4U
#define EQEP_QCAPCTL_CCPS_M   0x70U     // eQEP capture timer clock prescaler
#define EQEP_QCAPCTL_CEN      0x8000U   // Enable eQEP capture

//*************************************************************************************************
//
// The following are defines for the bit fields in the QPOSCTL register
//
//*************************************************************************************************
#define EQEP_QPOSCTL_PCSPW_S   0U
#define EQEP_QPOSCTL_PCSPW_M   0xFFFU    // Position compare sync pulse width
#define EQEP_QPOSCTL_PCE       0x1000U   // Position compare enable/disable
#define EQEP_QPOSCTL_PCPOL     0x2000U   // Polarity of sync output
#define EQEP_QPOSCTL_PCLOAD    0x4000U   // Position compare of shadow load
#define EQEP_QPOSCTL_PCSHDW    0x8000U   // Position compare of shadow enable

//*************************************************************************************************
//
// The following are defines for the bit fields in the QEINT register
//
//*************************************************************************************************
#define EQEP_QEINT_PCE   0x2U     // Position counter error interrupt enable
#define EQEP_QEINT_QPE   0x4U     // Quadrature phase error interrupt enable
#define EQEP_QEINT_QDC   0x8U     // Quadrature direction change interrupt enable
#define EQEP_QEINT_WTO   0x10U    // Watchdog time out interrupt enable
#define EQEP_QEINT_PCU   0x20U    // Position counter underflow interrupt enable
#define EQEP_QEINT_PCO   0x40U    // Position counter overflow interrupt enable
#define EQEP_QEINT_PCR   0x80U    // Position-compare ready interrupt enable
#define EQEP_QEINT_PCM   0x100U   // Position-compare match interrupt enable
#define EQEP_QEINT_SEL   0x200U   // Strobe event latch interrupt enable
#define EQEP_QEINT_IEL   0x400U   // Index event latch interrupt enable
#define EQEP_QEINT_UTO   0x800U   // Unit time out interrupt enable

//*************************************************************************************************
//
// The following are defines for the bit fields in the QFLG register
//
//*************************************************************************************************
#define EQEP_QFLG_INT   0x1U     // Global interrupt status flag
#define EQEP_QFLG_PCE   0x2U     // Position counter error interrupt flag
#define EQEP_QFLG_PHE   0x4U     // Quadrature phase error interrupt flag
#define EQEP_QFLG_QDC   0x8U     // Quadrature direction change interrupt flag
#define EQEP_QFLG_WTO   0x10U    // Watchdog timeout interrupt flag
#define EQEP_QFLG_PCU   0x20U    // Position counter underflow interrupt flag
#define EQEP_QFLG_PCO   0x40U    // Position counter overflow interrupt flag
#define EQEP_QFLG_PCR   0x80U    // Position-compare ready interrupt flag
#define EQEP_QFLG_PCM   0x100U   // eQEP compare match event interrupt flag
#define EQEP_QFLG_SEL   0x200U   // Strobe event latch interrupt flag
#define EQEP_QFLG_IEL   0x400U   // Index event latch interrupt flag
#define EQEP_QFLG_UTO   0x800U   // Unit time out interrupt flag

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCLR register
//
//*************************************************************************************************
#define EQEP_QCLR_INT   0x1U     // Global interrupt clear flag
#define EQEP_QCLR_PCE   0x2U     // Clear position counter error interrupt flag
#define EQEP_QCLR_PHE   0x4U     // Clear quadrature phase error interrupt flag
#define EQEP_QCLR_QDC   0x8U     // Clear quadrature direction change interrupt flag
#define EQEP_QCLR_WTO   0x10U    // Clear watchdog timeout interrupt flag
#define EQEP_QCLR_PCU   0x20U    // Clear position counter underflow interrupt flag
#define EQEP_QCLR_PCO   0x40U    // Clear position counter overflow interrupt flag
#define EQEP_QCLR_PCR   0x80U    // Clear position-compare ready interrupt flag
#define EQEP_QCLR_PCM   0x100U   // Clear eQEP compare match event interrupt flag
#define EQEP_QCLR_SEL   0x200U   // Clear strobe event latch interrupt flag
#define EQEP_QCLR_IEL   0x400U   // Clear index event latch interrupt flag
#define EQEP_QCLR_UTO   0x800U   // Clear unit time out interrupt flag

//*************************************************************************************************
//
// The following are defines for the bit fields in the QFRC register
//
//*************************************************************************************************
#define EQEP_QFRC_PCE   0x2U     // Force position counter error interrupt
#define EQEP_QFRC_PHE   0x4U     // Force quadrature phase error interrupt
#define EQEP_QFRC_QDC   0x8U     // Force quadrature direction change interrupt
#define EQEP_QFRC_WTO   0x10U    // Force watchdog time out interrupt
#define EQEP_QFRC_PCU   0x20U    // Force position counter underflow interrupt
#define EQEP_QFRC_PCO   0x40U    // Force position counter overflow interrupt
#define EQEP_QFRC_PCR   0x80U    // Force position-compare ready interrupt
#define EQEP_QFRC_PCM   0x100U   // Force position-compare match interrupt
#define EQEP_QFRC_SEL   0x200U   // Force strobe event latch interrupt
#define EQEP_QFRC_IEL   0x400U   // Force index event latch interrupt
#define EQEP_QFRC_UTO   0x800U   // Force unit time out interrupt

//*************************************************************************************************
//
// The following are defines for the bit fields in the QEPSTS register
//
//*************************************************************************************************
#define EQEP_QEPSTS_PCEF     0x1U    // Position counter error flag.
#define EQEP_QEPSTS_FIMF     0x2U    // First index marker flag
#define EQEP_QEPSTS_CDEF     0x4U    // Capture direction error flag
#define EQEP_QEPSTS_COEF     0x8U    // Capture overflow error flag
#define EQEP_QEPSTS_QDLF     0x10U   // eQEP direction latch flag
#define EQEP_QEPSTS_QDF      0x20U   // Quadrature direction flag
#define EQEP_QEPSTS_FIDF     0x40U   // The first index marker
#define EQEP_QEPSTS_UPEVNT   0x80U   // Unit position event flag

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCTMR register
//
//*************************************************************************************************
#define EQEP_QCTMR_QCTMR_S   0U
#define EQEP_QCTMR_QCTMR_M   0xFFFFU   // This register provides time base for edge capture unit

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCPRD register
//
//*************************************************************************************************
#define EQEP_QCPRD_QCPRD_S   0U
#define EQEP_QCPRD_QCPRD_M   0xFFFFU   // This register holds the period count value between the last successive eQEP position events

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCTMRLAT register
//
//*************************************************************************************************
#define EQEP_QCTMRLAT_QCTMRLAT_S   0U
#define EQEP_QCTMRLAT_QCTMRLAT_M   0xFFFFU   // The eQEP capture timer value can be latched into this register on two events viz., unit timeout event, reading the eQEP position counter

//*************************************************************************************************
//
// The following are defines for the bit fields in the QCPRDLAT register
//
//*************************************************************************************************
#define EQEP_QCPRDLAT_QCPRDLAT_S   0U
#define EQEP_QCPRDLAT_QCPRDLAT_M   0xFFFFU   // eQEP capture period value can be latched into this register on two events viz., unit timeout event, reading the eQEP position counter.




#endif
