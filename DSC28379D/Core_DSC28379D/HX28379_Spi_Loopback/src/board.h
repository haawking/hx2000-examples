/*
 * board.h
 *
 *  Created on: 2024��1��4��
 *      Author: yuetq
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_

//
// Included Files
//

#include "driverlib.h"
#include "device.h"

//*****************************************************************************
//
// PinMux Configurations
//
//*****************************************************************************

//
// SPIA -> mySPI0 Pinmux
//
//
// SPIA_PICO - GPIO Settings
//
#define GPIO_PIN_SPIA_PICO 16
#define mySPI0_SPIPICO_GPIO 16
#define mySPI0_SPIPICO_PIN_CONFIG GPIO_16_SPISIMOA
//
// SPIA_POCI - GPIO Settings
//
#define GPIO_PIN_SPIA_POCI 17
#define mySPI0_SPIPOCI_GPIO 17
#define mySPI0_SPIPOCI_PIN_CONFIG GPIO_17_SPISOMIA
//
// SPIA_CLK - GPIO Settings
//
#define GPIO_PIN_SPIA_CLK 18
#define mySPI0_SPICLK_GPIO 18
#define mySPI0_SPICLK_PIN_CONFIG GPIO_18_SPICLKA
//
// SPIA_PTE - GPIO Settings
//
#define GPIO_PIN_SPIA_PTE 19
#define mySPI0_SPIPTE_GPIO 19
#define mySPI0_SPIPTE_PIN_CONFIG GPIO_19_SPISTEA

//*****************************************************************************
//
// SPI Configurations
//
//*****************************************************************************
#define mySPI0_BASE SPIA_BASE
#define mySPI0_BITRATE 1000000
void mySPI0_init();

//*****************************************************************************
//
// Board Configurations
//
//*****************************************************************************
void	Board_init();
void	SPI_init();
void	PinMux_init();





#endif /* SRC_BOARD_H_ */
