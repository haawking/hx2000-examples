//###########################################################################
//
// FILE:   asysctl.h
//
// TITLE:  H28x driver for Analog System Control.
//
//###########################################################################

#ifndef ASYSCTL_H
#define ASYSCTL_H

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//*****************************************************************************
//
//! \addtogroup asysctl_api ASysCtl
//! @{
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_asysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "debug.h"
#include "cpu.h"

//*****************************************************************************
//
// Defines used for setting AnalogReference functions.
// ASysCtl_setAnalogReferenceExternal()
// ASysCtl_setAnalogReference2P5()
// ASysCtl_setAnalogReference1P65()
//
//*****************************************************************************
#define ASYSCTL_VREFHIA  0x1U //!< VREFHIA
#define ASYSCTL_VREFHIB  0x2U //!< VREFHIB
#define ASYSCTL_VREFHIC  0x4U //!< VREFHIC
#define ASYSCTL_VREFHID  0x8U //!< VREFHID
//*****************************************************************************
//
// Prototypes for the APIs.
//
//*****************************************************************************

//*****************************************************************************
//
//! Enable temperature sensor.
//!
//! This function enables the temperature sensor output to the ADC.
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void
ASysCtl_enableTemperatureSensor(void)
{
    EALLOW;

    //
    // Set the temperature sensor enable bit.
    //
    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_TSNSCTL) |= ASYSCTL_TSNSCTL_ENABLE;

    EDIS;
}

//*****************************************************************************
//
//! Disable temperature sensor.
//!
//! This function disables the temperature sensor output to the ADC.
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void
ASysCtl_disableTemperatureSensor(void)
{
    EALLOW;

    //
    // Clear the temperature sensor enable bit.
    //
    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_TSNSCTL) &= ~(ASYSCTL_TSNSCTL_ENABLE);

    EDIS;
}

//*****************************************************************************
//
//! Locks the temperature sensor control register.
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void ASysCtl_lockTemperatureSensor(void)
{
    EALLOW;

    //
    // Write a 1 to the lock bit in the LOCK register.
    //
    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_LOCK) |= ASYSCTL_LOCK_TSNSCTL;

    EDIS;
}

//*****************************************************************************
//
//! Set the analog voltage reference selection to external.
//!
//! \param reference is the analog reference.
//!
//! The parameter \e reference can be a combination of the following values:
//!
//! - \b ASYSCTL_VREFHI
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void
ASysCtl_setAnalogReferenceExternal(uint32_t reference)
{
    ASSERT((reference & (
                         ASYSCTL_VREFHIA |
						 ASYSCTL_VREFHIB |
						 ASYSCTL_VREFHIC |
						 ASYSCTL_VREFHID
                        )) == reference);

    EALLOW;

    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_PMUREF2P5EN;
    //
    // Write selection to the Analog External Reference Select bit.
    //
    if((reference & ASYSCTL_VREFHIA) == ASYSCTL_VREFHIA)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFAVDDSEL;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHIAEN;
	}

	if((reference & ASYSCTL_VREFHIB) == ASYSCTL_VREFHIB)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHIBEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFBVDDSEL;
	}

	if((reference & ASYSCTL_VREFHIC) == ASYSCTL_VREFHIC)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHICEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFCVDDSEL;
	}

	if((reference & ASYSCTL_VREFHID) == ASYSCTL_VREFHID)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHDEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFDVDDSEL;
	}

    EDIS;
}
//*****************************************************************************
//
//! Set the internal analog voltage reference selection to 2.5V.
//!
//! \param reference is the analog reference.
//!
//! The parameter \e reference can be a combination of the following values:
//!
//! - \b ASYSCTL_VREFHI
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void ASysCtl_setAnalogReference2P5(uint32_t reference)
{
	ASSERT((reference & (
	                         ASYSCTL_VREFHIA |
							 ASYSCTL_VREFHIB |
							 ASYSCTL_VREFHIC |
							 ASYSCTL_VREFHID
	                        )) == reference);

    EALLOW;

    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_PMUREF2P5EN;
    //
    // Write selection to the Analog Voltage Reference Select bit.
    //
    if((reference & ASYSCTL_VREFHIA) == ASYSCTL_VREFHIA)
	{
    	HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFHIAEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFAVDDSEL;
	}

	if((reference & ASYSCTL_VREFHIB) == ASYSCTL_VREFHIB)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFHIBEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFBVDDSEL;
	}

	if((reference & ASYSCTL_VREFHIC) == ASYSCTL_VREFHIC)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFHICEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFCVDDSEL;
	}

	if((reference & ASYSCTL_VREFHID) == ASYSCTL_VREFHID)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFHDEN;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFDVDDSEL;
	}

    EDIS;
}

//*****************************************************************************
//
//! Set the internal analog voltage reference selection to 1.65V.
//!
//! \param reference is the analog reference.
//!
//! The parameter \e reference can be a combination of the following values:
//!
//! - \b ASYSCTL_VREFHI
//!
//! \return None.
//
//*****************************************************************************
static __always_inline void ASysCtl_setAnalogReference1P65(uint16_t reference)
{
	ASSERT((reference & (
	                         ASYSCTL_VREFHIA |
							 ASYSCTL_VREFHIB |
							 ASYSCTL_VREFHIC |
							 ASYSCTL_VREFHID
	                        )) == reference);

    EALLOW;

    HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_PMUREF2P5EN;
	//
	// Write selection to the Analog Voltage Reference Select bit.
	//
	if((reference & ASYSCTL_VREFHIA) == ASYSCTL_VREFHIA)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFAVDDSEL;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHIAEN;
	}

	if((reference & ASYSCTL_VREFHIB) == ASYSCTL_VREFHIB)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFBVDDSEL;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHIBEN;
	}

	if((reference & ASYSCTL_VREFHIC) == ASYSCTL_VREFHIC)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFCVDDSEL;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHICEN;
	}

	if((reference & ASYSCTL_VREFHID) == ASYSCTL_VREFHID)
	{
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) |= ASYSCTL_ANAREFCTL_ANAREFDVDDSEL;
		HWREG(ANALOGSUBSYS_BASE + ASYSCTL_O_ANAREFCTL) &= ~ASYSCTL_ANAREFCTL_ANAREFHDEN;
	}

    EDIS;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif // ASYSCTL_H
