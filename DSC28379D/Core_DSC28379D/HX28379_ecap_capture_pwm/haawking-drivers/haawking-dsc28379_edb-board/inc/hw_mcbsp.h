//###########################################################################
//
// FILE:    hw_mcbsp.h
//
// TITLE:   Definitions for the MCBSP registers.
//
//###########################################################################
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//   Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXS320F28379D, Hal DriverLib, 1.0.0
//
// Release time: 2023-08-30 03:07:28.906482
//
//#############################################################################


#ifndef HW_MCBSP_H
#define HW_MCBSP_H

//*************************************************************************************************
//
// The following are defines for the MCBSP register offsets
//
//*************************************************************************************************
#define MCBSP_O_DRR2     0x0U    // Data receive register bits 31-16
#define MCBSP_O_DRR1     0x4U    // Data receive register bits 15-0
#define MCBSP_O_DXR2     0x8U    // Data transmit register bits 31-16
#define MCBSP_O_DXR1     0xCU    // Data transmit register bits 15-0
#define MCBSP_O_SPCR2    0x10U   // Serial port control register 2
#define MCBSP_O_SPCR1    0x14U   // Serial port control register 1
#define MCBSP_O_RCR2     0x18U   // Receive Control register 2
#define MCBSP_O_RCR1     0x1CU   // Receive Control register 1
#define MCBSP_O_XCR2     0x20U   // Transmit Control register 2
#define MCBSP_O_XCR1     0x24U   // Transmit Control register 1
#define MCBSP_O_SRGR2    0x28U   // Sample rate generator register 2
#define MCBSP_O_SRGR1    0x2CU   // Sample rate generator register 1
#define MCBSP_O_MCR2     0x30U   // Multi-channel control register 2
#define MCBSP_O_MCR1     0x34U   // Multi-channel control register 1
#define MCBSP_O_RCERA    0x38U   // Receive channel enable partition A
#define MCBSP_O_RCERB    0x3CU   // Receive channel enable partition B
#define MCBSP_O_XCERA    0x40U   // Transmit channel enable partition A
#define MCBSP_O_XCERB    0x44U   // Transmit channel enable partition B
#define MCBSP_O_PCR      0x48U   // Pin Control register
#define MCBSP_O_RCERC    0x4CU   // Receive channel enable partition C
#define MCBSP_O_RCERD    0x50U   // Receive channel enable partition D
#define MCBSP_O_XCERC    0x54U   // Transmit channel enable partition C
#define MCBSP_O_XCERD    0x58U   // Transmit channel enable partition D
#define MCBSP_O_RCERE    0x5CU   // Receive channel enable partition E
#define MCBSP_O_RCERF    0x60U   // Receive channel enable partition F
#define MCBSP_O_XCERE    0x64U   // Transmit channel enable partition E
#define MCBSP_O_XCERF    0x68U   // Transmit channel enable partition F
#define MCBSP_O_RCERG    0x6CU   // Receive channel enable partition G
#define MCBSP_O_RCERH    0x70U   // Receive channel enable partition H
#define MCBSP_O_XCERG    0x74U   // Transmit channel enable partition G
#define MCBSP_O_XCERH    0x78U   // Transmit channel enable partition H
#define MCBSP_O_MFFINT   0x8CU   // Interrupt enable


//*************************************************************************************************
//
// The following are defines for the bit fields in the DRR2 register
//
//*************************************************************************************************
#define MCBSP_DRR2_HWLB_S   0U
#define MCBSP_DRR2_HWLB_M   0xFFU     // High word low byte
#define MCBSP_DRR2_HWHB_S   8U
#define MCBSP_DRR2_HWHB_M   0xFF00U   // High word high byte

//*************************************************************************************************
//
// The following are defines for the bit fields in the DRR1 register
//
//*************************************************************************************************
#define MCBSP_DRR1_LWLB_S   0U
#define MCBSP_DRR1_LWLB_M   0xFFU     // Low word low byte
#define MCBSP_DRR1_LWHB_S   8U
#define MCBSP_DRR1_LWHB_M   0xFF00U   // Low word high byte

//*************************************************************************************************
//
// The following are defines for the bit fields in the DXR2 register
//
//*************************************************************************************************
#define MCBSP_DXR2_HWLB_S   0U
#define MCBSP_DXR2_HWLB_M   0xFFU     // High word low byte
#define MCBSP_DXR2_HWHB_S   8U
#define MCBSP_DXR2_HWHB_M   0xFF00U   // High word high byte

//*************************************************************************************************
//
// The following are defines for the bit fields in the DXR1 register
//
//*************************************************************************************************
#define MCBSP_DXR1_LWLB_S   0U
#define MCBSP_DXR1_LWLB_M   0xFFU     // Low word low byte
#define MCBSP_DXR1_LWHB_S   8U
#define MCBSP_DXR1_LWHB_M   0xFF00U   // Low word high byte

//*************************************************************************************************
//
// The following are defines for the bit fields in the SPCR2 register
//
//*************************************************************************************************
#define MCBSP_SPCR2_XRST       0x1U     // Transmitter reset
#define MCBSP_SPCR2_XRDY       0x2U     // Transmitter ready
#define MCBSP_SPCR2_XEMPTY     0x4U     // Transmitter empty
#define MCBSP_SPCR2_XSYNCERR   0x8U     // Transmit sync error INT flag
#define MCBSP_SPCR2_XINTM_S    4U
#define MCBSP_SPCR2_XINTM_M    0x30U    // Transmit Interupt mode bits
#define MCBSP_SPCR2_GRST       0x40U    // Sample rate generator reset
#define MCBSP_SPCR2_FRST       0x80U    // Frame sync logic reset
#define MCBSP_SPCR2_SOFT       0x100U   // SOFT bit
#define MCBSP_SPCR2_FREE       0x200U   // FREE bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the SPCR1 register
//
//*************************************************************************************************
#define MCBSP_SPCR1_RRST       0x1U      // Receiver reset
#define MCBSP_SPCR1_RRDY       0x2U      // Receiver ready
#define MCBSP_SPCR1_RFULL      0x4U      // Receiver full
#define MCBSP_SPCR1_RSYNCERR   0x8U      // Receive sync error INT flag
#define MCBSP_SPCR1_RINTM_S    4U
#define MCBSP_SPCR1_RINTM_M    0x30U     // Receive Interupt mode bits
#define MCBSP_SPCR1_DXENA      0x80U     // DX delay enable
#define MCBSP_SPCR1_CLKSTP_S   11U
#define MCBSP_SPCR1_CLKSTP_M   0x1800U   // Clock stop mode
#define MCBSP_SPCR1_RJUST_S    13U
#define MCBSP_SPCR1_RJUST_M    0x6000U   // Rx sign extension and justification mode
#define MCBSP_SPCR1_DLB        0x8000U   // Digital loopback

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCR2 register
//
//*************************************************************************************************
#define MCBSP_RCR2_RDATDLY_S    0U
#define MCBSP_RCR2_RDATDLY_M    0x3U      // Receive data delay
#define MCBSP_RCR2_RFIG         0x4U      // Receive frame sync ignore
#define MCBSP_RCR2_RCOMPAND_S   3U
#define MCBSP_RCR2_RCOMPAND_M   0x18U     // Receive Companding Mode selects
#define MCBSP_RCR2_RWDLEN2_S    5U
#define MCBSP_RCR2_RWDLEN2_M    0xE0U     // Receive word length 2
#define MCBSP_RCR2_RFRLEN2_S    8U
#define MCBSP_RCR2_RFRLEN2_M    0x7F00U   // Receive Frame length 2
#define MCBSP_RCR2_RPHASE       0x8000U   // Receive Phase

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCR1 register
//
//*************************************************************************************************
#define MCBSP_RCR1_RWDLEN1_S   5U
#define MCBSP_RCR1_RWDLEN1_M   0xE0U     // Receive word length 1
#define MCBSP_RCR1_RFRLEN1_S   8U
#define MCBSP_RCR1_RFRLEN1_M   0x7F00U   // Receive Frame length 1

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCR2 register
//
//*************************************************************************************************
#define MCBSP_XCR2_XDATDLY_S    0U
#define MCBSP_XCR2_XDATDLY_M    0x3U      // Transmit data delay
#define MCBSP_XCR2_XFIG         0x4U      // Transmit frame sync ignore
#define MCBSP_XCR2_XCOMPAND_S   3U
#define MCBSP_XCR2_XCOMPAND_M   0x18U     // Transmit Companding Mode selects
#define MCBSP_XCR2_XWDLEN2_S    5U
#define MCBSP_XCR2_XWDLEN2_M    0xE0U     // Transmit word length 2
#define MCBSP_XCR2_XFRLEN2_S    8U
#define MCBSP_XCR2_XFRLEN2_M    0x7F00U   // Transmit Frame length 2
#define MCBSP_XCR2_XPHASE       0x8000U   // Transmit Phase

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCR1 register
//
//*************************************************************************************************
#define MCBSP_XCR1_XWDLEN1_S   5U
#define MCBSP_XCR1_XWDLEN1_M   0xE0U     // Transmit word length 1
#define MCBSP_XCR1_XFRLEN1_S   8U
#define MCBSP_XCR1_XFRLEN1_M   0x7F00U   // Transmit Frame length 1

//*************************************************************************************************
//
// The following are defines for the bit fields in the SRGR2 register
//
//*************************************************************************************************
#define MCBSP_SRGR2_FPER_S   0U
#define MCBSP_SRGR2_FPER_M   0xFFFU    // Frame-sync period
#define MCBSP_SRGR2_FSGM     0x1000U   // Frame sync generator mode
#define MCBSP_SRGR2_CLKSM    0x2000U   // Sample rate generator mode
#define MCBSP_SRGR2_GSYNC    0x8000U   // CLKG sync

//*************************************************************************************************
//
// The following are defines for the bit fields in the SRGR1 register
//
//*************************************************************************************************
#define MCBSP_SRGR1_CLKGDV_S   0U
#define MCBSP_SRGR1_CLKGDV_M   0xFFU     // CLKG divider
#define MCBSP_SRGR1_FWID_S     8U
#define MCBSP_SRGR1_FWID_M     0xFF00U   // Frame width

//*************************************************************************************************
//
// The following are defines for the bit fields in the MCR2 register
//
//*************************************************************************************************
#define MCBSP_MCR2_XMCM_S     0U
#define MCBSP_MCR2_XMCM_M     0x3U     // Transmit data delay
#define MCBSP_MCR2_XCBLK_S    2U
#define MCBSP_MCR2_XCBLK_M    0x1CU    // Transmit frame sync ignore
#define MCBSP_MCR2_XPABLK_S   5U
#define MCBSP_MCR2_XPABLK_M   0x60U    // Transmit Companding Mode selects
#define MCBSP_MCR2_XPBBLK_S   7U
#define MCBSP_MCR2_XPBBLK_M   0x180U   // Transmit word length 2
#define MCBSP_MCR2_XMCME      0x200U   // Transmit Frame length 2

//*************************************************************************************************
//
// The following are defines for the bit fields in the MCR1 register
//
//*************************************************************************************************
#define MCBSP_MCR1_RMCM       0x1U     // Receive multichannel mode
#define MCBSP_MCR1_RCBLK_S    2U
#define MCBSP_MCR1_RCBLK_M    0x1CU    // eceive current block
#define MCBSP_MCR1_RPABLK_S   5U
#define MCBSP_MCR1_RPABLK_M   0x60U    // Receive partition A Block
#define MCBSP_MCR1_RPBBLK_S   7U
#define MCBSP_MCR1_RPBBLK_M   0x180U   // Receive partition B Block
#define MCBSP_MCR1_RMCME      0x200U   // Receive multi-channel enhance mode

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERA register
//
//*************************************************************************************************
#define MCBSP_RCERA_RCE0    0x1U      // Receive channel 0 enable bit
#define MCBSP_RCERA_RCE1    0x2U      // Receive channel 1 enable bit
#define MCBSP_RCERA_RCE2    0x4U      // Receive channel 2 enable bit
#define MCBSP_RCERA_RCE3    0x8U      // Receive channel 3 enable bit
#define MCBSP_RCERA_RCE4    0x10U     // Receive channel 4 enable bit
#define MCBSP_RCERA_RCE5    0x20U     // Receive channel 5 enable bit
#define MCBSP_RCERA_RCE6    0x40U     // Receive channel 6 enable bit
#define MCBSP_RCERA_RCE7    0x80U     // Receive channel 7 enable bit
#define MCBSP_RCERA_RCE8    0x100U    // Receive channel 8 enable bit
#define MCBSP_RCERA_RCE9    0x200U    // Receive channel 9 enable bit
#define MCBSP_RCERA_RCE10   0x400U    // Receive channel 10 enable bit
#define MCBSP_RCERA_RCE11   0x800U    // Receive channel 11 enable bit
#define MCBSP_RCERA_RCE12   0x1000U   // Receive channel 12 enable bit
#define MCBSP_RCERA_RCE13   0x2000U   // Receive channel 13 enable bit
#define MCBSP_RCERA_RCE14   0x4000U   // Receive channel 14 enable bit
#define MCBSP_RCERA_RCE15   0x8000U   // Receive channel 15 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERB register
//
//*************************************************************************************************
#define MCBSP_RCERB_RCE0    0x1U      // Receive channel 16 enable bit
#define MCBSP_RCERB_RCE1    0x2U      // Receive channel 17 enable bit
#define MCBSP_RCERB_RCE2    0x4U      // Receive channel 18 enable bit
#define MCBSP_RCERB_RCE3    0x8U      // Receive channel 19 enable bit
#define MCBSP_RCERB_RCE4    0x10U     // Receive channel 20 enable bit
#define MCBSP_RCERB_RCE5    0x20U     // Receive channel 21 enable bit
#define MCBSP_RCERB_RCE6    0x40U     // Receive channel 22 enable bit
#define MCBSP_RCERB_RCE7    0x80U     // Receive channel 23 enable bit
#define MCBSP_RCERB_RCE8    0x100U    // Receive channel 24 enable bit
#define MCBSP_RCERB_RCE9    0x200U    // Receive channel 25 enable bit
#define MCBSP_RCERB_RCE10   0x400U    // Receive channel 26 enable bit
#define MCBSP_RCERB_RCE11   0x800U    // Receive channel 27 enable bit
#define MCBSP_RCERB_RCE12   0x1000U   // Receive channel 28 enable bit
#define MCBSP_RCERB_RCE13   0x2000U   // Receive channel 29 enable bit
#define MCBSP_RCERB_RCE14   0x4000U   // Receive channel 30 enable bit
#define MCBSP_RCERB_RCE15   0x8000U   // Receive channel 31 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERA register
//
//*************************************************************************************************
#define MCBSP_XCERA_XCE0    0x1U      // Transmit channel 0 enable bit
#define MCBSP_XCERA_XCE1    0x2U      // Transmit channel 1 enable bit
#define MCBSP_XCERA_XCE2    0x4U      // Transmit channel 2 enable bit
#define MCBSP_XCERA_XCE3    0x8U      // Transmit channel 3 enable bit
#define MCBSP_XCERA_XCE4    0x10U     // Transmit channel 4 enable bit
#define MCBSP_XCERA_XCE5    0x20U     // Transmit channel 5 enable bit
#define MCBSP_XCERA_XCE6    0x40U     // Transmit channel 6 enable bit
#define MCBSP_XCERA_XCE7    0x80U     // Transmit channel 7 enable bit
#define MCBSP_XCERA_XCE8    0x100U    // Transmit channel 8 enable bit
#define MCBSP_XCERA_XCE9    0x200U    // Transmit channel 9 enable bit
#define MCBSP_XCERA_XCE10   0x400U    // Transmit channel 10 enable bit
#define MCBSP_XCERA_XCE11   0x800U    // Transmit channel 11 enable bit
#define MCBSP_XCERA_XCE12   0x1000U   // Transmit channel 12 enable bit
#define MCBSP_XCERA_XCE13   0x2000U   // Transmit channel 13 enable bit
#define MCBSP_XCERA_XCE14   0x4000U   // Transmit channel 14 enable bit
#define MCBSP_XCERA_XCE15   0x8000U   // Transmit channel 15 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERB register
//
//*************************************************************************************************
#define MCBSP_XCERB_XCE0    0x1U      // Transmit channel 16 enable bit
#define MCBSP_XCERB_XCE1    0x2U      // Transmit channel 17 enable bit
#define MCBSP_XCERB_XCE2    0x4U      // Transmit channel 18 enable bit
#define MCBSP_XCERB_XCE3    0x8U      // Transmit channel 19 enable bit
#define MCBSP_XCERB_XCE4    0x10U     // Transmit channel 20 enable bit
#define MCBSP_XCERB_XCE5    0x20U     // Transmit channel 21 enable bit
#define MCBSP_XCERB_XCE6    0x40U     // Transmit channel 22 enable bit
#define MCBSP_XCERB_XCE7    0x80U     // Transmit channel 23 enable bit
#define MCBSP_XCERB_XCE8    0x100U    // Transmit channel 24 enable bit
#define MCBSP_XCERB_XCE9    0x200U    // Transmit channel 25 enable bit
#define MCBSP_XCERB_XCE10   0x400U    // Transmit channel 26 enable bit
#define MCBSP_XCERB_XCE11   0x800U    // Transmit channel 27 enable bit
#define MCBSP_XCERB_XCE12   0x1000U   // Transmit channel 28 enable bit
#define MCBSP_XCERB_XCE13   0x2000U   // Transmit channel 29 enable bit
#define MCBSP_XCERB_XCE14   0x4000U   // Transmit channel 30 enable bit
#define MCBSP_XCERB_XCE15   0x8000U   // Transmit channel 31 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the PCR register
//
//*************************************************************************************************
#define MCBSP_PCR_CLKRP    0x1U     // Receive Clock polarity
#define MCBSP_PCR_CLKXP    0x2U     // Transmit clock polarity
#define MCBSP_PCR_FSRP     0x4U     // Receive Frame synchronization polarity
#define MCBSP_PCR_FSXP     0x8U     // Transmit Frame synchronization polarity
#define MCBSP_PCR_SCLKME   0x80U    // Sample clock mode selection
#define MCBSP_PCR_CLKRM    0x100U   // Receiver Clock Mode
#define MCBSP_PCR_CLKXM    0x200U   // Transmit Clock Mode.
#define MCBSP_PCR_FSRM     0x400U   // Receive Frame Synchronization Mode
#define MCBSP_PCR_FSXM     0x800U   // Transmit Frame Synchronization Mode

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERC register
//
//*************************************************************************************************
#define MCBSP_RCERC_RCE0    0x1U      // Receive channel 32 enable bit
#define MCBSP_RCERC_RCE1    0x2U      // Receive channel 33 enable bit
#define MCBSP_RCERC_RCE2    0x4U      // Receive channel 34 enable bit
#define MCBSP_RCERC_RCE3    0x8U      // Receive channel 35 enable bit
#define MCBSP_RCERC_RCE4    0x10U     // Receive channel 36 enable bit
#define MCBSP_RCERC_RCE5    0x20U     // Receive channel 37 enable bit
#define MCBSP_RCERC_RCE6    0x40U     // Receive channel 38 enable bit
#define MCBSP_RCERC_RCE7    0x80U     // Receive channel 39 enable bit
#define MCBSP_RCERC_RCE8    0x100U    // Receive channel 40 enable bit
#define MCBSP_RCERC_RCE9    0x200U    // Receive channel 41 enable bit
#define MCBSP_RCERC_RCE10   0x400U    // Receive channel 42 enable bit
#define MCBSP_RCERC_RCE11   0x800U    // Receive channel 43 enable bit
#define MCBSP_RCERC_RCE12   0x1000U   // Receive channel 44 enable bit
#define MCBSP_RCERC_RCE13   0x2000U   // Receive channel 45 enable bit
#define MCBSP_RCERC_RCE14   0x4000U   // Receive channel 46 enable bit
#define MCBSP_RCERC_RCE15   0x8000U   // Receive channel 47 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERD register
//
//*************************************************************************************************
#define MCBSP_RCERD_RCE0    0x1U      // Receive channel 48 enable bit
#define MCBSP_RCERD_RCE1    0x2U      // Receive channel 49 enable bit
#define MCBSP_RCERD_RCE2    0x4U      // Receive channel 50 enable bit
#define MCBSP_RCERD_RCE3    0x8U      // Receive channel 51 enable bit
#define MCBSP_RCERD_RCE4    0x10U     // Receive channel 52 enable bit
#define MCBSP_RCERD_RCE5    0x20U     // Receive channel 53 enable bit
#define MCBSP_RCERD_RCE6    0x40U     // Receive channel 54 enable bit
#define MCBSP_RCERD_RCE7    0x80U     // Receive channel 55 enable bit
#define MCBSP_RCERD_RCE8    0x100U    // Receive channel 56 enable bit
#define MCBSP_RCERD_RCE9    0x200U    // Receive channel 57 enable bit
#define MCBSP_RCERD_RCE10   0x400U    // Receive channel 58 enable bit
#define MCBSP_RCERD_RCE11   0x800U    // Receive channel 59 enable bit
#define MCBSP_RCERD_RCE12   0x1000U   // Receive channel 60 enable bit
#define MCBSP_RCERD_RCE13   0x2000U   // Receive channel 61 enable bit
#define MCBSP_RCERD_RCE14   0x4000U   // Receive channel 62 enable bit
#define MCBSP_RCERD_RCE15   0x8000U   // Receive channel 63 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERC register
//
//*************************************************************************************************
#define MCBSP_XCERC_XCE0    0x1U      // Transmit channel 32 enable bit
#define MCBSP_XCERC_XCE1    0x2U      // Transmit channel 33 enable bit
#define MCBSP_XCERC_XCE2    0x4U      // Transmit channel 34 enable bit
#define MCBSP_XCERC_XCE3    0x8U      // Transmit channel 35 enable bit
#define MCBSP_XCERC_XCE4    0x10U     // Transmit channel 36 enable bit
#define MCBSP_XCERC_XCE5    0x20U     // Transmit channel 37 enable bit
#define MCBSP_XCERC_XCE6    0x40U     // Transmit channel 38 enable bit
#define MCBSP_XCERC_XCE7    0x80U     // Transmit channel 39 enable bit
#define MCBSP_XCERC_XCE8    0x100U    // Transmit channel 40 enable bit
#define MCBSP_XCERC_XCE9    0x200U    // Transmit channel 41 enable bit
#define MCBSP_XCERC_XCE10   0x400U    // Transmit channel 42 enable bit
#define MCBSP_XCERC_XCE11   0x800U    // Transmit channel 43 enable bit
#define MCBSP_XCERC_XCE12   0x1000U   // Transmit channel 44 enable bit
#define MCBSP_XCERC_XCE13   0x2000U   // Transmit channel 45 enable bit
#define MCBSP_XCERC_XCE14   0x4000U   // Transmit channel 46 enable bit
#define MCBSP_XCERC_XCE15   0x8000U   // Transmit channel 47 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERD register
//
//*************************************************************************************************
#define MCBSP_XCERD_XCE0    0x1U      // Transmit channel 48 enable bit
#define MCBSP_XCERD_XCE1    0x2U      // Transmit channel 49 enable bit
#define MCBSP_XCERD_XCE2    0x4U      // Transmit channel 50 enable bit
#define MCBSP_XCERD_XCE3    0x8U      // Transmit channel 51 enable bit
#define MCBSP_XCERD_XCE4    0x10U     // Transmit channel 52 enable bit
#define MCBSP_XCERD_XCE5    0x20U     // Transmit channel 53 enable bit
#define MCBSP_XCERD_XCE6    0x40U     // Transmit channel 54 enable bit
#define MCBSP_XCERD_XCE7    0x80U     // Transmit channel 55 enable bit
#define MCBSP_XCERD_XCE8    0x100U    // Transmit channel 56 enable bit
#define MCBSP_XCERD_XCE9    0x200U    // Transmit channel 57 enable bit
#define MCBSP_XCERD_XCE10   0x400U    // Transmit channel 58 enable bit
#define MCBSP_XCERD_XCE11   0x800U    // Transmit channel 59 enable bit
#define MCBSP_XCERD_XCE12   0x1000U   // Transmit channel 60 enable bit
#define MCBSP_XCERD_XCE13   0x2000U   // Transmit channel 61 enable bit
#define MCBSP_XCERD_XCE14   0x4000U   // Transmit channel 62 enable bit
#define MCBSP_XCERD_XCE15   0x8000U   // Transmit channel 63 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERE register
//
//*************************************************************************************************
#define MCBSP_RCERE_RCE0    0x1U      // Receive channel 64 enable bit
#define MCBSP_RCERE_RCE1    0x2U      // Receive channel 65 enable bit
#define MCBSP_RCERE_RCE2    0x4U      // Receive channel 66 enable bit
#define MCBSP_RCERE_RCE3    0x8U      // Receive channel 67 enable bit
#define MCBSP_RCERE_RCE4    0x10U     // Receive channel 68 enable bit
#define MCBSP_RCERE_RCE5    0x20U     // Receive channel 69 enable bit
#define MCBSP_RCERE_RCE6    0x40U     // Receive channel 70 enable bit
#define MCBSP_RCERE_RCE7    0x80U     // Receive channel 71 enable bit
#define MCBSP_RCERE_RCE8    0x100U    // Receive channel 72 enable bit
#define MCBSP_RCERE_RCE9    0x200U    // Receive channel 73 enable bit
#define MCBSP_RCERE_RCE10   0x400U    // Receive channel 74 enable bit
#define MCBSP_RCERE_RCE11   0x800U    // Receive channel 75 enable bit
#define MCBSP_RCERE_RCE12   0x1000U   // Receive channel 76 enable bit
#define MCBSP_RCERE_RCE13   0x2000U   // Receive channel 77 enable bit
#define MCBSP_RCERE_RCE14   0x4000U   // Receive channel 78 enable bit
#define MCBSP_RCERE_RCE15   0x8000U   // Receive channel 79 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERF register
//
//*************************************************************************************************
#define MCBSP_RCERF_RCE0    0x1U      // Receive channel 80 enable bit
#define MCBSP_RCERF_RCE1    0x2U      // Receive channel 81 enable bit
#define MCBSP_RCERF_RCE2    0x4U      // Receive channel 82 enable bit
#define MCBSP_RCERF_RCE3    0x8U      // Receive channel 83 enable bit
#define MCBSP_RCERF_RCE4    0x10U     // Receive channel 84 enable bit
#define MCBSP_RCERF_RCE5    0x20U     // Receive channel 85 enable bit
#define MCBSP_RCERF_RCE6    0x40U     // Receive channel 86 enable bit
#define MCBSP_RCERF_RCE7    0x80U     // Receive channel 87 enable bit
#define MCBSP_RCERF_RCE8    0x100U    // Receive channel 88 enable bit
#define MCBSP_RCERF_RCE9    0x200U    // Receive channel 89 enable bit
#define MCBSP_RCERF_RCE10   0x400U    // Receive channel 90 enable bit
#define MCBSP_RCERF_RCE11   0x800U    // Receive channel 91 enable bit
#define MCBSP_RCERF_RCE12   0x1000U   // Receive channel 92 enable bit
#define MCBSP_RCERF_RCE13   0x2000U   // Receive channel 93 enable bit
#define MCBSP_RCERF_RCE14   0x4000U   // Receive channel 94 enable bit
#define MCBSP_RCERF_RCE15   0x8000U   // Receive channel 95 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERE register
//
//*************************************************************************************************
#define MCBSP_XCERE_XCE0    0x1U      // Transmit channel 64 enable bit
#define MCBSP_XCERE_XCE1    0x2U      // Transmit channel 65 enable bit
#define MCBSP_XCERE_XCE2    0x4U      // Transmit channel 66 enable bit
#define MCBSP_XCERE_XCE3    0x8U      // Transmit channel 67 enable bit
#define MCBSP_XCERE_XCE4    0x10U     // Transmit channel 68 enable bit
#define MCBSP_XCERE_XCE5    0x20U     // Transmit channel 69 enable bit
#define MCBSP_XCERE_XCE6    0x40U     // Transmit channel 70 enable bit
#define MCBSP_XCERE_XCE7    0x80U     // Transmit channel 71 enable bit
#define MCBSP_XCERE_XCE8    0x100U    // Transmit channel 72 enable bit
#define MCBSP_XCERE_XCE9    0x200U    // Transmit channel 73 enable bit
#define MCBSP_XCERE_XCE10   0x400U    // Transmit channel 74 enable bit
#define MCBSP_XCERE_XCE11   0x800U    // Transmit channel 75 enable bit
#define MCBSP_XCERE_XCE12   0x1000U   // Transmit channel 76 enable bit
#define MCBSP_XCERE_XCE13   0x2000U   // Transmit channel 77 enable bit
#define MCBSP_XCERE_XCE14   0x4000U   // Transmit channel 78 enable bit
#define MCBSP_XCERE_XCE15   0x8000U   // Transmit channel 79 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERF register
//
//*************************************************************************************************
#define MCBSP_XCERF_XCE0    0x1U      // Transmit channel 80 enable bit
#define MCBSP_XCERF_XCE1    0x2U      // Transmit channel 81 enable bit
#define MCBSP_XCERF_XCE2    0x4U      // Transmit channel 82 enable bit
#define MCBSP_XCERF_XCE3    0x8U      // Transmit channel 83 enable bit
#define MCBSP_XCERF_XCE4    0x10U     // Transmit channel 84 enable bit
#define MCBSP_XCERF_XCE5    0x20U     // Transmit channel 85 enable bit
#define MCBSP_XCERF_XCE6    0x40U     // Transmit channel 86 enable bit
#define MCBSP_XCERF_XCE7    0x80U     // Transmit channel 87 enable bit
#define MCBSP_XCERF_XCE8    0x100U    // Transmit channel 88 enable bit
#define MCBSP_XCERF_XCE9    0x200U    // Transmit channel 89 enable bit
#define MCBSP_XCERF_XCE10   0x400U    // Transmit channel 90 enable bit
#define MCBSP_XCERF_XCE11   0x800U    // Transmit channel 91 enable bit
#define MCBSP_XCERF_XCE12   0x1000U   // Transmit channel 92 enable bit
#define MCBSP_XCERF_XCE13   0x2000U   // Transmit channel 93 enable bit
#define MCBSP_XCERF_XCE14   0x4000U   // Transmit channel 94 enable bit
#define MCBSP_XCERF_XCE15   0x8000U   // Transmit channel 95 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERG register
//
//*************************************************************************************************
#define MCBSP_RCERG_RCE0    0x1U      // Receive channel 96 enable bit
#define MCBSP_RCERG_RCE1    0x2U      // Receive channel 97 enable bit
#define MCBSP_RCERG_RCE2    0x4U      // Receive channel 98 enable bit
#define MCBSP_RCERG_RCE3    0x8U      // Receive channel 99 enable bit
#define MCBSP_RCERG_RCE4    0x10U     // Receive channel 100 enable bit
#define MCBSP_RCERG_RCE5    0x20U     // Receive channel 101 enable bit
#define MCBSP_RCERG_RCE6    0x40U     // Receive channel 102 enable bit
#define MCBSP_RCERG_RCE7    0x80U     // Receive channel 103 enable bit
#define MCBSP_RCERG_RCE8    0x100U    // Receive channel 104 enable bit
#define MCBSP_RCERG_RCE9    0x200U    // Receive channel 105 enable bit
#define MCBSP_RCERG_RCE10   0x400U    // Receive channel 106 enable bit
#define MCBSP_RCERG_RCE11   0x800U    // Receive channel 107 enable bit
#define MCBSP_RCERG_RCE12   0x1000U   // Receive channel 108 enable bit
#define MCBSP_RCERG_RCE13   0x2000U   // Receive channel 109 enable bit
#define MCBSP_RCERG_RCE14   0x4000U   // Receive channel 110 enable bit
#define MCBSP_RCERG_RCE15   0x8000U   // Receive channel 111 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the RCERH register
//
//*************************************************************************************************
#define MCBSP_RCERH_RCE0    0x1U      // Receive channel 112 enable bit
#define MCBSP_RCERH_RCE1    0x2U      // Receive channel 113 enable bit
#define MCBSP_RCERH_RCE2    0x4U      // Receive channel 114 enable bit
#define MCBSP_RCERH_RCE3    0x8U      // Receive channel 115 enable bit
#define MCBSP_RCERH_RCE4    0x10U     // Receive channel 116 enable bit
#define MCBSP_RCERH_RCE5    0x20U     // Receive channel 117 enable bit
#define MCBSP_RCERH_RCE6    0x40U     // Receive channel 118 enable bit
#define MCBSP_RCERH_RCE7    0x80U     // Receive channel 119 enable bit
#define MCBSP_RCERH_RCE8    0x100U    // Receive channel 120 enable bit
#define MCBSP_RCERH_RCE9    0x200U    // Receive channel 121 enable bit
#define MCBSP_RCERH_RCE10   0x400U    // Receive channel 122 enable bit
#define MCBSP_RCERH_RCE11   0x800U    // Receive channel 123 enable bit
#define MCBSP_RCERH_RCE12   0x1000U   // Receive channel 124 enable bit
#define MCBSP_RCERH_RCE13   0x2000U   // Receive channel 125 enable bit
#define MCBSP_RCERH_RCE14   0x4000U   // Receive channel 126 enable bit
#define MCBSP_RCERH_RCE15   0x8000U   // Receive channel 127 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERG register
//
//*************************************************************************************************
#define MCBSP_XCERG_XCE0    0x1U      // Transmit channel 96 enable bit
#define MCBSP_XCERG_XCE1    0x2U      // Transmit channel 97 enable bit
#define MCBSP_XCERG_XCE2    0x4U      // Transmit channel 98 enable bit
#define MCBSP_XCERG_XCE3    0x8U      // Transmit channel 99 enable bit
#define MCBSP_XCERG_XCE4    0x10U     // Transmit channel 100 enable bit
#define MCBSP_XCERG_XCE5    0x20U     // Transmit channel 101 enable bit
#define MCBSP_XCERG_XCE6    0x40U     // Transmit channel 102 enable bit
#define MCBSP_XCERG_XCE7    0x80U     // Transmit channel 103 enable bit
#define MCBSP_XCERG_XCE8    0x100U    // Transmit channel 104 enable bit
#define MCBSP_XCERG_XCE9    0x200U    // Transmit channel 105 enable bit
#define MCBSP_XCERG_XCE10   0x400U    // Transmit channel 106 enable bit
#define MCBSP_XCERG_XCE11   0x800U    // Transmit channel 107 enable bit
#define MCBSP_XCERG_XCE12   0x1000U   // Transmit channel 108 enable bit
#define MCBSP_XCERG_XCE13   0x2000U   // Transmit channel 109 enable bit
#define MCBSP_XCERG_XCE14   0x4000U   // Transmit channel 110 enable bit
#define MCBSP_XCERG_XCE15   0x8000U   // Transmit channel 111 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the XCERH register
//
//*************************************************************************************************
#define MCBSP_XCERH_XCE0    0x1U      // Transmit channel 112 enable bit
#define MCBSP_XCERH_XCE1    0x2U      // Transmit channel 113 enable bit
#define MCBSP_XCERH_XCE2    0x4U      // Transmit channel 114 enable bit
#define MCBSP_XCERH_XCE3    0x8U      // Transmit channel 115 enable bit
#define MCBSP_XCERH_XCE4    0x10U     // Transmit channel 116 enable bit
#define MCBSP_XCERH_XCE5    0x20U     // Transmit channel 117 enable bit
#define MCBSP_XCERH_XCE6    0x40U     // Transmit channel 118 enable bit
#define MCBSP_XCERH_XCE7    0x80U     // Transmit channel 119 enable bit
#define MCBSP_XCERH_XCE8    0x100U    // Transmit channel 120 enable bit
#define MCBSP_XCERH_XCE9    0x200U    // Transmit channel 121 enable bit
#define MCBSP_XCERH_XCE10   0x400U    // Transmit channel 122 enable bit
#define MCBSP_XCERH_XCE11   0x800U    // Transmit channel 123 enable bit
#define MCBSP_XCERH_XCE12   0x1000U   // Transmit channel 124 enable bit
#define MCBSP_XCERH_XCE13   0x2000U   // Transmit channel 125 enable bit
#define MCBSP_XCERH_XCE14   0x4000U   // Transmit channel 126 enable bit
#define MCBSP_XCERH_XCE15   0x8000U   // Transmit channel 127 enable bit

//*************************************************************************************************
//
// The following are defines for the bit fields in the MFFINT register
//
//*************************************************************************************************
#define MCBSP_MFFINT_XINT   0x1U   // Enable for Receive Interrupt
#define MCBSP_MFFINT_RINT   0x4U   // Enable for transmit Interrupt



#endif

