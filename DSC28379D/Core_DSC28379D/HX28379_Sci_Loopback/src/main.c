/******************************************************************
 文 档 名：      sci_ex1_loopback
 开 发 环 境：   Haawking IDE V2.2.7
 开 发 板：       Core_DSC28379_V1.0
 D S P：          DSC28379
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：
	 本程序使用外设的内部环回测试模式。
	 除了引导模式引脚配置外，不需要其他硬件配置。
	 引脚复用和SCI模块通过sysconfig文件进行配置。

	 此测试使用SCI模块的环回测试模式发送从0x00到0xFF的字符。
	 测试将发送一个字符，然后检查接收缓冲区是否正确匹配。

 连接方式：
	无

 现象：
	观察变量
	  - loopCount - 发送的字符数
	  - errorCount - 检测到的错误数
	  - sendChar - 发送的字符
	  - receivedChar - 接收到的字符

 版 本：      V1.0.0
 时 间：      2024年06月12日
 作 者：      duohan
 @ mail：    support@mail.haawking.com
 *******************************************************************************/
/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Globals
//
uint16_t loopCount;
uint16_t errorCount;


int main(void)
{
    Device_init();

    uint16_t sendChar;
    uint16_t receivedChar;


	//
	// Setup GPIO by disabling pin locks and enabling pullups
	//
	Device_initGPIO();

	//
	// Initialize PIE and clear PIE registers. Disables CPU interrupts.
	//
	Interrupt_initModule();

	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	//
	Interrupt_initVectorTable();

	//
	// Board Initialization
	//
	Board_init();

	//
	// Enables CPU interrupts
	//
	Interrupt_enableMaster();

	//
	// Initialize counts
	//
	loopCount = 0;
	errorCount = 0;

	//
	// Send a character starting with 0
	//
	sendChar = 1;

	//
	// Send Characters forever starting with 0x00 and going through 0xFF.
	// After sending each, check the receive buffer for the correct value.
	//
	for(;;)
	{
	  SCI_writeCharNonBlocking(mySCI0_BASE, sendChar);


	  //
	  // Wait for RRDY/RXFFST = 1 for 1 data available in FIFO
	  //
	  while(SCI_getRxFIFOStatus(mySCI0_BASE) == SCI_FIFO_RX0)
	  {
		  ;
	  }
	  //
	  // Check received character
	  //
	  receivedChar = SCI_readCharBlockingFIFO(mySCI0_BASE);

	  //
	  // Received character not correct
	  //
	  if(receivedChar != sendChar)
	  {
		  errorCount++;
		  // CaseFail();
		  while(1);
	  }
	  else
	  {
		  // CaseSuccess();
		  while(1);
	  }

	  //
	  // Move to the next character and repeat the test
	  //
	  sendChar++;

	  //
	  // Limit the character to 8-bits
	  //
	  sendChar &= 0x00FF;
	  loopCount++;

	}
    while(1);
    return 0;
}

//
// End of File
//

