/*#############################################################################*/
/*                                                                             */
/* $Copyright:                                                                 */
/* Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd                 */
/* http://www.haawking.com/ All rights reserved.                               */
/*                                                                             */
/* Redistribution and use in source and binary forms, with or without          */
/* modification, are permitted provided that the following conditions          */
/* are met:                                                                    */
/*                                                                             */
/*   Redistributions of source code must retain the above copyright            */
/*   notice, this list of conditions and the following disclaimer.             */
/*                                                                             */
/*   Redistributions in binary form must reproduce the above copyright         */
/*   notice, this list of conditions and the following disclaimer in the       */
/*   documentation and/or other materials provided with the                    */
/*   distribution.                                                             */
/*                                                                             */
/*   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of  */
/*   its contributors may be used to endorse or promote products derived       */
/*   from this software without specific prior written permission.             */
/*                                                                             */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS         */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT           */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR       */
/* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT            */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY       */
/* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT         */
/* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE       */
/* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.        */
/*                                                                             */
/*#############################################################################*/
/*                                                                             */
/* Release for HXS320F28379DEDB, Flash Linker, 1.0.0                     */
/*                                                                             */
/* Release time: 2023-08-28 13:16:55.263222                                    */
/*                                                                             */
/*#############################################################################*/

OUTPUT_ARCH( "riscv" )
ENTRY(_start)
INCLUDE "Peripheral.ld" 
INCLUDE "BootROM.ld" 

MEMORY
{
  RAMM0M1(rwx) :          ORIGIN = 0x000, LENGTH = 0x1000             /* RAM M0M1 */
  
  RAMLS0To2(rwx) :        ORIGIN = 0x10000, LENGTH = 0x6000          /* RAM LS0~2 */
  RAMLS3ToD1(rwx) :        ORIGIN = 0x16000, LENGTH = 0x8000          /* RAM LS3~D1 */
						  
  RAMGS0(rwx) :           ORIGIN = 0x1000000, LENGTH = 0x2000          /* RAM GS0 */
  RAMGS1(rwx) :           ORIGIN = 0x1002000, LENGTH = 0x2000          /* RAM GS1 */
  RAMGS2(rwx) :           ORIGIN = 0x1004000, LENGTH = 0x2000          /* RAM GS2 */
  RAMGS3(rwx) :           ORIGIN = 0x1006000, LENGTH = 0x2000          /* RAM GS3 */ 
  RAMGS4(rwx) :           ORIGIN = 0x1008000, LENGTH = 0x2000          /* RAM GS4 */
  RAMGS5(rwx) :           ORIGIN = 0x100A000, LENGTH = 0x2000          /* RAM GS5 */
  RAMGS6(rwx) :           ORIGIN = 0x100C000, LENGTH = 0x2000          /* RAM GS6 */
  RAMGS7(rwx) :           ORIGIN = 0x100E000, LENGTH = 0x2000          /* RAM GS7 */
  RAMGS8(rwx) :           ORIGIN = 0x1010000, LENGTH = 0x2000          /* RAM GS8 */
  RAMGS9(rwx) :           ORIGIN = 0x1012000, LENGTH = 0x2000          /* RAM GS9 */
  RAMGS10(rwx) :           ORIGIN = 0x1014000, LENGTH = 0x2000          /* RAM GS10 */
  RAMGS11(rwx) :           ORIGIN = 0x1016000, LENGTH = 0x2000          /* RAM GS11 */ 
  RAMGS12(rwx) :           ORIGIN = 0x1018000, LENGTH = 0x2000          /* RAM GS12 */
  RAMGS13(rwx) :           ORIGIN = 0x101A000, LENGTH = 0x2000          /* RAM GS13 */
  RAMGS14(rwx) :           ORIGIN = 0x101C000, LENGTH = 0x2000          /* RAM GS14 */
  RAMGS15(rwx) :           ORIGIN = 0x101E000, LENGTH = 0x2000          /* RAM GS15 */
  
  CPU1TOCPU2RAM(rwx) :           ORIGIN = 0x1021000, LENGTH = 0x800          /* RAM CPU1TOCPU2 */
  CPU2TOCPU1RAM(rwx) :           ORIGIN = 0x1021800, LENGTH = 0x800          /* RAM CPU2TOCPU1 */ 
  
  CANA_MSG_RAM(rwx) :        ORIGIN = 0x10000400, LENGTH = 0x800          /* RAM CANA_MSG */
  CANB_MSG_RAM(rwx) :        ORIGIN = 0x10001400, LENGTH = 0x800          /* RAM CANB_MSG */
  
  BEGIN(rwx) :       ORIGIN = 0x600000, LENGTH = 0x010
  FLASH(rwx) :      ORIGIN = 0x600010, LENGTH = 0x80000          /* FLASH */ 

  IER_REGISTER_FILE(rw) : ORIGIN  = 0x400C10, LENGTH = 0x04
  IFR_REGISTER_FILE(rw) : ORIGIN  = 0x400D10, LENGTH = 0x04
}

/*----------------------------------------------------------------------*/
/* Sections                                                             */
/*----------------------------------------------------------------------*/

SECTIONS
{
    . = 0x0;
    M0M1 : 
    {
    __global_sp$ = . + 0xFF8;
    } > RAMM0M1
	   
	/* data segment */
	. = 0x00010000;
	.sdata  0x00010000  : AT( __flash_start )  {
		__data_start = .;
		__global_pointer$ = . + 0x800;
		*(.data)  *(.data.*)
		*(.sdata .sdata.* .gnu.linkonce.s.*)
        *(ramconsts)
	*(.scratchpad)  FILL(0x400)
		__data_end = ALIGN(0x10) ;
	}  > RAMLS0To2
	
	/* bss segment */
	.sbss  ALIGN(0x10) : {
	__bss_start = .;
		*(.sbss .sbss.* .gnu.linkonce.sb.*)
		*(.scommon)
	}  > RAMLS0To2
	.bss  ALIGN(0x10)  : { *(.bss)   *(.bss.*)  *(COMMON)   __bss_end = ALIGN(0x10);} > RAMLS0To2
	
	  . = 0x00016000;
    .L3ToD1 : AT(__l3tod1_flash_start)
    {
    __l3tod1_data_start = . ;
    *(ramfuncs)
	*(.ramfunc)
    __l3tod1_data_end = ALIGN(10) ;
    } > RAMLS3ToD1
	

	. = 0x01000000;
	ramgs0 :AT(__gs0_flash_start)
	{
		__gs0_data_start = .;
		*(ramgs0)
		*(SHARERAMGS0)		
		__gs0_data_end = ALIGN(0x10);
	} > RAMGS0

	. = 0x01002000;
	ramgs1 :AT(__gs1_flash_start)
	{
		__gs1_data_start = .;
		*(ramgs1)
		*(SHARERAMGS1)		
		__gs1_data_end = ALIGN(0x10);
	} > RAMGS1
	
		. = 0x01004000;
	ramgs2 :AT(__gs2_flash_start)
	{
		__gs2_data_start = .;
		*(ramgs2)
		*(SHARERAMGS2)		
		__gs2_data_end = ALIGN(0x10);
	} > RAMGS2
	
		. = 0x01006000;
	ramgs3 :AT(__gs3_flash_start)
	{
		__gs3_data_start = .;
		*(ramgs3)
		*(SHARERAMGS3)		
		__gs3_data_end = ALIGN(0x10);
	} > RAMGS3
	
		. = 0x01008000;
	ramgs4 :AT(__gs4_flash_start)
	{
		__gs4_data_start = .;
		*(ramgs4)
		*(SHARERAMGS4)		
		__gs4_data_end = ALIGN(0x10);
	} > RAMGS4
	
		. = 0x0100A000;
	ramgs5 :AT(__gs5_flash_start)
	{
		__gs5_data_start = .;
		*(ramgs5)
		*(SHARERAMGS5)		
		__gs5_data_end = ALIGN(0x10);
	} > RAMGS5
	
		. = 0x0100C000;
	ramgs6 :AT(__gs6_flash_start)
	{
		__gs6_data_start = .;
		*(ramgs6)
		*(SHARERAMGS6)		
		__gs6_data_end = ALIGN(0x10);
	} > RAMGS6
	
		. = 0x0100E000;
	ramgs7 :AT(__gs7_flash_start)
	{
		__gs7_data_start = .;
		*(ramgs7)
		*(SHARERAMGS7)		
		__gs7_data_end = ALIGN(0x10);
	} > RAMGS7
	
		. = 0x01010000;
	ramgs8 :AT(__gs8_flash_start)
	{
		__gs8_data_start = .;
		*(ramgs8)
		*(SHARERAMGS8)		
		__gs8_data_end = ALIGN(0x10);
	} > RAMGS8
	
		. = 0x01012000;
	ramgs9 :AT(__gs9_flash_start)
	{
		__gs9_data_start = .;
		*(ramgs9)
		*(SHARERAMGS9)		
		__gs9_data_end = ALIGN(0x10);
	} > RAMGS9
	
		. = 0x01014000;
	ramgs10 :AT(__gs10_flash_start)
	{
		__gs10_data_start = .;
		*(ramgs10)
		*(SHARERAMGS10)		
		__gs10_data_end = ALIGN(0x10);
	} > RAMGS10
	
		. = 0x01016000;
	ramgs11 :AT(__gs11_flash_start)
	{
		__gs11_data_start = .;
		*(ramgs11)
		*(SHARERAMGS11)		
		__gs11_data_end = ALIGN(0x10);
	} > RAMGS11
	
		. = 0x01018000;
	ramgs12 :AT(__gs12_flash_start)
	{
		__gs12_data_start = .;
		*(ramgs12)
		*(SHARERAMGS12)		
		__gs12_data_end = ALIGN(0x10);
	} > RAMGS12
	
		. = 0x0101A000;
	ramgs13 :AT(__gs13_flash_start)
	{
		__gs13_data_start = .;
		*(ramgs13)
		*(SHARERAMGS13)		
		__gs13_data_end = ALIGN(0x10);
	} > RAMGS13
	
		. = 0x0101C000;
	ramgs14 :AT(__gs14_flash_start)
	{
		__gs14_data_start = .;
		*(ramgs14)
		*(SHARERAMGS14)		
		__gs14_data_end = ALIGN(0x10);
	} > RAMGS14
	
		. = 0x0101E000;
	ramgs15 :AT(__gs15_flash_start)
	{
		__gs15_data_start = .;
		*(ramgs15)
		*(SHARERAMGS15)		
		__gs15_data_end = ALIGN(0x10);
	} > RAMGS15
	
	    . = 0x01021000;
    MSGRAM_CPU1_TO_CPU2  :AT(__cpu1_to_cpu2_flash_start)
    { 
       __cpu1_to_cpu2_data_start = .;
       *(MsgRAM_CPU1_TO_CPU2) 
       __cpu1_to_cpu2_data_end = ALIGN(0x10);
    } > CPU1TOCPU2RAM
    
     . = 0x1021800;
    MSGRAM_CPU2_TO_CPU1 :
    { 
       *(MsgRAM_CPU2_TO_CPU1) 
    } > CPU2TOCPU1RAM
  
	. = 0x400C10;
    .IER_REGISTER(NOLOAD) : {*(.ier_register)} > IER_REGISTER_FILE
    .IFR_REGISTER(NOLOAD) : {*(.ifr_register)} > IFR_REGISTER_FILE
    
    . = 0x00600000;
	.codestart : { *(codestart) } > BEGIN
	
	/* text: test code section */
	. = 0x600010;
	.text.init  : { *(.text.init)  }  >  FLASH
	.text  ALIGN(0x10) : 
	{ 
		*(.text)   
		*(.IQmath.*)  
		*(.text*)  
		*(.rodata .rodata.*)
		*(.srodata.cst16) *(.srodata.cst8) *(.srodata.cst4) *(.srodata.cst2) *(.srodata*) 
	} > FLASH

	.FLASH_DATA_START   ALIGN(0x10) :{ __flash_start   =  . ; } > FLASH
	.FLASH_DATA_SECTION ALIGN(0x10) : { __l3tod1_flash_start =  __flash_start + ( __data_end - __data_start ) ; } > FLASH 
	.FLASH_RAMFUNCS_SECTION ALIGN(0x10) : { __gs0_flash_start =  __l3tod1_flash_start + ( __l3tod1_data_end - __l3tod1_data_start ) ; } > FLASH 	
	.FLASH_GS0_SECTION  ALIGN(0x10) : { __gs1_flash_start = __gs0_flash_start + (__gs0_data_end - __gs0_data_start ) ; } > FLASH
	.FLASH_GS1_SECTION  ALIGN(0x10) : { __gs2_flash_start = __gs1_flash_start + (__gs1_data_end - __gs1_data_start ) ; } > FLASH
	.FLASH_GS2_SECTION  ALIGN(0x10) : { __gs3_flash_start = __gs2_flash_start + (__gs2_data_end - __gs2_data_start ) ; } > FLASH
	.FLASH_GS3_SECTION  ALIGN(0x10) : { __gs4_flash_start = __gs3_flash_start + (__gs3_data_end - __gs3_data_start ) ; } > FLASH
	.FLASH_GS4_SECTION  ALIGN(0x10) : { __gs5_flash_start = __gs4_flash_start + (__gs4_data_end - __gs4_data_start ) ; } > FLASH
	.FLASH_GS5_SECTION  ALIGN(0x10) : { __gs6_flash_start = __gs5_flash_start + (__gs5_data_end - __gs5_data_start ) ; } > FLASH
	.FLASH_GS6_SECTION  ALIGN(0x10) : { __gs7_flash_start = __gs6_flash_start + (__gs6_data_end - __gs6_data_start ) ; } > FLASH
	.FLASH_GS7_SECTION  ALIGN(0x10) : { __gs8_flash_start = __gs7_flash_start + (__gs7_data_end - __gs7_data_start ) ; } > FLASH
	.FLASH_GS8_SECTION  ALIGN(0x10) : { __gs9_flash_start = __gs8_flash_start + (__gs8_data_end - __gs8_data_start ) ; } > FLASH
	.FLASH_GS9_SECTION  ALIGN(0x10) : { __gs10_flash_start = __gs9_flash_start + (__gs9_data_end - __gs9_data_start ) ; } > FLASH
	.FLASH_GS10_SECTION  ALIGN(0x10) : { __gs11_flash_start = __gs10_flash_start + (__gs10_data_end - __gs10_data_start ) ; } > FLASH
	.FLASH_GS11_SECTION  ALIGN(0x10) : { __gs12_flash_start = __gs11_flash_start + (__gs11_data_end - __gs11_data_start ) ; } > FLASH
	.FLASH_GS12_SECTION  ALIGN(0x10) : { __gs13_flash_start = __gs12_flash_start + (__gs12_data_end - __gs12_data_start ) ; } > FLASH
	.FLASH_GS13_SECTION  ALIGN(0x10) : { __gs14_flash_start = __gs13_flash_start + (__gs13_data_end - __gs13_data_start ) ; } > FLASH
	.FLASH_GS14_SECTION  ALIGN(0x10) : { __gs15_flash_start = __gs14_flash_start + (__gs14_data_end - __gs14_data_start ) ; } > FLASH
	.FLASH_GS15_SECTION  ALIGN(0x10) : { __cpu1_to_cpu2_flash_start = __gs15_flash_start + (__gs15_data_end - __gs15_data_start ) ; } > FLASH
	.FLASH_CPU1TOCPU2_SECTION  ALIGN(0x10) : { __cpu1_to_cpu2_flash_end = __cpu1_to_cpu2_flash_start + (__cpu1_to_cpu2_data_end - __cpu1_to_cpu2_data_start ) ; } > FLASH  
  
	/* End of uninitalized data segement */
	_end = .;
}

