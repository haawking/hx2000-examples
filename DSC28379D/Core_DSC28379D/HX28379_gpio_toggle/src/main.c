/******************************************************************
 文 档 名：      gpio toggle
 开 发 环 境：   Haawking IDE V2.2.7
 开 发 板：       Core_DSC28379_V1.0
 D S P：          DSC28379
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：
	 该例程实现了对gpio 28的两次翻转


 连接方式：
	无

 现象：


 版 本：      V1.0.0
 时 间：      2024年06月12日
 作 者：      duohan
 @ mail：    support@mail.haawking.com
 *******************************************************************************/
/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"


int main(void)
{
	Uint32 flag1,flag2,flag3 = 0;

    Device_init();

	EALLOW;
	GPIO_setPinConfig(GPIO_28_GPIO28);
	GPIO_setPadConfig(28,GPIO_PIN_TYPE_STD);
	GPIO_setQualificationMode(28,GPIO_QUAL_SYNC);
	GPIO_setDirectionMode(28,GPIO_DIR_MODE_OUT);
	EDIS;

	flag1 = GPIO_readPin(28);

	GPIO_togglePin(28);
	NOP;
	NOP;
	NOP;

	flag2 = GPIO_readPin(28);

	GPIO_togglePin(28);
	NOP;
	NOP;
	NOP;

	flag3 = GPIO_readPin(28);

	if((flag1 != flag2) && (flag2 != flag3))
	{
		// CaseSuccess();
		while(1);
	}
	else
	{
		// CaseFail();
		while(1);
	}
	while(1){};
    return 0;
}

//
// End of File
//

