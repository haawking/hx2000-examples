/******************************************************************
 文 档 名：      i2c_ex1_loopback
 开 发 环 境：   Haawking IDE V2.2.7
 开 发 板：       Core_DSC28379_V1.0
 D S P：          DSC28379
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：
	 本程序使用I2C模块的内部环回测试模式。同时使用TX和RX I2C FIFO及其
	 中断。引脚复用和I2C初始化通过sysconfig文件完成。

	 一串数据被发送，然后与接收到的串进行比较。
	 发送的数据如下：
	  0000 0001
	  0001 0002
	  0002 0003
	  ....
	  00FE 00FF
	  00FF 0000
	  等等..
	 此模式将无限循环。

 连接方式：
	无

 现象：
	观察变量
  	  - sData - 要发送的数据
  	  - rData - 接收到的数据
  	  - rDataPoint - 用于跟踪接收串中的最后一个位置，以进行错误检查

 版 本：      V1.0.0
 时 间：      2024年06月12日
 作 者：      duohan
 @ mail：    support@mail.haawking.com
 *******************************************************************************/
/*
 * Copyright (c) 2019-2023 Beijing Haawking Technology Co.,Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Junning Wu
 * Email  : junning.wu@mail.haawking.com
 * FILE   : main.c
 *****************************************************************/
 
#include <syscalls.h>
#include "IQmathLib.h"
#include "hx_intrinsics.h"
#include "driverlib.h"
#include "device.h"
#include "hx_fintdiv.h"
#include "board.h"

//
// Defines
//
#define SLAVE_ADDRESS   0x3C

//
// Globals
//
uint16_t sData[2];                  // Send data buffer
uint16_t rData[2];                  // Receive data buffer
uint16_t rDataPoint = 0;            // To keep track of where we are in the
                                    // data stream to check received data

//
// Function Prototypes
//
__interrupt void i2cFIFOISR(void);

int main(void)
{
    Device_init();
    uint16_t i;

	//
	// Disable pin locks and enable internal pullups.
	//
	Device_initGPIO();

	//
	// Initialize PIE and clear PIE registers. Disables CPU interrupts.
	//
	Interrupt_initModule();

	//
	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	//
	Interrupt_initVectorTable();

	//
	// Board initialization
	//
	Board_init();

	//
	// For loopback mode only
	//
	I2C_setOwnSlaveAddress(myI2C0_BASE, SLAVE_ADDRESS);

	//
	// Interrupts that are used in this example are re-mapped to ISR functions
	// found within this file.
	//
	Interrupt_register(INT_I2CA_FIFO, &i2cFIFOISR);

	//
	// Initialize the data buffers
	//
	for(i = 0; i < 2; i++)
	{
	   sData[i] = i;
	   rData[i]= 0;
	}

	//
	// Enable interrupts required for this example
	//
	Interrupt_enable(INT_I2CA_FIFO);

	//
	// Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
	//
	EINT;
	ERTM;

    while(1);
    return 0;
}


//
// I2C A Transmit & Receive FIFO ISR.
//
 __interrupt void i2cFIFOISR(void)
{
    uint16_t i;

    //
    // If receive FIFO interrupt flag is set, read data
    //
    if((I2C_getInterruptStatus(myI2C0_BASE) & I2C_INT_RXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            rData[i] = I2C_getData(myI2C0_BASE);
        }

        //
        // Check received data
        //
        for(i = 0; i < 2; i++)
        {
            if(rData[i] != ((rDataPoint + i) & 0xFF))
            {
                //
                // Something went wrong. rData doesn't contain expected data.
                //
                while(1);
            }
        }
        // CaseSuccess();
        while(1);
        rDataPoint = (rDataPoint + 1) & 0xFF;

        //
        // Clear interrupt flag
        //
        I2C_clearInterruptStatus(myI2C0_BASE, I2C_INT_RXFF);

    }
    //
    // If transmit FIFO interrupt flag is set, put data in the buffer
    //
    else if((I2C_getInterruptStatus(myI2C0_BASE) & I2C_INT_TXFF) != 0)
    {
        for(i = 0; i < 2; i++)
        {
            I2C_putData(myI2C0_BASE, sData[i]);
        }

        //
        // Send the start condition
        //
        I2C_sendStartCondition(myI2C0_BASE);

        //
        // Increment data for next cycle
        //
        for(i = 0; i < 2; i++)
        {
           sData[i] = (sData[i] + 1) & 0xFF;
        }

        //
        // Clear interrupt flag
        //
        I2C_clearInterruptStatus(myI2C0_BASE, I2C_INT_TXFF);
    }

    //
    // Issue ACK
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}
//
// End of File
//

