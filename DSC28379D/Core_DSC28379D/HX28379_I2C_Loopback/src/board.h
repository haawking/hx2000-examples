/*
 * board.h
 *
 *  Created on: 2024��1��5��
 *      Author: yuetq
 */



//
// Included Files
//

#include "driverlib.h"
#include "device.h"

//*****************************************************************************
//
// PinMux Configurations
//
//*****************************************************************************

//
// I2CA -> myI2C0 Pinmux
//
//
// SDAA - GPIO Settings
//
#define GPIO_PIN_SDAA 42
#define myI2C0_I2CSDA_GPIO 42
#define myI2C0_I2CSDA_PIN_CONFIG GPIO_42_SDAA
//
// SCLA - GPIO Settings
//
#define GPIO_PIN_SCLA 43
#define myI2C0_I2CSCL_GPIO 43
#define myI2C0_I2CSCL_PIN_CONFIG GPIO_43_SCLA

//*****************************************************************************
//
// I2C Configurations
//
//*****************************************************************************
#define myI2C0_BASE I2CA_BASE
#define myI2C0_BITRATE 400000
#define myI2C0_TARGET_ADDRESS 60
#define myI2C0_OWN_TARGET_ADDRESS 0
void myI2C0_init();

//*****************************************************************************
//
// Board Configurations
//
//*****************************************************************************
void	Board_init();
void	I2C_init();
void	PinMux_init();

