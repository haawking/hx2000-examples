
#ifndef HRPWM_H_
#define HRPWM_H_


#include "HXM32G407_config.h"

void HRPWM1_config(uint16 period);
void HRPWM2_config(uint16 period);
void HRPWM3_config(uint16 period);

void INTERRUPT EPWM1_ISR(void);
void INTERRUPT EPWM2_ISR(void);
void INTERRUPT EPWM3_ISR(void);




#endif /* HRPWM_H_ */
