//###########################################################################
//
// FILE:    HXM32G407_OTP.c
//
// TITLE:   HXM32G407 OTP file.
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "HXM32G407_Device.h"     //  Headerfile Include File


/***************************************************************
  To  uncomment ,you can use OTP
*****************************************************************/

/***************************************************************

volatile Uint32   CODE_SECTION(".User_Otp_Table") OTP_DATA[] = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};


*****************************************************************/
