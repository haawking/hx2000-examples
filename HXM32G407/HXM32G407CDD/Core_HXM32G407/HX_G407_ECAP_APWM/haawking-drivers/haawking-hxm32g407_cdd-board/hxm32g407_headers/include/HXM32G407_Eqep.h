//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.304548
//
//#############################################################################


#ifndef HXM32G407_EQEP_H
#define HXM32G407_EQEP_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// EQEP Individual Register Bit Definitions:

struct QDECCTL_BITS {                         // bits description
    Uint32 rsvd1:5;                           // 4:0 Reserved
    Uint32 QSP:1;                             // 5 QEPS Input Polarity
    Uint32 QIP:1;                             // 6 QEPI Input Polarity
    Uint32 QBP:1;                             // 7 QEPB Input Polarity
    Uint32 QAP:1;                             // 8 QEPA Input Polarity
    Uint32 IGATE:1;                           // 9 Index Pulse Gating Option
    Uint32 SWAP:1;                            // 10 CLK/DIR Signal Source for Position
    Uint32 XCR:1;                             // 11 External Clock Rate
    Uint32 SPSEL:1;                           // 12 Sync Output Pin Select
    Uint32 SOEN:1;                            // 13 Enable Position Compare Sync
    Uint32 QSRC:2;                            // 15:14 Postion Counter Source
    Uint32 rsvd2:16;                          // 31:16 Reserved
};

union QDECCTL_REG {
    Uint32  all;
    struct  QDECCTL_BITS  bit;
};

struct QEPCTL_BITS {                          // bits description
    Uint32 WDE:1;                             // 0 QEP watchdog enable
    Uint32 UTE:1;                             // 1 QEP unit timer enable
    Uint32 QCLM:1;                            // 2 QEP capture latch mode
    Uint32 QPEN:1;                            // 3 Quadrature postotion counter enable
    Uint32 IEL:2;                             // 5:4 Index event latch
    Uint32 SEL:1;                             // 6 Strobe event latch
    Uint32 SWI:1;                             // 7 Software init position counter
    Uint32 IEI:2;                             // 9:8 Index event init of position count
    Uint32 SEI:2;                             // 11:10 Strobe event init
    Uint32 PCRM:2;                            // 13:12 Postion counter reset
    Uint32 FREE_SOFT:2;                       // 15:14 Emulation mode
    Uint32 rsvd1:16;                          // 31:16 Reserved
};

union QEPCTL_REG {
    Uint32  all;
    struct  QEPCTL_BITS  bit;
};

struct QCAPCTL_BITS {                         // bits description
    Uint32 UPPS:4;                            // 3:0 Unit position pre-scale
    Uint32 CCPS:3;                            // 6:4 QEP capture timer pre-scale
    Uint32 rsvd1:8;                           // 14:7 Reserved
    Uint32 CEN:1;                             // 15 Enable QEP capture
    Uint32 rsvd2:16;                          // 31:16 Reserved
};

union QCAPCTL_REG {
    Uint32  all;
    struct  QCAPCTL_BITS  bit;
};

struct QPOSCTL_BITS {                         // bits description
    Uint32 PCSPW:12;                          // 11:0 Position compare sync pulse width
    Uint32 PCE:1;                             // 12 Position compare enable/disable
    Uint32 PCPOL:1;                           // 13 Polarity of sync output
    Uint32 PCLOAD:1;                          // 14 Position compare of shadow load
    Uint32 PCSHDW:1;                          // 15 Position compare of shadow enable
    Uint32 rsvd1:16;                          // 31:16 Reserved
};

union QPOSCTL_REG {
    Uint32  all;
    struct  QPOSCTL_BITS  bit;
};

struct QEINT_BITS {                           // bits description
    Uint32 rsvd1:1;                           // 0 Reserved
    Uint32 PCE:1;                             // 1 Position counter error
    Uint32 QPE:1;                             // 2 Quadrature phase error //phe in verilog
    Uint32 QDC:1;                             // 3 Quadrature dir change
    Uint32 WTO:1;                             // 4 Watchdog timeout
    Uint32 PCU:1;                             // 5 Position counter underflow
    Uint32 PCO:1;                             // 6 Position counter overflow
    Uint32 PCR:1;                             // 7 Position compare ready
    Uint32 PCM:1;                             // 8 Position compare match
    Uint32 SEL:1;                             // 9 Strobe event latch
    Uint32 IEL:1;                             // 10 Event latch
    Uint32 UTO:1;                             // 11 Unit timeout
    Uint32 rsvd2:20;                          // 31:12 Reserved
};

union QEINT_REG {
    Uint32  all;
    struct  QEINT_BITS  bit;
};

struct QFLG_BITS {                            // bits description
    Uint32 INT:1;                             // 0 Global.interrupt
    Uint32 PCE:1;                             // 1 Position counter error
    Uint32 PHE:1;                             // 2 Quadrature phase error
    Uint32 QDC:1;                             // 3 Quadrature dir change
    Uint32 WTO:1;                             // 4 Watchdog timeout
    Uint32 PCU:1;                             // 5 Position counter underflow
    Uint32 PCO:1;                             // 6 Position counter overflow
    Uint32 PCR:1;                             // 7 Position compare ready
    Uint32 PCM:1;                             // 8 Position compare match
    Uint32 SEL:1;                             // 9 Strobe event latch
    Uint32 IEL:1;                             // 10 Event latch
    Uint32 UTO:1;                             // 11 Unit timeout
    Uint32 rsvd1:20;                          // 31:12 Reserved
};

union QFLG_REG {
    Uint32  all;
    struct  QFLG_BITS  bit;
};

struct QCLR_BITS {                            // bits description
    Uint32 INT:1;                             // 0 Global.interrupt
    Uint32 PCE:1;                             // 1 Position counter error
    Uint32 PHE:1;                             // 2 Quadrature phase error
    Uint32 QDC:1;                             // 3 Quadrature dir change
    Uint32 WTO:1;                             // 4 Watchdog timeout
    Uint32 PCU:1;                             // 5 Position counter underflow
    Uint32 PCO:1;                             // 6 Position counter overflow
    Uint32 PCR:1;                             // 7 Position compare ready
    Uint32 PCM:1;                             // 8 Position compare match
    Uint32 SEL:1;                             // 9 Strobe event latch
    Uint32 IEL:1;                             // 10 Event latch
    Uint32 UTO:1;                             // 11 Unit timeout
    Uint32 rsvd1:20;                          // 31:12 Reserved
};

union QCLR_REG {
    Uint32  all;
    struct  QCLR_BITS  bit;
};

struct QFRC_BITS {                            // bits description
    Uint32 rsvd1:1;                           // 0 Reserved
    Uint32 PCE:1;                             // 1 Position counter error
    Uint32 PHE:1;                             // 2 Quadrature phase error
    Uint32 QDC:1;                             // 3 Quadrature dir change
    Uint32 WTO:1;                             // 4 Watchdog timeout
    Uint32 PCU:1;                             // 5 Position counter underflow
    Uint32 PCO:1;                             // 6 Position counter overflow
    Uint32 PCR:1;                             // 7 Position compare ready
    Uint32 PCM:1;                             // 8 Position compare match
    Uint32 SEL:1;                             // 9 Strobe event latch
    Uint32 IEL:1;                             // 10 Event latch
    Uint32 UTO:1;                             // 11 Unit timeout
    Uint32 rsvd2:20;                          // 31:12 Reserved
};

union QFRC_REG {
    Uint32  all;
    struct  QFRC_BITS  bit;
};

struct QEPSTS_BITS {                          // bits description
    Uint32 PCEF:1;                            // 0 Position counter error
    Uint32 FIMF:1;                            // 1 First index marker
    Uint32 CDEF:1;                            // 2 Capture direction error
    Uint32 COEF:1;                            // 3 Capture overflow error
    Uint32 QDLF:1;                            // 4 QEP direction latch
    Uint32 QDF:1;                             // 5 Quadrature direction
    Uint32 FIDF:1;                            // 6 Direction on first index marker
    Uint32 UPEVNT:1;                          // 7 Unit position event flag
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union QEPSTS_REG {
    Uint32  all;
    struct  QEPSTS_BITS  bit;
};

struct  EQEP_REGS {
    Uint32                                   QPOSCNT;                     // 0x0 Position Counter
    Uint32                                   QPOSINIT;                    // 0x4 Position Counter Init
    Uint32                                   QPOSMAX;                     // 0x8 Maximum Position Count
    Uint32                                   QPOSCMP;                     // 0xc Position Compare
    Uint32                                   QPOSILAT;                    // 0x10 Index Position Latch
    Uint32                                   QPOSSLAT;                    // 0x14 Strobe Position Latch
    Uint32                                   QPOSLAT;                     // 0x18 Position Latch
    Uint32                                   QUTMR;                       // 0x1c QEP Unit Timer
    Uint32                                   QUPRD;                       // 0x20 QEP Unit Period
    Uint32                                   QWDTMR;                      // 0x24 QEP Watchdog Timer
    Uint32                                   QWDPRD;                      // 0x28 QEP Watchdog Period
    union   QDECCTL_REG                      QDECCTL;                     // 0x2c Quadrature Decoder Control
    union   QEPCTL_REG                       QEPCTL;                      // 0x30 QEP Control
    union   QCAPCTL_REG                      QCAPCTL;                     // 0x34 Qaudrature Capture Control
    union   QPOSCTL_REG                      QPOSCTL;                     // 0x38 Position Compare Control
    union   QEINT_REG                        QEINT;                       // 0x3c QEP Interrupt Control
    union   QFLG_REG                         QFLG;                        // 0x40 QEP Interrupt Flag
    union   QCLR_REG                         QCLR;                        // 0x44 QEP Interrupt Clear //only get 0 in verilog ??
    union   QFRC_REG                         QFRC;                        // 0x48 QEP Interrupt Force //only get 0 in verilog ??
    union   QEPSTS_REG                       QEPSTS;                      // 0x4c QEP Status
    Uint32                                   QCTMR;                       // 0x50 QEP Capture Timer
    Uint32                                   QCPRD;                       // 0x54 QEP Capture Period
    Uint32                                   QCTMRLAT;                    // 0x58 QEP Capture Latch
    Uint32                                   QCPRDLAT;                    // 0x5c QEP Capture Period Latch
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================