#ifndef XINT_H_
#define XINT_H_

#include "HXM32G407_config.h"

void XINT2_Init(void);
void INTERRUPT xint2_isr(void);

#endif/*ECAP_H_*/
