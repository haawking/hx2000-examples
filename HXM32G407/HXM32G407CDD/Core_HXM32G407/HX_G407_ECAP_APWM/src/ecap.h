#ifndef ECAP_H_
#define ECAP_H_

#include "HXM32G407_config.h"

void ECap_Gpio(void);
void ECap_Init(void);
void INTERRUPT ecap_isr(void);

void InitLED(void);

#endif/*ECAP_H_*/
