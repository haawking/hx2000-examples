/******************************************************************
 文 档 名：      HX_HXM32G407_GPIO
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：   芯片主频120MHz

 连接方式：   LED1 ----- GPIO44/GPIO43
                      KEY  ----- GPIO3/GPIO27

 现象：         当检测到KEY/S100按下时，LED1常亮

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
/******************************************************************
 函数名：void  InitKEY(void)
 参	数：无
 返回值：无
 作	用：配置GPIO3 GPIO27为输入模式，上拉禁止
 ******************************************************************/

void InitKEY(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO3 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO3 = 1;

	GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO27 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO27 = 1;
	EDIS;
}

/******************************************************************
 函数名：void  InitLED(void)
 参	数：无
 返回值：无
 作	用：配置GPIO44为输出模式
 ******************************************************************/
void InitLED(void)
{
	EALLOW;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0; /* 普通IO,对应D402，LED灯 */
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1; /* 输出IO */

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	EDIS;

}

int main(void)
{


	InitSysCtrl();/*将PLL配置成10倍频1分频，配置系统时钟为120M */

	InitKEY();

	InitLED();

	while (1)
	{

		if (GpioDataRegs.GPADAT.bit.GPIO3 == 0)

		{
			GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;

		}
		else if(GpioDataRegs.GPADAT.bit.GPIO27 == 0)
		{
			GpioDataRegs.GPBCLEAR.bit.GPIO43 = 1;
		}
		else
		{
			GpioDataRegs.GPBSET.bit.GPIO44 = 1;
			GpioDataRegs.GPBSET.bit.GPIO43 = 1;
		}

	}

	return 0;
}


//  ----------------------------------------------------------------------------
