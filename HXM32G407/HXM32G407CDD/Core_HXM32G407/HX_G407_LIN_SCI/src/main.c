/******************************************************************
 文 档 名：     HX_HXM32G407_LIN_LIN
 开 发 环 境：Haawking IDE V2.1.3
 开 发 板 ：   Start_HXM32G407_V1.2
 D S P：        HXM32G407
 使 用 库：    无
 作     用：     通过对LIN_SCI配置，模拟串口功能实现SCI通信与中断
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：LIN-SCI mode

 连接方式：LRX/GPIO23接CH340 TX,LTX/GPIO22接CH340 RX，Baud 9600bps ,SCI连接上位机观察收发数据

 现象：

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "system.h"

Uint8 Byte_Tx=0;
Uint32 LinL0IntCount;
Uint32 LinL1IntCount;
Uint32 LinL0IntVect;
Uint32 LinL1IntVect;
Uint16 sdataA[4];
Uint16 rdataA[4];
Uint16 i;

int main(void)
{
	/*初始化系统控制*/
	InitSysCtrl();

	/*lin_sci的Gpio引脚配置*/
	Initlin_scia_gpio();
	/*用于显示接收与发送状态*/
	InitLED();
	/*关中断*/
	DINT;

	InitPieCtrl();
	/*清中断*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE中断向量表*/
	InitPieVectTable();

	EALLOW;
	/*将LIN0INTA的中断入口地址赋值给Lina_Levl0_ISR，执行接收中断*/
	PieVectTable.LIN0INTA = &Lina_Level0_ISR;
	/*将LIN0INTA的中断入口地址赋值给Lina_Levl0_ISR，执行发送中断*/
	PieVectTable.LIN1INTA = &Lina_Level1_ISR;
	EDIS;

	/*Lin_sci功能配置*/
	lin_scia_init();

	/*使能打开对应的CPU IER中断*/
	IER |= M_INT9;    //IER=0x100
	/*使能打开对应的PIE IER中断*/
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
	PieCtrlRegs.PIEIER9.bit.INTx3 = 1;
	PieCtrlRegs.PIEIER9.bit.INTx4 = 1;

	/*打开全局中断*/
	EINT;

	for (i = 0; i < 4; i++) {
		sdataA[i] = i;
	}

	while (1) {

		LinaRegs.SCITD = Byte_Tx++;
		DELAY_US(5000);

		if (LinL0IntVect == 11) {
			GpioDataRegs.GPBTOGGLE.bit.GPIO41 = 1;
		}
		if (LinL1IntVect == 12) {
			GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;
		}
	}

	return 0;
}

// ----------------------------------------------------------------------------
