//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.256925
//
//#############################################################################


#ifndef HXM32G407_ECAP_H
#define HXM32G407_ECAP_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// ECAP Individual Register Bit Definitions:

struct ECCTL1_BITS {                          // bits description
    Uint32 CAP1POL:1;                         // 0 Capture Event 1 Polarity select
    Uint32 CTRRST1:1;                         // 1 Counter Reset on Capture Event 1
    Uint32 CAP2POL:1;                         // 2 Capture Event 2 Polarity select
    Uint32 CTRRST2:1;                         // 3 Counter Reset on Capture Event 2
    Uint32 CAP3POL:1;                         // 4 Capture Event 3 Polarity select
    Uint32 CTRRST3:1;                         // 5 Counter Reset on Capture Event 3
    Uint32 CAP4POL:1;                         // 6 Capture Event 4 Polarity select
    Uint32 CTRRST4:1;                         // 7 Counter Reset on Capture Event 4
    Uint32 CAPLDEN:1;                         // 8 Enable Loading CAP1-4 regs on a Cap Event
    Uint32 PRESCALE:5;                        // 13:9 Event Filter prescale select
    Uint32 FREE_SOFT:2;                       // 15:14 Emulation mode
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ECCTL1_REG {
    Uint32  all;
    struct  ECCTL1_BITS  bit;
};

struct ECCTL2_BITS {                          // bits description
    Uint32 CONT_ONESHT:1;                     // 0 Continuous or one-shot
    Uint32 STOP_WRAP:2;                       // 2:1 Stop value for one-shot, Wrap for continuous
    Uint32 REARM:1;                           // 3 One-shot re-arm
    Uint32 TSCTRSTOP:1;                       // 4 TSCNT counter stop
    Uint32 SYNCI_EN:1;                        // 5 Counter sync-in select
    Uint32 SYNCO_SEL:2;                       // 7:6 Sync-out mode
    Uint32 SWSYNC:1;                          // 8 SW forced counter sync
    Uint32 CAP_APWM:1;                        // 9 CAP/APWM operating mode select
    Uint32 APWMPOL:1;                         // 10 APWM output polarity select
    Uint32 rsvd1:5;                           // 15:11  
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ECCTL2_REG {
    Uint32  all;
    struct  ECCTL2_BITS  bit;
};

struct ECEINT_BITS {                          // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 CEVT1:1;                           // 1 Capture Event 1 Interrupt Enable
    Uint32 CEVT2:1;                           // 2 Capture Event 2 Interrupt Enable
    Uint32 CEVT3:1;                           // 3 Capture Event 3 Interrupt Enable
    Uint32 CEVT4:1;                           // 4 Capture Event 4 Interrupt Enable
    Uint32 CTROVF:1;                          // 5 Counter Overflow Interrupt Enable
    Uint32 CTR_EQ_PRD:1;                      // 6 Period Equal Interrupt Enable
    Uint32 CTR_EQ_CMP:1;                      // 7 Compare Equal Interrupt Enable
    Uint32 rsvd2:8;                           // 15:8 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union ECEINT_REG {
    Uint32  all;
    struct  ECEINT_BITS  bit;
};

struct ECFLG_BITS {                           // bits description
    Uint32 INT:1;                             // 0 Global Flag
    Uint32 CEVT1:1;                           // 1 Capture Event 1 Interrupt Flag
    Uint32 CEVT2:1;                           // 2 Capture Event 2 Interrupt Flag
    Uint32 CEVT3:1;                           // 3 Capture Event 3 Interrupt Flag
    Uint32 CEVT4:1;                           // 4 Capture Event 4 Interrupt Flag
    Uint32 CTROVF:1;                          // 5 Counter Overflow Interrupt Flag
    Uint32 CTR_EQ_PRD:1;                      // 6 Period Equal Interrupt Flag
    Uint32 CTR_EQ_CMP:1;                      // 7 Compare Equal Interrupt Flag
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ECFLG_REG {
    Uint32  all;
    struct  ECFLG_BITS  bit;
};

struct ECCLR_BITS {                           // bits description
    Uint32 INT:1;                             // 0 Global Flag
    Uint32 CEVT1:1;                           // 1 Capture Event 1 Interrupt Flag
    Uint32 CEVT2:1;                           // 2 Capture Event 2 Interrupt Flag
    Uint32 CEVT3:1;                           // 3 Capture Event 3 Interrupt Flag
    Uint32 CEVT4:1;                           // 4 Capture Event 4 Interrupt Flag
    Uint32 CTROVF:1;                          // 5 Counter Overflow Interrupt Flag
    Uint32 CTR_EQ_PRD:1;                      // 6 Period Equal Interrupt Flag
    Uint32 CTR_EQ_CMP:1;                      // 7 Compare Equal Interrupt Flag
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ECCLR_REG {
    Uint32  all;
    struct  ECCLR_BITS  bit;
};

struct ECFRC_BITS {                           // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 CEVT1:1;                           // 1 Capture Event 1 Interrupt Enable
    Uint32 CEVT2:1;                           // 2 Capture Event 2 Interrupt Enable
    Uint32 CEVT3:1;                           // 3 Capture Event 3 Interrupt Enable
    Uint32 CEVT4:1;                           // 4 Capture Event 4 Interrupt Enable
    Uint32 CTROVF:1;                          // 5 Counter Overflow Interrupt Enable
    Uint32 CTR_EQ_PRD:1;                      // 6 Period Equal Interrupt Enable
    Uint32 CTR_EQ_CMP:1;                      // 7 Compare Equal Interrupt Enable
    Uint32 rsvd2:8;                           // 15:8 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union ECFRC_REG {
    Uint32  all;
    struct  ECFRC_BITS  bit;
};

struct  ECAP_REGS {
    Uint32                                   TSCTR;                       // 0x0 Time stamp counter
    Uint32                                   CTRPHS;                      // 0x4 Counter phase
    Uint32                                   CAP1;                        // 0x8 Capture 1
    Uint32                                   CAP2;                        // 0xc Capture 2
    Uint32                                   CAP3;                        // 0x10 Capture 3
    Uint32                                   CAP4;                        // 0x14 Capture 4
    union   ECCTL1_REG                       ECCTL1;                      // 0x18 Capture Control Reg 1
    union   ECCTL2_REG                       ECCTL2;                      // 0x1c Capture Control Reg 2
    union   ECEINT_REG                       ECEINT;                      // 0x20 ECAP interrupt enable
    union   ECFLG_REG                        ECFLG;                       // 0x24 ECAP interrupt flags
    union   ECCLR_REG                        ECCLR;                       // 0x28 ECAP interrupt clear
    union   ECFRC_REG                        ECFRC;                       // 0x2c ECAP interrupt force
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
