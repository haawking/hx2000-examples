#ifndef SPI_H_
#define SPI_H_

#include  "HXM32G407_config.h"

void spi_fifo_init(void);
INTERRUPT void spiTxFifoIsr(void);
INTERRUPT void spiRxFifoIsr(void);
void InitLED(void);

extern Uint16 sdata;
extern Uint16 rdata;
extern Uint16 rdata_point;

#endif /* SPI_H_ */
