//###########################################################################
//
// FILE:    usDelay.S
//
// TITLE:   _HXM32G407_usDelay Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


.section  ramfuncs, "ax", @progbits

.global	 _HXM32G407_usDelay


_HXM32G407_usDelay:

.align 1
	addi  sp,sp,-20
	sw    a1,16(sp) 
	csrr  a1,0x7c0
	sw    a1,12(sp)
	csrr  a1,mstatus
	
.align 2
	rpt a0,4
	nop
   
	csrw  mstatus,a1
	lw    a1,12(sp)
	csrw  0x7c0,a1
	lw    a1,16(sp)
	addi  sp,sp,20

	ret
	
.size  _HXM32G407_usDelay,   .-_HXM32G407_usDelay
