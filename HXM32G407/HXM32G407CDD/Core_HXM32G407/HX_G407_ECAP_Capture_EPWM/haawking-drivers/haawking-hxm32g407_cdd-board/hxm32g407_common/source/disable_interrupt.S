//###########################################################################
//
// FILE:    disable_interrupt.S
//
// TITLE:   disable_interrupt Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 disable_interrupt


disable_interrupt:
    li a0, 0x0
    csrw mstatus, a0
	ret


	

