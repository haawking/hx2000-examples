#ifndef EPWM_H_
#define EPWM_H_

#include "HXM32G407_config.h"

extern uint32_t EPwm3TimerDirection;
extern uint32_t ECap1IntCount;
extern uint32_t ECap1PassCount;
extern void InitEPwmTimer(void);
void INTERRUPT ecap1_isr(void);

#endif/*EPWM_H_*/
