#ifndef ECAP_H_
#define ECAP_H_

#include "HXM32G407_config.h"

extern uint32_t ECap1IntCount;
extern uint32_t ECap1PassCount;
extern void InitECapture(void);
extern void InitLED(void);
void INTERRUPT ecap1_isr(void);

#endif/*ECAP_H_*/
