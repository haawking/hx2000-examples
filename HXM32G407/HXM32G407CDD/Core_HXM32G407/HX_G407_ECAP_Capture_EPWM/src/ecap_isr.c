#include "epwm.h"

#define PWM3_TIMER_MIN 10
#define PWM3_TIMER_MAX 80

uint32_t ECap1IntCount;
uint32_t ECap1PassCount;

#define EPwm_TIMER_UP 1
#define EPwm_TIMER_DOWN 0

void Fail();
/******************************************************************
 *函数名：void INTERRUPT ecap1_isr(void)
 *参 数：无
 *返回值：无
 *作 用：ecap1中断服务函数改变pwm计数周期
 ******************************************************************/
void INTERRUPT ecap1_isr(void)
{
	if(ECap1Regs.CAP2 > EPwm3Regs.TBPRD * 2 + 1 || ECap1Regs.CAP2 < EPwm3Regs.TBPRD * 2 - 1)
	{
		Fail();
	}

	if(ECap1Regs.CAP3 > EPwm3Regs.TBPRD * 2 + 1 || ECap1Regs.CAP3 < EPwm3Regs.TBPRD * 2 - 1)
	{
		Fail();
	}

	if(ECap1Regs.CAP4 > EPwm3Regs.TBPRD * 2 + 1 || ECap1Regs.CAP4 < EPwm3Regs.TBPRD * 2 - 1)
	{
		Fail();
	}

	ECap1IntCount++;

	if(EPwm3TimerDirection == EPwm_TIMER_UP)
	{
		if(EPwm3Regs.TBPRD < PWM3_TIMER_MAX)
		{
			EPwm3Regs.TBPRD++;
			GpioDataRegs.GPBTOGGLE.bit.GPIO41 = 1;
			DELAY_US(10000000);
		}
		else
		{
			EPwm3TimerDirection = EPwm_TIMER_DOWN;
			EPwm3Regs.TBPRD--;
			GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;
			DELAY_US(10000000);
		}
	}
	else
	{
		if(EPwm3Regs.TBPRD > PWM3_TIMER_MIN)
		{
			EPwm3Regs.TBPRD--;
			GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;
			DELAY_US(10000000);
		}
		else
		{
			EPwm3TimerDirection = EPwm_TIMER_UP;
			EPwm3Regs.TBPRD++;
			GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;
			DELAY_US(10000000);
		}
	}

	ECap1PassCount++;

	ECap1Regs.ECCLR.bit.CEVT4 = 1;
	ECap1Regs.ECCLR.bit.INT = 1;
	ECap1Regs.ECCTL2.bit.REARM = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP4;
}

void Fail()
{
	__asm(" NOP");
}
