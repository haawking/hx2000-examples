//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.328973
//
//#############################################################################


#ifndef HXM32G407_GPIO_H
#define HXM32G407_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// GPIO Individual Register Bit Definitions:

struct GPAMUX1_BITS {                         // bits description
    Uint32 GPIO0:2;                           // 1:0 GPIO0
    Uint32 GPIO1:2;                           // 3:2 GPIO1
    Uint32 GPIO2:2;                           // 5:4 GPIO2
    Uint32 GPIO3:2;                           // 7:6 GPIO3
    Uint32 GPIO4:2;                           // 9:8 GPIO4
    Uint32 GPIO5:2;                           // 11:10 GPIO5
    Uint32 GPIO6:2;                           // 13:12 GPIO6
    Uint32 GPIO7:2;                           // 15:14 GPIO7
    Uint32 GPIO8:2;                           // 17:16 GPIO8
    Uint32 GPIO9:2;                           // 19:18 GPIO9
    Uint32 GPIO10:2;                          // 21:20 GPIO10
    Uint32 GPIO11:2;                          // 23:22 GPIO11
    Uint32 GPIO12:2;                          // 25:24 GPIO12
    Uint32 GPIO13:2;                          // 27:26 GPIO13
    Uint32 GPIO14:2;                          // 29:28 GPIO14
    Uint32 GPIO15:2;                          // 31:30 GPIO15
};

union GPAMUX1_REG {
    Uint32  all;
    struct  GPAMUX1_BITS  bit;
};

struct GPAMUX2_BITS {                         // bits description
    Uint32 GPIO16:2;                          // 1:0 GPIO16
    Uint32 GPIO17:2;                          // 3:2 GPIO17
    Uint32 GPIO18:2;                          // 5:4 GPIO18
    Uint32 GPIO19:2;                          // 7:6 GPIO19
    Uint32 GPIO20:2;                          // 9:8 GPIO20
    Uint32 GPIO21:2;                          // 11:10 GPIO21
    Uint32 GPIO22:2;                          // 13:12 GPIO22
    Uint32 GPIO23:2;                          // 15:14 GPIO23
    Uint32 GPIO24:2;                          // 17:16 GPIO24
    Uint32 GPIO25:2;                          // 19:18 GPIO25
    Uint32 GPIO26:2;                          // 21:20 GPIO26
    Uint32 GPIO27:2;                          // 23:22 GPIO27
    Uint32 GPIO28:2;                          // 25:24 GPIO28
    Uint32 GPIO29:2;                          // 27:26 GPIO29
    Uint32 GPIO30:2;                          // 29:28 GPIO30
    Uint32 GPIO31:2;                          // 31:30 GPIO31
};

union GPAMUX2_REG {
    Uint32  all;
    struct  GPAMUX2_BITS  bit;
};

struct GPBMUX1_BITS {                         // bits description
    Uint32 GPIO32:2;                          // 1:0 GPIO32
    Uint32 GPIO33:2;                          // 3:2 GPIO33
    Uint32 GPIO34:2;                          // 5:4 GPIO34
    Uint32 GPIO35:2;                          // 7:6 GPIO35
    Uint32 GPIO36:2;                          // 9:8 GPIO36
    Uint32 GPIO37:2;                          // 11:10 GPIO37
    Uint32 GPIO38:2;                          // 13:12 GPIO38
    Uint32 GPIO39:2;                          // 15:14 GPIO39
    Uint32 GPIO40:2;                          // 17:16 GPIO40
    Uint32 GPIO41:2;                          // 19:18 GPIO41
    Uint32 GPIO42:2;                          // 21:20 GPIO42
    Uint32 GPIO43:2;                          // 23:22 GPIO43
    Uint32 GPIO44:2;                          // 25:24 GPIO44
    Uint32 rsvd1:6;                           // 31:26 reserved
};

union GPBMUX1_REG {
    Uint32  all;
    struct  GPBMUX1_BITS  bit;
};

struct AIOMUX1_BITS {                         // bits description
    Uint32 rsvd1:2;                           // 1:0 reserved
    Uint32 rsvd2:2;                           // 3:2 reserved
    Uint32 AIO2:2;                            // 5:4 AIO2
    Uint32 rsvd3:2;                           // 7:6 reserved
    Uint32 AIO4:2;                            // 9:8 AIO4
    Uint32 rsvd4:2;                           // 11:10 reserved
    Uint32 AIO6:2;                            // 13:12 AIO6
    Uint32 rsvd5:2;                           // 15:14 reserved
    Uint32 rsvd6:2;                           // 17:16 reserved
    Uint32 rsvd7:2;                           // 19:18 reserved
    Uint32 AIO10:2;                           // 21:20 AIO10
    Uint32 rsvd8:2;                           // 23:22 reserved
    Uint32 AIO12:2;                           // 25:24 AIO12
    Uint32 rsvd9:2;                           // 27:26 reserved
    Uint32 AIO14:2;                           // 29:28 AIO14
    Uint32 rsvd10:2;                          // 31:30 reserved
};

union AIOMUX1_REG {
    Uint32  all;
    struct  AIOMUX1_BITS  bit;
};

struct GPACTRL_BITS {                         // bits description
    Uint32 QUALPRD0:8;                        // 7:0 Qual period
    Uint32 QUALPRD1:8;                        // 15:8 Qual period
    Uint32 QUALPRD2:8;                        // 23:16 Qual period
    Uint32 QUALPRD3:8;                        // 31:24 Qual period
};

union GPACTRL_REG {
    Uint32  all;
    struct  GPACTRL_BITS  bit;
};

struct GPBCTRL_BITS {                         // bits description
    Uint32 QUALPRD0:8;                        // 7:0 Qual period
    Uint32 QUALPRD1:8;                        // 15:8 Qual period
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union GPBCTRL_REG {
    Uint32  all;
    struct  GPBCTRL_BITS  bit;
};

struct GPAQSEL1_BITS {                        // bits description
    Uint32 GPIO0:2;                           // 1:0 GPIO0
    Uint32 GPIO1:2;                           // 3:2 GPIO1
    Uint32 GPIO2:2;                           // 5:4 GPIO2
    Uint32 GPIO3:2;                           // 7:6 GPIO3
    Uint32 GPIO4:2;                           // 9:8 GPIO4
    Uint32 GPIO5:2;                           // 11:10 GPIO5
    Uint32 GPIO6:2;                           // 13:12 GPIO6
    Uint32 GPIO7:2;                           // 15:14 GPIO7
    Uint32 GPIO8:2;                           // 17:16 GPIO8
    Uint32 GPIO9:2;                           // 19:18 GPIO9
    Uint32 GPIO10:2;                          // 21:20 GPIO10
    Uint32 GPIO11:2;                          // 23:22 GPIO11
    Uint32 GPIO12:2;                          // 25:24 GPIO12
    Uint32 GPIO13:2;                          // 27:26 GPIO13
    Uint32 GPIO14:2;                          // 29:28 GPIO14
    Uint32 GPIO15:2;                          // 31:30 GPIO15
};

union GPAQSEL1_REG {
    Uint32  all;
    struct  GPAQSEL1_BITS  bit;
};

struct GPAQSEL2_BITS {                        // bits description
    Uint32 GPIO16:2;                          // 1:0 GPIO16
    Uint32 GPIO17:2;                          // 3:2 GPIO17
    Uint32 GPIO18:2;                          // 5:4 GPIO18
    Uint32 GPIO19:2;                          // 7:6 GPIO19
    Uint32 GPIO20:2;                          // 9:8 GPIO20
    Uint32 GPIO21:2;                          // 11:10 GPIO21
    Uint32 GPIO22:2;                          // 13:12 GPIO22
    Uint32 GPIO23:2;                          // 15:14 GPIO23
    Uint32 GPIO24:2;                          // 17:16 GPIO24
    Uint32 GPIO25:2;                          // 19:18 GPIO25
    Uint32 GPIO26:2;                          // 21:20 GPIO26
    Uint32 GPIO27:2;                          // 23:22 GPIO27
    Uint32 GPIO28:2;                          // 25:24 GPIO28
    Uint32 GPIO29:2;                          // 27:26 GPIO29
    Uint32 GPIO30:2;                          // 29:28 GPIO30
    Uint32 GPIO31:2;                          // 31:30 GPIO31
};

union GPAQSEL2_REG {
    Uint32  all;
    struct  GPAQSEL2_BITS  bit;
};

struct GPBQSEL1_BITS {                        // bits description
    Uint32 GPIO32:2;                          // 1:0 GPIO32
    Uint32 GPIO33:2;                          // 3:2 GPIO33
    Uint32 GPIO34:2;                          // 5:4 GPIO34
    Uint32 GPIO35:2;                          // 7:6 GPIO35
    Uint32 GPIO36:2;                          // 9:8 GPIO36
    Uint32 GPIO37:2;                          // 11:10 GPIO37
    Uint32 GPIO38:2;                          // 13:12 GPIO38
    Uint32 GPIO39:2;                          // 15:14 GPIO39
    Uint32 GPIO40:2;                          // 17:16 GPIO40
    Uint32 GPIO41:2;                          // 19:18 GPIO41
    Uint32 GPIO42:2;                          // 21:20 GPIO42
    Uint32 GPIO43:2;                          // 23:22 GPIO43
    Uint32 GPIO44:2;                          // 25:24 GPIO44
    Uint32 rsvd1:6;                           // 31:26 reserved
};

union GPBQSEL1_REG {
    Uint32  all;
    struct  GPBQSEL1_BITS  bit;
};

struct GPADIR_BITS {                          // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPADIR_REG {
    Uint32  all;
    struct  GPADIR_BITS  bit;
};

struct GPBDIR_BITS {                          // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBDIR_REG {
    Uint32  all;
    struct  GPBDIR_BITS  bit;
};

struct AIODIR_BITS {                          // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 rsvd2:1;                           // 1 reserved
    Uint32 AIO2:1;                            // 2 AIO2
    Uint32 rsvd3:1;                           // 3 reserved
    Uint32 AIO4:1;                            // 4 AIO4
    Uint32 rsvd4:1;                           // 5 reserved
    Uint32 AIO6:1;                            // 6 AIO6
    Uint32 rsvd5:1;                           // 7 reserved
    Uint32 rsvd6:1;                           // 8 reserved
    Uint32 rsvd7:1;                           // 9 reserved
    Uint32 AIO10:1;                           // 10 AIO10
    Uint32 rsvd8:1;                           // 11 reserved
    Uint32 AIO12:1;                           // 12 AIO12
    Uint32 rsvd9:1;                           // 13 reserved
    Uint32 AIO14:1;                           // 14 AIO14
    Uint32 rsvd10:1;                          // 15 reserved
    Uint32 rsvd11:16;                         // 31:16 reserved
};

union AIODIR_REG {
    Uint32  all;
    struct  AIODIR_BITS  bit;
};

struct GPAPUD_BITS {                          // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPAPUD_REG {
    Uint32  all;
    struct  GPAPUD_BITS  bit;
};

struct GPBPUD_BITS {                          // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBPUD_REG {
    Uint32  all;
    struct  GPBPUD_BITS  bit;
};

struct  GPIO_CTRL_REGS {
    union   GPAMUX1_REG                      GPAMUX1;                     // 0x0 GPIO A Mux 1 Register (GPIO0 to 15) 00
    union   GPAMUX2_REG                      GPAMUX2;                     // 0x4 GPIO A Mux 2 Register (GPIO16 to 31) 04
    union   GPBMUX1_REG                      GPBMUX1;                     // 0x8 GPIO B Mux 1 Register (GPIO32 to 44) 08
    union   AIOMUX1_REG                      AIOMUX1;                     // 0xc Analog IO Mux 1 Register (AIO0 to 15) 0c
    union   GPACTRL_REG                      GPACTRL;                     // 0x10 GPIO A Control Register (GPIO0 to 31) 10
    union   GPBCTRL_REG                      GPBCTRL;                     // 0x14 GPIO B Control Register (GPIO32 to 38) 14
    union   GPAQSEL1_REG                     GPAQSEL1;                    // 0x18 GPIO A Qualifier Select 1 Register (GPIO0 to 15) 18
    union   GPAQSEL2_REG                     GPAQSEL2;                    // 0x1c GPIO A Qualifier Select 2 Register (GPIO16 to 31) 1C
    union   GPBQSEL1_REG                     GPBQSEL1;                    // 0x20 GPIO B Qualifier Select 1 Register (GPIO32 to 38) 20
    union   GPADIR_REG                       GPADIR;                      // 0x24 GPIO A Direction Register (GPIO0 to 31) 24
    union   GPBDIR_REG                       GPBDIR;                      // 0x28 GPIO B Direction Register (GPIO32 to 38) 28
    union   AIODIR_REG                       AIODIR;                      // 0x2c Analog IO Direction Register (AIO0 to 15) 2c
    union   GPAPUD_REG                       GPAPUD;                      // 0x30 GPIO A Pull Up Disable Register (GPIO0 to 31) 30
    union   GPBPUD_REG                       GPBPUD;                      // 0x34 GPIO B Pull Up Disable Register (GPIO32 to 44) 34
};

struct GPADAT_BITS {                          // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPADAT_REG {
    Uint32  all;
    struct  GPADAT_BITS  bit;
};

struct GPBDAT_BITS {                          // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBDAT_REG {
    Uint32  all;
    struct  GPBDAT_BITS  bit;
};

struct AIODAT_BITS {                          // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 rsvd2:1;                           // 1 reserved
    Uint32 AIO2:1;                            // 2 AIO2
    Uint32 rsvd3:1;                           // 3 reserved
    Uint32 AIO4:1;                            // 4 AIO4
    Uint32 rsvd4:1;                           // 5 reserved
    Uint32 AIO6:1;                            // 6 AIO6
    Uint32 rsvd5:1;                           // 7 reserved
    Uint32 rsvd6:1;                           // 8 reserved
    Uint32 rsvd7:1;                           // 9 reserved
    Uint32 AIO10:1;                           // 10 AIO10
    Uint32 rsvd8:1;                           // 11 reserved
    Uint32 AIO12:1;                           // 12 AIO12
    Uint32 rsvd9:1;                           // 13 reserved
    Uint32 AIO14:1;                           // 14 AIO14
    Uint32 rsvd10:1;                          // 15 reserved
    Uint32 rsvd11:16;                         // 31:16 reserved
};

union AIODAT_REG {
    Uint32  all;
    struct  AIODAT_BITS  bit;
};

struct GPASET_BITS {                          // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPASET_REG {
    Uint32  all;
    struct  GPASET_BITS  bit;
};

struct GPACLEAR_BITS {                        // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPACLEAR_REG {
    Uint32  all;
    struct  GPACLEAR_BITS  bit;
};

struct GPATOGGLE_BITS {                       // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPATOGGLE_REG {
    Uint32  all;
    struct  GPATOGGLE_BITS  bit;
};

struct GPBSET_BITS {                          // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBSET_REG {
    Uint32  all;
    struct  GPBSET_BITS  bit;
};

struct GPBCLEAR_BITS {                        // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBCLEAR_REG {
    Uint32  all;
    struct  GPBCLEAR_BITS  bit;
};

struct GPBTOGGLE_BITS {                       // bits description
    Uint32 GPIO32:1;                          // 0 GPIO32
    Uint32 GPIO33:1;                          // 1 GPIO33
    Uint32 GPIO34:1;                          // 2 GPIO34
    Uint32 GPIO35:1;                          // 3 GPIO35
    Uint32 GPIO36:1;                          // 4 GPIO36
    Uint32 GPIO37:1;                          // 5 GPIO37
    Uint32 GPIO38:1;                          // 6 GPIO38
    Uint32 GPIO39:1;                          // 7 GPIO39
    Uint32 GPIO40:1;                          // 8 GPIO40
    Uint32 GPIO41:1;                          // 9 GPIO41
    Uint32 GPIO42:1;                          // 10 GPIO42
    Uint32 GPIO43:1;                          // 11 GPIO43
    Uint32 GPIO44:1;                          // 12 GPIO44
    Uint32 rsvd1:3;                           // 15:13 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPBTOGGLE_REG {
    Uint32  all;
    struct  GPBTOGGLE_BITS  bit;
};

struct AIOSET_BITS {                          // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 rsvd2:1;                           // 1 reserved
    Uint32 AIO2:1;                            // 2 AIO2
    Uint32 rsvd3:1;                           // 3 reserved
    Uint32 AIO4:1;                            // 4 AIO4
    Uint32 rsvd4:1;                           // 5 reserved
    Uint32 AIO6:1;                            // 6 AIO6
    Uint32 rsvd5:1;                           // 7 reserved
    Uint32 rsvd6:1;                           // 8 reserved
    Uint32 rsvd7:1;                           // 9 reserved
    Uint32 AIO10:1;                           // 10 AIO10
    Uint32 rsvd8:1;                           // 11 reserved
    Uint32 AIO12:1;                           // 12 AIO12
    Uint32 rsvd9:1;                           // 13 reserved
    Uint32 AIO14:1;                           // 14 AIO14
    Uint32 rsvd10:1;                          // 15 reserved
    Uint32 rsvd11:16;                         // 31:16 reserved
};

union AIOSET_REG {
    Uint32  all;
    struct  AIOSET_BITS  bit;
};

struct AIOCLEAR_BITS {                        // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 rsvd2:1;                           // 1 reserved
    Uint32 AIO2:1;                            // 2 AIO2
    Uint32 rsvd3:1;                           // 3 reserved
    Uint32 AIO4:1;                            // 4 AIO4
    Uint32 rsvd4:1;                           // 5 reserved
    Uint32 AIO6:1;                            // 6 AIO6
    Uint32 rsvd5:1;                           // 7 reserved
    Uint32 rsvd6:1;                           // 8 reserved
    Uint32 rsvd7:1;                           // 9 reserved
    Uint32 AIO10:1;                           // 10 AIO10
    Uint32 rsvd8:1;                           // 11 reserved
    Uint32 AIO12:1;                           // 12 AIO12
    Uint32 rsvd9:1;                           // 13 reserved
    Uint32 AIO14:1;                           // 14 AIO14
    Uint32 rsvd10:1;                          // 15 reserved
    Uint32 rsvd11:16;                         // 31:16 reserved
};

union AIOCLEAR_REG {
    Uint32  all;
    struct  AIOCLEAR_BITS  bit;
};

struct AIOTOGGLE_BITS {                       // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 rsvd2:1;                           // 1 reserved
    Uint32 AIO2:1;                            // 2 AIO2
    Uint32 rsvd3:1;                           // 3 reserved
    Uint32 AIO4:1;                            // 4 AIO4
    Uint32 rsvd4:1;                           // 5 reserved
    Uint32 AIO6:1;                            // 6 AIO6
    Uint32 rsvd5:1;                           // 7 reserved
    Uint32 rsvd6:1;                           // 8 reserved
    Uint32 rsvd7:1;                           // 9 reserved
    Uint32 AIO10:1;                           // 10 AIO10
    Uint32 rsvd8:1;                           // 11 reserved
    Uint32 AIO12:1;                           // 12 AIO12
    Uint32 rsvd9:1;                           // 13 reserved
    Uint32 AIO14:1;                           // 14 AIO14
    Uint32 rsvd10:1;                          // 15 reserved
    Uint32 rsvd11:16;                         // 31:16 reserved
};

union AIOTOGGLE_REG {
    Uint32  all;
    struct  AIOTOGGLE_BITS  bit;
};

struct  GPIO_DATA_REGS {
    union   GPADAT_REG                       GPADAT;                      // 0x0 GPIO Data Register (GPIO0 to 31) 38
    union   GPBDAT_REG                       GPBDAT;                      // 0x4 GPIO Data Register (GPIO32 to 38) 3C
    union   AIODAT_REG                       AIODAT;                      // 0x8 Analog IO Data Register (AIO0-15) 40
    union   GPASET_REG                       GPASET;                      // 0xc GPIO Data Set Register (GPIO0 to 31) 44
    union   GPACLEAR_REG                     GPACLEAR;                    // 0x10 GPIO Data Clear Register (GPIO0 to 31) 48
    union   GPATOGGLE_REG                    GPATOGGLE;                   // 0x14 GPIO Data Toggle Register (GPIO0 to 31) 4C
    union   GPBSET_REG                       GPBSET;                      // 0x18 GPIO Data Set Register (GPIO32 to 38) 50
    union   GPBCLEAR_REG                     GPBCLEAR;                    // 0x1c GPIO Data Clear Register (GPIO32 to 38) 54
    union   GPBTOGGLE_REG                    GPBTOGGLE;                   // 0x20 GPIO Data Toggle Register (GPIO32 to 38) 58
    union   AIOSET_REG                       AIOSET;                      // 0x24 Analog IO Data Set Register (AIO0-15) 5C
    union   AIOCLEAR_REG                     AIOCLEAR;                    // 0x28 Analog IO Data Clear Register (AIO0-15) 60
    union   AIOTOGGLE_REG                    AIOTOGGLE;                   // 0x2c Analog IO Data Toggle Register (AIO0-15) 64
};

struct GPIOXINT1SEL_BITS {                    // bits description
    Uint32 GPIOSEL:5;                         // 4:0 Select GPIO interrupt input source
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPIOXINT1SEL_REG {
    Uint32  all;
    struct  GPIOXINT1SEL_BITS  bit;
};

struct GPIOXINT2SEL_BITS {                    // bits description
    Uint32 GPIOSEL:5;                         // 4:0 Select GPIO interrupt input source
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPIOXINT2SEL_REG {
    Uint32  all;
    struct  GPIOXINT2SEL_BITS  bit;
};

struct GPIOXINT3SEL_BITS {                    // bits description
    Uint32 GPIOSEL:5;                         // 4:0 Select GPIO interrupt input source
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union GPIOXINT3SEL_REG {
    Uint32  all;
    struct  GPIOXINT3SEL_BITS  bit;
};

struct GPIOLPMSEL_BITS {                      // bits description
    Uint32 GPIO0:1;                           // 0 GPIO0
    Uint32 GPIO1:1;                           // 1 GPIO1
    Uint32 GPIO2:1;                           // 2 GPIO2
    Uint32 GPIO3:1;                           // 3 GPIO3
    Uint32 GPIO4:1;                           // 4 GPIO4
    Uint32 GPIO5:1;                           // 5 GPIO5
    Uint32 GPIO6:1;                           // 6 GPIO6
    Uint32 GPIO7:1;                           // 7 GPIO7
    Uint32 GPIO8:1;                           // 8 GPIO8
    Uint32 GPIO9:1;                           // 9 GPIO9
    Uint32 GPIO10:1;                          // 10 GPIO10
    Uint32 GPIO11:1;                          // 11 GPIO11
    Uint32 GPIO12:1;                          // 12 GPIO12
    Uint32 GPIO13:1;                          // 13 GPIO13
    Uint32 GPIO14:1;                          // 14 GPIO14
    Uint32 GPIO15:1;                          // 15 GPIO15
    Uint32 GPIO16:1;                          // 16 GPIO16
    Uint32 GPIO17:1;                          // 17 GPIO17
    Uint32 GPIO18:1;                          // 18 GPIO18
    Uint32 GPIO19:1;                          // 19 GPIO19
    Uint32 GPIO20:1;                          // 20 GPIO20
    Uint32 GPIO21:1;                          // 21 GPIO21
    Uint32 GPIO22:1;                          // 22 GPIO22
    Uint32 GPIO23:1;                          // 23 GPIO23
    Uint32 GPIO24:1;                          // 24 GPIO24
    Uint32 GPIO25:1;                          // 25 GPIO25
    Uint32 GPIO26:1;                          // 26 GPIO26
    Uint32 GPIO27:1;                          // 27 GPIO27
    Uint32 GPIO28:1;                          // 28 GPIO28
    Uint32 GPIO29:1;                          // 29 GPIO29
    Uint32 GPIO30:1;                          // 30 GPIO30
    Uint32 GPIO31:1;                          // 31 GPIO31
};

union GPIOLPMSEL_REG {
    Uint32  all;
    struct  GPIOLPMSEL_BITS  bit;
};

struct  GPIO_INT_REGS {
    union   GPIOXINT1SEL_REG                 GPIOXINT1SEL;                // 0x0 XINT1 GPIO Input Selection 68
    union   GPIOXINT2SEL_REG                 GPIOXINT2SEL;                // 0x4 XINT2 GPIO Input Selection 6C
    union   GPIOXINT3SEL_REG                 GPIOXINT3SEL;                // 0x8 XINT3 GPIO Input Selection 70
    union   GPIOLPMSEL_REG                   GPIOLPMSEL;                  // 0xc Low power modes GPIO input select 74
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================