//###########################################################################
//
// FILE:    ier_unset.S
//
// TITLE:   ier_unset Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 ier_unset

ier_unset:
csrc 0x304,a0  //IER &= ~a0 
ret


	

