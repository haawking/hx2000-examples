//###########################################################################
//
// FILE:	HXM32G407_CSMPasswords.c
//
// TITLE:	HXM32G407 Code Security Module Passwords.
// 
// DESCRIPTION:
//
//         This file is used to specify password values to
//         program into the CSM password locations in Flash
//         at 0x73FFF0 - 73FFFF.
//
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "HXM32G407_Device.h"     //  Headerfile Include File
/**********************************************
password file
*****************************************************************/

volatile struct CSM_PWL  CODE_SECTION(".CsmPwl") CsmPwl = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};


void init_call(void)
{
	CsmPwl.PSWD0;
	CsmPwl.PSWD1;
	CsmPwl.PSWD2;
	CsmPwl.PSWD3;
}




