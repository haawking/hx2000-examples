﻿/******************************************************************
 文 档 名 ：     HX_HXM32G407_EQEP
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 作 用：EQEP测速
 说 明： (1)通过epwm1输出SysFreq*7500*2/SetSpeed个TBCLK周期，
 占空比为SysFreq*3750/SetSpeed，向上向下计数，50%

 (2)通过EQEP实现：a）b）两种功能下，设置转速 6000  c）功能下 设置转速 60
 a)脉冲捕获：采用最大编码值捕获，单位周期为SysFreq*100，编码器方向模式
 输入，仅在上升沿计数，仿真模式不受影响，位置计数器在单位时间事件复位，
 单位超时时锁存。
 现象：脉冲捕获 PulseNum=5，PulseDir=1;
 b)M法测速：采用最大编码值捕获，单位周期为SysFreq*100，编码器正交模式输入，
 在上升沿下降沿均计数，仿真模式不受影响，位置计数器在单位时间事件复位，
 单位超时时锁存。
 模拟电机以3000rpm旋转，测出电机转速MotorSpeed=SetSpeed=3000。
 c)T法测速：采用最大编码值4000，单位周期6000，编码器正交模式输入，
 在上升沿下降沿均计数，仿真模式不受影响，位置计数器在单位时间事件复位，
 单位超时时锁存，捕获控制：单位位置事件1分频，捕获时钟预分频1分频。

 模拟电机以60rpm旋转，测出电机转速MotorSpeed=SetSpeed=60。
 (3) 通过定时器中断实现三种不同的功能切换：中断计数<5000，
 进行脉冲捕获；5000<中断计数<10000，M法测速；中断计数>10000

 ----------------------例程使用说明-----------------------------
 *
 *   GPIO0接GPIO20 QEPA，GPIO1接GPIO21 QEPB
 *
 *
 * 现象：
 *由定时器中断，切换三种不同功能，0-5s内 脉冲捕获 ，即LED2/D400常亮
 *5-10s内 采用M法测量转速，模拟电机转速测量值与设定值一致，LED3/D401常亮，不一致则LED3灭
 *10s后 采用T法测量转速，模拟电机转速测量值与设定值一致，LED3/D401常亮，不一致则LED3灭
 版 本：V1.0.0
 时 间：2022年8月25日
 作 者：heyang
 @ mail：support@mail.haawking.com

 ******************************************************************/
#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "system.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	/*EQEP1引脚配置*/
	InitEQep1Gpio();
	/*初始化LED配置，用于指示实际转速是否到达给定转速*/
	InitLED();
	/*初始化pie中断控制*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE中断向量表*/
	InitPieVectTable();
	/*定时器配置*/
	Timer0_init();
	
	EALLOW;
	/*将timer0_ISR入口地址赋给TINT0，执行M法、T法测速切换中断服务程序*/
	PieVectTable.TINT0 = &timer0_ISR;
	EDIS;
	
	/*电机转速初始化*/
	MotorSpeed_init();

	EALLOW;
	/*每个启用的ePWM模块中的TBCLK（时基时钟）均已停止*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;
	/*配置epwm1，用于模拟电机转动*/
	EPWM1_Config();

	EALLOW;
	/*所有使能的ePWM模块时钟都是在TBCLK的第一个上升沿对齐的情况下开始的*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*EQEP脉冲捕获配置*/
	EQEP_pulseCap();
	/*使能打开相应的CPU IER中断*/
	IER |= M_INT1;
	/*使能打开相应的PIE IER中断*/
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
	/*使能打开全局中断*/
	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
