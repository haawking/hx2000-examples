#ifndef EPWM_H_
#define EPWM_H_

#include "HXM32G407_config.h"

void InitEpwm1_Example(void);
void InitEpwm2_Example(void);
void InitEpwm3_Example(void);

void INTERRUPT epwm1_tz_isr(void);
void INTERRUPT epwm2_tz_isr(void);
void INTERRUPT epwm3_tz_isr(void);

extern Uint16 EPwm_TZ_CBC_flag;
extern Uint16 EPwm_TZ_DC_flag;

void InitLED(void);

#endif/*EPWM_H_*/
