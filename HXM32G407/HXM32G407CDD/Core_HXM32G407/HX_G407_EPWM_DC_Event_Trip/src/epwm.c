#include "epwm.h"
/******************************************************************
*函数名：void InitEpwm1_Example(void)
*参 数：无
*返回值：无
*作 用：初始化Epwm1
******************************************************************/
void InitEpwm1_Example(void)
{
	/* Period = 4000*TBCLK counts*/
	EPwm1Regs.TBPRD=2000;
	/*epwm时基计数器相位=0*/
	EPwm1Regs.TBPHS.half.TBPHS=0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））。*/
	EPwm1Regs.TBCTR=0x0000;

	/*设置CMPA值为EPWM1_MAX_CMPA*/
	EPwm1Regs.CMPA.half.CMPA=1000;
	/*设置CMPB值为EPWM1_MAX_CMPB*/
	EPwm1Regs.CMPB=0;

	/*增减计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm1Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	/*时基计数器等于零时，CMPA加载镜像寄存器*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	/*时基计数器等于零时，CMPB加载镜像寄存器*/
	EPwm1Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;

	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm1Regs.AQCTLB.bit.CBD = AQ_SET;

	/*使能双边沿延时死区*/
	EPwm1Regs.DBCTL.bit.OUT_MODE=DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm1Regs.DBCTL.bit.POLSEL=DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm1Regs.DBCTL.bit.IN_MODE=DBA_ALL;
	/* 死区上升沿延时50TBCLK*/
    EPwm1Regs.DBRED = 50;
    /*死区下降沿延时50TBCLK*/
    EPwm1Regs.DBFED = 50;

	EALLOW;
	/*TZ3输入为DCAH输入的信号源*/
	EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL=DC_TZ3;
	/*TZ2输入为DCAL输入的信号源*/
	EPwm1Regs.DCTRIPSEL.bit.DCALCOMPSEL=DC_TZ2;
	/*设置触发条件为TZ2置高,TZ3置低*/
	EPwm1Regs.TZDCSEL.bit.DCAEVT1=TZ_DCAL_HI_DCAH_LOW;
	/*0源是 DCAEVT1 信号*/
	EPwm1Regs.DCACTL.bit.EVT1SRCSEL=DC_EVT1;
	/*1源是异步信号*/
	EPwm1Regs.DCACTL.bit.EVT1FRCSYNCSEL=DC_EVT_ASYNC;
	/*启用 DCAEVT1 作为此 ePWM 模块的 OSHT 跳闸源。*/
	EPwm1Regs.TZSEL.bit.DCAEVT1=1;
	/*发生跳闸事件时，强制 EPWMxA 进入高电平*/
	EPwm1Regs.TZCTL.bit.TZA=TZ_FORCE_HI;
	/* 发生跳闸事件时，强制 EPWMxB 进入低电平*/
	EPwm1Regs.TZCTL.bit.TZB=TZ_FORCE_LO;
	/*跳闸区域单次中断使能*/
	EPwm1Regs.TZEINT.bit.OST=1;
	EDIS;
};
/******************************************************************
*函数名：void InitEpwm2_Example(void)
*参 数：无
*返回值：无
*作 用：初始化Epwm2
******************************************************************/
void InitEpwm2_Example(void)
{
	/* Period = 4000*TBCLK counts*/
	EPwm2Regs.TBPRD=2000;
	/*epwm时基计数器相位=0*/
	EPwm2Regs.TBPHS.half.TBPHS=0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））。*/
	EPwm2Regs.TBCTR=0x0000;

	/*设置CMPA值为EPWM2_MAX_CMPA*/
	EPwm2Regs.CMPA.half.CMPA=1000;
	/*设置CMPB值为EPWM2_MAX_CMPB*/
	EPwm2Regs.CMPB=0;
	/*增减计数模式*/
	EPwm2Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm2Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.CLKDIV=TB_DIV1;
	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;
	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxA输出高电平*/
	EPwm2Regs.AQCTLA.bit.CAU=AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm2Regs.AQCTLA.bit.CBD=AQ_CLEAR;
	/*时基计数器等于 PRD 时,强制 EPWMxA 输出为高电平*/
	EPwm2Regs.AQCTLB.bit.PRD=AQ_SET;
	/*时基计数器等于零时,强制 EPWMxA 输出低电平*/
	EPwm2Regs.AQCTLB.bit.ZRO=AQ_CLEAR;

	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm2Regs.AQCTLB.bit.CBD = AQ_SET;

	/*使能双边沿延时死区*/
	EPwm2Regs.DBCTL.bit.OUT_MODE=DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm2Regs.DBCTL.bit.POLSEL=DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm2Regs.DBCTL.bit.IN_MODE=DBA_ALL;
	/* 死区上升沿延时50TBCLK*/
    EPwm2Regs.DBRED = 50;
    /*死区下降沿延时50TBCLK*/
    EPwm2Regs.DBFED = 50;

	EALLOW;
	/*设置TZ3为DCAH输入源*/
	EPwm2Regs.DCTRIPSEL.bit.DCAHCOMPSEL=DC_TZ3;
	/*设置TZ2为DCAL输入源*/
	EPwm2Regs.DCTRIPSEL.bit.DCALCOMPSEL=DC_TZ2;
	/*设置触发条件为TZ2置低,TZ3置高*/
	EPwm2Regs.TZDCSEL.bit.DCAEVT2=TZ_DCAL_LOW;
	/*设置DCAEVT1 信号为信号源*/
	EPwm2Regs.DCACTL.bit.EVT1SRCSEL=DC_EVT1;
	/*源是异步信号*/
	EPwm2Regs.DCACTL.bit.EVT1FRCSYNCSEL=DC_EVT_ASYNC;
	/*启用DCAEVT2作为此ePWM模块的CBC跳闸源*/
	EPwm2Regs.TZSEL.bit.DCAEVT2=1;
	/*发生跳闸事件时,强制 EPWMxA 进入高电平*/
	EPwm2Regs.TZCTL.bit.TZA=TZ_FORCE_HI;
	/*发生跳闸事件时,将 EPWMxB 强制为低电平*/
	EPwm2Regs.TZCTL.bit.TZB=TZ_FORCE_LO;
	/*跳闸区域逐周期中断使能*/
	EPwm2Regs.TZEINT.bit.CBC=1;
	EDIS;
}
/******************************************************************
*函数名：void InitEpwm3_Example(void)
*参 数：无
*返回值：无
*作 用：初始化Epwm3
******************************************************************/
void InitEpwm3_Example(void)
{
	/* Period = 4000*TBCLK counts*/
	EPwm3Regs.TBPRD=2000;
	/*epwm时基计数器相位=0*/
	EPwm3Regs.TBPHS.half.TBPHS=0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））。*/
	EPwm3Regs.TBCTR=0x0000;

	/*设置CMPA值为EPWM3_MAX_CMPA*/
	EPwm3Regs.CMPA.half.CMPA=1000;
	/*设置CMPA值为EPWM3_MAX_CMPB*/
	EPwm3Regs.CMPB=0;

	/*增减计数模式*/
	EPwm3Regs.TBCTL.bit.CTRMODE=TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm3Regs.TBCTL.bit.PHSEN=TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm3Regs.TBCTL.bit.HSPCLKDIV=TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm3Regs.TBCTL.bit.CLKDIV=TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm3Regs.CMPCTL.bit.SHDWAMODE=CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm3Regs.CMPCTL.bit.SHDWBMODE=CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm3Regs.CMPCTL.bit.LOADAMODE=CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm3Regs.CMPCTL.bit.LOADBMODE=CC_CTR_ZERO;

	/*时基计数器等于PRD时，强制EPWMxA输出低电平*/
	EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;
	/*时基计数器等于零时，强制EPWMxA输出为高电平*/
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递减时强制EPWMxA输出低电平*/
	EPwm3Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	/*时基计数器等于活动的CMPA寄存器且计数器递增时，强制EPWMxA输出为高电平*/
	EPwm3Regs.AQCTLB.bit.CBD = AQ_SET;

	/*使能双边沿延时死区*/
	EPwm3Regs.DBCTL.bit.OUT_MODE=DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm3Regs.DBCTL.bit.POLSEL=DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm3Regs.DBCTL.bit.IN_MODE=DBA_ALL;
	/* 死区上升沿延时50TBCLK*/
    EPwm3Regs.DBRED = 50;
    /*死区下降沿延时50TBCLK*/
    EPwm3Regs.DBFED = 50;

	EALLOW;
	/*设置TZ3为DCAH输入源*/
	EPwm3Regs.DCTRIPSEL.bit.DCAHCOMPSEL=DC_TZ3;
	/*设置TZ2为DCAL输入源*/
	EPwm3Regs.DCTRIPSEL.bit.DCALCOMPSEL=DC_TZ2;
	/*设置触发条件为TZ2置低,TZ3置高*/
	EPwm3Regs.TZDCSEL.bit.DCAEVT2=TZ_DCAL_LOW;
	 /*设置DCAEVT1 信号为信号源*/
	EPwm3Regs.DCACTL.bit.EVT1SRCSEL=DC_EVT1;
	/*源是异步信号*/
	EPwm3Regs.DCACTL.bit.EVT1FRCSYNCSEL=DC_EVT_ASYNC;
	/*设置TZ3为DCBH输入源*/
	EPwm3Regs.DCTRIPSEL.bit.DCBHCOMPSEL=DC_TZ3;
	/*设置TZ2为DCBL输入源*/
	EPwm3Regs.DCTRIPSEL.bit.DCBLCOMPSEL=DC_TZ2;
	/*设置触发条件为TZ2置低,TZ3置高*/
	EPwm3Regs.TZDCSEL.bit.DCBEVT1=TZ_DCBL_LOW;
	 /*设置DCAEVT1 信号为信号源*/
	EPwm3Regs.DCBCTL.bit.EVT1SRCSEL=DC_EVT1;
	/*源是异步信号*/
	EPwm3Regs.DCBCTL.bit.EVT1FRCSYNCSEL=DC_EVT_ASYNC;
	/*数字比较输出A事件2,强制EPWMxA进入高电平状态*/
	EPwm3Regs.TZCTL.bit.DCAEVT2=TZ_FORCE_HI;
	/*数字比较输出B事件1,强制EPWMxB处于低电平状态*/
	EPwm3Regs.TZCTL.bit.DCBEVT1=TZ_FORCE_LO;
	/*数字比较器输出A事件2中断使能*/
	EPwm3Regs.TZEINT.bit.DCAEVT2=1;
	EDIS;
}
