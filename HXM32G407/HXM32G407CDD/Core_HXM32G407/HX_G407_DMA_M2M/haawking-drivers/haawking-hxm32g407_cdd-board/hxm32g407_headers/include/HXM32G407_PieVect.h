//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.472199
//
//#############################################################################


#ifndef HXM32G407_PIEVECT_H
#define HXM32G407_PIEVECT_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// PIEVECT Individual Register Bit Definitions:

struct  PIE_VECT_TABLE {
    PINT                                     ADCINT1;                     // 0x0 ADC - if Group 10 ADCINT1 is enabled, this must be rsvd1_1
    PINT                                     ADCINT2;                     // 0x4 ADC - if Group 10 ADCINT2 is enabled, this must be rsvd1_2
    PINT                                     rsvd1_3;                     // 0x8  
    PINT                                     XINT1;                       // 0xc  
    PINT                                     XINT2;                       // 0x10  
    PINT                                     ADCINT9;                     // 0x14 ADC
    PINT                                     TINT0;                       // 0x18 Timer 0
    PINT                                     WAKEINT;                     // 0x1c WD
    PINT                                     EPWM1_TZINT;                 // 0x20 EPWM-1
    PINT                                     EPWM2_TZINT;                 // 0x24 EPWM-2
    PINT                                     EPWM3_TZINT;                 // 0x28 EPWM-3
    PINT                                     EPWM4_TZINT;                 // 0x2c EPWM-4
    PINT                                     EPWM5_TZINT;                 // 0x30  
    PINT                                     EPWM6_TZINT;                 // 0x34  
    PINT                                     EPWM7_TZINT;                 // 0x38  
    PINT                                     rsvd2_8;                     // 0x3c  
    PINT                                     EPWM1_INT;                   // 0x40 EPWM-1
    PINT                                     EPWM2_INT;                   // 0x44 EPWM-2
    PINT                                     EPWM3_INT;                   // 0x48 EPWM-3
    PINT                                     EPWM4_INT;                   // 0x4c EPWM-4
    PINT                                     EPWM5_INT;                   // 0x50  
    PINT                                     EPWM6_INT;                   // 0x54  
    PINT                                     EPWM7_INT;                   // 0x58  
    PINT                                     rsvd3_8;                     // 0x5c  
    PINT                                     ECAP1_INT;                   // 0x60 ECAP-1
    PINT                                     rsvd4_2;                     // 0x64  
    PINT                                     rsvd4_3;                     // 0x68  
    PINT                                     rsvd4_4;                     // 0x6c  
    PINT                                     rsvd4_5;                     // 0x70  
    PINT                                     rsvd4_6;                     // 0x74  
    PINT                                     HRCAP1_INT;                  // 0x78  
    PINT                                     HRCAP2_INT;                  // 0x7c  
    PINT                                     EQEP1_INT;                   // 0x80  
    PINT                                     EQEP2_INT;                   // 0x84  
    PINT                                     rsvd5_3;                     // 0x88  
    PINT                                     rsvd5_4;                     // 0x8c  
    PINT                                     rsvd5_5;                     // 0x90  
    PINT                                     rsvd5_6;                     // 0x94  
    PINT                                     rsvd5_7;                     // 0x98  
    PINT                                     rsvd5_8;                     // 0x9c  
    PINT                                     SPIRXINTA;                   // 0xa0 SPI-A
    PINT                                     SPITXINTA;                   // 0xa4 SPI-A
    PINT                                     SPIRXINTB;                   // 0xa8 SPI-B
    PINT                                     SPITXINTB;                   // 0xac SPI-B
    PINT                                     rsvd6_5;                     // 0xb0  
    PINT                                     rsvd6_6;                     // 0xb4  
    PINT                                     rsvd6_7;                     // 0xb8  
    PINT                                     rsvd6_8;                     // 0xbc  
    PINT                                     DINTCH1;                     // 0xc0  
    PINT                                     DINTCH2;                     // 0xc4  
    PINT                                     DINTCH3;                     // 0xc8  
    PINT                                     DINTCH4;                     // 0xcc  
    PINT                                     DINTCH5;                     // 0xd0  
    PINT                                     DINTCH6;                     // 0xd4  
    PINT                                     rsvd7_7;                     // 0xd8  
    PINT                                     rsvd7_8;                     // 0xdc  
    PINT                                     I2CINT1A;                    // 0xe0 I2C-A
    PINT                                     I2CINT2A;                    // 0xe4  
    PINT                                     rsvd8_3;                     // 0xe8  
    PINT                                     rsvd8_4;                     // 0xec  
    PINT                                     rsvd8_5;                     // 0xf0  
    PINT                                     rsvd8_6;                     // 0xf4  
    PINT                                     rsvd8_7;                     // 0xf8  
    PINT                                     rsvd8_8;                     // 0xfc  
    PINT                                     SCIRXINTA;                   // 0x100 SCI-A
    PINT                                     SCITXINTA;                   // 0x104  
    PINT                                     LIN0INTA;                    // 0x108 LIN-A
    PINT                                     LIN1INTA;                    // 0x10c LIN-A
    PINT                                     ECAN0INTA;                   // 0x110  
    PINT                                     ECAN1INTA;                   // 0x114  
    PINT                                     rsvd9_7;                     // 0x118  
    PINT                                     rsvd9_8;                     // 0x11c  
    PINT                                     rsvd10_1;                    // 0x120  
    PINT                                     rsvd10_2;                    // 0x124  
    PINT                                     ADCINT3;                     // 0x128 ADC
    PINT                                     ADCINT4;                     // 0x12c ADC
    PINT                                     ADCINT5;                     // 0x130 ADC
    PINT                                     ADCINT6;                     // 0x134 ADC
    PINT                                     ADCINT7;                     // 0x138 ADC
    PINT                                     ADCINT8;                     // 0x13c ADC
    PINT                                     rsvd11_1;                    // 0x140  
    PINT                                     rsvd11_2;                    // 0x144  
    PINT                                     rsvd11_3;                    // 0x148  
    PINT                                     rsvd11_4;                    // 0x14c  
    PINT                                     rsvd11_5;                    // 0x150  
    PINT                                     rsvd11_6;                    // 0x154  
    PINT                                     rsvd11_7;                    // 0x158  
    PINT                                     rsvd11_8;                    // 0x15c  
    PINT                                     XINT3;                       // 0x160  
    PINT                                     rsvd12_2;                    // 0x164  
    PINT                                     rsvd12_3;                    // 0x168  
    PINT                                     rsvd12_4;                    // 0x16c  
    PINT                                     rsvd12_5;                    // 0x170  
    PINT                                     rsvd12_6;                    // 0x174  
    PINT                                     rsvd12_7;                    // 0x178  
    PINT                                     rsvd12_8;                    // 0x17c  
    PINT                                     TINT1;                       // 0x180 CPU-Timer1 //380
    PINT                                     rsvd13_2;                    // 0x184  
    PINT                                     rsvd13_3;                    // 0x188  
    PINT                                     rsvd13_4;                    // 0x18c  
    PINT                                     rsvd13_5;                    // 0x190  
    PINT                                     rsvd13_6;                    // 0x194  
    PINT                                     rsvd13_7;                    // 0x198  
    PINT                                     rsvd13_8;                    // 0x19c  
    PINT                                     TINT2;                       // 0x1a0 CPU-Timer2 //3A0
    PINT                                     rsvd14_2;                    // 0x1a4  
    PINT                                     rsvd14_3;                    // 0x1a8  
    PINT                                     rsvd14_4;                    // 0x1ac  
    PINT                                     rsvd14_5;                    // 0x1b0  
    PINT                                     rsvd14_6;                    // 0x1b4  
    PINT                                     rsvd14_7;                    // 0x1b8  
    PINT                                     rsvd14_8;                    // 0x1bc  
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
