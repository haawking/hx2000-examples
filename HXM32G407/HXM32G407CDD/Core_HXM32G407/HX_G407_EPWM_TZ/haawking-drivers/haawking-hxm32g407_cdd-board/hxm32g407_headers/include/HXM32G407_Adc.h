//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.095464
//
//#############################################################################


#ifndef HXM32G407_ADC_H
#define HXM32G407_ADC_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// ADC Individual Register Bit Definitions:

struct ADCCTL1_BITS {                         // bits description
    Uint32 rsvd1:1;                           // 0 Reserved
    Uint32 VREFLOCONV:1;                      // 1 VSSA Connection
    Uint32 INTPULSEPOS:1;                     // 2 INT pulse generation control
    Uint32 rsvd2:1;                           // 3 Reserved
    Uint32 ADCRDY:1;                          // 4 ADC Ready new
    Uint32 rsvd3:3;                           // 7:5 Reserved
    Uint32 ADCBSYCHN:5;                       // 12:8 ADC busy on a channel
    Uint32 ADCBSY:1;                          // 13 ADC busy signal
    Uint32 ADCENABLE:1;                       // 14 ADC enable
    Uint32 RESET:1;                           // 15 ADC Master reset
    Uint32 rsvd4:16;                          // 31:16 Reserved
};

union ADCCTL1_REG {
    Uint32  all;
    struct  ADCCTL1_BITS  bit;
};

struct ADCCTL2_BITS {                         // bits description
    Uint32 CLKDIV2EN:1;                       // 0 ADC input clock /2 enable
    Uint32 rsvd1:1;                           // 1 Reserved
    Uint32 SELDO_HS_LU:1;                     // 2 ADC result signed or unsigned new
    Uint32 rsvd2:29;                          // 31:3 Reserved
};

union ADCCTL2_REG {
    Uint32  all;
    struct  ADCCTL2_BITS  bit;
};

struct ADCINTFLG_BITS {                       // bits description
    Uint32 ADCINT1:1;                         // 0 ADC Interrupt Flag 1
    Uint32 ADCINT2:1;                         // 1 ADC Interrupt Flag 2
    Uint32 ADCINT3:1;                         // 2 ADC Interrupt Flag 3
    Uint32 ADCINT4:1;                         // 3 ADC Interrupt Flag 4
    Uint32 ADCINT5:1;                         // 4 ADC Interrupt Flag 5
    Uint32 ADCINT6:1;                         // 5 ADC Interrupt Flag 6
    Uint32 ADCINT7:1;                         // 6 ADC Interrupt Flag 7
    Uint32 ADCINT8:1;                         // 7 ADC Interrupt Flag 8
    Uint32 ADCINT9:1;                         // 8 ADC Interrupt Flag 9
    Uint32 rsvd1:7;                           // 15:9 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCINTFLG_REG {
    Uint32  all;
    struct  ADCINTFLG_BITS  bit;
};

struct ADCINTFLGCLR_BITS {                    // bits description
    Uint32 ADCINT1:1;                         // 0 ADC Interrupt Flag 1 Clear
    Uint32 ADCINT2:1;                         // 1 ADC Interrupt Flag 2 Clear
    Uint32 ADCINT3:1;                         // 2 ADC Interrupt Flag 3 Clear
    Uint32 ADCINT4:1;                         // 3 ADC Interrupt Flag 4 Clear
    Uint32 ADCINT5:1;                         // 4 ADC Interrupt Flag 5 Clear
    Uint32 ADCINT6:1;                         // 5 ADC Interrupt Flag 6 Clear
    Uint32 ADCINT7:1;                         // 6 ADC Interrupt Flag 7 Clear
    Uint32 ADCINT8:1;                         // 7 ADC Interrupt Flag 8 Clear
    Uint32 ADCINT9:1;                         // 8 ADC Interrupt Flag 9 Clear
    Uint32 rsvd1:7;                           // 15:9 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCINTFLGCLR_REG {
    Uint32  all;
};

struct ADCINTOVF_BITS {                       // bits description
    Uint32 ADCINT1:1;                         // 0 ADC Interrupt Overflow Flag 1
    Uint32 ADCINT2:1;                         // 1 ADC Interrupt Overflow Flag 2
    Uint32 ADCINT3:1;                         // 2 ADC Interrupt Overflow Flag 3
    Uint32 ADCINT4:1;                         // 3 ADC Interrupt Overflow Flag 4
    Uint32 ADCINT5:1;                         // 4 ADC Interrupt Overflow Flag 5
    Uint32 ADCINT6:1;                         // 5 ADC Interrupt Overflow Flag 6
    Uint32 ADCINT7:1;                         // 6 ADC Interrupt Overflow Flag 7
    Uint32 ADCINT8:1;                         // 7 ADC Interrupt Overflow Flag 8
    Uint32 ADCINT9:1;                         // 8 ADC Interrupt Overflow Flag 9
    Uint32 rsvd1:7;                           // 15:9 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCINTOVF_REG {
    Uint32  all;
    struct  ADCINTOVF_BITS  bit;
};

struct ADCINTOVFCLR_BITS {                    // bits description
    Uint32 ADCINT1:1;                         // 0 ADC Interrupt Overflow Flag 1 Clear
    Uint32 ADCINT2:1;                         // 1 ADC Interrupt Overflow Flag 2 Clear
    Uint32 ADCINT3:1;                         // 2 ADC Interrupt Overflow Flag 3 Clear
    Uint32 ADCINT4:1;                         // 3 ADC Interrupt Overflow Flag 4 Clear
    Uint32 ADCINT5:1;                         // 4 ADC Interrupt Overflow Flag 5 Clear
    Uint32 ADCINT6:1;                         // 5 ADC Interrupt Overflow Flag 6 Clear
    Uint32 ADCINT7:1;                         // 6 ADC Interrupt Overflow Flag 7 Clear
    Uint32 ADCINT8:1;                         // 7 ADC Interrupt Overflow Flag 8 Clear
    Uint32 ADCINT9:1;                         // 8 ADC Interrupt Overflow Flag 9 Clear
    Uint32 rsvd1:7;                           // 15:9 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCINTOVFCLR_REG {
    Uint32  all;
};

struct INTSEL1N2_BITS {                       // bits description
    Uint32 INT1SEL:5;                         // 4:0 INT1 EOC Source Select
    Uint32 INT1E:1;                           // 5 INT1 Interrupt Enable
    Uint32 INT1CONT:1;                        // 6 INT1 Continuous Mode Enable
    Uint32 rsvd1:1;                           // 7 reserved
    Uint32 INT2SEL:5;                         // 12:8 INT2 EOC Source Select
    Uint32 INT2E:1;                           // 13 INT2 Interrupt Enable
    Uint32 INT2CONT:1;                        // 14 INT2 Continuous Mode Enable
    Uint32 rsvd2:1;                           // 15 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union INTSEL1N2_REG {
    Uint32  all;
    struct  INTSEL1N2_BITS  bit;
};

struct INTSEL3N4_BITS {                       // bits description
    Uint32 INT3SEL:5;                         // 4:0 INT3 EOC Source Select
    Uint32 INT3E:1;                           // 5 INT3 Interrupt Enable
    Uint32 INT3CONT:1;                        // 6 INT3 Continuous Mode Enable
    Uint32 rsvd1:1;                           // 7 reserved
    Uint32 INT4SEL:5;                         // 12:8 INT4 EOC Source Select
    Uint32 INT4E:1;                           // 13 INT4 Interrupt Enable
    Uint32 INT4CONT:1;                        // 14 INT4 Continuous Mode Enable
    Uint32 rsvd2:1;                           // 15 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union INTSEL3N4_REG {
    Uint32  all;
    struct  INTSEL3N4_BITS  bit;
};

struct INTSEL5N6_BITS {                       // bits description
    Uint32 INT5SEL:5;                         // 4:0 INT5 EOC Source Select
    Uint32 INT5E:1;                           // 5 INT5 Interrupt Enable
    Uint32 INT5CONT:1;                        // 6 INT5 Continuous Mode Enable
    Uint32 rsvd1:1;                           // 7 reserved
    Uint32 INT6SEL:5;                         // 12:8 INT6 EOC Source Select
    Uint32 INT6E:1;                           // 13 INT6 Interrupt Enable
    Uint32 INT6CONT:1;                        // 14 INT6 Continuous Mode Enable
    Uint32 rsvd2:1;                           // 15 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union INTSEL5N6_REG {
    Uint32  all;
    struct  INTSEL5N6_BITS  bit;
};

struct INTSEL7N8_BITS {                       // bits description
    Uint32 INT7SEL:5;                         // 4:0 INT7 EOC Source Select
    Uint32 INT7E:1;                           // 5 INT7 Interrupt Enable
    Uint32 INT7CONT:1;                        // 6 INT7 Continuous Mode Enable
    Uint32 rsvd1:1;                           // 7 reserved
    Uint32 INT8SEL:5;                         // 12:8 INT8 EOC Source Select
    Uint32 INT8E:1;                           // 13 INT8 Interrupt Enable
    Uint32 INT8CONT:1;                        // 14 INT8 Continuous Mode Enable
    Uint32 rsvd2:1;                           // 15 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union INTSEL7N8_REG {
    Uint32  all;
    struct  INTSEL7N8_BITS  bit;
};

struct INTSEL9N10_BITS {                      // bits description
    Uint32 INT9SEL:5;                         // 4:0 INT9 EOC Source Select
    Uint32 INT9E:1;                           // 5 INT9 Interrupt Enable
    Uint32 INT9CONT:1;                        // 6 INT9 Continuous Mode Enable
    Uint32 rsvd1:9;                           // 15:7 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTSEL9N10_REG {
    Uint32  all;
    struct  INTSEL9N10_BITS  bit;
};

struct SOCPRICTL_BITS {                       // bits description
    Uint32 SOCPRIORITY:5;                     // 4:0 Start-of-conversion Priority
    Uint32 RRPOINTER:6;                       // 10:5 Round Robin Pointer
    Uint32 rsvd1:1;                           // 11 reserved
    Uint32 ONESHOT:1;                         // 12 ONESHOT Mode
    Uint32 rsvd2:19;                          // 31:13 reserved
};

union SOCPRICTL_REG {
    Uint32  all;
    struct  SOCPRICTL_BITS  bit;
};

struct ADCSAMPLEMODE_BITS {                   // bits description
    Uint32 SIMULEN0:1;                        // 0 Simutaneous Sampling Enable for SOC0/SOC1
    Uint32 SIMULEN2:1;                        // 1 Simutaneous Sampling Enable for SOC2/SOC3
    Uint32 SIMULEN4:1;                        // 2 Simutaneous Sampling Enable for SOC4/SOC5
    Uint32 SIMULEN6:1;                        // 3 Simutaneous Sampling Enable for SOC6/SOC7
    Uint32 SIMULEN8:1;                        // 4 Simutaneous Sampling Enable for SOC8/SOC9
    Uint32 SIMULEN10:1;                       // 5 Simutaneous Sampling Enable for SOC10/SOC11
    Uint32 SIMULEN12:1;                       // 6 Simutaneous Sampling Enable for SOC12/SOC13
    Uint32 SIMULEN14:1;                       // 7 Simutaneous Sampling Enable for SOC14/SOC15
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSAMPLEMODE_REG {
    Uint32  all;
    struct  ADCSAMPLEMODE_BITS  bit;
};

struct ADCINTSOCSEL1_BITS {                   // bits description
    Uint32 SOC0:2;                            // 1:0 SOC0 ADC Interrupt Trigger Select
    Uint32 SOC1:2;                            // 3:2 SOC1 ADC Interrupt Trigger Select
    Uint32 SOC2:2;                            // 5:4 SOC2 ADC Interrupt Trigger Select
    Uint32 SOC3:2;                            // 7:6 SOC3 ADC Interrupt Trigger Select
    Uint32 SOC4:2;                            // 9:8 SOC4 ADC Interrupt Trigger Select
    Uint32 SOC5:2;                            // 11:10 SOC5 ADC Interrupt Trigger Select
    Uint32 SOC6:2;                            // 13:12 SOC6 ADC Interrupt Trigger Select
    Uint32 SOC7:2;                            // 15:14 SOC7 ADC Interrupt Trigger Select
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCINTSOCSEL1_REG {
    Uint32  all;
    struct  ADCINTSOCSEL1_BITS  bit;
};

struct ADCINTSOCSEL2_BITS {                   // bits description
    Uint32 SOC8:2;                            // 1:0 SOC8 ADC Interrupt Trigger Select
    Uint32 SOC9:2;                            // 3:2 SOC9 ADC Interrupt Trigger Select
    Uint32 SOC10:2;                           // 5:4 SOC10 ADC Interrupt Trigger Select
    Uint32 SOC11:2;                           // 7:6 SOC11 ADC Interrupt Trigger Select
    Uint32 SOC12:2;                           // 9:8 SOC12 ADC Interrupt Trigger Select
    Uint32 SOC13:2;                           // 11:10 SOC13 ADC Interrupt Trigger Select
    Uint32 SOC14:2;                           // 13:12 SOC14 ADC Interrupt Trigger Select
    Uint32 SOC15:2;                           // 15:14 SOC15 ADC Interrupt Trigger Select
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCINTSOCSEL2_REG {
    Uint32  all;
    struct  ADCINTSOCSEL2_BITS  bit;
};

struct ADCSOCFLG1_BITS {                      // bits description
    Uint32 SOC0:1;                            // 0 Start-of-conversion for CONV0
    Uint32 SOC1:1;                            // 1 Start-of-conversion for CONV1
    Uint32 SOC2:1;                            // 2 Start-of-conversion for CONV2
    Uint32 SOC3:1;                            // 3 Start-of-conversion for CONV3
    Uint32 SOC4:1;                            // 4 Start-of-conversion for CONV4
    Uint32 SOC5:1;                            // 5 Start-of-conversion for CONV5
    Uint32 SOC6:1;                            // 6 Start-of-conversion for CONV6
    Uint32 SOC7:1;                            // 7 Start-of-conversion for CONV7
    Uint32 SOC8:1;                            // 8 Start-of-conversion for CONV8
    Uint32 SOC9:1;                            // 9 Start-of-conversion for CONV9
    Uint32 SOC10:1;                           // 10 Start-of-conversion for CONV10
    Uint32 SOC11:1;                           // 11 Start-of-conversion for CONV11
    Uint32 SOC12:1;                           // 12 Start-of-conversion for CONV12
    Uint32 SOC13:1;                           // 13 Start-of-conversion for CONV13
    Uint32 SOC14:1;                           // 14 Start-of-conversion for CONV14
    Uint32 SOC15:1;                           // 15 Start-of-conversion for CONV15
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCSOCFLG1_REG {
    Uint32  all;
    struct  ADCSOCFLG1_BITS  bit;
};

struct ADCSOCFRC1_BITS {                      // bits description
    Uint32 SOC0:1;                            // 0 Start-of-conversion for CONV0
    Uint32 SOC1:1;                            // 1 Start-of-conversion for CONV1
    Uint32 SOC2:1;                            // 2 Start-of-conversion for CONV2
    Uint32 SOC3:1;                            // 3 Start-of-conversion for CONV3
    Uint32 SOC4:1;                            // 4 Start-of-conversion for CONV4
    Uint32 SOC5:1;                            // 5 Start-of-conversion for CONV5
    Uint32 SOC6:1;                            // 6 Start-of-conversion for CONV6
    Uint32 SOC7:1;                            // 7 Start-of-conversion for CONV7
    Uint32 SOC8:1;                            // 8 Start-of-conversion for CONV8
    Uint32 SOC9:1;                            // 9 Start-of-conversion for CONV9
    Uint32 SOC10:1;                           // 10 Start-of-conversion for CONV10
    Uint32 SOC11:1;                           // 11 Start-of-conversion for CONV11
    Uint32 SOC12:1;                           // 12 Start-of-conversion for CONV12
    Uint32 SOC13:1;                           // 13 Start-of-conversion for CONV13
    Uint32 SOC14:1;                           // 14 Start-of-conversion for CONV14
    Uint32 SOC15:1;                           // 15 Start-of-conversion for CONV15
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCSOCFRC1_REG {
    Uint32  all;
};

struct ADCSOCOVF1_BITS {                      // bits description
    Uint32 SOC0:1;                            // 0 Start-of-conversion for CONV0
    Uint32 SOC1:1;                            // 1 Start-of-conversion for CONV1
    Uint32 SOC2:1;                            // 2 Start-of-conversion for CONV2
    Uint32 SOC3:1;                            // 3 Start-of-conversion for CONV3
    Uint32 SOC4:1;                            // 4 Start-of-conversion for CONV4
    Uint32 SOC5:1;                            // 5 Start-of-conversion for CONV5
    Uint32 SOC6:1;                            // 6 Start-of-conversion for CONV6
    Uint32 SOC7:1;                            // 7 Start-of-conversion for CONV7
    Uint32 SOC8:1;                            // 8 Start-of-conversion for CONV8
    Uint32 SOC9:1;                            // 9 Start-of-conversion for CONV9
    Uint32 SOC10:1;                           // 10 Start-of-conversion for CONV10
    Uint32 SOC11:1;                           // 11 Start-of-conversion for CONV11
    Uint32 SOC12:1;                           // 12 Start-of-conversion for CONV12
    Uint32 SOC13:1;                           // 13 Start-of-conversion for CONV13
    Uint32 SOC14:1;                           // 14 Start-of-conversion for CONV14
    Uint32 SOC15:1;                           // 15 Start-of-conversion for CONV15
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCSOCOVF1_REG {
    Uint32  all;
    struct  ADCSOCOVF1_BITS  bit;
};

struct ADCSOCOVFCLR1_BITS {                   // bits description
    Uint32 SOC0:1;                            // 0 Start-of-conversion for CONV0
    Uint32 SOC1:1;                            // 1 Start-of-conversion for CONV1
    Uint32 SOC2:1;                            // 2 Start-of-conversion for CONV2
    Uint32 SOC3:1;                            // 3 Start-of-conversion for CONV3
    Uint32 SOC4:1;                            // 4 Start-of-conversion for CONV4
    Uint32 SOC5:1;                            // 5 Start-of-conversion for CONV5
    Uint32 SOC6:1;                            // 6 Start-of-conversion for CONV6
    Uint32 SOC7:1;                            // 7 Start-of-conversion for CONV7
    Uint32 SOC8:1;                            // 8 Start-of-conversion for CONV8
    Uint32 SOC9:1;                            // 9 Start-of-conversion for CONV9
    Uint32 SOC10:1;                           // 10 Start-of-conversion for CONV10
    Uint32 SOC11:1;                           // 11 Start-of-conversion for CONV11
    Uint32 SOC12:1;                           // 12 Start-of-conversion for CONV12
    Uint32 SOC13:1;                           // 13 Start-of-conversion for CONV13
    Uint32 SOC14:1;                           // 14 Start-of-conversion for CONV14
    Uint32 SOC15:1;                           // 15 Start-of-conversion for CONV15
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCSOCOVFCLR1_REG {
    Uint32  all;
};

struct ADCSOC0CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC0CTL_REG {
    Uint32  all;
    struct  ADCSOC0CTL_BITS  bit;
};

struct ADCSOC1CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC1CTL_REG {
    Uint32  all;
    struct  ADCSOC1CTL_BITS  bit;
};

struct ADCSOC2CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC2CTL_REG {
    Uint32  all;
    struct  ADCSOC2CTL_BITS  bit;
};

struct ADCSOC3CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC3CTL_REG {
    Uint32  all;
    struct  ADCSOC3CTL_BITS  bit;
};

struct ADCSOC4CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC4CTL_REG {
    Uint32  all;
    struct  ADCSOC4CTL_BITS  bit;
};

struct ADCSOC5CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC5CTL_REG {
    Uint32  all;
    struct  ADCSOC5CTL_BITS  bit;
};

struct ADCSOC6CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC6CTL_REG {
    Uint32  all;
    struct  ADCSOC6CTL_BITS  bit;
};

struct ADCSOC7CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC7CTL_REG {
    Uint32  all;
    struct  ADCSOC7CTL_BITS  bit;
};

struct ADCSOC8CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC8CTL_REG {
    Uint32  all;
    struct  ADCSOC8CTL_BITS  bit;
};

struct ADCSOC9CTL_BITS {                      // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC9CTL_REG {
    Uint32  all;
    struct  ADCSOC9CTL_BITS  bit;
};

struct ADCSOC10CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC10CTL_REG {
    Uint32  all;
    struct  ADCSOC10CTL_BITS  bit;
};

struct ADCSOC11CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC11CTL_REG {
    Uint32  all;
    struct  ADCSOC11CTL_BITS  bit;
};

struct ADCSOC12CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC12CTL_REG {
    Uint32  all;
    struct  ADCSOC12CTL_BITS  bit;
};

struct ADCSOC13CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC13CTL_REG {
    Uint32  all;
    struct  ADCSOC13CTL_BITS  bit;
};

struct ADCSOC14CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC14CTL_REG {
    Uint32  all;
    struct  ADCSOC14CTL_BITS  bit;
};

struct ADCSOC15CTL_BITS {                     // bits description
    Uint32 ACQPS:6;                           // 5:0 Acquisition Pulse Size
    Uint32 CHSEL:4;                           // 9:6 SOCx Channel Select
    Uint32 rsvd1:1;                           // 10 reserved
    Uint32 TRIGSEL:5;                         // 15:11 SOCx Trigger Select
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ADCSOC15CTL_REG {
    Uint32  all;
    struct  ADCSOC15CTL_BITS  bit;
};

struct ADCREFTRIM_BITS {                      // bits description
    Uint32 BG_FINE_TRIM:5;                    // 4:0 Coarse trim for internal BG
    Uint32 BG_CAORSE_TRIM:4;                  // 8:5 Fine trim for internal BG
    Uint32 EXTREF_FINE_TRIM:5;                // 13:9 Fine trim for external reference
    Uint32 rsvd1:18;                          // 31:14 reserved
};

union ADCREFTRIM_REG {
    Uint32  all;
    struct  ADCREFTRIM_BITS  bit;
};

struct ADCOFFTRIM_BITS {                      // bits description
    Uint32 OFFTRIM:9;                         // 8:0 Offset Trim
    Uint32 rsvd1:9;                           // 17:9 reserved
    Uint32 rsvd2:14;                          // 31:18 reserved
};

union ADCOFFTRIM_REG {
    Uint32  all;
    struct  ADCOFFTRIM_BITS  bit;
};

struct COMPHYSTCTL_BITS {                     // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 COMP1_HYST_DISABLE:1;              // 1 Comparator 1 Hysteresis Disable
    Uint32 COMP1_HYST_SEL:2;                  // 3:2 cmp1_hyst_sel new
    Uint32 rsvd2:2;                           // 5:4 reserved
    Uint32 COMP2_HYST_DISABLE:1;              // 6 Comparator 2 Hysteresis Disable
    Uint32 COMP2_HYST_SEL:2;                  // 8:7 cmp2_hyst_sel new
    Uint32 rsvd3:2;                           // 10:9 reserved
    Uint32 COMP3_HYST_DISABLE:1;              // 11 Comparator 3 Hysteresis Disable 28034
    Uint32 COMP3_HYST_SEL:2;                  // 13:12 cmp3_hyst_sel 28034
    Uint32 rsvd4:18;                          // 31:14 reserved
};

union COMPHYSTCTL_REG {
    Uint32  all;
    struct  COMPHYSTCTL_BITS  bit;
};

struct ADCREV_BITS {                          // bits description
    Uint32 TYPE_T:8;                          // 7:0 TYPE
    Uint32 REV:8;                             // 15:8 version
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union ADCREV_REG {
    Uint32  all;
    struct  ADCREV_BITS  bit;
};

struct  ADC_REGS {
    union   ADCCTL1_REG                      ADCCTL1;                     // 0x0 0x0 ADC Control 1
    union   ADCCTL2_REG                      ADCCTL2;                     // 0x4 0x4 ADC Control 2
    union   ADCINTFLG_REG                    ADCINTFLG;                   // 0x8 0x8 ADC Interrupt Flag
    union   ADCINTFLGCLR_REG                 ADCINTFLGCLR;                // 0xc 0xc ADC Interrupt Flag Clear
    union   ADCINTOVF_REG                    ADCINTOVF;                   // 0x10 0x10 ADC Interrupt Overflow
    union   ADCINTOVFCLR_REG                 ADCINTOVFCLR;                // 0x14 0x14 ADC Interrupt Overflow Clear
    union   INTSEL1N2_REG                    INTSEL1N2;                   // 0x18 0x18 ADC Interrupt 1 and 2 Selection
    union   INTSEL3N4_REG                    INTSEL3N4;                   // 0x1c 0x1c ADC Interrupt 3 and 4 Selection
    union   INTSEL5N6_REG                    INTSEL5N6;                   // 0x20 0x20 ADC Interrupt 5 and 6 Selection
    union   INTSEL7N8_REG                    INTSEL7N8;                   // 0x24 0x24 ADC Interrupt 7 and 8 Selection
    union   INTSEL9N10_REG                   INTSEL9N10;                  // 0x28 0x28 ADC Interrupt 9 and 10 Selection
    union   SOCPRICTL_REG                    SOCPRICTL;                   // 0x2c 0x2c ADC SOC Priority control
    union   ADCSAMPLEMODE_REG                ADCSAMPLEMODE;               // 0x30 0x30 ADC Sampling Mode
    union   ADCINTSOCSEL1_REG                ADCINTSOCSEL1;               // 0x34 0x34 ADC Interrupt SOC Selection 1
    union   ADCINTSOCSEL2_REG                ADCINTSOCSEL2;               // 0x38 0x38 ADC Interrupt SOC Selection 2
    union   ADCSOCFLG1_REG                   ADCSOCFLG1;                  // 0x3c 0x3c ADC SOC Flag 1
    union   ADCSOCFRC1_REG                   ADCSOCFRC1;                  // 0x40 0x40 ADC SOC Flag Force 1
    union   ADCSOCOVF1_REG                   ADCSOCOVF1;                  // 0x44 0x44 ADC SOC Overflow 1
    union   ADCSOCOVFCLR1_REG                ADCSOCOVFCLR1;               // 0x48 0x48 ADC SOC Overflow Clear 1
    union   ADCSOC0CTL_REG                   ADCSOC0CTL;                  // 0x4c 0x4c ADC SOC0 Control
    union   ADCSOC1CTL_REG                   ADCSOC1CTL;                  // 0x50 0x50 ADC SOC1 Control
    union   ADCSOC2CTL_REG                   ADCSOC2CTL;                  // 0x54 0x54 ADC SOC2 Control
    union   ADCSOC3CTL_REG                   ADCSOC3CTL;                  // 0x58 0x58 ADC SOC3 Control
    union   ADCSOC4CTL_REG                   ADCSOC4CTL;                  // 0x5c 0x5c ADC SOC4 Control
    union   ADCSOC5CTL_REG                   ADCSOC5CTL;                  // 0x60 0x60 ADC SOC5 Control
    union   ADCSOC6CTL_REG                   ADCSOC6CTL;                  // 0x64 0x64 ADC SOC6 Control
    union   ADCSOC7CTL_REG                   ADCSOC7CTL;                  // 0x68 0x68 ADC SOC7 Control
    union   ADCSOC8CTL_REG                   ADCSOC8CTL;                  // 0x6c 0x6c ADC SOC8 Control
    union   ADCSOC9CTL_REG                   ADCSOC9CTL;                  // 0x70 0x70 ADC SOC9 Control
    union   ADCSOC10CTL_REG                  ADCSOC10CTL;                 // 0x74 0x74 ADC SOC10 Control
    union   ADCSOC11CTL_REG                  ADCSOC11CTL;                 // 0x78 0x78 ADC SOC11 Control
    union   ADCSOC12CTL_REG                  ADCSOC12CTL;                 // 0x7c 0x7c ADC SOC12 Control
    union   ADCSOC13CTL_REG                  ADCSOC13CTL;                 // 0x80 0x80 ADC SOC13 Control
    union   ADCSOC14CTL_REG                  ADCSOC14CTL;                 // 0x84 0x84 ADC SOC14 Control
    union   ADCSOC15CTL_REG                  ADCSOC15CTL;                 // 0x88 0x88 ADC SOC15 Control
    union   ADCREFTRIM_REG                   ADCREFTRIM;                  // 0x8c 0x8c Reference Trim Register
    union   ADCOFFTRIM_REG                   ADCOFFTRIM;                  // 0x90 0x90 Offset Trim Register
    union   COMPHYSTCTL_REG                  COMPHYSTCTL;                 // 0x94 0x94 COMP Hysteresis Control Register
    union   ADCREV_REG                       ADCREV;                      // 0x98 0x98
};

struct  ADC_RESULT_REGS {
    Uint32                                   ADCRESULT0;                  // 0x0 0x0 Conversion Result Buffer 0
    Uint32                                   ADCRESULT1;                  // 0x4 0x4 Conversion Result Buffer 1
    Uint32                                   ADCRESULT2;                  // 0x8 0x8 Conversion Result Buffer 2
    Uint32                                   ADCRESULT3;                  // 0xc 0xc Conversion Result Buffer 3
    Uint32                                   ADCRESULT4;                  // 0x10 0x10 Conversion Result Buffer 4
    Uint32                                   ADCRESULT5;                  // 0x14 0x14 Conversion Result Buffer 5
    Uint32                                   ADCRESULT6;                  // 0x18 0x18 Conversion Result Buffer 6
    Uint32                                   ADCRESULT7;                  // 0x1c 0x1c Conversion Result Buffer 7
    Uint32                                   ADCRESULT8;                  // 0x20 0x20 Conversion Result Buffer 8
    Uint32                                   ADCRESULT9;                  // 0x24 0x24 Conversion Result Buffer 9
    Uint32                                   ADCRESULT10;                 // 0x28 0x28 Conversion Result Buffer 10
    Uint32                                   ADCRESULT11;                 // 0x2c 0x2c Conversion Result Buffer 11
    Uint32                                   ADCRESULT12;                 // 0x30 0x30 Conversion Result Buffer 12
    Uint32                                   ADCRESULT13;                 // 0x34 0x34 Conversion Result Buffer 13
    Uint32                                   ADCRESULT14;                 // 0x38 0x38 Conversion Result Buffer 14
    Uint32                                   ADCRESULT15;                 // 0x3c 0x3c Conversion Result Buffer 15
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================