#ifndef epwm_h_
#define epwm_h_

#include "HXM32G407_config.h"

void InitEPwm1Example(void);
void InitEPwm2Example(void);

void INTERRUPT epwm1_tz_isr(void);
void INTERRUPT epwm2_tz_isr(void);

extern Uint16 EPwm_TZ_CBC_flag;

void InitLED(void);

#endif/*epwm_h_*/
