#include "HXM32G407_config.h"
/******************************************************************
 *函数名：void InitLED()
 *参 数：无
 *返回值：无
 *作 用：初始化LED
 ******************************************************************/
void InitLED(void)
{
	/*允许访问受保护的空间*/
	EALLOW;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;

	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;
	/*禁止访问受保护的空间*/
	EDIS;
}
