//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-29 09:40:27.898804
//
//#############################################################################


#ifndef HXM32G407_SYSCTRL_H
#define HXM32G407_SYSCTRL_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// SYSCTRL Individual Register Bit Definitions:

struct BORCFG_BITS {                          // bits description
    Uint32 rsvd1:3;                           // 2:0 reserved
    Uint32 DIV_T:3;                           // 5:3 BOR Trigger point
    Uint32 rsvd2:26;                          // 31:6 reserved
};

union BORCFG_REG {
    Uint32  all;
    struct  BORCFG_BITS  bit;
};

struct  SYS_PWR_CTRL_REGS {
    union   BORCFG_REG                       BORCFG;                      // 0x0 0: BOR Configuration Register
};

struct INTOSC1TRIMEN_BITS {                   // bits description
    Uint32 REF_DIV:5;                         // 4:0  
    Uint32 TRIM_EN:1;                         // 5 Config trim enable
    Uint32 rsvd1:1;                           // 6 reserved
    Uint32 TRIM_DONE:1;                       // 7 Trim done
    Uint32 TRIM_DATA_OUT:8;                   // 15:8 Trim data out
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTOSC1TRIMEN_REG {
    Uint32  all;
    struct  INTOSC1TRIMEN_BITS  bit;
};

struct INTOSC2TRIMEN_BITS {                   // bits description
    Uint32 REF_DIV:5;                         // 4:0  
    Uint32 TRIM_EN:1;                         // 5 Config trim enable
    Uint32 rsvd1:1;                           // 6 reserved
    Uint32 TRIM_DONE:1;                       // 7 Trim done
    Uint32 TRIM_DATA_OUT:8;                   // 15:8 Trim data out
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTOSC2TRIMEN_REG {
    Uint32  all;
    struct  INTOSC2TRIMEN_BITS  bit;
};

struct PLLCFG_BITS {                          // bits description
    Uint32 PLL1_M:8;                          // 7:0 Directly configure the M value of PLL1
    Uint32 PLL1_postdiv:3;                    // 10:8 Directly configure the posdiv value of PLL1
    Uint32 PLL1_OD:2;                         // 12:11 Directly configure the OD value of PLL1
    Uint32 PLL1_N:4;                          // 16:13 Directly configure the N value of PLL1
    Uint32 PLL0_M:6;                          // 22:17 Directly configure the M value of PLL0
    Uint32 PLL0_OD:2;                         // 24:23 Directly configure the OD value of PLL0
    Uint32 PLL0_N:2;                          // 26:25 Directly configure the N value of PLL0
    Uint32 rsvd1:4;                           // 30:27 reserved
    Uint32 PLL_CFG_BPEN:1;                    // 31 Bypass indirect configuration function is used instead of direct configuration function
};

union PLLCFG_REG {
    Uint32  all;
    struct  PLLCFG_BITS  bit;
};

struct INTOSC1STAT_BITS {                     // bits description
    Uint32 OSC_ON:1;                          // 0  
    Uint32 rsvd1:15;                          // 15:1 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTOSC1STAT_REG {
    Uint32  all;
    struct  INTOSC1STAT_BITS  bit;
};

struct INTOSC2STAT_BITS {                     // bits description
    Uint32 OSC_ON:1;                          // 0  
    Uint32 rsvd1:15;                          // 15:1 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTOSC2STAT_REG {
    Uint32  all;
    struct  INTOSC2STAT_BITS  bit;
};

struct BGRVTRIM_BITS {                        // bits description
    Uint32 BGRV_TRIM:5;                       // 4:0  
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union BGRVTRIM_REG {
    Uint32  all;
    struct  BGRVTRIM_BITS  bit;
};

struct BGRITRIM_BITS {                        // bits description
    Uint32 BGRI_TRIM:4;                       // 3:0  
    Uint32 rsvd1:12;                          // 15:4 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union BGRITRIM_REG {
    Uint32  all;
    struct  BGRITRIM_BITS  bit;
};

struct ANAMUX_BITS {                          // bits description
    Uint32 ANALOG_MUXCTRL0:1;                 // 0  
    Uint32 ANALOG_MUXCTRL1:1;                 // 1  
    Uint32 rsvd1:14;                          // 15:2 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union ANAMUX_REG {
    Uint32  all;
    struct  ANAMUX_BITS  bit;
};

struct BOR18TRIM_BITS {                       // bits description
    Uint32 V1P8_BOR_TRIM:2;                   // 1:0  
    Uint32 rsvd1:14;                          // 15:2 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union BOR18TRIM_REG {
    Uint32  all;
    struct  BOR18TRIM_BITS  bit;
};

struct BOR33TRIM_BITS {                       // bits description
    Uint32 V3P3_BOR_TRIM:3;                   // 2:0  
    Uint32 rsvd1:13;                          // 15:3 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union BOR33TRIM_REG {
    Uint32  all;
    struct  BOR33TRIM_BITS  bit;
};

struct LDOTRIM_BITS {                         // bits description
    Uint32 LDO_TRIM:4;                        // 3:0  
    Uint32 rsvd1:12;                          // 15:4 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union LDOTRIM_REG {
    Uint32  all;
    struct  LDOTRIM_BITS  bit;
};

struct VREGTRIM_BITS {                        // bits description
    Uint32 VREG_TRIM:4;                       // 3:0  
    Uint32 rsvd1:12;                          // 15:4 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union VREGTRIM_REG {
    Uint32  all;
    struct  VREGTRIM_BITS  bit;
};

struct BOREN_BITS {                           // bits description
    Uint32 BOR_EN:1;                          // 0  
    Uint32 rsvd1:15;                          // 15:1 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union BOREN_REG {
    Uint32  all;
    struct  BOREN_BITS  bit;
};

struct  SYS_LDO_OSC_CTRL_REGS {
    union   INTOSC1TRIMEN_REG                INTOSC1TRIMEN;               // 0x0  
    union   INTOSC2TRIMEN_REG                INTOSC2TRIMEN;               // 0x4  
    union   PLLCFG_REG                       PLLCFG;                      // 0x8  
    union   INTOSC1STAT_REG                  INTOSC1STAT;                 // 0xc  
    union   INTOSC2STAT_REG                  INTOSC2STAT;                 // 0x10  
    union   BGRVTRIM_REG                     BGRVTRIM;                    // 0x14  
    union   BGRITRIM_REG                     BGRITRIM;                    // 0x18  
    union   ANAMUX_REG                       ANAMUX;                      // 0x1c  
    union   BOR18TRIM_REG                    BOR18TRIM;                   // 0x20  
    union   BOR33TRIM_REG                    BOR33TRIM;                   // 0x24  
    union   LDOTRIM_REG                      LDOTRIM;                     // 0x28  
    union   VREGTRIM_REG                     VREGTRIM;                    // 0x2c  
    union   BOREN_REG                        BOREN;                       // 0x30  
};

struct XCLK_BITS {                            // bits description
    Uint32 XCLKOUTDIV:2;                      // 1:0 XCLKOUT Divide Ratio
    Uint32 rsvd1:4;                           // 5:2 reserved
    Uint32 XCLKINSEL:1;                       // 6 XCLKIN Source Select bit
    Uint32 rsvd2:25;                          // 31:7 reserved
};

union XCLK_REG {
    Uint32  all;
    struct  XCLK_BITS  bit;
};

struct PLLSTS_BITS {                          // bits description
    Uint32 PLLLOCKS:1;                        // 0 PLL lock status
    Uint32 rsvd1:1;                           // 1 reserved
    Uint32 PLLOFF:1;                          // 2 PLL off bit
    Uint32 rsvd2:1;                           // 3 reserved
    Uint32 MCLKCLR:1;                         // 4 reserved
    Uint32 OSCOFF:1;                          // 5 Oscillator clock off
    Uint32 MCLKOFF:1;                         // 6 Missing clock detect
    Uint32 DIVSEL:2;                          // 8:7 Divide select (/4 default)
    Uint32 rsvd3:6;                           // 14:9 reserved
    Uint32 NORMRDYE:1;                        // 15 VREG NORMRDY enable bit
    Uint32 rsvd4:16;                          // 31:16  
};

union PLLSTS_REG {
    Uint32  all;
    struct  PLLSTS_BITS  bit;
};

struct PCLKCR0_BITS {                         // bits description
    Uint32 HRPWMENCLK:1;                      // 0 Enable low speed clk to HRPWM
    Uint32 LINAENCLK:1;                       // 1 Enable LIN clock
    Uint32 TBCLKSYNC:1;                       // 2 ETWPM Module TBCLK enable/sync
    Uint32 ADCENCLK:1;                        // 3 Enable high speed clk to ADC
    Uint32 I2CAENCLK:1;                       // 4 Enable SYSCLKOUT to I2C-A
    Uint32 rsvd1:2;                           // 6:5 reserved
    Uint32 ECAN_LINCLKDIV4:1;                 // 7  
    Uint32 SPIAENCLK:1;                       // 8 Enable low speed clk to SPI-A
    Uint32 SPIBENCLK:1;                       // 9 Enable low speed clk to SPI-B
    Uint32 SCIAENCLK:1;                       // 10 Enable low speed clk to SCI-A
    Uint32 rsvd2:3;                           // 13:11 reserved
    Uint32 ECANAENCLK:1;                      // 14 ECAN-A clock enable
    Uint32 rsvd3:17;                          // 31:15 reserved
};

union PCLKCR0_REG {
    Uint32  all;
    struct  PCLKCR0_BITS  bit;
};

struct PCLKCR1_BITS {                         // bits description
    Uint32 EPWM1ENCLK:1;                      // 0 Enable SYSCLKOUT to EPWM1
    Uint32 EPWM2ENCLK:1;                      // 1 Enable SYSCLKOUT to EPWM2
    Uint32 EPWM3ENCLK:1;                      // 2 Enable SYSCLKOUT to EPWM3
    Uint32 EPWM4ENCLK:1;                      // 3 Enable SYSCLKOUT to EPWM4
    Uint32 EPWM5ENCLK:1;                      // 4 Enable SYSCLKOUT to EPWM5
    Uint32 EPWM6ENCLK:1;                      // 5 Enable SYSCLKOUT to EPWM6
    Uint32 EPWM7ENCLK:1;                      // 6 Enable SYSCLKOUT to EPWM7
    Uint32 rsvd1:1;                           // 7 reserved
    Uint32 ECAP1ENCLK:1;                      // 8 Enable SYSCLKOUT to ECAP1
    Uint32 rsvd2:5;                           // 13:9 reserved
    Uint32 EQEP1ENCLK:1;                      // 14 enable eqep1
    Uint32 EQEP2ENCLK:1;                      // 15 enable eqep2
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union PCLKCR1_REG {
    Uint32  all;
    struct  PCLKCR1_BITS  bit;
};

struct PCLKCR2_BITS {                         // bits description
    Uint32 rsvd1:8;                           // 7:0 reserved
    Uint32 HRCAP1ENCLK:1;                     // 8 enable hrcap1 clock
    Uint32 HRCAP2ENCLK:1;                     // 9 enable hrcap2 clock
    Uint32 rsvd2:22;                          // 31:10 reserved
};

union PCLKCR2_REG {
    Uint32  all;
    struct  PCLKCR2_BITS  bit;
};

struct PCLKCR3_BITS {                         // bits description
    Uint32 rsvd1:3;                           // 2:0 reserved
    Uint32 rsvd2:5;                           // 7:3 reserved
    Uint32 CPUTIMER0ENCLK:1;                  // 8 Enable SYSCLKOUT to CPUTIMER0
    Uint32 CPUTIMER1ENCLK:1;                  // 9 Enable SYSCLKOUT to CPUTIMER1
    Uint32 CPUTIMER2ENCLK:1;                  // 10 Enable SYSCLKOUT to CPUTIMER2
    Uint32 rsvd3:2;                           // 12:11 reserved
    Uint32 GPIOINENCLK:1;                     // 13 Enable SYSCLKOUT to GPIO
    Uint32 rsvd4:2;                           // 15:14  
    Uint32 rsvd5:16;                          // 31:16 reserved
};

union PCLKCR3_REG {
    Uint32  all;
    struct  PCLKCR3_BITS  bit;
};

struct LOSPCP_BITS {                          // bits description
    Uint32 LSPCLK:4;                          // 3:0 Rate relative to SYSCLKOUT
    Uint32 rsvd1:28;                          // 31:4 reserved
};

union LOSPCP_REG {
    Uint32  all;
    struct  LOSPCP_BITS  bit;
};

struct INTOSC1TRIM_BITS {                     // bits description
    Uint32 TRIM_DATA:8;                       // 7:0 Trim data
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union INTOSC1TRIM_REG {
    Uint32  all;
    struct  INTOSC1TRIM_BITS  bit;
};

struct INTOSC2TRIM_BITS {                     // bits description
    Uint32 TRIM_DATA:8;                       // 7:0 Trim data
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16 5-bit fine trim value
};

union INTOSC2TRIM_REG {
    Uint32  all;
    struct  INTOSC2TRIM_BITS  bit;
};

struct CLKCTL_BITS {                          // bits description
    Uint32 OSCCLKSRCSEL:1;                    // 0 Oscillator clock source select bit
    Uint32 OSCCLKSRC2SEL:1;                   // 1 Oscillator 2 clock source select bit
    Uint32 WDCLKSRCSEL:1;                     // 2 Watchdog clock source select bit
    Uint32 TMR2CLKSRCSEL:2;                   // 4:3 CPU timer 2 clock source select bit
    Uint32 TMR2CLKPRESCALE:3;                 // 7:5 CPU timer 2 clock pre-scale value
    Uint32 rsvd1:1;                           // 8  
    Uint32 INTOSC1HALTI:1;                    // 9 Internal oscillator 1 halt mode ignore bit
    Uint32 rsvd2:1;                           // 10  
    Uint32 INTOSC2HALTI:1;                    // 11 Internal oscillator 2 halt mode ignore bit
    Uint32 WDHALTI:1;                         // 12 Watchdog halt mode ignore bit
    Uint32 XCLKINOFF:1;                       // 13 XCLKIN off bit
    Uint32 XTALOSCOFF:1;                      // 14 Crystal (External) oscillator off bit
    Uint32 NMIRESETSEL:1;                     // 15 NMI reset select bit
    Uint32 rsvd3:16;                          // 31:16  
};

union CLKCTL_REG {
    Uint32  all;
    struct  CLKCTL_BITS  bit;
};

struct PLLCR_BITS {                           // bits description
    Uint32 DIVF:5;                            // 4:0 Set clock ratio for the PLL
    Uint32 DIVN:1;                            // 5 Pre-divider of reference clock(CLKIN)
    Uint32 DIVM:2;                            // 7:6 VCO output divider control signal
    Uint32 rsvd1:24;                          // 31:8 reserved
};

union PLLCR_REG {
    Uint32  all;
    struct  PLLCR_BITS  bit;
};

struct LPMCR0_BITS {                          // bits description
    Uint32 LPM:2;                             // 1:0 Set the low power mode
    Uint32 QUALSTDBY:6;                       // 7:2 Qualification
    Uint32 rsvd1:7;                           // 14:8 reserved
    Uint32 WDINTE:1;                          // 15 Enables WD to wake the device from STANDBY
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union LPMCR0_REG {
    Uint32  all;
    struct  LPMCR0_BITS  bit;
};

struct  SYS_CTRL_REGS {
    union   XCLK_REG                         XCLK;                        // 0x0 XCLKOUT Control 00
    union   PLLSTS_REG                       PLLSTS;                      // 0x4 PLL Status Register 04
    union   PCLKCR0_REG                      PCLKCR0;                     // 0x8 Peripheral clock control register 08
    union   PCLKCR1_REG                      PCLKCR1;                     // 0xc Peripheral clock control register 0C
    union   PCLKCR2_REG                      PCLKCR2;                     // 0x10 Peripheral clock control register 10 10
    union   PCLKCR3_REG                      PCLKCR3;                     // 0x14 Peripheral clock control register 14
    union   LOSPCP_REG                       LOSPCP;                      // 0x18 Low-speed peripheral clock pre-scaler 18
    union   INTOSC1TRIM_REG                  INTOSC1TRIM;                 // 0x1c Internal Oscillator 1 Trim 1C
    union   INTOSC2TRIM_REG                  INTOSC2TRIM;                 // 0x20 Internal Oscillator 2 Trim 20
    union   CLKCTL_REG                       CLKCTL;                      // 0x24 Clock Control Register 24
    union   PLLCR_REG                        PLLCR;                       // 0x28 PLL control register 28
    Uint32                                   PLLLOCKPRD;                  // 0x2c PLL Lock Period Register 2C
    union   LPMCR0_REG                       LPMCR0;                      // 0x30 Low-power mode control register 0 30
    Uint32                                   SCSR;                        // 0x34 System control and status register 34
    Uint32                                   WDCNTR;                      // 0x38 WD counter register 38
    Uint32                                   WDKEY;                       // 0x3c WD reset key register 3C
    Uint32                                   WDCR;                        // 0x40 Watchdog Control Register 40
    Uint32                                   JTAG_DEBUG;                  // 0x44 44
};

struct CSMSCR_BITS {                          // bits description
    Uint32 SECURE:1;                          // 0 Secure flag
    Uint32 rsvd1:30;                          // 30:1 reserved
    Uint32 FORCESEC:1;                        // 31  
};

union CSMSCR_REG {
    Uint32  all;
    struct  CSMSCR_BITS  bit;
};

struct  CSM_REGS {
    union   CSMSCR_REG                       CSMSCR;                      // 0x0 CSM Status & Control register 00
    Uint32                                   KEY0;                        // 0x4 KEY reg bits 31-0 04
    Uint32                                   KEY1;                        // 0x8 KEY reg bits 63-32 08
    Uint32                                   KEY2;                        // 0xc KEY reg bits 95-64 0C
    Uint32                                   KEY3;                        // 0x10 KEY reg bits 127-96 10
};

struct FOPT_BITS {                            // bits description
    Uint32 ENPIPE:1;                          // 0 Enable Pipeline Mode
    Uint32 rsvd1:31;                          // 31:1 reserved
};

union FOPT_REG {
    Uint32  all;
    struct  FOPT_BITS  bit;
};

struct FPWR_BITS {                            // bits description
    Uint32 PWR:2;                             // 1:0 Power Mode bits
    Uint32 rsvd1:30;                          // 31:2 reserved
};

union FPWR_REG {
    Uint32  all;
    struct  FPWR_BITS  bit;
};

struct FSTATUS_BITS {                         // bits description
    Uint32 PWRS:2;                            // 1:0 Power Mode Status bits
    Uint32 STDBYWAITS:1;                      // 2 Bank/Pump Sleep to Standby Wait Counter Status bits
    Uint32 ACTIVEWAITS:1;                     // 3 Bank/Pump Standby to Active Wait Counter Status bits
    Uint32 rsvd1:28;                          // 31:4 reserved
};

union FSTATUS_REG {
    Uint32  all;
    struct  FSTATUS_BITS  bit;
};

struct FREWSTAT_BITS {                        // bits description
    Uint32 FLASH_Busy:1;                      // 0  
    Uint32 FLASH_Init:1;                      // 1  
    Uint32 FLASH_read:1;                      // 2  
    Uint32 FLASH_erase:1;                     // 3  
    Uint32 FLASH_clear_latch:1;               // 4  
    Uint32 FLASH_write_page_latch:1;          // 5  
    Uint32 rsvd1:26;                          // 31:6  
};

union FREWSTAT_REG {
    Uint32  all;
    struct  FREWSTAT_BITS  bit;
};

struct FMPER_BITS {                           // bits description
    Uint32 page_id:10;                        // 9:0  
    Uint32 rsvd1:22;                          // 31:10  
};

union FMPER_REG {
    Uint32  all;
    struct  FMPER_BITS  bit;
};

struct FMSER_BITS {                           // bits description
    Uint32 sector_id:7;                       // 6:0  
    Uint32 rsvd1:25;                          // 31:7  
};

union FMSER_REG {
    Uint32  all;
    struct  FMSER_BITS  bit;
};

struct FMBER_BITS {                           // bits description
    Uint32 bank_id:2;                         // 1:0  
    Uint32 rsvd1:30;                          // 31:2  
};

union FMBER_REG {
    Uint32  all;
    struct  FMBER_BITS  bit;
};

struct FOTPER_BITS {                          // bits description
    Uint32 otp_id:4;                          // 3:0  
    Uint32 rsvd1:28;                          // 31:4  
};

union FOTPER_REG {
    Uint32  all;
    struct  FOTPER_BITS  bit;
};

struct FMPP_BITS {                            // bits description
    Uint32 page_id:10;                        // 9:0  
    Uint32 rsvd1:22;                          // 31:10  
};

union FMPP_REG {
    Uint32  all;
    struct  FMPP_BITS  bit;
};

struct FOTPP_BITS {                           // bits description
    Uint32 otp_id:4;                          // 3:0  
    Uint32 rsvd1:28;                          // 31:4  
};

union FOTPP_REG {
    Uint32  all;
    struct  FOTPP_BITS  bit;
};

struct  FLASH_REGS {
    struct CSM_REGS                          SYSCsmRegs;                  // 0x0 0
    Uint32                                   rsvd1[27];                   // 0x4 Reserved
    union   FOPT_REG                         FOPT;                        // 0x80 0x80
    union   FPWR_REG                         FPWR;                        // 0x84 0x84
    union   FSTATUS_REG                      FSTATUS;                     // 0x88 0x88
    union   FREWSTAT_REG                     FREWSTAT;                    // 0x8c 0x8C
    Uint32                                   FRWAIT;                      // 0x90 0x90
    Uint32                                   OTPER;                       // 0x94 0x94
    Uint32                                   FT3U;                        // 0x98 0x98
    Uint32                                   FT100N;                      // 0x9c 0x9C
    Uint32                                   FT60U;                       // 0xa0 0xA0
    Uint32                                   FT6U;                        // 0xa4 0xA4
    Uint32                                   FT15U;                       // 0xa8 0xA8
    Uint32                                   FT5U;                        // 0xac 0xAC
    Uint32                                   FT6M;                        // 0xb0 0xB0
    Uint32                                   FT2M;                        // 0xb4 0xB4
    Uint32                                   FT200U;                      // 0xb8 0xB8
    union   FMPER_REG                        FMPER;                       // 0xbc 0xBC
    union   FMSER_REG                        FMSER;                       // 0xc0 0xC0
    union   FMBER_REG                        FMBER;                       // 0xc4 0xC4
    Uint32                                   FMUER;                       // 0xc8 0xC8
    union   FOTPER_REG                       FOTPER;                      // 0xcc 0xCC
    Uint32                                   FCLRLAT;                     // 0xd0 0xD0
    union   FMPP_REG                         FMPP;                        // 0xd4 0xD4
    union   FOTPP_REG                        FOTPP;                       // 0xd8 0xD8
    Uint32                                   FWLATEN;                     // 0xdc 0xDC
};

struct  CSM_PWL {
   Uint32   PSWD0;  // PSWD bits 31:0
   Uint32   PSWD1;  // PSWD bits 63:32
   Uint32   PSWD2;  // PSWD bits 95:64
   Uint32   PSWD3;  // PSWD bits 127:96
};

//---------------------------------------------------------------------------
// System Control External References & Function Declarations:
//

#define CsmRegs   (P_FlashRegs->SYSCsmRegs)

#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
