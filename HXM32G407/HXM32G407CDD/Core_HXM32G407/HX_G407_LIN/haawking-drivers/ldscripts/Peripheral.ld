/*#############################################################################*/
/*                                                                             */
/* $Copyright:                                                                 */
/* Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd               */
/* http://www.haawking.com/ All rights reserved.                               */
/*                                                                             */
/* Redistribution and use in source and binary forms, with or without          */
/* modification, are permitted provided that the following conditions          */
/* are met:                                                                    */
/*                                                                             */
/*   Redistributions of source code must retain the above copyright            */
/*   notice, this list of conditions and the following disclaimer.             */
/*                                                                             */
/*   Redistributions in binary form must reproduce the above copyright         */
/*   notice, this list of conditions and the following disclaimer in the       */
/*   documentation and/or other materials provided with the                    */
/*   distribution.                                                             */
/*                                                                             */
/*   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of  */
/*   its contributors may be used to endorse or promote products derived       */
/*   from this software without specific prior written permission.             */
/*                                                                             */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS         */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT           */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR       */
/* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT            */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,       */
/* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY       */
/* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT         */
/* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE       */
/* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.        */
/*                                                                             */
/*#############################################################################*/
/*                                                                             */
/* Release for HXM32G407MCT7CDD, Peripheral Linker, 1.0.0                      */
/*                                                                             */
/* Release time: 2023-08-29 11:44:11.448580                                    */
/*                                                                             */
/*#############################################################################*/


/*----------------------------------------------------------------------*/
/* Memories                                                             */
/*----------------------------------------------------------------------*/
MEMORY
{
    AdcRegs_FILE                    (rwx)  : ORIGIN = 0x00001400, LENGTH = 156                /* Adc registers */                
    AdcResult_FILE                  (rwx)  : ORIGIN = 0x0000149c, LENGTH = 64                 /* AdcRe registers */              
    CpuTimer0Regs_FILE              (rwx)  : ORIGIN = 0x00001800, LENGTH = 16                 /* CpuTimer0 registers */          
    CpuTimer1Regs_FILE              (rwx)  : ORIGIN = 0x00001810, LENGTH = 16                 /* CpuTimer1 registers */          
    CpuTimer2Regs_FILE              (rwx)  : ORIGIN = 0x00001820, LENGTH = 16                 /* CpuTimer2 registers */          
    DevEmuRegs_FILE                 (rwx)  : ORIGIN = 0xDC80    , LENGTH = 12                 /* DevEmu registers */             
    DmaRegs_FILE                    (rwx)  : ORIGIN = 0x00001C00, LENGTH = 1024               /* Dma registers */                
    ECanaRegs_FILE                  (rwx)  : ORIGIN = 0x9000    , LENGTH = 104                /* ECana registers */              
    ECanaLAMRegs_FILE               (rwx)  : ORIGIN = 0x9080    , LENGTH = 128                /* ECanaLAM registers */           
    ECanaMOTSRegs_FILE              (rwx)  : ORIGIN = 0x9100    , LENGTH = 128                /* ECanaMOTS registers */          
    ECanaMOTORegs_FILE              (rwx)  : ORIGIN = 0x9180    , LENGTH = 128                /* ECanaMOTO registers */          
    ECanaMboxes_FILE                (rwx)  : ORIGIN = 0x9200    , LENGTH = 512                /* ECanaMb registers */            
    ECap1Regs_FILE                  (rwx)  : ORIGIN = 0xD000    , LENGTH = 48                 /* ECap1 registers */              
    EPwm1Regs_FILE                  (rwx)  : ORIGIN = 0xB000    , LENGTH = 0x100              /* EPwm1 registers */              
    EPwm2Regs_FILE                  (rwx)  : ORIGIN = 0xB100    , LENGTH = 0x100              /* EPwm2 registers */              
    EPwm3Regs_FILE                  (rwx)  : ORIGIN = 0xB200    , LENGTH = 0x100              /* EPwm3 registers */              
    EPwm4Regs_FILE                  (rwx)  : ORIGIN = 0xB300    , LENGTH = 0x100              /* EPwm4 registers */              
    EPwm5Regs_FILE                  (rwx)  : ORIGIN = 0xB400    , LENGTH = 0x100              /* EPwm5 registers */              
    EPwm6Regs_FILE                  (rwx)  : ORIGIN = 0xB500    , LENGTH = 0x100              /* EPwm6 registers */              
    EPwm7Regs_FILE                  (rwx)  : ORIGIN = 0xB600    , LENGTH = 0x100              /* EPwm7 registers */              
    EQep1Regs_FILE                  (rwx)  : ORIGIN = 0x0000D400, LENGTH = 100                /* EQep1 registers */              
    EQep2Regs_FILE                  (rwx)  : ORIGIN = 0x0000D480, LENGTH = 100                /* EQep2 registers */              
    GpioCtrlRegs_FILE               (rwx)  : ORIGIN = 0x0000D800, LENGTH = 56                 /* GpioCtrl registers */           
    GpioDataRegs_FILE               (rwx)  : ORIGIN = 0x0000D838, LENGTH = 48                 /* GpioData registers */           
    GpioIntRegs_FILE                (rwx)  : ORIGIN = 0x0000D868, LENGTH = 16                 /* GpioInt registers */            
    HRCap1Regs_FILE                 (rwx)  : ORIGIN = 0xCC00    , LENGTH = 0x70               /* HRCap1 registers */             
    HRCap2Regs_FILE                 (rwx)  : ORIGIN = 0xCC80    , LENGTH = 0x70               /* HRCap2 registers */             
    I2caRegs_FILE                   (rwx)  : ORIGIN = 0x0000E400, LENGTH = 256                /* I2ca registers */               
    LinaRegs_FILE                   (rwx)  : ORIGIN = 0xC400    , LENGTH = 0x400              /* Lina registers */               
    PieCtrlRegs_FILE                (rwx)  : ORIGIN = 0x00001900, LENGTH = 104                /* PieCtrl registers */            
    XIntruptRegs_FILE               (rwx)  : ORIGIN = 0x00001968, LENGTH = 124                /* XIntrupt registers */           
    PieEmuRegs_FILE                 (rwx)  : ORIGIN = 0x1BC0    , LENGTH = 4                  /* PieEmu registers */             
    PieVectTable_FILE               (rwx)  : ORIGIN = 0x00001A00, LENGTH = 448                /* PieVectT registers */           
    SciaRegs_FILE                   (rwx)  : ORIGIN = 0x0000E000, LENGTH = 248                /* Scia registers */               
    SpiaRegs_FILE                   (rwx)  : ORIGIN = 0x0000E800, LENGTH = 48                 /* Spia registers */               
    SpibRegs_FILE                   (rwx)  : ORIGIN = 0x0000E880, LENGTH = 48                 /* Spib registers */               
    SysCtrlRegs_FILE                (rwx)  : ORIGIN = 0x0000DC00, LENGTH = 256                /* SysCtrl registers */            
    SysPwrCtrlRegs_FILE             (rwx)  : ORIGIN = 0x0000DC60, LENGTH = 4                  /* SysPwrCtrl registers */         
    SysLdoOSCCtrlRegs_FILE          (rwx)  : ORIGIN = 0x0000DC98, LENGTH = 60                 /* SysLdoOSCCtrl registers */                      
    FlashRegs_FILE                  (rwx)  : ORIGIN = 0x7AF800  , LENGTH = 256                /* Flash registers */              
}

/*----------------------------------------------------------------------*/
/* Sections                                                             */
/*----------------------------------------------------------------------*/
SECTIONS
{
    .AdcRegs(NOLOAD)               : {*(.AdcRegs)}               > AdcRegs_FILE           
    .AdcResult(NOLOAD)             : {*(.AdcResult)}             > AdcResult_FILE         
    .CpuTimer0Regs(NOLOAD)         : {*(.CpuTimer0Regs)}         > CpuTimer0Regs_FILE     
    .CpuTimer1Regs(NOLOAD)         : {*(.CpuTimer1Regs)}         > CpuTimer1Regs_FILE     
    .CpuTimer2Regs(NOLOAD)         : {*(.CpuTimer2Regs)}         > CpuTimer2Regs_FILE     
    .DevEmuRegs(NOLOAD)            : {*(.DevEmuRegs)}            > DevEmuRegs_FILE        
    .DmaRegs(NOLOAD)               : {*(.DmaRegs)}               > DmaRegs_FILE              
    .ECanaRegs(NOLOAD)             : {*(.ECanaRegs)}             > ECanaRegs_FILE         
    .ECanaLAMRegs(NOLOAD)          : {*(.ECanaLAMRegs)}          > ECanaLAMRegs_FILE      
    .ECanaMOTSRegs(NOLOAD)         : {*(.ECanaMOTSRegs)}         > ECanaMOTSRegs_FILE     
    .ECanaMOTORegs(NOLOAD)         : {*(.ECanaMOTORegs)}         > ECanaMOTORegs_FILE     
    .ECanaMboxes(NOLOAD)           : {*(.ECanaMboxes)}           > ECanaMboxes_FILE       
    .ECap1Regs(NOLOAD)             : {*(.ECap1Regs)}             > ECap1Regs_FILE         
    .EPwm1Regs(NOLOAD)             : {*(.EPwm1Regs)}             > EPwm1Regs_FILE         
    .EPwm2Regs(NOLOAD)             : {*(.EPwm2Regs)}             > EPwm2Regs_FILE         
    .EPwm3Regs(NOLOAD)             : {*(.EPwm3Regs)}             > EPwm3Regs_FILE         
    .EPwm4Regs(NOLOAD)             : {*(.EPwm4Regs)}             > EPwm4Regs_FILE         
    .EPwm5Regs(NOLOAD)             : {*(.EPwm5Regs)}             > EPwm5Regs_FILE         
    .EPwm6Regs(NOLOAD)             : {*(.EPwm6Regs)}             > EPwm6Regs_FILE         
    .EPwm7Regs(NOLOAD)             : {*(.EPwm7Regs)}             > EPwm7Regs_FILE         
    .EQep1Regs(NOLOAD)             : {*(.EQep1Regs)}             > EQep1Regs_FILE         
    .EQep2Regs(NOLOAD)             : {*(.EQep2Regs)}             > EQep2Regs_FILE         
    .GpioCtrlRegs(NOLOAD)          : {*(.GpioCtrlRegs)}          > GpioCtrlRegs_FILE      
    .GpioDataRegs(NOLOAD)          : {*(.GpioDataRegs)}          > GpioDataRegs_FILE      
    .GpioIntRegs(NOLOAD)           : {*(.GpioIntRegs)}           > GpioIntRegs_FILE       
    .HRCap1Regs(NOLOAD)            : {*(.HRCap1Regs)}            > HRCap1Regs_FILE        
    .HRCap2Regs(NOLOAD)            : {*(.HRCap2Regs)}            > HRCap2Regs_FILE        
    .I2caRegs(NOLOAD)              : {*(.I2caRegs)}              > I2caRegs_FILE          
    .LinaRegs(NOLOAD)              : {*(.LinaRegs)}              > LinaRegs_FILE          
    .PieCtrlRegs(NOLOAD)           : {*(.PieCtrlRegs)}           > PieCtrlRegs_FILE       
    .XIntruptRegs(NOLOAD)          : {*(.XIntruptRegs)}          > XIntruptRegs_FILE      
    .PieEmuRegs(NOLOAD)            : {*(.PieEmuRegs)}            > PieEmuRegs_FILE        
    .PieVectTable(NOLOAD)          : {*(.PieVectTable)}          > PieVectTable_FILE      
    .SciaRegs(NOLOAD)              : {*(.SciaRegs)}              > SciaRegs_FILE          
    .SpiaRegs(NOLOAD)              : {*(.SpiaRegs)}              > SpiaRegs_FILE          
    .SpibRegs(NOLOAD)              : {*(.SpibRegs)}              > SpibRegs_FILE          
    .SysCtrlRegs(NOLOAD)           : {*(.SysCtrlRegs)}           > SysCtrlRegs_FILE       
    .SysPwrCtrlRegs(NOLOAD)        : {*(.SysPwrCtrlRegs)}        > SysPwrCtrlRegs_FILE    
    .SysLdoOSCCtrlRegs(NOLOAD)     : {*(.SysLdoOSCCtrlRegs)}     > SysLdoOSCCtrlRegs_FILE          
    .FlashRegs(NOLOAD)             : {*(.FlashRegs)}             > FlashRegs_FILE         
}
