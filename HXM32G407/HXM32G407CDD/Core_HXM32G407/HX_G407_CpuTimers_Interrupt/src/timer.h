/******************************************************************
 文 档 名：     timer.h
 D S P：       DSC28027
 使 用 库：
 作     用：
 说     明：      提供timer接口初始化配置
 ---------------------------- 使用说明 ----------------------------
 功能描述：


 版 本：V0.0.3
 时 间：2022年4月7日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
#ifndef TIMER_H_
#define TIMER_H_

#include "HXM32G407_config.h"

typedef struct timer0_Type
{
	union
	{
		Uint16 All;
		struct
		{
			Uint16 OnemsdFlag :1;
		} Status_Bits;
	} Mark_Para;

	Uint32 msCounter;
} timer0;

typedef struct timer1_Type
{
	union
	{
		Uint16 All;
		struct
		{
			Uint16 OnemsdFlag :1;
		} Status_Bits;
	} Mark_Para;

	Uint32 msCounter;
} timer1;

extern timer0 timer0Base;
extern timer1 timer1Base;

void Timer_init(void);


#endif /* TIMER_H_ */
