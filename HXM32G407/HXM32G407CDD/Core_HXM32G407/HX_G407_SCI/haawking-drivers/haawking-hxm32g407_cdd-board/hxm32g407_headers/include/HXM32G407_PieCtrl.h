//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-29 09:40:27.827457
//
//#############################################################################


#ifndef HXM32G407_PIECTRL_H
#define HXM32G407_PIECTRL_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// PIECTRL Individual Register Bit Definitions:

struct EMU_BITS {                             // bits description
    Uint32 EmuKey:16;                         // 15:0 EmuKey
    Uint32 EmuBMode:16;                       // 31:16 EmuBMode
};

union EMU_REG {
    Uint32  all;
};

struct  PIE_EMU_REGS {
    union   EMU_REG                          EMU;                         // 0x0  
};

struct PIECTRL_BITS {                         // bits description
    Uint32 ENPIE:1;                           // 0 Enable PIE block
    Uint32 PIEVECT:15;                        // 15:1 Fetched vector address
    Uint32 rsvd1:16;                          // 31:16 reserved
};

union PIECTRL_REG {
    Uint32  all;
    struct  PIECTRL_BITS  bit;
};

struct PIEACK_BITS {                          // bits description
    Uint32 ACK1:1;                            // 0 Acknowledge PIE interrupt group 1
    Uint32 ACK2:1;                            // 1 Acknowledge PIE interrupt group 2
    Uint32 ACK3:1;                            // 2 Acknowledge PIE interrupt group 3
    Uint32 ACK4:1;                            // 3 Acknowledge PIE interrupt group 4
    Uint32 ACK5:1;                            // 4 Acknowledge PIE interrupt group 5
    Uint32 ACK6:1;                            // 5 Acknowledge PIE interrupt group 6
    Uint32 ACK7:1;                            // 6 Acknowledge PIE interrupt group 7
    Uint32 ACK8:1;                            // 7 Acknowledge PIE interrupt group 8
    Uint32 ACK9:1;                            // 8 Acknowledge PIE interrupt group 9
    Uint32 ACK10:1;                           // 9 Acknowledge PIE interrupt group 10
    Uint32 ACK11:1;                           // 10 Acknowledge PIE interrupt group 11
    Uint32 ACK12:1;                           // 11 Acknowledge PIE interrupt group 12
    Uint32 rsvd1:20;                          // 31:12 Reserved
};

union PIEACK_REG {
    Uint32  all;
    struct  PIEACK_BITS  bit;
};

struct PIEIER1_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER1_REG {
    Uint32  all;
    struct  PIEIER1_BITS  bit;
};

struct PIEIFR1_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR1_REG {
    Uint32  all;
    struct  PIEIFR1_BITS  bit;
};

struct PIEIER2_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER2_REG {
    Uint32  all;
    struct  PIEIER2_BITS  bit;
};

struct PIEIFR2_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR2_REG {
    Uint32  all;
    struct  PIEIFR2_BITS  bit;
};

struct PIEIER3_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER3_REG {
    Uint32  all;
    struct  PIEIER3_BITS  bit;
};

struct PIEIFR3_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR3_REG {
    Uint32  all;
    struct  PIEIFR3_BITS  bit;
};

struct PIEIER4_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER4_REG {
    Uint32  all;
    struct  PIEIER4_BITS  bit;
};

struct PIEIFR4_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR4_REG {
    Uint32  all;
    struct  PIEIFR4_BITS  bit;
};

struct PIEIER5_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER5_REG {
    Uint32  all;
    struct  PIEIER5_BITS  bit;
};

struct PIEIFR5_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR5_REG {
    Uint32  all;
    struct  PIEIFR5_BITS  bit;
};

struct PIEIER6_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER6_REG {
    Uint32  all;
    struct  PIEIER6_BITS  bit;
};

struct PIEIFR6_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR6_REG {
    Uint32  all;
    struct  PIEIFR6_BITS  bit;
};

struct PIEIER7_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER7_REG {
    Uint32  all;
    struct  PIEIER7_BITS  bit;
};

struct PIEIFR7_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR7_REG {
    Uint32  all;
    struct  PIEIFR7_BITS  bit;
};

struct PIEIER8_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER8_REG {
    Uint32  all;
    struct  PIEIER8_BITS  bit;
};

struct PIEIFR8_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR8_REG {
    Uint32  all;
    struct  PIEIFR8_BITS  bit;
};

struct PIEIER9_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER9_REG {
    Uint32  all;
    struct  PIEIER9_BITS  bit;
};

struct PIEIFR9_BITS {                         // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR9_REG {
    Uint32  all;
    struct  PIEIFR9_BITS  bit;
};

struct PIEIER10_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER10_REG {
    Uint32  all;
    struct  PIEIER10_BITS  bit;
};

struct PIEIFR10_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR10_REG {
    Uint32  all;
    struct  PIEIFR10_BITS  bit;
};

struct PIEIER11_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER11_REG {
    Uint32  all;
    struct  PIEIER11_BITS  bit;
};

struct PIEIFR11_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR11_REG {
    Uint32  all;
    struct  PIEIFR11_BITS  bit;
};

struct PIEIER12_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIER12_REG {
    Uint32  all;
    struct  PIEIER12_BITS  bit;
};

struct PIEIFR12_BITS {                        // bits description
    Uint32 INTx1:1;                           // 0 INTx.1
    Uint32 INTx2:1;                           // 1 INTx.2
    Uint32 INTx3:1;                           // 2 INTx.3
    Uint32 INTx4:1;                           // 3 INTx.4
    Uint32 INTx5:1;                           // 4 INTx.5
    Uint32 INTx6:1;                           // 5 INTx.6
    Uint32 INTx7:1;                           // 6 INTx.7
    Uint32 INTx8:1;                           // 7 INTx.8
    Uint32 rsvd1:24;                          // 31:8 Reserved
};

union PIEIFR12_REG {
    Uint32  all;
    struct  PIEIFR12_BITS  bit;
};

struct  PIE_CTRL_REGS {
    union   PIECTRL_REG                      PIECTRL;                     // 0x0 PIE control register 00
    union   PIEACK_REG                       PIEACK;                      // 0x4 PIE acknowledge 04
    union   PIEIER1_REG                      PIEIER1;                     // 0x8 PIE INT1 IER register 08
    union   PIEIFR1_REG                      PIEIFR1;                     // 0xc PIE INT1 IFR register 0C
    union   PIEIER2_REG                      PIEIER2;                     // 0x10 PIE INT2 IER register 10
    union   PIEIFR2_REG                      PIEIFR2;                     // 0x14 PIE INT2 IFR register 14
    union   PIEIER3_REG                      PIEIER3;                     // 0x18 PIE INT3 IER register 18
    union   PIEIFR3_REG                      PIEIFR3;                     // 0x1c PIE INT3 IFR register 1C
    union   PIEIER4_REG                      PIEIER4;                     // 0x20 PIE INT4 IER register 20
    union   PIEIFR4_REG                      PIEIFR4;                     // 0x24 PIE INT4 IFR register 24
    union   PIEIER5_REG                      PIEIER5;                     // 0x28 PIE INT5 IER register 28
    union   PIEIFR5_REG                      PIEIFR5;                     // 0x2c PIE INT5 IFR register 2C
    union   PIEIER6_REG                      PIEIER6;                     // 0x30 PIE INT6 IER register 30
    union   PIEIFR6_REG                      PIEIFR6;                     // 0x34 PIE INT6 IFR register 34
    union   PIEIER7_REG                      PIEIER7;                     // 0x38 PIE INT7 IER register 38
    union   PIEIFR7_REG                      PIEIFR7;                     // 0x3c PIE INT7 IFR register 3C
    union   PIEIER8_REG                      PIEIER8;                     // 0x40 PIE INT8 IER register 40
    union   PIEIFR8_REG                      PIEIFR8;                     // 0x44 PIE INT8 IFR register 44
    union   PIEIER9_REG                      PIEIER9;                     // 0x48 PIE INT9 IER register 48
    union   PIEIFR9_REG                      PIEIFR9;                     // 0x4c PIE INT9 IFR register 4C
    union   PIEIER10_REG                     PIEIER10;                    // 0x50 PIE INT10 IER register 50
    union   PIEIFR10_REG                     PIEIFR10;                    // 0x54 PIE INT10 IFR register 54
    union   PIEIER11_REG                     PIEIER11;                    // 0x58 PIE INT11 IER register 58
    union   PIEIFR11_REG                     PIEIFR11;                    // 0x5c PIE INT11 IFR register 5C
    union   PIEIER12_REG                     PIEIER12;                    // 0x60 PIE INT12 IER register 60
    union   PIEIFR12_REG                     PIEIFR12;                    // 0x64 PIE INT12 IFR register 64
};

struct XINT1CR_BITS {                         // bits description
    Uint32 ENABLE:1;                          // 0 Enable XINCTR
    Uint32 rsvd1:1;                           // 1 Reserved
    Uint32 POLARITY:2;                        // 3:2 Polarity of XINCTR
    Uint32 rsvd2:28;                          // 31:4 Reserved
};

union XINT1CR_REG {
    Uint32  all;
    struct  XINT1CR_BITS  bit;
};

struct XINT2CR_BITS {                         // bits description
    Uint32 ENABLE:1;                          // 0 Enable XINCTR
    Uint32 rsvd1:1;                           // 1 Reserved
    Uint32 POLARITY:2;                        // 3:2 Polarity of XINCTR
    Uint32 rsvd2:28;                          // 31:4 Reserved
};

union XINT2CR_REG {
    Uint32  all;
    struct  XINT2CR_BITS  bit;
};

struct XINT3CR_BITS {                         // bits description
    Uint32 ENABLE:1;                          // 0 Enable XINCTR
    Uint32 rsvd1:1;                           // 1 Reserved
    Uint32 POLARITY:2;                        // 3:2 Polarity of XINCTR
    Uint32 rsvd2:28;                          // 31:4 Reserved
};

union XINT3CR_REG {
    Uint32  all;
    struct  XINT3CR_BITS  bit;
};

struct  XINTRUPT_REGS {
    union   XINT1CR_REG                      XINT1CR;                     // 0x0 XINT1 configuration register
    union   XINT2CR_REG                      XINT2CR;                     // 0x4 XINT2 configuration register
    union   XINT3CR_REG                      XINT3CR;                     // 0x8 XINT3 configuration register
    Uint32                                   XINT1CTR;                    // 0xc XINT1 counter register
    Uint32                                   XINT2CTR;                    // 0x10 XINT2 counter register
    Uint32                                   XINT3CTR;                    // 0x14 XINT3 counter register
};

#define PIEACK_GROUP1   0x0001
#define PIEACK_GROUP2   0x0002
#define PIEACK_GROUP3   0x0004
#define PIEACK_GROUP4   0x0008
#define PIEACK_GROUP5   0x0010
#define PIEACK_GROUP6   0x0020
#define PIEACK_GROUP7   0x0040
#define PIEACK_GROUP8   0x0080
#define PIEACK_GROUP9   0x0100
#define PIEACK_GROUP10  0x0200
#define PIEACK_GROUP11  0x0400
#define PIEACK_GROUP12  0x0800

#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
