/******************************************************************************************
 文 档 名：      HX_HXM32G407_CAN_Interrupt
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：

 连接方式：1.VCC-3.3V 共地 GPIO30-RX GPIO31-TX 连接到USB-CAN模块上
 	 	 	 	  2. USB-CAN模块连接湖人板CANH CANL，J3（ 2-1引脚）相连
 现象：      通过USB_CAN通信软件，可实时显示发送与接收数据。1000Kbps ,ID 01

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ****************************************************************************************/

 
#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "can.h"

int main(void)
{
    InitSysCtrl();  //使用外部时钟12M
	CAN_Init();
	InitECanaGpio();
    EALLOW;
    ECanaRegs.CANMIM.bit.MIM0=1;//打开接收掩码，接收到数据触发中断
    ECanaRegs.CANMIL.bit.MIL0 = 0;   // 选择EcanA中断0
    ECanaRegs.CANGIM.bit.I0EN = 1;  // 使能中断0
    PieVectTable.ECAN0INTA = &eCanRxIsr;  // CANA 0接收中断入口
    EDIS;
    PieCtrlRegs.PIEIER9.bit.INTx5 = 1;                      // 使能ECAN1中断
    IER |= M_INT9; 											// Enable CPU INT9
    EINT;
    while(1){
   //    	CAN_Tx();//发送数据
    }
	return 0;
}



// ----------------------------------------------------------------------------
