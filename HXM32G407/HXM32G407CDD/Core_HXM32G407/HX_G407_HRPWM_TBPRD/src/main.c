/******************************************************************
 文 档 名：     HX_HXM32G407_HRPWM_TBPRD
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 作     用：      HRPWM与常规PWM对比测试
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：  功能一：HRPWM与常规PWM的周期精度对比测试
 功能二：HRPWM的SFO微边沿定位功能测试

 连接方式：  功能一：EPWM1A(GPIO0)接示波器1通道,EPWM2A(GPIO2)接示波器2通道,
 另一端接地,观察双通道波形,EPWM1A频率在2.353-2.4M间变化,而EPWM2A频率仅为2.4M,
 可见HRPWM相比PWM工作点较多,故精度较高
 功能二：EPWM1A(GPIO0)接示波器1通道,EPWM3A(GPIO4)接示波器2通道,
 另一端接地,观察双通道波形
 
 现象：         EPWM1频率在2.353-2.4M间变化,而EPWM2频率仅为2.4M

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "hrpwm.h"

int main(void)
{
	/*系统时钟初始化*/
	InitSysCtrl();

	/*EPwm的Gpio引脚配置*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();

	InitPieCtrl();

	/*关中断,清中断*/
	IER = 0x0000;
	IFR = 0x0000;

	/*中断向量表初始化*/
	InitPieVectTable();

	/*中断向量表配置:指向所使用的中断服务函数地址*/
	EALLOW;
	PieVectTable.EPWM1_INT = &EPWM1_ISR;
	PieVectTable.EPWM2_INT = &EPWM2_ISR;
	PieVectTable.EPWM3_INT = &EPWM3_ISR;
	EDIS;

	/*EPWM的时基同步使能屏蔽，可进行EPWM的初始化配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*EPWM的初始化配置，PWM频率200KHz*/
	HRPWM1_config(50);
	HRPWM2_config(50);
	HRPWM3_config(50);

	/*EPWM的时基同步使能，可EPWM的初始化配置有效*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	/*对应的CPU IER中断使能*/
	IER |= M_INT3;

	/*对应的PIE IER中断使能*/
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

	/*CPU INT全局中断使能*/
	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
