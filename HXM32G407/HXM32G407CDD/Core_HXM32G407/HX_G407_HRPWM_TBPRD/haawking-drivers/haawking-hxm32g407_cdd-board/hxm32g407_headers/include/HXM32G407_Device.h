//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.0
//
// Release time: 2023-08-28 11:20:15.365295
//
//#############################################################################


#ifndef HXM32G407_DEVICE_H
#define HXM32G407_DEVICE_H

#define	_CPU_M0_BASE_ADDR	((uint32_t)0x00000000)
#define	_CPU_M1_BASE_ADDR	((uint32_t)0x00000800)
#define	_ADC_BASE_ADDR	    ((uint32_t)0x00001400) 
#define	_TIMER_PIE_BASE_ADDR	((uint32_t)0x00001800)
#define	_DMA_BASE_ADDR		((uint32_t)0x00001C00)
#define	_DEBUG_BASE_ADDR	((uint32_t)0x0000A000)
#define	_APB_0_BASE_ADDR		((uint32_t)0x0000A000)
#define	_APB_1_BASE_ADDR		((uint32_t)0x0000E000)
#define	_L0_BASE_ADDR		((uint32_t)0x00010000)
#define	_L1_BASE_ADDR		((uint32_t)0x00014000)
#define	_OTP_BASE_ADDR		((uint32_t)0x007A0000)

#define	_BOOT_ROM_BASE_ADDR	((uint32_t)0x007F8000)
#define	_CMP_BASE_ADDR		((uint32_t)0x00C000)

#define	_L0_DUAL_MAPPED_BASE_ADDR	((uint32_t)0x007F0000)

/***********************************************************************
APB-0  PERIPHERALS
*************************************************************************/
#define _CAN_BASE_ADDR 		((uint32_t) 0x9000)
#define _LIN_BASE_ADDR		((uint32_t) 0xC400)

#define _HRCAP_BASE_ADDR	((uint32_t)0x0000CC00)
#define	_ECAP_BASE_ADDR		((uint32_t)0x0000D000)
#define	_EQEP_BASE_ADDR		((uint32_t)0x0000D400)
#define	_GPIO_BASE_ADDR		((uint32_t)0x0000D800)
#define	_SYS_CTRL_BASE_ADDR	((uint32_t) 0xDC00)



/***********************************************************************
APB-1  PERIPHERALS
*************************************************************************/
#define	_SCI_BASE_ADDR		((uint32_t)0x0000E000)
#define	_I2C_BASE_ADDR		((uint32_t)0x0000E400)
#define	_SPI_BASE_ADDR		((uint32_t)0x0000E800)


/**************************************************************************
TIMER AND PIE PERIPHERALS
**************************************************************************/

#define	_TIMER_0_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0)  
#define	_TIMER_1_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x10)
#define	_TIMER_2_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x20)


/*************************************************************************
	PIE PERIPHERALS
*************************************************************************/
#define	_PIE_BASE_ADDR	(_TIMER_PIE_BASE_ADDR + 0x100)
#define _VECTOR_PIE_BASE_ADDR  (_TIMER_PIE_BASE_ADDR + 0x200)
#define _PIE_EMU_REG_ADDR  ((uint32_t)0x1BC0)



#define _EPWM_BASE_ADDR	  ((uint32_t)0xB000)

/*************************************************************************
	EPWM PERIPHERALS
*************************************************************************/

#define	_EPWM_PER1_BASE_ADDR  _EPWM_BASE_ADDR
#define	_EPWM_PER2_BASE_ADDR  (_EPWM_PER1_BASE_ADDR + 0x100)
#define	_EPWM_PER3_BASE_ADDR  (_EPWM_PER2_BASE_ADDR + 0x100)
#define	_EPWM_PER4_BASE_ADDR  (_EPWM_PER3_BASE_ADDR + 0x100)

#define	_EPWM_PER5_BASE_ADDR  (_EPWM_PER4_BASE_ADDR + 0x100)
#define	_EPWM_PER6_BASE_ADDR  (_EPWM_PER5_BASE_ADDR + 0x100)
#define	_EPWM_PER7_BASE_ADDR  (_EPWM_PER6_BASE_ADDR + 0x100)




/*************************************************************************
	CMP PERIPHERALS
*************************************************************************/

#define	_CMP_PER1_ADDR	_CMP_BASE_ADDR
#define	_CMP_PER2_ADDR	(_CMP_PER1_ADDR + 0x80)
#define	_CMP_PER3_ADDR	(_CMP_PER2_ADDR + 0x100)


//0x73FFF0 -0x73FFFF
#define _CSM_PASSWORD_BASE_ADDR	((uint32_t)0x73FFF0)

#define _DEV_EMU_BASE_ADDR	(_SYS_CTRL_BASE_ADDR  +  0x80)

#define _FLASH_PER_REG_ADDR		((uint32_t)0x7AF800)



/*************************************************************************
	GPIO PERIPHERALS
*************************************************************************/

#define _GPIO_DATA_ADDR		(_GPIO_BASE_ADDR + 0x38)

#define _GPIO_INT_ADDR		(_GPIO_BASE_ADDR + 0x68)



/***************************************************************
	SPI PERIHERALS
*****************************************************************/
#define _SPIA_BASE_ADDR		_SPI_BASE_ADDR
#define _SPIB_BASE_ADDR		(_SPI_BASE_ADDR + 0x80)


#include <stdio.h>

#include "hx_rv32_dev.h"

#include "hx_rv32_type.h"


#define CODE_SECTION(v) __attribute__((section(v)))



//---------------------------------------------------------------------------
// Common CPU Definitions:
//

extern  volatile unsigned int IFR;
extern  volatile unsigned int IER;



extern Uint32 ier_set(Uint32 group);
extern Uint32 ier_unset(Uint32 group);

extern Uint32 ifr_set(Uint32 group);
extern Uint32 ifr_unset(Uint32 group);

extern uint32_t read_reg32(uintptr_t addr);
extern uint32_t read_reg16(uintptr_t addr);
extern uint8_t read_reg8(uintptr_t addr);

extern void write_reg32(uintptr_t addr,uint32_t val);
extern void write_reg16(uintptr_t addr,uint16_t val);
extern void write_reg8(uintptr_t addr,uint8_t val);


/*************************************************************
you can use it  like this

IER_ENABLE(M_INT1|M_INT3);
IER_DISABLE(M_INT1|M_INT3);

***********************************************************************/

#define IER_ENABLE(v)   ier_set(v)
#define IER_DISABLE(v)  ier_unset(v)

#define IFR_ENABLE(v)   ifr_set(v)
#define IFR_DISABLE(v)  ifr_unset(v)




#define EALLOW  asm("csrsi 0x7C1, 0x01")  // eallow  register id is  0x7C1 ,enable write spieacl register
#define	EDIS  	asm("csrci 0x7C1, 0x01")  // disable

#define  ESTOP0 asm(" ebreak"); //send debug call


#define M_INT1  0x0001
#define M_INT2  0x0002
#define M_INT3  0x0004
#define M_INT4  0x0008
#define M_INT5  0x0010
#define M_INT6  0x0020
#define M_INT7  0x0040
#define M_INT8  0x0080
#define M_INT9  0x0100
#define M_INT10 0x0200
#define M_INT11 0x0400
#define M_INT12 0x0800
#define M_INT13 0x1000
#define M_INT14 0x2000

#define BIT0    0x0001
#define BIT1    0x0002
#define BIT2    0x0004
#define BIT3    0x0008
#define BIT4    0x0010
#define BIT5    0x0020
#define BIT6    0x0040
#define BIT7    0x0080
#define BIT8    0x0100
#define BIT9    0x0200
#define BIT10   0x0400
#define BIT11   0x0800
#define BIT12   0x1000
#define BIT13   0x2000
#define BIT14   0x4000
#define BIT15   0x8000


//---------------------------------------------------------------------------
// Include All Peripheral Header Files:
//
#include "HXM32G407_Adc.h"
#include "HXM32G407_CpuTimers.h"
#include "HXM32G407_DevEmu.h"
#include "HXM32G407_Dma.h"
#include "HXM32G407_ECan.h"
#include "HXM32G407_ECap.h"
#include "HXM32G407_EPwm.h"
#include "HXM32G407_Eqep.h"
#include "HXM32G407_Gpio.h"
#include "HXM32G407_HRCap.h"
#include "HXM32G407_I2c.h"
#include "HXM32G407_Lin.h"
#include "HXM32G407_PieCtrl.h"
#include "HXM32G407_PieVect.h"
#include "HXM32G407_Sci.h"
#include "HXM32G407_Spi.h"
#include "HXM32G407_SysCtrl.h"
#include "HXM32G407_globalvariabledefs.h"

#define HXM32G407_EPWM1 1
#define HXM32G407_EPWM2 1
#define HXM32G407_EPWM3 1
#define HXM32G407_EPWM4 1
#define HXM32G407_EPWM5 1
#define HXM32G407_EPWM6 1
#define HXM32G407_EPWM7 1
#define HXM32G407_ECAP1 1
#define HXM32G407_EQEP1 1
#define HXM32G407_EQEP2 1
#define HXM32G407_ECANA 1
#define HXM32G407_HRCAP1 1
#define HXM32G407_HRCAP2 1
#define HXM32G407_SPIA  1
#define HXM32G407_SPIB  1
#define HXM32G407_SCIA  1
#define HXM32G407_I2CA  1
#define HXM32G407_LINA  1

// Timer definitions based on System Clock
// 120 MHz devices 
#define      mSec0_5          60000           // 0.5 mS
#define      mSec0_75         90000           // 0.75 mS
#define      mSec1            120000          // 1.0 mS
#define      mSec2            240000          // 2.0 mS
#define      mSec5            600000          // 5.0 mS
#define      mSec7_5          900000          // 7.5 mS
#define      mSec10           1200000         // 10 mS
#define      mSec20           2400000         // 20 mS
#define      mSec50           6000000         // 50 mS
#define      mSec75           9000000         // 75 mS
#define      mSec100          12000000        // 100 mS
#define      mSec200          24000000        // 200 mS
#define      mSec500          60000000        // 500 mS
#define      mSec750          90000000        // 750 mS
#define      mSec1000         120000000       // 1000 mS
#define      mSec2000         240000000       // 2000 mS
#define      mSec5000         600000000       // 5000 mS



extern volatile Uint32   IER;
extern volatile Uint32   IFR;


#endif

//===========================================================================
// End of file.
//===========================================================================
