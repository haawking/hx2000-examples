//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.141492
//
//#############################################################################


#ifndef HXM32G407_DEVEMU_H
#define HXM32G407_DEVEMU_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// DEVEMU Individual Register Bit Definitions:

struct DEVICECNF_BITS {                       // bits description
    Uint32 rsvd1:5;                           // 4:0 reserved
    Uint32 XRSn:1;                            // 5 XRSn Signal Status
    Uint32 rsvd2:21;                          // 26:6 reserved
    Uint32 TRSTn:1;                           // 27 Status of TRSTn signal
    Uint32 rsvd3:4;                           // 31:28 reserved
};

union DEVICECNF_REG {
    Uint32  all;
    struct  DEVICECNF_BITS  bit;
};

struct CLASSID_BITS {                         // bits description
    Uint32 CLASSNO:8;                         // 7:0 Class Number
    Uint32 PARTTYPE:8;                        // 15:8 Part Type
    Uint32 resvd1:16;                         // 31:16 reserved
};

union CLASSID_REG {
    Uint32  all;
    struct  CLASSID_BITS  bit;
};

struct  DEV_EMU_REGS {
    union   DEVICECNF_REG                    DEVICECNF;                   // 0x0 Device Configuration
    union   CLASSID_REG                      CLASSID;                     // 0x4 Class ID
    Uint32                                   REVID;                       // 0x8 Device ID
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
