//###########################################################################
//
// FILE:   HXM32G407_Project.h
//
// TITLE:  HXM32G407 Project Headerfile and Examples Include File
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef HXM32G407_PROJECT_H
#define HXM32G407_PROJECT_H

#include "HXM32G407_Device.h"     //  Headerfile Include File
#include "HXM32G407_Examples.h"   //  Examples Include File

#endif  
