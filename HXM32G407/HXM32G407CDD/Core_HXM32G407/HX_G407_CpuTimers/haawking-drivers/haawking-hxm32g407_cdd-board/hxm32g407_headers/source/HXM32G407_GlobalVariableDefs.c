

//---------------------------------------------------------------------------
// Included Files
//

#include "HXM32G407_Device.h" 

//---------------------------------------------------------------------------
// Define Global Peripheral Variables:
//


//
// Structure
//
volatile struct ADC_REGS CODE_SECTION(".AdcRegs") AdcRegs;
volatile struct ADC_RESULT_REGS CODE_SECTION(".AdcResult") AdcResult;
volatile struct CPUTIMER_REGS CODE_SECTION(".CpuTimer0Regs") CpuTimer0Regs;
volatile struct CPUTIMER_REGS CODE_SECTION(".CpuTimer1Regs") CpuTimer1Regs;
volatile struct CPUTIMER_REGS CODE_SECTION(".CpuTimer2Regs") CpuTimer2Regs;
volatile struct DEV_EMU_REGS CODE_SECTION(".DevEmuRegs") DevEmuRegs;
volatile struct DMA_REGS CODE_SECTION(".DmaRegs") DmaRegs;
volatile struct ECAN_REGS CODE_SECTION(".ECanaRegs") ECanaRegs;
volatile struct LAM_REGS CODE_SECTION(".ECanaLAMRegs") ECanaLAMRegs;
volatile struct MOTS_REGS CODE_SECTION(".ECanaMOTSRegs") ECanaMOTSRegs;
volatile struct MOTO_REGS CODE_SECTION(".ECanaMOTORegs") ECanaMOTORegs;
volatile struct ECAN_MBOXES CODE_SECTION(".ECanaMboxes") ECanaMboxes;
volatile struct ECAP_REGS CODE_SECTION(".ECap1Regs") ECap1Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm1Regs") EPwm1Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm2Regs") EPwm2Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm3Regs") EPwm3Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm4Regs") EPwm4Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm5Regs") EPwm5Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm6Regs") EPwm6Regs;
volatile struct EPWM_REGS CODE_SECTION(".EPwm7Regs") EPwm7Regs;
volatile struct EQEP_REGS CODE_SECTION(".EQep1Regs") EQep1Regs;
volatile struct EQEP_REGS CODE_SECTION(".EQep2Regs") EQep2Regs;
volatile struct GPIO_CTRL_REGS CODE_SECTION(".GpioCtrlRegs") GpioCtrlRegs;
volatile struct GPIO_DATA_REGS CODE_SECTION(".GpioDataRegs") GpioDataRegs;
volatile struct GPIO_INT_REGS CODE_SECTION(".GpioIntRegs") GpioIntRegs;
volatile struct HRCAP_REGS CODE_SECTION(".HRCap1Regs") HRCap1Regs;
volatile struct HRCAP_REGS CODE_SECTION(".HRCap2Regs") HRCap2Regs;
volatile struct I2C_REGS CODE_SECTION(".I2caRegs") I2caRegs;
volatile struct LIN_REGS CODE_SECTION(".LinaRegs") LinaRegs;
volatile struct PIE_CTRL_REGS CODE_SECTION(".PieCtrlRegs") PieCtrlRegs;
volatile struct XINTRUPT_REGS CODE_SECTION(".XIntruptRegs") XIntruptRegs;
volatile struct PIE_EMU_REGS CODE_SECTION(".PieEmuRegs") PieEmuRegs;
volatile struct PIE_VECT_TABLE CODE_SECTION(".PieVectTable") PieVectTable;
volatile struct SCI_REGS CODE_SECTION(".SciaRegs") SciaRegs;
volatile struct SPI_REGS CODE_SECTION(".SpiaRegs") SpiaRegs;
volatile struct SPI_REGS CODE_SECTION(".SpibRegs") SpibRegs;
volatile struct SYS_CTRL_REGS CODE_SECTION(".SysCtrlRegs") SysCtrlRegs;
volatile struct SYS_PWR_CTRL_REGS CODE_SECTION(".SysPwrCtrlRegs") SysPwrCtrlRegs;
volatile struct SYS_LDO_OSC_CTRL_REGS CODE_SECTION(".SysLdoOSCCtrlRegs") SysLdoOSCCtrlRegs;
volatile struct FLASH_REGS CODE_SECTION(".FlashRegs") FlashRegs;
volatile Uint32 CODE_SECTION(".ier_register") IER;
volatile Uint32 CODE_SECTION(".ifr_register") IFR;



//
// Pointer
//
volatile struct ADC_REGS *const CODE_SECTION(".text") P_AdcRegs = (volatile struct ADC_REGS *)0x00001400;
volatile struct ADC_RESULT_REGS *const CODE_SECTION(".text") P_AdcResult = (volatile struct ADC_RESULT_REGS *)0x0000149c;
volatile struct CPUTIMER_REGS *const CODE_SECTION(".text") P_CpuTimer0Regs = (volatile struct CPUTIMER_REGS *)0x00001800;
volatile struct CPUTIMER_REGS *const CODE_SECTION(".text") P_CpuTimer1Regs = (volatile struct CPUTIMER_REGS *)0x00001810;
volatile struct CPUTIMER_REGS *const CODE_SECTION(".text") P_CpuTimer2Regs = (volatile struct CPUTIMER_REGS *)0x00001820;
volatile struct DEV_EMU_REGS *const CODE_SECTION(".text") P_DevEmuRegs = (volatile struct DEV_EMU_REGS *)0xDC80;
volatile struct DMA_REGS *const CODE_SECTION(".text") P_DmaRegs = (volatile struct DMA_REGS *)0x00001C00;
volatile struct ECAN_REGS *const CODE_SECTION(".text") P_ECanaRegs = (volatile struct ECAN_REGS *)0x9000;
volatile struct LAM_REGS *const CODE_SECTION(".text") P_ECanaLAMRegs = (volatile struct LAM_REGS *)0x9080;
volatile struct MOTS_REGS *const CODE_SECTION(".text") P_ECanaMOTSRegs = (volatile struct MOTS_REGS *)0x9100;
volatile struct MOTO_REGS *const CODE_SECTION(".text") P_ECanaMOTORegs = (volatile struct MOTO_REGS *)0x9180;
volatile struct ECAN_MBOXES *const CODE_SECTION(".text") P_ECanaMboxes = (volatile struct ECAN_MBOXES *)0x9200;
volatile struct ECAP_REGS *const CODE_SECTION(".text") P_ECap1Regs = (volatile struct ECAP_REGS *)0xD000;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm1Regs = (volatile struct EPWM_REGS *)0xB000;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm2Regs = (volatile struct EPWM_REGS *)0xB100;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm3Regs = (volatile struct EPWM_REGS *)0xB200;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm4Regs = (volatile struct EPWM_REGS *)0xB300;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm5Regs = (volatile struct EPWM_REGS *)0xB400;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm6Regs = (volatile struct EPWM_REGS *)0xB500;
volatile struct EPWM_REGS *const CODE_SECTION(".text") P_EPwm7Regs = (volatile struct EPWM_REGS *)0xB600;
volatile struct EQEP_REGS *const CODE_SECTION(".text") P_EQep1Regs = (volatile struct EQEP_REGS *)0x0000D400;
volatile struct EQEP_REGS *const CODE_SECTION(".text") P_EQep2Regs = (volatile struct EQEP_REGS *)0x0000D480;
volatile struct GPIO_CTRL_REGS *const CODE_SECTION(".text") P_GpioCtrlRegs = (volatile struct GPIO_CTRL_REGS *)0x0000D800;
volatile struct GPIO_DATA_REGS *const CODE_SECTION(".text") P_GpioDataRegs = (volatile struct GPIO_DATA_REGS *)0x0000D838;
volatile struct GPIO_INT_REGS *const CODE_SECTION(".text") P_GpioIntRegs = (volatile struct GPIO_INT_REGS *)0x0000D868;
volatile struct HRCAP_REGS *const CODE_SECTION(".text") P_HRCap1Regs = (volatile struct HRCAP_REGS *)0xCC00;
volatile struct HRCAP_REGS *const CODE_SECTION(".text") P_HRCap2Regs = (volatile struct HRCAP_REGS *)0xCC80;
volatile struct I2C_REGS *const CODE_SECTION(".text") P_I2caRegs = (volatile struct I2C_REGS *)0x0000E400;
volatile struct LIN_REGS *const CODE_SECTION(".text") P_LinaRegs = (volatile struct LIN_REGS *)0xC400;
volatile struct PIE_CTRL_REGS *const CODE_SECTION(".text") P_PieCtrlRegs = (volatile struct PIE_CTRL_REGS *)0x00001900;
volatile struct XINTRUPT_REGS *const CODE_SECTION(".text") P_XIntruptRegs = (volatile struct XINTRUPT_REGS *)0x00001968;
volatile struct PIE_EMU_REGS *const CODE_SECTION(".text") P_PieEmuRegs = (volatile struct PIE_EMU_REGS *)0x1BC0;
volatile struct PIE_VECT_TABLE *const CODE_SECTION(".text") P_PieVectTable = (volatile struct PIE_VECT_TABLE *)0x00001A00;
volatile struct SCI_REGS *const CODE_SECTION(".text") P_SciaRegs = (volatile struct SCI_REGS *)0x0000E000;
volatile struct SPI_REGS *const CODE_SECTION(".text") P_SpiaRegs = (volatile struct SPI_REGS *)0x0000E800;
volatile struct SPI_REGS *const CODE_SECTION(".text") P_SpibRegs = (volatile struct SPI_REGS *)0x0000E880;
volatile struct SYS_CTRL_REGS *const CODE_SECTION(".text") P_SysCtrlRegs = (volatile struct SYS_CTRL_REGS *)0x0000DC00;
volatile struct SYS_PWR_CTRL_REGS *const CODE_SECTION(".text") P_SysPwrCtrlRegs = (volatile struct SYS_PWR_CTRL_REGS *)0x0000DC60;
volatile struct SYS_LDO_OSC_CTRL_REGS *const CODE_SECTION(".text") P_SysLdoOSCCtrlRegs = (volatile struct SYS_LDO_OSC_CTRL_REGS *)0x0000DC98;
volatile struct CSM_PWL *const CODE_SECTION(".text") P_CsmPwl = (volatile struct CSM_PWL *)0x73FFF0;
volatile struct FLASH_REGS *const CODE_SECTION(".text") P_FlashRegs = (volatile struct FLASH_REGS *)0x7AF800;

//===========================================================================
// End of file.
//===========================================================================
