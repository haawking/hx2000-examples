#ifndef TIMER_H_
#define TIMER_H_

#include "HXM32G407_Device.h"
#include "HXM32G407_Examples.h"

extern Uint32 msCounter;

void  INTERRUPT timer0_ISR(void);

void Timer0_init(void);




#endif /* TIMER_H_ */
