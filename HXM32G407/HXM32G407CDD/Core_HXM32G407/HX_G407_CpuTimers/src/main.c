/******************************************************************
 文 档 名：     HX_HXM32G407_Timer_interrupt
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz

 连接方式：

 现象：       当定时器1的计数器减到0,进入中断LED1闪烁

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "timer.h"
void InitLED(void);

int main(void)
{
	InitSysCtrl();

	DINT;

	InitPieCtrl(); /*初始化PIE控制寄存器为默认状态*/

	IER = 0x0000; /*不使能CPU中断*/
	IFR = 0x0000; /*清除所有的CPU中断标志*/

	InitLED();

	InitPieVectTable();

	InitCpuTimers();

	Timer0_init();

	ConfigCpuTimer(&CpuTimer1, 120, 500000);/*定时器1初始化，第二个参数为CPU SYSCLK =120M， 第三个参数为设定的周期，us单位*/
	CpuTimer1Regs.TCR.bit.TSS = 0;/* To start or restart the CPU-timer, set TSS to 0*/

	ConfigCpuTimer(&CpuTimer2, 120, 100000);/*定时器1初始化，第二个参数为CPU SYSCLK = 120M， 第三个参数为设定的周期，us单位*/
	CpuTimer2Regs.TCR.bit.TSS = 0;/* To start or restart the CPU-timer, set TSS to 0*/

	EINT;

	while (1)
	{
		if (msCounter > 100)
		{
			msCounter = 0;
			GpioDataRegs.GPBTOGGLE.bit.GPIO44 = 1;/*GPIO44电平翻转，LED1交互闪烁*/

			GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;/*GPIO43电平翻转，D402 LED交互闪烁*/
		}
		if (CpuTimer1Regs.TCR.bit.TIF == 1)/*定时器1查询周期*/
		{
			CpuTimer1Regs.TCR.bit.TIF = 1;/* clear flag*/
		}

		if (CpuTimer2Regs.TCR.bit.TIF == 1)/*定时器2查询周期*/
		{
			CpuTimer2Regs.TCR.bit.TIF = 1;/* clear flag*/
		}
	}

	while (1)
	{
	}

	return 0;
}

/******************************************************************
 函数名：void  InitLED(void)
 参	数：无
 返回值：无
 作	用：配置GPIO44为输出模式
 ******************************************************************/

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0; /*普通IO，对应LED1灯*/
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1; /*输出*/

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; /*普通IO，对应D402灯*/
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1; /*输出*/
	EDIS;

}

// ----------------------------------------------------------------------------
