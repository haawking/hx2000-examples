
#ifndef EPWM_H_
#define EPWM_H_

#include "HXM32G407_config.h"
#include "HXM32G407_EPwm_defines.h"

void InitEPwm1Example(void);
void InitEPwm2Example(void);
void InitEPwm3Example(void);

void INTERRUPT epmw1_isr(void);

void epwm1_compare(void);

void thei(void);



#endif /* EPWM_H_ */
