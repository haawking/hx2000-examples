//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-29 09:40:27.477902
//
//#############################################################################



//---------------------------------------------------------------------------
// Extern Global Peripheral Variables:
//


//
// Structure
//
extern volatile struct ADC_REGS AdcRegs;
extern volatile struct ADC_RESULT_REGS AdcResult;
extern volatile struct CPUTIMER_REGS CpuTimer0Regs;
extern volatile struct CPUTIMER_REGS CpuTimer1Regs;
extern volatile struct CPUTIMER_REGS CpuTimer2Regs;
extern volatile struct DEV_EMU_REGS DevEmuRegs;
extern volatile struct DMA_REGS DmaRegs;
extern volatile struct ECAN_REGS ECanaRegs;
extern volatile struct LAM_REGS ECanaLAMRegs;
extern volatile struct MOTS_REGS ECanaMOTSRegs;
extern volatile struct MOTO_REGS ECanaMOTORegs;
extern volatile struct ECAN_MBOXES ECanaMboxes;
extern volatile struct ECAP_REGS ECap1Regs;
extern volatile struct EPWM_REGS EPwm1Regs;
extern volatile struct EPWM_REGS EPwm2Regs;
extern volatile struct EPWM_REGS EPwm3Regs;
extern volatile struct EPWM_REGS EPwm4Regs;
extern volatile struct EPWM_REGS EPwm5Regs;
extern volatile struct EPWM_REGS EPwm6Regs;
extern volatile struct EPWM_REGS EPwm7Regs;
extern volatile struct EQEP_REGS EQep1Regs;
extern volatile struct EQEP_REGS EQep2Regs;
extern volatile struct GPIO_CTRL_REGS GpioCtrlRegs;
extern volatile struct GPIO_DATA_REGS GpioDataRegs;
extern volatile struct GPIO_INT_REGS GpioIntRegs;
extern volatile struct HRCAP_REGS HRCap1Regs;
extern volatile struct HRCAP_REGS HRCap2Regs;
extern volatile struct I2C_REGS I2caRegs;
extern volatile struct LIN_REGS LinaRegs;
extern volatile struct PIE_CTRL_REGS PieCtrlRegs;
extern volatile struct XINTRUPT_REGS XIntruptRegs;
extern volatile struct PIE_EMU_REGS PieEmuRegs;
extern volatile struct PIE_VECT_TABLE PieVectTable;
extern volatile struct SCI_REGS SciaRegs;
extern volatile struct SPI_REGS SpiaRegs;
extern volatile struct SPI_REGS SpibRegs;
extern volatile struct SYS_CTRL_REGS SysCtrlRegs;
extern volatile struct SYS_PWR_CTRL_REGS SysPwrCtrlRegs;
extern volatile struct SYS_LDO_OSC_CTRL_REGS SysLdoOSCCtrlRegs;
extern volatile struct CSM_PWL CsmPwl;
extern volatile struct FLASH_REGS FlashRegs;
extern volatile Uint32 IER;
extern volatile Uint32 IFR;



//
// Pointer
//
extern volatile struct ADC_REGS *const P_AdcRegs;
extern volatile struct ADC_RESULT_REGS *const P_AdcResult;
extern volatile struct CPUTIMER_REGS *const P_CpuTimer0Regs;
extern volatile struct CPUTIMER_REGS *const P_CpuTimer1Regs;
extern volatile struct CPUTIMER_REGS *const P_CpuTimer2Regs;
extern volatile struct DEV_EMU_REGS *const P_DevEmuRegs;
extern volatile struct DMA_REGS *const P_DmaRegs;
extern volatile struct ECAN_REGS *const P_ECanaRegs;
extern volatile struct LAM_REGS *const P_ECanaLAMRegs;
extern volatile struct MOTS_REGS *const P_ECanaMOTSRegs;
extern volatile struct MOTO_REGS *const P_ECanaMOTORegs;
extern volatile struct ECAN_MBOXES *const P_ECanaMboxes;
extern volatile struct ECAP_REGS *const P_ECap1Regs;
extern volatile struct EPWM_REGS *const P_EPwm1Regs;
extern volatile struct EPWM_REGS *const P_EPwm2Regs;
extern volatile struct EPWM_REGS *const P_EPwm3Regs;
extern volatile struct EPWM_REGS *const P_EPwm4Regs;
extern volatile struct EPWM_REGS *const P_EPwm5Regs;
extern volatile struct EPWM_REGS *const P_EPwm6Regs;
extern volatile struct EPWM_REGS *const P_EPwm7Regs;
extern volatile struct EQEP_REGS *const P_EQep1Regs;
extern volatile struct EQEP_REGS *const P_EQep2Regs;
extern volatile struct GPIO_CTRL_REGS *const P_GpioCtrlRegs;
extern volatile struct GPIO_DATA_REGS *const P_GpioDataRegs;
extern volatile struct GPIO_INT_REGS *const P_GpioIntRegs;
extern volatile struct HRCAP_REGS *const P_HRCap1Regs;
extern volatile struct HRCAP_REGS *const P_HRCap2Regs;
extern volatile struct I2C_REGS *const P_I2caRegs;
extern volatile struct LIN_REGS *const P_LinaRegs;
extern volatile struct PIE_CTRL_REGS *const P_PieCtrlRegs;
extern volatile struct XINTRUPT_REGS *const P_XIntruptRegs;
extern volatile struct PIE_EMU_REGS *const P_PieEmuRegs;
extern volatile struct PIE_VECT_TABLE *const P_PieVectTable;
extern volatile struct SCI_REGS *const P_SciaRegs;
extern volatile struct SPI_REGS *const P_SpiaRegs;
extern volatile struct SPI_REGS *const P_SpibRegs;
extern volatile struct SYS_CTRL_REGS *const P_SysCtrlRegs;
extern volatile struct SYS_PWR_CTRL_REGS *const P_SysPwrCtrlRegs;
extern volatile struct SYS_LDO_OSC_CTRL_REGS *const P_SysLdoOSCCtrlRegs;
extern volatile struct CSM_PWL *const P_CsmPwl;
extern volatile struct FLASH_REGS *const P_FlashRegs;

//===========================================================================
// End of file.
//===========================================================================
