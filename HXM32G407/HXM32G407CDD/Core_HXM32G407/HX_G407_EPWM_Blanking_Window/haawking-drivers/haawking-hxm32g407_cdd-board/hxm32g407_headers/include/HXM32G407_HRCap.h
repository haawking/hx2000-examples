//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.382597
//
//#############################################################################


#ifndef HXM32G407_HRCAP_H
#define HXM32G407_HRCAP_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// HRCAP Individual Register Bit Definitions:

struct HCCTL_BITS {                           // bits description
    Uint32 SOFTRESET:1;                       // 0 Soft Reset
    Uint32 RISEINTE:1;                        // 1 RISE Capture Interrupt Enable Bit
    Uint32 FALLINTE:1;                        // 2 FALL Capture Interrupt Enable Bit
    Uint32 OVFINTE:1;                         // 3 Counter Overflow Interrupt Enable Bit
    Uint32 rsvd1:4;                           // 7:4 reserved
    Uint32 HCCAPCLKSEL:1;                     // 8 Capture Clock Select Bit
    Uint32 rsvd2:7;                           // 15:9 reserved
    Uint32 rsvd3:16;                          // 31:16 reserved
};

union HCCTL_REG {
    Uint32  all;
    struct  HCCTL_BITS  bit;
};

struct HCIFR_BITS {                           // bits description
    Uint32 INT:1;                             // 0 Global Interrupt Flag
    Uint32 RISE:1;                            // 1 RISE Capture Interrupt Flag
    Uint32 FALL:1;                            // 2 FALL Capture Interrupt Flag
    Uint32 COUNTEROVF:1;                      // 3 Counter Overflow Interrupt Flag
    Uint32 RISEOVF:1;                         // 4 RISE Interrupt Overflow Event Flag
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union HCIFR_REG {
    Uint32  all;
    struct  HCIFR_BITS  bit;
};

struct HCICLR_BITS {                          // bits description
    Uint32 INT:1;                             // 0 Global Interrupt Flag Clear Bit
    Uint32 RISE:1;                            // 1 RISE Capture Interrupt Flag Clear Bit
    Uint32 FALL:1;                            // 2 FALL Capture Interrupt Flag Clear Bit
    Uint32 COUNTEROVF:1;                      // 3 Counter Overflow Interrupt Flag Clear Bit
    Uint32 RISEOVF:1;                         // 4 RISE Int. Overflow Event Flag Clear Bit
    Uint32 rsvd1:11;                          // 15:5 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union HCICLR_REG {
    Uint32  all;
    struct  HCICLR_BITS  bit;
};

struct HCIFRC_BITS {                          // bits description
    Uint32 rsvd1:1;                           // 0 reserved
    Uint32 RISE:1;                            // 1 RISE Capture Interrupt Flag Force Bit
    Uint32 FALL:1;                            // 2 FALL Capture Interrupt Flag Force Bit
    Uint32 COUNTEROVF:1;                      // 3 Counter Overflow Interrupt Flag Force Bit
    Uint32 rsvd2:28;                          // 31:4 reserved
};

union HCIFRC_REG {
    Uint32  all;
    struct  HCIFRC_BITS  bit;
};

struct HCCAL_BITS {                           // bits description
    Uint32 DLL_START_POINT:8;                 // 7:0 Default:0x20
    Uint32 PHASE_DETECT_SEL:3;                // 10:8 Default:0x0
    Uint32 rsvd1:3;                           // 13:11 reserved
    Uint32 HRCAPMODE:1;                       // 14 capture mode selection
    Uint32 HRPWMSEL:1;                        // 15 HRPWM selection
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union HCCAL_REG {
    Uint32  all;
    struct  HCCAL_BITS  bit;
};

struct HCMEPSTATUS_BITS {                     // bits description
    Uint32 LOCK_STATUS:2;                     // 1:0 /3-lock error
    Uint32 MEP_SCALE_FACTOR:8;                // 9:2  
    Uint32 rsvd1:6;                           // 15:10 reserved
    Uint32 rsvd2:16;                          // 31:16 reserved
};

union HCMEPSTATUS_REG {
    Uint32  all;
    struct  HCMEPSTATUS_BITS  bit;
};

struct  HRCAP_REGS {
    union   HCCTL_REG                        HCCTL;                       // 0x0 h0 0x00
    union   HCIFR_REG                        HCIFR;                       // 0x4 h1 0x04
    union   HCICLR_REG                       HCICLR;                      // 0x8 h2 0x08
    union   HCIFRC_REG                       HCIFRC;                      // 0xc h3 0x0C
    Uint32                                   HCCOUNTER;                   // 0x10 h4 0x10
    union   HCCAL_REG                        HCCAL;                       // 0x14 h5 0x14
    Uint32                                   HCCALMEP;                    // 0x18 h6 0x18
    union   HCMEPSTATUS_REG                  HCMEPSTATUS;                 // 0x1c h7 0x1C
    Uint32                                   rsvd1[8];                    // 0x20 Reserved
    Uint32                                   HCCAPCNTRISE0;               // 0x40 h10 0x40
    Uint32                                   rsvd2;                       // 0x44 Reserved
    Uint32                                   HCCAPCNTFALL0;               // 0x48 h12 0x48
    Uint32                                   rsvd3[5];                    // 0x4c Reserved
    Uint32                                   HCCAPCNTRISE1;               // 0x60 h18 0x60
    Uint32                                   rsvd4;                       // 0x64 Reserved
    Uint32                                   HCCAPCNTFALL1;               // 0x68 h1A 0x68
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================