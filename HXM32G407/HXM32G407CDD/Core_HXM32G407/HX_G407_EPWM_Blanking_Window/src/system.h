
#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "HXM32G407_config.h"

//epwm
void InitEpwm2_Example(void);

//epwm_tz
void INTERRUPT epwm2_tz_isr(void);

void InitLED(void);

#endif /* SYSTEM_H_ */
