#include "system.h"

/******************************************************************
 *函数名：void INTERRUPT epwm2_tz_isr(void)
 *参 数：无
 *返回值：无
 *作 用：epwm2-tz中断服务函数
 ******************************************************************/
void INTERRUPT epwm2_tz_isr(void)
{
	EALLOW;
	/*清除数字比较输出 A 事件 2 的标志*/
	EPwm2Regs.TZCLR.bit.DCAEVT2 = 1;

	EPwm2Regs.TZCLR.bit.DCBEVT1 = 1;
	/*清除中断标志位*/
	EPwm2Regs.TZCLR.bit.INT = 1;
	EDIS;
	/*PIE中断应答*/
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}
