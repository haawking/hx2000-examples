//###########################################################################
//
// FILE:    enable_interrupt.S
//
// TITLE:   enable_interrupt Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: HXM32G407 Support Library V1.0.1 $
// $Release Date: 2023-03-14 07:38:45 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

.section  .text 

.global	 enable_interrupt



enable_interrupt:
    li a0, 0x88
    csrs mstatus, a0
	ret


	

