/******************************************************************
 文 档 名：      HXM32G407_HRCAP_Capture_PWM
 开 发 环 境：  Haawking IDE V2.1.3
 开 发 板：      Core_HXM32G407_V1.3
                       Start_HXM32G407_V1.2
 D S P：          HXM32G407
 使 用 库：
 作     用：      通过定时器和GPIO的配置，实现LED和蜂鸣器的操作效果
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：HRCAP捕获PWM输出

 连接方式：将GPIO0、GPIO2分别与GPIO26、GPIO11连接

 现象： 一、被HRCAP1捕获的EPWM1周期先减小D401闪，随后周期增大，D400闪
			二、被HRCAP2捕获的EPWM2周期先增大D402闪，随后周期减小，蜂鸣器间断响

 版 本：      V1.0.0
 时 间：      2022年8月25日
 作 者：      heyang
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "HXM32G407_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "HRCAP.h"

Uint16 first;
Uint16 PULSELOW;
Uint16 PULSEHIGH;

int main(void)
{
	/*系统时钟初始化*/
	InitSysCtrl();
	/*LED初始化*/
	InitLED();
	/*HRCAP与EPWM的GPIO引脚定义*/
	InitHRCapGpio();
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	/*关中断*/
	IER = 0x0000;
	IFR = 0x0000;
	/*打开中断向量表*/
	InitPieVectTable();

	EALLOW;
	/*中断向量表HRCAP1_INT指向执行捕获PWM输出上升沿的中断服务程序*/
	PieVectTable.HRCAP1_INT = &hrcap1_isr;
	/*中断向量表HRCAP2_INT指向执行捕获PWM输出下降沿的中断服务程序*/
	PieVectTable.HRCAP2_INT = &hrcap2_isr;
	EDIS;

	/*HRCAP1上升沿捕获与HRCAP2下降沿捕获功能配置*/
	HRCAP1_Config();
	HRCAP2_Config();

	EALLOW;
	/*禁止EPWM的时基使能，允许EPWM初始化配置写入*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*EPWM的初始化配置:PWM1向下计数，PWM2向上计数*/
	ePWM1_Config(1000);
	ePWM2_Config(1000);

	EALLOW;
	/*打开EPWM的时基使能，使EPWM的初始化配置起作用*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	/*指令周期延迟决策变量定义，用于实现捕获EPWM波*/
	first = 0;
	/*使能打开IER的第4组中断向量*/
	IER |= M_INT4;
	/*使能打开IER的第4组中断向量的第七、八个向量*/
	PieCtrlRegs.PIEIER4.bit.INTx7 = 1;
	PieCtrlRegs.PIEIER4.bit.INTx8 = 1;
	/*使能打开全局中断*/
	EINT;
	while(1)
	{
	}
	return 0;
}

// ----------------------------------------------------------------------------
