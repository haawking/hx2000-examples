#ifndef epwm_h_
#define epwm_h_

#include "HXM32G407_config.h"

void InitEPwm1Example(void);
void InitEPwm2Example(void);
void InitEPwm3Example(void);

void INTERRUPT epwm1_isr(void);
void INTERRUPT epwm2_isr(void);
void INTERRUPT epwm3_isr(void);

void epwm1_pc(void);
void epwm2_pc(void);

#endif/*epwm_h_*/
