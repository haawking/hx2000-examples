//#############################################################################
//
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd
// http://www.haawking.com/ All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Beijing Haawking Technology Co.,Ltd nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
//#############################################################################
//
// Release for HXM32G407MCT7CDD, Bitfield DriverLib, 1.0.2
//
// Release time: 2023-08-28 19:28:37.496274
//
//#############################################################################


#ifndef HXM32G407_SPI_H
#define HXM32G407_SPI_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// SPI Individual Register Bit Definitions:

struct SPICCR_BITS {                          // bits description
    Uint32 SPICHAR:4;                         // 3:0 Character length control
    Uint32 SPILBK:1;                          // 4 Loop-back enable/disable
    Uint32 rsvd1:1;                           // 5 reserved
    Uint32 CLKPOLARITY:1;                     // 6 Clock polarity
    Uint32 SPISWRESET:1;                      // 7 SPI SW Reset
    Uint32 rsvd2:8;                           // 15:8 reserved
    Uint32 rsvd3:16;                          // 31:16  
};

union SPICCR_REG {
    Uint32  all;
    struct  SPICCR_BITS  bit;
};

struct SPICTL_BITS {                          // bits description
    Uint32 SPIINTENA:1;                       // 0 Interrupt enable
    Uint32 TALK:1;                            // 1 Master/Slave transmit enable
    Uint32 MASTER_SLAVE:1;                    // 2 Network control mode
    Uint32 CLK_PHASE:1;                       // 3 Clock phase select
    Uint32 OVERRUNINTENA:1;                   // 4 Overrun interrupt enable
    Uint32 DMA_ENABLE:1;                      // 5  
    Uint32 rsvd1:10;                          // 15:6 reserved
    Uint32 rsvd2:16;                          // 31:16  
};

union SPICTL_REG {
    Uint32  all;
    struct  SPICTL_BITS  bit;
};

struct SPISTS_BITS {                          // bits description
    Uint32 rsvd1:5;                           // 4:0 reserved
    Uint32 BUFFULL_FLAG:1;                    // 5 SPI transmit buffer full flag
    Uint32 INT_FLAG:1;                        // 6 SPI interrupt flag
    Uint32 OVERRUN_FLAG:1;                    // 7 SPI reciever overrun flag
    Uint32 rsvd2:8;                           // 15:8 reserved
    Uint32 rsvd3:16;                          // 31:16  
};

union SPISTS_REG {
    Uint32  all;
    struct  SPISTS_BITS  bit;
};

struct SPIFFTX_BITS {                         // bits description
    Uint32 TXFFIL:5;                          // 4:0 Interrupt level
    Uint32 TXFFIENA:1;                        // 5 Interrupt enable
    Uint32 TXFFINTCLR:1;                      // 6 Clear INT flag ///?? 0
    Uint32 TXFFINT:1;                         // 7 INT flag
    Uint32 TXFFST:5;                          // 12:8 FIFO status
    Uint32 TXFIFO:1;                          // 13 FIFO reset
    Uint32 SPIFFENA:1;                        // 14 Enhancement enable
    Uint32 SPIRST:1;                          // 15 Reset SPI
    Uint32 rsvd1:16;                          // 31:16  
};

union SPIFFTX_REG {
    Uint32  all;
    struct  SPIFFTX_BITS  bit;
};

struct SPIFFRX_BITS {                         // bits description
    Uint32 RXFFIL:5;                          // 4:0 Interrupt level
    Uint32 RXFFIENA:1;                        // 5 Interrupt enable
    Uint32 RXFFINTCLR:1;                      // 6 Clear INT flag
    Uint32 RXFFINT:1;                         // 7 INT flag
    Uint32 RXFFST:5;                          // 12:8 FIFO status
    Uint32 RXFIFORESET:1;                     // 13 FIFO reset
    Uint32 RXFFOVFCLR:1;                      // 14 Clear overflow
    Uint32 RXFFOVF:1;                         // 15 FIFO overflow
    Uint32 rsvd1:16;                          // 31:16  
};

union SPIFFRX_REG {
    Uint32  all;
    struct  SPIFFRX_BITS  bit;
};

struct SPIFFCT_BITS {                         // bits description
    Uint32 TXDLY:8;                           // 7:0 FIFO transmit delay
    Uint32 rsvd1:8;                           // 15:8 reserved
    Uint32 rsvd2:16;                          // 31:16  
};

union SPIFFCT_REG {
    Uint32  all;
    struct  SPIFFCT_BITS  bit;
};

struct SPIPRI_BITS {                          // bits description
    Uint32 TRIWIRE:1;                         // 0 -wire mode select bit
    Uint32 STEINV:1;                          // 1 SPISTE inversion bit
    Uint32 rsvd1:2;                           // 3:2 reserved
    Uint32 FREE:1;                            // 4 Free emulation mode control
    Uint32 SOFT:1;                            // 5 Soft emulation mode control
    Uint32 rsvd2:1;                           // 6 rsvd
    Uint32 rsvd3:9;                           // 15:7 reserved
    Uint32 rsvd4:16;                          // 31:16  
};

union SPIPRI_REG {
    Uint32  all;
    struct  SPIPRI_BITS  bit;
};

struct  SPI_REGS {
    union   SPICCR_REG                       SPICCR;                      // 0x0 Configuration register 00
    union   SPICTL_REG                       SPICTL;                      // 0x4 Operation control register 04
    union   SPISTS_REG                       SPISTS;                      // 0x8 Status register 08
    Uint32                                   SPIBRR;                      // 0xc Baud Rate 0C
    Uint32                                   SPIRXEMU;                    // 0x10 Emulation buffer 10
    Uint32                                   SPIRXBUF;                    // 0x14 Serial input buffer 14
    Uint32                                   SPITXBUF;                    // 0x18 Serial output buffer 18
    Uint32                                   SPIDAT;                      // 0x1c Serial data 1C
    union   SPIFFTX_REG                      SPIFFTX;                     // 0x20 FIFO transmit register 20
    union   SPIFFRX_REG                      SPIFFRX;                     // 0x24 FIFO recieve register 24
    union   SPIFFCT_REG                      SPIFFCT;                     // 0x28 FIFO control register 28
    union   SPIPRI_REG                       SPIPRI;                      // 0x2c FIFO Priority control 2C
};


#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif

//===========================================================================
// End of file.
//===========================================================================
