//###########################################################################
//
// FILE:    DSP2803x_Dma_defines.h
//
// TITLE:   #defines used in Dma examples
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-10-26 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef DSP2803x_DMA_DEFINES_H
#define DSP2803x_DMA_DEFINES_H


#ifdef __cplusplus
extern "C" {
#endif

//
// MODE
//
// PERINTSEL bits
//

#define DMA_XINT1         1
#define DMA_XINT2        2
#define DMA_XINT3        3
#define DMA_TINT0        4
#define DMA_TINT1         5
#define DMA_TINT2        6
#define DMA_ADC0        7
#define DMA_ADC1         8
#define DMA_ADC2        9
#define DMA_ADC3        10
#define DMA_ADC4        11
#define DMA_ADC5        12
#define DMA_ADC6        13
#define DMA_ADC7        14
#define DMA_ADC8        15
#define DMA_SPIA_TX     16
#define DMA_SPIA_RX     17
#define DMA_SPIB_TX     18
#define DMA_SPIB_RX     19
#define DMA_I2CA_TX     20
#define DMA_I2CA_RX     21
#define DMA_SCIA_TX     22
#define DMA_SCIA_RX     23
#define DMA_EPWM1_SOCA      24
#define DMA_EPWM1_SOCB       25
#define DMA_EPWM2_SOCA      26
#define DMA_EPWM2_SOCB       27
#define DMA_EPWM3_SOCA      28
#define DMA_EPWM3_SOCB      29
#define DMA_EPWM4_SOCA      30
#define DMA_EPWM4_SOCB      31
#define DMA_EPWM5_SOCA      32
#define DMA_EPWM5_SOCB      33
#define DMA_EPWM6_SOCA      34
#define DMA_EPWM6_SOCB       35
#define DMA_EPWM7_SOCA       36
#define DMA_EPWM7_SOCB        37
#define DMA_EPWM8_SOCA        38
#define DMA_EPWM8_SOCB        39

//
// OVERINTE bit
//
#define	OVRFLOW_DISABLE	0x0
#define	OVEFLOW_ENABLE	0x1

//
// PERINTE bit
//
#define	PERINT_DISABLE	0x0
#define	PERINT_ENABLE   0x1

//
// CHINTMODE bits
//
#define	CHINT_BEGIN		0x0
#define	CHINT_END     	0x1

//
// ONESHOT bits
//
#define	ONESHOT_DISABLE	0x0
#define	ONESHOT_ENABLE	0x1

//
// CONTINOUS bit
//
#define	CONT_DISABLE	0x0
#define	CONT_ENABLE 	0x1

//
// SYNCE bit
//
#define	SYNC_DISABLE	0x0
#define	SYNC_ENABLE     0x1

//
// SYNCSEL bit
//
#define	SYNC_SRC		0x0
#define	SYNC_DST        0x1

//
// DATASIZE bit
//
#define	SIXTEEN_BIT    	0x0
#define	THIRTYTWO_BIT   0x1

//
// CHINTE bit
//
#define	CHINT_DISABLE	0x0
#define	CHINT_ENABLE   	0x1

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif

//
// End of file
//

