//###########################################################################
//
// FILE:    ier_unset.S
//
// TITLE:   ier_unset Initialization & Support Functions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-10-26 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


.section  .text 

.global	 ifr_set

ifr_set:
csrs 0x344,a0  //IFR |= a0 
 ret


	

