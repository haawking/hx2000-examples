/******************************************************************
 文  档  名：  HX_DSC28035_CpuTimer_Interrupt
 开 发 环 境：Haawking IDE V2.1.5
 开 发 板：     Core_DSC28035
                      Start_DSC28035
 D S P：       DSC28035
 使 用 库：
 作	    用：   定时器1中断例程
 说	    明：
 ----------------------例程使用说明-----------------------------
 *  CpuTimer1 作为500ms周期
 *  LED1 闪烁由CpuTimer1中断控制
 *
 现        象：LED1闪烁
 版	    本：V1.0.0
 时	    间：2023年12月13日
 作	    者：
 mail： support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "timer.h"

/*全局变量*/
uint32 count;
/*函数原型*/
void InitLED(void);

int main(void)
{

    InitSysCtrl();  //Initializes the System Control registers to a known state.

    /*关闭中断*/
    DINT;

    /*初始化PIE控制*/
    InitPieCtrl();

    /*关闭CPU中断，清除中断标志位*/
    IER = 0x0000;
    IFR = 0x0000;

    /*初始化PIE中断向量表*/
    InitPieVectTable();

    InitLED(); /*初始化LED*/

    Timer_init(); /*定时器0 定时器1 初始化，定时器0为1ms周期中断 定时器1为500ms */


    /*中断配置步骤-----5*/
    //PieCtrlRegs.PIECTRL.bit.ENPIE = 1;    /*Enable the PIE block*/
    EINT;


    while(1)
    {

    }

	return 0;
}

/******************************************************************
 函数名：void InitLED()
 参	数：无
 返回值：无
 作	用：初始化LED
 说	明：
 ******************************************************************/
void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0; /*普通IO，对应LED1灯*/
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1; /*输出*/

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0; /*普通IO，对应LED1灯*/
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1; /*输出*/

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; /*普通IO，对应LED1灯*/
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1; /*输出*/
	EDIS;

}


// ----------------------------------------------------------------------------
