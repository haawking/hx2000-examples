//#############################################################################
//
// FILE: CLAmath.h
//
// DESCRIPTION: CLA Math Library Prototypes
//
//#############################################################################


#ifndef __CLAMATH_H__
#define __CLAMATH_H__

//#############################################################################
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//#############################################################################
#ifdef __cplusplus
extern "C"
{
#endif
//#############################################################################
//
// Includes
//
//#############################################################################
#include <stdint.h>

//#############################################################################
//
// Macro Definitions
//
//#############################################################################

typedef float         float32_t;
typedef long double   float64_t;


//#############################################################################
//
// Structures, variables and typedefs.
//
//#############################################################################

//CLAsincosTable Variables
extern float32_t CLAsincosTable[];
extern float32_t CLAsinTable[];
extern float32_t *CLAsincosTable_Sin0;
extern float32_t CLAcosTable[];
extern float32_t *CLAsincosTable_Cos0;
extern float32_t *CLAsinTableEnd;
extern float32_t *CLAcosTableEnd;
extern float32_t *CLAsincosTableEnd;
extern float32_t CLAsincosTable_TABLE_SIZE;
extern float32_t CLAsincosTable_TABLE_SIZEDivTwoPi;
extern float32_t CLAsincosTable_TwoPiDivTABLE_SIZE;
extern float32_t CLAsincosTable_TABLE_MASK;
extern float32_t CLAsincosTable_Coef0;
extern float32_t CLAsincosTable_Coef1;
extern float32_t CLAsincosTable_Coef1_pos;
extern float32_t CLAsincosTable_Coef2;
extern float32_t CLAsincosTable_Coef3;
extern float32_t CLAsincosTable_Coef3_neg;

//CLAatanTable Variables
extern float32_t CLAatan2HalfPITable[];
extern float32_t CLAatan2Table[];
extern float32_t *CLAatan2TableEnd;
extern float32_t *CLAINV2PI;

//CLAacosineTable Variables
extern float32_t CLAacosinHalfPITable[];
extern float32_t CLAacosinTable[];
extern float32_t *CLAacosinTableEnd;

//CLAasineTable Variables
extern float32_t CLAasinHalfPITable[];
extern float32_t CLAasinTable[];
extern float32_t *CLAasinTableEnd;

//CLAexpTable Variables
extern float32_t CLAINV1,CLAINV2,CLAINV3,CLAINV4;
extern float32_t CLAINV5,CLAINV6,CLAINV7,CLALOG10;
extern float32_t CLAExpTable[];
extern float32_t *CLAExpTableEnd;

//CLAlnTable Variables
extern float32_t CLALNV2,CLALNVe,CLALNV10,CLABIAS;
extern long CLALN_TABLE_MASK1,CLALN_TABLE_MASK2;
extern float32_t CLALnTable[];
extern float32_t *CLALnTableEnd;

//Linker Defined variables
extern uint16_t  _cla_scratchpad_start;
extern uint16_t  _cla_scratchpad_end;

//#############################################################################
//
// Function Prototypes
//
//#############################################################################
extern float32_t CLAacos( float32_t fVal );
extern float32_t CLAacos_spc( float32_t fVal );
extern float32_t CLAasin( float32_t fVal );
extern float32_t CLAatan( float32_t fVal );
extern float32_t CLAatan2( float32_t fVal1, float32_t fVal2 );
extern float32_t CLAatan2PU( float32_t fVal1, float32_t fVal2 );
extern float32_t CLAcos( float32_t fAngleRad);
extern float32_t CLAcosPU( float32_t fAngleRadPU );
extern float32_t CLAdiv( float32_t fNum, float32_t fDen);
extern float32_t CLAexp( float32_t fVal);
extern float32_t CLAexp10( float32_t fVal);
extern float32_t CLAexp2( float32_t fNum, float32_t fDen );
extern float32_t CLAisqrt( float32_t fVal );
extern float32_t CLAln( float32_t fVal);
extern float32_t CLAlog10( float32_t fVal);
extern float32_t CLAsin( float32_t fAngleRad );
extern float32_t CLAsinPU( float32_t fAngleRadPU);
extern float32_t CLAsqrt( float32_t fVal);
extern void CLAsincos(float32_t fAngleRad, float32_t *y_sin, float32_t *y_cos);
extern float32_t CLAexpN(float32_t fVal, float32_t N);
extern float32_t CLAlogN(float32_t fVal, float32_t N);

//#############################################################################
//
// Inline Functions
//
//#############################################################################
#define CLAMATH_TWOPI_DIV_TABLE_SIZE   0.04908738521234f
#define CLAMATH_LN_2                   0.693147181f
#define CLAMATH_LN_10                  2.302585093f
#define CLAMATH_TABLE_SIZE             64f
#define CLAMATH_TABLE_SIZE_M_1         63.0f
#define CLAMATH_PI                     3.141592653589f
#define CLAMATH_PI_DIV_TWO             1.570796327f


//###########################################################################
//
//End of the C bindings section for C++ compilers.
//
//###########################################################################
#ifdef __cplusplus
}
#endif //__cplusplus

#endif // __CLAMATH_H__
