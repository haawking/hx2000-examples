/******************************************************************
 工 程 名：      f28035_cla_pwm_control
 开 发 环 境：Haawking IDE V2.2.1
 开 发 板 ：   Core_DSC28035PNS_V1.0
 D S P：      DSC28035
 使 用 库：    无
 作     用：
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------

 描  述：      CLA Task 1 更新 CMP参数，产生不同的占空比

 现  象：     可查看左侧工程目录 doc文件夹

 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：      Gengzhuo
 @mail：   support@mail.haawking.com
 ******************************************************************/

// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "CLAmath.h"
#include "cla.h"

//
// Defines
//

//
// Globals
//
EPWM_INFO epwm1_info;
EPWM_INFO epwm2_info;
EPWM_INFO epwm3_info;

//
//Task 1 (C) Variables
//

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//

//
// Function Prototypes
//
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void);
__interrupt void cla1_task2_isr(void);
__interrupt void cla1_task3_isr(void);
__interrupt void cla1_task4_isr(void);
__interrupt void cla1_task5_isr(void);
__interrupt void cla1_task6_isr(void);
__interrupt void cla1_task7_isr(void);
__interrupt void cla1_task8_isr(void);

//
// Main
//
int main(void)
{

	InitSysCtrl();

	DINT;

	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	EALLOW;
	PieVectTable.EPWM1_INT = &epwm1_isr;
	PieVectTable.EPWM2_INT = &epwm2_isr;
	PieVectTable.EPWM3_INT = &epwm3_isr;
	EDIS;

	CLA_configClaMemory();

	CLA_initCpu1Cla1();

	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	InitEPwm1();
	InitEPwm2();
	InitEPwm3();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	IER |= M_INT3;

	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

	EINT;


	for (;;)
	{

	}

	return 0;
}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
	EALLOW;
	//友商F28035老版本CLA协处理器的配置方式
	//Cla1Regs.MMEMCFG.bit.PROGE         = 1;
	//Cla1Regs.MCTL.bit.IACKE        = 1;
	//Cla1Regs.MMEMCFG.bit.RAM0E        = CLARAM0_ENABLE;
	//Cla1Regs.MMEMCFG.bit.RAM1E        = CLARAM1_ENABLE;
	//昊芯CLA协处理器的配置方式，LS1配置为CLA独享的代码区，LS0和LS2配置为CPU和CLA共享的数据区
	Cla1Regs.LSMSEL.bit.MSEL_LS0 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS1 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS2 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS0 = 0;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS1 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS2 = 0;
	EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
	EALLOW;
	PieVectTable.CLA1_INT1 = &cla1_task1_isr;
	PieVectTable.CLA1_INT2 = &cla1_task2_isr;
	PieVectTable.CLA1_INT3 = &cla1_task3_isr;
	PieVectTable.CLA1_INT4 = &cla1_task4_isr;
	PieVectTable.CLA1_INT5 = &cla1_task5_isr;
	PieVectTable.CLA1_INT6 = &cla1_task6_isr;
	PieVectTable.CLA1_INT7 = &cla1_task7_isr;
	PieVectTable.CLA1_INT8 = &cla1_task8_isr;
	EDIS;

	//
	// Compute all CLA task vectors
	//
	EALLOW;
	Cla1Regs.MVECT1 = (Uint32) &Cla1Task1;
	Cla1Regs.MVECT2 = (Uint32) &Cla1Task2;
	Cla1Regs.MVECT3 = (Uint32) &Cla1Task3;
	Cla1Regs.MVECT4 = (Uint32) &Cla1Task4;
	Cla1Regs.MVECT5 = (Uint32) &Cla1Task5;
	Cla1Regs.MVECT6 = (Uint32) &Cla1Task6;
	Cla1Regs.MVECT7 = (Uint32) &Cla1Task7;
	Cla1Regs.MVECT8 = (Uint32) &Cla1Task8;
	Cla1Regs._MVECTBGRND =   (uint32)&Cla1BackgroundTask;
	Cla1Regs.MIER.all = 0xFF;

	Cla1Regs.CLA1TASKSRCSEL1.bit.TASK1 = 36;
    Cla1Regs._MCTLBGRND.bit.BGEN = 1;
	Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
	EDIS;

	PieCtrlRegs.PIEIER11.all = 0xFFFF;
	IER |= M_INT11;
	EINT;
}

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task2_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task3_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task4_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task5_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task6_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task7_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task8_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

//
// InitEPwm1Example - EPwm1 example
//
void InitEPwm1()
{
	//
	// Setup TBCLK
	//
	EPwm1Regs.TBPRD = EPWM1_TIMER_TBPRD;   // Set timer period 801 TBCLKs
	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;   // Phase is 0
	EPwm1Regs.TBCTR = 0x0000;              // Clear counter

	//
	// Set Compare values
	//
	EPwm1Regs.CMPA.half.CMPA = EPWM1_MIN_CMPA;     // Set compare A value
	EPwm1Regs.CMPB = EPWM1_MAX_CMPB;               // Set Compare B value

	//
	// Setup counter mode
	//
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	//
	// Setup shadowing
	//
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	//
	// Set actions
	//
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;    // Set PWM1A on event A, up count
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;  // Clear PWM1A on event A, down count

	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;   // Set PWM1B on event B, up count
	EPwm1Regs.AQCTLB.bit.CBD = AQ_CLEAR; // Clear PWM1B on event B, down count

	//
	// Interrupt where we will change the Compare Values
	//
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;      // Select INT on Zero event
	EPwm1Regs.ETSEL.bit.INTEN = 1;                 // Enable INT
	EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;            // Generate INT on 3rd event

	//
	// Information this example uses to keep track of the direction the
	// CMPA/CMPB values are moving, the min and max allowed values and
	// a pointer to the correct ePWM registers
	//

	//
	// Start by increasing CMPA & decreasing CMPB
	//
	epwm1_info.EPwm_CMPA_Direction = EPWM_CMP_UP;
	epwm1_info.EPwm_CMPB_Direction = EPWM_CMP_DOWN;

	epwm1_info.EPwmTimerIntCount = 0;      //Zero the interrupt counter
	epwm1_info.EPwmRegHandle = &EPwm1Regs; //Set the pointer to the ePWM module
	epwm1_info.EPwmMaxCMPA = EPWM1_MAX_CMPA;  // Setup min/max CMPA/CMPB values
	epwm1_info.EPwmMinCMPA = EPWM1_MIN_CMPA;
	epwm1_info.EPwmMaxCMPB = EPWM1_MAX_CMPB;
	epwm1_info.EPwmMinCMPB = EPWM1_MIN_CMPB;
}

//
// InitEPwm2Example - EPwm2 example
//
void InitEPwm2()
{
	//
	// Setup TBCLK
	//
	EPwm2Regs.TBPRD = EPWM2_TIMER_TBPRD;        // Set timer period 801 TBCLKs
	EPwm2Regs.TBPHS.half.TBPHS = 0x0000;        // Phase is 0
	EPwm2Regs.TBCTR = 0x0000;                   // Clear counter

	//
	// Set Compare values
	//
	EPwm2Regs.CMPA.half.CMPA = EPWM2_MIN_CMPA;     // Set compare A value
	EPwm2Regs.CMPB = EPWM2_MIN_CMPB;               // Set Compare B value

	//
	// Setup counter mode
	//
	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	//
	// Setup shadowing
	//
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	//
	// Set actions
	//
	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;    // Set PWM2A on event A, up count
	EPwm2Regs.AQCTLA.bit.CBD = AQ_CLEAR;  // Clear PWM2A on event B, down count

	EPwm2Regs.AQCTLB.bit.ZRO = AQ_CLEAR;       // Clear PWM2B on zero
	EPwm2Regs.AQCTLB.bit.PRD = AQ_SET;       // Set PWM2B on period

	//
	// Interrupt where we will change the Compare Values
	//
	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm2Regs.ETSEL.bit.INTEN = 1;                // Enable INT
	EPwm2Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event

	//
	// Information this example uses to keep track of the direction the
	// CMPA/CMPB values are moving, the min and max allowed values and
	// a pointer to the correct ePWM registers
	//

	//
	// Start by increasing CMPA & increasing CMPB
	//
	epwm2_info.EPwm_CMPA_Direction = EPWM_CMP_UP;
	epwm2_info.EPwm_CMPB_Direction = EPWM_CMP_UP;

	epwm2_info.EPwmTimerIntCount = 0;      //Zero the interrupt counter
	epwm2_info.EPwmRegHandle = &EPwm2Regs; //Set the pointer to the ePWM module
	epwm2_info.EPwmMaxCMPA = EPWM2_MAX_CMPA;  // Setup min/max CMPA/CMPB values
	epwm2_info.EPwmMinCMPA = EPWM2_MIN_CMPA;
	epwm2_info.EPwmMaxCMPB = EPWM2_MAX_CMPB;
	epwm2_info.EPwmMinCMPB = EPWM2_MIN_CMPB;
}

//
// InitEPwm3Example - EPwm3 example
//
void InitEPwm3(void)
{
	//
	// Setup TBCLK
	//
	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;    // Count up/down
	EPwm3Regs.TBPRD = EPWM3_TIMER_TBPRD;          // Set timer period
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;       // Disable phase loading
	EPwm3Regs.TBPHS.half.TBPHS = 0x0000;          // Phase is 0
	EPwm3Regs.TBCTR = 0x0000;                     // Clear counter
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;      // Clock ratio to SYSCLKOUT
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	//
	// Setup shadow register load on ZERO
	//
	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	//
	// Set Compare values
	//
	EPwm3Regs.CMPA.half.CMPA = EPWM3_MIN_CMPA;    // Set compare A value
	EPwm3Regs.CMPB = EPWM3_MAX_CMPB;              // Set Compare B value

	//
	// Set Actions
	//
	EPwm3Regs.AQCTLA.bit.PRD = AQ_SET;    // Set PWM3A on period
	EPwm3Regs.AQCTLA.bit.CBD = AQ_CLEAR;  // Clear PWM3A on event B, down count

	EPwm3Regs.AQCTLB.bit.PRD = AQ_CLEAR;     // Clear PWM3A on period
	EPwm3Regs.AQCTLB.bit.CAU = AQ_SET;       // Set PWM3A on event A, up count

	//
	// Interrupt where we will change the Compare Values
	//
	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm3Regs.ETSEL.bit.INTEN = 1;                // Enable INT
	EPwm3Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event

	//
	// Information this example uses to keep track of the direction the
	// CMPA/CMPB values are moving, the min and max allowed values and
	// a pointer to the correct ePWM registers
	//

	//
	// Start by increasing CMPA & decreasing CMPB
	//
	epwm3_info.EPwm_CMPA_Direction = EPWM_CMP_UP;
	epwm3_info.EPwm_CMPB_Direction = EPWM_CMP_DOWN;

	epwm3_info.EPwmTimerIntCount = 0;     // Zero the interrupt counter
	epwm3_info.EPwmRegHandle = &EPwm3Regs; //Set the pointer to the ePWM module
	epwm3_info.EPwmMaxCMPA = EPWM3_MAX_CMPA;  // Setup min/max CMPA/CMPB values
	epwm3_info.EPwmMinCMPA = EPWM3_MIN_CMPA;
	epwm3_info.EPwmMaxCMPB = EPWM3_MAX_CMPB;
	epwm3_info.EPwmMinCMPB = EPWM3_MIN_CMPB;
}

//
// update_compare -
//
void update_compare(EPWM_INFO *epwm_info)
{
	//
	// Change the CMPA/CMPB values every 10'th interrupt
	//
	if (epwm_info->EPwmTimerIntCount == 10)
	{
		epwm_info->EPwmTimerIntCount = 0;

		//
		// If we were increasing CMPA, check to see if we reached the max value
		// If not, increase CMPA else, change directions and decrease CMPA
		//
		if (epwm_info->EPwm_CMPA_Direction == EPWM_CMP_UP)
		{
			if (epwm_info->EPwmRegHandle->CMPA.half.CMPA < epwm_info->EPwmMaxCMPA)
			{
				epwm_info->EPwmRegHandle->CMPA.half.CMPA++;
			}
			else
			{
				epwm_info->EPwm_CMPA_Direction = EPWM_CMP_DOWN;
				epwm_info->EPwmRegHandle->CMPA.half.CMPA--;
			}
		}

		//
		// If we were decreasing CMPA, check to see if we reached the min value
		// If not, decrease CMPA else, change directions and increase CMPA
		//
		else
		{
			if (epwm_info->EPwmRegHandle->CMPA.half.CMPA == epwm_info->EPwmMinCMPA)
			{
				epwm_info->EPwm_CMPA_Direction = EPWM_CMP_UP;
				epwm_info->EPwmRegHandle->CMPA.half.CMPA++;
			}
			else
			{
				epwm_info->EPwmRegHandle->CMPA.half.CMPA--;
			}
		}

		//
		// If we were increasing CMPB, check to see if we reached the max value
		// If not, increase CMPB else, change directions and decrease CMPB
		//
		if (epwm_info->EPwm_CMPB_Direction == EPWM_CMP_UP)
		{
			if (epwm_info->EPwmRegHandle->CMPB < epwm_info->EPwmMaxCMPB)
			{
				epwm_info->EPwmRegHandle->CMPB++;
			}
			else
			{
				epwm_info->EPwm_CMPB_Direction = EPWM_CMP_DOWN;
				epwm_info->EPwmRegHandle->CMPB--;
			}
		}

		//
		// If we were decreasing CMPB, check to see if we reached the min value
		// If not, decrease CMPB else, change directions and increase CMPB
		//
		else
		{
			if (epwm_info->EPwmRegHandle->CMPB == epwm_info->EPwmMinCMPB)
			{
				epwm_info->EPwm_CMPB_Direction = EPWM_CMP_UP;
				epwm_info->EPwmRegHandle->CMPB++;
			}
			else
			{
				epwm_info->EPwmRegHandle->CMPB--;
			}
		}
	}
	else
	{
		epwm_info->EPwmTimerIntCount++;
	}

	return;
}

//
// epwm1_isr - EPwm1 ISR
//
__interrupt void epwm1_isr(void)
{

	//
	// Clear INT flag for this timer
	//
	EPwm1Regs.ETCLR.bit.INT = 1;

	//
	// Acknowledge this interrupt to receive more interrupts from group 3
	//
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

//
// epwm2_isr - EPwm2 ISR
//
__interrupt void epwm2_isr(void)
{

	//
	// Clear INT flag for this timer
	//
	EPwm2Regs.ETCLR.bit.INT = 1;

	//
	// Acknowledge this interrupt to receive more interrupts from group 3
	//
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

//
// epwm3_isr - EPwm3 ISR
//
__interrupt void epwm3_isr(void)
{

	//
	// Clear INT flag for this timer
	//
	EPwm3Regs.ETCLR.bit.INT = 1;

	//
	// Acknowledge this interrupt to receive more interrupts from group 3
	//
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

//
// End of file
//
