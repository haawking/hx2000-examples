#ifndef TIMER0_BASE_H_
#define TIMER0_BASE_H_

#include "dsc_config.h"

typedef struct timer0_Type
{
	union
	{
		Uint16 All;
		struct
		{
				Uint16 OnemsdFlag :1;
		} Status_Bits;
	} Mark_Para;

	Uint32 msCounter;
} timer0;

extern timer0 timer0Base;

void Timer0_init(void);

INTERRUPT void cpu_timer0_isr(void);

#endif /* TIMER0_BASE_H_ */
