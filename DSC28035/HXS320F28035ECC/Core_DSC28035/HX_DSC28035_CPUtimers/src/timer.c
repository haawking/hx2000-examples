#include "dsc_config.h"

#include "timer.h"

timer0 timer0Base;

/******************************************************************
 *函数名：Timer0_init
 *参 数：无
 *返回值：无
 *作 用：初始化Timer
 ******************************************************************/
void Timer0_init()
{
	/*初始化Cpu定时器*/
	InitCpuTimers();

	/*开启模块中断使能，位于 Timer->RegsAddr->TCR.bit.TIE = 1;
	 120MHz,1000us ，即为 1ms中断周期*/
	ConfigCpuTimer(&CpuTimer0, 120, 1000);
	/* 使能中断*/
	CpuTimer0Regs.TCR.bit.TIE = 1;
	/* 开始计时*/
	CpuTimer0Regs.TCR.bit.TSS = 0;

	/*重映射中断服务函数*/
	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	EDIS;
	/*连接CPU中断Y*/
	IER |= M_INT1;
	/*连接Y中断里的第几位*/
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
}
/******************************************************************
 *函数名：INTERRUPT void cpu_timer0_isr(void)
 *参 数：无
 *返回值：无
 *作 用：CPU 定时器0 中断服务函数
 ******************************************************************/

INTERRUPT void cpu_timer0_isr(void)
{
	timer0Base.msCounter++;
	timer0Base.Mark_Para.Status_Bits.OnemsdFlag = 1;

	/*中断响应*/
	EALLOW;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
	EDIS;
}


