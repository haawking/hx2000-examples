//###########################################################################
//
// FILE:   DSP2803x_Comp.h
//
// TITLE:  DSP2803x Device Comparator Register Definitions
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2023-10-26 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2023 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#ifndef DSP2803x_COMP_H
#define DSP2803x_COMP_H

#ifdef __cplusplus
extern "C" {
#endif




//===========================================================================
//  Comparator Register Bit Definitions
//

struct COMPCTL_BITS {            // bits      description
    Uint32   COMPDACEN:1;        // 0         Comparator/DAC  Enable
    Uint32   COMPSOURCE:1;       // 1         Source select for comparator inverting input
    Uint32   CMPINV:1;           // 2         Invert select for Comparator
    Uint32   QUALSEL:5;          // 7:3       Qualification Period for synchronized output of the comparator
    Uint32   SYNCSEL:1;          // 8         Synchronization select for output of the comparator
    Uint32   rsvd1:23;           // 31:9      Reserved
};

union COMPCTL_REG  {
    Uint32                         all;
    struct COMPCTL_BITS            bit;
};

struct COMPSTS_BITS {            // bits      description
    Uint32   COMPSTS:1;          // 0         Logical latched value of the comparator
    Uint32   rsvd1:31;           // 31:1      Reserved
};

union COMPSTS_REG  {
    Uint32                         all;
    struct COMPSTS_BITS            bit;
};

struct DACCTL_BITS
{                                // bits      description
    Uint32 DACSOURCE:1;          // 0         DAC source control bits.
    Uint32 RAMPSOURCE:3;         // 4:1       Ramp generator source control  
    Uint32 rsvd1:10;             // 13:4      Reserved
    Uint32 FREE_SOFT:2;          // 15:14     Debug Mode Bit
    Uint32 rsvd2:16;             // 31:16     Reserved
};

union  DACCTL_REG
{
    Uint32 all;
    struct DACCTL_BITS bit;
};


struct DACVAL_BITS {             // bits      description
    Uint32   DACVAL:10;          // 9:0       DAC Value bit
    Uint32   rsvd1:22;           // 31:10     Reserved
};

union DACVAL_REG  {
    Uint32                         all;
    struct DACVAL_BITS             bit;
};


union  RAMPMAXREF_ACTIVE_REG 
{
    Uint32 all;
};


union  RAMPMAXREF_SHDW_REG
{
    Uint32 all;
};


union  RAMPDECVAL_ACTIVE_REG
{
    Uint32 all;
};


union  RAMPDECVAL_SHDW_REG	
{
    Uint32 all;
};


union  RAMPSTS_REG
{
    Uint32 all;
};


struct DACEX_BITS
{                                 // bits      description
    Uint32 DACCLKDIV:4;           // 3:0       Frequency division value of DAC clock and system clock
    Uint32 rsvd1:11;              // 14:4      Reserved
    Uint32 CMPRDY:1;              // 15        CMP power-on reset completed flag  
    Uint32 rsvd2:16;              // 31:16     Reserved
};

union  DACEX_REG
{
    Uint32 all;
    struct DACEX_BITS bit;
};


//===========================================================================
//  Comparator Register Definitions
//

struct COMP_REGS {
    union  COMPCTL_REG             COMPCTL;		               //00
    union  COMPSTS_REG             COMPSTS;		               //04
    union  DACCTL_REG		       DACCTL;		               //08
    union  DACVAL_REG              DACVAL;		               //0C
    union  RAMPMAXREF_ACTIVE_REG   RAMPMAXREF_ACTIVE;          //10
    union  RAMPMAXREF_SHDW_REG	   RAMPMAXREF_SHDW;            //14
    union  RAMPDECVAL_ACTIVE_REG   RAMPDECVAL_ACTIVE;          //18
    union  RAMPDECVAL_SHDW_REG	   RAMPDECVAL_SHDW;            //1C
    union  RAMPSTS_REG			   RAMPSTS;                    //20
    union  DACEX_REG	  		   DACEX;                      //24
};

//===========================================================================
//  Comparator External References and Function Declarations
//


extern volatile struct COMP_REGS  *const P_Comp1Regs;
extern volatile struct COMP_REGS  *const P_Comp2Regs;
extern volatile struct COMP_REGS  *const P_Comp3Regs;

extern volatile struct COMP_REGS  Comp1Regs;
extern volatile struct COMP_REGS  Comp2Regs;	
extern volatile struct COMP_REGS  Comp3Regs;

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif 
//===========================================================================
// End of file
//===========================================================================
