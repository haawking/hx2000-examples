/******************************************************************
 文 档 名：     cla.c
 D S P：       DSC2803x
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年12月13日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "cla.h"

//
// Defines
//

//
// Globals
//

//
// Function Definitions
//

//
//Task 1
//
__interrupt void Cla1Task1(void)
{

	_CLA_MTVEC_INIT(); //初始化MTVEC寄存器，CLA例外后跳转的地址信息

	task1_runtimes++;
	GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
	for (u32 i = 0; i < 20000; i++)
	{
	}
	GpioDataRegs.GPBSET.bit.GPIO44 = 1;
}

//
//Task 2
//
__interrupt void Cla1Task2(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 3
//
__interrupt void Cla1Task3(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 4
//
__interrupt void Cla1Task4(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 5
//
__interrupt void Cla1Task5(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 6
//
__interrupt void Cla1Task6(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 7
//
__interrupt void Cla1Task7(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 8
//
__interrupt void Cla1Task8(void)
{
	// _CLA_MTVEC_INIT();
}

//-----------------------------------------------------------------------------
//
// Background Task
//
//-----------------------------------------------------------------------------
__interrupt void Cla1BackgroundTask(void)
{
	// _CLA_MTVEC_INIT();
	bgr_runtimes++;
	GpioDataRegs.GPBTOGGLE.bit.GPIO44 = 1;
	GpioDataRegs.GPATOGGLE.bit.GPIO25 = 1;
	for (u32 i = 0; i < 1000000; i++)
	{

	}
}

//
// End of file
//

