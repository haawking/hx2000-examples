/******************************************************************
 工 程 名：      f28035_cla_background_nesting_task
 开 发 环 境：Haawking IDE V2.2.1
 开 发 板 ：   Core_DSC28035PNS_V1.0
 D S P：      DSC28035
 使 用 库：    无
 作     用：
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------

 描  述：      CLA Task 1 打断正在执行的 Background Task

 现  象：     左侧工程目录 doc文件夹可查看波形
          0  -->  GPIO25
          1  -->  GPIO44

 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：      Gengzhuo
 @mail：   support@mail.haawking.com
 ******************************************************************/


//
// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "CLAmath.h"
#include "cla.h"

//
// Defines
//

//
// Globals
//

//
//Task 1 (C) Variables
//

uint32_t CODE_SECTION("Cla1DataRam0") bgr_runtimes = 0;
uint32_t CODE_SECTION("Cla1DataRam0") task1_runtimes = 0;
uint32_t c_count1 = 0;

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//


//
// Function Prototypes
//
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void);
__interrupt void cla1_task2_isr(void);
__interrupt void cla1_task3_isr(void);
__interrupt void cla1_task4_isr(void);
__interrupt void cla1_task5_isr(void);
__interrupt void cla1_task6_isr(void);
__interrupt void cla1_task7_isr(void);
__interrupt void cla1_task8_isr(void);

//
// Main
//
int main(void)
{

	InitSysCtrl();

	DINT;

	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	EALLOW;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;
	GpioCtrlRegs.GPADIR.bit.GPIO25 = 1;
	EDIS;

	CLA_configClaMemory();

	CLA_initCpu1Cla1();


	InitEPwm1();

	EALLOW;
	PieVectTable.EPWM1_INT = &epwm1_isr;
	EDIS;

	IER |= M_INT3;
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;


	EINT;

	for (;;)
	{

		if (Cla1Regs._MSTSBGRND.bit.RUN == 0)
		{
			EALLOW;
			Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
			EDIS;
		}
		c_count1++;

	}

	return 0;
}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
	EALLOW;
	//友商F28035老版本CLA协处理器的配置方式
	//Cla1Regs.MMEMCFG.bit.PROGE         = 1;
	//Cla1Regs.MCTL.bit.IACKE        = 1;
	//Cla1Regs.MMEMCFG.bit.RAM0E        = CLARAM0_ENABLE;
	//Cla1Regs.MMEMCFG.bit.RAM1E        = CLARAM1_ENABLE;
	//昊芯CLA协处理器的配置方式，LS1配置为CLA独享的代码区，LS0和LS2配置为CPU和CLA共享的数据区
	Cla1Regs.LSMSEL.bit.MSEL_LS0 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS1 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS2 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS0 = 0;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS1 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS2 = 0;
	EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
	EALLOW;
	PieVectTable.CLA1_INT1 = &cla1_task1_isr;
	PieVectTable.CLA1_INT2 = &cla1_task2_isr;
	PieVectTable.CLA1_INT3 = &cla1_task3_isr;
	PieVectTable.CLA1_INT4 = &cla1_task4_isr;
	PieVectTable.CLA1_INT5 = &cla1_task5_isr;
	PieVectTable.CLA1_INT6 = &cla1_task6_isr;
	PieVectTable.CLA1_INT7 = &cla1_task7_isr;
	PieVectTable.CLA1_INT8 = &cla1_task8_isr;
	EDIS;

	//
	// Compute all CLA task vectors
	//
	EALLOW;
	Cla1Regs.MVECT1 = (Uint32) &Cla1Task1;
	Cla1Regs.MVECT2 = (Uint32) &Cla1Task2;
	Cla1Regs.MVECT3 = (Uint32) &Cla1Task3;
	Cla1Regs.MVECT4 = (Uint32) &Cla1Task4;
	Cla1Regs.MVECT5 = (Uint32) &Cla1Task5;
	Cla1Regs.MVECT6 = (Uint32) &Cla1Task6;
	Cla1Regs.MVECT7 = (Uint32) &Cla1Task7;
	Cla1Regs.MVECT8 = (Uint32) &Cla1Task8;
	Cla1Regs._MVECTBGRND = (uint32) &Cla1BackgroundTask;
	Cla1Regs.MIER.all = 0xFF;
	Cla1Regs.MIER.bit.INT8 = 0;

	Cla1Regs.CLA1TASKSRCSEL1.bit.TASK1 = 36; //EPWM1INT 触发

	Cla1Regs.CLA1TASKSRCSEL2.bit.TASK8 = 0;
	Cla1Regs._MCTLBGRND.bit.BGEN = 1;
	Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
	Cla1Regs._MCTLBGRND.bit.TRIGEN = 0; //0 表示触发源为软件  1 表示硬件触发源 与Task 8相同
	EDIS;

	PieCtrlRegs.PIEIER11.all = 0xFFFF;
	IER |= M_INT11;
	EINT;
}

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void)
{

	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task2_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task3_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task4_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task5_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task6_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task7_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task8_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}


//
// InitEPwm1Example - EPwm1 example
//
void InitEPwm1()
{
	//
	// Setup TBCLK
	//
	EPwm1Regs.TBPRD = 300000;   // Set timer period
	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;   // Phase is 0
	EPwm1Regs.TBCTR = 0x0000;              // Clear counter

	//
	// Set Compare values
	//
	EPwm1Regs.CMPA.half.CMPA = 100000;     // Set compare A value
	EPwm1Regs.CMPB = 100000;               // Set Compare B value

	//
	// Setup counter mode
	//
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV4;

	//
	// Setup shadowing
	//
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	//
	// Set actions
	//
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;    // Set PWM1A on event A, up count
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;  // Clear PWM1A on event A, down count

	EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR;   // Set PWM1B on event B, up count
	EPwm1Regs.AQCTLB.bit.CBD = AQ_SET; // Clear PWM1B on event B, down count

	//
	// Interrupt where we will change the Compare Values
	//
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;      // Select INT on Zero event
	EPwm1Regs.ETSEL.bit.INTEN = 1;                 // Enable INT
	EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;            // Generate INT on 3rd event

}



//
// epwm1_isr - EPwm1 ISR
//
__interrupt void epwm1_isr(void)
{

	//
	// Clear INT flag for this timer
	//
	EPwm1Regs.ETCLR.bit.INT = 1;

	//
	// Acknowledge this interrupt to receive more interrupts from group 3
	//
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

//
// End of file
//
