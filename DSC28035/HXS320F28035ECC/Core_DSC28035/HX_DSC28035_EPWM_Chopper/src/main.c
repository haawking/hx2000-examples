﻿/******************************************************************
 文 档 名 ：      HX_DSC28035_EPWM_Chopper
 开 发 环 境：  Haawking IDE V2.2.1
 开 发 板：      Core_DSC28035_V1.0
                       Start_DSC28035
 D S P：          DSC28035
 使 用 库：：

 ----------------------例程使用说明-----------------------------
 *
 *            测试epwm功能
 *
 作 用：pwm波斩波输出
 说 明：
 （1）斩波PWM1，一次触发调制脉宽，输出频率250kHz；
（2）斩波PWM2:
  A.一次触发调制脉宽，输出频率250kHz；
  B.5M高频时钟信号调制，输出频率不变，脉宽调制62.5%输出。
  C.PWM1A的频率是PWM2A频率的3倍
 *
 *
 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();

	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();

	/*将PIE控制寄存器初始化为默认状态，该状态禁止所有PIE中断并清除所有标志*/
	InitPieCtrl();

	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;

	/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();

	/*允许访问受保护的空间*/
	EALLOW;
	/*将epmw1_isr入口地址赋给EPWM1_INT,一次触发1/8首脉宽调制斩波*/
	PieVectTable.EPWM1_INT = &epwm1_isr;
	/*将epmw2_isr入口地址赋给EPWM2_INT，一次触发1/8首脉宽调制，高频三分频、62.5%脉宽调制斩波*/
	PieVectTable.EPWM2_INT = &epwm2_isr;
	/*将epmw3_isr入口地址赋给EPWM3_INT*/
	PieVectTable.EPWM3_INT = &epwm3_isr;
	/*禁止访问受保护的空间*/
	EDIS;

	/*禁用TBCLK同步，允许EPWM初始化配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*初始化EPWM1，EPWM2，EPWM3，频率30KHz，向上向下计数模式*/
	InitEPwm1Example();
	InitEPwm2Example();
	InitEPwm3Example();
	
	EALLOW;
	/*使能TBCLK同步，EPWM配置功能起作用*/
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*禁止CPU中断并清除所有中断标志*/
	IER |= M_INT3;

	/*使能相对应的中断*/
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

	EINT;
	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
