/******************************************************************
 文 档 名 ：     HX_DSC28035_SCI
 开 发 环 境：  Haawking IDE V2.2.1
 开 发 板：      Core_DSC28035_V1.0
                       Start_DSC28035_
 D S P：          DSC28035
 使 用 库：
 作 用：SCI数据收发
 说 明：串口一端接入CH340数据线
 通过串口调试助手，打开已连接串口，先发送0x41(或字符A),然后可连续发送多组数据
 ----------------------例程使用说明-----------------------------
 *
 *            测试SCI功能
 *
 *
 * 现象：发送接收数据
 *
 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

void Scia_Config(void);
void Scia_Gpio(void);
void Scia_Autobaudlock(void);

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化pie中断控制*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER_DISABLE(0xffff);
	IFR_DISABLE(0xffff);
	/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();
	/*配置Scia_Gpio*/
	Scia_Gpio();
	/*SCIA初始化*/
	Scia_Config();
	Scia_Autobaudlock();

	while(1)
	{
		while(SciaRegs.SCIRXST.bit.RXRDY != 1)
		{
		}
		SciaRegs.SCITXBUF = SciaRegs.SCIRXBUF.bit.RXDT;
	}

	return 0;
}

// ----------------------------------------------------------------------------
