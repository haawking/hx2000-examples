/******************************************************************
 文 档 名：     HX_DSC28035_LIN
 开 发 环 境：Haawking IDE V2.2.1
 开 发 板 ：   Start_DSC28035_V1.0
 D S P：        DSC28035
 使 用 库：    无
 作     用：     通过对LIN_LIN配置，演示LIN模式的LIN通信实现，
 通过接收帧头ID触发中断，根据ID帧头对应的帧属性,
 决定主机收发状态,从而实现通信传输
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------

 连接方式：LIN转USB模块连接485A 485B,上位机观察数据

 现象：

 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include <stdio.h>
#include "dsc_config.h"
#include "hx_rv32_dev.h"
#include "hx_rv32_type.h"
#include <syscalls.h>
#include "xcustom.h"
#include "IQmathLib.h"
#include "LIN.h"

Uint16 receive_data;

int main(void)
{
	InitSysCtrl();//系统时钟初始化
	InitLinaGpio();//LIN的GPIO引脚配置

	LIN_LIN_init();//LIN_LIN功能参数配置

	InitPieCtrl();//关中断

	IER=0x0000;//清中断
	IFR=0x0000;

	InitPieVectTable();//初始化中断向量表

	EALLOW;
	PieVectTable.LIN1INTA=&LIN1_isr;//ID有效掩码匹配中断,用于判断主机的传输状态
	EDIS;

	IER|=M_INT9;//打开CPU的IER中断

	PieCtrlRegs.PIEIER9.bit.INTx4=1;//打开PIE对应的中断

	EINT;//打开全局中断

	LIN_interrupt_init();//LIN中断及ID配置

	LinaRegs.LINID.bit.IDBYTE=LIN_ID;//ID掩码发送，用于生成帧头

	while(1)
	{
		receive_data=LIN_trans();//LIN的发送
	}
	return 0;
}

// ----------------------------------------------------------------------------

