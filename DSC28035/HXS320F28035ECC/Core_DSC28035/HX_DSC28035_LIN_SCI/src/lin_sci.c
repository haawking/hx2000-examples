#include "system.h"

void Initlin_scia_gpio(void)
{
	EALLOW;
//	GpioCtrlRegs.GPAPUD.bit.GPIO14=0;//1禁止内部上拉，0使能引脚的内部上拉
//	GpioCtrlRegs.GPAPUD.bit.GPIO15=0;
	GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO23 = 0;
//	GpioCtrlRegs.GPAPUD.bit.GPIO9=0;
//	GpioCtrlRegs.GPAPUD.bit.GPIO11=0;

//	GpioCtrlRegs.GPAQSEL1.bit.GPIO15=3;//GPIO15 B口接收异步配置
//	GpioCtrlRegs.GPAQSEL1.bit.GPIO15=1;
//	GpioCtrlRegs.GPAQSEL1.bit.GPIO11=0x01;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 3;

//	GpioCtrlRegs.GPACTRL.bit.QUALPRD0=0x03;//0-7
//	GpioCtrlRegs.GPACTRL.bit.QUALPRD1=3;//8-15
//	GpioCtrlRegs.GPACTRL.bit.QUALPRD2=0x03;//16-23
//	GpioCtrlRegs.GPACTRL.bit.QUALPRD3=0x03;//24-31

//	GpioCtrlRegs.GPAMUX1.bit.GPIO9=2;  //LINTXA
//	GpioCtrlRegs.GPAMUX1.bit.GPIO11=2; //LINRXA
//	GpioCtrlRegs.GPAMUX1.bit.GPIO14=2; //GPIO14 RS485A发送引脚
//	GpioCtrlRegs.GPAMUX1.bit.GPIO15=2; //GPIO15 RS485B接收引脚
	GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 3; //LINTXA
	GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 3; //LINRXA

	GpioCtrlRegs.GPBPUD.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;  //输出
	GpioDataRegs.GPBCLEAR.bit.GPIO44 = 1;
	EDIS;
}

void resetLinSci(void)
{
	LinaRegs.SCIGCR0.bit.RESET = 0;
	LinaRegs.SCIGCR0.bit.RESET = 1;
	LinaRegs.SCIGCR1.bit.SWnRST = 0;
}

void setRxConfig(void)
{
	LinaRegs.SCIGCR1.bit.RXENA = 1;
	LinaRegs.SCIGCR1.bit.TXENA = 0;
}

void lin_scia_init(void)
{
	EALLOW;

	LinaRegs.SCIGCR0.bit.RESET = 0; /*Lin_sci功能重置*/
	LinaRegs.SCIGCR0.bit.RESET = 1;
	LinaRegs.SCIGCR1.bit.SWnRST = 0;

	LinaRegs.SCIGCR1.bit.LINMODE = 0;    //SCI模式
	LinaRegs.SCIGCR1.bit.COMMMODE = 0;   //SCI-idleline
	LinaRegs.SCIGCR1.bit.CLK_MASTER = 1; /*使能SCI时钟*/
	LinaRegs.SCIGCR1.bit.TIMINGMODE = 1; /*使用异步时序*/
	LinaRegs.SCIGCR1.bit.SLEEP = 0;      //禁用SCI睡眠模式

	LinaRegs.SCIGCR1.bit.CONT = 1; /*1连续传输，调试模式继续工作*/
	LinaRegs.SCIGCR1.bit.MBUFMODE = 0;   //0禁用多缓冲区模式
	LinaRegs.SCIGCR1.bit.LOOPBACK = 0;   //0禁用回环
	LinaRegs.SCIGCR1.bit.PARITYENA = 0;  //0禁用奇偶校验
	LinaRegs.SCIGCR1.bit.PARITY = 0;     //0奇校验
	LinaRegs.SCIGCR1.bit.STOP = 0;       //0一个停止位
	LinaRegs.SCIGCR1.bit.RXENA = 1;      //接收线使能
	LinaRegs.SCIGCR1.bit.TXENA = 1;      //发送线使能

	LinaRegs.SCIGCR1.bit.ADAPT = 0;          //0禁用自动波特率
	LinaRegs.BRSR.bit.SCI_LIN_PSH = 0x0000; //LIN时钟配置
	LinaRegs.BRSR.bit.SCI_LIN_PSL = 0x0185; //389    60M/(389+1+10/16)/16=9600
	LinaRegs.BRSR.bit.M = 10;               //LIN分频器配置

	LinaRegs.SCIFORMAT.bit.LENGTH = 0x7;   //缓冲SCI模式下，帧长度，8个字符，每个字符有8位
	LinaRegs.SCIFORMAT.bit.CHAR = 0x7;    //字符长度，

	LinaRegs.SCIFLR.bit.TXWAKE = 0;      //SCI采用空闲线传输，无需唤醒地址位传输
	LinaRegs.SCIFLR.bit.TXEMPTY = 1;     //0发送缓冲区与移位寄存器均加载数据
	LinaRegs.SCIFLR.bit.TXRDY = 1;       //SCITD准备好接收下一个字符

	LinaRegs.IODFTCTRL.bit.IODFTENA = 0x0;   //禁用IODFT
  //LinaRegs.SCICLEARINT.all=0xFFFFFFFF; //中断功能启用

	LinaRegs.SCISETINTLVL.bit.SETRXINTLVL = 0; //接收校验与错误中断，映射到INT0
	LinaRegs.SCISETINTLVL.bit.SETTXINTLVL = 1; //发送校验与错误中断，映射到INT1

	LinaRegs.SCISETINT.bit.SETRXINT = 1;       //接收中断启用
	LinaRegs.SCISETINT.bit.SETTXINT = 1;       //发送中断启用

	LinaRegs.SCIGCR1.bit.SWnRST = 1;

	EDIS;
}

