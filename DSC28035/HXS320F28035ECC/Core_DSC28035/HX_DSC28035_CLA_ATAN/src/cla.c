/******************************************************************
 文 档 名：     cla.c
 D S P：       DSC2803x
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年12月13日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "cla.h"

//
// Defines
//


//
// Globals
//


//
// Function Definitions
//

//Task 1 : Calculate atan(Y)
// Description:
//             Step(1):   if( 1.0 >= abs(Y) )
//                            Numerator   = abs(Y)
//                            Denominator = 1.0
//                        else
//                            Numerator   = 1.0
//                            Denominator = abs(Y)
//
//             Step(2):   Ratio = Numerator/Denominator
//
//                        Note: Ratio range = 0.0 to 1.0
//
//             Step(3):   Use the upper 6-bits of the "Ratio" value as an
//                        index into the table to obtain the coefficients
//                        for a second order equation:
//
//                        _FPUatan2Table:
//                             CoeffA0[0]
//                             CoeffA1[0]
//                             CoeffA2[0]
//                                .
//                                .
//                             CoeffA0[63]
//                             CoeffA1[63]
//                             CoeffA2[63]
//
//             Step(4):   Calculate the angle using the following equation:
//
//                        arctan(Ratio) = A0 + A1*Ratio + A2*Ratio*Ratio
//                        arctan(Ratio) = A0 + Ratio(A1 + A2*Ratio)
//
//             Step(5):   The final angle is determined as follows:
//
//                        if( Y >= 0 and 1.0 >= abs(Y) )
//                            Angle = arctan(abs(Y)/1.0)
//                        if( Y >= 0 and 1.0 <  abs(Y) )
//                            Angle = PI/2 - arctan(1.0/abs(Y))
//                        if( Y < 0 )
//                            Angle = -Angle
__interrupt void Cla1Task1 ( void )
{
    //
    //Local Variables
    //
    unsigned int uxTblIdx; //unsigned integer valued Table index
    float ratio;
    float num,den;
    float A0,A1,A2; //Table coefficients
    float *base;
    float result;

    _CLA_MTVEC_INIT(); //初始化MTVEC寄存器，CLA例外后跳转的地址信息

    //
    //Preprocessing
    //
//    __mdebugstop();
    num = fminf(fabsf(fVal),1.0f);
    den = fmaxf(fabsf(fVal),1.0f);

    ratio = (num/den); //Expected the newton raphson algo for better
                       //accuracy on the divide
    uxTblIdx = ratio * TABLE_SIZE_M_1 * 3; //convert table index to u16-bits
    uxTblIdx = uxTblIdx * 3; //Table is ordered as 3 32-bit coefficients, the
                             //index points to these triplets, hence the *3*sizeof(float)

   base = &CLAatan2Table[uxTblIdx];
   A0 = *base++;
   A1 = *base++;
   A2 = *base;

   result = A0 + ratio*(A1 + A2*ratio);

   //
   //Post processing
   //
   if(fabsf(fVal) > 1.0f)
   {
      result = PIBYTWO - result;
   }

   if(fVal < 0.0f)
   {
      result = -result;
   }

    fResult = result;
}


//
//Task 2
//
__interrupt void Cla1Task2(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 3
//
__interrupt void Cla1Task3(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 4
//
__interrupt void Cla1Task4(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 5
//
__interrupt void Cla1Task5(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 6
//
__interrupt void Cla1Task6(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 7
//
__interrupt void Cla1Task7(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 8
//
__interrupt void Cla1Task8(void)
{
	// _CLA_MTVEC_INIT();
}

//-----------------------------------------------------------------------------
//
// Background Task
//
//-----------------------------------------------------------------------------
__interrupt void Cla1BackgroundTask(void)
{
	// _CLA_MTVEC_INIT();
}

//
// End of file
//

