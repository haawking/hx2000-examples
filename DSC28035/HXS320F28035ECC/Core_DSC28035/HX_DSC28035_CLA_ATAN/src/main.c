/******************************************************************
 工 程 名：      f28035_cla_atan
 开 发 环 境：Haawking IDE V2.2.1
 开 发 板 ：   Core_DSC28035PNS_V1.0
 D S P：      DSC28035
 使 用 库：    无
 作     用：
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------

 描  述：      CLA Task 1 将计算 由于 CPU提供的 -1.0 到 1.0 之间 目标值的 反正切
                    计算方式为 查找表

 现  象：     分别将 fResult fVal pass fail 放入 Liveview视图，可查看
                   计算过程和结果

 版 本：      V1.0.0
 时 间：      2023年12月13日
 作 者：      Gengzhuo
 @mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "CLAmath.h"
#include "cla.h"

//
// Defines
//


//
// Globals
//

//
//Task 1 (C) Variables
//
float CODE_SECTION("Cla1ToCpuMsgRAM") fResult = 0.0f;
float CODE_SECTION("CpuToCla1MsgRAM") PIBYTWO = 1.570796327;
float CODE_SECTION("CpuToCla1MsgRAM") fVal = 0.0f;

float y[BUFFER_SIZE];

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//The Exponential table
//
float CODE_SECTION("Cla1DataRam0") CLAatan2Table[]={
	    0.000000000000, 1.000040679675, -0.007811069750,
	    -0.000003807022, 1.000528067772, -0.023410345493,
	    -0.000018968310, 1.001498568106, -0.038941197075,
	    -0.000052831927, 1.002943680965, -0.054358563160,
	    -0.000112417996, 1.004850785788, -0.069618206649,
	    -0.000204295885, 1.007203293243, -0.084677028723,
	    -0.000334469493, 1.009980843501, -0.099493367628,
	    -0.000508272606, 1.013159547023, -0.114027278430,
	    -0.000730276035, 1.016712263449, -0.128240790343,
	    -0.001004207980, 1.020608913557, -0.142098138715,
	    -0.001332888717, 1.024816818801, -0.155565969259,
	    -0.001718180431, 1.029301062569, -0.168613512667,
	    -0.002160952612, 1.034024867135, -0.181212728329,
	    -0.002661063159, 1.038949980222, -0.193338416410,
	    -0.003217354977, 1.044037065160, -0.204968298146,
	    -0.003827667546, 1.049246088868, -0.216083064706,
	    -0.004488862702, 1.054536702192, -0.226666395507,
	    -0.005196863579, 1.059868607573, -0.236704947279,
	    -0.005946705500, 1.065201909527, -0.246188315613,
	    -0.006732597422, 1.070497443994, -0.255108971022,
	    -0.007547992413, 1.075717083222, -0.263462171840,
	    -0.008385665599, 1.080824013499, -0.271245856476,
	    -0.009237797924, 1.085782983693, -0.278460517693,
	    -0.010096064139, 1.090560523211, -0.285109061650,
	    -0.010951723407, 1.095125128557, -0.291196654447,
	    -0.011795711029, 1.099447418304, -0.296730558920,
	    -0.012618729873, 1.103500256784, -0.301719964310,
	    -0.013411340199, 1.107258847274, -0.306175811325,
	    -0.014164046711, 1.110700795887, -0.310110614947,
	    -0.014867381831, 1.113806147717, -0.313538287176,
	    -0.015511984298, 1.116557397059, -0.316473961655,
	    -0.016088672395, 1.118939473766, -0.318933821948,
	    -0.016588511229, 1.120939707956, -0.320934934991,
	    -0.017002873645, 1.122547775367, -0.322495091022,
	    -0.017323494499, 1.123755625749, -0.323632651068,
	    -0.017542518140, 1.124557396633, -0.324366402860,
	    -0.017652539065, 1.124949314810, -0.324715425833,
	    -0.017646635823, 1.124929587773, -0.324698965683,
	    -0.017518398344, 1.124498287266, -0.324336318787,
	    -0.017261948920, 1.123657226955, -0.323646726613,
	    -0.016871957164, 1.122409836098, -0.322649280139,
	    -0.016343649294, 1.120761030918, -0.321362834119,
	    -0.015672812166, 1.118717085258, -0.319805931024,
	    -0.014855792451, 1.116285501852, -0.317996734279,
	    -0.013889491441, 1.113474885484, -0.315952970460,
	    -0.012771355912, 1.110294819035, -0.313691879968,
	    -0.011499365513, 1.106755743329, -0.311230175714,
	    -0.010072017131, 1.102868841531, -0.308584009287,
	    -0.008488306656, 1.098645928648, -0.305768944066,
	    -0.006747708579, 1.094099346648, -0.302799934724,
	    -0.004850153815, 1.089241865523, -0.299691312610,
	    -0.002796006119, 1.084086590517, -0.296456776409,
	    -0.000586037441, 1.078646875666, -0.293109387609,
	    0.001778597452, 1.072936243727, -0.289661570229,
	    0.004296386804, 1.066968312440, -0.286125114351,
	    0.006965487894, 1.060756727064, -0.282511182960,
	    0.009783753017, 1.054315099009, -0.278830321668,
	    0.012748755207, 1.047656950440, -0.275092470943,
	    0.015857813546, 1.040795664582, -0.271306980411,
	    0.019108017893, 1.033744441476, -0.267482624928,
	    0.022496252887, 1.026516258989, -0.263627622089,
	    0.026019221162, 1.019123838651, -0.259749650860,
	    0.029673465636, 1.011579616186, -0.255855871106,
	    0.033455390838, 1.003895716322, -0.251952943763,
	    0.037361283212, 0.996083931611, -0.248047051426
	};

	float atan_expected[BUFFER_SIZE]={
	    1.5395565,  1.5385494,  1.5374753,  1.5363272,
	    1.5350972,  1.5337762,  1.5323538,  1.5308176,
	    1.5291537,  1.5273454,  1.5253731,  1.5232133,
	    1.5208379,  1.5182133,  1.5152978,  1.5120405,
	    1.5083776,  1.5042281,  1.4994888,  1.4940244,
	    1.4876550,  1.4801364,  1.4711276,  1.4601392,
	    1.4464413,  1.4288993,  1.4056476,  1.3734008,
	    1.3258177,  1.2490457,  1.1071488,  0.7853982,
	    0.0000000, -0.7853982, -1.1071488, -1.2490457,
	    -1.3258177, -1.3734008, -1.4056476, -1.4288993,
	    -1.4464413, -1.4601392, -1.4711276, -1.4801364,
	    -1.4876550, -1.4940244, -1.4994888, -1.5042281,
	    -1.5083776, -1.5120405, -1.5152978, -1.5182133,
	    -1.5208379, -1.5232133, -1.5253731, -1.5273454,
	    -1.5291537, -1.5308176, -1.5323538, -1.5337762,
	    -1.5350972, -1.5363272, -1.5374753, -1.5385494
	};

uint16_t pass = 0;
uint16_t fail = 0;

//
// Function Prototypes
//
void CLA_runTest(void);
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void);
__interrupt void cla1_task2_isr(void);
__interrupt void cla1_task3_isr(void);
__interrupt void cla1_task4_isr(void);
__interrupt void cla1_task5_isr(void);
__interrupt void cla1_task6_isr(void);
__interrupt void cla1_task7_isr(void);
__interrupt void cla1_task8_isr(void);

//
// Main
//
int main(void)
{

	InitSysCtrl();

	DINT;

	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	CLA_configClaMemory();

	CLA_initCpu1Cla1();

	EINT;

	CLA_runTest();

	for (;;)
	{

	}

	return 0;
}

//
// CLA_runTest - Execute CLA task tests for specified vectors
//
void CLA_runTest(void)
{
    int16_t i;
    float error;

    //
    // Initialize the CPUToCLA1MsgRam variables here
    //
    PIBYTWO = 1.570796327;

    for(i=0; i < BUFFER_SIZE; i++)
    {
        fVal = (float)((BUFFER_SIZE/2) - i);

        Cla1ForceTask1andWait();

        //效果演示用 延迟效果
        //通过Liveview视图 刷新可查看cla task1执行计算过程
        for (u32 i = 0; i < 1000000; ++i)
        {
        }

        y[i] = fResult;
        error = fabsf(atan_expected[i]-y[i]);

        if(error < 0.1f)
        {
            pass++;
        }
        else
        {
            fail++;
        }
    }

}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
	EALLOW;
	//友商F28035老版本CLA协处理器的配置方式
	//Cla1Regs.MMEMCFG.bit.PROGE         = 1;
	//Cla1Regs.MCTL.bit.IACKE        = 1;
	//Cla1Regs.MMEMCFG.bit.RAM0E        = CLARAM0_ENABLE;
	//Cla1Regs.MMEMCFG.bit.RAM1E        = CLARAM1_ENABLE;
	//昊芯CLA协处理器的配置方式，LS1配置为CLA独享的代码区，LS0和LS2配置为CPU和CLA共享的数据区
	Cla1Regs.LSMSEL.bit.MSEL_LS0 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS1 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS2 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS0 = 0;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS1 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS2 = 0;
	EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
	EALLOW;
	PieVectTable.CLA1_INT1 = &cla1_task1_isr;
	PieVectTable.CLA1_INT2 = &cla1_task2_isr;
	PieVectTable.CLA1_INT3 = &cla1_task3_isr;
	PieVectTable.CLA1_INT4 = &cla1_task4_isr;
	PieVectTable.CLA1_INT5 = &cla1_task5_isr;
	PieVectTable.CLA1_INT6 = &cla1_task6_isr;
	PieVectTable.CLA1_INT7 = &cla1_task7_isr;
	PieVectTable.CLA1_INT8 = &cla1_task8_isr;
	EDIS;

	//
	// Compute all CLA task vectors
	//
	EALLOW;
	Cla1Regs.MVECT1 = (Uint32) &Cla1Task1;
	Cla1Regs.MVECT2 = (Uint32) &Cla1Task2;
	Cla1Regs.MVECT3 = (Uint32) &Cla1Task3;
	Cla1Regs.MVECT4 = (Uint32) &Cla1Task4;
	Cla1Regs.MVECT5 = (Uint32) &Cla1Task5;
	Cla1Regs.MVECT6 = (Uint32) &Cla1Task6;
	Cla1Regs.MVECT7 = (Uint32) &Cla1Task7;
	Cla1Regs.MVECT8 = (Uint32) &Cla1Task8;
	Cla1Regs._MVECTBGRND =   (Uint32)&Cla1BackgroundTask;
	Cla1Regs.MIER.all = 0xFF;

	Cla1Regs.CLA1TASKSRCSEL1.bit.TASK1 = 36;
    Cla1Regs._MCTLBGRND.bit.BGEN = 1;
	Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
	EDIS;

	PieCtrlRegs.PIEIER11.all = 0xFFFF;
	IER |= M_INT11;
	EINT;
}

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task2_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task3_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task4_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task5_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task6_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task7_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task8_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

//
// End of file
//

