/*******************************************************************
 文 档 名：     HX_DSC28035_ADC
 开 发 环 境：  Haawking IDE V2.2.1
 开 发 板：     Core_DSC28035
 D S P：       DSC28035
 使 用 库：
 作     用：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，ADC时钟15MHz, 通过配置ADC，PWM1的寄存器，实现ADC采集，获取被测电压对应数字量

 连接方式：把被测电压和ADCIN5连接
			       Core板上将测试的电压与F28035的与ADC通道采集输入引脚A5连接
 	 	 	       Start板上电位器RG端与ADC通道采集输入引脚A5连接

 现象：在Haawking IDE调试界面上可正确读出电压对应的数字量值adcValue
 	 	    Start板上当正常运行时，	D400	亮，		D401	灭；
 	 	    当adc采样异常时，	D400	灭，		D401	亮；

 版 本：V1.0.0
 时 间：2023年12月13日
 作 者：
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

extern void pwmInit();
extern void adcInit();
extern void gpioInit();
extern void adcIsr();
void Adc_Dummy_SOC_Config();

uint16_t adcCount = 0;
uint16_t adcValue[3] = {0,0,0};
uint16_t i = 0,j = 0;
uint16_t error = 0;

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    DINT;

    gpioInit();
    pwmInit();
    adcInit();

	// 为规避035ECC版本的ADC问题，需要调用下面的函数Adc_Dummy_SOC_Config()
    Adc_Dummy_SOC_Config();

    EALLOW;
	PieVectTable.ADCINT1 = &adcIsr;
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;
	IER |= M_INT1;
	EINT;
	EDIS;

    while(1)
    {

    }

	return 0;
}

__interrupt void adcIsr()
{
	adcCount ++;

	adcValue[0] = AdcResult.ADCRESULT0;
	if(!((adcValue[0]>0)&&(adcValue[0]<4095)))
	{
		error++;
	}
	adcValue[1] = AdcResult.ADCRESULT1;
	if(!((adcValue[1]>0)&&(adcValue[1]<4095)))
	{
		error++;
	}

	adcValue[2] = AdcResult.ADCRESULT2;
	if(!((adcValue[2]>0)&&(adcValue[2]<4095)))
	{
		error++;
	}

	if(adcCount > 1000)
	{
		adcCount = 0;
		if(error != 0)
		{
			GpioDataRegs.GPBSET.bit.GPIO41 = 1;
			GpioDataRegs.GPACLEAR.bit.GPIO31 = 1;

		}
		else
		{
			GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
			GpioDataRegs.GPASET.bit.GPIO31 = 1;

		}
	}


	AdcRegs.ADCINTFLGCLR.all = 0x1FF;
	AdcRegs.ADCINTOVFCLR.all = 0x1FF;
	PieCtrlRegs.PIEACK.bit.ACK1 = 1;
}

void pwmInit()
{
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;

	EPwm1Regs.TBPRD = 12000;               	//10k Hz
	EPwm1Regs.TBPHS.half.TBPHS = 0;
	EPwm1Regs.TBCTR = 0;
	EPwm1Regs.TBCTL.bit.FREE_SOFT = 3;
	EPwm1Regs.TBCTL.bit.CLKDIV = 0;
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm1Regs.TBCTL.bit.CTRMODE = 0;    			// up count

	EPwm1Regs.CMPA.half.CMPA = 6000;
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0; // CMPA shadow
	EPwm1Regs.CMPCTL.bit.LOADAMODE = 1; // Load on CTR = Prd

	EPwm1Regs.AQCTLA.bit.ZRO = 2;       //set
	EPwm1Regs.AQCTLA.bit.CAU = 1;       //clear

	EPwm1Regs.ETSEL.bit.SOCAEN = 1;
	EPwm1Regs.ETSEL.bit.SOCASEL = 1;
	EPwm1Regs.ETPS.bit.SOCACNT = 1;
	EPwm1Regs.ETPS.bit.SOCAPRD = 1;
	EPwm1Regs.ETCLR.bit.SOCA = 1;

	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void adcInit()
{
	InitAdc();

	EALLOW;
	AdcRegs.ADCCTL1.bit.INTPULSEPOS=1;
	//AdcRegs.ADCCTL1.bit.TEMPCONV=0;

	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 3;    // System clock is 120M / 8 = 15Mhz

	AdcRegs.INTSEL1N2.bit.INT1CONT = 1;
	AdcRegs.INTSEL1N2.bit.INT1SEL = 2;		//EOC2 TRIG
	AdcRegs.INTSEL1N2.bit.INT1E = 1;

	AdcRegs.ADCINTOVFCLR.all = 1;
	AdcRegs.ADCINTFLGCLR.all = 1;

	AdcRegs.ADCSOC0CTL.bit.CHSEL = 5;		//A5
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 5;		//SOCx触发源选择:ADCTRIG5-ePWM1
//	AdcRegs.ADCSOC1CTL.bit.ACQPS = 7;

	AdcRegs.ADCSOC1CTL.bit.CHSEL = 5;		//A5
	AdcRegs.ADCSOC1CTL.bit.TRIGSEL = 5;
//	AdcRegs.ADCSOC1CTL.bit.ACQPS = 7;

	AdcRegs.ADCSOC2CTL.bit.CHSEL = 6;		//A6
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 5;
//	AdcRegs.ADCSOC2CTL.bit.ACQPS = 7;

	EDIS;
}

void gpioInit()
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0; 	//Core_LED1
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;
	GpioDataRegs.GPASET.bit.GPIO31 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0; 	//D400 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; 	//D402 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	EDIS;
}
/*********************************************************
 *    在当前的035ECC版本中，启动ADC转换之后，需要保持ADC
 *    有SOC一直处于活跃的状态，否则，可能导致转换的结果被
 *    写入错误的ADCRESULT寄存器。
 *    此问题的详细描述，见迁移指南。
 *
 *    Adc_Dummy_SOC_Config()的作用是使用Timer2来触发一个
 *    应用中未使用的SOC（本例中是SOC15）,让ADC处于一直
 *    活跃转换的状态。
 *********************************************************/
void Adc_Dummy_SOC_Config()
{
        //An extra Timer is needed to trigger an extra ADC SOC
        EALLOW;
        CpuTimer2Regs.TCR.bit.TSS = 1;
        CpuTimer2Regs.PRD.all = 30;        //4MHz Frequency
        CpuTimer2Regs.TIM.all = 0;
        CpuTimer2Regs.TCR.bit.TRB = 1;
        CpuTimer2Regs.TCR.bit.TIE = 1;
        CpuTimer2Regs.TCR.bit.TSS = 0;
        //An extra ADC SOC is used and the result can be ignored
        AdcRegs.ADCSOC15CTL.bit.CHSEL =  8;  // B0
        AdcRegs.ADCSOC15CTL.bit.TRIGSEL = 3;  // timer2
        EDIS;
}
// ----------------------------------------------------------------------------
