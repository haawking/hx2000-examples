/******************************************************************
 文 档 名：      HX_DSC28035_Flash_API
 开 发 环 境：  Haawking IDE V2.2.1
 开 发 板：      Core_DSC28035_V1.0
 D S P：          DSC28035
 使 用 库：
 作     用：    调用ROM中的Flash API实现对片内的flash的读写擦除等操作
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz

 连接方式：

 现象：       读写正确的时候，开发板上LED301/核心板上LED1亮。
                  否则，开发板上LED301/核心板上LED1闪烁
                  在调试时，可以使用Memory视图填入Flash的地址(比如Sector 124的地址0x71E000)，
                  观察Flash中的内容。

 版 本：      V1.0.0
 时 间：      2023年5月9日
 作 者：      liyuyao
 @ mail：   support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "FlashApi.h"

#define BUFF_LEN							8							// Buffer的大小，是32bit数据的个数
#define BUFF_BYTE_NUM				(BUFF_LEN * 4)		// Buffer的字节数，是8bit数据的个数
#define FLASH_BASE_ADDR			0x700000				// Flash的起始地址
#define FLASH_SECTOR_SIZE			1024						// 一个Sector的大小，单位为Byte

// 向Flash中写入数据，每次写一个Buffer 大小的数据，写完一个Sector的循环次数
#define SECTOR_PROG_LOOP		(FLASH_SECTOR_SIZE/BUFF_BYTE_NUM)

uint16 Erase_result,Program_result;
uint32 program_buffer[BUFF_LEN];
uint32 i,j;
uint32 err_cnt;

void CODE_SECTION("ramfuncs")
Success(void)
{
	GpioDataRegs.GPACLEAR.bit.GPIO31 = 1;
	GpioDataRegs.GPBCLEAR.bit.GPIO41 = 1;
}

void CODE_SECTION("ramfuncs")
Fail(void)
{
	while(1)
	{
		GpioDataRegs.GPADAT.bit.GPIO31=0;
		GpioDataRegs.GPBDAT.bit.GPIO41 = 0;
		for(i = 0; i < 100; i++)
			for(j = 0; j < 12000; j++);
		GpioDataRegs.GPADAT.bit.GPIO31=1;
		GpioDataRegs.GPBDAT.bit.GPIO41 = 1;
		for(i = 0; i < 100; i++)
			for(j = 0; j < 12000; j++);
	}
}


//
// 因为需要操作芯片内部的Flash，需要将部分的代码存放在芯片的RAM中
//
void CODE_SECTION("ramfuncs")
ROM_Flash_API_test1()
{

	/*   擦除第120到127扇区
	 *  HXS320F28035总共有256个Sector，每个Sector的大小是1024 Byte
	 *  Flash Addr的范围为0x700000~0x73FFFF
	 * */
	for(i = 120;i < 128;i++)
	{
		Erase_result = (* Flash28035_SectorErase)(i);
	    if(Erase_result != 0)
		{
	    	Fail();
		}
	}
	for(i = 0;i<BUFF_LEN;i++)
	{
		program_buffer[i] = 0x55AA1234 + i;
	}
	/*  写Buffer[LEN]到第120到123扇区,
	 * Flash28035_Program()写入的时候，地址需要4 Byte对齐.
	 * 如果一次性写入1个32bit数据（总共4 Byte），写满一个1024 Byte大小的Sector，需要调用256次Flash28035_Program()
	 * 120扇区的偏移地址是120 * 1024 = 122880（0x1E000）
	 *  */
	for(i = 120;i < 124;i++)
	{
		for(j = 0;j < 256;j++)
		{
			   Program_result = (* Flash28035_Program) 		\
					   ((uint32 *)(FLASH_BASE_ADDR + FLASH_SECTOR_SIZE*i + 4*j), program_buffer, 1);
		       if(Program_result != 0)
		       {
		    	  Fail();
		       }
		}
	}

	/*  写Buffer[LEN]到第124到127扇区,
	 * Flash28035_Program()写入的时候，地址需要4 Byte对齐.
	 * 如果一次性写入8个32bit数据（总共32 Byte），那么写满一个1024 Byte大小的Sector，需要调用32次Flash28035_Program()
	 * 124扇区的偏移地址是124 * 1024 = 126976（0x1F000）
	 *  */
	for(i = 124;i < 128;i++)
	{
		for(j = 0;j < SECTOR_PROG_LOOP;j++)
		{
			   Program_result = (* Flash28035_Program) 		\
					   ((uint32 *)(FLASH_BASE_ADDR + FLASH_SECTOR_SIZE*i + BUFF_BYTE_NUM*j),   		\
							   program_buffer,BUFF_LEN);
		       if(Program_result != 0)
		       {
		    	  Fail();
		       }

		       // 使用Flash28035_Verify()函数做校验
		       err_cnt = (* Flash28035_Verify) 		\
					   ((uint32 *)(FLASH_BASE_ADDR + FLASH_SECTOR_SIZE*i + BUFF_BYTE_NUM*j),   		\
							   program_buffer,BUFF_LEN);
		       if(err_cnt != 0)
		       {
		    	  Fail();
		       }
		}
	}

    Success();
}
void GPIO_init()
{
	EALLOW;
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;
	GpioDataRegs.GPASET.bit.GPIO31 = 1;

	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	EDIS;
}
int main(void)
{
	InitSysCtrl();

	GPIO_init();

    ROM_Flash_API_test1();

   while(1);
   //return 0;
}

//
// End of File
//
