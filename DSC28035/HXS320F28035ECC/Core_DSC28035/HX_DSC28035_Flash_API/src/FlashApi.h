#ifndef SRC_FLASHAPI_H_
#define SRC_FLASHAPI_H_

/*
 * Uint16 Flash28035_Verify
*		(Uint32 *FlashAddr, Uint32 *BufAddr,
*		Uint32 Length)
 * 功能：将FLASH中的数据与缓存Buf中的数据进行一致性比对。
* 入参：FlashAddr的取值范围为0x700000~0x740000。
* 入参：BufAddr是缓存的数据的首地址。
* 入参：Length是BufAddr数组的长度，32bit数据的个数。
* 返回值：0代表Flash中的数据写入成功，40代表Verify失败。
 * */
#define Flash28035_Verify 				\
		(Uint16 (*)(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length))0x7fcb56

/*
 * Uint16  Flash28035_ProgVerify
 * 		(Uint32 *FlashAddr, Uint32 Length)
 * 功能：验证FLASH编程之前是否为全F。
* 入参：FlashAddr的取值范围为0x700000~0x740000。
* 入参：Length是将要烧写的32bit数据的个数。
* 返回值：0代表FLASH编程之前的区域为全F，31代表将要烧写的FLASH区域为非全F。
* */
#define Flash28035_ProgVerify		\
		(Uint16 (*)(Uint32 *FlashAddr, Uint32 Length))0x7fcaf6

/*
 * Uint16  Flash28035_EraseVerify
 * 		(Uint32 *FlashAddr, Uint32 Length)
 * 功能：将FLASH中的数据与0xFFFFFFFF进行一致性对比，判断是否擦除成功。
* 入参：FlashAddr的取值范围为0x700000~0x740000。
* 入参：Length是将要烧写的32bit数据的个数。
* 返回值：0代表相应Page为全F。其他值代表不为0xFFFFFFFF的个数。
* */
#define Flash28035_EraseVerify		\
		(Uint16 (*)(Uint32 *FlashAddr, Uint32 Length))0x7fcd56

/*
 * Uint16  Flash28035 SectorErase(uint32 SectorIdx)
 * 功能：根据输入的SectorIdx，对FLASH相应的Sector进行擦除。
 * 入参：SectorIdx的取值范围为0~255。一个Page大小为0x400。
 * 返回值：0代表成功，10代表未解密，20代表SectorIdx的取值超限，22代表擦除失败。
 * */
#define Flash28035_SectorErase      \
    (Uint16 (*)(uint32 SectorIdx))0x7fcdcc

/*
 * Uint16  Flash28035_MassErase()
 * 功能：将FLASH空间全部擦除。
 * 返回值：0代表成功，10代表未解密，22代表擦除失败。
 * */
#define Flash28035_MassErase    \
	(Uint16 (*)())0x7fce76

/*
 *
 * Uint16  Flash28035_Program
 *     (Uint32 *FlashAddr, Uint32 *BufAddr,
 *     Uint32 Length)
* 功能：从给定的地址处开始，根据长度，将缓存中的数据，烧写到Flash中。
* 入参：FlashAddr的取值范围为0x700000~0x740000。
* 入参：BufAddr是缓存的数据的地址。
* 入参：Length是BufAddr数组的长度，32bit数据的个数。
* 返回值：0代表成功，10代表未解密，12代表FlashAddr的取值超限，30代表烧写失败。
* */
#define Flash28035_Program   \
	 (Uint16 (*)(Uint32 *FlashAddr, Uint32 *BufAddr, Uint32 Length))0x7fca00

/*
* Uint16  Flash28035_OTP_Program
*     (Uint32 *FlashAddr, Uint32 *BufAddr,
*     Uint32 Length)
* 功能：从给定的地址处开始，根据长度，将缓存中的数据，烧写到OTP中。
* 入参：FlashAddr的取值范围为0x7A0000~0x7A3000。
* 入参：BufAddr是缓存的数据的地址。
* 入参：Length是BufAddr数组的长度，32bit数据的个数。
* 返回值：0代表成功，10代表未解密，12代表FlashAddr的取值超限，30代表烧写失败。
* */
#define Flash28035_OtpProgram 		\
	(Uint16 (*)(Uint32 *OtpAddr,Uint32 *BufAddr,Uint32 Length))0x7fcbbe


#endif /* SRC_FLASHAPI_H_ */
