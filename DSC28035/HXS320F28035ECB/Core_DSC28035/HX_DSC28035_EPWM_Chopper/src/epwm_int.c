#include "epwm.h"

/******************************************************************
 *函数名：void INTERRUPT epwm1_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数改变pwm死区
 ******************************************************************/
void INTERRUPT epwm1_isr(void)
{
	epwm1_pc();
	EPwm1Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************
 *函数名：void INTERRUPT epwm2_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数改变pwm死区
 ******************************************************************/
void INTERRUPT epwm2_isr(void)
{
	epwm2_pc();

	EPwm2Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************
 *函数名：void INTERRUPT epwm3_isr(void)
 *参 数：无
 *返回值：无
 *作 用：中断服务函数改变pwm死区
 ******************************************************************/
void INTERRUPT epwm3_isr(void)
{

	EPwm3Regs.ETCLR.bit.INT = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
