#include "epwm.h"
/******************************************************************
 *函数名：void InitEpwm1_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm1
 ******************************************************************/
void InitEPwm1Example()
{
	/* Period = 4000*TBCLK counts*/
	EPwm1Regs.TBPRD = 2000;
	/*epwm时基计数器相位=0*/
	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器TBPRD（活 动）←TBPRD（影子）*/
	EPwm1Regs.TBCTR = 0x0000;

	/*设置CMPA值为EPWM1_MAX_CMPA*/
	EPwm1Regs.CMPA.half.CMPA = 1000;
	/*设置CMPB值为EPWM1_MAX_CMPB*/
	EPwm1Regs.CMPB = 0;

	/*增减计数模式*/
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPB主寄存器从映射寄存器中加载数据*/
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxA输出高电平*/
	EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
	/*当时间基准计数器的值等于CMPA的值，且正在减计数时，使EPWMxA输出低电平*/
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	/*当时间基准计数器的值等于CMPB的值，且正在增计数时，使EPWMxB输出高电平*/
	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm1Regs.AQCTLB.bit.CBD = AQ_CLEAR;

	/*选择EPWMx_INT产生的条件：TBCTR=0x0000时产生*/
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	/*使能产生中断信号EPWMx_INT*/
	EPwm1Regs.ETSEL.bit.INTEN = 1;
	/*ePWM中断（EPWMx_INT）周期选择：每发生一次事件产生中断信号EPWMx_INT*/
	EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;

	/*使能上升沿和下降沿延时信号*/
	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	/* 死区上升沿延时计数器*/
	EPwm1Regs.DBRED = 50;
	/*死区下降沿延时计数器*/
	EPwm1Regs.DBFED = 50;
}
/******************************************************************
 *函数名：void InitEpwm2_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm2
 ******************************************************************/
void InitEPwm2Example()
{
	/* Period = 4000*TBCLK counts*/
	EPwm2Regs.TBPRD = 2000;
	/*epwm时基计数器相位=0*/
	EPwm2Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））*/
	EPwm2Regs.TBCTR = 0x0000;

	/*设置CMPA值为EPWM2_MIN_CMPA*/
	EPwm2Regs.CMPA.half.CMPA = 1000;
	/*设置CMPB值为EPWM2_MAX_CMPB*/
	EPwm2Regs.CMPB = 0;

	/*增减计数模式*/
	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;


	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxA输出高电平*/
	EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
	/*当时间基准计数器的值等于CMPA的值，且正在减计数时，使EPWMxA输出低电平*/
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	/*当时间基准计数器的值等于CMPB的值，且正在增计数时，使EPWMxB输出高电平*/
	EPwm2Regs.AQCTLB.bit.CBU = AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm2Regs.AQCTLB.bit.CBD = AQ_CLEAR;

	/*时基计数器等于零时产生,ePWM 中断*/
	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	/*使能 EPWMx_INT 生成*/
	EPwm2Regs.ETSEL.bit.INTEN = 1;
	/*在 ETPS [INTCNT] = 1,1 上产生中断（第三个事件）*/
	EPwm2Regs.ETPS.bit.INTPRD = ET_1ST;

	/*使能上升沿和下降沿延时信号*/
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*互补输出:EPWMxA置高,EPWMxB置低*/
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/*EPWMxA作为上升沿和下降沿时的信号源*/
	EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	/* 死区上升沿延时计数器*/
	EPwm2Regs.DBRED = 50;
	/*死区下降沿延时计数器*/
	EPwm2Regs.DBFED = 50;
}
/******************************************************************
 *函数名：void InitEpwm3_Example(void)
 *参 数：无
 *返回值：无
 *作 用：初始化Epwm3
 ******************************************************************/
void InitEPwm3Example()
{
	/* Period = 4000*TBCLK counts*/
	EPwm3Regs.TBPRD = 2000;
	/*epwm时基计数器相位=0*/
	EPwm3Regs.TBPHS.half.TBPHS = 0x0000;
	/*当时基计数器等于零（TBCTR = 0x0000）时，影子寄存器的内容将传输到活动寄存器（TBPRD（活 动）←TBPRD（影子））*/
	EPwm3Regs.TBCTR = 0x0000;
	/*设置CMPA值为EPWM3_MIN_CMPA*/
	EPwm3Regs.CMPA.half.CMPA = 1000;
	/*设置CMPA值为EPWM3_MAX_CMPB*/
	EPwm3Regs.CMPB = 0;

	/*增减计数模式*/
	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
	/*禁止TBCTR加载相位寄存器TBPHS中的值*/
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	/*高速时钟分频 1倍分频*/
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	/*时基时钟分频 1倍分频*/
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	/*CMPA寄存器工作模式选择：映射模式*/
	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	/*CMPB寄存器工作模式选择：映射模式*/
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	/*当TBCTR=0x0000时，CMPA主寄存器从映射寄存器中加载数据*/
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

	/*时基计数器等于 PRD 时,强制 EPWMxA 输出为高电平*/
	EPwm3Regs.AQCTLA.bit.PRD = AQ_SET;
	/*当时间基准计数器的值等于CMPB的值，且正在减计数时，使EPWMxB输出低电平*/
	EPwm3Regs.AQCTLA.bit.CBD = AQ_CLEAR;
	/*时基计数器等于 PRD 时,强制 EPWMxB 输出为低电平。*/
	EPwm3Regs.AQCTLB.bit.PRD = AQ_CLEAR;
	/*当时间基准计数器的值等于CMPA的值，且正在增计数时，使EPWMxB输出高电平*/
	EPwm3Regs.AQCTLB.bit.CAU = AQ_SET;

	/*时基计数器等于零时产生,ePWM 中断*/
	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;
	/*使能 EPWMx_INT 生成*/
	EPwm3Regs.ETSEL.bit.INTEN = 1;
	/*在 ETPS [INTCNT] = 1,1 上产生中断（第三个事件）*/
	EPwm3Regs.ETPS.bit.INTPRD = ET_1ST;

	/*对于输出 EPWMxA 的上升沿延迟和输出 EPWMxB 的下降沿延迟，完全启用了死区*/
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	/*主动高互补（AHC）。 EPWMxB 被反相。*/
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	/* EPWMxA作为下降沿和上升沿延迟的信号源。*/
	EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	/*设置上升沿延迟*/
	EPwm3Regs.DBRED = 50;
	/*设置下降沿延迟*/
	EPwm3Regs.DBFED = 50;
}


