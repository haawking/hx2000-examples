/******************************************************************
 文 档 名 ：     HX_DSC28035_EPWM_DeadBand
 开 发 环 境：  Haawking IDE V2.1.4
 开 发 板：      Core_DSC28035_V1.0
                       Start_DSC28035
 D S P：          DSC28035
 作 用：
 说 明：

 ----------------------例程使用说明-----------------------------
 *            测试epwm_DeadBand功能
 * 现象：
 * PWM1A00 PWM1B01
 * PWM2A02 PWM2B03
 * PWM3A04 PWM3B05
 *   变化的死区
 *
 *
 版 本：      V1.0.0
 时 间：      2023年4月4日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "epwm.h"

void InitLED(void);

int main(void)
{
	/*初始化系统控制：PLL,WatchDog，使能外设时钟*/
	InitSysCtrl();
	/*初始化GPIO，复用为EPwm功能*/
	InitEPwm1Gpio();
	InitEPwm2Gpio();
	InitEPwm3Gpio();

	/*将PIE控制寄存器初始化为默认状态，该状态禁止所有PIE中断并清除所有标志*/
	InitPieCtrl();
	/*禁止CPU中断并清除所有中断标志*/
	IER = 0x0000;
	IFR = 0x0000;
	/*初始化PIE向量表，为PIE向量表中的所有中断向量配置对应向量的入口地址*/
	InitPieVectTable();

	EALLOW;
	//允许访问受保护的空间
	/*将epmw1_isr入口地址赋给EPWM1_INT*/
	PieVectTable.EPWM1_INT = &epwm1_isr;
	/*将epmw2_isr入口地址赋给EPWM2_INT*/
	PieVectTable.EPWM2_INT = &epwm2_isr;
	/*将epmw3_isr入口地址赋给EPWM3_INT*/
	PieVectTable.EPWM3_INT = &epwm3_isr;
	/*将epwm1_tz_isr入口地址赋给EPWM1_TZINT*/
	EDIS;
	//禁止访问受保护的空间

	/*TBCLK被禁止，此时，ePWM模块的时钟使能通过PCLKCR1寄存器进行配置*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
	EDIS;

	/*初始化EPWM1*/
	InitEpwm1_Example();
	/*初始化EPWM2*/
	InitEpwm2_Example();
	/*初始化EPWM3*/
	InitEpwm3_Example();

	/*所有使能的ePWM模块同步使用TBCLK*/
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
	/*INT2、INT3中断待定*/
	IER |= M_INT2 | M_INT3;

	/*使能相对应的中断*/
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER2.bit.INTx3 = 1;

	EINT;

	while(1)
	{
	}

	return 0;
}

// ----------------------------------------------------------------------------
