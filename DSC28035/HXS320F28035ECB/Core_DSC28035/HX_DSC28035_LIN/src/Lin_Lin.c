#include "LIN.h"

void LIN_LIN_init(void)
{
	EALLOW;
	LinaRegs.SCIGCR0.bit.RESET=1;//从复位中释放
	LinaRegs.SCIGCR1.bit.SWnRST=0;//SCI软复位,允许写入配置

	LinaRegs.SCIGCR1.bit.LINMODE=1;//配置为LIN模式

	LinaRegs.SCIGCR1.bit.CLK_MASTER=0;//节点处于从机模式
	LinaRegs.SCIGCR1.bit.ADAPT=0;//禁用自动调整波特率

	LinaRegs.SCIGCR1.bit.COMMMODE=0;//ID4和ID5不用于数据长度控制

	LinaRegs.SCIGCR1.bit.CONT=1;//进入调试模式,直到数据发送与接收完成
	LinaRegs.SCIGCR1.bit.CTYPE=1;//增强校验,使用LIN2.0标准

	LinaRegs.SCIGCR1.bit.HGENCTRL=1;//使用ID-SlaveTask字节来进行ID过滤

	LinaRegs.SCIGCR1.bit.LOOPBACK=0;//禁止自回环
	LinaRegs.SCIGCR1.bit.MBUFMODE=1;//启用多缓冲模式
	LinaRegs.SCIGCR1.bit.PARITYENA=0;//奇偶校验禁用

	LinaRegs.SCIGCR1.bit.RXENA=1;//RX接收使能
	LinaRegs.SCIGCR1.bit.TXENA=1;//TX传输使能

	LinaRegs.SCICLEARINT.all=0xFFFFFFFF;//启用所有中断

	LinaRegs.BRSR.bit.SCI_LIN_PSL=194;//波特率配置为19.2kbps
	LinaRegs.BRSR.bit.M=5;

	LinaRegs.LINCOMP.bit.SBREAK=5;//5-同步中断时间延迟，扩展到18位
	LinaRegs.LINCOMP.bit.SDEL=3;//3-同步分隔符时间延迟，有4个字节

	LinaRegs.LINMASK.bit.TXIDMASK=0xFF;//传输ID掩码配置FF
	LinaRegs.LINMASK.bit.RXIDMASK=0xFF;//接收ID掩码配置FF

	LinaRegs.SCIFORMAT.bit.LENGTH=0x7;//传输数据长度为n+1=8字节

	LinaRegs.SCIGCR1.bit.SWnRST=1;//SCI复位释放，配置生效
	EDIS;
}

void LIN_interrupt_init(void)
{
	LinaRegs.SCIGCR1.bit.SWnRST=0;//禁止软复位,开始配置

	LinaRegs.SCIGCR1.bit.PARITYENA=0;//屏蔽奇偶校验使能

	LinaRegs.SCISETINT.bit.SETIDINT=1;//ID匹配有效：一旦收到有效匹配ID，将产生中断

	LinaRegs.SCISETINTLVL.bit.SETIDINTLVL=1;//ID匹配有效映射到INT1执行

	LinaRegs.LINMASK.bit.RXIDMASK=0xff;//RX ID掩码为0xff
	LinaRegs.LINMASK.bit.TXIDMASK=0xff;;//TX ID掩码为0xff

	LinaRegs.LINID.bit.IDSLAVETASKBYTE=LINIDNUM_Slave;//传入帧头的ID标识符

	LinaRegs.SCIGCR1.bit.SWnRST=1;//SCI复位释放，配置生效

	LinaRegs.SCIFORMAT.bit.LENGTH=0x7;//传输数据长度为n+1=8字节
}
