#include "LIN.h"

void INTERRUPT LIN1_isr(void)
{
	if(LinaRegs.LINID.bit.RECEIVEDID==0x49)//接收到帧头ID49接收
	{
		LIN_receive();//接收数据
	}
	else if(LinaRegs.LINID.bit.RECEIVEDID==0xA3)//接收到帧头A3发送
	{
		tx_flag=0;
	}

	LinaRegs.SCICLEARINT.bit.CLRIDINT=1;//清除ID帧头接收中断

	PieCtrlRegs.PIEACK.all=PIEACK_GROUP9;
}
