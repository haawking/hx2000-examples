#include "LIN.h"

Uint16 i;

Uint16 wordData,byteData;

Uint16 tx_flag=1;

void LIN_receive(void)
{
	Uint32 temp;

	temp=LinaRegs.LINRD0.all;//接收数据
	temp=LinaRegs.LINRD1.all;
}

Uint16 LIN_trans(void)
{
	if(tx_flag==0)
	{
		for(i=0;i<4400;i++)//延时一定时间发送数据
		{
		}

		LinaRegs.LINTD0.all=LinaRegs.LINRD0.all;//发送缓冲区发送数据
		LinaRegs.LINTD1.all=LinaRegs.LINRD1.all;

		wordData=LinaRegs.LINTD0.all;
		byteData=LinaRegs.LINTD1.all;

		wordData|=(byteData<<8);
		tx_flag=1;
	}

	LinaRegs.SCISETINT.bit.SETIDINT=1;//使能打开ID接收中断

	return wordData;
}
