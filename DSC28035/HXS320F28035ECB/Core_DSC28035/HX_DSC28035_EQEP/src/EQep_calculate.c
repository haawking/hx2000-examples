﻿#include "system.h"

_iq PulseNum;
Uint16 PulseDir;
_iq CapNum;
float MotorSpeed;
_iq MotorDir;
float MotorAngle;

void MotorSpeed_init(void)
{
	/*设置电机转速*/
	SetSpeed = 3000;
	/*设置占空比为50%,模拟正交编码脉冲*/
	Pwm1CMPA = SysFreq * 3750 / SetSpeed;
}

void EQEP_PulseCap_cal(void)
{
	PulseNum=EQep1Regs.QPOSLAT;
	PulseDir=EQep1Regs.QEPSTS.bit.QDF;
}

void EQEP_MSpeed_cal(void)
{
	PulseNum=EQep1Regs.QPOSLAT;
	PulseDir=EQep1Regs.QEPSTS.bit.QDF;

	if(PulseDir==1)
	{
		MotorDir=CW;
	}
	else
	{
		MotorDir=CCW;
		PulseNum=0xFFFFFFFF-PulseNum;
	}

	MotorSpeed=MotorDir*PulseNum*5*60/4;
}

void EQEP_TSpeed_Angle_cal(void)
{
	PulseNum=EQep1Regs.QPOSLAT;
	PulseDir=EQep1Regs.QEPSTS.bit.QDF;
	CapNum=EQep1Regs.QCPRDLAT;

	if(PulseDir==1)
	{
		MotorDir=CW;
	}
	else
	{
		MotorDir=CCW;
		PulseNum=4000-PulseNum;
	}

	MotorSpeed=MotorDir*60*5*EQep1Regs.QUPRD/(4*CapNum);
	MotorAngle=PulseNum*360/4000;
}
