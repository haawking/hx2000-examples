#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "dsc_config.h"
#include "IQmathLib.h"

/*EPwm_motor*/
void EPWM1_Config(void);

#define EPWM1_TBPRD SysFreq*7500/SetSpeed
extern Uint16 Pwm1CMPA;

/*EQep_PulseCap */
void EQEP_pulseCap(void);
void EQEP_MSpeed(void);
void EQEP_TSpeed_Angle(void);

void EQEP_PulseCap_cal();
void EQEP_MSpeed_cal(void);
void EQEP_TSpeed_Angle_cal(void);
void InitLED(void);

void InitEQep1Gpio(void);

#define SysFreq 120//MHz

/*EQep*/
extern _iq PulseNum;
extern Uint16 PulseDir;
extern _iq CapNum;
extern float MotorSpeed;
extern _iq MotorDir;
extern float MotorAngle;
extern float SetSpeed;

void MotorSpeed_init(void);

#define _FLASH      1
#define CW     1
#define CCW		 -1

/*xint_isr  M QEP switch to T QEP*/
void INTERRUPT timer0_ISR(void);
extern Uint32 msCounter;
void Timer0_init(void);

#endif/*SYSTEM_H_*/
