/*******************************************************************
 文 档 名：     HX_DSC28035_ADC_SIMUL
 开 发 环 境：  Haawking IDE V2.1.4
 开 发 板：     Core_DSC28035_
                       Start_DSC28035_
 D S P：       DSC28035
 使 用 库：
 作     用：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，ADC时钟15MHz, 通过配置ADC，PWM1的寄存器，实现ADC采集，获取被测电压对应数字量

 连接方式：
 	 	 	 	 Start板上电位器RG端可模拟被测电压

 	 	 	 	 把被测电压和ADCINA0/A1/A2/A3/A4/A5/A6/A7连接，
 	 	 	 	 可从adcValA[0]/A[1]/A[2]/A[3]/A[4]/A[5]/A[6]/A[7]
 	 	 	 	 ADCINB0/B1/B2/B3/B4/B5/B6/B7连接，
 	 	 	 	 可从adcValB[0]/B[1]/B[2]/B[3]/B[4]/B[5]/B[6]/B[7]


 现象：在Haawking IDE调试界面上可正确读出电压对应的数字量值adcValA adcValB
			转换结果为Vi=X/4096*3.3V。

 版 本：V1.0.0
 时 间：2023年4月4日
 作 者：
 ******************************************************************/
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

extern void pwmInit();
extern void adcInit();
extern void gpioInit();
extern void adcIsr();

uint16_t adcCount = 0;
Uint32 adcValA[8];
Uint32 adcValB[8];
uint16_t i = 0,j = 0;
uint16_t error = 0;

int main(void)
{
    InitSysCtrl();  //Initializes the System Control registers to a known state.

    DINT;

    gpioInit();
    pwmInit();
    adcInit();

    EALLOW;
	PieVectTable.ADCINT1 = &adcIsr;
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;
	IER |= M_INT1;
	EINT;
	EDIS;

    while(1)
    {

    }

	return 0;
}

__interrupt void adcIsr()
{


		adcValA[0] = AdcResult.ADCRESULT0;
		adcValA[1] = AdcResult.ADCRESULT2;
		adcValA[2] = AdcResult.ADCRESULT4;
		adcValA[3] = AdcResult.ADCRESULT6;
		adcValA[4] = AdcResult.ADCRESULT8;
		adcValA[5] = AdcResult.ADCRESULT10;
		adcValA[6] = AdcResult.ADCRESULT12;
		adcValA[7] = AdcResult.ADCRESULT14;

		adcValB[0] = AdcResult.ADCRESULT1;
		adcValB[1] = AdcResult.ADCRESULT3;
		adcValB[2] = AdcResult.ADCRESULT5;
		adcValB[3] = AdcResult.ADCRESULT7;
		adcValB[4] = AdcResult.ADCRESULT9;
		adcValB[5] = AdcResult.ADCRESULT11;
		adcValB[6] = AdcResult.ADCRESULT13;
		adcValB[7] = AdcResult.ADCRESULT15;


	AdcRegs.ADCINTFLGCLR.all = 1;
	AdcRegs.ADCINTOVFCLR.all = 1;
	PieCtrlRegs.PIEACK.bit.ACK1 = 1;
}

void pwmInit()
{
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;

	EPwm1Regs.TBPRD = 12000;               	//200k
	EPwm1Regs.TBPHS.half.TBPHS = 0;
	EPwm1Regs.TBCTR = 0;
	EPwm1Regs.TBCTL.bit.FREE_SOFT = 3;
	EPwm1Regs.TBCTL.bit.CLKDIV = 0;
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm1Regs.TBCTL.bit.CTRMODE = 0;    			// up count

	EPwm1Regs.CMPA.half.CMPA = 6000;
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0; // CMPA shadow
	EPwm1Regs.CMPCTL.bit.LOADAMODE = 1; // Load on CTR = Prd

	EPwm1Regs.AQCTLA.bit.ZRO = 2;       //set
	EPwm1Regs.AQCTLA.bit.CAU = 1;       //clear

	EPwm1Regs.ETSEL.bit.SOCAEN = 1;
	EPwm1Regs.ETSEL.bit.SOCASEL = 1;
	EPwm1Regs.ETPS.bit.SOCACNT = 1;
	EPwm1Regs.ETPS.bit.SOCAPRD = 1;
	EPwm1Regs.ETCLR.bit.SOCA = 1;

	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;
}

void adcInit()
{
	InitAdc();
	InitAdcAio();
	EALLOW;

	AdcRegs.ADCCTL1.bit.INTPULSEPOS=1;
	//AdcRegs.ADCCTL1.bit.TEMPCONV=0;

	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 1;    // System clock is 120M , /4

	AdcRegs.INTSEL1N2.bit.INT1CONT = 1;
	AdcRegs.INTSEL1N2.bit.INT1SEL = 2;		//EOC6 TRIG
	AdcRegs.INTSEL1N2.bit.INT1E = 1;

	AdcRegs.ADCINTOVFCLR.all = 1;
	AdcRegs.ADCINTFLGCLR.all = 1;

		/*SOC0与SOC1同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN0=1;
		/*SOC2与SOC3同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN2=1;
		/*SOC4与SOC5同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN4=1;
		/*SOC6与SOC7同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN6=1;
		/*SOC8与SOC9同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN8=1;
		/*SOC10与SOC11同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN10=1;
		/*SOC12与SOC13同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN12=1;
		/*SOC14与SOC15同时采样*/
		AdcRegs.ADCSAMPLEMODE.bit.SIMULEN14=1;


		/*将SOC0设置为样本A0/B0,结果分别存储在RESULT0/RESULT1中*/
		AdcRegs.ADCSOC0CTL.bit.CHSEL = 0;
		/*将SOC2设置为样本A1/B1,结果分别存储在RESULT2/RESULT3中*/
		AdcRegs.ADCSOC2CTL.bit.CHSEL = 1;
		/*将SOC4设置为样本A2/B2,结果分别存储在RESULT4/RESULT5中*/
		AdcRegs.ADCSOC4CTL.bit.CHSEL = 2;
		/*将SOC6设置为样本A3/B3,结果分别存储在RESULT6/RESULT7中*/
		AdcRegs.ADCSOC6CTL.bit.CHSEL = 3;
		/*将SOC8设置为样本A4/B4,结果分别存储在RESULT8/RESULT9中*/
		AdcRegs.ADCSOC8CTL.bit.CHSEL = 4;
		/*将SOC10设置为样本A5/B5,结果分别存储在RESULT10/RESULT11中*/
		AdcRegs.ADCSOC10CTL.bit.CHSEL = 5;
		/*将SOC12设置为样本A6/B6,结果分别存储在RESULT12/RESULT13中*/
		AdcRegs.ADCSOC12CTL.bit.CHSEL = 6;
		/*将SOC14设置为样本A7/B7,结果分别存储在RESULT14/RESULT15中*/
		AdcRegs.ADCSOC14CTL.bit.CHSEL = 7;

	/*SOCx触发源选择:ADCTRIG5-ePWM1，ADCSOCA*/
		AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC4CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC6CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC8CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC10CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC12CTL.bit.TRIGSEL = 0x05;
		AdcRegs.ADCSOC14CTL.bit.TRIGSEL = 0x05;

//		/*采样窗口长达6个周期（2n+6个时钟周期）*/
//		AdcRegs.ADCSOC0CTL.bit.ACQPS = 0;
//		AdcRegs.ADCSOC2CTL.bit.ACQPS = 0;
//		AdcRegs.ADCSOC4CTL.bit.ACQPS = 0;
//		AdcRegs.ADCSOC6CTL.bit.ACQPS = 0;
//		AdcRegs.ADCSOC8CTL.bit.ACQPS = 0;
//		AdcRegs.ADCSOC10CTL.bit.ACQPS= 0;
//		AdcRegs.ADCSOC12CTL.bit.ACQPS =0;
//		AdcRegs.ADCSOC14CTL.bit.ACQPS =0;

	EDIS;
}

void gpioInit()
{
	EALLOW;
	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0; 	//D400 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0; 	//D401 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;
	GpioDataRegs.GPBSET.bit.GPIO34 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; 	//D402 LED
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	EDIS;
}
// ----------------------------------------------------------------------------
