#include "timer.h"

timer0 timer0Base;
timer1 timer1Base;

INTERRUPT void cpu_timer0_isr(void);
INTERRUPT void cpu_timer1_isr(void);

/******************************************************************
 函数名：Timer_init()
 参	数：无
 返回值：无
 作	用：定时器0初始化-1ms 周期
 说	明：
 ******************************************************************/
void Timer_init()
{
	InitCpuTimers();

	//中断配置步骤-----1,开启模块中断使能，位于 Timer->RegsAddr->TCR.bit.TIE = 1;
	ConfigCpuTimer(&CpuTimer0, 120, 100);  //120MHz,1000us ，即为 1ms中断周期
	CpuTimer0Regs.TCR.bit.TIE = 1;  //使能中断
	CpuTimer0Regs.TCR.bit.TSS = 0;  // 打开定时器

    ConfigCpuTimer(&CpuTimer1, 120, 500000); /*定时器1初始化，第二个参数为CPU SYSCLK = 120M， 第三个参数为设定的周期，us单位*/
    CpuTimer1Regs.TCR.bit.TSS = 0; /*打开定时器*/
    CpuTimer1Regs.TCR.bit.TIE = 1;  //使能中断

	//中断配置步骤-----2，重映射中断服务函数
	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	PieVectTable.TINT1 = &cpu_timer1_isr;
	EDIS;
	//中断配置步骤-----3，连接CPU中断第1组 13组
	IER |= M_INT1 | M_INT13;
	//中断配置步骤-----4，连接中断里的第几位
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
}
/******************************************************************
 函数名：cpu_timer0_isr()
 参	数：无
 返回值：无
 作	用：
 说	明：
 ******************************************************************/
//CPU 定时器0 中断服务函数
INTERRUPT void cpu_timer0_isr(void)
{
	timer0Base.msCounter++;
	timer0Base.Mark_Para.Status_Bits.OnemsdFlag = 1;

	// Acknowledge this interrupt to receive more interrupts from group 1
	EALLOW;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
	EDIS;
}

/******************************************************************
 函数名：cpu_timer1_isr()
 参	数：无
 返回值：无
 作	用：
 说	明：
 ******************************************************************/
//CPU 定时器1 中断服务函数
INTERRUPT void cpu_timer1_isr(void)
{
	timer1Base.msCounter++;
	timer1Base.Mark_Para.Status_Bits.OnemsdFlag = 1;

	GpioDataRegs.GPATOGGLE.bit.GPIO31 = 1;   /*Core_LED1翻转*/

	GpioDataRegs.GPBTOGGLE.bit.GPIO44 = 1;   /*LED1翻转*/

	GpioDataRegs.GPBTOGGLE.bit.GPIO43 = 1;   /*D402翻转*/
}

