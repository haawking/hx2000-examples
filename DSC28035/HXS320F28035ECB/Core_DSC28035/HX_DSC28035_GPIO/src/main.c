/******************************************************************
 文 档 名：      HX_DSC28035_GPIO
 开 发 环 境：  Haawking IDE V2.1.4
 开 发 板：      Core_DSC28035_V1.0
 D S P：          DSC28035
 使 用 库：
 说     明：      FLASH工程
 -------------------------- 例程使用说明 --------------------------
 功能描述：   芯片主频120MHz

 连接方式：   LED1 ----- GPIO31
                      KEY  ----- GPIO30

 现象：         当检测到KEY按下时，LED1常亮

 版 本：      V1.0.0
 时 间：      2023年4月4日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/

#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
/******************************************************************
 函数名：void  InitKEY(void)
 参	数：无
 返回值：无
 作	用：配置GPIO30为输入模式，上拉禁止
 ******************************************************************/

void InitKEY(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO30 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 1;
	EDIS;
}

/******************************************************************
 函数名：void  InitLED(void)
 参	数：无
 返回值：无
 作	用：配置GPIO31为输出模式
 ******************************************************************/
void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0; /* 普通IO,LED1灯 */
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1; /* 输出IO */
	EDIS;

}

int main(void)
{


	InitSysCtrl();/*将PLL配置成10倍频1分频，配置系统时钟为120M */

	InitKEY();

	InitLED();

	while (1)
	{

		if (GpioDataRegs.GPADAT.bit.GPIO30 == 0)  //按下KEY

		{
			GpioDataRegs.GPACLEAR.bit.GPIO31 = 1;  //亮

		}
		else
		{
			GpioDataRegs.GPASET.bit.GPIO31 = 1;  //灭
		}

	}

	return 0;
}


//  ----------------------------------------------------------------------------
