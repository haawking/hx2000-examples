//###########################################################################
//
// FILE:    DSP2803x_OTP.c
//
// TITLE:   DSP2803x OTP file.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2022-10-18 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################


#include "DSP2803x_Device.h"     //  Headerfile Include File


/***************************************************************
  To  uncomment ,you can use OTP
*****************************************************************/

/***************************************************************

volatile Uint32   CODE_SECTION(".User_Otp_Table") OTP_DATA[] = 
{
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
	0xFFFFFFFF,
};


*****************************************************************/
