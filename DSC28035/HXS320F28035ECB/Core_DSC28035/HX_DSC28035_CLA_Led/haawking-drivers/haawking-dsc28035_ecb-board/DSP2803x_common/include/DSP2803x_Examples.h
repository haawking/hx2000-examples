//###########################################################################
//
// FILE:   DSP2803x_Examples.h
//
// TITLE:  DSP2803x Device Definitions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2022-10-18 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################
#ifndef DSP2803x_EXAMPLES_H
#define DSP2803x_EXAMPLES_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
      Specify the PLL control register (PLLCR) and divide select (DIVSEL) value.
-----------------------------------------------------------------------------*/

//#define DSP28_DIVSEL   0 // Enable /4 for SYSCLKOUT
//#define DSP28_DIVSEL   1 // Disable /4 for SYSCKOUT
//#define DSP28_DIVSEL   2 // Enable /2 for SYSCLKOUT
#define DSP28_DIVSEL   3 // Enable /1 for SYSCLKOUT

//#define DSP28_PLLCR_BYPASS     0
#define  DSP28_PLLCR_BYPASS     1

// F(CLKOUT) = F(REFCLK)*(DSP28_PLLCR_F+15)/(DSP28_PLLCR_N+1)/(DSP28_PLLCR_K+1)
//#define DSP28_PLLCR_F   63    //78
//#define DSP28_PLLCR_F   62    //77
//#define DSP28_PLLCR_F   61    //76
//#define DSP28_PLLCR_F   60    //75
//#define DSP28_PLLCR_F   59    //74
//#define DSP28_PLLCR_F   58    //73
//#define DSP28_PLLCR_F   57    //72
//#define DSP28_PLLCR_F   56    //71
//#define DSP28_PLLCR_F   55    //70
//#define DSP28_PLLCR_F   54    //69
//#define DSP28_PLLCR_F   53    //68
//#define DSP28_PLLCR_F   52    //67
//#define DSP28_PLLCR_F   51    //66
//#define DSP28_PLLCR_F   50    //65
//#define DSP28_PLLCR_F   49    //64
//#define DSP28_PLLCR_F   48    //63
//#define DSP28_PLLCR_F   47    //62
//#define DSP28_PLLCR_F   46    //61
//#define DSP28_PLLCR_F   45    //60
//#define DSP28_PLLCR_F   44    //59
//#define DSP28_PLLCR_F   43    //58
//#define DSP28_PLLCR_F   42    //57
//#define DSP28_PLLCR_F   41    //56
//#define DSP28_PLLCR_F   40    //55
//#define DSP28_PLLCR_F   39    //54
//#define DSP28_PLLCR_F   38    //53
//#define DSP28_PLLCR_F   37    //52
//#define DSP28_PLLCR_F   36    //51
//#define DSP28_PLLCR_F   35    //50
//#define DSP28_PLLCR_F   34    //49
//#define DSP28_PLLCR_F   33    //48
//#define DSP28_PLLCR_F   32    //47
//#define DSP28_PLLCR_F   31    //46
//#define DSP28_PLLCR_F   30    //45
//#define DSP28_PLLCR_F   29    //44
//#define DSP28_PLLCR_F   28    //43
//#define DSP28_PLLCR_F   27    //42
//#define DSP28_PLLCR_F   26    //41
#define DSP28_PLLCR_F   25     //40 Uncomment for 120 MHz devices [120 MHz = (12MHz/2 * 40)/2]
//#define DSP28_PLLCR_F   24    //39
//#define DSP28_PLLCR_F   23    //38
//#define DSP28_PLLCR_F   22    //37
//#define DSP28_PLLCR_F   21    //36
//#define DSP28_PLLCR_F   20    //35
//#define DSP28_PLLCR_F   19    //34
//#define DSP28_PLLCR_F   18    //33
//#define DSP28_PLLCR_F   17    //32
//#define DSP28_PLLCR_F   16    //31
//#define DSP28_PLLCR_F   15    //30
//#define DSP28_PLLCR_F   14    //29
//#define DSP28_PLLCR_F   13    //28
//#define DSP28_PLLCR_F   12    //27
//#define DSP28_PLLCR_F   11    //26
//#define DSP28_PLLCR_F   10    //25
//#define DSP28_PLLCR_F   9     //24
//#define DSP28_PLLCR_F   8     //23
//#define DSP28_PLLCR_F    7    //22
//#define DSP28_PLLCR_F    6    //21
//#define DSP28_PLLCR_F    5    //20
//#define DSP28_PLLCR_F    4    //19
//#define DSP28_PLLCR_F    3    //18
//#define DSP28_PLLCR_F    2    //17
//#define DSP28_PLLCR_F    1    //16
//#define DSP28_PLLCR_F    0    //15



//#define DSP28_PLLCR_N   15   //16
//#define DSP28_PLLCR_N   14   //15
//#define DSP28_PLLCR_N   13   //14
//#define DSP28_PLLCR_N   12   //13
//#define DSP28_PLLCR_N   11   //12
//#define DSP28_PLLCR_N   10   //11
//#define DSP28_PLLCR_N   9    //10
//#define DSP28_PLLCR_N   8    //9
//#define DSP28_PLLCR_N   7    //8
//#define DSP28_PLLCR_N   6    //7
//#define DSP28_PLLCR_N   5    //6
//#define DSP28_PLLCR_N   4    //5
//#define DSP28_PLLCR_N   3    //4
//#define DSP28_PLLCR_N   2    //3
#define DSP28_PLLCR_N   1      //2   Uncomment for 120 MHz devices [120 MHz = (12MHz/2 * 40)/2]
//#define DSP28_PLLCR_N   0    //1



//#define DSP28_PLLCR_K   15   //16
//#define DSP28_PLLCR_K   14   //15
//#define DSP28_PLLCR_K   13   //14
//#define DSP28_PLLCR_K   12   //13
//#define DSP28_PLLCR_K   11   //12
//#define DSP28_PLLCR_K   10   //11
//#define DSP28_PLLCR_K   9    //10
//#define DSP28_PLLCR_K   8    //9
//#define DSP28_PLLCR_K   7    //8
//#define DSP28_PLLCR_K   6    //7
//#define DSP28_PLLCR_K   5    //6
//#define DSP28_PLLCR_K   4    //5
//#define DSP28_PLLCR_K   3    //4
//#define DSP28_PLLCR_K   2    //3
#define DSP28_PLLCR_K   1      //2   Uncomment for 120 MHz devices [120 MHz = (12MHz/2 * 40)/2]
//#define DSP28_PLLCR_K   0    //1
//----------------------------------------------------------------------------

/*-----------------------------------------------------------------------------
      Specify the clock rate of the CPU (SYSCLKOUT) in nS.

      Take into account the input clock frequency and the PLL multiplier
      selected in step 1.

      Use one of the values provided, or define your own.
      The trailing L is required tells the compiler to treat
      the number as a 64-bit value.

      Only one statement should be uncommented.
      Example 1: 120 MHz devices:
                 CLKIN is a 12 MHz crystal or internal 12 MHz oscillator

                 In step 1 the user specified PLLCR = 10 DIVSEL=3 for a
                 120 MHz CPU clock (SYSCLKOUT = 120 MHz).

                 In this case, the CPU_RATE will be 8.333L
                 Uncomment the line: #define CPU_RATE 8.333L

      Example 2: 96 MHz devices:
                 CLKIN is a 12 MHz crystal or internal 12 MHz oscillator

                 In step 1 the user specified PLLCR = 8 for a
                 96 MHz CPU clock (SYSCLKOUT = 96 MHz).

                 In this case, the CPU_RATE will be 10.417L
                 Uncomment the line: #define CPU_RATE 10.417L


      Example 3: 60 MHz devices:
                 CLKIN is a 12 MHz crystal or internal 12 MHz oscillator

                 In step 1 the user specified PLLCR = 10 DIVSEL=2 for a
                 60 MHz CPU clock (SYSCLKOUT = 60 MHz).

                 In this case, the CPU_RATE will be 16.667L
                 Uncomment the line: #define CPU_RATE 16.667L

-----------------------------------------------------------------------------*/
#define CPU_RATE   8.333L   // for a 120MHz CPU clock speed (SYSCLKOUT) 
//#define CPU_RATE   10.417L   // for a 96MHz CPU clock speed (SYSCLKOUT)   
//#define CPU_RATE   16.667L   // for a 60MHz CPU clock speed (SYSCLKOUT) 
//#define CPU_RATE   20.000L   // for a 50MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   25.000L   // for a 40MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   33.333L   // for a 30MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   41.667L   // for a 24MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   50.000L   // for a 20MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   66.667L   // for a 15MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   83.33L   // for a 12MHz CPU clock speed (SYSCLKOUT) 
//#define CPU_RATE  100.000L   // for a 10MHz CPU clock speed  (SYSCLKOUT)


#define CPU_FRQ_120MHZ    1     // 120 Mhz CPU Freq (12 MHz input clock)
#define CPU_FRQ_96MHZ    0


//
// Device_cal function which is available in Bootrom memory
// This function is called in SysCtl_resetPeripheral after resetting
// analog peripherals
//
#define configOscTrim ((void (*)(void))((uintptr_t)0x7fbe70))
#define configLdoTrim ((void (*)(void))((uintptr_t)0x7fbf04))
#define configBgrTrim ((void (*)(void))((uintptr_t)0x7fbf46))

// The following pointer to a function call internal oscillators
static inline void
Device_cal(void)
{
    (*  configOscTrim)();
    (*  configLdoTrim)();
    (*  configBgrTrim)();

}

//---------------------------------------------------------------------------
// Include Example Header Files:
//

#include "DSP2803x_GlobalPrototypes.h"         // Prototypes for global functions within the
#include "DSP2803x_EPwm_defines.h"              // Macros used for PWM examples.
#include "DSP2803x_I2c_defines.h"                  // Macros used for I2C examples.
#include "DSP2803x_Dma_defines.h"               // Macros used for DMA examples.
#include "DSP2803x_Cla_defines.h"       // Macros used for CLA examples.

#ifndef DSP28_BIOS

#include "DSP2803x_DefaultISR.h"

#endif



#define   PARTNO_28035PAG   0xBE
#define   PARTNO_28035PN    0xBF





extern void _DSP2803x_usDelay(uint32 data);

// DO NOT MODIFY THIS LINE.
#define DELAY_US(A)  _DSP2803x_usDelay((((long double)A * 1000.0L) / (long double)CPU_RATE) - 15.0L) // range A: 1 ~ 34000


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif 

//===========================================================================
// End of file.
//===========================================================================
