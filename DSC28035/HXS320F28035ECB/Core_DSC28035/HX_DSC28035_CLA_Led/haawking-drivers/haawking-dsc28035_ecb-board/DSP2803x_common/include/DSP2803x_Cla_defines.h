//###########################################################################
//
// FILE:   DSP2803x_Cla_defines.h
//
// TITLE:  #defines used in CLA examples
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2022-10-18 05:22:24 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef DSP2803X_CLA_DEFINES_H
#define DSP2803X_CLA_DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif



//
// MCTL Register
//
#define CLA_FORCE_RESET           0x1

//
// MIER Interrupt Enable Register
//
#define CLA_INT_ENABLE            0x1
#define CLA_INT_DISABLE           0x0


//
// Peripheral Interrupt Source Select define for DMAnCLASourceSelect Register
//
#define CLA_TRIG_NOPERPH     0
#define CLA_TRIG_ADCAINT1    1
#define CLA_TRIG_ADCAINT2    2
#define CLA_TRIG_ADCAINT3    3
#define CLA_TRIG_ADCAINT4    4
#define CLA_TRIG_ADCAINT5    5
#define CLA_TRIG_ADCAINT6    6
#define CLA_TRIG_ADCAINT7    7
#define CLA_TRIG_ADCAINT8    8
#define CLA_TRIG_ADCAINT9    9

#define CLA_TRIG_XINT1      29
#define CLA_TRIG_XINT2      30
#define CLA_TRIG_XINT3      31

#define CLA_TRIG_EPWM1INT   36
#define CLA_TRIG_EPWM2INT   37
#define CLA_TRIG_EPWM3INT   38
#define CLA_TRIG_EPWM4INT   39
#define CLA_TRIG_EPWM5INT   40
#define CLA_TRIG_EPWM6INT   41
#define CLA_TRIG_EPWM7INT   42
#define CLA_TRIG_EPWM8INT   43

#define CLA_TRIG_TINT0      68
#define CLA_TRIG_TINT1      69
#define CLA_TRIG_TINT2      70

#define CLA_TRIG_ECAP1INT   75

#define CLA_TRIG_EQEP1INT   83
#define CLA_TRIG_EQEP2INT   84

#define CLA_TRIG_ECAP3INT2  89

#define CLA_TRIG_SPITXINTA  109
#define CLA_TRIG_SPIRXINTA  110
#define CLA_TRIG_SPITXINTB  111
#define CLA_TRIG_SPIRXINTB  112

#define CLA_TRIG_LINA_INT1  117
#define CLA_TRIG_LINA_INT0  118


//
// Useful CLA Operation Macros:
//
static inline void Cla1ForceTask1andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT1 =1;
    EDIS;
	NOP;
    NOP;
    NOP;
    while(Cla1Regs.MIRUN.bit.INT1 == 1);
}

static inline void Cla1ForceTask2andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT2 =1;
    EDIS;
    NOP;
    NOP;
    NOP;
    while(Cla1Regs.MIRUN.bit.INT2 == 1);
}


static inline void Cla1ForceTask3andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT3 =1;
    EDIS;
	NOP;
    NOP;
    NOP;
    while(Cla1Regs.MIRUN.bit.INT3 == 1);
}

static inline void Cla1ForceTask4andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT4 =1;
    EDIS;
	NOP;
    NOP;
    NOP;
    while(Cla1Regs.MIRUN.bit.INT4 == 1);
}


static inline void Cla1ForceTask5andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT5 =1;
    EDIS;
	NOP;
    NOP;
    NOP;
    while(Cla1Regs.MIRUN.bit.INT5 == 1);
}

static inline void Cla1ForceTask6andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT6 =1;
    EDIS;
	NOP;
	NOP;
	NOP;
    while(Cla1Regs.MIRUN.bit.INT6 == 1);
}

static inline void Cla1ForceTask7andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT7 =1;
    EDIS;
	NOP;
	NOP;
	NOP;
    while(Cla1Regs.MIRUN.bit.INT7 == 1);
}

static inline void Cla1ForceTask8andWait()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT8 =1;
    EDIS;
	NOP;
	NOP;
	NOP;
    while(Cla1Regs.MIRUN.bit.INT8 == 1);
}


static inline void Cla1ForceTask1()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT1 =1;
    EDIS;
}
static inline void Cla1ForceTask2()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT2 =1;
    EDIS;
}
static inline void Cla1ForceTask3()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT3 =1;
    EDIS;
}
static inline void Cla1ForceTask4()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT4 =1;
    EDIS;
}
static inline void Cla1ForceTask5()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT5 =1;
    EDIS;
}
static inline void Cla1ForceTask6()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT6 =1;
    EDIS;
}
static inline void Cla1ForceTask7()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT7 =1;
    EDIS;
}
static inline void Cla1ForceTask8()
{
    EALLOW;
    Cla1Regs.MIFRC.bit.INT8 =1;
    EDIS;
}



#ifdef __cplusplus
}
#endif /* extern "C" */

#endif   // - end of DSP2803x_CLA_DEFINES_H

//
// End of file
//

