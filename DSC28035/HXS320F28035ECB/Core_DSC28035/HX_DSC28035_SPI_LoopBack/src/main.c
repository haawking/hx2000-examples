/******************************************************************
文 档 名：     HX_DSC28035_SPI_LoopBack
开 发 环 境：Haawking IDE V2.1.4
开 发 板：     Core_DSC28035_V1.0
                     Start_DSC28035

D S P：         DSC28035
使 用 库：
作     用：      通过配置SCI模块寄存器，使能串口中断模式下的发送和接收
说     明：      FLASH工程
-------------------------- 例程使用说明 --------------------------
功能描述：芯片主频120MHz，通过配置SCIA的寄存器，实现SCIA模块的发送和接收功能


连接方式：通过CH340串口线连接电脑端的USB口

现象：LED1亮通过
 版 本：V1.0.0
 时 间：2023年4月4日
 作 者：
 @ mail：support@mail.haawking.com
 ******************************************************************/
 
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"

#include "DSP28x_Project.h"

//
// Function Prototypes
//
void spi_xmit(Uint16 a);
void spi_fifo_init(void);
void spi_init(void);
void error(void);
void pass(void);
void InitLED(void)
{
	EALLOW;

	GpioCtrlRegs.GPAMUX2.bit.GPIO31=0;
	GpioCtrlRegs.GPADIR.bit.GPIO31=1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO34=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO34=1;
    GpioCtrlRegs.GPBMUX1.bit.GPIO41=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO41=1;
	GpioCtrlRegs.GPBMUX1.bit.GPIO43=0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43=1;

	GpioDataRegs.GPBSET.bit.GPIO34 = 1;
	GpioDataRegs.GPBSET.bit.GPIO43 = 1;
	GpioDataRegs.GPBSET.bit.GPIO41 = 1;
	EDIS;
}
//void Case_Start()
//{
//	//write_reg32(MB_ADDR,0x5A5A0001);
//}
//Uint16 i;

//
// Main
//
int main(void)
{

    Uint16 sdata;           // send data
    Uint16 rdata;           // received data

    //
    // Step 1. Initialize System Control:
    // PLL, WatchDog, enable Peripheral Clocks
    // This example function is found in the DSP2803x_SysCtrl.c file.
    //
    InitSysCtrl();
    InitLED();
    //
    // Step 2. Initialize GPIO:
    // This example function is found in the DSP2803x_Gpio.c file and
    // illustrates how to set the GPIO to it's default state.
    //
    // InitGpio();  // Skipped for this example

    //
    // Setup only the GP I/O only for SPI-A functionality
    // This function is found in DSP2803x_Spi.c
    //
    InitSpiaGpio();

    //
    // Step 3. Clear all interrupts and initialize PIE vector table:
    // Disable CPU interrupts
    //
    DINT;

    //
    // Initialize PIE control registers to their default state.
    // The default state is all PIE interrupts disabled and flags
    // are cleared.
    // This function is found in the DSP2803x_PieCtrl.c file.
    //
    InitPieCtrl();

    //
    // Disable CPU interrupts and clear all CPU interrupt flags:
    //
    IER = 0x0000;
    IFR = 0x0000;

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    //

    //
    // Service Routines (ISR).
    // This will populate the entire table, even if the interrupt
    // is not used in this example.  This is useful for debug purposes.
    // The shell ISR routines are found in DSP2803x_DefaultIsr.c.
    // This function is found in DSP2803x_PieVect.c.
    //
    InitPieVectTable();

    //
    // Step 4. Initialize all the Device Peripherals:
    //
    spi_fifo_init();	        // Initialize the Spi FIFO
    spi_init();		            // init SPI

    //
    // Step 5. User specific code:
    // Interrupts are not used in this example.
    //

    sdata = 0x0000;

        //
        // Transmit data
        //
        spi_xmit(sdata);

        //
        // Wait until data is received
        //
        while(SpiaRegs.SPIFFRX.bit.RXFFST !=1)
        {

        }

        //
        // Check against sent data
        //
        rdata = SpiaRegs.SPIRXBUF;

        if(rdata != sdata)
        {
        	error();

        }
        pass();
}

//
// Step 7. Insert all local Interrupt Service Routines (ISRs)
// and functions here:
//

//
// error - Error Checking Function
//
void error(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO34=1;


    for (;;);
}

void pass(void)
{

	GpioDataRegs.GPACLEAR.bit.GPIO31=1;

	for (;;);
}

//
// spi_init - SPI Intialization
//
void
spi_init()
{
    SpiaRegs.SPICCR.all =0x000F;    // Reset on, rising edge, 16-bit char bits

    //
    // Enable master mode, normal phase, enable talk, and SPI int disabled.
    //
    SpiaRegs.SPICTL.all =0x0006;

    SpiaRegs.SPIBRR =0x007F;
    SpiaRegs.SPICCR.all =0x009F;   // Relinquish SPI from Reset
    SpiaRegs.SPIPRI.bit.FREE = 1;  // Set so breakpoints don't disturb xmission
}

//
// spi_xmit -
//
void
spi_xmit(Uint16 a)
{
    SpiaRegs.SPITXBUF=a;
}

//
// spi_fifo_init - Initialize SPI FIFO registers
//
void
spi_fifo_init()
{
    SpiaRegs.SPIFFTX.all=0xE040;
    SpiaRegs.SPIFFRX.all=0x2044;
    SpiaRegs.SPIFFCT.all=0x0;
}
//
// End of File
//

