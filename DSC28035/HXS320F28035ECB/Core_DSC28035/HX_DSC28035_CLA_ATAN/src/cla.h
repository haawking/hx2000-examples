/******************************************************************
 文 档 名：     cla.h
 D S P：       DSC28035
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年3月28日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

#ifndef _CLA_H_
#define _CLA_H_

//
// Included Files
//
#include "DSP28x_Project.h"
#include "DSP2803x_Cla_defines.h"

//
// Defines
//
#define BUFFER_SIZE       64
#define TABLE_SIZE        64
#define TABLE_SIZE_M_1    TABLE_SIZE-1
#define INV2PI            0.159154943

//
// Globals
//



//
// Common Functions
//
//
__always_inline void _CLA_MTVEC_INIT(void)
{
    asm volatile(".align 2");
    asm volatile("la t0, _CLA_TRAP_ENTRY": : : "t0");
    asm volatile("csrw mtvec, t0": : : "t0");
}


__always_inline void  _CLA_usDELAY(unsigned int data)
{
       asm volatile( \
                       ".align 2;"\
                       "rpt %[loop], 4;" \
                       "and x0, x0, x0;" \
                       ".align 1;" \
                       : \
                       : [loop]"r"(data)\
                       : );
}

#define _CLA_DELAY_US(A)  _CLA_usDELAY((((long double)A * 1000.0L) / (long double)CPU_RATE) - 15.0L) // range A: 1 ~ 34000


//
//Task 1 (C) Variables
//
extern float y[];            //Result vector
extern float fVal;           //Holds the input argument to the task
extern float fResult;        //The arsine of the input argument

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//
extern float PIBYTWO;
extern float CLAatan2Table[];

//
// CLA C Tasks
//
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task1();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task2();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task3();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task4();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task5();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task6();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task7();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task8();
__interrupt void CODE_SECTION("Cla1Prog") Cla1BackgroundTask();

#endif /* _CLA_H_ */

//
// End of file
//
