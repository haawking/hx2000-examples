#include "f_dspcan.h"

void CAN_Gpio(void)
{
	EALLOW;
	/* 配置eCAN的外设功能复用*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 1; // GPIO30 is CANRXA
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 1; // GPIO31 is CANTXA
	/* 允许内部的上拉翻转，控制Ecan通信时序  */
	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;
	/* 异步接收*/
	GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 3;
	EDIS;
}

void CAN_Init()
{
   volatile struct ECAN_REGS ECanaShadow;//配置ECAN为影子寄存器模式

   EALLOW;
    ECanaShadow.CANTIOC.all = ECanaRegs.CANTIOC.all;//配置TX发送引脚
    ECanaShadow.CANTIOC.bit.TXFUNC = 1;
    ECanaRegs.CANTIOC.all = ECanaShadow.CANTIOC.all;

    ECanaShadow.CANRIOC.all = ECanaRegs.CANRIOC.all;//配置RX接收引脚
    ECanaShadow.CANRIOC.bit.RXFUNC = 1;
    ECanaRegs.CANRIOC.all = ECanaShadow.CANRIOC.all;

// MBOX0信箱配置初始化清零
   ECanaMboxes.MBOX0.MSGCTRL.bit.DLC=0;//初始化MBOX0发送字节为空
   ECanaMboxes.MBOX0.MSGCTRL.bit.RTR=0;//配置MBOX0远程发送请求为发送/接收
   ECanaMboxes.MBOX0.MSGCTRL.bit.TPL=0;//配置MBOX0为发送信箱

// MBOX1信箱配置初始化清零
    ECanaMboxes.MBOX1.MSGCTRL.bit.DLC=0;//初始化MBOX1发送字节为空
    ECanaMboxes.MBOX1.MSGCTRL.bit.RTR=0;//配置MBOX1远程发送请求为发送/接收
    ECanaMboxes.MBOX1.MSGCTRL.bit.TPL=0;//配置MBOX1为发送信箱

       // Initialize all bits of 'Message Control Register' to zero
       // Some bits of MSGCTRL register come up in an unknown state.
       // For proper operation, all bits (including reserved bits) of MSGCTRL must
       // be initialized to zero
       //
       ECanaMboxes.MBOX0.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX1.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX2.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX3.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX4.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX5.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX6.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX7.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX8.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX9.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX10.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX11.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX12.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX13.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX14.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX15.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX16.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX17.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX18.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX19.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX20.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX21.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX22.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX23.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX24.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX25.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX26.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX27.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX28.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX29.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX30.MSGCTRL.all = 0x00000000;
       ECanaMboxes.MBOX31.MSGCTRL.all = 0x00000000;


   ECanaRegs.CANRMP.all = 0xFFFFFFFF;//清除接收邮箱RMPn的挂起状态

   ECanaRegs.CANGIF0.all = 0xFFFFFFFF;//清除MBOXn信箱中断标志位
   ECanaRegs.CANGIF1.all = 0xFFFFFFFF;

/* 配置 eCANA的初始化*/
	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    ECanaShadow.CANES.all = ECanaRegs.CANES.all;

    do
	{
	    ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 1 );  		// Wait for CCE bit to be set..

    ECanaShadow.CANBTC.all = 0;

	ECanaShadow.CANBTC.bit.BRPREG = 2;//配置ECAN的位时序
	ECanaShadow.CANBTC.bit.TSEG2REG = 4;
	ECanaShadow.CANBTC.bit.TSEG1REG = 13;

    ECanaRegs.CANBTC.all = ECanaShadow.CANBTC.all;

    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 0 ;            // Set CCR = 0
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    ECanaShadow.CANES.all = ECanaRegs.CANES.all;

    do
    {
       ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 0 ); 		// Wait for CCE bit to be  cleared..

   ECanaRegs.CANME.all = 0;     // 写入MSGID标识前关闭信箱MEn的使能标志

/* Assign MSGID to MBOX1 */

   ECanaMboxes.MBOX1.MSGID.bit.STDMSGID= 0x1;	// Standard ID of 1, Acceptance mask disabled
   ECanaMboxes.MBOX0.MSGID.bit.STDMSGID= 0x1;	// Standard ID of 1, Acceptance mask disabled

   ECanaMboxes.MBOX1.MSGCTRL.bit.DLC = 0x2;//配置发送MBOX1字节长度为2
   ECanaMboxes.MBOX0.MSGCTRL.bit.DLC = 0x2;//配置接收MBOX0长度为2

   ECanaRegs.CANMD.bit.MD0=0;//配置MBOX0信箱为接收
   ECanaRegs.CANMD.bit.MD1=1;//配置MBOX1信箱为发送

   ECanaRegs.CANME.bit.ME0= 1;
   ECanaRegs.CANME.bit.ME1= 1;//使能打开MBOX0与MBOX1信箱

   if(ECanaRegs.CANTRS.bit.TRS0 != 0)//判断MBOX0信箱是否有操作，无操作，则准备接收
   {
	   ECanaRegs.CANTRS.bit.TRS0 = 1;
   }

   EDIS;

    return;
}
