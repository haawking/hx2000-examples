/******************************************************************
 文 档 名：     cla.h
 D S P：       DSC28035
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年3月28日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

#ifndef _CLA_H_
#define _CLA_H_


//
// Included Files
//
#include "DSP28x_Project.h"
#include "DSP2803x_Cla_defines.h"


//
// Common Functions
//
//
__always_inline void _CLA_MTVEC_INIT(void)
{
    asm volatile(".align 2");
    asm volatile("la t0, _CLA_TRAP_ENTRY": : : "t0");
    asm volatile("csrw mtvec, t0": : : "t0");
}


__always_inline void  _CLA_usDELAY(unsigned int data)
{
       asm volatile( \
                       ".align 2;"\
                       "rpt %[loop], 4;" \
                       "and x0, x0, x0;" \
                       ".align 1;" \
                       : \
                       : [loop]"r"(data)\
                       : );
}

#define _CLA_DELAY_US(A)  _CLA_usDELAY((((long double)A * 1000.0L) / (long double)CPU_RATE) - 15.0L) // range A: 1 ~ 34000



//
// Typedefs
//
typedef struct
{
	volatile struct EPWM_REGS *EPwmRegHandle;
	Uint16 EPwm_CMPA_Direction;
	Uint16 EPwm_CMPB_Direction;
	Uint16 EPwmTimerIntCount;
	Uint16 EPwmMaxCMPA;
	Uint16 EPwmMinCMPA;
	Uint16 EPwmMaxCMPB;
	Uint16 EPwmMinCMPB;
} EPWM_INFO;

//
// Prototype statements
//
void InitEPwm1(void);
void InitEPwm2(void);
void InitEPwm3(void);
__interrupt void epwm1_isr(void);
__interrupt void epwm2_isr(void);
__interrupt void epwm3_isr(void);

void CODE_SECTION("Cla1Prog") update_compare(EPWM_INFO*);

//
// Globals
//
extern EPWM_INFO CODE_SECTION("Cla1DataRam0") epwm1_info;
extern EPWM_INFO CODE_SECTION("Cla1DataRam0") epwm2_info;
extern EPWM_INFO CODE_SECTION("Cla1DataRam0") epwm3_info;

//
// Defines for the period for each timer
//
#define EPWM1_TIMER_TBPRD  2000  // Period register
#define EPWM1_MAX_CMPA     1950
#define EPWM1_MIN_CMPA       50
#define EPWM1_MAX_CMPB     1950
#define EPWM1_MIN_CMPB       50

#define EPWM2_TIMER_TBPRD  2000  // Period register
#define EPWM2_MAX_CMPA     1950
#define EPWM2_MIN_CMPA       50
#define EPWM2_MAX_CMPB     1950
#define EPWM2_MIN_CMPB       50

#define EPWM3_TIMER_TBPRD  2000  // Period register
#define EPWM3_MAX_CMPA      950
#define EPWM3_MIN_CMPA       50
#define EPWM3_MAX_CMPB     1950
#define EPWM3_MIN_CMPB     1050

//
// Defines that keep track of which way the compare value is moving
//
#define EPWM_CMP_UP   1
#define EPWM_CMP_DOWN 0

//
//Task 1 (C) Variables
//


//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//
extern float CLAasinTable[]; //The arcsine lookup table
//
// CLA C Tasks
//
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task1();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task2();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task3();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task4();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task5();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task6();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task7();
__interrupt void CODE_SECTION("Cla1Prog") Cla1Task8();
__interrupt void CODE_SECTION("Cla1Prog") Cla1BackgroundTask();

#endif /* _CLA_H_ */

//
// End of file
//
