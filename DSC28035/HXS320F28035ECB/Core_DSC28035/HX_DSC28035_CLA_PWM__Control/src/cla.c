/******************************************************************
 文 档 名：     cla.c
 D S P：       DSC2803x
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年3月28日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "cla.h"

//
// Defines
//

//
// Globals
//


//
// Function Definitions
//

//
//Task 1
//
__interrupt void Cla1Task1(void)
{

	_CLA_MTVEC_INIT(); //初始化MTVEC寄存器，CLA例外后跳转的地址信息

	//
	// Update the CMPA and CMPB values
	//
	update_compare(&epwm1_info);
//	update_compare(&epwm2_info);
//	update_compare(&epwm3_info);
}


//
//Task 2
//
__interrupt void Cla1Task2(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 3
//
__interrupt void Cla1Task3(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 4
//
__interrupt void Cla1Task4(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 5
//
__interrupt void Cla1Task5(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 6
//
__interrupt void Cla1Task6(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 7
//
__interrupt void Cla1Task7(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 8
//
__interrupt void Cla1Task8(void)
{
	// _CLA_MTVEC_INIT();
}

//-----------------------------------------------------------------------------
//
// Background Task
//
//-----------------------------------------------------------------------------
__interrupt void Cla1BackgroundTask(void)
{
	// _CLA_MTVEC_INIT();
}

//
// End of file
//

