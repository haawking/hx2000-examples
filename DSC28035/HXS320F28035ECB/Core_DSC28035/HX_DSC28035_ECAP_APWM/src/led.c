#include "ecap.h"

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO31= 0;
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0;
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	EDIS;
}


