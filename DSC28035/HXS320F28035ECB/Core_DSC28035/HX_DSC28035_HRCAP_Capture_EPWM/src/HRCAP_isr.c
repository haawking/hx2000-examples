#include "HRCAP.h"

//defines

#define PWM1_TIMER_MAX 4000

#define EPWM_TIMER_UP   1
#define EPWM_TIMER_DOWN 0

void INTERRUPT hrcap1_isr(void)
{
	EALLOW;
	/*上升沿溢出捕获事件产生时执行空语句*/
	if(HRCap1Regs.HCIFR.bit.RISEOVF==1)
	{
		__asm(" NOP");
	}
	/*延迟一条指令周期，保证EPWM到达上升沿，便于捕获*/
	if(first==0)
	{
		first=1;
	}
	else
	{
		/*上升沿捕获使用禁用*/
		HRCap1Regs.HCCTL.bit.RISEINTE=0;
		/*高脉冲宽度为下降沿捕获计数器计数+1*/
				PULSEHIGH=HRCap1Regs.HCCAPCNTFALL1+1;
				/*高脉冲宽度在HRCAP的可捕获周期之外，捕获失败，执行空语句*/
				if((PULSEHIGH>((Uint32)(EPwm1Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ+NUM_HCCAPCLKS_PER_HCSYSCLK+1))||(PULSEHIGH<((Uint32)(EPwm1Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ-NUM_HCCAPCLKS_PER_HCSYSCLK-1)))
				{
					__asm(" NOP");
				}

				/*低脉冲宽度为下降沿捕获计数器计数+1*/
				PULSELOW=HRCap1Regs.HCCAPCNTRISE0+1;
				/*低脉冲宽度在HRCAP的可捕获周期之外，捕获失败，执行空语句*/
				if((PULSELOW>((Uint32)(EPwm1Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ+NUM_HCCAPCLKS_PER_HCSYSCLK+1))||(PULSELOW<((Uint32)(EPwm1Regs.TBPRD+1)*HCCAPCLK_FREQ/SYSCLK_FREQ-NUM_HCCAPCLKS_PER_HCSYSCLK-1)))
				{
					__asm(" NOP");
				}

				/*高低脉冲宽度在HRCAP捕获周期内时，EPWM1执行周期从1000-4000TBCLK间变化的捕获指令，输出的PWM波周期在1000-4000之间先逐渐增大，然后又逐渐减小*/
				if (EPwm1TimerDirection == EPWM_TIMER_UP)
		        {
					/*判断PWM周期是否小于最大值，若小于，则增大到最大值，此时D400(GPIO34)翻转闪灯*/
					if(EPwm1Regs.TBPRD < PWM1_TIMER_MAX)
		            {
		                EPwm1Regs.TBPRD+=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO34=1;
		                DELAY_US(1000000);
		            }
		            else
		            {
						/*PWM周期大于等于最大值时，减小至最小值，此时D401(GPIO41)翻转闪灯*/
		            	EPwm1TimerDirection = EPWM_TIMER_DOWN;
		                EPwm1Regs.TBPRD-=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO41=1;
		                DELAY_US(1000000);
		            }
		        }
		        else
		        {
		            if(EPwm1Regs.TBPRD > period)
		            {
		                EPwm1Regs.TBPRD-=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO41=1;
		                DELAY_US(1000000);
		            }
		            else
		            {
		                EPwm1TimerDirection = EPWM_TIMER_UP;
		                EPwm1Regs.TBPRD+=500;
		                GpioDataRegs.GPBTOGGLE.bit.GPIO34=1;
		                DELAY_US(1000000);
		            }
		        }
		    }
	/*清除HRCAP的所有HCIFR中断置位*/
	HRCap1Regs.HCICLR.all=0x001F;
	/*使能上升沿捕获中断*/
	HRCap1Regs.HCCTL.bit.RISEINTE=1;
	/*清除HRCAP的全局中断标志*/
	HRCap1Regs.HCICLR.bit.INT=1;
	PieCtrlRegs.PIEACK.all=PIEACK_GROUP4;
	EDIS;
}
