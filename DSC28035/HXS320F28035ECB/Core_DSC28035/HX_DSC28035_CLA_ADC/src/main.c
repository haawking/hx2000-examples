/******************************************************************
 工 程 名：    f28035_cla_adc
 开 发 环 境：Haawking IDE V2.1.4
 开 发 板 ：   Core_DSC28035PNS_V1.0
 D S P：      DSC28035
 使 用 库：    无
 作     用：
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------

 描  述：      CLA Task 1 访问 ADC

 现  象：

 版 本：      V1.0.0
 时 间：      2023年3月28日
 作 者：      Gengzhuo
 @mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "CLAmath.h"
#include "cla.h"

//
// Defines
//

//
// Globals
//

//
//Task 1 (C) Variables
//
uint32_t CODE_SECTION("Cla1ToCpuMsgRAM") cResult = 0;
uint32_t CODE_SECTION("CpuToCla1MsgRAM") aVal = 0;

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
// Function Prototypes
//
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);

__interrupt void adc_isr(void);
void adc_init(void);

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void);
__interrupt void cla1_task2_isr(void);
__interrupt void cla1_task3_isr(void);
__interrupt void cla1_task4_isr(void);
__interrupt void cla1_task5_isr(void);
__interrupt void cla1_task6_isr(void);
__interrupt void cla1_task7_isr(void);
__interrupt void cla1_task8_isr(void);

//
// Main
//
int main(void)
{

	InitSysCtrl();

	DINT;

	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	EALLOW;
	PieVectTable.ADCINT1 = &adc_isr;
	EDIS;

	InitAdc();

	//
	// Enable ADCINT1 in PIE
	//
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1; // Enable INT 1.1 in the PIE
	IER |= M_INT1;                     // Enable CPU Interrupt 1

	CLA_configClaMemory();

	CLA_initCpu1Cla1();


	//
	// Configure ADC
	//
	EALLOW;
	AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1; //ADCINT1 trips after AdcResults latch
	AdcRegs.INTSEL1N2.bit.INT1E = 1; // Enabled ADCINT1
	AdcRegs.INTSEL1N2.bit.INT1CONT = 0; // Disable ADCINT1 Continuous mode
	AdcRegs.INTSEL1N2.bit.INT1SEL = 2;  // setup EOC2 to trigger
										// ADCINT1 to fire

	//
	// set SOC0 channel select to ADCINA4
	//
	AdcRegs.ADCSOC0CTL.bit.CHSEL = 4;

	AdcRegs.ADCSOC1CTL.bit.CHSEL = 4;  //set SOC1 channel select to ADCINA4
	AdcRegs.ADCSOC2CTL.bit.CHSEL = 2;  //set SOC2 channel select to ADCINA2

	AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 5;

	AdcRegs.ADCSOC1CTL.bit.TRIGSEL = 5;

	AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 5;

	AdcRegs.ADCSOC0CTL.bit.ACQPS = 0;

	AdcRegs.ADCSOC1CTL.bit.ACQPS = 0;

	AdcRegs.ADCSOC2CTL.bit.ACQPS = 0;
	EDIS;

	//
	// Configure epwm1 param
	//
	EPwm1Regs.ETSEL.bit.SOCAEN = 1;    // Enable SOC on A group
	EPwm1Regs.ETSEL.bit.SOCASEL = 4;   // Select SOC from from CPMA on upcount
	EPwm1Regs.ETPS.bit.SOCAPRD = 1;    // Generate pulse on 1st event
	EPwm1Regs.CMPA.half.CMPA = 0x0080; // Set compare A value
	EPwm1Regs.TBPRD = 0xFFFF;          // Set period for ePWM1
	EPwm1Regs.TBCTL.bit.CTRMODE = 0;   // count up and start

	EINT;

	for (;;)
	{

	}

	return 0;
}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
	EALLOW;
	//友商F28035老版本CLA协处理器的配置方式
	//Cla1Regs.MMEMCFG.bit.PROGE         = 1;
	//Cla1Regs.MCTL.bit.IACKE        = 1;
	//Cla1Regs.MMEMCFG.bit.RAM0E        = CLARAM0_ENABLE;
	//Cla1Regs.MMEMCFG.bit.RAM1E        = CLARAM1_ENABLE;
	//昊芯CLA协处理器的配置方式，LS1配置为CLA独享的代码区，LS0和LS2配置为CPU和CLA共享的数据区
	Cla1Regs.LSMSEL.bit.MSEL_LS0 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS1 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS2 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS0 = 0;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS1 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS2 = 0;
	EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
	EALLOW;
	PieVectTable.CLA1_INT1 = &cla1_task1_isr;
	PieVectTable.CLA1_INT2 = &cla1_task2_isr;
	PieVectTable.CLA1_INT3 = &cla1_task3_isr;
	PieVectTable.CLA1_INT4 = &cla1_task4_isr;
	PieVectTable.CLA1_INT5 = &cla1_task5_isr;
	PieVectTable.CLA1_INT6 = &cla1_task6_isr;
	PieVectTable.CLA1_INT7 = &cla1_task7_isr;
	PieVectTable.CLA1_INT8 = &cla1_task8_isr;
	EDIS;

	//
	// Compute all CLA task vectors
	//
	EALLOW;
	Cla1Regs.MVECT1 = (Uint32) &Cla1Task1;
	Cla1Regs.MVECT2 = (Uint32) &Cla1Task2;
	Cla1Regs.MVECT3 = (Uint32) &Cla1Task3;
	Cla1Regs.MVECT4 = (Uint32) &Cla1Task4;
	Cla1Regs.MVECT5 = (Uint32) &Cla1Task5;
	Cla1Regs.MVECT6 = (Uint32) &Cla1Task6;
	Cla1Regs.MVECT7 = (Uint32) &Cla1Task7;
	Cla1Regs.MVECT8 = (Uint32) &Cla1Task8;
	Cla1Regs._MVECTBGRND = (Uint32) &Cla1BackgroundTask;
	Cla1Regs.MIER.all = 0xFF;

	Cla1Regs.CLA1TASKSRCSEL1.bit.TASK1 = 1; //ADCA1_INT 触发
	Cla1Regs._MCTLBGRND.bit.BGEN = 1;
	Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
	EDIS;

	PieCtrlRegs.PIEIER11.all = 0xFFFF;
	IER |= M_INT11;
	EINT;
}

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task2_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task3_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task4_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task5_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task6_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task7_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task8_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

//
// adc_isr -
//
__interrupt void adc_isr(void)
{

	//
	// Clear ADCINT1 flag reinitialize for next SOC
	//
	AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

	return;
}

//
// End of file
//
