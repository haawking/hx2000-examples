//###########################################################################
//
// FILE:  DSP2803x_Cla.h
//
// TITLE: CLA Type 0 Register Definitions.
//
//###########################################################################
// $HAAWKING Release: DSP2803x Support Library V1.0.0 $
// $Release Date: 2022-10-18 05:22:31 $
// $Copyright:
// Copyright (C) 2019-2022 Beijing Haawking Technology Co.,Ltd - http://www.haawking.com/
//###########################################################################

#ifndef    DSP2803X_CLA_H
#define   DSP2803X_CLA_H

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------------------------
// CLA Individual Register Bit Definitions:





struct _MSTF_BITS {                     // bits description
    Uint32 LVF:1;                       // 0     Latched Overflow Flag
    Uint32 LUF:1;                       // 1     Latched Underflow Flag
    Uint32 NF:1;                        // 2     Negative Float Flag
    Uint32 ZF:1;                        // 3     Zero Float Flag
    Uint32 rsvd1:2;                     // 5:4   Reserved
    Uint32 TF:1;                        // 6     Test Flag
    Uint32 rsvd2:2;                     // 8:7   Reserved
    Uint32 RNDF32:1;                    // 9     Round 32-bit Floating-Point Mode
    Uint32 rsvd3:1;                     // 10    Reserved
    Uint32 MEALLOW:1;                   // 11    MEALLOW Status
    Uint32 rsvd4:20;                    // 31:12 Reserved
};

union _MSTF_REG {
    Uint32  all;
    struct  _MSTF_BITS  bit;
};


struct _MPSACTL_BITS {                    // bits description
    Uint32 MPABSTART:1;                   // 0    Start logging PAB onto PSA1
    Uint32 MPABCYC:1;                     // 1    PAB logging into PSA1 is on every cycle or when PAB changes.
    Uint32 MDWDBSTART:1;                  // 2    Start logging DWDB onto PSA2
    Uint32 MDWDBCYC:1;                    // 3    DWDB logging into PSA2 is on every cycle.
    Uint32 MPSA1CLEAR:1;                  // 4    PSA1 clear
    Uint32 MPSA2CLEAR:1;                  // 5    PSA2 Clear
    Uint32 MPSA2CFG:2;                    // 7:6  PSA2 Polynomial Configuration
    Uint32 rsvd1:24;                      // 31:8 Reserved
};

union _MPSACTL_REG {
    Uint32  all;
    struct  _MPSACTL_BITS  bit;
};


struct SOFTINTEN_BITS {                 // bits description
    Uint32 TASK1:1;                     // 0    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK2:1;                     // 1    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK3:1;                     // 2    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK4:1;                     // 3    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK5:1;                     // 4    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK6:1;                     // 5    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK7:1;                     // 6    Configure Software Interrupt or End of Task interrupt.
    Uint32 TASK8:1;                     // 7    Configure Software Interrupt or End of Task interrupt.
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union SOFTINTEN_REG {
    Uint32  all;
    struct  SOFTINTEN_BITS  bit;
};


struct SOFTINTFRC_BITS {                // bits description
    Uint32 TASK1:1;                     // 0    Force CLA software interrupt for the corresponding task.
    Uint32 TASK2:1;                     // 1    Force CLA software interrupt for the corresponding task.
    Uint32 TASK3:1;                     // 2    Force CLA software interrupt for the corresponding task.
    Uint32 TASK4:1;                     // 3    Force CLA software interrupt for the corresponding task.
    Uint32 TASK5:1;                     // 4    Force CLA software interrupt for the corresponding task.
    Uint32 TASK6:1;                     // 5    Force CLA software interrupt for the corresponding task.
    Uint32 TASK7:1;                     // 6    Force CLA software interrupt for the corresponding task.
    Uint32 TASK8:1;                     // 7    Force CLA software interrupt for the corresponding task.
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union SOFTINTFRC_REG {
    Uint32  all;
    struct  SOFTINTFRC_BITS  bit;
};

struct MCTL_BITS {                      // bits description
    Uint32 HARDRESET:1;                 // 0    Hard Reset
    Uint32 SOFTRESET:1;                 // 1    Soft Reset
    Uint32 rsvd1:30;                    // 31:3 Reserved
};

union MCTL_REG {
    Uint32  all;
    struct  MCTL_BITS  bit;
};

struct _MSTSBGRND_BITS {                // bits description
    Uint32 RUN:1;                       // 0    Background task run status bit.
    Uint32 _BGINTM:1;                   // 1    Indicates whether background task can be interrupted.
    Uint32 BGOVF:1;                     // 2    Background task harware trigger overflow.
    Uint32 rsvd1:29;                    // 31:3 Reserved
};

union _MSTSBGRND_REG {
    Uint32  all;
    struct  _MSTSBGRND_BITS  bit;
};

struct _MCTLBGRND_BITS {                // bits description
    Uint32 BGSTART:1;                   // 0     Background task start bit
    Uint32 TRIGEN:1;                    // 1     Background task hardware trigger enable
    Uint32 rsvd1:13;                    // 14:2  Reserved
    Uint32 BGEN:1;                      // 15    Enable background task
    Uint32 rsvd2:16;                    // 31:16 Reserved
};

union _MCTLBGRND_REG {
    Uint32  all;
    struct  _MCTLBGRND_BITS  bit;
};


struct MIFR_BITS {                      // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Flag
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Flag
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Flag
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Flag
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Flag
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Flag
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Flag
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Flag
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MIFR_REG {
    Uint32  all;
    struct  MIFR_BITS  bit;
};

struct MIOVF_BITS {                     // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Overflow Flag
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Overflow Flag
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Overflow Flag
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Overflow Flag
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Overflow Flag
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Overflow Flag
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Overflow Flag
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Overflow Flag
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MIOVF_REG {
    Uint32  all;
    struct  MIOVF_BITS  bit;
};

struct MIFRC_BITS {                     // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Force
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Force
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Force
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Force
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Force
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Force
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Force
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Force
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MIFRC_REG {
    Uint32  all;
    struct  MIFRC_BITS  bit;
};

struct MICLR_BITS {                     // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Flag Clear
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Flag Clear
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Flag Clear
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Flag Clear
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Flag Clear
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Flag Clear
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Flag Clear
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Flag Clear
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MICLR_REG {
    Uint32  all;
    struct  MICLR_BITS  bit;
};

struct MICLROVF_BITS {                  // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Overflow Flag Clear
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Overflow Flag Clear
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Overflow Flag Clear
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Overflow Flag Clear
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Overflow Flag Clear
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Overflow Flag Clear
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Overflow Flag Clear
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Overflow Flag Clear
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MICLROVF_REG {
    Uint32  all;
    struct  MICLROVF_BITS  bit;
};

struct MIER_BITS {                      // bits description
    Uint32 INT1:1;                      // 0    Task 1 Interrupt Enable
    Uint32 INT2:1;                      // 1    Task 2 Interrupt Enable
    Uint32 INT3:1;                      // 2    Task 3 Interrupt Enable
    Uint32 INT4:1;                      // 3    Task 4 Interrupt Enable
    Uint32 INT5:1;                      // 4    Task 5 Interrupt Enable
    Uint32 INT6:1;                      // 5    Task 6 Interrupt Enable
    Uint32 INT7:1;                      // 6    Task 7 Interrupt Enable
    Uint32 INT8:1;                      // 7    Task 8 Interrupt Enable
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MIER_REG {
    Uint32  all;
    struct  MIER_BITS  bit;
};

struct MIRUN_BITS {                     // bits description
    Uint32 INT1:1;                      // 0    Task 1 Run Status
    Uint32 INT2:1;                      // 1    Task 2 Run Status
    Uint32 INT3:1;                      // 2    Task 3 Run Status
    Uint32 INT4:1;                      // 3    Task 4 Run Status
    Uint32 INT5:1;                      // 4    Task 5 Run Status
    Uint32 INT6:1;                      // 5    Task 6 Run Status
    Uint32 INT7:1;                      // 6    Task 7 Run Status
    Uint32 INT8:1;                      // 7    Task 8 Run Status
    Uint32 rsvd1:24;                    // 31:8 Reserved
};

union MIRUN_REG {
    Uint32  all;
    struct  MIRUN_BITS  bit;
};




struct CLA1TASKSRCSELLOCK_BITS {         // bits description
    Uint32  CLA1TASKSRCSEL1 :1;          // 0     CLA1TASKSRCSEL1 Register Lock bit
    Uint32  CLA2TASKSRCSEL2:1;           // 1     CLA2TASKSRCSEL2 Register Lock bit
    Uint32  rsvd1:30;                    // 31:2  Reserved
};



union CLA1TASKSRCSELLOCK_REG {
    Uint32  all;
    struct  CLA1TASKSRCSELLOCK_BITS  bit;
};

struct CLA1TASKSRCSEL1_BITS {             // bits description
    Uint32  TASK1:8;                      // 7:0   Selects the Trigger Source for TASK1 of CLA1
    Uint32  TASK2:8;                      // 15:8  Selects the Trigger Source for TASK2 of CLA1
    Uint32  TASK3:8;                      // 23:16 Selects the Trigger Source for TASK3 of CLA1
    Uint32  TASK4:8;                      // 31:24 Selects the Trigger Source for TASK4 of CLA1
};



union CLA1TASKSRCSEL1_REG {
    Uint32  all;
    struct   CLA1TASKSRCSEL1_BITS  bit;
};

struct CLA1TASKSRCSEL2_BITS {              // bits description
    Uint32  TASK5:8;                       //7:0   Selects the Trigger Source for TASK5 of CLA1
    Uint32  TASK6:8;                       //15:8  Selects the Trigger Source for TASK6 of CLA1
    Uint32  TASK7:8;                       //23:16 Selects the Trigger Source for TASK7 of CLA1
    Uint32  TASK8:8;                       //31:24 Selects the Trigger Source for TASK8 of CLA1
};



union CLA1TASKSRCSEL2_REG {
    Uint32  all;
    struct   CLA1TASKSRCSEL2_BITS  bit;
};

struct LSLOCK_BITS {                       // bits description
    Uint32  LOSK_LS0:1;                    // 0    L0 Locks the write to access protection,master select
    Uint32  LOSK_LS1:1;                    // 1    L1 Locks the write to access protection,master select
    Uint32  LOSK_LS2:1;                    // 2    L2 Locks the write to access protection,master select
    Uint32  LOSK_LS3:1;                    // 3    L3 Locks the write to access protection,master select
    Uint32  rsvd1:28;                      // 31:4 Reserved
};




union LSLOCK_REG {
    Uint32  all;
    struct   LSLOCK_BITS  bit;
};



struct LSCOMMIT_BITS {                    // bits description
    Uint32   COMMIT_LS0:1;                // 0    L0 Permanently Locks the write to access protection ,master select,program or data memory select.
    Uint32   COMMIT_LS1:1;                // 1    L1 Permanently Locks the write to access protection ,master select,program or data memory select.
    Uint32   COMMIT_LS2:1;                // 2    L2 Permanently Locks the write to access protection ,master select,program or data memory select.
    Uint32   COMMIT_LS3:1;                // 3    L3 Permanently Locks the write to access protection ,master select,program or data memory select.
    Uint32   rsvd1:28;                    // 31:4 Reserved
};




union LSCOMMIT_REG {
    Uint32  all;
    struct  LSCOMMIT_BITS bit;
};



struct LSMSEL_BITS {                    // bits description
    Uint32   MSEL_LS0:2;                //1:0  Master Select for LS0 RAM
    Uint32   MSEL_LS1:2;                //3:2  Master Select for LS1 RAM
    Uint32   MSEL_LS2:2;                //5:4  Master Select for LS2 RAM
    Uint32   MSEL_LS3:2;                //7:6  Master Select for LS3 RAM
    Uint32   rsvd1:24;                  //31:8 Reserved
};




union  LSMSEL_REG {
    Uint32    all;
    struct    LSMSEL_BITS bit;
};


struct LSCLAPGM_BITS {                    // bits description
    Uint32    CLAPGM_LS0:1;               //0     Selects LS0 RAM as program vs data memory for CLA
    Uint32    CLAPGM_LS1:1;               //1     Selects LS1 RAM as program vs data memory for CLA
    Uint32    CLAPGM_LS2:1;               //2     Selects LS2 RAM as program vs data memory for CLA
    Uint32    CLAPGM_LS3:1;               //3     Selects LS3 RAM as program vs data memory for CLA
    Uint32    rsvd1:28;                   //31:4  Reserved
};



union  LSCLAPGM_REG{
    Uint32      all;
    struct      LSCLAPGM_BITS  bit;
};




struct CLA_REGS {
    Uint32                                                    MVECT1;                      // Task Interrupt Vector                              40
    Uint32                                                    MVECT2;                      // Task Interrupt Vector                              44
    Uint32                                                    MVECT3;                      // Task Interrupt Vector                              48
    Uint32                                                    MVECT4;                      // Task Interrupt Vector                              4C
    Uint32                                                    MVECT5;                      // Task Interrupt Vector                              50
    Uint32                                                    MVECT6;                      // Task Interrupt Vector                              54
    Uint32                                                    MVECT7;                      // Task Interrupt Vector                              58
    Uint32                                                    MVECT8;                      // Task Interrupt Vector                              5C
    union                  MCTL_REG                           MCTL;                        // Control Register                                   60
    Uint32                                                    _MVECTBGRNDACTIVE;           // Active register for MVECTBGRND.                    64
    union                  SOFTINTEN_REG                      SOFTINTEN;                   // CLA Software Interrupt Enable Register             68
    union                  _MSTSBGRND_REG                     _MSTSBGRND;                  // Status register for the back ground task.          6C
    union                  _MCTLBGRND_REG                     _MCTLBGRND;                  // Control register for the back ground task.         70
    Uint32                                                    _MVECTBGRND;                 // Vector for the back ground task.                   74
    union                  MIFR_REG                           MIFR;                        // Interrupt Flag Register                            78
    union                  MIOVF_REG                          MIOVF;                       // Interrupt Overflow Flag Register                   7C
    union                  MIFRC_REG                          MIFRC;                       // Interrupt Force Register                           80
    union                  MICLR_REG                          MICLR;                       // Interrupt Flag Clear Register                      84
    union                  MICLROVF_REG                       MICLROVF;                    // Interrupt Overflow Flag Clear Register             88
    union                  MIER_REG                           MIER;                        // Interrupt Enable Register                          8C
    union                  MIRUN_REG                          MIRUN;                       // Interrupt Run Status Register                      90
    Uint32                                                    _MPC;                        // CLA Program Counter                                94
    union                  _MSTF_REG                          _MSTF;                       // CLA Floating-Point Status Register                 98
    union                  _MPSACTL_REG                       _MPSACTL;                    // CLA PSA Control Register                           9C
    Uint32                                                    _MPSA1;                      // CLA PSA1 Register                                  A0
    Uint32                                                    _MPSA2;                      // CLA PSA2 Register                                  A4
    union              CLA1TASKSRCSELLOCK_REG                 CLA1TASKSRCSELLOCK;          // CLA CLA1TASKSRCSELLOCK                             A8
    union               CLA1TASKSRCSEL1_REG                   CLA1TASKSRCSEL1;             // CLA CLA1TASKSRCSEL1                                AC
    union               CLA1TASKSRCSEL2_REG                   CLA1TASKSRCSEL2;             // CLA CLA1TASKSRCSEL2                                B0
    union                   LSLOCK_REG                        SLOCK;                       // CLA  LSLOCK                                        B4
    union                  LSCOMMIT_REG                       LSCOMMIT;                    // CLA  LSCOMMIT                                      B8
    union                   LSMSEL_REG                        LSMSEL;                      // CLA  LSMSEL                                        BC
    union                  LSCLAPGM_REG                       LSCLAPGM;                    // CLA  LSCLAPGM                                      C0
    Uint32                                                     RPC;                        // CLA   RPC                                          C4
};


struct CLA_ONLY_REGS {
    Uint32                                             _MVECTBGRNDACTIVE;                   // Active register for MVECTBGRND.                    0
    union               _MPSACTL_REG                   _MPSACTL;                            // CLA PSA Control Register                           4
    Uint32                                             _MPSA1;                              // CLA PSA1 Register                                  8
    Uint32                                             _MPSA2;                              // CLA PSA2 Register                                  C
    union              SOFTINTEN_REG                   SOFTINTEN;                           // CLA Software Interrupt Enable Register            10
    union              SOFTINTFRC_REG                  SOFTINTFRC;                          // CLA Software Interrupt Force Register             14
};




extern volatile struct CLA_ONLY_REGS  Cla1OnlyRegs;

extern volatile struct CLA_REGS Cla1Regs;



#ifdef __cplusplus
}
#endif /* extern "C" */


#endif
