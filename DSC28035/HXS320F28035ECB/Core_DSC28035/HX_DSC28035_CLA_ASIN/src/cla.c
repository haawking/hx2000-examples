/******************************************************************
 文 档 名：     cla.c
 D S P：       DSC2803x
 作     用：
 说     明：     
 ---------------------------- 功能说明 ----------------------------
 描 述：


 现 象：

 版 本：V1.0.0
 日 期：2023年3月28日
 作 者：Gengzhuo
 @mail：support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "cla.h"

//
// Defines
//

//
// Globals
//


//
// Function Definitions
//
//
//Task 1 : Calculate asin(X)
// Description:
//             Step(1): Calculate absolute of the input X
//
//             Step(2):   Use the upper 6-bits of input "X" value as an
//                          index into the table to obtain the coefficients
//                          for a second order equation:
//
//                        _FPUasinTable:
//                             CoeffA0[0]
//                             CoeffA1[0]
//                             CoeffA2[0]
//                                .
//                                .
//                             CoeffA0[63]
//                             CoeffA1[63]
//                             CoeffA2[63]
//
//             Step(3):   Calculate the angle using the following equation:
//
//                        arctan(Ratio) = A0 + A1*Ratio + A2*Ratio*Ratio
//                        arctan(Ratio) = A0 + Ratio(A1 + A2*Ratio)
//
//             Step(4):   The final angle is determined as follows:
//
//                        if( X < 0 )
//                            Angle = -Angle
__interrupt void Cla1Task1(void)
{
	//
	//Local Variables
	//
	int xTblIdx; //integer valued Table index
	float A0, A1, A2; //Table coefficients
	float *entry;
	float result;

	_CLA_MTVEC_INIT(); //初始化MTVEC寄存器，CLA例外后跳转的地址信息

	//
	//Preprocessing
	//
//    __mdebugstop();
	xTblIdx = fVal * TABLE_SIZE_M_1; //convert table index to u16-bits
	xTblIdx = xTblIdx * 3; //Table is ordered as 3 32-bit coefficients, the
						   //index points to these triplets, hence the *3*sizeof(float)
	entry = &CLAasinTable[xTblIdx];
	A0 = *entry++;
	A1 = *entry++;
	A2 = *entry;

	result = A0 + fVal * (A1 + A2 * fVal);


	//
	//Post processing
	//
	if (fVal < 0)
	{
		result = -result;
	}

	fResult = result;
}


//
//Task 2
//
__interrupt void Cla1Task2(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 3
//
__interrupt void Cla1Task3(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 4
//
__interrupt void Cla1Task4(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 5
//
__interrupt void Cla1Task5(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 6
//
__interrupt void Cla1Task6(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 7
//
__interrupt void Cla1Task7(void)
{
	// _CLA_MTVEC_INIT();
}

//
//Task 8
//
__interrupt void Cla1Task8(void)
{
	// _CLA_MTVEC_INIT();
}

//-----------------------------------------------------------------------------
//
// Background Task
//
//-----------------------------------------------------------------------------
__interrupt void Cla1BackgroundTask(void)
{
	// _CLA_MTVEC_INIT();
}

//
// End of file
//

