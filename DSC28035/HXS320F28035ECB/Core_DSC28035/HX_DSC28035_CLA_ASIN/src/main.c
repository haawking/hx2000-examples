/******************************************************************
 工 程 名：      f28035_cla_asin
 开 发 环 境：Haawking IDE V2.1.4
 开 发 板 ：   Core_DSC28035PNS_V1.0
 D S P：      DSC28035
 使 用 库：    无
 作     用：
 说     明：     FLASH工程
 -------------------------- 例程使用说明 --------------------------

 描  述：      CLA Task 1 将计算 由于 CPU提供的 -1.0 到 1.0 之间 目标值的 反正弦
                    计算方式为 查找表

 现  象：     分别将 fResult fVal pass fail 放入 Liveview视图，可查看
                   计算过程和结果

 版 本：      V1.0.0
 时 间：      2023年3月28日
 作 者：      Gengzhuo
 @mail：   support@mail.haawking.com
 ******************************************************************/

//
// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
#include "CLAmath.h"
#include "cla.h"

//
// Defines
//


//
// Globals
//

//
//Task 1 (C) Variables
//
float CODE_SECTION("Cla1ToCpuMsgRAM") fResult = 0.0f;
float CODE_SECTION("CpuToCla1MsgRAM") fVal = 0.0f;

float y[BUFFER_SIZE];

//
//Task 2 (C) Variables
//

//
//Task 3 (C) Variables
//

//
//Task 4 (C) Variables
//

//
//Task 5 (C) Variables
//

//
//Task 6 (C) Variables
//

//
//Task 7 (C) Variables
//

//
//Task 8 (C) Variables
//

//
//Common (C) Variables
//The Exponential table
//
float CODE_SECTION("Cla1DataRam0") CLAasinTable[] = {
		    0.0, 1.0, 0.0,
		    0.000000636202, 0.999877862610, 0.007815361896,
		    0.000005099694, 0.999510644409, 0.015647916155,
		    0.000017268312, 0.998895919094, 0.023514960332,
		    0.000041121765, 0.998029615282, 0.031434003631,
		    0.000080794520, 0.996905974725, 0.039422875916,
		    0.000140631089, 0.995517492804, 0.047499840611,
		    0.000225244584, 0.993854840311, 0.055683712914,
		    0.000339579512, 0.991906765146, 0.063993984848,
		    0.000488979852, 0.989659972212, 0.072450958820,
		    0.000679263611, 0.987098979366, 0.081075891529,
		    0.000916805182, 0.984205946802, 0.089891150305,
		    0.001208627040, 0.980960476685, 0.098920384204,
		    0.001562502549, 0.977339379243, 0.108188712551,
		    0.001987071928, 0.973316400729, 0.117722933997,
		    0.002491973784, 0.968861907789, 0.127551759665,
		    0.003087995053, 0.963942521723, 0.137706074532,
		    0.003787242692, 0.958520694794, 0.148219231941,
		    0.004603341138, 0.952554219267, 0.159127386977,
		    0.005551660294, 0.945995657913, 0.170469875522,
		    0.006649579796, 0.938791682505, 0.182289647088,
		    0.007916796475, 0.930882303984, 0.194633761132,
		    0.009375683410, 0.922199974574, 0.207553958472,
		    0.011051710808, 0.912668537890, 0.221107321885,
		    0.012973941175, 0.902201997769, 0.235357042896,
		    0.015175614174, 0.890703070035, 0.250373315541,
		    0.017694840102, 0.878061473098, 0.266234382514,
		    0.020575425537, 0.864151902887, 0.283027765009,
		    0.023867860513, 0.848831624374, 0.300851714968,
		    0.027630504055, 0.831937595031, 0.319816937941,
		    0.031931014547, 0.813283013821, 0.340048646894,
		    0.036848083955, 0.792653161200, 0.361689022958,
		    0.042473551274, 0.769800358920, 0.384900179460,
		    0.048914992206, 0.744437830278, 0.409867752228,
		    0.056298910750, 0.716232177740, 0.436805274317,
		    0.064774696786, 0.684794109766, 0.465959540059,
		    0.074519565699, 0.649666934178, 0.497617226179,
		    0.085744766889, 0.610312179660, 0.532113122767,
		    0.098703445606, 0.566091493186, 0.569840443472,
		    0.113700678529, 0.516243664372, 0.611263845480,
		    0.131106395009, 0.459855210927, 0.656936015611,
		    0.151372169232, 0.395822366759, 0.707518998893,
		    0.175053263659, 0.322801460177, 0.763811905770,
		    0.202837883870, 0.239143420888, 0.826787304376,
		    0.235586468765, 0.142806299514, 0.897639596948,
		    0.274385149825, 0.031236880585, 0.977850174820,
		    0.320619535938, -0.098791845166, 1.069276441800,
		    0.376078169620, -0.251407364538, 1.174275392129,
		    0.443100143614, -0.431959397725, 1.295878193174,
		    0.524789871827, -0.647485610469, 1.438041695773,
		    0.625336471263, -0.907400624736, 1.606018804842,
		    0.750500589935, -1.224540947101, 1.806917563896,
		    0.908377657341, -1.616794995066, 2.050569262035,
		    1.110633894185, -2.109729648039, 2.350920816737,
		    1.374584721437, -2.740985157716, 2.728353889708,
		    1.726848242753, -3.567962877198, 3.213722960014,
		    2.210117561056, -4.682006534082, 3.855770086891,
		    2.896554011854, -6.236312386687, 4.735651038017,
		    3.916505715382, -8.505488022524, 5.997790945975,
		    5.526855868703, -12.026617159136, 7.922628470498,
		    8.298197116322, -17.983705080358, 11.123941286820,
		    13.741706072449, -29.488929624542, 17.203344479111,
		    27.202707817485, -57.466598393615, 31.741016484669,
		    83.158101335898, -171.803399517566, 90.149831709374
		};

float asin_expected[BUFFER_SIZE]={
	1.570796, 1.393789, 1.320141, 1.263401, 1.215375,
	1.172892, 1.134327, 1.098718, 1.065436, 1.034046,
	1.004232, 0.9757544, 0.9484279, 0.9221048, 0.8966658,
	0.8720123, 0.8480621, 0.8247454, 0.8020028, 0.7797828,
	0.7580408, 0.7367374, 0.7158381, 0.6953120, 0.6751316,
	0.6552721, 0.6357113, 0.6164289, 0.5974064, 0.5786270,
	0.5600753, 0.5417370, 0.5235988, 0.5056486, 0.4878751,
	0.4702678, 0.4528166, 0.4355124, 0.4183464, 0.4013104,
	0.3843968, 0.3675981, 0.3509074, 0.3343180, 0.3178237,
	0.3014185, 0.2850964, 0.2688521, 0.2526802, 0.2365756,
	0.2205333, 0.2045484, 0.1886164, 0.1727327, 0.1568929,
	0.1410927, 0.1253278, 0.1095943, 0.09388787, 0.07820469,
	0.06254076, 0.04689218, 0.03125509, 0.01562564
};

uint16_t pass = 0;
uint16_t fail = 0;

//
// Function Prototypes
//
void CLA_runTest(void);
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void);
__interrupt void cla1_task2_isr(void);
__interrupt void cla1_task3_isr(void);
__interrupt void cla1_task4_isr(void);
__interrupt void cla1_task5_isr(void);
__interrupt void cla1_task6_isr(void);
__interrupt void cla1_task7_isr(void);
__interrupt void cla1_task8_isr(void);

//
// Main
//
int main(void)
{

	InitSysCtrl();

	DINT;

	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	CLA_configClaMemory();

	CLA_initCpu1Cla1();

	EINT;

	CLA_runTest();

	for (;;)
	{

	}

	return 0;
}

//
// CLA_runTest - Execute CLA task tests for specified vectors
//
void CLA_runTest(void)
{
	int16_t i;
	float error;

	for (i = 0; i < BUFFER_SIZE; i++)
	{
		fVal = (float) (BUFFER_SIZE - i) / (float) BUFFER_SIZE;

		Cla1ForceTask1andWait();

		//效果演示用 延迟效果
		//通过Liveview视图 刷新可查看cla task1执行计算过程
		for (u32 i = 0; i < 1000000; ++i)
		{
		}

		y[i] = fResult;
		error = fabsf(asin_expected[i] - y[i]);

		if (error < 0.1f)
		{
			pass++;
		}
		else
		{
			fail++;
		}
	}

}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
	EALLOW;
	//友商F28035老版本CLA协处理器的配置方式
	//Cla1Regs.MMEMCFG.bit.PROGE         = 1;
	//Cla1Regs.MCTL.bit.IACKE        = 1;
	//Cla1Regs.MMEMCFG.bit.RAM0E        = CLARAM0_ENABLE;
	//Cla1Regs.MMEMCFG.bit.RAM1E        = CLARAM1_ENABLE;
	//昊芯CLA协处理器的配置方式，LS1配置为CLA独享的代码区，LS0和LS2配置为CPU和CLA共享的数据区
	Cla1Regs.LSMSEL.bit.MSEL_LS0 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS1 = 1;
	Cla1Regs.LSMSEL.bit.MSEL_LS2 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS0 = 0;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS1 = 1;
	Cla1Regs.LSCLAPGM.bit.CLAPGM_LS2 = 0;
	EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
	EALLOW;
	PieVectTable.CLA1_INT1 = &cla1_task1_isr;
	PieVectTable.CLA1_INT2 = &cla1_task2_isr;
	PieVectTable.CLA1_INT3 = &cla1_task3_isr;
	PieVectTable.CLA1_INT4 = &cla1_task4_isr;
	PieVectTable.CLA1_INT5 = &cla1_task5_isr;
	PieVectTable.CLA1_INT6 = &cla1_task6_isr;
	PieVectTable.CLA1_INT7 = &cla1_task7_isr;
	PieVectTable.CLA1_INT8 = &cla1_task8_isr;
	EDIS;

	//
	// Compute all CLA task vectors
	//
	EALLOW;
	Cla1Regs.MVECT1 = (Uint32) &Cla1Task1;
	Cla1Regs.MVECT2 = (Uint32) &Cla1Task2;
	Cla1Regs.MVECT3 = (Uint32) &Cla1Task3;
	Cla1Regs.MVECT4 = (Uint32) &Cla1Task4;
	Cla1Regs.MVECT5 = (Uint32) &Cla1Task5;
	Cla1Regs.MVECT6 = (Uint32) &Cla1Task6;
	Cla1Regs.MVECT7 = (Uint32) &Cla1Task7;
	Cla1Regs.MVECT8 = (Uint32) &Cla1Task8;
	Cla1Regs._MVECTBGRND =   (uint32)&Cla1BackgroundTask;
	Cla1Regs.MIER.all = 0xFF;

	Cla1Regs.CLA1TASKSRCSEL1.bit.TASK1 = 0;
    Cla1Regs._MCTLBGRND.bit.BGEN = 1;
	Cla1Regs._MCTLBGRND.bit.BGSTART = 1;
	EDIS;

	PieCtrlRegs.PIEIER11.all = 0xFFFF;
	IER |= M_INT11;
	EINT;
}

//
// CLA ISRs
//
__interrupt void cla1_task1_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task2_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task3_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task4_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task5_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task6_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task7_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

__interrupt void cla1_task8_isr(void)
{
	PieCtrlRegs.PIEACK.bit.ACK11 = 1;
}

//
// End of file
//
