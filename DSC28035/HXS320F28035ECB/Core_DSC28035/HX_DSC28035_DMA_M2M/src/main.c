/******************************************************************
 文 档 名：      HXcore_DSC28035_DMA_M2M
 开 发 环 境：  Haawking IDE V2.1.4
 开 发 板：      Core_DSC28035_V1.0
                       Start_DSC28035
 D S P：          DSC28035
 使 用 库：
 -------------------------- 例程使用说明 --------------------------
 功能描述：芯片主频120MHz，通过DMA实现从内存到内存的数据传输

 连接方式：

 现象：
 Core: DMA传输数据成功并且正确，LED1(GPIO31)点亮； DMA传输数据失败或不一致，
 LED1(GPIO31)灭
 Start: DMA传输数据成功并且正确，D402(GPIO43)点亮； DMA传输数据失败或不一致，
D402 LED1(GPIO43)灭

 版 本：      V1.0.0
 时 间：      2023年4月4日
 作 者：
 @ mail：   support@mail.haawking.com
 ******************************************************************/
//
// Included Files
//
#include "dsc_config.h"
#include <syscalls.h>
#include "IQmathLib.h"
//#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File

//
// Defines
//
#define BUF_SIZE   1024         // Sample buffer size

//
// DMA Defines
//
#define CH1_TOTAL               DATA_POINTS_PER_CHANNEL
#define CH1_WORDS_PER_BURST     ADC_CHANNELS_TO_CONVERT

#pragma DATA_SECTION(DMABuf1,"DMARAML4");
#pragma DATA_SECTION(DMABuf2,"DMARAML5");

volatile Uint16 DMABuf1[1024];
volatile Uint16 DMABuf2[1024];

volatile Uint16 *DMADest;
volatile Uint16 *DMASource;

//
// Function Prototypes
//
__interrupt void local_DINTCH1_ISR(void);

void InitLED(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX2.bit.GPIO31=0;
	GpioCtrlRegs.GPADIR.bit.GPIO31=1;
	EDIS;
}

// Main
//
int main(void)
{
    Uint16 i;

    //
    // Step 1. Initialize System Control:
    // PLL, WatchDog, enable Peripheral Clocks
    //

    InitSysCtrl();
    InitLED();
    //
    //
    // Step 3. Clear all interrupts and initialize PIE vector table:
    // Disable CPU interrupts
    //
    DINT;

    //
    // Initialize the PIE control registers to their default state.
    // The default state is all PIE interrupts disabled and flags
    // are cleared.
    //
    InitPieCtrl();

    //
    // Disable CPU interrupts and clear all CPU interrupt flags
    //
    IER = 0x0000;
    IFR = 0x0000;

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    // This will populate the entire table, even if the interrupt
    // is not used in this example.  This is useful for debug purposes.
    // The shell ISR routines are found in DSP2833x_DefaultIsr.c.
    // This function is found in DSP2833x_PieVect.c.
    //
    InitPieVectTable();

    //
    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
    //
    EALLOW;	// Allow access to EALLOW protected registers
    PieVectTable.DINTCH1= &local_DINTCH1_ISR;
    EDIS;   // Disable access to EALLOW protected registers

    IER = M_INT7 ;                        //Enable INT7 (7.1 DMA Ch1)
    EnableInterrupts();
    CpuTimer0Regs.TCR.bit.TSS  = 1;       //Stop Timer0 for now

    //
    // Step 5. User specific code, enable interrupts:
    //

    //
    // Initialize DMA
    //
    DMAInitialize();

    //
    // Initialize Tables
    //
    for (i=0; i<BUF_SIZE; i++)
    {
        DMABuf1[i] = 0;
        DMABuf2[i] = i;
    }

    //
    // Configure DMA Channel  配置DMA通道
    //
    DMADest   = &DMABuf1[0];
    DMASource = &DMABuf2[0];
    DMACH1AddrConfig(DMADest,DMASource);  //配置DMA的数据的目的地址和源地址

    //
    // Will set up to use 32-bit datasize, pointers are based on 16-bit words
    //
    DMACH1BurstConfig(31,2,2);  //配置每帧多少字，帧内源地址增加偏移和帧内目的地址增加偏移  帧循环 内循环

    //
    // so need to increment by 2 to grab the correct location
    //
    DMACH1TransferConfig(31,2,2);  //配置每次触发DMA转移多少帧，帧间源地址增加偏移和帧间目的地址增加偏移 地址增加偏移就是指传输一个帧的地址增量 传送循环 外循环
    DMACH1WrapConfig(0xFFFF,0,0xFFFF,0);  //打包过程配置

    //
    // Use timer0 to start the x-fer.
    // Since this is a static copy use one shot mode, so only one trigger
    // is needed. Also using 32-bit mode to decrease x-fer time
    //
    DMACH1ModeConfig(DMA_TINT0,PERINT_ENABLE,ONESHOT_ENABLE,CONT_DISABLE,
                     SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,THIRTYTWO_BIT,
                     CHINT_END,CHINT_ENABLE);  //DMA模式寄存器

    StartDMACH1();

    //
    // Init the timer 0
    //

    //
    // load low value so we can start the DMA quickly
    //
    CpuTimer0Regs.TIM.all = 512;
    CpuTimer0Regs.TCR.bit.SOFT = 1;      //Allow to free run even if halted
    CpuTimer0Regs.TCR.bit.FREE = 1;
    CpuTimer0Regs.TCR.bit.TIE  = 1;      //Enable the timer0 interrupt signal
    CpuTimer0Regs.TCR.bit.TSS  = 0;      //restart the timer 0
    for(;;)
    {

    }

    return 0;
}

//
// local_DINTCH1_ISR - INT7.1(DMA Channel 1)
//
__interrupt void
local_DINTCH1_ISR(void)
{
    //
    // To receive more interrupts from this PIE group, acknowledge this
    // interrupt
    //
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;

    //
    // Next two lines for debug only to halt the processor here
    // Remove after inserting ISR Code
    //
    //__asm ("      ESTOP0");
    GpioDataRegs.GPACLEAR.bit.GPIO31=1;

    for(;;);
}

//
// End of file
//

// ----------------------------------------------------------------------------
